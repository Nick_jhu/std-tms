function Init3DAnimate(n, t) {
    var y = 400,
        b = 300,
        l = new THREE.Scene,
        a = new THREE.OrthographicCamera(y / -2, y / 2, b / 2, b / -2, -1e3, 1e3),
        p = new THREE.WebGLRenderer({
            antialias: !0
        }),
        f, v, w, u, d;
    p.setPixelRatio(window.devicePixelRatio);
    p.setSize(y, b);
    document.getElementById(t).appendChild(p.domElement);
    var e = y / n.size[2],
        i = {
            x: n.size[0] * e,
            y: n.size[1] * e,
            z: n.size[2] * e
        },
        k = new THREE.Object3D,
        s = new THREE.Geometry;
    for (s.vertices.push(new THREE.Vector3(i.x / 2, -i.y / 2 + .5, -i.z / 2)), s.vertices.push(new THREE.Vector3(-i.x / 2, -i.y / 2 + .5, -i.z / 2)), s.vertices.push(new THREE.Vector3(i.x / 2, -i.y / 2, -i.z / 2 + .5)), s.vertices.push(new THREE.Vector3(i.x / 2, i.y / 2, -i.z / 2 + .5)), s.vertices.push(new THREE.Vector3(i.x / 2 - .5, -i.y / 2, -i.z / 2)), s.vertices.push(new THREE.Vector3(i.x / 2 - .5, -i.y / 2, i.z / 2)), k.add(new THREE.LineSegments(s, new THREE.LineBasicMaterial({
            color: 16777215
        }))), k.add(new THREE.Mesh(new THREE.BoxGeometry(i.x, i.y, i.z), new THREE.MeshFaceMaterial([new THREE.MeshBasicMaterial({
            color: 13421772,
            side: THREE.DoubleSide
        }), new THREE.MeshNormalMaterial({
            transparent: !0,
            opacity: 0
        }), new THREE.MeshNormalMaterial({
            transparent: !0,
            opacity: 0
        }), new THREE.MeshBasicMaterial({
            color: 13421772,
            side: THREE.DoubleSide
        }), new THREE.MeshNormalMaterial({
            transparent: !0,
            opacity: 0
        }), new THREE.MeshBasicMaterial({
            color: 13421772,
            side: THREE.DoubleSide
        })]))), l.add(k), f = [], v = 0; v < n.boxes.length; v++) {
        var o = n.boxes[v],
            r = {
                x: o.size[0] * e,
                y: o.size[1] * e,
                z: o.size[2] * e
            },
            h = new THREE.Object3D,
            c = new THREE.Geometry;
        c.vertices.push(new THREE.Vector3(-r.x / 2 - .5, r.y / 2 + .5, r.z / 2 + .5));
        c.vertices.push(new THREE.Vector3(r.x / 2 - .5, r.y / 2 + .5, r.z / 2 + .5));
        c.vertices.push(new THREE.Vector3(-r.x / 2 - .5, r.y / 2 + .5, r.z / 2 + .5));
        c.vertices.push(new THREE.Vector3(-r.x / 2 - .5, -r.y / 2 + .5, r.z / 2 + .5));
        c.vertices.push(new THREE.Vector3(-r.x / 2 - .5, r.y / 2 + .5, r.z / 2 + .5));
        c.vertices.push(new THREE.Vector3(-r.x / 2 - .5, r.y / 2 + .5, -r.z / 2 + .5));
        h.add(new THREE.LineSegments(c, new THREE.LineBasicMaterial({
            color: o.lineColor
        })));
        h.add(new THREE.Mesh(new THREE.BoxGeometry(r.x, r.y, r.z), new THREE.MeshBasicMaterial({
            color: o.color
        })));
        w = {
            x: o.pos[0] * e,
            y: o.pos[1] * e,
            z: o.pos[2] * e
        };
        h.position.x = i.x / 2 - w.x - r.x / 2;
        h.position.y = -i.y / 2 + w.y + r.y / 2;
        h.position.z = i.z / 2 + r.z / 2;
        h.expectedZ = -i.z / 2 + w.z + r.z / 2;
        f[v] = h
    }
    a.position.x = -100;
    a.position.y = 50;
    a.position.z = 100;
    a.lookAt(l.position);
    u = -1;
    d = function() {
        if(u==n.boxes.length)
            return;
        requestAnimationFrame(d);
        u == -1 ? (u++, l.add(f[u])) : (f[u].position.setZ(f[u].position.z - 10), f[u].position.z <= f[u].expectedZ && (f[u].position.setZ(f[u].expectedZ), u++, u < f.length ? l.add(f[u]) : ""));
        p.render(l, a)
    };
    d()
}
var THREE = {
    REVISION: "76"
};
typeof define == "function" && define.amd ? define("three", THREE) : "undefined" != typeof exports && "undefined" != typeof module && (module.exports = THREE);
Number.EPSILON === undefined && (Number.EPSILON = Math.pow(2, -52));
Math.sign === undefined && (Math.sign = function(n) {
    return n < 0 ? -1 : n > 0 ? 1 : +n
});
Function.prototype.name === undefined && Object.defineProperty !== undefined && Object.defineProperty(Function.prototype, "name", {
    get: function() {
        return this.toString().match(/^\s*function\s*(\S*)\s*\(/)[1]
    }
});
Object.assign === undefined && Object.defineProperty(Object, "assign", {
    writable: !0,
    configurable: !0,
    value: function(n) {
        "use strict";
        var f, i, s, t, e, r, h, u, o;
        if (n === undefined || n === null) throw new TypeError("Cannot convert first argument to object");
        for (f = Object(n), i = 1, s = arguments.length; i !== s; ++i)
            if (t = arguments[i], t !== undefined && t !== null)
                for (t = Object(t), e = Object.keys(t), r = 0, h = e.length; r !== h; ++r) u = e[r], o = Object.getOwnPropertyDescriptor(t, u), o !== undefined && o.enumerable && (f[u] = t[u]);
        return f
    }
});
THREE.MOUSE = {
    LEFT: 0,
    MIDDLE: 1,
    RIGHT: 2
};
THREE.CullFaceNone = 0;
THREE.CullFaceBack = 1;
THREE.CullFaceFront = 2;
THREE.CullFaceFrontBack = 3;
THREE.FrontFaceDirectionCW = 0;
THREE.FrontFaceDirectionCCW = 1;
THREE.BasicShadowMap = 0;
THREE.PCFShadowMap = 1;
THREE.PCFSoftShadowMap = 2;
THREE.FrontSide = 0;
THREE.BackSide = 1;
THREE.DoubleSide = 2;
THREE.FlatShading = 1;
THREE.SmoothShading = 2;
THREE.NoColors = 0;
THREE.FaceColors = 1;
THREE.VertexColors = 2;
THREE.NoBlending = 0;
THREE.NormalBlending = 1;
THREE.AdditiveBlending = 2;
THREE.SubtractiveBlending = 3;
THREE.MultiplyBlending = 4;
THREE.CustomBlending = 5;
THREE.AddEquation = 100;
THREE.SubtractEquation = 101;
THREE.ReverseSubtractEquation = 102;
THREE.MinEquation = 103;
THREE.MaxEquation = 104;
THREE.ZeroFactor = 200;
THREE.OneFactor = 201;
THREE.SrcColorFactor = 202;
THREE.OneMinusSrcColorFactor = 203;
THREE.SrcAlphaFactor = 204;
THREE.OneMinusSrcAlphaFactor = 205;
THREE.DstAlphaFactor = 206;
THREE.OneMinusDstAlphaFactor = 207;
THREE.DstColorFactor = 208;
THREE.OneMinusDstColorFactor = 209;
THREE.SrcAlphaSaturateFactor = 210;
THREE.NeverDepth = 0;
THREE.AlwaysDepth = 1;
THREE.LessDepth = 2;
THREE.LessEqualDepth = 3;
THREE.EqualDepth = 4;
THREE.GreaterEqualDepth = 5;
THREE.GreaterDepth = 6;
THREE.NotEqualDepth = 7;
THREE.MultiplyOperation = 0;
THREE.MixOperation = 1;
THREE.AddOperation = 2;
THREE.NoToneMapping = 0;
THREE.LinearToneMapping = 1;
THREE.ReinhardToneMapping = 2;
THREE.Uncharted2ToneMapping = 3;
THREE.CineonToneMapping = 4;
THREE.UVMapping = 300;
THREE.CubeReflectionMapping = 301;
THREE.CubeRefractionMapping = 302;
THREE.EquirectangularReflectionMapping = 303;
THREE.EquirectangularRefractionMapping = 304;
THREE.SphericalReflectionMapping = 305;
THREE.CubeUVReflectionMapping = 306;
THREE.CubeUVRefractionMapping = 307;
THREE.RepeatWrapping = 1e3;
THREE.ClampToEdgeWrapping = 1001;
THREE.MirroredRepeatWrapping = 1002;
THREE.NearestFilter = 1003;
THREE.NearestMipMapNearestFilter = 1004;
THREE.NearestMipMapLinearFilter = 1005;
THREE.LinearFilter = 1006;
THREE.LinearMipMapNearestFilter = 1007;
THREE.LinearMipMapLinearFilter = 1008;
THREE.UnsignedByteType = 1009;
THREE.ByteType = 1010;
THREE.ShortType = 1011;
THREE.UnsignedShortType = 1012;
THREE.IntType = 1013;
THREE.UnsignedIntType = 1014;
THREE.FloatType = 1015;
THREE.HalfFloatType = 1025;
THREE.UnsignedShort4444Type = 1016;
THREE.UnsignedShort5551Type = 1017;
THREE.UnsignedShort565Type = 1018;
THREE.AlphaFormat = 1019;
THREE.RGBFormat = 1020;
THREE.RGBAFormat = 1021;
THREE.LuminanceFormat = 1022;
THREE.LuminanceAlphaFormat = 1023;
THREE.RGBEFormat = THREE.RGBAFormat;
THREE.DepthFormat = 1026;
THREE.RGB_S3TC_DXT1_Format = 2001;
THREE.RGBA_S3TC_DXT1_Format = 2002;
THREE.RGBA_S3TC_DXT3_Format = 2003;
THREE.RGBA_S3TC_DXT5_Format = 2004;
THREE.RGB_PVRTC_4BPPV1_Format = 2100;
THREE.RGB_PVRTC_2BPPV1_Format = 2101;
THREE.RGBA_PVRTC_4BPPV1_Format = 2102;
THREE.RGBA_PVRTC_2BPPV1_Format = 2103;
THREE.RGB_ETC1_Format = 2151;
THREE.LoopOnce = 2200;
THREE.LoopRepeat = 2201;
THREE.LoopPingPong = 2202;
THREE.InterpolateDiscrete = 2300;
THREE.InterpolateLinear = 2301;
THREE.InterpolateSmooth = 2302;
THREE.ZeroCurvatureEnding = 2400;
THREE.ZeroSlopeEnding = 2401;
THREE.WrapAroundEnding = 2402;
THREE.TrianglesDrawMode = 0;
THREE.TriangleStripDrawMode = 1;
THREE.TriangleFanDrawMode = 2;
THREE.LinearEncoding = 3e3;
THREE.sRGBEncoding = 3001;
THREE.GammaEncoding = 3007;
THREE.RGBEEncoding = 3002;
THREE.LogLuvEncoding = 3003;
THREE.RGBM7Encoding = 3004;
THREE.RGBM16Encoding = 3005;
THREE.RGBDEncoding = 3006;
THREE.BasicDepthPacking = 3200;
THREE.RGBADepthPacking = 3201;
THREE.Color = function(n) {
    return arguments.length === 3 ? this.fromArray(arguments) : this.set(n)
};
THREE.Color.prototype = {
    constructor: THREE.Color,
    r: 1,
    g: 1,
    b: 1,
    set: function(n) {
        return n instanceof THREE.Color ? this.copy(n) : typeof n == "number" ? this.setHex(n) : typeof n == "string" && this.setStyle(n), this
    },
    setScalar: function(n) {
        this.r = n;
        this.g = n;
        this.b = n
    },
    setHex: function(n) {
        return n = Math.floor(n), this.r = (n >> 16 & 255) / 255, this.g = (n >> 8 & 255) / 255, this.b = (n & 255) / 255, this
    },
    setRGB: function(n, t, i) {
        return this.r = n, this.g = t, this.b = i, this
    },
    setHSL: function() {
        function n(n, t, i) {
            return (i < 0 && (i += 1), i > 1 && (i -= 1), i < 1 / 6) ? n + (t - n) * 6 * i : i < 1 / 2 ? t : i < 2 / 3 ? n + (t - n) * 6 * (2 / 3 - i) : n
        }
        return function(t, i, r) {
            if (t = THREE.Math.euclideanModulo(t, 1), i = THREE.Math.clamp(i, 0, 1), r = THREE.Math.clamp(r, 0, 1), i === 0) this.r = this.g = this.b = r;
            else {
                var u = r <= .5 ? r * (1 + i) : r + i - r * i,
                    f = 2 * r - u;
                this.r = n(f, u, t + 1 / 3);
                this.g = n(f, u, t);
                this.b = n(f, u, t - 1 / 3)
            }
            return this
        }
    }(),
    setStyle: function(n) {
        function f(t) {
            t !== undefined && parseFloat(t) < 1 && console.warn("THREE.Color: Alpha component of " + n + " will be ignored.")
        }
        var r, i, o, u, e, t;
        if (r = /^((?:rgb|hsl)a?)\(\s*([^\)]*)\)/.exec(n)) {
            o = r[1];
            u = r[2];
            switch (o) {
                case "rgb":
                case "rgba":
                    if (i = /^(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*(,\s*([0-9]*\.?[0-9]+)\s*)?$/.exec(u)) return this.r = Math.min(255, parseInt(i[1], 10)) / 255, this.g = Math.min(255, parseInt(i[2], 10)) / 255, this.b = Math.min(255, parseInt(i[3], 10)) / 255, f(i[5]), this;
                    if (i = /^(\d+)\%\s*,\s*(\d+)\%\s*,\s*(\d+)\%\s*(,\s*([0-9]*\.?[0-9]+)\s*)?$/.exec(u)) return this.r = Math.min(100, parseInt(i[1], 10)) / 100, this.g = Math.min(100, parseInt(i[2], 10)) / 100, this.b = Math.min(100, parseInt(i[3], 10)) / 100, f(i[5]), this;
                    break;
                case "hsl":
                case "hsla":
                    if (i = /^([0-9]*\.?[0-9]+)\s*,\s*(\d+)\%\s*,\s*(\d+)\%\s*(,\s*([0-9]*\.?[0-9]+)\s*)?$/.exec(u)) {
                        var s = parseFloat(i[1]) / 360,
                            h = parseInt(i[2], 10) / 100,
                            c = parseInt(i[3], 10) / 100;
                        return f(i[5]), this.setHSL(s, h, c)
                    }
            }
        } else if (r = /^\#([A-Fa-f0-9]+)$/.exec(n)) {
            if (t = r[1], e = t.length, e === 3) return this.r = parseInt(t.charAt(0) + t.charAt(0), 16) / 255, this.g = parseInt(t.charAt(1) + t.charAt(1), 16) / 255, this.b = parseInt(t.charAt(2) + t.charAt(2), 16) / 255, this;
            if (e === 6) return this.r = parseInt(t.charAt(0) + t.charAt(1), 16) / 255, this.g = parseInt(t.charAt(2) + t.charAt(3), 16) / 255, this.b = parseInt(t.charAt(4) + t.charAt(5), 16) / 255, this
        }
        return n && n.length > 0 && (t = THREE.ColorKeywords[n], t !== undefined ? this.setHex(t) : console.warn("THREE.Color: Unknown color " + n)), this
    },
    clone: function() {
        return new this.constructor(this.r, this.g, this.b)
    },
    copy: function(n) {
        return this.r = n.r, this.g = n.g, this.b = n.b, this
    },
    copyGammaToLinear: function(n, t) {
        return t === undefined && (t = 2), this.r = Math.pow(n.r, t), this.g = Math.pow(n.g, t), this.b = Math.pow(n.b, t), this
    },
    copyLinearToGamma: function(n, t) {
        t === undefined && (t = 2);
        var i = t > 0 ? 1 / t : 1;
        return this.r = Math.pow(n.r, i), this.g = Math.pow(n.g, i), this.b = Math.pow(n.b, i), this
    },
    convertGammaToLinear: function() {
        var n = this.r,
            t = this.g,
            i = this.b;
        return this.r = n * n, this.g = t * t, this.b = i * i, this
    },
    convertLinearToGamma: function() {
        return this.r = Math.sqrt(this.r), this.g = Math.sqrt(this.g), this.b = Math.sqrt(this.b), this
    },
    getHex: function() {
        return this.r * 255 << 16 ^ this.g * 255 << 8 ^ this.b * 255 << 0
    },
    getHexString: function() {
        return ("000000" + this.getHex().toString(16)).slice(-6)
    },
    getHSL: function(n) {
        var s = n || {
                h: 0,
                s: 0,
                l: 0
            },
            e = this.r,
            t = this.g,
            i = this.b,
            r = Math.max(e, t, i),
            o = Math.min(e, t, i),
            u, h, c = (o + r) / 2,
            f;
        if (o === r) u = 0, h = 0;
        else {
            f = r - o;
            h = c <= .5 ? f / (r + o) : f / (2 - r - o);
            switch (r) {
                case e:
                    u = (t - i) / f + (t < i ? 6 : 0);
                    break;
                case t:
                    u = (i - e) / f + 2;
                    break;
                case i:
                    u = (e - t) / f + 4
            }
            u /= 6
        }
        return s.h = u, s.s = h, s.l = c, s
    },
    getStyle: function() {
        return "rgb(" + (this.r * 255 | 0) + "," + (this.g * 255 | 0) + "," + (this.b * 255 | 0) + ")"
    },
    offsetHSL: function(n, t, i) {
        var r = this.getHSL();
        return r.h += n, r.s += t, r.l += i, this.setHSL(r.h, r.s, r.l), this
    },
    add: function(n) {
        return this.r += n.r, this.g += n.g, this.b += n.b, this
    },
    addColors: function(n, t) {
        return this.r = n.r + t.r, this.g = n.g + t.g, this.b = n.b + t.b, this
    },
    addScalar: function(n) {
        return this.r += n, this.g += n, this.b += n, this
    },
    multiply: function(n) {
        return this.r *= n.r, this.g *= n.g, this.b *= n.b, this
    },
    multiplyScalar: function(n) {
        return this.r *= n, this.g *= n, this.b *= n, this
    },
    lerp: function(n, t) {
        return this.r += (n.r - this.r) * t, this.g += (n.g - this.g) * t, this.b += (n.b - this.b) * t, this
    },
    equals: function(n) {
        return n.r === this.r && n.g === this.g && n.b === this.b
    },
    fromArray: function(n, t) {
        return t === undefined && (t = 0), this.r = n[t], this.g = n[t + 1], this.b = n[t + 2], this
    },
    toArray: function(n, t) {
        return n === undefined && (n = []), t === undefined && (t = 0), n[t] = this.r, n[t + 1] = this.g, n[t + 2] = this.b, n
    }
};
THREE.ColorKeywords = {
    aliceblue: 15792383,
    antiquewhite: 16444375,
    aqua: 65535,
    aquamarine: 8388564,
    azure: 15794175,
    beige: 16119260,
    bisque: 16770244,
    black: 0,
    blanchedalmond: 16772045,
    blue: 255,
    blueviolet: 9055202,
    brown: 10824234,
    burlywood: 14596231,
    cadetblue: 6266528,
    chartreuse: 8388352,
    chocolate: 13789470,
    coral: 16744272,
    cornflowerblue: 6591981,
    cornsilk: 16775388,
    crimson: 14423100,
    cyan: 65535,
    darkblue: 139,
    darkcyan: 35723,
    darkgoldenrod: 12092939,
    darkgray: 11119017,
    darkgreen: 25600,
    darkgrey: 11119017,
    darkkhaki: 12433259,
    darkmagenta: 9109643,
    darkolivegreen: 5597999,
    darkorange: 16747520,
    darkorchid: 10040012,
    darkred: 9109504,
    darksalmon: 15308410,
    darkseagreen: 9419919,
    darkslateblue: 4734347,
    darkslategray: 3100495,
    darkslategrey: 3100495,
    darkturquoise: 52945,
    darkviolet: 9699539,
    deeppink: 16716947,
    deepskyblue: 49151,
    dimgray: 6908265,
    dimgrey: 6908265,
    dodgerblue: 2003199,
    firebrick: 11674146,
    floralwhite: 16775920,
    forestgreen: 2263842,
    fuchsia: 16711935,
    gainsboro: 14474460,
    ghostwhite: 16316671,
    gold: 16766720,
    goldenrod: 14329120,
    gray: 8421504,
    green: 32768,
    greenyellow: 11403055,
    grey: 8421504,
    honeydew: 15794160,
    hotpink: 16738740,
    indianred: 13458524,
    indigo: 4915330,
    ivory: 16777200,
    khaki: 15787660,
    lavender: 15132410,
    lavenderblush: 16773365,
    lawngreen: 8190976,
    lemonchiffon: 16775885,
    lightblue: 11393254,
    lightcoral: 15761536,
    lightcyan: 14745599,
    lightgoldenrodyellow: 16448210,
    lightgray: 13882323,
    lightgreen: 9498256,
    lightgrey: 13882323,
    lightpink: 16758465,
    lightsalmon: 16752762,
    lightseagreen: 2142890,
    lightskyblue: 8900346,
    lightslategray: 7833753,
    lightslategrey: 7833753,
    lightsteelblue: 11584734,
    lightyellow: 16777184,
    lime: 65280,
    limegreen: 3329330,
    linen: 16445670,
    magenta: 16711935,
    maroon: 8388608,
    mediumaquamarine: 6737322,
    mediumblue: 205,
    mediumorchid: 12211667,
    mediumpurple: 9662683,
    mediumseagreen: 3978097,
    mediumslateblue: 8087790,
    mediumspringgreen: 64154,
    mediumturquoise: 4772300,
    mediumvioletred: 13047173,
    midnightblue: 1644912,
    mintcream: 16121850,
    mistyrose: 16770273,
    moccasin: 16770229,
    navajowhite: 16768685,
    navy: 128,
    oldlace: 16643558,
    olive: 8421376,
    olivedrab: 7048739,
    orange: 16753920,
    orangered: 16729344,
    orchid: 14315734,
    palegoldenrod: 15657130,
    palegreen: 10025880,
    paleturquoise: 11529966,
    palevioletred: 14381203,
    papayawhip: 16773077,
    peachpuff: 16767673,
    peru: 13468991,
    pink: 16761035,
    plum: 14524637,
    powderblue: 11591910,
    purple: 8388736,
    red: 16711680,
    rosybrown: 12357519,
    royalblue: 4286945,
    saddlebrown: 9127187,
    salmon: 16416882,
    sandybrown: 16032864,
    seagreen: 3050327,
    seashell: 16774638,
    sienna: 10506797,
    silver: 12632256,
    skyblue: 8900331,
    slateblue: 6970061,
    slategray: 7372944,
    slategrey: 7372944,
    snow: 16775930,
    springgreen: 65407,
    steelblue: 4620980,
    tan: 13808780,
    teal: 32896,
    thistle: 14204888,
    tomato: 16737095,
    turquoise: 4251856,
    violet: 15631086,
    wheat: 16113331,
    white: 16777215,
    whitesmoke: 16119285,
    yellow: 16776960,
    yellowgreen: 10145074
};
THREE.Quaternion = function(n, t, i, r) {
    this._x = n || 0;
    this._y = t || 0;
    this._z = i || 0;
    this._w = r !== undefined ? r : 1
};
THREE.Quaternion.prototype = {
    constructor: THREE.Quaternion,
    get x() {
        return this._x
    },
    set x(n) {
        this._x = n;
        this.onChangeCallback()
    },
    get y() {
        return this._y
    },
    set y(n) {
        this._y = n;
        this.onChangeCallback()
    },
    get z() {
        return this._z
    },
    set z(n) {
        this._z = n;
        this.onChangeCallback()
    },
    get w() {
        return this._w
    },
    set w(n) {
        this._w = n;
        this.onChangeCallback()
    },
    set: function(n, t, i, r) {
        return this._x = n, this._y = t, this._z = i, this._w = r, this.onChangeCallback(), this
    },
    clone: function() {
        return new this.constructor(this._x, this._y, this._z, this._w)
    },
    copy: function(n) {
        return this._x = n.x, this._y = n.y, this._z = n.z, this._w = n.w, this.onChangeCallback(), this
    },
    setFromEuler: function(n, t) {
        if (n instanceof THREE.Euler == !1) throw new Error("THREE.Quaternion: .setFromEuler() now expects a Euler rotation rather than a Vector3 and order.");
        var i = Math.cos(n._x / 2),
            r = Math.cos(n._y / 2),
            u = Math.cos(n._z / 2),
            f = Math.sin(n._x / 2),
            e = Math.sin(n._y / 2),
            o = Math.sin(n._z / 2),
            s = n.order;
        return s === "XYZ" ? (this._x = f * r * u + i * e * o, this._y = i * e * u - f * r * o, this._z = i * r * o + f * e * u, this._w = i * r * u - f * e * o) : s === "YXZ" ? (this._x = f * r * u + i * e * o, this._y = i * e * u - f * r * o, this._z = i * r * o - f * e * u, this._w = i * r * u + f * e * o) : s === "ZXY" ? (this._x = f * r * u - i * e * o, this._y = i * e * u + f * r * o, this._z = i * r * o + f * e * u, this._w = i * r * u - f * e * o) : s === "ZYX" ? (this._x = f * r * u - i * e * o, this._y = i * e * u + f * r * o, this._z = i * r * o - f * e * u, this._w = i * r * u + f * e * o) : s === "YZX" ? (this._x = f * r * u + i * e * o, this._y = i * e * u + f * r * o, this._z = i * r * o - f * e * u, this._w = i * r * u - f * e * o) : s === "XZY" && (this._x = f * r * u - i * e * o, this._y = i * e * u - f * r * o, this._z = i * r * o + f * e * u, this._w = i * r * u + f * e * o), t !== !1 && this.onChangeCallback(), this
    },
    setFromAxisAngle: function(n, t) {
        var r = t / 2,
            i = Math.sin(r);
        return this._x = n.x * i, this._y = n.y * i, this._z = n.z * i, this._w = Math.cos(r), this.onChangeCallback(), this
    },
    setFromRotationMatrix: function(n) {
        var i = n.elements,
            r = i[0],
            e = i[4],
            o = i[8],
            s = i[1],
            u = i[5],
            h = i[9],
            c = i[2],
            l = i[6],
            f = i[10],
            a = r + u + f,
            t;
        return a > 0 ? (t = .5 / Math.sqrt(a + 1), this._w = .25 / t, this._x = (l - h) * t, this._y = (o - c) * t, this._z = (s - e) * t) : r > u && r > f ? (t = 2 * Math.sqrt(1 + r - u - f), this._w = (l - h) / t, this._x = .25 * t, this._y = (e + s) / t, this._z = (o + c) / t) : u > f ? (t = 2 * Math.sqrt(1 + u - r - f), this._w = (o - c) / t, this._x = (e + s) / t, this._y = .25 * t, this._z = (h + l) / t) : (t = 2 * Math.sqrt(1 + f - r - u), this._w = (s - e) / t, this._x = (o + c) / t, this._y = (h + l) / t, this._z = .25 * t), this.onChangeCallback(), this
    },
    setFromUnitVectors: function() {
        var n, t, i = 1e-6;
        return function(r, u) {
            return n === undefined && (n = new THREE.Vector3), t = r.dot(u) + 1, t < i ? (t = 0, Math.abs(r.x) > Math.abs(r.z) ? n.set(-r.y, r.x, 0) : n.set(0, -r.z, r.y)) : n.crossVectors(r, u), this._x = n.x, this._y = n.y, this._z = n.z, this._w = t, this.normalize(), this
        }
    }(),
    inverse: function() {
        return this.conjugate().normalize(), this
    },
    conjugate: function() {
        return this._x *= -1, this._y *= -1, this._z *= -1, this.onChangeCallback(), this
    },
    dot: function(n) {
        return this._x * n._x + this._y * n._y + this._z * n._z + this._w * n._w
    },
    lengthSq: function() {
        return this._x * this._x + this._y * this._y + this._z * this._z + this._w * this._w
    },
    length: function() {
        return Math.sqrt(this._x * this._x + this._y * this._y + this._z * this._z + this._w * this._w)
    },
    normalize: function() {
        var n = this.length();
        return n === 0 ? (this._x = 0, this._y = 0, this._z = 0, this._w = 1) : (n = 1 / n, this._x = this._x * n, this._y = this._y * n, this._z = this._z * n, this._w = this._w * n), this.onChangeCallback(), this
    },
    multiply: function(n, t) {
        return t !== undefined ? (console.warn("THREE.Quaternion: .multiply() now only accepts one argument. Use .multiplyQuaternions( a, b ) instead."), this.multiplyQuaternions(n, t)) : this.multiplyQuaternions(this, n)
    },
    multiplyQuaternions: function(n, t) {
        var i = n._x,
            r = n._y,
            u = n._z,
            f = n._w,
            e = t._x,
            o = t._y,
            s = t._z,
            h = t._w;
        return this._x = i * h + f * e + r * s - u * o, this._y = r * h + f * o + u * e - i * s, this._z = u * h + f * s + i * o - r * e, this._w = f * h - i * e - r * o - u * s, this.onChangeCallback(), this
    },
    slerp: function(n, t) {
        var r;
        if (t === 0) return this;
        if (t === 1) return this.copy(n);
        var u = this._x,
            f = this._y,
            e = this._z,
            o = this._w,
            i = o * n._w + u * n._x + f * n._y + e * n._z;
        if (i < 0 ? (this._w = -n._w, this._x = -n._x, this._y = -n._y, this._z = -n._z, i = -i) : this.copy(n), i >= 1) return this._w = o, this._x = u, this._y = f, this._z = e, this;
        if (r = Math.sqrt(1 - i * i), Math.abs(r) < .001) return this._w = .5 * (o + this._w), this._x = .5 * (u + this._x), this._y = .5 * (f + this._y), this._z = .5 * (e + this._z), this;
        var c = Math.atan2(r, i),
            s = Math.sin((1 - t) * c) / r,
            h = Math.sin(t * c) / r;
        return this._w = o * s + this._w * h, this._x = u * s + this._x * h, this._y = f * s + this._y * h, this._z = e * s + this._z * h, this.onChangeCallback(), this
    },
    equals: function(n) {
        return n._x === this._x && n._y === this._y && n._z === this._z && n._w === this._w
    },
    fromArray: function(n, t) {
        return t === undefined && (t = 0), this._x = n[t], this._y = n[t + 1], this._z = n[t + 2], this._w = n[t + 3], this.onChangeCallback(), this
    },
    toArray: function(n, t) {
        return n === undefined && (n = []), t === undefined && (t = 0), n[t] = this._x, n[t + 1] = this._y, n[t + 2] = this._z, n[t + 3] = this._w, n
    },
    onChange: function(n) {
        return this.onChangeCallback = n, this
    },
    onChangeCallback: function() {}
};
Object.assign(THREE.Quaternion, {
    slerp: function(n, t, i, r) {
        return i.copy(n).slerp(t, r)
    },
    slerpFlat: function(n, t, i, r, u, f, e) {
        var o = i[r + 0],
            s = i[r + 1],
            h = i[r + 2],
            c = i[r + 3],
            w = u[f + 0],
            b = u[f + 1],
            k = u[f + 2],
            d = u[f + 3],
            p, g, a, v;
        if (c !== d || o !== w || s !== b || h !== k) {
            var l = 1 - e,
                y = o * w + s * b + h * k + c * d,
                nt = y >= 0 ? 1 : -1,
                tt = 1 - y * y;
            tt > Number.EPSILON && (p = Math.sqrt(tt), g = Math.atan2(p, y * nt), l = Math.sin(l * g) / p, e = Math.sin(e * g) / p);
            a = e * nt;
            o = o * l + w * a;
            s = s * l + b * a;
            h = h * l + k * a;
            c = c * l + d * a;
            l === 1 - e && (v = 1 / Math.sqrt(o * o + s * s + h * h + c * c), o *= v, s *= v, h *= v, c *= v)
        }
        n[t] = o;
        n[t + 1] = s;
        n[t + 2] = h;
        n[t + 3] = c
    }
});
THREE.Vector2 = function(n, t) {
    this.x = n || 0;
    this.y = t || 0
};
THREE.Vector2.prototype = {
    constructor: THREE.Vector2,
    get width() {
        return this.x
    },
    set width(n) {
        this.x = n
    },
    get height() {
        return this.y
    },
    set height(n) {
        this.y = n
    },
    set: function(n, t) {
        return this.x = n, this.y = t, this
    },
    setScalar: function(n) {
        return this.x = n, this.y = n, this
    },
    setX: function(n) {
        return this.x = n, this
    },
    setY: function(n) {
        return this.y = n, this
    },
    setComponent: function(n, t) {
        switch (n) {
            case 0:
                this.x = t;
                break;
            case 1:
                this.y = t;
                break;
            default:
                throw new Error("index is out of range: " + n);
        }
    },
    getComponent: function(n) {
        switch (n) {
            case 0:
                return this.x;
            case 1:
                return this.y;
            default:
                throw new Error("index is out of range: " + n);
        }
    },
    clone: function() {
        return new this.constructor(this.x, this.y)
    },
    copy: function(n) {
        return this.x = n.x, this.y = n.y, this
    },
    add: function(n, t) {
        return t !== undefined ? (console.warn("THREE.Vector2: .add() now only accepts one argument. Use .addVectors( a, b ) instead."), this.addVectors(n, t)) : (this.x += n.x, this.y += n.y, this)
    },
    addScalar: function(n) {
        return this.x += n, this.y += n, this
    },
    addVectors: function(n, t) {
        return this.x = n.x + t.x, this.y = n.y + t.y, this
    },
    addScaledVector: function(n, t) {
        return this.x += n.x * t, this.y += n.y * t, this
    },
    sub: function(n, t) {
        return t !== undefined ? (console.warn("THREE.Vector2: .sub() now only accepts one argument. Use .subVectors( a, b ) instead."), this.subVectors(n, t)) : (this.x -= n.x, this.y -= n.y, this)
    },
    subScalar: function(n) {
        return this.x -= n, this.y -= n, this
    },
    subVectors: function(n, t) {
        return this.x = n.x - t.x, this.y = n.y - t.y, this
    },
    multiply: function(n) {
        return this.x *= n.x, this.y *= n.y, this
    },
    multiplyScalar: function(n) {
        return isFinite(n) ? (this.x *= n, this.y *= n) : (this.x = 0, this.y = 0), this
    },
    divide: function(n) {
        return this.x /= n.x, this.y /= n.y, this
    },
    divideScalar: function(n) {
        return this.multiplyScalar(1 / n)
    },
    min: function(n) {
        return this.x = Math.min(this.x, n.x), this.y = Math.min(this.y, n.y), this
    },
    max: function(n) {
        return this.x = Math.max(this.x, n.x), this.y = Math.max(this.y, n.y), this
    },
    clamp: function(n, t) {
        return this.x = Math.max(n.x, Math.min(t.x, this.x)), this.y = Math.max(n.y, Math.min(t.y, this.y)), this
    },
    clampScalar: function() {
        var n, t;
        return function(i, r) {
            return n === undefined && (n = new THREE.Vector2, t = new THREE.Vector2), n.set(i, i), t.set(r, r), this.clamp(n, t)
        }
    }(),
    clampLength: function(n, t) {
        var i = this.length();
        return this.multiplyScalar(Math.max(n, Math.min(t, i)) / i), this
    },
    floor: function() {
        return this.x = Math.floor(this.x), this.y = Math.floor(this.y), this
    },
    ceil: function() {
        return this.x = Math.ceil(this.x), this.y = Math.ceil(this.y), this
    },
    round: function() {
        return this.x = Math.round(this.x), this.y = Math.round(this.y), this
    },
    roundToZero: function() {
        return this.x = this.x < 0 ? Math.ceil(this.x) : Math.floor(this.x), this.y = this.y < 0 ? Math.ceil(this.y) : Math.floor(this.y), this
    },
    negate: function() {
        return this.x = -this.x, this.y = -this.y, this
    },
    dot: function(n) {
        return this.x * n.x + this.y * n.y
    },
    lengthSq: function() {
        return this.x * this.x + this.y * this.y
    },
    length: function() {
        return Math.sqrt(this.x * this.x + this.y * this.y)
    },
    lengthManhattan: function() {
        return Math.abs(this.x) + Math.abs(this.y)
    },
    normalize: function() {
        return this.divideScalar(this.length())
    },
    angle: function() {
        var n = Math.atan2(this.y, this.x);
        return n < 0 && (n += 2 * Math.PI), n
    },
    distanceTo: function(n) {
        return Math.sqrt(this.distanceToSquared(n))
    },
    distanceToSquared: function(n) {
        var t = this.x - n.x,
            i = this.y - n.y;
        return t * t + i * i
    },
    setLength: function(n) {
        return this.multiplyScalar(n / this.length())
    },
    lerp: function(n, t) {
        return this.x += (n.x - this.x) * t, this.y += (n.y - this.y) * t, this
    },
    lerpVectors: function(n, t, i) {
        return this.subVectors(t, n).multiplyScalar(i).add(n), this
    },
    equals: function(n) {
        return n.x === this.x && n.y === this.y
    },
    fromArray: function(n, t) {
        return t === undefined && (t = 0), this.x = n[t], this.y = n[t + 1], this
    },
    toArray: function(n, t) {
        return n === undefined && (n = []), t === undefined && (t = 0), n[t] = this.x, n[t + 1] = this.y, n
    },
    fromAttribute: function(n, t, i) {
        return i === undefined && (i = 0), t = t * n.itemSize + i, this.x = n.array[t], this.y = n.array[t + 1], this
    },
    rotateAround: function(n, t) {
        var i = Math.cos(t),
            r = Math.sin(t),
            u = this.x - n.x,
            f = this.y - n.y;
        return this.x = u * i - f * r + n.x, this.y = u * r + f * i + n.y, this
    }
};
THREE.Vector3 = function(n, t, i) {
    this.x = n || 0;
    this.y = t || 0;
    this.z = i || 0
};
THREE.Vector3.prototype = {
    constructor: THREE.Vector3,
    set: function(n, t, i) {
        return this.x = n, this.y = t, this.z = i, this
    },
    setScalar: function(n) {
        return this.x = n, this.y = n, this.z = n, this
    },
    setX: function(n) {
        return this.x = n, this
    },
    setY: function(n) {
        return this.y = n, this
    },
    setZ: function(n) {
        return this.z = n, this
    },
    setComponent: function(n, t) {
        switch (n) {
            case 0:
                this.x = t;
                break;
            case 1:
                this.y = t;
                break;
            case 2:
                this.z = t;
                break;
            default:
                throw new Error("index is out of range: " + n);
        }
    },
    getComponent: function(n) {
        switch (n) {
            case 0:
                return this.x;
            case 1:
                return this.y;
            case 2:
                return this.z;
            default:
                throw new Error("index is out of range: " + n);
        }
    },
    clone: function() {
        return new this.constructor(this.x, this.y, this.z)
    },
    copy: function(n) {
        return this.x = n.x, this.y = n.y, this.z = n.z, this
    },
    add: function(n, t) {
        return t !== undefined ? (console.warn("THREE.Vector3: .add() now only accepts one argument. Use .addVectors( a, b ) instead."), this.addVectors(n, t)) : (this.x += n.x, this.y += n.y, this.z += n.z, this)
    },
    addScalar: function(n) {
        return this.x += n, this.y += n, this.z += n, this
    },
    addVectors: function(n, t) {
        return this.x = n.x + t.x, this.y = n.y + t.y, this.z = n.z + t.z, this
    },
    addScaledVector: function(n, t) {
        return this.x += n.x * t, this.y += n.y * t, this.z += n.z * t, this
    },
    sub: function(n, t) {
        return t !== undefined ? (console.warn("THREE.Vector3: .sub() now only accepts one argument. Use .subVectors( a, b ) instead."), this.subVectors(n, t)) : (this.x -= n.x, this.y -= n.y, this.z -= n.z, this)
    },
    subScalar: function(n) {
        return this.x -= n, this.y -= n, this.z -= n, this
    },
    subVectors: function(n, t) {
        return this.x = n.x - t.x, this.y = n.y - t.y, this.z = n.z - t.z, this
    },
    multiply: function(n, t) {
        return t !== undefined ? (console.warn("THREE.Vector3: .multiply() now only accepts one argument. Use .multiplyVectors( a, b ) instead."), this.multiplyVectors(n, t)) : (this.x *= n.x, this.y *= n.y, this.z *= n.z, this)
    },
    multiplyScalar: function(n) {
        return isFinite(n) ? (this.x *= n, this.y *= n, this.z *= n) : (this.x = 0, this.y = 0, this.z = 0), this
    },
    multiplyVectors: function(n, t) {
        return this.x = n.x * t.x, this.y = n.y * t.y, this.z = n.z * t.z, this
    },
    applyEuler: function() {
        var n;
        return function(t) {
            return t instanceof THREE.Euler == !1 && console.error("THREE.Vector3: .applyEuler() now expects an Euler rotation rather than a Vector3 and order."), n === undefined && (n = new THREE.Quaternion), this.applyQuaternion(n.setFromEuler(t)), this
        }
    }(),
    applyAxisAngle: function() {
        var n;
        return function(t, i) {
            return n === undefined && (n = new THREE.Quaternion), this.applyQuaternion(n.setFromAxisAngle(t, i)), this
        }
    }(),
    applyMatrix3: function(n) {
        var i = this.x,
            r = this.y,
            u = this.z,
            t = n.elements;
        return this.x = t[0] * i + t[3] * r + t[6] * u, this.y = t[1] * i + t[4] * r + t[7] * u, this.z = t[2] * i + t[5] * r + t[8] * u, this
    },
    applyMatrix4: function(n) {
        var i = this.x,
            r = this.y,
            u = this.z,
            t = n.elements;
        return this.x = t[0] * i + t[4] * r + t[8] * u + t[12], this.y = t[1] * i + t[5] * r + t[9] * u + t[13], this.z = t[2] * i + t[6] * r + t[10] * u + t[14], this
    },
    applyProjection: function(n) {
        var i = this.x,
            r = this.y,
            u = this.z,
            t = n.elements,
            f = 1 / (t[3] * i + t[7] * r + t[11] * u + t[15]);
        return this.x = (t[0] * i + t[4] * r + t[8] * u + t[12]) * f, this.y = (t[1] * i + t[5] * r + t[9] * u + t[13]) * f, this.z = (t[2] * i + t[6] * r + t[10] * u + t[14]) * f, this
    },
    applyQuaternion: function(n) {
        var f = this.x,
            e = this.y,
            o = this.z,
            t = n.x,
            i = n.y,
            r = n.z,
            u = n.w,
            s = u * f + i * o - r * e,
            h = u * e + r * f - t * o,
            c = u * o + t * e - i * f,
            l = -t * f - i * e - r * o;
        return this.x = s * u + l * -t + h * -r - c * -i, this.y = h * u + l * -i + c * -t - s * -r, this.z = c * u + l * -r + s * -i - h * -t, this
    },
    project: function() {
        var n;
        return function(t) {
            return n === undefined && (n = new THREE.Matrix4), n.multiplyMatrices(t.projectionMatrix, n.getInverse(t.matrixWorld)), this.applyProjection(n)
        }
    }(),
    unproject: function() {
        var n;
        return function(t) {
            return n === undefined && (n = new THREE.Matrix4), n.multiplyMatrices(t.matrixWorld, n.getInverse(t.projectionMatrix)), this.applyProjection(n)
        }
    }(),
    transformDirection: function(n) {
        var i = this.x,
            r = this.y,
            u = this.z,
            t = n.elements;
        return this.x = t[0] * i + t[4] * r + t[8] * u, this.y = t[1] * i + t[5] * r + t[9] * u, this.z = t[2] * i + t[6] * r + t[10] * u, this.normalize(), this
    },
    divide: function(n) {
        return this.x /= n.x, this.y /= n.y, this.z /= n.z, this
    },
    divideScalar: function(n) {
        return this.multiplyScalar(1 / n)
    },
    min: function(n) {
        return this.x = Math.min(this.x, n.x), this.y = Math.min(this.y, n.y), this.z = Math.min(this.z, n.z), this
    },
    max: function(n) {
        return this.x = Math.max(this.x, n.x), this.y = Math.max(this.y, n.y), this.z = Math.max(this.z, n.z), this
    },
    clamp: function(n, t) {
        return this.x = Math.max(n.x, Math.min(t.x, this.x)), this.y = Math.max(n.y, Math.min(t.y, this.y)), this.z = Math.max(n.z, Math.min(t.z, this.z)), this
    },
    clampScalar: function() {
        var n, t;
        return function(i, r) {
            return n === undefined && (n = new THREE.Vector3, t = new THREE.Vector3), n.set(i, i, i), t.set(r, r, r), this.clamp(n, t)
        }
    }(),
    clampLength: function(n, t) {
        var i = this.length();
        return this.multiplyScalar(Math.max(n, Math.min(t, i)) / i), this
    },
    floor: function() {
        return this.x = Math.floor(this.x), this.y = Math.floor(this.y), this.z = Math.floor(this.z), this
    },
    ceil: function() {
        return this.x = Math.ceil(this.x), this.y = Math.ceil(this.y), this.z = Math.ceil(this.z), this
    },
    round: function() {
        return this.x = Math.round(this.x), this.y = Math.round(this.y), this.z = Math.round(this.z), this
    },
    roundToZero: function() {
        return this.x = this.x < 0 ? Math.ceil(this.x) : Math.floor(this.x), this.y = this.y < 0 ? Math.ceil(this.y) : Math.floor(this.y), this.z = this.z < 0 ? Math.ceil(this.z) : Math.floor(this.z), this
    },
    negate: function() {
        return this.x = -this.x, this.y = -this.y, this.z = -this.z, this
    },
    dot: function(n) {
        return this.x * n.x + this.y * n.y + this.z * n.z
    },
    lengthSq: function() {
        return this.x * this.x + this.y * this.y + this.z * this.z
    },
    length: function() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z)
    },
    lengthManhattan: function() {
        return Math.abs(this.x) + Math.abs(this.y) + Math.abs(this.z)
    },
    normalize: function() {
        return this.divideScalar(this.length())
    },
    setLength: function(n) {
        return this.multiplyScalar(n / this.length())
    },
    lerp: function(n, t) {
        return this.x += (n.x - this.x) * t, this.y += (n.y - this.y) * t, this.z += (n.z - this.z) * t, this
    },
    lerpVectors: function(n, t, i) {
        return this.subVectors(t, n).multiplyScalar(i).add(n), this
    },
    cross: function(n, t) {
        if (t !== undefined) return console.warn("THREE.Vector3: .cross() now only accepts one argument. Use .crossVectors( a, b ) instead."), this.crossVectors(n, t);
        var i = this.x,
            r = this.y,
            u = this.z;
        return this.x = r * n.z - u * n.y, this.y = u * n.x - i * n.z, this.z = i * n.y - r * n.x, this
    },
    crossVectors: function(n, t) {
        var i = n.x,
            r = n.y,
            u = n.z,
            f = t.x,
            e = t.y,
            o = t.z;
        return this.x = r * o - u * e, this.y = u * f - i * o, this.z = i * e - r * f, this
    },
    projectOnVector: function() {
        var n, t;
        return function(i) {
            return n === undefined && (n = new THREE.Vector3), n.copy(i).normalize(), t = this.dot(n), this.copy(n).multiplyScalar(t)
        }
    }(),
    projectOnPlane: function() {
        var n;
        return function(t) {
            return n === undefined && (n = new THREE.Vector3), n.copy(this).projectOnVector(t), this.sub(n)
        }
    }(),
    reflect: function() {
        var n;
        return function(t) {
            return n === undefined && (n = new THREE.Vector3), this.sub(n.copy(t).multiplyScalar(2 * this.dot(t)))
        }
    }(),
    angleTo: function(n) {
        var t = this.dot(n) / Math.sqrt(this.lengthSq() * n.lengthSq());
        return Math.acos(THREE.Math.clamp(t, -1, 1))
    },
    distanceTo: function(n) {
        return Math.sqrt(this.distanceToSquared(n))
    },
    distanceToSquared: function(n) {
        var t = this.x - n.x,
            i = this.y - n.y,
            r = this.z - n.z;
        return t * t + i * i + r * r
    },
    setFromSpherical: function(n) {
        var t = Math.sin(n.phi) * n.radius;
        return this.x = t * Math.sin(n.theta), this.y = Math.cos(n.phi) * n.radius, this.z = t * Math.cos(n.theta), this
    },
    setFromMatrixPosition: function(n) {
        return this.setFromMatrixColumn(n, 3)
    },
    setFromMatrixScale: function(n) {
        var t = this.setFromMatrixColumn(n, 0).length(),
            i = this.setFromMatrixColumn(n, 1).length(),
            r = this.setFromMatrixColumn(n, 2).length();
        return this.x = t, this.y = i, this.z = r, this
    },
    setFromMatrixColumn: function(n, t) {
        return typeof n == "number" && (console.warn("THREE.Vector3: setFromMatrixColumn now expects ( matrix, index )."), n = arguments[1], t = arguments[0]), this.fromArray(n.elements, t * 4)
    },
    equals: function(n) {
        return n.x === this.x && n.y === this.y && n.z === this.z
    },
    fromArray: function(n, t) {
        return t === undefined && (t = 0), this.x = n[t], this.y = n[t + 1], this.z = n[t + 2], this
    },
    toArray: function(n, t) {
        return n === undefined && (n = []), t === undefined && (t = 0), n[t] = this.x, n[t + 1] = this.y, n[t + 2] = this.z, n
    },
    fromAttribute: function(n, t, i) {
        return i === undefined && (i = 0), t = t * n.itemSize + i, this.x = n.array[t], this.y = n.array[t + 1], this.z = n.array[t + 2], this
    }
};
THREE.Vector4 = function(n, t, i, r) {
    this.x = n || 0;
    this.y = t || 0;
    this.z = i || 0;
    this.w = r !== undefined ? r : 1
};
THREE.Vector4.prototype = {
    constructor: THREE.Vector4,
    set: function(n, t, i, r) {
        return this.x = n, this.y = t, this.z = i, this.w = r, this
    },
    setScalar: function(n) {
        return this.x = n, this.y = n, this.z = n, this.w = n, this
    },
    setX: function(n) {
        return this.x = n, this
    },
    setY: function(n) {
        return this.y = n, this
    },
    setZ: function(n) {
        return this.z = n, this
    },
    setW: function(n) {
        return this.w = n, this
    },
    setComponent: function(n, t) {
        switch (n) {
            case 0:
                this.x = t;
                break;
            case 1:
                this.y = t;
                break;
            case 2:
                this.z = t;
                break;
            case 3:
                this.w = t;
                break;
            default:
                throw new Error("index is out of range: " + n);
        }
    },
    getComponent: function(n) {
        switch (n) {
            case 0:
                return this.x;
            case 1:
                return this.y;
            case 2:
                return this.z;
            case 3:
                return this.w;
            default:
                throw new Error("index is out of range: " + n);
        }
    },
    clone: function() {
        return new this.constructor(this.x, this.y, this.z, this.w)
    },
    copy: function(n) {
        return this.x = n.x, this.y = n.y, this.z = n.z, this.w = n.w !== undefined ? n.w : 1, this
    },
    add: function(n, t) {
        return t !== undefined ? (console.warn("THREE.Vector4: .add() now only accepts one argument. Use .addVectors( a, b ) instead."), this.addVectors(n, t)) : (this.x += n.x, this.y += n.y, this.z += n.z, this.w += n.w, this)
    },
    addScalar: function(n) {
        return this.x += n, this.y += n, this.z += n, this.w += n, this
    },
    addVectors: function(n, t) {
        return this.x = n.x + t.x, this.y = n.y + t.y, this.z = n.z + t.z, this.w = n.w + t.w, this
    },
    addScaledVector: function(n, t) {
        return this.x += n.x * t, this.y += n.y * t, this.z += n.z * t, this.w += n.w * t, this
    },
    sub: function(n, t) {
        return t !== undefined ? (console.warn("THREE.Vector4: .sub() now only accepts one argument. Use .subVectors( a, b ) instead."), this.subVectors(n, t)) : (this.x -= n.x, this.y -= n.y, this.z -= n.z, this.w -= n.w, this)
    },
    subScalar: function(n) {
        return this.x -= n, this.y -= n, this.z -= n, this.w -= n, this
    },
    subVectors: function(n, t) {
        return this.x = n.x - t.x, this.y = n.y - t.y, this.z = n.z - t.z, this.w = n.w - t.w, this
    },
    multiplyScalar: function(n) {
        return isFinite(n) ? (this.x *= n, this.y *= n, this.z *= n, this.w *= n) : (this.x = 0, this.y = 0, this.z = 0, this.w = 0), this
    },
    applyMatrix4: function(n) {
        var i = this.x,
            r = this.y,
            u = this.z,
            f = this.w,
            t = n.elements;
        return this.x = t[0] * i + t[4] * r + t[8] * u + t[12] * f, this.y = t[1] * i + t[5] * r + t[9] * u + t[13] * f, this.z = t[2] * i + t[6] * r + t[10] * u + t[14] * f, this.w = t[3] * i + t[7] * r + t[11] * u + t[15] * f, this
    },
    divideScalar: function(n) {
        return this.multiplyScalar(1 / n)
    },
    setAxisAngleFromQuaternion: function(n) {
        this.w = 2 * Math.acos(n.w);
        var t = Math.sqrt(1 - n.w * n.w);
        return t < .0001 ? (this.x = 1, this.y = 0, this.z = 0) : (this.x = n.x / t, this.y = n.y / t, this.z = n.z / t), this
    },
    setAxisAngleFromRotationMatrix: function(n) {
        var g, t, i, r, f = .01,
            v = .1,
            u = n.elements,
            b = u[0],
            e = u[4],
            o = u[8],
            s = u[1],
            k = u[5],
            h = u[9],
            c = u[2],
            l = u[6],
            d = u[10],
            a;
        if (Math.abs(e - s) < f && Math.abs(o - c) < f && Math.abs(h - l) < f) {
            if (Math.abs(e + s) < v && Math.abs(o + c) < v && Math.abs(h + l) < v && Math.abs(b + k + d - 3) < v) return this.set(1, 0, 0, 0), this;
            g = Math.PI;
            var y = (b + 1) / 2,
                p = (k + 1) / 2,
                w = (d + 1) / 2,
                nt = (e + s) / 4,
                tt = (o + c) / 4,
                it = (h + l) / 4;
            return y > p && y > w ? y < f ? (t = 0, i = .707106781, r = .707106781) : (t = Math.sqrt(y), i = nt / t, r = tt / t) : p > w ? p < f ? (t = .707106781, i = 0, r = .707106781) : (i = Math.sqrt(p), t = nt / i, r = it / i) : w < f ? (t = .707106781, i = .707106781, r = 0) : (r = Math.sqrt(w), t = tt / r, i = it / r), this.set(t, i, r, g), this
        }
        return a = Math.sqrt((l - h) * (l - h) + (o - c) * (o - c) + (s - e) * (s - e)), Math.abs(a) < .001 && (a = 1), this.x = (l - h) / a, this.y = (o - c) / a, this.z = (s - e) / a, this.w = Math.acos((b + k + d - 1) / 2), this
    },
    min: function(n) {
        return this.x = Math.min(this.x, n.x), this.y = Math.min(this.y, n.y), this.z = Math.min(this.z, n.z), this.w = Math.min(this.w, n.w), this
    },
    max: function(n) {
        return this.x = Math.max(this.x, n.x), this.y = Math.max(this.y, n.y), this.z = Math.max(this.z, n.z), this.w = Math.max(this.w, n.w), this
    },
    clamp: function(n, t) {
        return this.x = Math.max(n.x, Math.min(t.x, this.x)), this.y = Math.max(n.y, Math.min(t.y, this.y)), this.z = Math.max(n.z, Math.min(t.z, this.z)), this.w = Math.max(n.w, Math.min(t.w, this.w)), this
    },
    clampScalar: function() {
        var n, t;
        return function(i, r) {
            return n === undefined && (n = new THREE.Vector4, t = new THREE.Vector4), n.set(i, i, i, i), t.set(r, r, r, r), this.clamp(n, t)
        }
    }(),
    floor: function() {
        return this.x = Math.floor(this.x), this.y = Math.floor(this.y), this.z = Math.floor(this.z), this.w = Math.floor(this.w), this
    },
    ceil: function() {
        return this.x = Math.ceil(this.x), this.y = Math.ceil(this.y), this.z = Math.ceil(this.z), this.w = Math.ceil(this.w), this
    },
    round: function() {
        return this.x = Math.round(this.x), this.y = Math.round(this.y), this.z = Math.round(this.z), this.w = Math.round(this.w), this
    },
    roundToZero: function() {
        return this.x = this.x < 0 ? Math.ceil(this.x) : Math.floor(this.x), this.y = this.y < 0 ? Math.ceil(this.y) : Math.floor(this.y), this.z = this.z < 0 ? Math.ceil(this.z) : Math.floor(this.z), this.w = this.w < 0 ? Math.ceil(this.w) : Math.floor(this.w), this
    },
    negate: function() {
        return this.x = -this.x, this.y = -this.y, this.z = -this.z, this.w = -this.w, this
    },
    dot: function(n) {
        return this.x * n.x + this.y * n.y + this.z * n.z + this.w * n.w
    },
    lengthSq: function() {
        return this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w
    },
    length: function() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w)
    },
    lengthManhattan: function() {
        return Math.abs(this.x) + Math.abs(this.y) + Math.abs(this.z) + Math.abs(this.w)
    },
    normalize: function() {
        return this.divideScalar(this.length())
    },
    setLength: function(n) {
        return this.multiplyScalar(n / this.length())
    },
    lerp: function(n, t) {
        return this.x += (n.x - this.x) * t, this.y += (n.y - this.y) * t, this.z += (n.z - this.z) * t, this.w += (n.w - this.w) * t, this
    },
    lerpVectors: function(n, t, i) {
        return this.subVectors(t, n).multiplyScalar(i).add(n), this
    },
    equals: function(n) {
        return n.x === this.x && n.y === this.y && n.z === this.z && n.w === this.w
    },
    fromArray: function(n, t) {
        return t === undefined && (t = 0), this.x = n[t], this.y = n[t + 1], this.z = n[t + 2], this.w = n[t + 3], this
    },
    toArray: function(n, t) {
        return n === undefined && (n = []), t === undefined && (t = 0), n[t] = this.x, n[t + 1] = this.y, n[t + 2] = this.z, n[t + 3] = this.w, n
    },
    fromAttribute: function(n, t, i) {
        return i === undefined && (i = 0), t = t * n.itemSize + i, this.x = n.array[t], this.y = n.array[t + 1], this.z = n.array[t + 2], this.w = n.array[t + 3], this
    }
};
THREE.Euler = function(n, t, i, r) {
    this._x = n || 0;
    this._y = t || 0;
    this._z = i || 0;
    this._order = r || THREE.Euler.DefaultOrder
};
THREE.Euler.RotationOrders = ["XYZ", "YZX", "ZXY", "XZY", "YXZ", "ZYX"];
THREE.Euler.DefaultOrder = "XYZ";
THREE.Euler.prototype = {
    constructor: THREE.Euler,
    get x() {
        return this._x
    },
    set x(n) {
        this._x = n;
        this.onChangeCallback()
    },
    get y() {
        return this._y
    },
    set y(n) {
        this._y = n;
        this.onChangeCallback()
    },
    get z() {
        return this._z
    },
    set z(n) {
        this._z = n;
        this.onChangeCallback()
    },
    get order() {
        return this._order
    },
    set order(n) {
        this._order = n;
        this.onChangeCallback()
    },
    set: function(n, t, i, r) {
        return this._x = n, this._y = t, this._z = i, this._order = r || this._order, this.onChangeCallback(), this
    },
    clone: function() {
        return new this.constructor(this._x, this._y, this._z, this._order)
    },
    copy: function(n) {
        return this._x = n._x, this._y = n._y, this._z = n._z, this._order = n._order, this.onChangeCallback(), this
    },
    setFromRotationMatrix: function(n, t, i) {
        var u = THREE.Math.clamp,
            r = n.elements,
            f = r[0],
            s = r[4],
            h = r[8],
            c = r[1],
            e = r[5],
            l = r[9],
            a = r[2],
            v = r[6],
            o = r[10];
        return t = t || this._order, t === "XYZ" ? (this._y = Math.asin(u(h, -1, 1)), Math.abs(h) < .99999 ? (this._x = Math.atan2(-l, o), this._z = Math.atan2(-s, f)) : (this._x = Math.atan2(v, e), this._z = 0)) : t === "YXZ" ? (this._x = Math.asin(-u(l, -1, 1)), Math.abs(l) < .99999 ? (this._y = Math.atan2(h, o), this._z = Math.atan2(c, e)) : (this._y = Math.atan2(-a, f), this._z = 0)) : t === "ZXY" ? (this._x = Math.asin(u(v, -1, 1)), Math.abs(v) < .99999 ? (this._y = Math.atan2(-a, o), this._z = Math.atan2(-s, e)) : (this._y = 0, this._z = Math.atan2(c, f))) : t === "ZYX" ? (this._y = Math.asin(-u(a, -1, 1)), Math.abs(a) < .99999 ? (this._x = Math.atan2(v, o), this._z = Math.atan2(c, f)) : (this._x = 0, this._z = Math.atan2(-s, e))) : t === "YZX" ? (this._z = Math.asin(u(c, -1, 1)), Math.abs(c) < .99999 ? (this._x = Math.atan2(-l, e), this._y = Math.atan2(-a, f)) : (this._x = 0, this._y = Math.atan2(h, o))) : t === "XZY" ? (this._z = Math.asin(-u(s, -1, 1)), Math.abs(s) < .99999 ? (this._x = Math.atan2(v, e), this._y = Math.atan2(h, f)) : (this._x = Math.atan2(-l, o), this._y = 0)) : console.warn("THREE.Euler: .setFromRotationMatrix() given unsupported order: " + t), this._order = t, i !== !1 && this.onChangeCallback(), this
    },
    setFromQuaternion: function() {
        var n;
        return function(t, i, r) {
            return n === undefined && (n = new THREE.Matrix4), n.makeRotationFromQuaternion(t), this.setFromRotationMatrix(n, i, r), this
        }
    }(),
    setFromVector3: function(n, t) {
        return this.set(n.x, n.y, n.z, t || this._order)
    },
    reorder: function() {
        var n = new THREE.Quaternion;
        return function(t) {
            n.setFromEuler(this);
            this.setFromQuaternion(n, t)
        }
    }(),
    equals: function(n) {
        return n._x === this._x && n._y === this._y && n._z === this._z && n._order === this._order
    },
    fromArray: function(n) {
        return this._x = n[0], this._y = n[1], this._z = n[2], n[3] !== undefined && (this._order = n[3]), this.onChangeCallback(), this
    },
    toArray: function(n, t) {
        return n === undefined && (n = []), t === undefined && (t = 0), n[t] = this._x, n[t + 1] = this._y, n[t + 2] = this._z, n[t + 3] = this._order, n
    },
    toVector3: function(n) {
        return n ? n.set(this._x, this._y, this._z) : new THREE.Vector3(this._x, this._y, this._z)
    },
    onChange: function(n) {
        return this.onChangeCallback = n, this
    },
    onChangeCallback: function() {}
};
THREE.Line3 = function(n, t) {
    this.start = n !== undefined ? n : new THREE.Vector3;
    this.end = t !== undefined ? t : new THREE.Vector3
};
THREE.Line3.prototype = {
    constructor: THREE.Line3,
    set: function(n, t) {
        return this.start.copy(n), this.end.copy(t), this
    },
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        return this.start.copy(n.start), this.end.copy(n.end), this
    },
    center: function(n) {
        var t = n || new THREE.Vector3;
        return t.addVectors(this.start, this.end).multiplyScalar(.5)
    },
    delta: function(n) {
        var t = n || new THREE.Vector3;
        return t.subVectors(this.end, this.start)
    },
    distanceSq: function() {
        return this.start.distanceToSquared(this.end)
    },
    distance: function() {
        return this.start.distanceTo(this.end)
    },
    at: function(n, t) {
        var i = t || new THREE.Vector3;
        return this.delta(i).multiplyScalar(n).add(this.start)
    },
    closestPointToPointParameter: function() {
        var t = new THREE.Vector3,
            n = new THREE.Vector3;
        return function(i, r) {
            t.subVectors(i, this.start);
            n.subVectors(this.end, this.start);
            var f = n.dot(n),
                e = n.dot(t),
                u = e / f;
            return r && (u = THREE.Math.clamp(u, 0, 1)), u
        }
    }(),
    closestPointToPoint: function(n, t, i) {
        var r = this.closestPointToPointParameter(n, t),
            u = i || new THREE.Vector3;
        return this.delta(u).multiplyScalar(r).add(this.start)
    },
    applyMatrix4: function(n) {
        return this.start.applyMatrix4(n), this.end.applyMatrix4(n), this
    },
    equals: function(n) {
        return n.start.equals(this.start) && n.end.equals(this.end)
    }
};
THREE.Box2 = function(n, t) {
    this.min = n !== undefined ? n : new THREE.Vector2(+Infinity, +Infinity);
    this.max = t !== undefined ? t : new THREE.Vector2(-Infinity, -Infinity)
};
THREE.Box2.prototype = {
    constructor: THREE.Box2,
    set: function(n, t) {
        return this.min.copy(n), this.max.copy(t), this
    },
    setFromPoints: function(n) {
        this.makeEmpty();
        for (var t = 0, i = n.length; t < i; t++) this.expandByPoint(n[t]);
        return this
    },
    setFromCenterAndSize: function() {
        var n = new THREE.Vector2;
        return function(t, i) {
            var r = n.copy(i).multiplyScalar(.5);
            return this.min.copy(t).sub(r), this.max.copy(t).add(r), this
        }
    }(),
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        return this.min.copy(n.min), this.max.copy(n.max), this
    },
    makeEmpty: function() {
        return this.min.x = this.min.y = +Infinity, this.max.x = this.max.y = -Infinity, this
    },
    isEmpty: function() {
        return this.max.x < this.min.x || this.max.y < this.min.y
    },
    center: function(n) {
        var t = n || new THREE.Vector2;
        return t.addVectors(this.min, this.max).multiplyScalar(.5)
    },
    size: function(n) {
        var t = n || new THREE.Vector2;
        return t.subVectors(this.max, this.min)
    },
    expandByPoint: function(n) {
        return this.min.min(n), this.max.max(n), this
    },
    expandByVector: function(n) {
        return this.min.sub(n), this.max.add(n), this
    },
    expandByScalar: function(n) {
        return this.min.addScalar(-n), this.max.addScalar(n), this
    },
    containsPoint: function(n) {
        return n.x < this.min.x || n.x > this.max.x || n.y < this.min.y || n.y > this.max.y ? !1 : !0
    },
    containsBox: function(n) {
        return this.min.x <= n.min.x && n.max.x <= this.max.x && this.min.y <= n.min.y && n.max.y <= this.max.y ? !0 : !1
    },
    getParameter: function(n, t) {
        var i = t || new THREE.Vector2;
        return i.set((n.x - this.min.x) / (this.max.x - this.min.x), (n.y - this.min.y) / (this.max.y - this.min.y))
    },
    intersectsBox: function(n) {
        return n.max.x < this.min.x || n.min.x > this.max.x || n.max.y < this.min.y || n.min.y > this.max.y ? !1 : !0
    },
    clampPoint: function(n, t) {
        var i = t || new THREE.Vector2;
        return i.copy(n).clamp(this.min, this.max)
    },
    distanceToPoint: function() {
        var n = new THREE.Vector2;
        return function(t) {
            var i = n.copy(t).clamp(this.min, this.max);
            return i.sub(t).length()
        }
    }(),
    intersect: function(n) {
        return this.min.max(n.min), this.max.min(n.max), this
    },
    union: function(n) {
        return this.min.min(n.min), this.max.max(n.max), this
    },
    translate: function(n) {
        return this.min.add(n), this.max.add(n), this
    },
    equals: function(n) {
        return n.min.equals(this.min) && n.max.equals(this.max)
    }
};
THREE.Box3 = function(n, t) {
    this.min = n !== undefined ? n : new THREE.Vector3(+Infinity, +Infinity, +Infinity);
    this.max = t !== undefined ? t : new THREE.Vector3(-Infinity, -Infinity, -Infinity)
};
THREE.Box3.prototype = {
    constructor: THREE.Box3,
    set: function(n, t) {
        return this.min.copy(n), this.max.copy(t), this
    },
    setFromArray: function(n) {
        for (var f = +Infinity, e = +Infinity, o = +Infinity, s = -Infinity, h = -Infinity, c = -Infinity, t = 0, l = n.length; t < l; t += 3) {
            var i = n[t],
                r = n[t + 1],
                u = n[t + 2];
            i < f && (f = i);
            r < e && (e = r);
            u < o && (o = u);
            i > s && (s = i);
            r > h && (h = r);
            u > c && (c = u)
        }
        this.min.set(f, e, o);
        this.max.set(s, h, c)
    },
    setFromPoints: function(n) {
        this.makeEmpty();
        for (var t = 0, i = n.length; t < i; t++) this.expandByPoint(n[t]);
        return this
    },
    setFromCenterAndSize: function() {
        var n = new THREE.Vector3;
        return function(t, i) {
            var r = n.copy(i).multiplyScalar(.5);
            return this.min.copy(t).sub(r), this.max.copy(t).add(r), this
        }
    }(),
    setFromObject: function() {
        var n = new THREE.Vector3;
        return function(t) {
            var i = this;
            return t.updateMatrixWorld(!0), this.makeEmpty(), t.traverse(function(t) {
                var u = t.geometry,
                    e, o, r, f;
                if (u !== undefined)
                    if (u instanceof THREE.Geometry)
                        for (e = u.vertices, r = 0, f = e.length; r < f; r++) n.copy(e[r]), n.applyMatrix4(t.matrixWorld), i.expandByPoint(n);
                    else if (u instanceof THREE.BufferGeometry && u.attributes.position !== undefined)
                    for (o = u.attributes.position.array, r = 0, f = o.length; r < f; r += 3) n.fromArray(o, r), n.applyMatrix4(t.matrixWorld), i.expandByPoint(n)
            }), this
        }
    }(),
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        return this.min.copy(n.min), this.max.copy(n.max), this
    },
    makeEmpty: function() {
        return this.min.x = this.min.y = this.min.z = +Infinity, this.max.x = this.max.y = this.max.z = -Infinity, this
    },
    isEmpty: function() {
        return this.max.x < this.min.x || this.max.y < this.min.y || this.max.z < this.min.z
    },
    center: function(n) {
        var t = n || new THREE.Vector3;
        return t.addVectors(this.min, this.max).multiplyScalar(.5)
    },
    size: function(n) {
        var t = n || new THREE.Vector3;
        return t.subVectors(this.max, this.min)
    },
    expandByPoint: function(n) {
        return this.min.min(n), this.max.max(n), this
    },
    expandByVector: function(n) {
        return this.min.sub(n), this.max.add(n), this
    },
    expandByScalar: function(n) {
        return this.min.addScalar(-n), this.max.addScalar(n), this
    },
    containsPoint: function(n) {
        return n.x < this.min.x || n.x > this.max.x || n.y < this.min.y || n.y > this.max.y || n.z < this.min.z || n.z > this.max.z ? !1 : !0
    },
    containsBox: function(n) {
        return this.min.x <= n.min.x && n.max.x <= this.max.x && this.min.y <= n.min.y && n.max.y <= this.max.y && this.min.z <= n.min.z && n.max.z <= this.max.z ? !0 : !1
    },
    getParameter: function(n, t) {
        var i = t || new THREE.Vector3;
        return i.set((n.x - this.min.x) / (this.max.x - this.min.x), (n.y - this.min.y) / (this.max.y - this.min.y), (n.z - this.min.z) / (this.max.z - this.min.z))
    },
    intersectsBox: function(n) {
        return n.max.x < this.min.x || n.min.x > this.max.x || n.max.y < this.min.y || n.min.y > this.max.y || n.max.z < this.min.z || n.min.z > this.max.z ? !1 : !0
    },
    intersectsSphere: function() {
        var n;
        return function(t) {
            return n === undefined && (n = new THREE.Vector3), this.clampPoint(t.center, n), n.distanceToSquared(t.center) <= t.radius * t.radius
        }
    }(),
    intersectsPlane: function(n) {
        var t, i;
        return n.normal.x > 0 ? (t = n.normal.x * this.min.x, i = n.normal.x * this.max.x) : (t = n.normal.x * this.max.x, i = n.normal.x * this.min.x), n.normal.y > 0 ? (t += n.normal.y * this.min.y, i += n.normal.y * this.max.y) : (t += n.normal.y * this.max.y, i += n.normal.y * this.min.y), n.normal.z > 0 ? (t += n.normal.z * this.min.z, i += n.normal.z * this.max.z) : (t += n.normal.z * this.max.z, i += n.normal.z * this.min.z), t <= n.constant && i >= n.constant
    },
    clampPoint: function(n, t) {
        var i = t || new THREE.Vector3;
        return i.copy(n).clamp(this.min, this.max)
    },
    distanceToPoint: function() {
        var n = new THREE.Vector3;
        return function(t) {
            var i = n.copy(t).clamp(this.min, this.max);
            return i.sub(t).length()
        }
    }(),
    getBoundingSphere: function() {
        var n = new THREE.Vector3;
        return function(t) {
            var i = t || new THREE.Sphere;
            return i.center = this.center(), i.radius = this.size(n).length() * .5, i
        }
    }(),
    intersect: function(n) {
        return this.min.max(n.min), this.max.min(n.max), this.isEmpty() && this.makeEmpty(), this
    },
    union: function(n) {
        return this.min.min(n.min), this.max.max(n.max), this
    },
    applyMatrix4: function() {
        var n = [new THREE.Vector3, new THREE.Vector3, new THREE.Vector3, new THREE.Vector3, new THREE.Vector3, new THREE.Vector3, new THREE.Vector3, new THREE.Vector3];
        return function(t) {
            return this.isEmpty() ? this : (n[0].set(this.min.x, this.min.y, this.min.z).applyMatrix4(t), n[1].set(this.min.x, this.min.y, this.max.z).applyMatrix4(t), n[2].set(this.min.x, this.max.y, this.min.z).applyMatrix4(t), n[3].set(this.min.x, this.max.y, this.max.z).applyMatrix4(t), n[4].set(this.max.x, this.min.y, this.min.z).applyMatrix4(t), n[5].set(this.max.x, this.min.y, this.max.z).applyMatrix4(t), n[6].set(this.max.x, this.max.y, this.min.z).applyMatrix4(t), n[7].set(this.max.x, this.max.y, this.max.z).applyMatrix4(t), this.setFromPoints(n), this)
        }
    }(),
    translate: function(n) {
        return this.min.add(n), this.max.add(n), this
    },
    equals: function(n) {
        return n.min.equals(this.min) && n.max.equals(this.max)
    }
};
THREE.Matrix3 = function() {
    this.elements = new Float32Array([1, 0, 0, 0, 1, 0, 0, 0, 1]);
    arguments.length > 0 && console.error("THREE.Matrix3: the constructor no longer reads arguments. use .set() instead.")
};
THREE.Matrix3.prototype = {
    constructor: THREE.Matrix3,
    set: function(n, t, i, r, u, f, e, o, s) {
        var h = this.elements;
        return h[0] = n, h[1] = r, h[2] = e, h[3] = t, h[4] = u, h[5] = o, h[6] = i, h[7] = f, h[8] = s, this
    },
    identity: function() {
        return this.set(1, 0, 0, 0, 1, 0, 0, 0, 1), this
    },
    clone: function() {
        return (new this.constructor).fromArray(this.elements)
    },
    copy: function(n) {
        var t = n.elements;
        return this.set(t[0], t[3], t[6], t[1], t[4], t[7], t[2], t[5], t[8]), this
    },
    setFromMatrix4: function(n) {
        var t = n.elements;
        return this.set(t[0], t[4], t[8], t[1], t[5], t[9], t[2], t[6], t[10]), this
    },
    applyToVector3Array: function() {
        var n;
        return function(t, i, r) {
            n === undefined && (n = new THREE.Vector3);
            i === undefined && (i = 0);
            r === undefined && (r = t.length);
            for (var f = 0, u = i; f < r; f += 3, u += 3) n.fromArray(t, u), n.applyMatrix3(this), n.toArray(t, u);
            return t
        }
    }(),
    applyToBuffer: function() {
        var n;
        return function(t, i, r) {
            n === undefined && (n = new THREE.Vector3);
            i === undefined && (i = 0);
            r === undefined && (r = t.length / t.itemSize);
            for (var f = 0, u = i; f < r; f++, u++) n.x = t.getX(u), n.y = t.getY(u), n.z = t.getZ(u), n.applyMatrix3(this), t.setXYZ(n.x, n.y, n.z);
            return t
        }
    }(),
    multiplyScalar: function(n) {
        var t = this.elements;
        return t[0] *= n, t[3] *= n, t[6] *= n, t[1] *= n, t[4] *= n, t[7] *= n, t[2] *= n, t[5] *= n, t[8] *= n, this
    },
    determinant: function() {
        var n = this.elements,
            t = n[0],
            i = n[1],
            r = n[2],
            u = n[3],
            f = n[4],
            e = n[5],
            o = n[6],
            s = n[7],
            h = n[8];
        return t * f * h - t * e * s - i * u * h + i * e * o + r * u * s - r * f * o
    },
    getInverse: function(n, t) {
        var v;
        n instanceof THREE.Matrix4 && console.error("THREE.Matrix3.getInverse no longer takes a Matrix4 argument.");
        var i = n.elements,
            r = this.elements,
            u = i[0],
            f = i[1],
            e = i[2],
            o = i[3],
            s = i[4],
            h = i[5],
            c = i[6],
            l = i[7],
            a = i[8],
            y = a * s - h * l,
            p = h * c - a * o,
            w = l * o - s * c,
            b = u * y + f * p + e * w;
        if (b === 0) {
            if (v = "THREE.Matrix3.getInverse(): can't invert matrix, determinant is 0", t || !1) throw new Error(v);
            else console.warn(v);
            return this.identity()
        }
        return r[0] = y, r[1] = e * l - a * f, r[2] = h * f - e * s, r[3] = p, r[4] = a * u - e * c, r[5] = e * o - h * u, r[6] = w, r[7] = f * c - l * u, r[8] = s * u - f * o, this.multiplyScalar(1 / b)
    },
    transpose: function() {
        var t, n = this.elements;
        return t = n[1], n[1] = n[3], n[3] = t, t = n[2], n[2] = n[6], n[6] = t, t = n[5], n[5] = n[7], n[7] = t, this
    },
    flattenToArrayOffset: function(n, t) {
        return console.warn("THREE.Matrix3: .flattenToArrayOffset is deprecated - just use .toArray instead."), this.toArray(n, t)
    },
    getNormalMatrix: function(n) {
        return this.setFromMatrix4(n).getInverse(this).transpose()
    },
    transposeIntoArray: function(n) {
        var t = this.elements;
        return n[0] = t[0], n[1] = t[3], n[2] = t[6], n[3] = t[1], n[4] = t[4], n[5] = t[7], n[6] = t[2], n[7] = t[5], n[8] = t[8], this
    },
    fromArray: function(n) {
        return this.elements.set(n), this
    },
    toArray: function(n, t) {
        n === undefined && (n = []);
        t === undefined && (t = 0);
        var i = this.elements;
        return n[t] = i[0], n[t + 1] = i[1], n[t + 2] = i[2], n[t + 3] = i[3], n[t + 4] = i[4], n[t + 5] = i[5], n[t + 6] = i[6], n[t + 7] = i[7], n[t + 8] = i[8], n
    }
};
THREE.Matrix4 = function() {
    this.elements = new Float32Array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);
    arguments.length > 0 && console.error("THREE.Matrix4: the constructor no longer reads arguments. use .set() instead.")
};
THREE.Matrix4.prototype = {
    constructor: THREE.Matrix4,
    set: function(n, t, i, r, u, f, e, o, s, h, c, l, a, v, y, p) {
        var w = this.elements;
        return w[0] = n, w[4] = t, w[8] = i, w[12] = r, w[1] = u, w[5] = f, w[9] = e, w[13] = o, w[2] = s, w[6] = h, w[10] = c, w[14] = l, w[3] = a, w[7] = v, w[11] = y, w[15] = p, this
    },
    identity: function() {
        return this.set(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1), this
    },
    clone: function() {
        return (new THREE.Matrix4).fromArray(this.elements)
    },
    copy: function(n) {
        return this.elements.set(n.elements), this
    },
    copyPosition: function(n) {
        var t = this.elements,
            i = n.elements;
        return t[12] = i[12], t[13] = i[13], t[14] = i[14], this
    },
    extractBasis: function(n, t, i) {
        return n.setFromMatrixColumn(this, 0), t.setFromMatrixColumn(this, 1), i.setFromMatrixColumn(this, 2), this
    },
    makeBasis: function(n, t, i) {
        return this.set(n.x, t.x, i.x, 0, n.y, t.y, i.y, 0, n.z, t.z, i.z, 0, 0, 0, 0, 1), this
    },
    extractRotation: function() {
        var n;
        return function(t) {
            n === undefined && (n = new THREE.Vector3);
            var i = this.elements,
                r = t.elements,
                u = 1 / n.setFromMatrixColumn(t, 0).length(),
                f = 1 / n.setFromMatrixColumn(t, 1).length(),
                e = 1 / n.setFromMatrixColumn(t, 2).length();
            return i[0] = r[0] * u, i[1] = r[1] * u, i[2] = r[2] * u, i[4] = r[4] * f, i[5] = r[5] * f, i[6] = r[6] * f, i[8] = r[8] * e, i[9] = r[9] * e, i[10] = r[10] * e, this
        }
    }(),
    makeRotationFromEuler: function(n) {
        n instanceof THREE.Euler == !1 && console.error("THREE.Matrix: .makeRotationFromEuler() now expects a Euler rotation rather than a Vector3 and order.");
        var t = this.elements,
            g = n.x,
            nt = n.y,
            tt = n.z,
            f = Math.cos(g),
            i = Math.sin(g),
            e = Math.cos(nt),
            r = Math.sin(nt),
            o = Math.cos(tt),
            u = Math.sin(tt);
        if (n.order === "XYZ") {
            var s = f * o,
                h = f * u,
                c = i * o,
                l = i * u;
            t[0] = e * o;
            t[4] = -e * u;
            t[8] = r;
            t[1] = h + c * r;
            t[5] = s - l * r;
            t[9] = -i * e;
            t[2] = l - s * r;
            t[6] = c + h * r;
            t[10] = f * e
        } else if (n.order === "YXZ") {
            var a = e * o,
                v = e * u,
                y = r * o,
                p = r * u;
            t[0] = a + p * i;
            t[4] = y * i - v;
            t[8] = f * r;
            t[1] = f * u;
            t[5] = f * o;
            t[9] = -i;
            t[2] = v * i - y;
            t[6] = p + a * i;
            t[10] = f * e
        } else if (n.order === "ZXY") {
            var a = e * o,
                v = e * u,
                y = r * o,
                p = r * u;
            t[0] = a - p * i;
            t[4] = -f * u;
            t[8] = y + v * i;
            t[1] = v + y * i;
            t[5] = f * o;
            t[9] = p - a * i;
            t[2] = -f * r;
            t[6] = i;
            t[10] = f * e
        } else if (n.order === "ZYX") {
            var s = f * o,
                h = f * u,
                c = i * o,
                l = i * u;
            t[0] = e * o;
            t[4] = c * r - h;
            t[8] = s * r + l;
            t[1] = e * u;
            t[5] = l * r + s;
            t[9] = h * r - c;
            t[2] = -r;
            t[6] = i * e;
            t[10] = f * e
        } else if (n.order === "YZX") {
            var w = f * e,
                b = f * r,
                k = i * e,
                d = i * r;
            t[0] = e * o;
            t[4] = d - w * u;
            t[8] = k * u + b;
            t[1] = u;
            t[5] = f * o;
            t[9] = -i * o;
            t[2] = -r * o;
            t[6] = b * u + k;
            t[10] = w - d * u
        } else if (n.order === "XZY") {
            var w = f * e,
                b = f * r,
                k = i * e,
                d = i * r;
            t[0] = e * o;
            t[4] = -u;
            t[8] = r * o;
            t[1] = w * u + d;
            t[5] = f * o;
            t[9] = b * u - k;
            t[2] = k * u - b;
            t[6] = i * o;
            t[10] = d * u + w
        }
        return t[3] = 0, t[7] = 0, t[11] = 0, t[12] = 0, t[13] = 0, t[14] = 0, t[15] = 1, this
    },
    makeRotationFromQuaternion: function(n) {
        var t = this.elements,
            i = n.x,
            r = n.y,
            f = n.z,
            e = n.w,
            s = i + i,
            o = r + r,
            u = f + f,
            h = i * s,
            c = i * o,
            l = i * u,
            a = r * o,
            v = r * u,
            y = f * u,
            p = e * s,
            w = e * o,
            b = e * u;
        return t[0] = 1 - (a + y), t[4] = c - b, t[8] = l + w, t[1] = c + b, t[5] = 1 - (h + y), t[9] = v - p, t[2] = l - w, t[6] = v + p, t[10] = 1 - (h + a), t[3] = 0, t[7] = 0, t[11] = 0, t[12] = 0, t[13] = 0, t[14] = 0, t[15] = 1, this
    },
    lookAt: function() {
        var t, i, n;
        return function(r, u, f) {
            t === undefined && (t = new THREE.Vector3);
            i === undefined && (i = new THREE.Vector3);
            n === undefined && (n = new THREE.Vector3);
            var e = this.elements;
            return n.subVectors(r, u).normalize(), n.lengthSq() === 0 && (n.z = 1), t.crossVectors(f, n).normalize(), t.lengthSq() === 0 && (n.x += .0001, t.crossVectors(f, n).normalize()), i.crossVectors(n, t), e[0] = t.x, e[4] = i.x, e[8] = n.x, e[1] = t.y, e[5] = i.y, e[9] = n.y, e[2] = t.z, e[6] = i.z, e[10] = n.z, this
        }
    }(),
    multiply: function(n, t) {
        return t !== undefined ? (console.warn("THREE.Matrix4: .multiply() now only accepts one argument. Use .multiplyMatrices( a, b ) instead."), this.multiplyMatrices(n, t)) : this.multiplyMatrices(this, n)
    },
    premultiply: function(n) {
        return this.multiplyMatrices(n, this)
    },
    multiplyMatrices: function(n, t) {
        var i = n.elements,
            r = t.elements,
            u = this.elements,
            f = i[0],
            e = i[4],
            o = i[8],
            s = i[12],
            h = i[1],
            c = i[5],
            l = i[9],
            a = i[13],
            v = i[2],
            y = i[6],
            p = i[10],
            w = i[14],
            b = i[3],
            k = i[7],
            d = i[11],
            g = i[15],
            nt = r[0],
            tt = r[4],
            it = r[8],
            rt = r[12],
            ut = r[1],
            ft = r[5],
            et = r[9],
            ot = r[13],
            st = r[2],
            ht = r[6],
            ct = r[10],
            lt = r[14],
            at = r[3],
            vt = r[7],
            yt = r[11],
            pt = r[15];
        return u[0] = f * nt + e * ut + o * st + s * at, u[4] = f * tt + e * ft + o * ht + s * vt, u[8] = f * it + e * et + o * ct + s * yt, u[12] = f * rt + e * ot + o * lt + s * pt, u[1] = h * nt + c * ut + l * st + a * at, u[5] = h * tt + c * ft + l * ht + a * vt, u[9] = h * it + c * et + l * ct + a * yt, u[13] = h * rt + c * ot + l * lt + a * pt, u[2] = v * nt + y * ut + p * st + w * at, u[6] = v * tt + y * ft + p * ht + w * vt, u[10] = v * it + y * et + p * ct + w * yt, u[14] = v * rt + y * ot + p * lt + w * pt, u[3] = b * nt + k * ut + d * st + g * at, u[7] = b * tt + k * ft + d * ht + g * vt, u[11] = b * it + k * et + d * ct + g * yt, u[15] = b * rt + k * ot + d * lt + g * pt, this
    },
    multiplyToArray: function(n, t, i) {
        var r = this.elements;
        return this.multiplyMatrices(n, t), i[0] = r[0], i[1] = r[1], i[2] = r[2], i[3] = r[3], i[4] = r[4], i[5] = r[5], i[6] = r[6], i[7] = r[7], i[8] = r[8], i[9] = r[9], i[10] = r[10], i[11] = r[11], i[12] = r[12], i[13] = r[13], i[14] = r[14], i[15] = r[15], this
    },
    multiplyScalar: function(n) {
        var t = this.elements;
        return t[0] *= n, t[4] *= n, t[8] *= n, t[12] *= n, t[1] *= n, t[5] *= n, t[9] *= n, t[13] *= n, t[2] *= n, t[6] *= n, t[10] *= n, t[14] *= n, t[3] *= n, t[7] *= n, t[11] *= n, t[15] *= n, this
    },
    applyToVector3Array: function() {
        var n;
        return function(t, i, r) {
            n === undefined && (n = new THREE.Vector3);
            i === undefined && (i = 0);
            r === undefined && (r = t.length);
            for (var f = 0, u = i; f < r; f += 3, u += 3) n.fromArray(t, u), n.applyMatrix4(this), n.toArray(t, u);
            return t
        }
    }(),
    applyToBuffer: function() {
        var n;
        return function(t, i, r) {
            n === undefined && (n = new THREE.Vector3);
            i === undefined && (i = 0);
            r === undefined && (r = t.length / t.itemSize);
            for (var f = 0, u = i; f < r; f++, u++) n.x = t.getX(u), n.y = t.getY(u), n.z = t.getZ(u), n.applyMatrix4(this), t.setXYZ(n.x, n.y, n.z);
            return t
        }
    }(),
    determinant: function() {
        var n = this.elements,
            t = n[0],
            i = n[4],
            r = n[8],
            u = n[12],
            f = n[1],
            e = n[5],
            o = n[9],
            s = n[13],
            h = n[2],
            c = n[6],
            l = n[10],
            a = n[14],
            v = n[3],
            y = n[7],
            p = n[11],
            w = n[15];
        return v * (+u * o * c - r * s * c - u * e * l + i * s * l + r * e * a - i * o * a) + y * (+t * o * a - t * s * l + u * f * l - r * f * a + r * s * h - u * o * h) + p * (+t * s * c - t * e * a - u * f * c + i * f * a + u * e * h - i * s * h) + w * (-r * e * h - t * o * c + t * e * l + r * f * c - i * f * l + i * o * h)
    },
    transpose: function() {
        var n = this.elements,
            t;
        return t = n[1], n[1] = n[4], n[4] = t, t = n[2], n[2] = n[8], n[8] = t, t = n[6], n[6] = n[9], n[9] = t, t = n[3], n[3] = n[12], n[12] = t, t = n[7], n[7] = n[13], n[13] = t, t = n[11], n[11] = n[14], n[14] = t, this
    },
    flattenToArrayOffset: function(n, t) {
        return console.warn("THREE.Matrix3: .flattenToArrayOffset is deprecated - just use .toArray instead."), this.toArray(n, t)
    },
    getPosition: function() {
        var n;
        return function() {
            return n === undefined && (n = new THREE.Vector3), console.warn("THREE.Matrix4: .getPosition() has been removed. Use Vector3.setFromMatrixPosition( matrix ) instead."), n.setFromMatrixColumn(this, 3)
        }
    }(),
    setPosition: function(n) {
        var t = this.elements;
        return t[12] = n.x, t[13] = n.y, t[14] = n.z, this
    },
    getInverse: function(n, t) {
        var k = this.elements,
            d = n.elements,
            i = d[0],
            r = d[1],
            u = d[2],
            f = d[3],
            e = d[4],
            o = d[5],
            s = d[6],
            h = d[7],
            c = d[8],
            l = d[9],
            a = d[10],
            v = d[11],
            y = d[12],
            p = d[13],
            w = d[14],
            b = d[15],
            nt = l * w * h - p * a * h + p * s * v - o * w * v - l * s * b + o * a * b,
            tt = y * a * h - c * w * h - y * s * v + e * w * v + c * s * b - e * a * b,
            it = c * p * h - y * l * h + y * o * v - e * p * v - c * o * b + e * l * b,
            rt = y * l * s - c * p * s - y * o * a + e * p * a + c * o * w - e * l * w,
            ut = i * nt + r * tt + u * it + f * rt,
            g;
        if (ut === 0) {
            if (g = "THREE.Matrix4.getInverse(): can't invert matrix, determinant is 0", t || !1) throw new Error(g);
            else console.warn(g);
            return this.identity()
        }
        return k[0] = nt, k[1] = p * a * f - l * w * f - p * u * v + r * w * v + l * u * b - r * a * b, k[2] = o * w * f - p * s * f + p * u * h - r * w * h - o * u * b + r * s * b, k[3] = l * s * f - o * a * f - l * u * h + r * a * h + o * u * v - r * s * v, k[4] = tt, k[5] = c * w * f - y * a * f + y * u * v - i * w * v - c * u * b + i * a * b, k[6] = y * s * f - e * w * f - y * u * h + i * w * h + e * u * b - i * s * b, k[7] = e * a * f - c * s * f + c * u * h - i * a * h - e * u * v + i * s * v, k[8] = it, k[9] = y * l * f - c * p * f - y * r * v + i * p * v + c * r * b - i * l * b, k[10] = e * p * f - y * o * f + y * r * h - i * p * h - e * r * b + i * o * b, k[11] = c * o * f - e * l * f - c * r * h + i * l * h + e * r * v - i * o * v, k[12] = rt, k[13] = c * p * u - y * l * u + y * r * a - i * p * a - c * r * w + i * l * w, k[14] = y * o * u - e * p * u - y * r * s + i * p * s + e * r * w - i * o * w, k[15] = e * l * u - c * o * u + c * r * s - i * l * s - e * r * a + i * o * a, this.multiplyScalar(1 / ut)
    },
    scale: function(n) {
        var t = this.elements,
            i = n.x,
            r = n.y,
            u = n.z;
        return t[0] *= i, t[4] *= r, t[8] *= u, t[1] *= i, t[5] *= r, t[9] *= u, t[2] *= i, t[6] *= r, t[10] *= u, t[3] *= i, t[7] *= r, t[11] *= u, this
    },
    getMaxScaleOnAxis: function() {
        var n = this.elements,
            t = n[0] * n[0] + n[1] * n[1] + n[2] * n[2],
            i = n[4] * n[4] + n[5] * n[5] + n[6] * n[6],
            r = n[8] * n[8] + n[9] * n[9] + n[10] * n[10];
        return Math.sqrt(Math.max(t, i, r))
    },
    makeTranslation: function(n, t, i) {
        return this.set(1, 0, 0, n, 0, 1, 0, t, 0, 0, 1, i, 0, 0, 0, 1), this
    },
    makeRotationX: function(n) {
        var t = Math.cos(n),
            i = Math.sin(n);
        return this.set(1, 0, 0, 0, 0, t, -i, 0, 0, i, t, 0, 0, 0, 0, 1), this
    },
    makeRotationY: function(n) {
        var t = Math.cos(n),
            i = Math.sin(n);
        return this.set(t, 0, i, 0, 0, 1, 0, 0, -i, 0, t, 0, 0, 0, 0, 1), this
    },
    makeRotationZ: function(n) {
        var t = Math.cos(n),
            i = Math.sin(n);
        return this.set(t, -i, 0, 0, i, t, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1), this
    },
    makeRotationAxis: function(n, t) {
        var e = Math.cos(t),
            r = Math.sin(t),
            s = 1 - e,
            o = n.x,
            u = n.y,
            i = n.z,
            f = s * o,
            h = s * u;
        return this.set(f * o + e, f * u - r * i, f * i + r * u, 0, f * u + r * i, h * u + e, h * i - r * o, 0, f * i - r * u, h * i + r * o, s * i * i + e, 0, 0, 0, 0, 1), this
    },
    makeScale: function(n, t, i) {
        return this.set(n, 0, 0, 0, 0, t, 0, 0, 0, 0, i, 0, 0, 0, 0, 1), this
    },
    compose: function(n, t, i) {
        return this.makeRotationFromQuaternion(t), this.scale(i), this.setPosition(n), this
    },
    decompose: function() {
        var t, n;
        return function(i, r, u) {
            t === undefined && (t = new THREE.Vector3);
            n === undefined && (n = new THREE.Matrix4);
            var f = this.elements,
                e = t.set(f[0], f[1], f[2]).length(),
                c = t.set(f[4], f[5], f[6]).length(),
                l = t.set(f[8], f[9], f[10]).length(),
                a = this.determinant();
            a < 0 && (e = -e);
            i.x = f[12];
            i.y = f[13];
            i.z = f[14];
            n.elements.set(this.elements);
            var o = 1 / e,
                s = 1 / c,
                h = 1 / l;
            return n.elements[0] *= o, n.elements[1] *= o, n.elements[2] *= o, n.elements[4] *= s, n.elements[5] *= s, n.elements[6] *= s, n.elements[8] *= h, n.elements[9] *= h, n.elements[10] *= h, r.setFromRotationMatrix(n), u.x = e, u.y = c, u.z = l, this
        }
    }(),
    makeFrustum: function(n, t, i, r, u, f) {
        var e = this.elements,
            o = 2 * u / (t - n),
            s = 2 * u / (r - i),
            h = (t + n) / (t - n),
            c = (r + i) / (r - i),
            l = -(f + u) / (f - u),
            a = -2 * f * u / (f - u);
        return e[0] = o, e[4] = 0, e[8] = h, e[12] = 0, e[1] = 0, e[5] = s, e[9] = c, e[13] = 0, e[2] = 0, e[6] = 0, e[10] = l, e[14] = a, e[3] = 0, e[7] = 0, e[11] = -1, e[15] = 0, this
    },
    makePerspective: function(n, t, i, r) {
        var u = i * Math.tan(THREE.Math.DEG2RAD * n * .5),
            f = -u,
            e = f * t,
            o = u * t;
        return this.makeFrustum(e, o, f, u, i, r)
    },
    makeOrthographic: function(n, t, i, r, u, f) {
        var e = this.elements,
            o = 1 / (t - n),
            s = 1 / (i - r),
            h = 1 / (f - u),
            c = (t + n) * o,
            l = (i + r) * s,
            a = (f + u) * h;
        return e[0] = 2 * o, e[4] = 0, e[8] = 0, e[12] = -c, e[1] = 0, e[5] = 2 * s, e[9] = 0, e[13] = -l, e[2] = 0, e[6] = 0, e[10] = -2 * h, e[14] = -a, e[3] = 0, e[7] = 0, e[11] = 0, e[15] = 1, this
    },
    equals: function(n) {
        for (var i = this.elements, r = n.elements, t = 0; t < 16; t++)
            if (i[t] !== r[t]) return !1;
        return !0
    },
    fromArray: function(n) {
        return this.elements.set(n), this
    },
    toArray: function(n, t) {
        n === undefined && (n = []);
        t === undefined && (t = 0);
        var i = this.elements;
        return n[t] = i[0], n[t + 1] = i[1], n[t + 2] = i[2], n[t + 3] = i[3], n[t + 4] = i[4], n[t + 5] = i[5], n[t + 6] = i[6], n[t + 7] = i[7], n[t + 8] = i[8], n[t + 9] = i[9], n[t + 10] = i[10], n[t + 11] = i[11], n[t + 12] = i[12], n[t + 13] = i[13], n[t + 14] = i[14], n[t + 15] = i[15], n
    }
};
THREE.Ray = function(n, t) {
    this.origin = n !== undefined ? n : new THREE.Vector3;
    this.direction = t !== undefined ? t : new THREE.Vector3
};
THREE.Ray.prototype = {
    constructor: THREE.Ray,
    set: function(n, t) {
        return this.origin.copy(n), this.direction.copy(t), this
    },
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        return this.origin.copy(n.origin), this.direction.copy(n.direction), this
    },
    at: function(n, t) {
        var i = t || new THREE.Vector3;
        return i.copy(this.direction).multiplyScalar(n).add(this.origin)
    },
    lookAt: function(n) {
        this.direction.copy(n).sub(this.origin).normalize()
    },
    recast: function() {
        var n = new THREE.Vector3;
        return function(t) {
            return this.origin.copy(this.at(t, n)), this
        }
    }(),
    closestPointToPoint: function(n, t) {
        var i = t || new THREE.Vector3,
            r;
        return (i.subVectors(n, this.origin), r = i.dot(this.direction), r < 0) ? i.copy(this.origin) : i.copy(this.direction).multiplyScalar(r).add(this.origin)
    },
    distanceToPoint: function(n) {
        return Math.sqrt(this.distanceSqToPoint(n))
    },
    distanceSqToPoint: function() {
        var n = new THREE.Vector3;
        return function(t) {
            var i = n.subVectors(t, this.origin).dot(this.direction);
            return i < 0 ? this.origin.distanceToSquared(t) : (n.copy(this.direction).multiplyScalar(i).add(this.origin), n.distanceToSquared(t))
        }
    }(),
    distanceSqToSegment: function() {
        var i = new THREE.Vector3,
            n = new THREE.Vector3,
            t = new THREE.Vector3;
        return function(r, u, f, e) {
            var b;
            i.copy(r).add(u).multiplyScalar(.5);
            n.copy(u).sub(r).normalize();
            t.copy(this.origin).sub(i);
            var h = r.distanceTo(u) * .5,
                c = -this.direction.dot(n),
                a = t.dot(this.direction),
                l = -t.dot(n),
                y = t.lengthSq(),
                w = Math.abs(1 - c * c),
                s, o, v, p;
            return w > 0 ? (s = c * l - a, o = c * a - l, p = h * w, s >= 0 ? o >= -p ? o <= p ? (b = 1 / w, s *= b, o *= b, v = s * (s + c * o + 2 * a) + o * (c * s + o + 2 * l) + y) : (o = h, s = Math.max(0, -(c * o + a)), v = -s * s + o * (o + 2 * l) + y) : (o = -h, s = Math.max(0, -(c * o + a)), v = -s * s + o * (o + 2 * l) + y) : o <= -p ? (s = Math.max(0, -(-c * h + a)), o = s > 0 ? -h : Math.min(Math.max(-h, -l), h), v = -s * s + o * (o + 2 * l) + y) : o <= p ? (s = 0, o = Math.min(Math.max(-h, -l), h), v = o * (o + 2 * l) + y) : (s = Math.max(0, -(c * h + a)), o = s > 0 ? h : Math.min(Math.max(-h, -l), h), v = -s * s + o * (o + 2 * l) + y)) : (o = c > 0 ? -h : h, s = Math.max(0, -(c * o + a)), v = -s * s + o * (o + 2 * l) + y), f && f.copy(this.direction).multiplyScalar(s).add(this.origin), e && e.copy(n).multiplyScalar(o).add(i), v
        }
    }(),
    intersectSphere: function() {
        var n = new THREE.Vector3;
        return function(t, i) {
            n.subVectors(t.center, this.origin);
            var r = n.dot(this.direction),
                f = n.dot(n) - r * r,
                e = t.radius * t.radius;
            if (f > e) return null;
            var o = Math.sqrt(e - f),
                u = r - o,
                s = r + o;
            return u < 0 && s < 0 ? null : u < 0 ? this.at(s, i) : this.at(u, i)
        }
    }(),
    intersectsSphere: function(n) {
        return this.distanceToPoint(n.center) <= n.radius
    },
    distanceToPlane: function(n) {
        var i = n.normal.dot(this.direction),
            t;
        return i === 0 ? n.distanceToPoint(this.origin) === 0 ? 0 : null : (t = -(this.origin.dot(n.normal) + n.constant) / i, t >= 0 ? t : null)
    },
    intersectPlane: function(n, t) {
        var i = this.distanceToPlane(n);
        return i === null ? null : this.at(i, t)
    },
    intersectsPlane: function(n) {
        var t = n.distanceToPoint(this.origin),
            i;
        return t === 0 ? !0 : (i = n.normal.dot(this.direction), i * t < 0) ? !0 : !1
    },
    intersectBox: function(n, t) {
        var i, r, f, e, o, s, h = 1 / this.direction.x,
            c = 1 / this.direction.y,
            l = 1 / this.direction.z,
            u = this.origin;
        return (h >= 0 ? (i = (n.min.x - u.x) * h, r = (n.max.x - u.x) * h) : (i = (n.max.x - u.x) * h, r = (n.min.x - u.x) * h), c >= 0 ? (f = (n.min.y - u.y) * c, e = (n.max.y - u.y) * c) : (f = (n.max.y - u.y) * c, e = (n.min.y - u.y) * c), i > e || f > r) ? null : ((f > i || i !== i) && (i = f), (e < r || r !== r) && (r = e), l >= 0 ? (o = (n.min.z - u.z) * l, s = (n.max.z - u.z) * l) : (o = (n.max.z - u.z) * l, s = (n.min.z - u.z) * l), i > s || o > r) ? null : ((o > i || i !== i) && (i = o), (s < r || r !== r) && (r = s), r < 0) ? null : this.at(i >= 0 ? i : r, t)
    },
    intersectsBox: function() {
        var n = new THREE.Vector3;
        return function(t) {
            return this.intersectBox(t, n) !== null
        }
    }(),
    intersectTriangle: function() {
        var n = new THREE.Vector3,
            i = new THREE.Vector3,
            t = new THREE.Vector3,
            r = new THREE.Vector3;
        return function(u, f, e, o, s) {
            var h, c, l, a, v;
            if (i.subVectors(f, u), t.subVectors(e, u), r.crossVectors(i, t), h = this.direction.dot(r), h > 0) {
                if (o) return null;
                c = 1
            } else if (h < 0) c = -1, h = -h;
            else return null;
            return (n.subVectors(this.origin, u), l = c * this.direction.dot(t.crossVectors(n, t)), l < 0) ? null : (a = c * this.direction.dot(i.cross(n)), a < 0) ? null : l + a > h ? null : (v = -c * n.dot(r), v < 0) ? null : this.at(v / h, s)
        }
    }(),
    applyMatrix4: function(n) {
        return this.direction.add(this.origin).applyMatrix4(n), this.origin.applyMatrix4(n), this.direction.sub(this.origin), this.direction.normalize(), this
    },
    equals: function(n) {
        return n.origin.equals(this.origin) && n.direction.equals(this.direction)
    }
};
THREE.Sphere = function(n, t) {
    this.center = n !== undefined ? n : new THREE.Vector3;
    this.radius = t !== undefined ? t : 0
};
THREE.Sphere.prototype = {
    constructor: THREE.Sphere,
    set: function(n, t) {
        return this.center.copy(n), this.radius = t, this
    },
    setFromPoints: function() {
        var n = new THREE.Box3;
        return function(t, i) {
            var f = this.center,
                r, u, e;
            for (i !== undefined ? f.copy(i) : n.setFromPoints(t).center(f), r = 0, u = 0, e = t.length; u < e; u++) r = Math.max(r, f.distanceToSquared(t[u]));
            return this.radius = Math.sqrt(r), this
        }
    }(),
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        return this.center.copy(n.center), this.radius = n.radius, this
    },
    empty: function() {
        return this.radius <= 0
    },
    containsPoint: function(n) {
        return n.distanceToSquared(this.center) <= this.radius * this.radius
    },
    distanceToPoint: function(n) {
        return n.distanceTo(this.center) - this.radius
    },
    intersectsSphere: function(n) {
        var t = this.radius + n.radius;
        return n.center.distanceToSquared(this.center) <= t * t
    },
    intersectsBox: function(n) {
        return n.intersectsSphere(this)
    },
    intersectsPlane: function(n) {
        return Math.abs(this.center.dot(n.normal) - n.constant) <= this.radius
    },
    clampPoint: function(n, t) {
        var r = this.center.distanceToSquared(n),
            i = t || new THREE.Vector3;
        return i.copy(n), r > this.radius * this.radius && (i.sub(this.center).normalize(), i.multiplyScalar(this.radius).add(this.center)), i
    },
    getBoundingBox: function(n) {
        var t = n || new THREE.Box3;
        return t.set(this.center, this.center), t.expandByScalar(this.radius), t
    },
    applyMatrix4: function(n) {
        return this.center.applyMatrix4(n), this.radius = this.radius * n.getMaxScaleOnAxis(), this
    },
    translate: function(n) {
        return this.center.add(n), this
    },
    equals: function(n) {
        return n.center.equals(this.center) && n.radius === this.radius
    }
};
THREE.Frustum = function(n, t, i, r, u, f) {
    this.planes = [n !== undefined ? n : new THREE.Plane, t !== undefined ? t : new THREE.Plane, i !== undefined ? i : new THREE.Plane, r !== undefined ? r : new THREE.Plane, u !== undefined ? u : new THREE.Plane, f !== undefined ? f : new THREE.Plane]
};
THREE.Frustum.prototype = {
    constructor: THREE.Frustum,
    set: function(n, t, i, r, u, f) {
        var e = this.planes;
        return e[0].copy(n), e[1].copy(t), e[2].copy(i), e[3].copy(r), e[4].copy(u), e[5].copy(f), this
    },
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        for (var i = this.planes, t = 0; t < 6; t++) i[t].copy(n.planes[t]);
        return this
    },
    setFromMatrix: function(n) {
        var i = this.planes,
            t = n.elements,
            o = t[0],
            s = t[1],
            h = t[2],
            r = t[3],
            c = t[4],
            l = t[5],
            a = t[6],
            u = t[7],
            v = t[8],
            y = t[9],
            p = t[10],
            f = t[11],
            w = t[12],
            b = t[13],
            k = t[14],
            e = t[15];
        return i[0].setComponents(r - o, u - c, f - v, e - w).normalize(), i[1].setComponents(r + o, u + c, f + v, e + w).normalize(), i[2].setComponents(r + s, u + l, f + y, e + b).normalize(), i[3].setComponents(r - s, u - l, f - y, e - b).normalize(), i[4].setComponents(r - h, u - a, f - p, e - k).normalize(), i[5].setComponents(r + h, u + a, f + p, e + k).normalize(), this
    },
    intersectsObject: function() {
        var n = new THREE.Sphere;
        return function(t) {
            var i = t.geometry;
            return i.boundingSphere === null && i.computeBoundingSphere(), n.copy(i.boundingSphere), n.applyMatrix4(t.matrixWorld), this.intersectsSphere(n)
        }
    }(),
    intersectsSphere: function(n) {
        for (var r = this.planes, u = n.center, f = -n.radius, i, t = 0; t < 6; t++)
            if (i = r[t].distanceToPoint(u), i < f) return !1;
        return !0
    },
    intersectsBox: function() {
        var n = new THREE.Vector3,
            t = new THREE.Vector3;
        return function(i) {
            for (var o = this.planes, r, f, e, u = 0; u < 6; u++)
                if (r = o[u], n.x = r.normal.x > 0 ? i.min.x : i.max.x, t.x = r.normal.x > 0 ? i.max.x : i.min.x, n.y = r.normal.y > 0 ? i.min.y : i.max.y, t.y = r.normal.y > 0 ? i.max.y : i.min.y, n.z = r.normal.z > 0 ? i.min.z : i.max.z, t.z = r.normal.z > 0 ? i.max.z : i.min.z, f = r.distanceToPoint(n), e = r.distanceToPoint(t), f < 0 && e < 0) return !1;
            return !0
        }
    }(),
    containsPoint: function(n) {
        for (var i = this.planes, t = 0; t < 6; t++)
            if (i[t].distanceToPoint(n) < 0) return !1;
        return !0
    }
};
THREE.Plane = function(n, t) {
    this.normal = n !== undefined ? n : new THREE.Vector3(1, 0, 0);
    this.constant = t !== undefined ? t : 0
};
THREE.Plane.prototype = {
    constructor: THREE.Plane,
    set: function(n, t) {
        return this.normal.copy(n), this.constant = t, this
    },
    setComponents: function(n, t, i, r) {
        return this.normal.set(n, t, i), this.constant = r, this
    },
    setFromNormalAndCoplanarPoint: function(n, t) {
        return this.normal.copy(n), this.constant = -t.dot(this.normal), this
    },
    setFromCoplanarPoints: function() {
        var n = new THREE.Vector3,
            t = new THREE.Vector3;
        return function(i, r, u) {
            var f = n.subVectors(u, r).cross(t.subVectors(i, r)).normalize();
            return this.setFromNormalAndCoplanarPoint(f, i), this
        }
    }(),
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        return this.normal.copy(n.normal), this.constant = n.constant, this
    },
    normalize: function() {
        var n = 1 / this.normal.length();
        return this.normal.multiplyScalar(n), this.constant *= n, this
    },
    negate: function() {
        return this.constant *= -1, this.normal.negate(), this
    },
    distanceToPoint: function(n) {
        return this.normal.dot(n) + this.constant
    },
    distanceToSphere: function(n) {
        return this.distanceToPoint(n.center) - n.radius
    },
    projectPoint: function(n, t) {
        return this.orthoPoint(n, t).sub(n).negate()
    },
    orthoPoint: function(n, t) {
        var i = this.distanceToPoint(n),
            r = t || new THREE.Vector3;
        return r.copy(this.normal).multiplyScalar(i)
    },
    intersectLine: function() {
        var n = new THREE.Vector3;
        return function(t, i) {
            var u = i || new THREE.Vector3,
                f = t.delta(n),
                e = this.normal.dot(f),
                r;
            return e === 0 ? this.distanceToPoint(t.start) === 0 ? u.copy(t.start) : undefined : (r = -(t.start.dot(this.normal) + this.constant) / e, r < 0 || r > 1) ? undefined : u.copy(f).multiplyScalar(r).add(t.start)
        }
    }(),
    intersectsLine: function(n) {
        var t = this.distanceToPoint(n.start),
            i = this.distanceToPoint(n.end);
        return t < 0 && i > 0 || i < 0 && t > 0
    },
    intersectsBox: function(n) {
        return n.intersectsPlane(this)
    },
    intersectsSphere: function(n) {
        return n.intersectsPlane(this)
    },
    coplanarPoint: function(n) {
        var t = n || new THREE.Vector3;
        return t.copy(this.normal).multiplyScalar(-this.constant)
    },
    applyMatrix4: function() {
        var n = new THREE.Vector3,
            t = new THREE.Matrix3;
        return function(i, r) {
            var u = this.coplanarPoint(n).applyMatrix4(i),
                f = r || t.getNormalMatrix(i),
                e = this.normal.applyMatrix3(f).normalize();
            return this.constant = -u.dot(e), this
        }
    }(),
    translate: function(n) {
        return this.constant = this.constant - n.dot(this.normal), this
    },
    equals: function(n) {
        return n.normal.equals(this.normal) && n.constant === this.constant
    }
};
THREE.Spherical = function(n, t, i) {
    return this.radius = n !== undefined ? n : 1, this.phi = t !== undefined ? t : 0, this.theta = i !== undefined ? i : 0, this
};
THREE.Spherical.prototype = {
    constructor: THREE.Spherical,
    set: function(n, t, i) {
        this.radius = n;
        this.phi = t;
        this.theta = i
    },
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        return this.radius.copy(n.radius), this.phi.copy(n.phi), this.theta.copy(n.theta), this
    },
    makeSafe: function() {
        var n = 1e-6;
        this.phi = Math.max(n, Math.min(Math.PI - n, this.phi))
    },
    setFromVector3: function(n) {
        return this.radius = n.length(), this.radius === 0 ? (this.theta = 0, this.phi = 0) : (this.theta = Math.atan2(n.x, n.z), this.phi = Math.acos(THREE.Math.clamp(n.y / this.radius, -1, 1))), this
    }
};
THREE.Math = {
    DEG2RAD: Math.PI / 180,
    RAD2DEG: 180 / Math.PI,
    generateUUID: function() {
        var r = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split(""),
            t = new Array(36),
            n = 0,
            i;
        return function() {
            for (var u = 0; u < 36; u++) u === 8 || u === 13 || u === 18 || u === 23 ? t[u] = "-" : u === 14 ? t[u] = "4" : (n <= 2 && (n = 33554432 + Math.random() * 16777216 | 0), i = n & 15, n = n >> 4, t[u] = r[u === 19 ? i & 3 | 8 : i]);
            return t.join("")
        }
    }(),
    clamp: function(n, t, i) {
        return Math.max(t, Math.min(i, n))
    },
    euclideanModulo: function(n, t) {
        return (n % t + t) % t
    },
    mapLinear: function(n, t, i, r, u) {
        return r + (n - t) * (u - r) / (i - t)
    },
    smoothstep: function(n, t, i) {
        return n <= t ? 0 : n >= i ? 1 : (n = (n - t) / (i - t), n * n * (3 - 2 * n))
    },
    smootherstep: function(n, t, i) {
        return n <= t ? 0 : n >= i ? 1 : (n = (n - t) / (i - t), n * n * n * (n * (n * 6 - 15) + 10))
    },
    random16: function() {
        return console.warn("THREE.Math.random16() has been deprecated. Use Math.random() instead."), Math.random()
    },
    randInt: function(n, t) {
        return n + Math.floor(Math.random() * (t - n + 1))
    },
    randFloat: function(n, t) {
        return n + Math.random() * (t - n)
    },
    randFloatSpread: function(n) {
        return n * (.5 - Math.random())
    },
    degToRad: function(n) {
        return n * THREE.Math.DEG2RAD
    },
    radToDeg: function(n) {
        return n * THREE.Math.RAD2DEG
    },
    isPowerOfTwo: function(n) {
        return (n & n - 1) == 0 && n !== 0
    },
    nearestPowerOfTwo: function(n) {
        return Math.pow(2, Math.round(Math.log(n) / Math.LN2))
    },
    nextPowerOfTwo: function(n) {
        return n--, n |= n >> 1, n |= n >> 2, n |= n >> 4, n |= n >> 8, n |= n >> 16, n++, n
    }
};
THREE.Spline = function(n) {
    function a(n, t, i, r, u, f, e) {
        var o = (i - n) * .5,
            s = (r - t) * .5;
        return (2 * (t - i) + o + s) * e + (-3 * (t - i) - 2 * o - s) * f + o * u + t
    }
    this.points = n;
    var i = [],
        f = {
            x: 0,
            y: 0,
            z: 0
        },
        l, t, r, u, e, o, s, h, c;
    this.initFromArray = function(n) {
        this.points = [];
        for (var t = 0; t < n.length; t++) this.points[t] = {
            x: n[t][0],
            y: n[t][1],
            z: n[t][2]
        }
    };
    this.getPoint = function(n) {
        return l = (this.points.length - 1) * n, t = Math.floor(l), r = l - t, i[0] = t === 0 ? t : t - 1, i[1] = t, i[2] = t > this.points.length - 2 ? this.points.length - 1 : t + 1, i[3] = t > this.points.length - 3 ? this.points.length - 1 : t + 2, o = this.points[i[0]], s = this.points[i[1]], h = this.points[i[2]], c = this.points[i[3]], u = r * r, e = r * u, f.x = a(o.x, s.x, h.x, c.x, r, u, e), f.y = a(o.y, s.y, h.y, c.y, r, u, e), f.z = a(o.z, s.z, h.z, c.z, r, u, e), f
    };
    this.getControlPointsArray = function() {
        for (var t, r = this.points.length, i = [], n = 0; n < r; n++) t = this.points[n], i[n] = [t.x, t.y, t.z];
        return i
    };
    this.getLength = function(n) {
        var i, f, e, o, h = 0,
            r = 0,
            c = 0,
            s = new THREE.Vector3,
            l = new THREE.Vector3,
            t = [],
            u = 0;
        for (t[0] = 0, n || (n = 100), e = this.points.length * n, s.copy(this.points[0]), i = 1; i < e; i++) f = i / e, o = this.getPoint(f), l.copy(o), u += l.distanceTo(s), s.copy(o), h = (this.points.length - 1) * f, r = Math.floor(h), r !== c && (t[r] = u, c = r);
        return t[t.length] = u, {
            chunks: t,
            total: u
        }
    };
    this.reparametrizeByArcLength = function(n) {
        var t, i, s, u, h, c, f, l, r = [],
            e = new THREE.Vector3,
            o = this.getLength();
        for (r.push(e.copy(this.points[0]).clone()), t = 1; t < this.points.length; t++) {
            for (c = o.chunks[t] - o.chunks[t - 1], f = Math.ceil(n * c / o.total), u = (t - 1) / (this.points.length - 1), h = t / (this.points.length - 1), i = 1; i < f - 1; i++) s = u + i * (1 / f) * (h - u), l = this.getPoint(s), r.push(e.copy(l).clone());
            r.push(e.copy(this.points[t]).clone())
        }
        this.points = r
    }
};
THREE.Triangle = function(n, t, i) {
    this.a = n !== undefined ? n : new THREE.Vector3;
    this.b = t !== undefined ? t : new THREE.Vector3;
    this.c = i !== undefined ? i : new THREE.Vector3
};
THREE.Triangle.normal = function() {
    var n = new THREE.Vector3;
    return function(t, i, r, u) {
        var f = u || new THREE.Vector3,
            e;
        return (f.subVectors(r, i), n.subVectors(t, i), f.cross(n), e = f.lengthSq(), e > 0) ? f.multiplyScalar(1 / Math.sqrt(e)) : f.set(0, 0, 0)
    }
}();
THREE.Triangle.barycoordFromPoint = function() {
    var n = new THREE.Vector3,
        t = new THREE.Vector3,
        i = new THREE.Vector3;
    return function(r, u, f, e, o) {
        n.subVectors(e, u);
        t.subVectors(f, u);
        i.subVectors(r, u);
        var h = n.dot(n),
            s = n.dot(t),
            c = n.dot(i),
            l = t.dot(t),
            a = t.dot(i),
            v = h * l - s * s,
            y = o || new THREE.Vector3;
        if (v === 0) return y.set(-2, -1, -1);
        var p = 1 / v,
            w = (l * c - s * a) * p,
            b = (h * a - s * c) * p;
        return y.set(1 - w - b, b, w)
    }
}();
THREE.Triangle.containsPoint = function() {
    var n = new THREE.Vector3;
    return function(t, i, r, u) {
        var f = THREE.Triangle.barycoordFromPoint(t, i, r, u, n);
        return f.x >= 0 && f.y >= 0 && f.x + f.y <= 1
    }
}();
THREE.Triangle.prototype = {
    constructor: THREE.Triangle,
    set: function(n, t, i) {
        return this.a.copy(n), this.b.copy(t), this.c.copy(i), this
    },
    setFromPointsAndIndices: function(n, t, i, r) {
        return this.a.copy(n[t]), this.b.copy(n[i]), this.c.copy(n[r]), this
    },
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        return this.a.copy(n.a), this.b.copy(n.b), this.c.copy(n.c), this
    },
    area: function() {
        var n = new THREE.Vector3,
            t = new THREE.Vector3;
        return function() {
            return n.subVectors(this.c, this.b), t.subVectors(this.a, this.b), n.cross(t).length() * .5
        }
    }(),
    midpoint: function(n) {
        var t = n || new THREE.Vector3;
        return t.addVectors(this.a, this.b).add(this.c).multiplyScalar(1 / 3)
    },
    normal: function(n) {
        return THREE.Triangle.normal(this.a, this.b, this.c, n)
    },
    plane: function(n) {
        var t = n || new THREE.Plane;
        return t.setFromCoplanarPoints(this.a, this.b, this.c)
    },
    barycoordFromPoint: function(n, t) {
        return THREE.Triangle.barycoordFromPoint(n, this.a, this.b, this.c, t)
    },
    containsPoint: function(n) {
        return THREE.Triangle.containsPoint(n, this.a, this.b, this.c)
    },
    closestPointToPoint: function() {
        var i, n, t, r;
        return function(u, f) {
            var e, s, o, h;
            if (i === undefined && (i = new THREE.Plane, n = [new THREE.Line3, new THREE.Line3, new THREE.Line3], t = new THREE.Vector3, r = new THREE.Vector3), e = f || new THREE.Vector3, s = Infinity, i.setFromCoplanarPoints(this.a, this.b, this.c), i.projectPoint(u, t), this.containsPoint(t) === !0) e.copy(t);
            else
                for (n[0].set(this.a, this.b), n[1].set(this.b, this.c), n[2].set(this.c, this.a), o = 0; o < n.length; o++) n[o].closestPointToPoint(t, !0, r), h = t.distanceToSquared(r), h < s && (s = h, e.copy(r));
            return e
        }
    }(),
    equals: function(n) {
        return n.a.equals(this.a) && n.b.equals(this.b) && n.c.equals(this.c)
    }
};
THREE.Interpolant = function(n, t, i, r) {
    this.parameterPositions = n;
    this._cachedIndex = 0;
    this.resultBuffer = r !== undefined ? r : new t.constructor(i);
    this.sampleValues = t;
    this.valueSize = i
};
THREE.Interpolant.prototype = {
    constructor: THREE.Interpolant,
    evaluate: function(n) {
        var u = this.parameterPositions,
            t = this._cachedIndex,
            r = u[t],
            i = u[t - 1],
            f, s, e, o;
        n: {
            t: {
                i: {
                    r: if (!(n < r)) {
                        for (e = t + 2;;) {
                            if (r === undefined) {
                                if (n < i) break r;
                                return t = u.length, this._cachedIndex = t, this.afterEnd_(t - 1, n, i)
                            }
                            if (t === e) break;
                            if (i = r, r = u[++t], n < r) break t
                        }
                        f = u.length;
                        break i
                    }if (!(n >= i)) {
                        for (s = u[1], n < s && (t = 2, i = s), e = t - 2;;) {
                            if (i === undefined) return this._cachedIndex = 0, this.beforeStart_(0, n, r);
                            if (t === e) break;
                            if (r = i, i = u[--t - 1], n >= i) break t
                        }
                        f = t;
                        t = 0;
                        break i
                    }
                    break n
                }
                while (t < f) o = t + f >>> 1,
                n < u[o] ? f = o : t = o + 1;
                if (r = u[t], i = u[t - 1], i === undefined) return this._cachedIndex = 0, this.beforeStart_(0, n, r);
                if (r === undefined) return t = u.length, this._cachedIndex = t, this.afterEnd_(t - 1, i, n)
            }
            this._cachedIndex = t;this.intervalChanged_(t, i, r)
        }
        return this.interpolate_(t, i, n, r)
    },
    settings: null,
    DefaultSettings_: {},
    getSettings_: function() {
        return this.settings || this.DefaultSettings_
    },
    copySampleValue_: function(n) {
        for (var i = this.resultBuffer, u = this.sampleValues, r = this.valueSize, f = n * r, t = 0; t !== r; ++t) i[t] = u[f + t];
        return i
    },
    interpolate_: function() {
        throw new Error("call to abstract method");
    },
    intervalChanged_: function() {}
};
Object.assign(THREE.Interpolant.prototype, {
    beforeStart_: THREE.Interpolant.prototype.copySampleValue_,
    afterEnd_: THREE.Interpolant.prototype.copySampleValue_
});
THREE.CubicInterpolant = function(n, t, i, r) {
    THREE.Interpolant.call(this, n, t, i, r);
    this._weightPrev = -0;
    this._offsetPrev = -0;
    this._weightNext = -0;
    this._offsetNext = -0
};
THREE.CubicInterpolant.prototype = Object.assign(Object.create(THREE.Interpolant.prototype), {
    constructor: THREE.CubicInterpolant,
    DefaultSettings_: {
        endingStart: THREE.ZeroCurvatureEnding,
        endingEnd: THREE.ZeroCurvatureEnding
    },
    intervalChanged_: function(n, t, i) {
        var r = this.parameterPositions,
            u = n - 2,
            f = n + 1,
            e = r[u],
            o = r[f],
            s, h;
        if (e === undefined) switch (this.getSettings_().endingStart) {
            case THREE.ZeroSlopeEnding:
                u = n;
                e = 2 * t - i;
                break;
            case THREE.WrapAroundEnding:
                u = r.length - 2;
                e = t + r[u] - r[u + 1];
                break;
            default:
                u = n;
                e = i
        }
        if (o === undefined) switch (this.getSettings_().endingEnd) {
            case THREE.ZeroSlopeEnding:
                f = n;
                o = 2 * i - t;
                break;
            case THREE.WrapAroundEnding:
                f = 1;
                o = i + r[1] - r[0];
                break;
            default:
                f = n - 1;
                o = t
        }
        s = (i - t) * .5;
        h = this.valueSize;
        this._weightPrev = s / (t - e);
        this._weightNext = s / (o - i);
        this._offsetPrev = u * h;
        this._offsetNext = f * h
    },
    interpolate_: function(n, t, i, r) {
        for (var a = this.resultBuffer, s = this.sampleValues, l = this.valueSize, v = n * l, y = v - l, p = this._offsetPrev, w = this._offsetNext, f = this._weightPrev, h = this._weightNext, e = (i - t) / (r - t), o = e * e, c = o * e, b = -f * c + 2 * f * o - f * e, k = (1 + f) * c + (-1.5 - 2 * f) * o + (-.5 + f) * e + 1, d = (-1 - h) * c + (1.5 + h) * o + .5 * e, g = h * c - h * o, u = 0; u !== l; ++u) a[u] = b * s[p + u] + k * s[y + u] + d * s[v + u] + g * s[w + u];
        return a
    }
});
THREE.DiscreteInterpolant = function(n, t, i, r) {
    THREE.Interpolant.call(this, n, t, i, r)
};
THREE.DiscreteInterpolant.prototype = Object.assign(Object.create(THREE.Interpolant.prototype), {
    constructor: THREE.DiscreteInterpolant,
    interpolate_: function(n) {
        return this.copySampleValue_(n - 1)
    }
});
THREE.LinearInterpolant = function(n, t, i, r) {
    THREE.Interpolant.call(this, n, t, i, r)
};
THREE.LinearInterpolant.prototype = Object.assign(Object.create(THREE.Interpolant.prototype), {
    constructor: THREE.LinearInterpolant,
    interpolate_: function(n, t, i, r) {
        for (var e = this.resultBuffer, o = this.sampleValues, f = this.valueSize, s = n * f, c = s - f, h = (i - t) / (r - t), l = 1 - h, u = 0; u !== f; ++u) e[u] = o[c + u] * l + o[s + u] * h;
        return e
    }
});
THREE.QuaternionLinearInterpolant = function(n, t, i, r) {
    THREE.Interpolant.call(this, n, t, i, r)
};
THREE.QuaternionLinearInterpolant.prototype = Object.assign(Object.create(THREE.Interpolant.prototype), {
    constructor: THREE.QuaternionLinearInterpolant,
    interpolate_: function(n, t, i, r) {
        for (var e = this.resultBuffer, o = this.sampleValues, f = this.valueSize, u = n * f, h = (i - t) / (r - t), s = u + f; u !== s; u += 4) THREE.Quaternion.slerpFlat(e, 0, o, u - f, o, u, h);
        return e
    }
});
THREE.Clock = function(n) {
    this.autoStart = n !== undefined ? n : !0;
    this.startTime = 0;
    this.oldTime = 0;
    this.elapsedTime = 0;
    this.running = !1
};
THREE.Clock.prototype = {
    constructor: THREE.Clock,
    start: function() {
        this.startTime = (performance || Date).now();
        this.oldTime = this.startTime;
        this.running = !0
    },
    stop: function() {
        this.getElapsedTime();
        this.running = !1
    },
    getElapsedTime: function() {
        return this.getDelta(), this.elapsedTime
    },
    getDelta: function() {
        var n = 0,
            t;
        return this.autoStart && !this.running && this.start(), this.running && (t = (performance || Date).now(), n = (t - this.oldTime) / 1e3, this.oldTime = t, this.elapsedTime += n), n
    }
};
THREE.EventDispatcher = function() {};
THREE.EventDispatcher.prototype = {
    constructor: THREE.EventDispatcher,
    apply: function(n) {
        n.addEventListener = THREE.EventDispatcher.prototype.addEventListener;
        n.hasEventListener = THREE.EventDispatcher.prototype.hasEventListener;
        n.removeEventListener = THREE.EventDispatcher.prototype.removeEventListener;
        n.dispatchEvent = THREE.EventDispatcher.prototype.dispatchEvent
    },
    addEventListener: function(n, t) {
        this._listeners === undefined && (this._listeners = {});
        var i = this._listeners;
        i[n] === undefined && (i[n] = []);
        i[n].indexOf(t) === -1 && i[n].push(t)
    },
    hasEventListener: function(n, t) {
        if (this._listeners === undefined) return !1;
        var i = this._listeners;
        return i[n] !== undefined && i[n].indexOf(t) !== -1 ? !0 : !1
    },
    removeEventListener: function(n, t) {
        var u, i, r;
        this._listeners !== undefined && (u = this._listeners, i = u[n], i !== undefined && (r = i.indexOf(t), r !== -1 && i.splice(r, 1)))
    },
    dispatchEvent: function(n) {
        var f, i, r, u, t;
        if (this._listeners !== undefined && (f = this._listeners, i = f[n.type], i !== undefined)) {
            for (n.target = this, r = [], u = i.length, t = 0; t < u; t++) r[t] = i[t];
            for (t = 0; t < u; t++) r[t].call(this, n)
        }
    }
};
THREE.Layers = function() {
    this.mask = 1
};
THREE.Layers.prototype = {
        constructor: THREE.Layers,
        set: function(n) {
            this.mask = 1 << n
        },
        enable: function(n) {
            this.mask |= 1 << n
        },
        toggle: function(n) {
            this.mask ^= 1 << n
        },
        disable: function(n) {
            this.mask &= ~(1 << n)
        },
        test: function(n) {
            return (this.mask & n.mask) != 0
        }
    },
    function(n) {
        function i(n, t) {
            return n.distance - t.distance
        }

        function t(n, i, r, u) {
            var e, f, o;
            if (n.visible !== !1 && (n.raycast(i, r), u === !0))
                for (e = n.children, f = 0, o = e.length; f < o; f++) t(e[f], i, r, !0)
        }
        n.Raycaster = function(t, i, r, u) {
            this.ray = new n.Ray(t, i);
            this.near = r || 0;
            this.far = u || Infinity;
            this.params = {
                Mesh: {},
                Line: {},
                LOD: {},
                Points: {
                    threshold: 1
                },
                Sprite: {}
            };
            Object.defineProperties(this.params, {
                PointCloud: {
                    get: function() {
                        return console.warn("THREE.Raycaster: params.PointCloud has been renamed to params.Points."), this.Points
                    }
                }
            })
        };
        n.Raycaster.prototype = {
            constructor: n.Raycaster,
            linePrecision: 1,
            set: function(n, t) {
                this.ray.set(n, t)
            },
            setFromCamera: function(t, i) {
                i instanceof n.PerspectiveCamera ? (this.ray.origin.setFromMatrixPosition(i.matrixWorld), this.ray.direction.set(t.x, t.y, .5).unproject(i).sub(this.ray.origin).normalize()) : i instanceof n.OrthographicCamera ? (this.ray.origin.set(t.x, t.y, -1).unproject(i), this.ray.direction.set(0, 0, -1).transformDirection(i.matrixWorld)) : console.error("THREE.Raycaster: Unsupported camera type.")
            },
            intersectObject: function(n, r) {
                var u = [];
                return t(n, this, u, r), u.sort(i), u
            },
            intersectObjects: function(n, r) {
                var u = [],
                    f, e;
                if (Array.isArray(n) === !1) return console.warn("THREE.Raycaster.intersectObjects: objects is not an Array."), u;
                for (f = 0, e = n.length; f < e; f++) t(n[f], this, u, r);
                return u.sort(i), u
            }
        }
    }(THREE);
THREE.Object3D = function() {
    function u() {
        t.setFromEuler(n, !1)
    }

    function f() {
        n.setFromQuaternion(t, undefined, !1)
    }
    Object.defineProperty(this, "id", {
        value: THREE.Object3DIdCount++
    });
    this.uuid = THREE.Math.generateUUID();
    this.name = "";
    this.type = "Object3D";
    this.parent = null;
    this.children = [];
    this.up = THREE.Object3D.DefaultUp.clone();
    var i = new THREE.Vector3,
        n = new THREE.Euler,
        t = new THREE.Quaternion,
        r = new THREE.Vector3(1, 1, 1);
    n.onChange(u);
    t.onChange(f);
    Object.defineProperties(this, {
        position: {
            enumerable: !0,
            value: i
        },
        rotation: {
            enumerable: !0,
            value: n
        },
        quaternion: {
            enumerable: !0,
            value: t
        },
        scale: {
            enumerable: !0,
            value: r
        },
        modelViewMatrix: {
            value: new THREE.Matrix4
        },
        normalMatrix: {
            value: new THREE.Matrix3
        }
    });
    this.rotationAutoUpdate = !0;
    this.matrix = new THREE.Matrix4;
    this.matrixWorld = new THREE.Matrix4;
    this.matrixAutoUpdate = THREE.Object3D.DefaultMatrixAutoUpdate;
    this.matrixWorldNeedsUpdate = !1;
    this.layers = new THREE.Layers;
    this.visible = !0;
    this.castShadow = !1;
    this.receiveShadow = !1;
    this.frustumCulled = !0;
    this.renderOrder = 0;
    this.userData = {}
};
THREE.Object3D.DefaultUp = new THREE.Vector3(0, 1, 0);
THREE.Object3D.DefaultMatrixAutoUpdate = !0;
THREE.Object3D.prototype = {
    constructor: THREE.Object3D,
    applyMatrix: function(n) {
        this.matrix.multiplyMatrices(n, this.matrix);
        this.matrix.decompose(this.position, this.quaternion, this.scale)
    },
    setRotationFromAxisAngle: function(n, t) {
        this.quaternion.setFromAxisAngle(n, t)
    },
    setRotationFromEuler: function(n) {
        this.quaternion.setFromEuler(n, !0)
    },
    setRotationFromMatrix: function(n) {
        this.quaternion.setFromRotationMatrix(n)
    },
    setRotationFromQuaternion: function(n) {
        this.quaternion.copy(n)
    },
    rotateOnAxis: function() {
        var n = new THREE.Quaternion;
        return function(t, i) {
            return n.setFromAxisAngle(t, i), this.quaternion.multiply(n), this
        }
    }(),
    rotateX: function() {
        var n = new THREE.Vector3(1, 0, 0);
        return function(t) {
            return this.rotateOnAxis(n, t)
        }
    }(),
    rotateY: function() {
        var n = new THREE.Vector3(0, 1, 0);
        return function(t) {
            return this.rotateOnAxis(n, t)
        }
    }(),
    rotateZ: function() {
        var n = new THREE.Vector3(0, 0, 1);
        return function(t) {
            return this.rotateOnAxis(n, t)
        }
    }(),
    translateOnAxis: function() {
        var n = new THREE.Vector3;
        return function(t, i) {
            return n.copy(t).applyQuaternion(this.quaternion), this.position.add(n.multiplyScalar(i)), this
        }
    }(),
    translateX: function() {
        var n = new THREE.Vector3(1, 0, 0);
        return function(t) {
            return this.translateOnAxis(n, t)
        }
    }(),
    translateY: function() {
        var n = new THREE.Vector3(0, 1, 0);
        return function(t) {
            return this.translateOnAxis(n, t)
        }
    }(),
    translateZ: function() {
        var n = new THREE.Vector3(0, 0, 1);
        return function(t) {
            return this.translateOnAxis(n, t)
        }
    }(),
    localToWorld: function(n) {
        return n.applyMatrix4(this.matrixWorld)
    },
    worldToLocal: function() {
        var n = new THREE.Matrix4;
        return function(t) {
            return t.applyMatrix4(n.getInverse(this.matrixWorld))
        }
    }(),
    lookAt: function() {
        var n = new THREE.Matrix4;
        return function(t) {
            n.lookAt(t, this.position, this.up);
            this.quaternion.setFromRotationMatrix(n)
        }
    }(),
    add: function(n) {
        if (arguments.length > 1) {
            for (var t = 0; t < arguments.length; t++) this.add(arguments[t]);
            return this
        }
        return n === this ? (console.error("THREE.Object3D.add: object can't be added as a child of itself.", n), this) : (n instanceof THREE.Object3D ? (n.parent !== null && n.parent.remove(n), n.parent = this, n.dispatchEvent({
            type: "added"
        }), this.children.push(n)) : console.error("THREE.Object3D.add: object not an instance of THREE.Object3D.", n), this)
    },
    remove: function(n) {
        var t, i;
        if (arguments.length > 1)
            for (t = 0; t < arguments.length; t++) this.remove(arguments[t]);
        i = this.children.indexOf(n);
        i !== -1 && (n.parent = null, n.dispatchEvent({
            type: "removed"
        }), this.children.splice(i, 1))
    },
    getObjectById: function(n) {
        return this.getObjectByProperty("id", n)
    },
    getObjectByName: function(n) {
        return this.getObjectByProperty("name", n)
    },
    getObjectByProperty: function(n, t) {
        var i, u, f, r;
        if (this[n] === t) return this;
        for (i = 0, u = this.children.length; i < u; i++)
            if (f = this.children[i], r = f.getObjectByProperty(n, t), r !== undefined) return r;
        return undefined
    },
    getWorldPosition: function(n) {
        var t = n || new THREE.Vector3;
        return this.updateMatrixWorld(!0), t.setFromMatrixPosition(this.matrixWorld)
    },
    getWorldQuaternion: function() {
        var n = new THREE.Vector3,
            t = new THREE.Vector3;
        return function(i) {
            var r = i || new THREE.Quaternion;
            return this.updateMatrixWorld(!0), this.matrixWorld.decompose(n, r, t), r
        }
    }(),
    getWorldRotation: function() {
        var n = new THREE.Quaternion;
        return function(t) {
            var i = t || new THREE.Euler;
            return this.getWorldQuaternion(n), i.setFromQuaternion(n, this.rotation.order, !1)
        }
    }(),
    getWorldScale: function() {
        var n = new THREE.Vector3,
            t = new THREE.Quaternion;
        return function(i) {
            var r = i || new THREE.Vector3;
            return this.updateMatrixWorld(!0), this.matrixWorld.decompose(n, t, r), r
        }
    }(),
    getWorldDirection: function() {
        var n = new THREE.Quaternion;
        return function(t) {
            var i = t || new THREE.Vector3;
            return this.getWorldQuaternion(n), i.set(0, 0, 1).applyQuaternion(n)
        }
    }(),
    raycast: function() {},
    traverse: function(n) {
        var i, t, r;
        for (n(this), i = this.children, t = 0, r = i.length; t < r; t++) i[t].traverse(n)
    },
    traverseVisible: function(n) {
        var i, t, r;
        if (this.visible !== !1)
            for (n(this), i = this.children, t = 0, r = i.length; t < r; t++) i[t].traverseVisible(n)
    },
    traverseAncestors: function(n) {
        var t = this.parent;
        t !== null && (n(t), t.traverseAncestors(n))
    },
    updateMatrix: function() {
        this.matrix.compose(this.position, this.quaternion, this.scale);
        this.matrixWorldNeedsUpdate = !0
    },
    updateMatrixWorld: function(n) {
        this.matrixAutoUpdate === !0 && this.updateMatrix();
        (this.matrixWorldNeedsUpdate === !0 || n === !0) && (this.parent === null ? this.matrixWorld.copy(this.matrix) : this.matrixWorld.multiplyMatrices(this.parent.matrixWorld, this.matrix), this.matrixWorldNeedsUpdate = !1, n = !0);
        for (var t = 0, i = this.children.length; t < i; t++) this.children[t].updateMatrixWorld(n)
    },
    toJSON: function(n) {
        function u(n) {
            var i = [],
                r, t;
            for (r in n) t = n[r], delete t.metadata, i.push(t);
            return i
        }
        var f = n === undefined || n === "",
            i = {},
            t, r;
        if (f && (n = {
                geometries: {},
                materials: {},
                textures: {},
                images: {}
            }, i.metadata = {
                version: 4.4,
                type: "Object",
                generator: "Object3D.toJSON"
            }), t = {}, t.uuid = this.uuid, t.type = this.type, this.name !== "" && (t.name = this.name), JSON.stringify(this.userData) !== "{}" && (t.userData = this.userData), this.castShadow === !0 && (t.castShadow = !0), this.receiveShadow === !0 && (t.receiveShadow = !0), this.visible === !1 && (t.visible = !1), t.matrix = this.matrix.toArray(), this.geometry !== undefined && (n.geometries[this.geometry.uuid] === undefined && (n.geometries[this.geometry.uuid] = this.geometry.toJSON(n)), t.geometry = this.geometry.uuid), this.material !== undefined && (n.materials[this.material.uuid] === undefined && (n.materials[this.material.uuid] = this.material.toJSON(n)), t.material = this.material.uuid), this.children.length > 0)
            for (t.children = [], r = 0; r < this.children.length; r++) t.children.push(this.children[r].toJSON(n).object);
        if (f) {
            var e = u(n.geometries),
                o = u(n.materials),
                s = u(n.textures),
                h = u(n.images);
            e.length > 0 && (i.geometries = e);
            o.length > 0 && (i.materials = o);
            s.length > 0 && (i.textures = s);
            h.length > 0 && (i.images = h)
        }
        return i.object = t, i
    },
    clone: function(n) {
        return (new this.constructor).copy(this, n)
    },
    copy: function(n, t) {
        var i, r;
        if (t === undefined && (t = !0), this.name = n.name, this.up.copy(n.up), this.position.copy(n.position), this.quaternion.copy(n.quaternion), this.scale.copy(n.scale), this.rotationAutoUpdate = n.rotationAutoUpdate, this.matrix.copy(n.matrix), this.matrixWorld.copy(n.matrixWorld), this.matrixAutoUpdate = n.matrixAutoUpdate, this.matrixWorldNeedsUpdate = n.matrixWorldNeedsUpdate, this.visible = n.visible, this.castShadow = n.castShadow, this.receiveShadow = n.receiveShadow, this.frustumCulled = n.frustumCulled, this.renderOrder = n.renderOrder, this.userData = JSON.parse(JSON.stringify(n.userData)), t === !0)
            for (i = 0; i < n.children.length; i++) r = n.children[i], this.add(r.clone());
        return this
    }
};
THREE.EventDispatcher.prototype.apply(THREE.Object3D.prototype);
THREE.Object3DIdCount = 0;
THREE.Face3 = function(n, t, i, r, u, f) {
    this.a = n;
    this.b = t;
    this.c = i;
    this.normal = r instanceof THREE.Vector3 ? r : new THREE.Vector3;
    this.vertexNormals = Array.isArray(r) ? r : [];
    this.color = u instanceof THREE.Color ? u : new THREE.Color;
    this.vertexColors = Array.isArray(u) ? u : [];
    this.materialIndex = f !== undefined ? f : 0
};
THREE.Face3.prototype = {
    constructor: THREE.Face3,
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        var t, i;
        for (this.a = n.a, this.b = n.b, this.c = n.c, this.normal.copy(n.normal), this.color.copy(n.color), this.materialIndex = n.materialIndex, t = 0, i = n.vertexNormals.length; t < i; t++) this.vertexNormals[t] = n.vertexNormals[t].clone();
        for (t = 0, i = n.vertexColors.length; t < i; t++) this.vertexColors[t] = n.vertexColors[t].clone();
        return this
    }
};
THREE.BufferAttribute = function(n, t, i) {
    this.uuid = THREE.Math.generateUUID();
    this.array = n;
    this.itemSize = t;
    this.dynamic = !1;
    this.updateRange = {
        offset: 0,
        count: -1
    };
    this.version = 0;
    this.normalized = i === !0
};
THREE.BufferAttribute.prototype = {
    constructor: THREE.BufferAttribute,
    get count() {
        return this.array.length / this.itemSize
    },
    set needsUpdate(n) {
        n === !0 && this.version++
    },
    setDynamic: function(n) {
        return this.dynamic = n, this
    },
    copy: function(n) {
        return this.array = new n.array.constructor(n.array), this.itemSize = n.itemSize, this.dynamic = n.dynamic, this
    },
    copyAt: function(n, t, i) {
        n *= this.itemSize;
        i *= t.itemSize;
        for (var r = 0, u = this.itemSize; r < u; r++) this.array[n + r] = t.array[i + r];
        return this
    },
    copyArray: function(n) {
        return this.array.set(n), this
    },
    copyColorsArray: function(n) {
        for (var r = this.array, u = 0, t, i = 0, f = n.length; i < f; i++) t = n[i], t === undefined && (console.warn("THREE.BufferAttribute.copyColorsArray(): color is undefined", i), t = new THREE.Color), r[u++] = t.r, r[u++] = t.g, r[u++] = t.b;
        return this
    },
    copyIndicesArray: function(n) {
        for (var r = this.array, u = 0, i, t = 0, f = n.length; t < f; t++) i = n[t], r[u++] = i.a, r[u++] = i.b, r[u++] = i.c;
        return this
    },
    copyVector2sArray: function(n) {
        for (var r = this.array, u = 0, i, t = 0, f = n.length; t < f; t++) i = n[t], i === undefined && (console.warn("THREE.BufferAttribute.copyVector2sArray(): vector is undefined", t), i = new THREE.Vector2), r[u++] = i.x, r[u++] = i.y;
        return this
    },
    copyVector3sArray: function(n) {
        for (var r = this.array, u = 0, t, i = 0, f = n.length; i < f; i++) t = n[i], t === undefined && (console.warn("THREE.BufferAttribute.copyVector3sArray(): vector is undefined", i), t = new THREE.Vector3), r[u++] = t.x, r[u++] = t.y, r[u++] = t.z;
        return this
    },
    copyVector4sArray: function(n) {
        for (var r = this.array, u = 0, t, i = 0, f = n.length; i < f; i++) t = n[i], t === undefined && (console.warn("THREE.BufferAttribute.copyVector4sArray(): vector is undefined", i), t = new THREE.Vector4), r[u++] = t.x, r[u++] = t.y, r[u++] = t.z, r[u++] = t.w;
        return this
    },
    set: function(n, t) {
        return t === undefined && (t = 0), this.array.set(n, t), this
    },
    getX: function(n) {
        return this.array[n * this.itemSize]
    },
    setX: function(n, t) {
        return this.array[n * this.itemSize] = t, this
    },
    getY: function(n) {
        return this.array[n * this.itemSize + 1]
    },
    setY: function(n, t) {
        return this.array[n * this.itemSize + 1] = t, this
    },
    getZ: function(n) {
        return this.array[n * this.itemSize + 2]
    },
    setZ: function(n, t) {
        return this.array[n * this.itemSize + 2] = t, this
    },
    getW: function(n) {
        return this.array[n * this.itemSize + 3]
    },
    setW: function(n, t) {
        return this.array[n * this.itemSize + 3] = t, this
    },
    setXY: function(n, t, i) {
        return n *= this.itemSize, this.array[n + 0] = t, this.array[n + 1] = i, this
    },
    setXYZ: function(n, t, i, r) {
        return n *= this.itemSize, this.array[n + 0] = t, this.array[n + 1] = i, this.array[n + 2] = r, this
    },
    setXYZW: function(n, t, i, r, u) {
        return n *= this.itemSize, this.array[n + 0] = t, this.array[n + 1] = i, this.array[n + 2] = r, this.array[n + 3] = u, this
    },
    clone: function() {
        return (new this.constructor).copy(this)
    }
};
THREE.Int8Attribute = function(n, t) {
    return new THREE.BufferAttribute(new Int8Array(n), t)
};
THREE.Uint8Attribute = function(n, t) {
    return new THREE.BufferAttribute(new Uint8Array(n), t)
};
THREE.Uint8ClampedAttribute = function(n, t) {
    return new THREE.BufferAttribute(new Uint8ClampedArray(n), t)
};
THREE.Int16Attribute = function(n, t) {
    return new THREE.BufferAttribute(new Int16Array(n), t)
};
THREE.Uint16Attribute = function(n, t) {
    return new THREE.BufferAttribute(new Uint16Array(n), t)
};
THREE.Int32Attribute = function(n, t) {
    return new THREE.BufferAttribute(new Int32Array(n), t)
};
THREE.Uint32Attribute = function(n, t) {
    return new THREE.BufferAttribute(new Uint32Array(n), t)
};
THREE.Float32Attribute = function(n, t) {
    return new THREE.BufferAttribute(new Float32Array(n), t)
};
THREE.Float64Attribute = function(n, t) {
    return new THREE.BufferAttribute(new Float64Array(n), t)
};
THREE.DynamicBufferAttribute = function(n, t) {
    return console.warn("THREE.DynamicBufferAttribute has been removed. Use new THREE.BufferAttribute().setDynamic( true ) instead."), new THREE.BufferAttribute(n, t).setDynamic(!0)
};
THREE.InstancedBufferAttribute = function(n, t, i) {
    THREE.BufferAttribute.call(this, n, t);
    this.meshPerAttribute = i || 1
};
THREE.InstancedBufferAttribute.prototype = Object.create(THREE.BufferAttribute.prototype);
THREE.InstancedBufferAttribute.prototype.constructor = THREE.InstancedBufferAttribute;
THREE.InstancedBufferAttribute.prototype.copy = function(n) {
    return THREE.BufferAttribute.prototype.copy.call(this, n), this.meshPerAttribute = n.meshPerAttribute, this
};
THREE.InterleavedBuffer = function(n, t) {
    this.uuid = THREE.Math.generateUUID();
    this.array = n;
    this.stride = t;
    this.dynamic = !1;
    this.updateRange = {
        offset: 0,
        count: -1
    };
    this.version = 0
};
THREE.InterleavedBuffer.prototype = {
    constructor: THREE.InterleavedBuffer,
    get length() {
        return this.array.length
    },
    get count() {
        return this.array.length / this.stride
    },
    set needsUpdate(n) {
        n === !0 && this.version++
    },
    setDynamic: function(n) {
        return this.dynamic = n, this
    },
    copy: function(n) {
        return this.array = new n.array.constructor(n.array), this.stride = n.stride, this.dynamic = n.dynamic, this
    },
    copyAt: function(n, t, i) {
        n *= this.stride;
        i *= t.stride;
        for (var r = 0, u = this.stride; r < u; r++) this.array[n + r] = t.array[i + r];
        return this
    },
    set: function(n, t) {
        return t === undefined && (t = 0), this.array.set(n, t), this
    },
    clone: function() {
        return (new this.constructor).copy(this)
    }
};
THREE.InstancedInterleavedBuffer = function(n, t, i) {
    THREE.InterleavedBuffer.call(this, n, t);
    this.meshPerAttribute = i || 1
};
THREE.InstancedInterleavedBuffer.prototype = Object.create(THREE.InterleavedBuffer.prototype);
THREE.InstancedInterleavedBuffer.prototype.constructor = THREE.InstancedInterleavedBuffer;
THREE.InstancedInterleavedBuffer.prototype.copy = function(n) {
    return THREE.InterleavedBuffer.prototype.copy.call(this, n), this.meshPerAttribute = n.meshPerAttribute, this
};
THREE.InterleavedBufferAttribute = function(n, t, i) {
    this.uuid = THREE.Math.generateUUID();
    this.data = n;
    this.itemSize = t;
    this.offset = i
};
THREE.InterleavedBufferAttribute.prototype = {
    constructor: THREE.InterleavedBufferAttribute,
    get length() {
        return console.warn("THREE.BufferAttribute: .length has been deprecated. Please use .count."), this.array.length
    },
    get count() {
        return this.data.count
    },
    setX: function(n, t) {
        return this.data.array[n * this.data.stride + this.offset] = t, this
    },
    setY: function(n, t) {
        return this.data.array[n * this.data.stride + this.offset + 1] = t, this
    },
    setZ: function(n, t) {
        return this.data.array[n * this.data.stride + this.offset + 2] = t, this
    },
    setW: function(n, t) {
        return this.data.array[n * this.data.stride + this.offset + 3] = t, this
    },
    getX: function(n) {
        return this.data.array[n * this.data.stride + this.offset]
    },
    getY: function(n) {
        return this.data.array[n * this.data.stride + this.offset + 1]
    },
    getZ: function(n) {
        return this.data.array[n * this.data.stride + this.offset + 2]
    },
    getW: function(n) {
        return this.data.array[n * this.data.stride + this.offset + 3]
    },
    setXY: function(n, t, i) {
        return n = n * this.data.stride + this.offset, this.data.array[n + 0] = t, this.data.array[n + 1] = i, this
    },
    setXYZ: function(n, t, i, r) {
        return n = n * this.data.stride + this.offset, this.data.array[n + 0] = t, this.data.array[n + 1] = i, this.data.array[n + 2] = r, this
    },
    setXYZW: function(n, t, i, r, u) {
        return n = n * this.data.stride + this.offset, this.data.array[n + 0] = t, this.data.array[n + 1] = i, this.data.array[n + 2] = r, this.data.array[n + 3] = u, this
    }
};
THREE.Geometry = function() {
    Object.defineProperty(this, "id", {
        value: THREE.GeometryIdCount++
    });
    this.uuid = THREE.Math.generateUUID();
    this.name = "";
    this.type = "Geometry";
    this.vertices = [];
    this.colors = [];
    this.faces = [];
    this.faceVertexUvs = [
        []
    ];
    this.morphTargets = [];
    this.morphNormals = [];
    this.skinWeights = [];
    this.skinIndices = [];
    this.lineDistances = [];
    this.boundingBox = null;
    this.boundingSphere = null;
    this.verticesNeedUpdate = !1;
    this.elementsNeedUpdate = !1;
    this.uvsNeedUpdate = !1;
    this.normalsNeedUpdate = !1;
    this.colorsNeedUpdate = !1;
    this.lineDistancesNeedUpdate = !1;
    this.groupsNeedUpdate = !1
};
THREE.Geometry.prototype = {
    constructor: THREE.Geometry,
    applyMatrix: function(n) {
        for (var f = (new THREE.Matrix3).getNormalMatrix(n), e, r, u, o, t = 0, i = this.vertices.length; t < i; t++) e = this.vertices[t], e.applyMatrix4(n);
        for (t = 0, i = this.faces.length; t < i; t++)
            for (r = this.faces[t], r.normal.applyMatrix3(f).normalize(), u = 0, o = r.vertexNormals.length; u < o; u++) r.vertexNormals[u].applyMatrix3(f).normalize();
        return this.boundingBox !== null && this.computeBoundingBox(), this.boundingSphere !== null && this.computeBoundingSphere(), this.verticesNeedUpdate = !0, this.normalsNeedUpdate = !0, this
    },
    rotateX: function() {
        var n;
        return function(t) {
            return n === undefined && (n = new THREE.Matrix4), n.makeRotationX(t), this.applyMatrix(n), this
        }
    }(),
    rotateY: function() {
        var n;
        return function(t) {
            return n === undefined && (n = new THREE.Matrix4), n.makeRotationY(t), this.applyMatrix(n), this
        }
    }(),
    rotateZ: function() {
        var n;
        return function(t) {
            return n === undefined && (n = new THREE.Matrix4), n.makeRotationZ(t), this.applyMatrix(n), this
        }
    }(),
    translate: function() {
        var n;
        return function(t, i, r) {
            return n === undefined && (n = new THREE.Matrix4), n.makeTranslation(t, i, r), this.applyMatrix(n), this
        }
    }(),
    scale: function() {
        var n;
        return function(t, i, r) {
            return n === undefined && (n = new THREE.Matrix4), n.makeScale(t, i, r), this.applyMatrix(n), this
        }
    }(),
    lookAt: function() {
        var n;
        return function(t) {
            n === undefined && (n = new THREE.Object3D);
            n.lookAt(t);
            n.updateMatrix();
            this.applyMatrix(n.matrix)
        }
    }(),
    fromBufferGeometry: function(n) {
        function p(n, t, i, r) {
            var f = o !== undefined ? [l[n].clone(), l[t].clone(), l[i].clone()] : [],
                e = s !== undefined ? [u.colors[n].clone(), u.colors[t].clone(), u.colors[i].clone()] : [],
                y = new THREE.Face3(n, t, i, f, e, r);
            u.faces.push(y);
            c !== undefined && u.faceVertexUvs[0].push([a[n].clone(), a[t].clone(), a[i].clone()]);
            h !== undefined && u.faceVertexUvs[1].push([v[n].clone(), v[t].clone(), v[i].clone()])
        }
        var u = this,
            f = n.index !== null ? n.index.array : undefined,
            r = n.attributes,
            e = r.position.array,
            o = r.normal !== undefined ? r.normal.array : undefined,
            s = r.color !== undefined ? r.color.array : undefined,
            c = r.uv !== undefined ? r.uv.array : undefined,
            h = r.uv2 !== undefined ? r.uv2.array : undefined,
            y, i, k, t;
        h !== undefined && (this.faceVertexUvs[1] = []);
        var l = [],
            a = [],
            v = [];
        for (t = 0, i = 0; t < e.length; t += 3, i += 2) u.vertices.push(new THREE.Vector3(e[t], e[t + 1], e[t + 2])), o !== undefined && l.push(new THREE.Vector3(o[t], o[t + 1], o[t + 2])), s !== undefined && u.colors.push(new THREE.Color(s[t], s[t + 1], s[t + 2])), c !== undefined && a.push(new THREE.Vector2(c[i], c[i + 1])), h !== undefined && v.push(new THREE.Vector2(h[i], h[i + 1]));
        if (f !== undefined)
            if (y = n.groups, y.length > 0)
                for (t = 0; t < y.length; t++) {
                    var w = y[t],
                        b = w.start,
                        d = w.count;
                    for (i = b, k = b + d; i < k; i += 3) p(f[i], f[i + 1], f[i + 2], w.materialIndex)
                } else
                    for (t = 0; t < f.length; t += 3) p(f[t], f[t + 1], f[t + 2]);
            else
                for (t = 0; t < e.length / 3; t += 3) p(t, t + 1, t + 2);
        return this.computeFaceNormals(), n.boundingBox !== null && (this.boundingBox = n.boundingBox.clone()), n.boundingSphere !== null && (this.boundingSphere = n.boundingSphere.clone()), this
    },
    center: function() {
        this.computeBoundingBox();
        var n = this.boundingBox.center().negate();
        return this.translate(n.x, n.y, n.z), n
    },
    normalize: function() {
        this.computeBoundingSphere();
        var t = this.boundingSphere.center,
            i = this.boundingSphere.radius,
            n = i === 0 ? 1 : 1 / i,
            r = new THREE.Matrix4;
        return r.set(n, 0, 0, -n * t.x, 0, n, 0, -n * t.y, 0, 0, n, -n * t.z, 0, 0, 0, 1), this.applyMatrix(r), this
    },
    computeFaceNormals: function() {
        for (var n = new THREE.Vector3, r = new THREE.Vector3, t = 0, u = this.faces.length; t < u; t++) {
            var i = this.faces[t],
                e = this.vertices[i.a],
                f = this.vertices[i.b],
                o = this.vertices[i.c];
            n.subVectors(o, f);
            r.subVectors(e, f);
            n.cross(r);
            n.normalize();
            i.normal.copy(n)
        }
    },
    computeVertexNormals: function(n) {
        var u, s, r, e, t, i, l, h, a, o, c, f;
        for (n === undefined && (n = !0), i = new Array(this.vertices.length), u = 0, s = this.vertices.length; u < s; u++) i[u] = new THREE.Vector3;
        if (n)
            for (o = new THREE.Vector3, c = new THREE.Vector3, r = 0, e = this.faces.length; r < e; r++) t = this.faces[r], l = this.vertices[t.a], h = this.vertices[t.b], a = this.vertices[t.c], o.subVectors(a, h), c.subVectors(l, h), o.cross(c), i[t.a].add(o), i[t.b].add(o), i[t.c].add(o);
        else
            for (r = 0, e = this.faces.length; r < e; r++) t = this.faces[r], i[t.a].add(t.normal), i[t.b].add(t.normal), i[t.c].add(t.normal);
        for (u = 0, s = this.vertices.length; u < s; u++) i[u].normalize();
        for (r = 0, e = this.faces.length; r < e; r++) t = this.faces[r], f = t.vertexNormals, f.length === 3 ? (f[0].copy(i[t.a]), f[1].copy(i[t.b]), f[2].copy(i[t.c])) : (f[0] = i[t.a].clone(), f[1] = i[t.b].clone(), f[2] = i[t.c].clone());
        this.faces.length > 0 && (this.normalsNeedUpdate = !0)
    },
    computeMorphNormals: function() {
        for (var t, e, n, f, h, c, s, o, u, i = 0, r = this.faces.length; i < r; i++)
            for (n = this.faces[i], n.__originalFaceNormal ? n.__originalFaceNormal.copy(n.normal) : n.__originalFaceNormal = n.normal.clone(), n.__originalVertexNormals || (n.__originalVertexNormals = []), t = 0, e = n.vertexNormals.length; t < e; t++) n.__originalVertexNormals[t] ? n.__originalVertexNormals[t].copy(n.vertexNormals[t]) : n.__originalVertexNormals[t] = n.vertexNormals[t].clone();
        for (f = new THREE.Geometry, f.faces = this.faces, t = 0, e = this.morphTargets.length; t < e; t++) {
            if (!this.morphNormals[t])
                for (this.morphNormals[t] = {}, this.morphNormals[t].faceNormals = [], this.morphNormals[t].vertexNormals = [], h = this.morphNormals[t].faceNormals, c = this.morphNormals[t].vertexNormals, i = 0, r = this.faces.length; i < r; i++) o = new THREE.Vector3, u = {
                    a: new THREE.Vector3,
                    b: new THREE.Vector3,
                    c: new THREE.Vector3
                }, h.push(o), c.push(u);
            for (s = this.morphNormals[t], f.vertices = this.morphTargets[t].vertices, f.computeFaceNormals(), f.computeVertexNormals(), i = 0, r = this.faces.length; i < r; i++) n = this.faces[i], o = s.faceNormals[i], u = s.vertexNormals[i], o.copy(n.normal), u.a.copy(n.vertexNormals[0]), u.b.copy(n.vertexNormals[1]), u.c.copy(n.vertexNormals[2])
        }
        for (i = 0, r = this.faces.length; i < r; i++) n = this.faces[i], n.normal = n.__originalFaceNormal, n.vertexNormals = n.__originalVertexNormals
    },
    computeTangents: function() {
        console.warn("THREE.Geometry: .computeTangents() has been removed.")
    },
    computeLineDistances: function() {
        for (var i = 0, t = this.vertices, n = 0, r = t.length; n < r; n++) n > 0 && (i += t[n].distanceTo(t[n - 1])), this.lineDistances[n] = i
    },
    computeBoundingBox: function() {
        this.boundingBox === null && (this.boundingBox = new THREE.Box3);
        this.boundingBox.setFromPoints(this.vertices)
    },
    computeBoundingSphere: function() {
        this.boundingSphere === null && (this.boundingSphere = new THREE.Sphere);
        this.boundingSphere.setFromPoints(this.vertices)
    },
    merge: function(n, t, i) {
        var u, s, k, a, c, y, r, o;
        if (n instanceof THREE.Geometry == !1) {
            console.error("THREE.Geometry.merge(): geometry not an instance of THREE.Geometry.", n);
            return
        }
        var h, l = this.vertices.length,
            tt = this.vertices,
            p = n.vertices,
            it = this.faces,
            w = n.faces,
            rt = this.faceVertexUvs[0],
            b = n.faceVertexUvs[0];
        for (i === undefined && (i = 0), t !== undefined && (h = (new THREE.Matrix3).getNormalMatrix(t)), u = 0, s = p.length; u < s; u++) k = p[u], a = k.clone(), t !== undefined && a.applyMatrix4(t), tt.push(a);
        for (u = 0, s = w.length; u < s; u++) {
            var f = w[u],
                e, v, d, g = f.vertexNormals,
                nt = f.vertexColors;
            for (e = new THREE.Face3(f.a + l, f.b + l, f.c + l), e.normal.copy(f.normal), h !== undefined && e.normal.applyMatrix3(h).normalize(), r = 0, o = g.length; r < o; r++) v = g[r].clone(), h !== undefined && v.applyMatrix3(h).normalize(), e.vertexNormals.push(v);
            for (e.color.copy(f.color), r = 0, o = nt.length; r < o; r++) d = nt[r], e.vertexColors.push(d.clone());
            e.materialIndex = f.materialIndex + i;
            it.push(e)
        }
        for (u = 0, s = b.length; u < s; u++)
            if (c = b[u], y = [], c !== undefined) {
                for (r = 0, o = c.length; r < o; r++) y.push(c[r].clone());
                rt.push(y)
            }
    },
    mergeMesh: function(n) {
        if (n instanceof THREE.Mesh == !1) {
            console.error("THREE.Geometry.mergeMesh(): mesh not an instance of THREE.Mesh.", n);
            return
        }
        n.matrixAutoUpdate && n.updateMatrix();
        this.merge(n.geometry, n.matrix)
    },
    mergeVertices: function() {
        for (var c = {}, u = [], i = [], f, e, l = Math.pow(10, 4), t, a, s, y, h, p, r, v, w, n = 0, o = this.vertices.length; n < o; n++) f = this.vertices[n], e = Math.round(f.x * l) + "_" + Math.round(f.y * l) + "_" + Math.round(f.z * l), c[e] === undefined ? (c[e] = n, u.push(this.vertices[n]), i[n] = u.length - 1) : i[n] = i[c[e]];
        for (h = [], n = 0, o = this.faces.length; n < o; n++)
            for (t = this.faces[n], t.a = i[t.a], t.b = i[t.b], t.c = i[t.c], a = [t.a, t.b, t.c], p = -1, r = 0; r < 3; r++)
                if (a[r] === a[(r + 1) % 3]) {
                    p = r;
                    h.push(n);
                    break
                }
        for (n = h.length - 1; n >= 0; n--)
            for (v = h[n], this.faces.splice(v, 1), s = 0, y = this.faceVertexUvs.length; s < y; s++) this.faceVertexUvs[s].splice(v, 1);
        return w = this.vertices.length - u.length, this.vertices = u, w
    },
    sortFacesByMaterialIndex: function() {
        function s(n, t) {
            return n.materialIndex - t.materialIndex
        }
        for (var r = this.faces, u = r.length, f, e, t, i, o, n = 0; n < u; n++) r[n]._id = n;
        for (r.sort(s), f = this.faceVertexUvs[0], e = this.faceVertexUvs[1], f && f.length === u && (t = []), e && e.length === u && (i = []), n = 0; n < u; n++) o = r[n]._id, t && t.push(f[o]), i && i.push(e[o]);
        t && (this.faceVertexUvs[0] = t);
        i && (this.faceVertexUvs[1] = i)
    },
    toJSON: function() {
        function f(n, t, i) {
            return i ? n | 1 << t : n & ~(1 << t)
        }

        function b(n) {
            var t = n.x.toString() + n.y.toString() + n.z.toString();
            return h[t] !== undefined ? h[t] : (h[t] = g.length / 3, g.push(n.x, n.y, n.z), h[t])
        }

        function k(n) {
            var t = n.r.toString() + n.g.toString() + n.b.toString();
            return l[t] !== undefined ? l[t] : (l[t] = c.length, c.push(n.getHex()), l[t])
        }

        function nt(n) {
            var t = n.x.toString() + n.y.toString();
            return v[t] !== undefined ? v[t] : (v[t] = a.length / 2, a.push(n.x, n.y), v[t])
        }
        var i = {
                metadata: {
                    version: 4.4,
                    type: "Geometry",
                    generator: "Geometry.toJSON"
                }
            },
            e, o, d, s, r, y, p, w;
        if (i.uuid = this.uuid, i.type = this.type, this.name !== "" && (i.name = this.name), this.parameters !== undefined) {
            e = this.parameters;
            for (o in e) e[o] !== undefined && (i[o] = e[o]);
            return i
        }
        for (d = [], r = 0; r < this.vertices.length; r++) s = this.vertices[r], d.push(s.x, s.y, s.z);
        var u = [],
            g = [],
            h = {},
            c = [],
            l = {},
            a = [],
            v = {};
        for (r = 0; r < this.faces.length; r++) {
            var t = this.faces[r],
                tt = this.faceVertexUvs[0][r] !== undefined,
                it = t.normal.length() > 0,
                rt = t.vertexNormals.length > 0,
                ut = t.color.r !== 1 || t.color.g !== 1 || t.color.b !== 1,
                ft = t.vertexColors.length > 0,
                n = 0;
            n = f(n, 0, 0);
            n = f(n, 1, !0);
            n = f(n, 2, !1);
            n = f(n, 3, tt);
            n = f(n, 4, it);
            n = f(n, 5, rt);
            n = f(n, 6, ut);
            n = f(n, 7, ft);
            u.push(n);
            u.push(t.a, t.b, t.c);
            u.push(t.materialIndex);
            tt && (y = this.faceVertexUvs[0][r], u.push(nt(y[0]), nt(y[1]), nt(y[2])));
            it && u.push(b(t.normal));
            rt && (p = t.vertexNormals, u.push(b(p[0]), b(p[1]), b(p[2])));
            ut && u.push(k(t.color));
            ft && (w = t.vertexColors, u.push(k(w[0]), k(w[1]), k(w[2])))
        }
        return i.data = {}, i.data.vertices = d, i.data.normals = g, c.length > 0 && (i.data.colors = c), a.length > 0 && (i.data.uvs = [a]), i.data.faces = u, i
    },
    clone: function() {
        return (new THREE.Geometry).copy(this)
    },
    copy: function(n) {
        var f, e, t, i, o, r, c, s, h, u, l, a;
        for (this.vertices = [], this.faces = [], this.faceVertexUvs = [
                []
            ], f = n.vertices, t = 0, i = f.length; t < i; t++) this.vertices.push(f[t].clone());
        for (e = n.faces, t = 0, i = e.length; t < i; t++) this.faces.push(e[t].clone());
        for (t = 0, i = n.faceVertexUvs.length; t < i; t++)
            for (o = n.faceVertexUvs[t], this.faceVertexUvs[t] === undefined && (this.faceVertexUvs[t] = []), r = 0, c = o.length; r < c; r++) {
                for (s = o[r], h = [], u = 0, l = s.length; u < l; u++) a = s[u], h.push(a.clone());
                this.faceVertexUvs[t].push(h)
            }
        return this
    },
    dispose: function() {
        this.dispatchEvent({
            type: "dispose"
        })
    }
};
THREE.EventDispatcher.prototype.apply(THREE.Geometry.prototype);
THREE.GeometryIdCount = 0;
THREE.DirectGeometry = function() {
    Object.defineProperty(this, "id", {
        value: THREE.GeometryIdCount++
    });
    this.uuid = THREE.Math.generateUUID();
    this.name = "";
    this.type = "DirectGeometry";
    this.indices = [];
    this.vertices = [];
    this.normals = [];
    this.colors = [];
    this.uvs = [];
    this.uvs2 = [];
    this.groups = [];
    this.morphTargets = {};
    this.skinWeights = [];
    this.skinIndices = [];
    this.boundingBox = null;
    this.boundingSphere = null;
    this.verticesNeedUpdate = !1;
    this.normalsNeedUpdate = !1;
    this.colorsNeedUpdate = !1;
    this.uvsNeedUpdate = !1;
    this.groupsNeedUpdate = !1
};
THREE.DirectGeometry.prototype = {
    constructor: THREE.DirectGeometry,
    computeBoundingBox: THREE.Geometry.prototype.computeBoundingBox,
    computeBoundingSphere: THREE.Geometry.prototype.computeBoundingSphere,
    computeFaceNormals: function() {
        console.warn("THREE.DirectGeometry: computeFaceNormals() is not a method of this type of geometry.")
    },
    computeVertexNormals: function() {
        console.warn("THREE.DirectGeometry: computeVertexNormals() is not a method of this type of geometry.")
    },
    computeGroups: function(n) {
        for (var t, r = [], u, e = n.faces, f, i = 0; i < e.length; i++) f = e[i], f.materialIndex !== u && (u = f.materialIndex, t !== undefined && (t.count = i * 3 - t.start, r.push(t)), t = {
            start: i * 3,
            materialIndex: u
        });
        t !== undefined && (t.count = i * 3 - t.start, r.push(t));
        this.groups = r
    },
    fromGeometry: function(n) {
        var g = n.faces,
            e = n.vertices,
            f = n.faceVertexUvs,
            tt = f[0] && f[0].length > 0,
            it = f[1] && f[1].length > 0,
            nt = n.morphTargets,
            k = nt.length,
            h, d, c, l, t, i, o, y, s, p, r, w, u, b;
        if (k > 0) {
            for (h = [], t = 0; t < k; t++) h[t] = [];
            this.morphTargets.position = h
        }
        if (d = n.morphNormals, c = d.length, c > 0) {
            for (l = [], t = 0; t < c; t++) l[t] = [];
            this.morphTargets.normal = l
        }
        var a = n.skinIndices,
            v = n.skinWeights,
            rt = a.length === e.length,
            ut = v.length === e.length;
        for (t = 0; t < g.length; t++) {
            for (i = g[t], this.vertices.push(e[i.a], e[i.b], e[i.c]), o = i.vertexNormals, o.length === 3 ? this.normals.push(o[0], o[1], o[2]) : (y = i.normal, this.normals.push(y, y, y)), s = i.vertexColors, s.length === 3 ? this.colors.push(s[0], s[1], s[2]) : (p = i.color, this.colors.push(p, p, p)), tt === !0 && (r = f[0][t], r !== undefined ? this.uvs.push(r[0], r[1], r[2]) : (console.warn("THREE.DirectGeometry.fromGeometry(): Undefined vertexUv ", t), this.uvs.push(new THREE.Vector2, new THREE.Vector2, new THREE.Vector2))), it === !0 && (r = f[1][t], r !== undefined ? this.uvs2.push(r[0], r[1], r[2]) : (console.warn("THREE.DirectGeometry.fromGeometry(): Undefined vertexUv2 ", t), this.uvs2.push(new THREE.Vector2, new THREE.Vector2, new THREE.Vector2))), u = 0; u < k; u++) w = nt[u].vertices, h[u].push(w[i.a], w[i.b], w[i.c]);
            for (u = 0; u < c; u++) b = d[u].vertexNormals[t], l[u].push(b.a, b.b, b.c);
            rt && this.skinIndices.push(a[i.a], a[i.b], a[i.c]);
            ut && this.skinWeights.push(v[i.a], v[i.b], v[i.c])
        }
        return this.computeGroups(n), this.verticesNeedUpdate = n.verticesNeedUpdate, this.normalsNeedUpdate = n.normalsNeedUpdate, this.colorsNeedUpdate = n.colorsNeedUpdate, this.uvsNeedUpdate = n.uvsNeedUpdate, this.groupsNeedUpdate = n.groupsNeedUpdate, this
    },
    dispose: function() {
        this.dispatchEvent({
            type: "dispose"
        })
    }
};
THREE.EventDispatcher.prototype.apply(THREE.DirectGeometry.prototype);
THREE.BufferGeometry = function() {
    Object.defineProperty(this, "id", {
        value: THREE.GeometryIdCount++
    });
    this.uuid = THREE.Math.generateUUID();
    this.name = "";
    this.type = "BufferGeometry";
    this.index = null;
    this.attributes = {};
    this.morphAttributes = {};
    this.groups = [];
    this.boundingBox = null;
    this.boundingSphere = null;
    this.drawRange = {
        start: 0,
        count: Infinity
    }
};
THREE.BufferGeometry.prototype = {
    constructor: THREE.BufferGeometry,
    getIndex: function() {
        return this.index
    },
    setIndex: function(n) {
        this.index = n
    },
    addAttribute: function(n, t) {
        if (t instanceof THREE.BufferAttribute == !1 && t instanceof THREE.InterleavedBufferAttribute == !1) {
            console.warn("THREE.BufferGeometry: .addAttribute() now expects ( name, attribute ).");
            this.addAttribute(n, new THREE.BufferAttribute(arguments[1], arguments[2]));
            return
        }
        if (n === "index") {
            console.warn("THREE.BufferGeometry.addAttribute: Use .setIndex() for index attribute.");
            this.setIndex(t);
            return
        }
        return this.attributes[n] = t, this
    },
    getAttribute: function(n) {
        return this.attributes[n]
    },
    removeAttribute: function(n) {
        return delete this.attributes[n], this
    },
    addGroup: function(n, t, i) {
        this.groups.push({
            start: n,
            count: t,
            materialIndex: i !== undefined ? i : 0
        })
    },
    clearGroups: function() {
        this.groups = []
    },
    setDrawRange: function(n, t) {
        this.drawRange.start = n;
        this.drawRange.count = t
    },
    applyMatrix: function(n) {
        var i = this.attributes.position,
            t, r;
        return i !== undefined && (n.applyToVector3Array(i.array), i.needsUpdate = !0), t = this.attributes.normal, t !== undefined && (r = (new THREE.Matrix3).getNormalMatrix(n), r.applyToVector3Array(t.array), t.needsUpdate = !0), this.boundingBox !== null && this.computeBoundingBox(), this.boundingSphere !== null && this.computeBoundingSphere(), this
    },
    rotateX: function() {
        var n;
        return function(t) {
            return n === undefined && (n = new THREE.Matrix4), n.makeRotationX(t), this.applyMatrix(n), this
        }
    }(),
    rotateY: function() {
        var n;
        return function(t) {
            return n === undefined && (n = new THREE.Matrix4), n.makeRotationY(t), this.applyMatrix(n), this
        }
    }(),
    rotateZ: function() {
        var n;
        return function(t) {
            return n === undefined && (n = new THREE.Matrix4), n.makeRotationZ(t), this.applyMatrix(n), this
        }
    }(),
    translate: function() {
        var n;
        return function(t, i, r) {
            return n === undefined && (n = new THREE.Matrix4), n.makeTranslation(t, i, r), this.applyMatrix(n), this
        }
    }(),
    scale: function() {
        var n;
        return function(t, i, r) {
            return n === undefined && (n = new THREE.Matrix4), n.makeScale(t, i, r), this.applyMatrix(n), this
        }
    }(),
    lookAt: function() {
        var n;
        return function(t) {
            n === undefined && (n = new THREE.Object3D);
            n.lookAt(t);
            n.updateMatrix();
            this.applyMatrix(n.matrix)
        }
    }(),
    center: function() {
        this.computeBoundingBox();
        var n = this.boundingBox.center().negate();
        return this.translate(n.x, n.y, n.z), n
    },
    setFromObject: function(n) {
        var t = n.geometry,
            i, r, u;
        return n instanceof THREE.Points || n instanceof THREE.Line ? (i = new THREE.Float32Attribute(t.vertices.length * 3, 3), r = new THREE.Float32Attribute(t.colors.length * 3, 3), this.addAttribute("position", i.copyVector3sArray(t.vertices)), this.addAttribute("color", r.copyColorsArray(t.colors)), t.lineDistances && t.lineDistances.length === t.vertices.length && (u = new THREE.Float32Attribute(t.lineDistances.length, 1), this.addAttribute("lineDistance", u.copyArray(t.lineDistances))), t.boundingSphere !== null && (this.boundingSphere = t.boundingSphere.clone()), t.boundingBox !== null && (this.boundingBox = t.boundingBox.clone())) : n instanceof THREE.Mesh && t instanceof THREE.Geometry && this.fromGeometry(t), this
    },
    updateFromObject: function(n) {
        var t = n.geometry,
            r, i;
        if (n instanceof THREE.Mesh) {
            if (r = t.__directGeometry, r === undefined) return this.fromGeometry(t);
            r.verticesNeedUpdate = t.verticesNeedUpdate;
            r.normalsNeedUpdate = t.normalsNeedUpdate;
            r.colorsNeedUpdate = t.colorsNeedUpdate;
            r.uvsNeedUpdate = t.uvsNeedUpdate;
            r.groupsNeedUpdate = t.groupsNeedUpdate;
            t.verticesNeedUpdate = !1;
            t.normalsNeedUpdate = !1;
            t.colorsNeedUpdate = !1;
            t.uvsNeedUpdate = !1;
            t.groupsNeedUpdate = !1;
            t = r
        }
        return t.verticesNeedUpdate === !0 && (i = this.attributes.position, i !== undefined && (i.copyVector3sArray(t.vertices), i.needsUpdate = !0), t.verticesNeedUpdate = !1), t.normalsNeedUpdate === !0 && (i = this.attributes.normal, i !== undefined && (i.copyVector3sArray(t.normals), i.needsUpdate = !0), t.normalsNeedUpdate = !1), t.colorsNeedUpdate === !0 && (i = this.attributes.color, i !== undefined && (i.copyColorsArray(t.colors), i.needsUpdate = !0), t.colorsNeedUpdate = !1), t.uvsNeedUpdate && (i = this.attributes.uv, i !== undefined && (i.copyVector2sArray(t.uvs), i.needsUpdate = !0), t.uvsNeedUpdate = !1), t.lineDistancesNeedUpdate && (i = this.attributes.lineDistance, i !== undefined && (i.copyArray(t.lineDistances), i.needsUpdate = !0), t.lineDistancesNeedUpdate = !1), t.groupsNeedUpdate && (t.computeGroups(n.geometry), this.groups = t.groups, t.groupsNeedUpdate = !1), this
    },
    fromGeometry: function(n) {
        return n.__directGeometry = (new THREE.DirectGeometry).fromGeometry(n), this.fromDirectGeometry(n.__directGeometry)
    },
    fromDirectGeometry: function(n) {
        var w = new Float32Array(n.vertices.length * 3),
            e, o, s, h, c, l, i, r, u, t, a, f, v, y, p;
        this.addAttribute("position", new THREE.BufferAttribute(w, 3).copyVector3sArray(n.vertices));
        n.normals.length > 0 && (e = new Float32Array(n.normals.length * 3), this.addAttribute("normal", new THREE.BufferAttribute(e, 3).copyVector3sArray(n.normals)));
        n.colors.length > 0 && (o = new Float32Array(n.colors.length * 3), this.addAttribute("color", new THREE.BufferAttribute(o, 3).copyColorsArray(n.colors)));
        n.uvs.length > 0 && (s = new Float32Array(n.uvs.length * 2), this.addAttribute("uv", new THREE.BufferAttribute(s, 2).copyVector2sArray(n.uvs)));
        n.uvs2.length > 0 && (h = new Float32Array(n.uvs2.length * 2), this.addAttribute("uv2", new THREE.BufferAttribute(h, 2).copyVector2sArray(n.uvs2)));
        n.indices.length > 0 && (c = n.vertices.length > 65535 ? Uint32Array : Uint16Array, l = new c(n.indices.length * 3), this.setIndex(new THREE.BufferAttribute(l, 1).copyIndicesArray(n.indices)));
        this.groups = n.groups;
        for (i in n.morphTargets) {
            for (r = [], u = n.morphTargets[i], t = 0, a = u.length; t < a; t++) f = u[t], v = new THREE.Float32Attribute(f.length * 3, 3), r.push(v.copyVector3sArray(f));
            this.morphAttributes[i] = r
        }
        return n.skinIndices.length > 0 && (y = new THREE.Float32Attribute(n.skinIndices.length * 4, 4), this.addAttribute("skinIndex", y.copyVector4sArray(n.skinIndices))), n.skinWeights.length > 0 && (p = new THREE.Float32Attribute(n.skinWeights.length * 4, 4), this.addAttribute("skinWeight", p.copyVector4sArray(n.skinWeights))), n.boundingSphere !== null && (this.boundingSphere = n.boundingSphere.clone()), n.boundingBox !== null && (this.boundingBox = n.boundingBox.clone()), this
    },
    computeBoundingBox: function() {
        this.boundingBox === null && (this.boundingBox = new THREE.Box3);
        var n = this.attributes.position.array;
        n !== undefined ? this.boundingBox.setFromArray(n) : this.boundingBox.makeEmpty();
        (isNaN(this.boundingBox.min.x) || isNaN(this.boundingBox.min.y) || isNaN(this.boundingBox.min.z)) && console.error('THREE.BufferGeometry.computeBoundingBox: Computed min/max have NaN values. The "position" attribute is likely to have NaN values.', this)
    },
    computeBoundingSphere: function() {
        var n = new THREE.Box3,
            t = new THREE.Vector3;
        return function() {
            var i, f, r, u, e;
            if (this.boundingSphere === null && (this.boundingSphere = new THREE.Sphere), i = this.attributes.position.array, i) {
                for (f = this.boundingSphere.center, n.setFromArray(i), n.center(f), r = 0, u = 0, e = i.length; u < e; u += 3) t.fromArray(i, u), r = Math.max(r, f.distanceToSquared(t));
                this.boundingSphere.radius = Math.sqrt(r);
                isNaN(this.boundingSphere.radius) && console.error('THREE.BufferGeometry.computeBoundingSphere(): Computed radius is NaN. The "position" attribute is likely to have NaN values.', this)
            }
        }
    }(),
    computeFaceNormals: function() {},
    computeVertexNormals: function() {
        var b = this.index,
            f = this.attributes,
            p = this.groups,
            r, w, c, y, k, n, u;
        if (f.position) {
            if (r = f.position.array, f.normal === undefined) this.addAttribute("normal", new THREE.BufferAttribute(new Float32Array(r.length), 3));
            else
                for (w = f.normal.array, n = 0, u = w.length; n < u; n++) w[n] = 0;
            var i = f.normal.array,
                o, s, h, l = new THREE.Vector3,
                e = new THREE.Vector3,
                a = new THREE.Vector3,
                t = new THREE.Vector3,
                v = new THREE.Vector3;
            if (b)
                for (c = b.array, p.length === 0 && this.addGroup(0, c.length), y = 0, k = p.length; y < k; ++y) {
                    var d = p[y],
                        g = d.start,
                        nt = d.count;
                    for (n = g, u = g + nt; n < u; n += 3) o = c[n + 0] * 3, s = c[n + 1] * 3, h = c[n + 2] * 3, l.fromArray(r, o), e.fromArray(r, s), a.fromArray(r, h), t.subVectors(a, e), v.subVectors(l, e), t.cross(v), i[o] += t.x, i[o + 1] += t.y, i[o + 2] += t.z, i[s] += t.x, i[s + 1] += t.y, i[s + 2] += t.z, i[h] += t.x, i[h + 1] += t.y, i[h + 2] += t.z
                } else
                    for (n = 0, u = r.length; n < u; n += 9) l.fromArray(r, n), e.fromArray(r, n + 3), a.fromArray(r, n + 6), t.subVectors(a, e), v.subVectors(l, e), t.cross(v), i[n] = t.x, i[n + 1] = t.y, i[n + 2] = t.z, i[n + 3] = t.x, i[n + 4] = t.y, i[n + 5] = t.z, i[n + 6] = t.x, i[n + 7] = t.y, i[n + 8] = t.z;
            this.normalizeNormals();
            f.normal.needsUpdate = !0
        }
    },
    merge: function(n, t) {
        var u, i, r, f;
        if (n instanceof THREE.BufferGeometry == !1) {
            console.error("THREE.BufferGeometry.merge(): geometry not an instance of THREE.BufferGeometry.", n);
            return
        }
        t === undefined && (t = 0);
        u = this.attributes;
        for (i in u)
            if (n.attributes[i] !== undefined) {
                var s = u[i],
                    h = s.array,
                    e = n.attributes[i],
                    o = e.array,
                    c = e.itemSize;
                for (r = 0, f = c * t; r < o.length; r++, f++) h[f] = o[r]
            }
        return this
    },
    normalizeNormals: function() {
        for (var t = this.attributes.normal.array, r, u, f, i, n = 0, e = t.length; n < e; n += 3) r = t[n], u = t[n + 1], f = t[n + 2], i = 1 / Math.sqrt(r * r + u * u + f * f), t[n] *= i, t[n + 1] *= i, t[n + 2] *= i
    },
    toNonIndexed: function() {
        var r, t, l, u;
        if (this.index === null) return console.warn("THREE.BufferGeometry.toNonIndexed(): Geometry is already non-indexed."), this;
        var f = new THREE.BufferGeometry,
            i = this.index.array,
            e = this.attributes;
        for (r in e) {
            var o = e[r],
                s = o.array,
                n = o.itemSize,
                h = new s.constructor(i.length * n),
                c = 0,
                a = 0;
            for (t = 0, l = i.length; t < l; t++)
                for (c = i[t] * n, u = 0; u < n; u++) h[a++] = s[c++];
            f.addAttribute(r, new THREE.BufferAttribute(h, n))
        }
        return f
    },
    toJSON: function() {
        var n = {
                metadata: {
                    version: 4.4,
                    type: "BufferGeometry",
                    generator: "BufferGeometry.toJSON"
                }
            },
            u, f, o, t, r, i, s, e;
        if (n.uuid = this.uuid, n.type = this.type, this.name !== "" && (n.name = this.name), this.parameters !== undefined) {
            u = this.parameters;
            for (t in u) u[t] !== undefined && (n[t] = u[t]);
            return n
        }
        n.data = {
            attributes: {}
        };
        f = this.index;
        f !== null && (i = Array.prototype.slice.call(f.array), n.data.index = {
            type: f.array.constructor.name,
            array: i
        });
        o = this.attributes;
        for (t in o) r = o[t], i = Array.prototype.slice.call(r.array), n.data.attributes[t] = {
            itemSize: r.itemSize,
            type: r.array.constructor.name,
            array: i,
            normalized: r.normalized
        };
        return s = this.groups, s.length > 0 && (n.data.groups = JSON.parse(JSON.stringify(s))), e = this.boundingSphere, e !== null && (n.data.boundingSphere = {
            center: e.center.toArray(),
            radius: e.radius
        }), n
    },
    clone: function() {
        return (new THREE.BufferGeometry).copy(this)
    },
    copy: function(n) {
        var e = n.index,
            r, u, o, f, t, s, i;
        e !== null && this.setIndex(e.clone());
        r = n.attributes;
        for (u in r) o = r[u], this.addAttribute(u, o.clone());
        for (f = n.groups, t = 0, s = f.length; t < s; t++) i = f[t], this.addGroup(i.start, i.count, i.materialIndex);
        return this
    },
    dispose: function() {
        this.dispatchEvent({
            type: "dispose"
        })
    }
};
THREE.EventDispatcher.prototype.apply(THREE.BufferGeometry.prototype);
THREE.BufferGeometry.MaxIndex = 65535;
THREE.InstancedBufferGeometry = function() {
    THREE.BufferGeometry.call(this);
    this.type = "InstancedBufferGeometry";
    this.maxInstancedCount = undefined
};
THREE.InstancedBufferGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.InstancedBufferGeometry.prototype.constructor = THREE.InstancedBufferGeometry;
THREE.InstancedBufferGeometry.prototype.addGroup = function(n, t, i) {
    this.groups.push({
        start: n,
        count: t,
        instances: i
    })
};
THREE.InstancedBufferGeometry.prototype.copy = function(n) {
    var e = n.index,
        r, u, o, f, t, s, i;
    e !== null && this.setIndex(e.clone());
    r = n.attributes;
    for (u in r) o = r[u], this.addAttribute(u, o.clone());
    for (f = n.groups, t = 0, s = f.length; t < s; t++) i = f[t], this.addGroup(i.start, i.count, i.instances);
    return this
};
THREE.EventDispatcher.prototype.apply(THREE.InstancedBufferGeometry.prototype);
THREE.Uniform = function(n) {
    typeof n == "string" && (console.warn("THREE.Uniform: Type parameter is no longer needed."), n = arguments[1]);
    this.value = n;
    this.dynamic = !1
};
THREE.Uniform.prototype = {
    constructor: THREE.Uniform,
    onUpdate: function(n) {
        return this.dynamic = !0, this.onUpdateCallback = n, this
    }
};
THREE.AnimationClip = function(n, t, i) {
    this.name = n || THREE.Math.generateUUID();
    this.tracks = i;
    this.duration = t !== undefined ? t : -1;
    this.duration < 0 && this.resetDuration();
    this.trim();
    this.optimize()
};
THREE.AnimationClip.prototype = {
    constructor: THREE.AnimationClip,
    resetDuration: function() {
        for (var u = this.tracks, t = 0, i, n = 0, r = u.length; n !== r; ++n) i = this.tracks[n], t = Math.max(t, i.times[i.times.length - 1]);
        this.duration = t
    },
    trim: function() {
        for (var n = 0; n < this.tracks.length; n++) this.tracks[n].trim(0, this.duration);
        return this
    },
    optimize: function() {
        for (var n = 0; n < this.tracks.length; n++) this.tracks[n].optimize();
        return this
    }
};
Object.assign(THREE.AnimationClip, {
    parse: function(n) {
        for (var i = [], r = n.tracks, f = 1 / (n.fps || 1), t = 0, u = r.length; t !== u; ++t) i.push(THREE.KeyframeTrack.parse(r[t]).scale(f));
        return new THREE.AnimationClip(n.name, n.duration, i)
    },
    toJSON: function(n) {
        for (var i = [], r = n.tracks, f = {
                name: n.name,
                duration: n.duration,
                tracks: i
            }, t = 0, u = r.length; t !== u; ++t) i.push(THREE.KeyframeTrack.toJSON(r[t]));
        return f
    },
    CreateFromMorphTargetSequence: function(n, t, i, r) {
        for (var o = t.length, h = [], u, e, s, f = 0; f < o; f++) u = [], e = [], u.push((f + o - 1) % o, f, (f + 1) % o), e.push(0, 1, 0), s = THREE.AnimationUtils.getKeyframeOrder(u), u = THREE.AnimationUtils.sortedArray(u, 1, s), e = THREE.AnimationUtils.sortedArray(e, 1, s), r || u[0] !== 0 || (u.push(o), e.push(e[0])), h.push(new THREE.NumberKeyframeTrack(".morphTargetInfluences[" + t[f].name + "]", u, e).scale(1 / i));
        return new THREE.AnimationClip(n, -1, h)
    },
    findByName: function(n, t) {
        for (var i = 0; i < n.length; i++)
            if (n[i].name === t) return n[i];
        return null
    },
    CreateClipsFromMorphTargetSequences: function(n, t, i) {
        for (var u = {}, s, e, o, h, r, f = 0, c = n.length; f < c; f++) s = n[f], e = s.name.match(/^([\w-]*?)([\d]+)$/), e && e.length > 1 && (r = e[1], o = u[r], o || (u[r] = o = []), o.push(s));
        h = [];
        for (r in u) h.push(THREE.AnimationClip.CreateFromMorphTargetSequence(r, u[r], t, i));
        return h
    },
    parseAnimation: function(n, t) {
        var e, i, o, r, c, l, a, u, v, s;
        if (!n) return console.error("  no animation in JSONLoader data"), null;
        var h = function(n, t, i, r, u) {
                if (i.length !== 0) {
                    var f = [],
                        e = [];
                    THREE.AnimationUtils.flattenJSON(i, f, e, r);
                    f.length !== 0 && u.push(new n(t, f, e))
                }
            },
            f = [],
            w = n.name || "default",
            y = n.length || -1,
            b = n.fps || 30,
            p = n.hierarchy || [];
        for (e = 0; e < p.length; e++)
            if (i = p[e].keys, i && i.length != 0)
                if (i[0].morphTargets) {
                    for (o = {}, r = 0; r < i.length; r++)
                        if (i[r].morphTargets)
                            for (u = 0; u < i[r].morphTargets.length; u++) o[i[r].morphTargets[u]] = -1;
                    for (c in o) {
                        for (l = [], a = [], u = 0; u !== i[r].morphTargets.length; ++u) v = i[r], l.push(v.time), a.push(v.morphTarget === c ? 1 : 0);
                        f.push(new THREE.NumberKeyframeTrack(".morphTargetInfluence[" + c + "]", l, a))
                    }
                    y = o.length * (b || 1)
                } else s = ".bones[" + t[e].name + "]", h(THREE.VectorKeyframeTrack, s + ".position", i, "pos", f), h(THREE.QuaternionKeyframeTrack, s + ".quaternion", i, "rot", f), h(THREE.VectorKeyframeTrack, s + ".scale", i, "scl", f);
        return f.length === 0 ? null : new THREE.AnimationClip(w, y, f)
    }
});
THREE.AnimationMixer = function(n) {
    this._root = n;
    this._initMemoryManager();
    this._accuIndex = 0;
    this.time = 0;
    this.timeScale = 1
};
THREE.AnimationMixer.prototype = {
    constructor: THREE.AnimationMixer,
    clipAction: function(n, t) {
        var h = t || this._root,
            s = h.uuid,
            i = typeof n == "string" ? n : n.name,
            r = n !== i ? n : null,
            f = this._actionsByClip[i],
            e, o, u;
        if (f !== undefined) {
            if (o = f.actionByRoot[s], o !== undefined) return o;
            if (e = f.knownActions[0], r = e._clip, n !== i && n !== r) throw new Error("Different clips with the same name detected!");
        }
        return r === null ? null : (u = new THREE.AnimationMixer._Action(this, r, t), this._bindAction(u, e), this._addInactiveAction(u, i, s), u)
    },
    existingAction: function(n, t) {
        var r = t || this._root,
            u = r.uuid,
            f = typeof n == "string" ? n : n.name,
            i = this._actionsByClip[f];
        return i !== undefined ? i.actionByRoot[u] || null : null
    },
    stopAllAction: function() {
        var t = this._actions,
            i = this._nActiveActions,
            r = this._bindings,
            u = this._nActiveBindings,
            n;
        for (this._nActiveActions = 0, this._nActiveBindings = 0, n = 0; n !== i; ++n) t[n].reset();
        for (n = 0; n !== u; ++n) r[n].useCount = 0;
        return this
    },
    update: function(n) {
        var i, u, f, t;
        n *= this.timeScale;
        var e = this._actions,
            o = this._nActiveActions,
            s = this.time += n,
            h = Math.sign(n),
            r = this._accuIndex ^= 1;
        for (t = 0; t !== o; ++t) i = e[t], i.enabled && i._update(s, n, h, r);
        for (u = this._bindings, f = this._nActiveBindings, t = 0; t !== f; ++t) u[t].apply(r);
        return this
    },
    getRoot: function() {
        return this._root
    },
    uncacheClip: function(n) {
        var i = this._actions,
            o = n.name,
            s = this._actionsByClip,
            h = s[o],
            u, r, c, t, f, e;
        if (h !== undefined) {
            for (u = h.knownActions, r = 0, c = u.length; r !== c; ++r) t = u[r], this._deactivateAction(t), f = t._cacheIndex, e = i[i.length - 1], t._cacheIndex = null, t._byClipCacheIndex = null, e._cacheIndex = f, i[f] = e, i.pop(), this._removeInactiveBindingsForAction(t);
            delete s[o]
        }
    },
    uncacheRoot: function(n) {
        var u = n.uuid,
            f = this._actionsByClip,
            e, o, t, s, i, h, r;
        for (e in f) o = f[e].actionByRoot, t = o[u], t !== undefined && (this._deactivateAction(t), this._removeInactiveAction(t));
        if (s = this._bindingsByRootAndName, i = s[u], i !== undefined)
            for (h in i) r = i[h], r.restoreOriginalState(), this._removeInactiveBinding(r)
    },
    uncacheAction: function(n, t) {
        var i = this.existingAction(n, t);
        i !== null && (this._deactivateAction(i), this._removeInactiveAction(i))
    }
};
THREE.EventDispatcher.prototype.apply(THREE.AnimationMixer.prototype);
THREE.AnimationMixer._Action = function(n, t, i) {
    var r, f;
    this._mixer = n;
    this._clip = t;
    this._localRoot = i || null;
    var e = t.tracks,
        u = e.length,
        o = new Array(u),
        s = {
            endingStart: THREE.ZeroCurvatureEnding,
            endingEnd: THREE.ZeroCurvatureEnding
        };
    for (r = 0; r !== u; ++r) f = e[r].createInterpolant(null), o[r] = f, f.settings = s;
    this._interpolantSettings = s;
    this._interpolants = o;
    this._propertyBindings = new Array(u);
    this._cacheIndex = null;
    this._byClipCacheIndex = null;
    this._timeScaleInterpolant = null;
    this._weightInterpolant = null;
    this.loop = THREE.LoopRepeat;
    this._loopCount = -1;
    this._startTime = null;
    this.time = 0;
    this.timeScale = 1;
    this._effectiveTimeScale = 1;
    this.weight = 1;
    this._effectiveWeight = 1;
    this.repetitions = Infinity;
    this.paused = !1;
    this.enabled = !0;
    this.clampWhenFinished = !1;
    this.zeroSlopeAtStart = !0;
    this.zeroSlopeAtEnd = !0
};
THREE.AnimationMixer._Action.prototype = {
    constructor: THREE.AnimationMixer._Action,
    play: function() {
        return this._mixer._activateAction(this), this
    },
    stop: function() {
        return this._mixer._deactivateAction(this), this.reset()
    },
    reset: function() {
        return this.paused = !1, this.enabled = !0, this.time = 0, this._loopCount = -1, this._startTime = null, this.stopFading().stopWarping()
    },
    isRunning: function() {
        var n = this._startTime;
        return this.enabled && !this.paused && this.timeScale !== 0 && this._startTime === null && this._mixer._isActiveAction(this)
    },
    isScheduled: function() {
        return this._mixer._isActiveAction(this)
    },
    startAt: function(n) {
        return this._startTime = n, this
    },
    setLoop: function(n, t) {
        return this.loop = n, this.repetitions = t, this
    },
    setEffectiveWeight: function(n) {
        return this.weight = n, this._effectiveWeight = this.enabled ? n : 0, this.stopFading()
    },
    getEffectiveWeight: function() {
        return this._effectiveWeight
    },
    fadeIn: function(n) {
        return this._scheduleFading(n, 0, 1)
    },
    fadeOut: function(n) {
        return this._scheduleFading(n, 1, 0)
    },
    crossFadeFrom: function(n, t, i) {
        var o = this._mixer;
        if (n.fadeOut(t), this.fadeIn(t), i) {
            var r = this._clip.duration,
                u = n._clip.duration,
                f = u / r,
                e = r / u;
            n.warp(1, f, t);
            this.warp(e, 1, t)
        }
        return this
    },
    crossFadeTo: function(n, t, i) {
        return n.crossFadeFrom(this, t, i)
    },
    stopFading: function() {
        var n = this._weightInterpolant;
        return n !== null && (this._weightInterpolant = null, this._mixer._takeBackControlInterpolant(n)), this
    },
    setEffectiveTimeScale: function(n) {
        return this.timeScale = n, this._effectiveTimeScale = this.paused ? 0 : n, this.stopWarping()
    },
    getEffectiveTimeScale: function() {
        return this._effectiveTimeScale
    },
    setDuration: function(n) {
        return this.timeScale = this._clip.duration / n, this.stopWarping()
    },
    syncWith: function(n) {
        return this.time = n.time, this.timeScale = n.timeScale, this.stopWarping()
    },
    halt: function(n) {
        return this.warp(this._currentTimeScale, 0, n)
    },
    warp: function(n, t, i) {
        var e = this._mixer,
            o = e.time,
            r = this._timeScaleInterpolant,
            s = this.timeScale,
            u, f;
        return r === null && (r = e._lendControlInterpolant(), this._timeScaleInterpolant = r), u = r.parameterPositions, f = r.sampleValues, u[0] = o, u[1] = o + i, f[0] = n / s, f[1] = t / s, this
    },
    stopWarping: function() {
        var n = this._timeScaleInterpolant;
        return n !== null && (this._timeScaleInterpolant = null, this._mixer._takeBackControlInterpolant(n)), this
    },
    getMixer: function() {
        return this._mixer
    },
    getClip: function() {
        return this._clip
    },
    getRoot: function() {
        return this._localRoot || this._mixer._root
    },
    _update: function(n, t, i, r) {
        var s = this._startTime,
            f, h, e, o, c, u, l;
        if (s !== null) {
            if (f = (n - s) * i, f < 0 || i === 0) return;
            this._startTime = null;
            t = i * f
        }
        if (t *= this._updateTimeScale(n), h = this._updateTime(t), e = this._updateWeight(n), e > 0)
            for (o = this._interpolants, c = this._propertyBindings, u = 0, l = o.length; u !== l; ++u) o[u].evaluate(h), c[u].accumulate(r, e)
    },
    _updateWeight: function(n) {
        var t = 0,
            i, r;
        return this.enabled && (t = this.weight, i = this._weightInterpolant, i !== null && (r = i.evaluate(n)[0], t *= r, n > i.parameterPositions[1] && (this.stopFading(), r === 0 && (this.enabled = !1)))), this._effectiveWeight = t, t
    },
    _updateTimeScale: function(n) {
        var t = 0,
            i, r;
        return this.paused || (t = this.timeScale, i = this._timeScaleInterpolant, i !== null && (r = i.evaluate(n)[0], t *= r, n > i.parameterPositions[1] && (this.stopWarping(), t === 0 ? this.pause = !0 : this.timeScale = t))), this._effectiveTimeScale = t, t
    },
    _updateTime: function(n) {
        var t = this.time + n,
            f, e, o;
        if (n === 0) return t;
        var i = this._clip.duration,
            s = this.loop,
            r = this._loopCount,
            u = !1;
        switch (s) {
            case THREE.LoopOnce:
                if (r === -1 && (this.loopCount = 0, this._setEndings(!0, !0, !1)), t >= i) t = i;
                else if (t < 0) t = 0;
                else break;
                this.clampWhenFinished ? this.pause = !0 : this.enabled = !1;
                this._mixer.dispatchEvent({
                    type: "finished",
                    action: this,
                    direction: n < 0 ? -1 : 1
                });
                break;
            case THREE.LoopPingPong:
                u = !0;
            case THREE.LoopRepeat:
                if (r === -1 && (n > 0 ? (r = 0, this._setEndings(!0, this.repetitions === 0, u)) : this._setEndings(this.repetitions === 0, !0, u)), t >= i || t < 0) {
                    if (f = Math.floor(t / i), t -= i * f, r += Math.abs(f), e = this.repetitions - r, e < 0) {
                        this.clampWhenFinished ? this.paused = !0 : this.enabled = !1;
                        t = n > 0 ? i : 0;
                        this._mixer.dispatchEvent({
                            type: "finished",
                            action: this,
                            direction: n > 0 ? 1 : -1
                        });
                        break
                    } else e === 0 ? (o = n < 0, this._setEndings(o, !o, u)) : this._setEndings(!1, !1, u);
                    this._loopCount = r;
                    this._mixer.dispatchEvent({
                        type: "loop",
                        action: this,
                        loopDelta: f
                    })
                }
                if (s === THREE.LoopPingPong && (r & 1) == 1) return this.time = t, i - t
        }
        return this.time = t, t
    },
    _setEndings: function(n, t, i) {
        var r = this._interpolantSettings;
        i ? (r.endingStart = THREE.ZeroSlopeEnding, r.endingEnd = THREE.ZeroSlopeEnding) : (r.endingStart = n ? this.zeroSlopeAtStart ? THREE.ZeroSlopeEnding : THREE.ZeroCurvatureEnding : THREE.WrapAroundEnding, r.endingEnd = t ? this.zeroSlopeAtEnd ? THREE.ZeroSlopeEnding : THREE.ZeroCurvatureEnding : THREE.WrapAroundEnding)
    },
    _scheduleFading: function(n, t, i) {
        var e = this._mixer,
            o = e.time,
            r = this._weightInterpolant,
            u, f;
        return r === null && (r = e._lendControlInterpolant(), this._weightInterpolant = r), u = r.parameterPositions, f = r.sampleValues, u[0] = o, f[0] = t, u[1] = o + n, f[1] = i, this
    }
};
Object.assign(THREE.AnimationMixer.prototype, {
    _bindAction: function(n, t) {
        var h = n._localRoot || this._root,
            c = n._clip.tracks,
            v = c.length,
            o = n._propertyBindings,
            y = n._interpolants,
            u = h.uuid,
            l = this._bindingsByRootAndName,
            f = l[u],
            r, a;
        for (f === undefined && (f = {}, l[u] = f), r = 0; r !== v; ++r) {
            var s = c[r],
                e = s.name,
                i = f[e];
            if (i !== undefined) o[r] = i;
            else {
                if (i = o[r], i !== undefined) {
                    i._cacheIndex === null && (++i.referenceCount, this._addInactiveBinding(i, u, e));
                    continue
                }
                a = t && t._propertyBindings[r].binding.parsedPath;
                i = new THREE.PropertyMixer(THREE.PropertyBinding.create(h, e, a), s.ValueTypeName, s.getValueSize());
                ++i.referenceCount;
                this._addInactiveBinding(i, u, e);
                o[r] = i
            }
            y[r].resultBuffer = i.buffer
        }
    },
    _activateAction: function(n) {
        var r, t, e, i;
        if (!this._isActiveAction(n)) {
            if (n._cacheIndex === null) {
                var o = (n._localRoot || this._root).uuid,
                    u = n._clip.name,
                    f = this._actionsByClip[u];
                this._bindAction(n, f && f.knownActions[0]);
                this._addInactiveAction(n, u, o)
            }
            for (r = n._propertyBindings, t = 0, e = r.length; t !== e; ++t) i = r[t], i.useCount++ == 0 && (this._lendBinding(i), i.saveOriginalState());
            this._lendAction(n)
        }
    },
    _deactivateAction: function(n) {
        var r, t, u, i;
        if (this._isActiveAction(n)) {
            for (r = n._propertyBindings, t = 0, u = r.length; t !== u; ++t) i = r[t], --i.useCount == 0 && (i.restoreOriginalState(), this._takeBackBinding(i));
            this._takeBackAction(n)
        }
    },
    _initMemoryManager: function() {
        this._actions = [];
        this._nActiveActions = 0;
        this._actionsByClip = {};
        this._bindings = [];
        this._nActiveBindings = 0;
        this._bindingsByRootAndName = {};
        this._controlInterpolants = [];
        this._nActiveControlInterpolants = 0;
        var n = this;
        this.stats = {
            actions: {
                get total() {
                    return n._actions.length
                },
                get inUse() {
                    return n._nActiveActions
                }
            },
            bindings: {
                get total() {
                    return n._bindings.length
                },
                get inUse() {
                    return n._nActiveBindings
                }
            },
            controlInterpolants: {
                get total() {
                    return n._controlInterpolants.length
                },
                get inUse() {
                    return n._nActiveControlInterpolants
                }
            }
        }
    },
    _isActiveAction: function(n) {
        var t = n._cacheIndex;
        return t !== null && t < this._nActiveActions
    },
    _addInactiveAction: function(n, t, i) {
        var f = this._actions,
            e = this._actionsByClip,
            r = e[t],
            u;
        r === undefined ? (r = {
            knownActions: [n],
            actionByRoot: {}
        }, n._byClipCacheIndex = 0, e[t] = r) : (u = r.knownActions, n._byClipCacheIndex = u.length, u.push(n));
        n._cacheIndex = f.length;
        f.push(n);
        r.actionByRoot[i] = n
    },
    _removeInactiveAction: function(n) {
        var t = this._actions,
            r = t[t.length - 1],
            u = n._cacheIndex,
            c, l;
        r._cacheIndex = u;
        t[u] = r;
        t.pop();
        n._cacheIndex = null;
        var f = n._clip.name,
            e = this._actionsByClip,
            o = e[f],
            i = o.knownActions,
            s = i[i.length - 1],
            h = n._byClipCacheIndex;
        s._byClipCacheIndex = h;
        i[h] = s;
        i.pop();
        n._byClipCacheIndex = null;
        c = o.actionByRoot;
        l = (t._localRoot || this._root).uuid;
        delete c[l];
        i.length === 0 && delete e[f];
        this._removeInactiveBindingsForAction(n)
    },
    _removeInactiveBindingsForAction: function(n) {
        for (var r = n._propertyBindings, i, t = 0, u = r.length; t !== u; ++t) i = r[t], --i.referenceCount == 0 && this._removeInactiveBinding(i)
    },
    _lendAction: function(n) {
        var t = this._actions,
            r = n._cacheIndex,
            i = this._nActiveActions++,
            u = t[i];
        n._cacheIndex = i;
        t[i] = n;
        u._cacheIndex = r;
        t[r] = u
    },
    _takeBackAction: function(n) {
        var t = this._actions,
            r = n._cacheIndex,
            i = --this._nActiveActions,
            u = t[i];
        n._cacheIndex = i;
        t[i] = n;
        u._cacheIndex = r;
        t[r] = u
    },
    _addInactiveBinding: function(n, t, i) {
        var u = this._bindingsByRootAndName,
            r = u[t],
            f = this._bindings;
        r === undefined && (r = {}, u[t] = r);
        r[i] = n;
        n._cacheIndex = f.length;
        f.push(n)
    },
    _removeInactiveBinding: function(n) {
        var t = this._bindings,
            i = n.binding,
            r = i.rootNode.uuid,
            s = i.path,
            u = this._bindingsByRootAndName,
            f = u[r],
            e = t[t.length - 1],
            o = n._cacheIndex,
            h;
        e._cacheIndex = o;
        t[o] = e;
        t.pop();
        delete f[s];
        n: {
            for (h in f) break n;delete u[r]
        }
    },
    _lendBinding: function(n) {
        var t = this._bindings,
            r = n._cacheIndex,
            i = this._nActiveBindings++,
            u = t[i];
        n._cacheIndex = i;
        t[i] = n;
        u._cacheIndex = r;
        t[r] = u
    },
    _takeBackBinding: function(n) {
        var t = this._bindings,
            r = n._cacheIndex,
            i = --this._nActiveBindings,
            u = t[i];
        n._cacheIndex = i;
        t[i] = n;
        u._cacheIndex = r;
        t[r] = u
    },
    _lendControlInterpolant: function() {
        var i = this._controlInterpolants,
            t = this._nActiveControlInterpolants++,
            n = i[t];
        return n === undefined && (n = new THREE.LinearInterpolant(new Float32Array(2), new Float32Array(2), 1, this._controlInterpolantsResultBuffer), n.__cacheIndex = t, i[t] = n), n
    },
    _takeBackControlInterpolant: function(n) {
        var t = this._controlInterpolants,
            r = n.__cacheIndex,
            i = --this._nActiveControlInterpolants,
            u = t[i];
        n.__cacheIndex = i;
        t[i] = n;
        u.__cacheIndex = r;
        t[r] = u
    },
    _controlInterpolantsResultBuffer: new Float32Array(1)
});
THREE.AnimationObjectGroup = function() {
    var i, n, r, t;
    for (this.uuid = THREE.Math.generateUUID(), this._objects = Array.prototype.slice.call(arguments), this.nCachedObjects_ = 0, i = {}, this._indicesByUUID = i, n = 0, r = arguments.length; n !== r; ++n) i[arguments[n].uuid] = n;
    this._paths = [];
    this._parsedPaths = [];
    this._bindings = [];
    this._bindingsIndicesByPath = {};
    t = this;
    this.stats = {
        objects: {
            get total() {
                return t._objects.length
            },
            get inUse() {
                return this.total - t.nCachedObjects_
            }
        },
        get bindingsPerObject() {
            return t._bindings.length
        }
    }
};
THREE.AnimationObjectGroup.prototype = {
    constructor: THREE.AnimationObjectGroup,
    add: function() {
        for (var i = this._objects, k = i.length, h = this.nCachedObjects_, e = this._indicesByUUID, v = this._paths, y = this._parsedPaths, c = this._bindings, p = c.length, n, u, o = 0, w = arguments.length; o !== w; ++o) {
            var r = arguments[o],
                l = r.uuid,
                t = e[l];
            if (t === undefined)
                for (t = k++, e[l] = t, i.push(r), n = 0, u = p; n !== u; ++n) c[n].push(new THREE.PropertyBinding(r, v[n], y[n]));
            else if (t < h) {
                var d = i[t],
                    f = --h,
                    b = i[f];
                for (e[b.uuid] = t, i[t] = b, e[l] = f, i[f] = r, n = 0, u = p; n !== u; ++n) {
                    var s = c[n],
                        g = s[f],
                        a = s[t];
                    s[t] = g;
                    a === undefined && (a = new THREE.PropertyBinding(r, v[n], y[n]));
                    s[f] = a
                }
            } else i[t] !== d && console.error("Different objects with the same UUID detected. Clean the caches or recreate your infrastructure when reloading scenes...")
        }
        this.nCachedObjects_ = h
    },
    remove: function() {
        for (var i = this._objects, b = i.length, e = this.nCachedObjects_, o = this._indicesByUUID, h = this._bindings, y = h.length, t, s, u, v, r = 0, c = arguments.length; r !== c; ++r) {
            var l = arguments[r],
                a = l.uuid,
                n = o[a];
            if (n !== undefined && n >= e)
                for (t = e++, s = i[t], o[s.uuid] = n, i[n] = s, o[a] = t, i[t] = l, u = 0, v = y; u !== v; ++u) {
                    var f = h[u],
                        p = f[t],
                        w = f[n];
                    f[n] = p;
                    f[t] = w
                }
        }
        this.nCachedObjects_ = e
    },
    uncache: function() {
        for (var t = this._objects, a = t.length, c = this.nCachedObjects_, e = this._indicesByUUID, l = this._bindings, v = l.length, u, f, i, s, n, h = 0, y = arguments.length; h !== y; ++h) {
            var b = arguments[h],
                p = b.uuid,
                r = e[p];
            if (r !== undefined)
                if (delete e[p], r < c) {
                    var o = --c,
                        w = t[o],
                        u = --a,
                        f = t[u];
                    for (e[w.uuid] = r, t[r] = w, e[f.uuid] = o, t[o] = f, t.pop(), i = 0, s = v; i !== s; ++i) {
                        var n = l[i],
                            k = n[o],
                            d = n[u];
                        n[r] = k;
                        n[o] = d;
                        n.pop()
                    }
                } else
                    for (u = --a, f = t[u], e[f.uuid] = r, t[r] = f, t.pop(), i = 0, s = v; i !== s; ++i) n = l[i], n[r] = n[u], n.pop()
        }
        this.nCachedObjects_ = c
    },
    subscribe_: function(n, t) {
        var o = this._bindingsIndicesByPath,
            r = o[n],
            u = this._bindings,
            i, s, h;
        if (r !== undefined) return u[r];
        var c = this._paths,
            l = this._parsedPaths,
            f = this._objects,
            a = f.length,
            v = this.nCachedObjects_,
            e = new Array(a);
        for (r = u.length, o[n] = r, c.push(n), l.push(t), u.push(e), i = v, s = f.length; i !== s; ++i) h = f[i], e[i] = new THREE.PropertyBinding(h, n, t);
        return e
    },
    unsubscribe_: function(n) {
        var e = this._bindingsIndicesByPath,
            t = e[n];
        if (t !== undefined) {
            var u = this._paths,
                f = this._parsedPaths,
                i = this._bindings,
                r = i.length - 1,
                o = i[r],
                s = n[r];
            e[s] = t;
            i[t] = o;
            i.pop();
            f[t] = f[r];
            f.pop();
            u[t] = u[r];
            u.pop()
        }
    }
};
THREE.AnimationUtils = {
    arraySlice: function(n, t, i) {
        return THREE.AnimationUtils.isTypedArray(n) ? new n.constructor(n.subarray(t, i)) : n.slice(t, i)
    },
    convertArray: function(n, t, i) {
        return !n || !i && n.constructor === t ? n : typeof t.BYTES_PER_ELEMENT == "number" ? new t(n) : Array.prototype.slice.call(n)
    },
    isTypedArray: function(n) {
        return ArrayBuffer.isView(n) && !(n instanceof DataView)
    },
    getKeyframeOrder: function(n) {
        function u(t, i) {
            return n[t] - n[i]
        }
        for (var r = n.length, i = new Array(r), t = 0; t !== r; ++t) i[t] = t;
        return i.sort(u), i
    },
    sortedArray: function(n, t, i) {
        for (var e = n.length, o = new n.constructor(e), s, r, u = 0, f = 0; f !== e; ++u)
            for (s = i[u] * t, r = 0; r !== t; ++r) o[f++] = n[s + r];
        return o
    },
    flattenJSON: function(n, t, i, r) {
        for (var e = 1, u = n[0], f; u !== undefined && u[r] === undefined;) u = n[e++];
        if (u !== undefined && (f = u[r], f !== undefined))
            if (Array.isArray(f)) {
                do f = u[r], f !== undefined && (t.push(u.time), i.push.apply(i, f)), u = n[e++]; while (u !== undefined)
            } else if (f.toArray !== undefined) {
            do f = u[r], f !== undefined && (t.push(u.time), f.toArray(i, i.length)), u = n[e++]; while (u !== undefined)
        } else
            do f = u[r], f !== undefined && (t.push(u.time), i.push(f)), u = n[e++]; while (u !== undefined)
    }
};
THREE.KeyframeTrack = function(n, t, i, r) {
    if (n === undefined) throw new Error("track name is undefined");
    if (t === undefined || t.length === 0) throw new Error("no keyframes in track named " + n);
    this.name = n;
    this.times = THREE.AnimationUtils.convertArray(t, this.TimeBufferType);
    this.values = THREE.AnimationUtils.convertArray(i, this.ValueBufferType);
    this.setInterpolation(r || this.DefaultInterpolation);
    this.validate();
    this.optimize()
};
THREE.KeyframeTrack.prototype = {
    constructor: THREE.KeyframeTrack,
    TimeBufferType: Float32Array,
    ValueBufferType: Float32Array,
    DefaultInterpolation: THREE.InterpolateLinear,
    InterpolantFactoryMethodDiscrete: function(n) {
        return new THREE.DiscreteInterpolant(this.times, this.values, this.getValueSize(), n)
    },
    InterpolantFactoryMethodLinear: function(n) {
        return new THREE.LinearInterpolant(this.times, this.values, this.getValueSize(), n)
    },
    InterpolantFactoryMethodSmooth: function(n) {
        return new THREE.CubicInterpolant(this.times, this.values, this.getValueSize(), n)
    },
    setInterpolation: function(n) {
        var t = undefined,
            i;
        switch (n) {
            case THREE.InterpolateDiscrete:
                t = this.InterpolantFactoryMethodDiscrete;
                break;
            case THREE.InterpolateLinear:
                t = this.InterpolantFactoryMethodLinear;
                break;
            case THREE.InterpolateSmooth:
                t = this.InterpolantFactoryMethodSmooth
        }
        if (t === undefined) {
            if (i = "unsupported interpolation for " + this.ValueTypeName + " keyframe track named " + this.name, this.createInterpolant === undefined)
                if (n !== this.DefaultInterpolation) this.setInterpolation(this.DefaultInterpolation);
                else throw new Error(i);
            console.warn(i);
            return
        }
        this.createInterpolant = t
    },
    getInterpolation: function() {
        switch (this.createInterpolant) {
            case this.InterpolantFactoryMethodDiscrete:
                return THREE.InterpolateDiscrete;
            case this.InterpolantFactoryMethodLinear:
                return THREE.InterpolateLinear;
            case this.InterpolantFactoryMethodSmooth:
                return THREE.InterpolateSmooth
        }
    },
    getValueSize: function() {
        return this.values.length / this.times.length
    },
    shift: function(n) {
        var i, t, r;
        if (n !== 0)
            for (i = this.times, t = 0, r = i.length; t !== r; ++t) i[t] += n;
        return this
    },
    scale: function(n) {
        var i, t, r;
        if (n !== 1)
            for (i = this.times, t = 0, r = i.length; t !== r; ++t) i[t] *= n;
        return this
    },
    trim: function(n, t) {
        for (var u = this.times, f = u.length, r = 0, i = f - 1, e; r !== f && u[r] < n;) ++r;
        while (i !== -1 && u[i] > t) --i;
        return ++i, (r !== 0 || i !== f) && (r >= i && (i = Math.max(i, 1), r = i - 1), e = this.getValueSize(), this.times = THREE.AnimationUtils.arraySlice(u, r, i), this.values = THREE.AnimationUtils.arraySlice(this.values, r * e, i * e)), this
    },
    validate: function() {
        var i = !0,
            e = this.getValueSize(),
            r, t, n, h, f;
        e - Math.floor(e) != 0 && (console.error("invalid value size in track", this), i = !1);
        var o = this.times,
            u = this.values,
            s = o.length;
        for (s === 0 && (console.error("track is empty", this), i = !1), r = null, n = 0; n !== s; n++) {
            if (t = o[n], typeof t == "number" && isNaN(t)) {
                console.error("time is not a valid number", this, n, t);
                i = !1;
                break
            }
            if (r !== null && r > t) {
                console.error("out of order keys", this, n, t, r);
                i = !1;
                break
            }
            r = t
        }
        if (u !== undefined && THREE.AnimationUtils.isTypedArray(u))
            for (n = 0, h = u.length; n !== h; ++n)
                if (f = u[n], isNaN(f)) {
                    console.error("value is not a valid number", this, n, f);
                    i = !1;
                    break
                }
        return i
    },
    optimize: function() {
        for (var r = this.times, f = this.values, i = this.getValueSize(), u = 1, s, l, a, n, t = 1, h = r.length - 1; t <= h; ++t) {
            var c = !1,
                e = r[t],
                v = r[t + 1];
            if (e !== v && (t !== 1 || e !== e[0])) {
                var o = t * i,
                    y = o - i,
                    p = o + i;
                for (n = 0; n !== i; ++n)
                    if (s = f[o + n], s !== f[y + n] || s !== f[p + n]) {
                        c = !0;
                        break
                    }
            }
            if (c) {
                if (t !== u)
                    for (r[u] = r[t], l = t * i, a = u * i, n = 0; n !== i; ++n) f[a + n] = f[l + n];
                ++u
            }
        }
        return u !== r.length && (this.times = THREE.AnimationUtils.arraySlice(r, 0, u), this.values = THREE.AnimationUtils.arraySlice(f, 0, u * i)), this
    }
};
Object.assign(THREE.KeyframeTrack, {
    parse: function(n) {
        var t, i, r;
        if (n.type === undefined) throw new Error("track type undefined, can not parse");
        return t = THREE.KeyframeTrack._getTrackTypeForValueTypeName(n.type), n.times === undefined && (console.warn("legacy JSON format detected, converting"), i = [], r = [], THREE.AnimationUtils.flattenJSON(n.keys, i, r, "value"), n.times = i, n.values = r), t.parse !== undefined ? t.parse(n) : new t(n.name, n.times, n.values, n.interpolation)
    },
    toJSON: function(n) {
        var r = n.constructor,
            t, i;
        return r.toJSON !== undefined ? t = r.toJSON(n) : (t = {
            name: n.name,
            times: THREE.AnimationUtils.convertArray(n.times, Array),
            values: THREE.AnimationUtils.convertArray(n.values, Array)
        }, i = n.getInterpolation(), i !== n.DefaultInterpolation && (t.interpolation = i)), t.type = n.ValueTypeName, t
    },
    _getTrackTypeForValueTypeName: function(n) {
        switch (n.toLowerCase()) {
            case "scalar":
            case "double":
            case "float":
            case "number":
            case "integer":
                return THREE.NumberKeyframeTrack;
            case "vector":
            case "vector2":
            case "vector3":
            case "vector4":
                return THREE.VectorKeyframeTrack;
            case "color":
                return THREE.ColorKeyframeTrack;
            case "quaternion":
                return THREE.QuaternionKeyframeTrack;
            case "bool":
            case "boolean":
                return THREE.BooleanKeyframeTrack;
            case "string":
                return THREE.StringKeyframeTrack
        }
        throw new Error("Unsupported typeName: " + n);
    }
});
THREE.PropertyBinding = function(n, t, i) {
    this.path = t;
    this.parsedPath = i || THREE.PropertyBinding.parseTrackName(t);
    this.node = THREE.PropertyBinding.findNode(n, this.parsedPath.nodeName) || n;
    this.rootNode = n
};
THREE.PropertyBinding.prototype = {
    constructor: THREE.PropertyBinding,
    getValue: function(n, t) {
        this.bind();
        this.getValue(n, t)
    },
    setValue: function(n, t) {
        this.bind();
        this.setValue(n, t)
    },
    bind: function() {
        var n = this.node,
            r = this.parsedPath,
            e = r.objectName,
            o = r.propertyName,
            s = r.propertyIndex,
            u, i, c, h, f, t;
        if (n || (n = THREE.PropertyBinding.findNode(this.rootNode, r.nodeName) || this.rootNode, this.node = n), this.getValue = this._getValue_unavailable, this.setValue = this._setValue_unavailable, !n) {
            console.error("  trying to update node for track: " + this.path + " but it wasn't found.");
            return
        }
        if (e) {
            u = r.objectIndex;
            switch (e) {
                case "materials":
                    if (!n.material) {
                        console.error("  can not bind to material as node does not have a material", this);
                        return
                    }
                    if (!n.material.materials) {
                        console.error("  can not bind to material.materials as node.material does not have a materials array", this);
                        return
                    }
                    n = n.material.materials;
                    break;
                case "bones":
                    if (!n.skeleton) {
                        console.error("  can not bind to bones as node does not have a skeleton", this);
                        return
                    }
                    for (n = n.skeleton.bones, t = 0; t < n.length; t++)
                        if (n[t].name === u) {
                            u = t;
                            break
                        }
                    break;
                default:
                    if (n[e] === undefined) {
                        console.error("  can not bind to objectName of node, undefined", this);
                        return
                    }
                    n = n[e]
            }
            if (u !== undefined) {
                if (n[u] === undefined) {
                    console.error("  trying to bind to objectIndex of objectName, but is undefined:", this, n);
                    return
                }
                n = n[u]
            }
        }
        if (i = n[o], !i) {
            c = r.nodeName;
            console.error("  trying to update property for track: " + c + "." + o + " but it wasn't found.", n);
            return
        }
        if (h = this.Versioning.None, n.needsUpdate !== undefined ? (h = this.Versioning.NeedsUpdate, this.targetObject = n) : n.matrixWorldNeedsUpdate !== undefined && (h = this.Versioning.MatrixWorldNeedsUpdate, this.targetObject = n), f = this.BindingType.Direct, s !== undefined) {
            if (o === "morphTargetInfluences") {
                if (!n.geometry) {
                    console.error("  can not bind to morphTargetInfluences becasuse node does not have a geometry", this);
                    return
                }
                if (!n.geometry.morphTargets) {
                    console.error("  can not bind to morphTargetInfluences becasuse node does not have a geometry.morphTargets", this);
                    return
                }
                for (t = 0; t < this.node.geometry.morphTargets.length; t++)
                    if (n.geometry.morphTargets[t].name === s) {
                        s = t;
                        break
                    }
            }
            f = this.BindingType.ArrayElement;
            this.resolvedProperty = i;
            this.propertyIndex = s
        } else i.fromArray !== undefined && i.toArray !== undefined ? (f = this.BindingType.HasFromToArray, this.resolvedProperty = i) : i.length !== undefined ? (f = this.BindingType.EntireArray, this.resolvedProperty = i) : this.propertyName = o;
        this.getValue = this.GetterByBindingType[f];
        this.setValue = this.SetterByBindingTypeAndVersioning[f][h]
    },
    unbind: function() {
        this.node = null;
        this.getValue = this._getValue_unbound;
        this.setValue = this._setValue_unbound
    }
};
Object.assign(THREE.PropertyBinding.prototype, {
    _getValue_unavailable: function() {},
    _setValue_unavailable: function() {},
    _getValue_unbound: THREE.PropertyBinding.prototype.getValue,
    _setValue_unbound: THREE.PropertyBinding.prototype.setValue,
    BindingType: {
        Direct: 0,
        EntireArray: 1,
        ArrayElement: 2,
        HasFromToArray: 3
    },
    Versioning: {
        None: 0,
        NeedsUpdate: 1,
        MatrixWorldNeedsUpdate: 2
    },
    GetterByBindingType: [function(n, t) {
        n[t] = this.node[this.propertyName]
    }, function(n, t) {
        for (var r = this.resolvedProperty, i = 0, u = r.length; i !== u; ++i) n[t++] = r[i]
    }, function(n, t) {
        n[t] = this.resolvedProperty[this.propertyIndex]
    }, function(n, t) {
        this.resolvedProperty.toArray(n, t)
    }],
    SetterByBindingTypeAndVersioning: [
        [function(n, t) {
            this.node[this.propertyName] = n[t]
        }, function(n, t) {
            this.node[this.propertyName] = n[t];
            this.targetObject.needsUpdate = !0
        }, function(n, t) {
            this.node[this.propertyName] = n[t];
            this.targetObject.matrixWorldNeedsUpdate = !0
        }],
        [function(n, t) {
            for (var r = this.resolvedProperty, i = 0, u = r.length; i !== u; ++i) r[i] = n[t++]
        }, function(n, t) {
            for (var r = this.resolvedProperty, i = 0, u = r.length; i !== u; ++i) r[i] = n[t++];
            this.targetObject.needsUpdate = !0
        }, function(n, t) {
            for (var r = this.resolvedProperty, i = 0, u = r.length; i !== u; ++i) r[i] = n[t++];
            this.targetObject.matrixWorldNeedsUpdate = !0
        }],
        [function(n, t) {
            this.resolvedProperty[this.propertyIndex] = n[t]
        }, function(n, t) {
            this.resolvedProperty[this.propertyIndex] = n[t];
            this.targetObject.needsUpdate = !0
        }, function(n, t) {
            this.resolvedProperty[this.propertyIndex] = n[t];
            this.targetObject.matrixWorldNeedsUpdate = !0
        }],
        [function(n, t) {
            this.resolvedProperty.fromArray(n, t)
        }, function(n, t) {
            this.resolvedProperty.fromArray(n, t);
            this.targetObject.needsUpdate = !0
        }, function(n, t) {
            this.resolvedProperty.fromArray(n, t);
            this.targetObject.matrixWorldNeedsUpdate = !0
        }]
    ]
});
THREE.PropertyBinding.Composite = function(n, t, i) {
    var r = i || THREE.PropertyBinding.parseTrackName(t);
    this._targetGroup = n;
    this._bindings = n.subscribe_(t, r)
};
THREE.PropertyBinding.Composite.prototype = {
    constructor: THREE.PropertyBinding.Composite,
    getValue: function(n, t) {
        this.bind();
        var r = this._targetGroup.nCachedObjects_,
            i = this._bindings[r];
        i !== undefined && i.getValue(n, t)
    },
    setValue: function(n, t) {
        for (var r = this._bindings, i = this._targetGroup.nCachedObjects_, u = r.length; i !== u; ++i) r[i].setValue(n, t)
    },
    bind: function() {
        for (var t = this._bindings, n = this._targetGroup.nCachedObjects_, i = t.length; n !== i; ++n) t[n].bind()
    },
    unbind: function() {
        for (var t = this._bindings, n = this._targetGroup.nCachedObjects_, i = t.length; n !== i; ++n) t[n].unbind()
    }
};
THREE.PropertyBinding.create = function(n, t, i) {
    return n instanceof THREE.AnimationObjectGroup ? new THREE.PropertyBinding.Composite(n, t, i) : new THREE.PropertyBinding(n, t, i)
};
THREE.PropertyBinding.parseTrackName = function(n) {
    var r = /^(([\w]+\/)*)([\w-\d]+)?(\.([\w]+)(\[([\w\d\[\]\_.:\- ]+)\])?)?(\.([\w.]+)(\[([\w\d\[\]\_. ]+)\])?)$/,
        t = r.exec(n),
        i;
    if (!t) throw new Error("cannot parse trackName at all: " + n);
    if (t.index === r.lastIndex && r.lastIndex++, i = {
            nodeName: t[3],
            objectName: t[5],
            objectIndex: t[7],
            propertyName: t[9],
            propertyIndex: t[11]
        }, i.propertyName === null || i.propertyName.length === 0) throw new Error("can not parse propertyName from trackName: " + n);
    return i
};
THREE.PropertyBinding.findNode = function(n, t) {
    var f, i, r, u;
    return !t || t === "" || t === "root" || t === "." || t === -1 || t === n.name || t === n.uuid ? n : n.skeleton && (f = function(n) {
        for (var r, i = 0; i < n.bones.length; i++)
            if (r = n.bones[i], r.name === t) return r;
        return null
    }, i = f(n.skeleton), i) ? i : n.children && (r = function(n) {
        for (var i, f, u = 0; u < n.length; u++) {
            if (i = n[u], i.name === t || i.uuid === t) return i;
            if (f = r(i.children), f) return f
        }
        return null
    }, u = r(n.children), u) ? u : null
};
THREE.PropertyMixer = function(n, t, i) {
    this.binding = n;
    this.valueSize = i;
    var u = Float64Array,
        r;
    switch (t) {
        case "quaternion":
            r = this._slerp;
            break;
        case "string":
        case "bool":
            u = Array;
            r = this._select;
            break;
        default:
            r = this._lerp
    }
    this.buffer = new u(i * 4);
    this._mixBufferRegion = r;
    this.cumulativeWeight = 0;
    this.useCount = 0;
    this.referenceCount = 0
};
THREE.PropertyMixer.prototype = {
    constructor: THREE.PropertyMixer,
    accumulate: function(n, t) {
        var f = this.buffer,
            u = this.valueSize,
            e = n * u + u,
            i = this.cumulativeWeight,
            r, o;
        if (i === 0) {
            for (r = 0; r !== u; ++r) f[e + r] = f[r];
            i = t
        } else i += t, o = t / i, this._mixBufferRegion(f, e, 0, o, u);
        this.cumulativeWeight = i
    },
    apply: function(n) {
        var t = this.valueSize,
            r = this.buffer,
            u = n * t + t,
            f = this.cumulativeWeight,
            s = this.binding,
            e, i, o;
        for (this.cumulativeWeight = 0, f < 1 && (e = t * 3, this._mixBufferRegion(r, u, e, 1 - f, t)), i = t, o = t + t; i !== o; ++i)
            if (r[i] !== r[i + t]) {
                s.setValue(r, u);
                break
            }
    },
    saveOriginalState: function() {
        var f = this.binding,
            t = this.buffer,
            i = this.valueSize,
            r = i * 3,
            n, u;
        for (f.getValue(t, r), n = i, u = r; n !== u; ++n) t[n] = t[r + n % i];
        this.cumulativeWeight = 0
    },
    restoreOriginalState: function() {
        var n = this.valueSize * 3;
        this.binding.setValue(this.buffer, n)
    },
    _select: function(n, t, i, r, u) {
        if (r >= .5)
            for (var f = 0; f !== u; ++f) n[t + f] = n[i + f]
    },
    _slerp: function(n, t, i, r) {
        THREE.Quaternion.slerpFlat(n, t, n, t, n, i, r)
    },
    _lerp: function(n, t, i, r, u) {
        for (var o = 1 - r, e, f = 0; f !== u; ++f) e = t + f, n[e] = n[e] * o + n[i + f] * r
    }
};
THREE.BooleanKeyframeTrack = function(n, t, i) {
    THREE.KeyframeTrack.call(this, n, t, i)
};
THREE.BooleanKeyframeTrack.prototype = Object.assign(Object.create(THREE.KeyframeTrack.prototype), {
    constructor: THREE.BooleanKeyframeTrack,
    ValueTypeName: "bool",
    ValueBufferType: Array,
    DefaultInterpolation: THREE.InterpolateDiscrete,
    InterpolantFactoryMethodLinear: undefined,
    InterpolantFactoryMethodSmooth: undefined
});
THREE.ColorKeyframeTrack = function(n, t, i, r) {
    THREE.KeyframeTrack.call(this, n, t, i, r)
};
THREE.ColorKeyframeTrack.prototype = Object.assign(Object.create(THREE.KeyframeTrack.prototype), {
    constructor: THREE.ColorKeyframeTrack,
    ValueTypeName: "color"
});
THREE.NumberKeyframeTrack = function(n, t, i, r) {
    THREE.KeyframeTrack.call(this, n, t, i, r)
};
THREE.NumberKeyframeTrack.prototype = Object.assign(Object.create(THREE.KeyframeTrack.prototype), {
    constructor: THREE.NumberKeyframeTrack,
    ValueTypeName: "number"
});
THREE.QuaternionKeyframeTrack = function(n, t, i, r) {
    THREE.KeyframeTrack.call(this, n, t, i, r)
};
THREE.QuaternionKeyframeTrack.prototype = Object.assign(Object.create(THREE.KeyframeTrack.prototype), {
    constructor: THREE.QuaternionKeyframeTrack,
    ValueTypeName: "quaternion",
    DefaultInterpolation: THREE.InterpolateLinear,
    InterpolantFactoryMethodLinear: function(n) {
        return new THREE.QuaternionLinearInterpolant(this.times, this.values, this.getValueSize(), n)
    },
    InterpolantFactoryMethodSmooth: undefined
});
THREE.StringKeyframeTrack = function(n, t, i, r) {
    THREE.KeyframeTrack.call(this, n, t, i, r)
};
THREE.StringKeyframeTrack.prototype = Object.assign(Object.create(THREE.KeyframeTrack.prototype), {
    constructor: THREE.StringKeyframeTrack,
    ValueTypeName: "string",
    ValueBufferType: Array,
    DefaultInterpolation: THREE.InterpolateDiscrete,
    InterpolantFactoryMethodLinear: undefined,
    InterpolantFactoryMethodSmooth: undefined
});
THREE.VectorKeyframeTrack = function(n, t, i, r) {
    THREE.KeyframeTrack.call(this, n, t, i, r)
};
THREE.VectorKeyframeTrack.prototype = Object.assign(Object.create(THREE.KeyframeTrack.prototype), {
    constructor: THREE.VectorKeyframeTrack,
    ValueTypeName: "vector"
});
THREE.Audio = function(n) {
    THREE.Object3D.call(this);
    this.type = "Audio";
    this.context = n.context;
    this.source = this.context.createBufferSource();
    this.source.onended = this.onEnded.bind(this);
    this.gain = this.context.createGain();
    this.gain.connect(n.getInput());
    this.autoplay = !1;
    this.startTime = 0;
    this.playbackRate = 1;
    this.isPlaying = !1;
    this.hasPlaybackControl = !0;
    this.sourceType = "empty";
    this.filter = null
};
THREE.Audio.prototype = Object.create(THREE.Object3D.prototype);
THREE.Audio.prototype.constructor = THREE.Audio;
THREE.Audio.prototype.getOutput = function() {
    return this.gain
};
THREE.Audio.prototype.setNodeSource = function(n) {
    return this.hasPlaybackControl = !1, this.sourceType = "audioNode", this.source = n, this.connect(), this
};
THREE.Audio.prototype.setBuffer = function(n) {
    var t = this;
    return t.source.buffer = n, t.sourceType = "buffer", t.autoplay && t.play(), this
};
THREE.Audio.prototype.play = function() {
    if (this.isPlaying === !0) {
        console.warn("THREE.Audio: Audio is already playing.");
        return
    }
    if (this.hasPlaybackControl === !1) {
        console.warn("THREE.Audio: this Audio has no playback control.");
        return
    }
    var n = this.context.createBufferSource();
    n.buffer = this.source.buffer;
    n.loop = this.source.loop;
    n.onended = this.source.onended;
    n.start(0, this.startTime);
    n.playbackRate.value = this.playbackRate;
    this.isPlaying = !0;
    this.source = n;
    this.connect()
};
THREE.Audio.prototype.pause = function() {
    if (this.hasPlaybackControl === !1) {
        console.warn("THREE.Audio: this Audio has no playback control.");
        return
    }
    this.source.stop();
    this.startTime = this.context.currentTime
};
THREE.Audio.prototype.stop = function() {
    if (this.hasPlaybackControl === !1) {
        console.warn("THREE.Audio: this Audio has no playback control.");
        return
    }
    this.source.stop();
    this.startTime = 0
};
THREE.Audio.prototype.connect = function() {
    this.filter !== null ? (this.source.connect(this.filter), this.filter.connect(this.getOutput())) : this.source.connect(this.getOutput())
};
THREE.Audio.prototype.disconnect = function() {
    this.filter !== null ? (this.source.disconnect(this.filter), this.filter.disconnect(this.getOutput())) : this.source.disconnect(this.getOutput())
};
THREE.Audio.prototype.getFilter = function() {
    return this.filter
};
THREE.Audio.prototype.setFilter = function(n) {
    n === undefined && (n = null);
    this.isPlaying === !0 ? (this.disconnect(), this.filter = n, this.connect()) : this.filter = n
};
THREE.Audio.prototype.setPlaybackRate = function(n) {
    if (this.hasPlaybackControl === !1) {
        console.warn("THREE.Audio: this Audio has no playback control.");
        return
    }
    this.playbackRate = n;
    this.isPlaying === !0 && (this.source.playbackRate.value = this.playbackRate)
};
THREE.Audio.prototype.getPlaybackRate = function() {
    return this.playbackRate
};
THREE.Audio.prototype.onEnded = function() {
    this.isPlaying = !1
};
THREE.Audio.prototype.setLoop = function(n) {
    if (this.hasPlaybackControl === !1) {
        console.warn("THREE.Audio: this Audio has no playback control.");
        return
    }
    this.source.loop = n
};
THREE.Audio.prototype.getLoop = function() {
    return this.hasPlaybackControl === !1 ? (console.warn("THREE.Audio: this Audio has no playback control."), !1) : this.source.loop
};
THREE.Audio.prototype.setVolume = function(n) {
    this.gain.gain.value = n
};
THREE.Audio.prototype.getVolume = function() {
    return this.gain.gain.value
};
THREE.AudioAnalyser = function(n, t) {
    this.analyser = n.context.createAnalyser();
    this.analyser.fftSize = t !== undefined ? t : 2048;
    this.data = new Uint8Array(this.analyser.frequencyBinCount);
    n.getOutput().connect(this.analyser)
};
THREE.AudioAnalyser.prototype = {
    constructor: THREE.AudioAnalyser,
    getData: function() {
        return this.analyser.getByteFrequencyData(this.data), this.data
    }
};
Object.defineProperty(THREE, "AudioContext", {
    get: function() {
        var n;
        return function() {
            return n === undefined && (n = new(window.AudioContext || window.webkitAudioContext)), n
        }
    }()
});
THREE.PositionalAudio = function(n) {
    THREE.Audio.call(this, n);
    this.panner = this.context.createPanner();
    this.panner.connect(this.gain)
};
THREE.PositionalAudio.prototype = Object.create(THREE.Audio.prototype);
THREE.PositionalAudio.prototype.constructor = THREE.PositionalAudio;
THREE.PositionalAudio.prototype.getOutput = function() {
    return this.panner
};
THREE.PositionalAudio.prototype.setRefDistance = function(n) {
    this.panner.refDistance = n
};
THREE.PositionalAudio.prototype.getRefDistance = function() {
    return this.panner.refDistance
};
THREE.PositionalAudio.prototype.setRolloffFactor = function(n) {
    this.panner.rolloffFactor = n
};
THREE.PositionalAudio.prototype.getRolloffFactor = function() {
    return this.panner.rolloffFactor
};
THREE.PositionalAudio.prototype.setDistanceModel = function(n) {
    this.panner.distanceModel = n
};
THREE.PositionalAudio.prototype.getDistanceModel = function() {
    return this.panner.distanceModel
};
THREE.PositionalAudio.prototype.setMaxDistance = function(n) {
    this.panner.maxDistance = n
};
THREE.PositionalAudio.prototype.getMaxDistance = function() {
    return this.panner.maxDistance
};
THREE.PositionalAudio.prototype.updateMatrixWorld = function() {
    var n = new THREE.Vector3;
    return function(t) {
        THREE.Object3D.prototype.updateMatrixWorld.call(this, t);
        n.setFromMatrixPosition(this.matrixWorld);
        this.panner.setPosition(n.x, n.y, n.z)
    }
}();
THREE.AudioListener = function() {
    THREE.Object3D.call(this);
    this.type = "AudioListener";
    this.context = THREE.AudioContext;
    this.gain = this.context.createGain();
    this.gain.connect(this.context.destination);
    this.filter = null
};
THREE.AudioListener.prototype = Object.create(THREE.Object3D.prototype);
THREE.AudioListener.prototype.constructor = THREE.AudioListener;
THREE.AudioListener.prototype.getInput = function() {
    return this.gain
};
THREE.AudioListener.prototype.removeFilter = function() {
    this.filter !== null && (this.gain.disconnect(this.filter), this.filter.disconnect(this.context.destination), this.gain.connect(this.context.destination), this.filter = null)
};
THREE.AudioListener.prototype.setFilter = function(n) {
    this.filter !== null ? (this.gain.disconnect(this.filter), this.filter.disconnect(this.context.destination)) : this.gain.disconnect(this.context.destination);
    this.filter = n;
    this.gain.connect(this.filter);
    this.filter.connect(this.context.destination)
};
THREE.AudioListener.prototype.getFilter = function() {
    return this.filter
};
THREE.AudioListener.prototype.setMasterVolume = function(n) {
    this.gain.gain.value = n
};
THREE.AudioListener.prototype.getMasterVolume = function() {
    return this.gain.gain.value
};
THREE.AudioListener.prototype.updateMatrixWorld = function() {
    var n = new THREE.Vector3,
        i = new THREE.Quaternion,
        r = new THREE.Vector3,
        t = new THREE.Vector3;
    return function(u) {
        THREE.Object3D.prototype.updateMatrixWorld.call(this, u);
        var e = this.context.listener,
            f = this.up;
        this.matrixWorld.decompose(n, i, r);
        t.set(0, 0, -1).applyQuaternion(i);
        e.setPosition(n.x, n.y, n.z);
        e.setOrientation(t.x, t.y, t.z, f.x, f.y, f.z)
    }
}();
THREE.Camera = function() {
    THREE.Object3D.call(this);
    this.type = "Camera";
    this.matrixWorldInverse = new THREE.Matrix4;
    this.projectionMatrix = new THREE.Matrix4
};
THREE.Camera.prototype = Object.create(THREE.Object3D.prototype);
THREE.Camera.prototype.constructor = THREE.Camera;
THREE.Camera.prototype.getWorldDirection = function() {
    var n = new THREE.Quaternion;
    return function(t) {
        var i = t || new THREE.Vector3;
        return this.getWorldQuaternion(n), i.set(0, 0, -1).applyQuaternion(n)
    }
}();
THREE.Camera.prototype.lookAt = function() {
    var n = new THREE.Matrix4;
    return function(t) {
        n.lookAt(this.position, t, this.up);
        this.quaternion.setFromRotationMatrix(n)
    }
}();
THREE.Camera.prototype.clone = function() {
    return (new this.constructor).copy(this)
};
THREE.Camera.prototype.copy = function(n) {
    return THREE.Object3D.prototype.copy.call(this, n), this.matrixWorldInverse.copy(n.matrixWorldInverse), this.projectionMatrix.copy(n.projectionMatrix), this
};
THREE.CubeCamera = function(n, t, i) {
    var f, e, o, s, h, l;
    THREE.Object3D.call(this);
    this.type = "CubeCamera";
    var r = 90,
        u = 1,
        c = new THREE.PerspectiveCamera(r, u, n, t);
    c.up.set(0, -1, 0);
    c.lookAt(new THREE.Vector3(1, 0, 0));
    this.add(c);
    f = new THREE.PerspectiveCamera(r, u, n, t);
    f.up.set(0, -1, 0);
    f.lookAt(new THREE.Vector3(-1, 0, 0));
    this.add(f);
    e = new THREE.PerspectiveCamera(r, u, n, t);
    e.up.set(0, 0, 1);
    e.lookAt(new THREE.Vector3(0, 1, 0));
    this.add(e);
    o = new THREE.PerspectiveCamera(r, u, n, t);
    o.up.set(0, 0, -1);
    o.lookAt(new THREE.Vector3(0, -1, 0));
    this.add(o);
    s = new THREE.PerspectiveCamera(r, u, n, t);
    s.up.set(0, -1, 0);
    s.lookAt(new THREE.Vector3(0, 0, 1));
    this.add(s);
    h = new THREE.PerspectiveCamera(r, u, n, t);
    h.up.set(0, -1, 0);
    h.lookAt(new THREE.Vector3(0, 0, -1));
    this.add(h);
    l = {
        format: THREE.RGBFormat,
        magFilter: THREE.LinearFilter,
        minFilter: THREE.LinearFilter
    };
    this.renderTarget = new THREE.WebGLRenderTargetCube(i, i, l);
    this.updateCubeMap = function(n, t) {
        this.parent === null && this.updateMatrixWorld();
        var i = this.renderTarget,
            r = i.texture.generateMipmaps;
        i.texture.generateMipmaps = !1;
        i.activeCubeFace = 0;
        n.render(t, c, i);
        i.activeCubeFace = 1;
        n.render(t, f, i);
        i.activeCubeFace = 2;
        n.render(t, e, i);
        i.activeCubeFace = 3;
        n.render(t, o, i);
        i.activeCubeFace = 4;
        n.render(t, s, i);
        i.texture.generateMipmaps = r;
        i.activeCubeFace = 5;
        n.render(t, h, i);
        n.setRenderTarget(null)
    }
};
THREE.CubeCamera.prototype = Object.create(THREE.Object3D.prototype);
THREE.CubeCamera.prototype.constructor = THREE.CubeCamera;
THREE.OrthographicCamera = function(n, t, i, r, u, f) {
    THREE.Camera.call(this);
    this.type = "OrthographicCamera";
    this.zoom = 1;
    this.left = n;
    this.right = t;
    this.top = i;
    this.bottom = r;
    this.near = u !== undefined ? u : .1;
    this.far = f !== undefined ? f : 2e3;
    this.updateProjectionMatrix()
};
THREE.OrthographicCamera.prototype = Object.create(THREE.Camera.prototype);
THREE.OrthographicCamera.prototype.constructor = THREE.OrthographicCamera;
THREE.OrthographicCamera.prototype.updateProjectionMatrix = function() {
    var n = (this.right - this.left) / (2 * this.zoom),
        t = (this.top - this.bottom) / (2 * this.zoom),
        i = (this.right + this.left) / 2,
        r = (this.top + this.bottom) / 2;
    this.projectionMatrix.makeOrthographic(i - n, i + n, r + t, r - t, this.near, this.far)
};
THREE.OrthographicCamera.prototype.copy = function(n) {
    return THREE.Camera.prototype.copy.call(this, n), this.left = n.left, this.right = n.right, this.top = n.top, this.bottom = n.bottom, this.near = n.near, this.far = n.far, this.zoom = n.zoom, this
};
THREE.OrthographicCamera.prototype.toJSON = function(n) {
    var t = THREE.Object3D.prototype.toJSON.call(this, n);
    return t.object.zoom = this.zoom, t.object.left = this.left, t.object.right = this.right, t.object.top = this.top, t.object.bottom = this.bottom, t.object.near = this.near, t.object.far = this.far, t
};
THREE.PerspectiveCamera = function(n, t, i, r) {
    THREE.Camera.call(this);
    this.type = "PerspectiveCamera";
    this.fov = n !== undefined ? n : 50;
    this.zoom = 1;
    this.near = i !== undefined ? i : .1;
    this.far = r !== undefined ? r : 2e3;
    this.focus = 10;
    this.aspect = t !== undefined ? t : 1;
    this.view = null;
    this.filmGauge = 35;
    this.filmOffset = 0;
    this.updateProjectionMatrix()
};
THREE.PerspectiveCamera.prototype = Object.create(THREE.Camera.prototype);
THREE.PerspectiveCamera.prototype.constructor = THREE.PerspectiveCamera;
THREE.PerspectiveCamera.prototype.setLens = function(n, t) {
    console.warn("THREE.PerspectiveCamera.setLens is deprecated. Use .setFocalLength and .filmGauge for a photographic setup.");
    t !== undefined && (this.filmGauge = t);
    this.setFocalLength(n)
};
THREE.PerspectiveCamera.prototype.setFocalLength = function(n) {
    var t = .5 * this.getFilmHeight() / n;
    this.fov = THREE.Math.RAD2DEG * 2 * Math.atan(t);
    this.updateProjectionMatrix()
};
THREE.PerspectiveCamera.prototype.getFocalLength = function() {
    var n = Math.tan(THREE.Math.DEG2RAD * .5 * this.fov);
    return .5 * this.getFilmHeight() / n
};
THREE.PerspectiveCamera.prototype.getEffectiveFOV = function() {
    return THREE.Math.RAD2DEG * 2 * Math.atan(Math.tan(THREE.Math.DEG2RAD * .5 * this.fov) / this.zoom)
};
THREE.PerspectiveCamera.prototype.getFilmWidth = function() {
    return this.filmGauge * Math.min(this.aspect, 1)
};
THREE.PerspectiveCamera.prototype.getFilmHeight = function() {
    return this.filmGauge / Math.max(this.aspect, 1)
};
THREE.PerspectiveCamera.prototype.setViewOffset = function(n, t, i, r, u, f) {
    this.aspect = n / t;
    this.view = {
        fullWidth: n,
        fullHeight: t,
        offsetX: i,
        offsetY: r,
        width: u,
        height: f
    };
    this.updateProjectionMatrix()
};
THREE.PerspectiveCamera.prototype.updateProjectionMatrix = function() {
    var f = this.near,
        t = f * Math.tan(THREE.Math.DEG2RAD * .5 * this.fov) / this.zoom,
        i = 2 * t,
        r = this.aspect * i,
        u = -.5 * r,
        n = this.view,
        e, o, s;
    n !== null && (e = n.fullWidth, o = n.fullHeight, u += n.offsetX * r / e, t -= n.offsetY * i / o, r *= n.width / e, i *= n.height / o);
    s = this.filmOffset;
    s !== 0 && (u += f * s / this.getFilmWidth());
    this.projectionMatrix.makeFrustum(u, u + r, t - i, t, f, this.far)
};
THREE.PerspectiveCamera.prototype.copy = function(n) {
    return THREE.Camera.prototype.copy.call(this, n), this.fov = n.fov, this.zoom = n.zoom, this.near = n.near, this.far = n.far, this.focus = n.focus, this.aspect = n.aspect, this.view = n.view === null ? null : Object.assign({}, n.view), this.filmGauge = n.filmGauge, this.filmOffset = n.filmOffset, this
};
THREE.PerspectiveCamera.prototype.toJSON = function(n) {
    var t = THREE.Object3D.prototype.toJSON.call(this, n);
    return t.object.fov = this.fov, t.object.zoom = this.zoom, t.object.near = this.near, t.object.far = this.far, t.object.focus = this.focus, t.object.aspect = this.aspect, this.view !== null && (t.object.view = Object.assign({}, this.view)), t.object.filmGauge = this.filmGauge, t.object.filmOffset = this.filmOffset, t
};
THREE.StereoCamera = function() {
    this.type = "StereoCamera";
    this.aspect = 1;
    this.cameraL = new THREE.PerspectiveCamera;
    this.cameraL.layers.enable(1);
    this.cameraL.matrixAutoUpdate = !1;
    this.cameraR = new THREE.PerspectiveCamera;
    this.cameraR.layers.enable(2);
    this.cameraR.matrixAutoUpdate = !1
};
THREE.StereoCamera.prototype = {
    constructor: THREE.StereoCamera,
    update: function() {
        var i, r, n, t, u, f = new THREE.Matrix4,
            e = new THREE.Matrix4;
        return function(o) {
            var y = i !== o.focus || r !== o.fov || n !== o.aspect * this.aspect || t !== o.near || u !== o.far;
            if (y) {
                i = o.focus;
                r = o.fov;
                n = o.aspect * this.aspect;
                t = o.near;
                u = o.far;
                var c = o.projectionMatrix.clone(),
                    v = .064 / 2,
                    l = v * t / i,
                    a = t * Math.tan(THREE.Math.DEG2RAD * r * .5),
                    s, h;
                e.elements[12] = -v;
                f.elements[12] = v;
                s = -a * n + l;
                h = a * n + l;
                c.elements[0] = 2 * t / (h - s);
                c.elements[8] = (h + s) / (h - s);
                this.cameraL.projectionMatrix.copy(c);
                s = -a * n - l;
                h = a * n - l;
                c.elements[0] = 2 * t / (h - s);
                c.elements[8] = (h + s) / (h - s);
                this.cameraR.projectionMatrix.copy(c)
            }
            this.cameraL.matrixWorld.copy(o.matrixWorld).multiply(e);
            this.cameraR.matrixWorld.copy(o.matrixWorld).multiply(f)
        }
    }()
};
THREE.Light = function(n, t) {
    THREE.Object3D.call(this);
    this.type = "Light";
    this.color = new THREE.Color(n);
    this.intensity = t !== undefined ? t : 1;
    this.receiveShadow = undefined
};
THREE.Light.prototype = Object.create(THREE.Object3D.prototype);
THREE.Light.prototype.constructor = THREE.Light;
THREE.Light.prototype.copy = function(n) {
    return THREE.Object3D.prototype.copy.call(this, n), this.color.copy(n.color), this.intensity = n.intensity, this
};
THREE.Light.prototype.toJSON = function(n) {
    var t = THREE.Object3D.prototype.toJSON.call(this, n);
    return t.object.color = this.color.getHex(), t.object.intensity = this.intensity, this.groundColor !== undefined && (t.object.groundColor = this.groundColor.getHex()), this.distance !== undefined && (t.object.distance = this.distance), this.angle !== undefined && (t.object.angle = this.angle), this.decay !== undefined && (t.object.decay = this.decay), this.penumbra !== undefined && (t.object.penumbra = this.penumbra), t
};
THREE.LightShadow = function(n) {
    this.camera = n;
    this.bias = 0;
    this.radius = 1;
    this.mapSize = new THREE.Vector2(512, 512);
    this.map = null;
    this.matrix = new THREE.Matrix4
};
THREE.LightShadow.prototype = {
    constructor: THREE.LightShadow,
    copy: function(n) {
        return this.camera = n.camera.clone(), this.bias = n.bias, this.radius = n.radius, this.mapSize.copy(n.mapSize), this
    },
    clone: function() {
        return (new this.constructor).copy(this)
    }
};
THREE.AmbientLight = function(n, t) {
    THREE.Light.call(this, n, t);
    this.type = "AmbientLight";
    this.castShadow = undefined
};
THREE.AmbientLight.prototype = Object.create(THREE.Light.prototype);
THREE.AmbientLight.prototype.constructor = THREE.AmbientLight;
THREE.DirectionalLight = function(n, t) {
    THREE.Light.call(this, n, t);
    this.type = "DirectionalLight";
    this.position.set(0, 1, 0);
    this.updateMatrix();
    this.target = new THREE.Object3D;
    this.shadow = new THREE.DirectionalLightShadow
};
THREE.DirectionalLight.prototype = Object.create(THREE.Light.prototype);
THREE.DirectionalLight.prototype.constructor = THREE.DirectionalLight;
THREE.DirectionalLight.prototype.copy = function(n) {
    return THREE.Light.prototype.copy.call(this, n), this.target = n.target.clone(), this.shadow = n.shadow.clone(), this
};
THREE.DirectionalLightShadow = function() {
    THREE.LightShadow.call(this, new THREE.OrthographicCamera(-5, 5, 5, -5, .5, 500))
};
THREE.DirectionalLightShadow.prototype = Object.create(THREE.LightShadow.prototype);
THREE.DirectionalLightShadow.prototype.constructor = THREE.DirectionalLightShadow;
THREE.HemisphereLight = function(n, t, i) {
    THREE.Light.call(this, n, i);
    this.type = "HemisphereLight";
    this.castShadow = undefined;
    this.position.set(0, 1, 0);
    this.updateMatrix();
    this.groundColor = new THREE.Color(t)
};
THREE.HemisphereLight.prototype = Object.create(THREE.Light.prototype);
THREE.HemisphereLight.prototype.constructor = THREE.HemisphereLight;
THREE.HemisphereLight.prototype.copy = function(n) {
    return THREE.Light.prototype.copy.call(this, n), this.groundColor.copy(n.groundColor), this
};
THREE.PointLight = function(n, t, i, r) {
    THREE.Light.call(this, n, t);
    this.type = "PointLight";
    this.distance = i !== undefined ? i : 0;
    this.decay = r !== undefined ? r : 1;
    this.shadow = new THREE.LightShadow(new THREE.PerspectiveCamera(90, 1, .5, 500))
};
THREE.PointLight.prototype = Object.create(THREE.Light.prototype);
THREE.PointLight.prototype.constructor = THREE.PointLight;
Object.defineProperty(THREE.PointLight.prototype, "power", {
    get: function() {
        return this.intensity * 4 * Math.PI
    },
    set: function(n) {
        this.intensity = n / (4 * Math.PI)
    }
});
THREE.PointLight.prototype.copy = function(n) {
    return THREE.Light.prototype.copy.call(this, n), this.distance = n.distance, this.decay = n.decay, this.shadow = n.shadow.clone(), this
};
THREE.SpotLight = function(n, t, i, r, u, f) {
    THREE.Light.call(this, n, t);
    this.type = "SpotLight";
    this.position.set(0, 1, 0);
    this.updateMatrix();
    this.target = new THREE.Object3D;
    this.distance = i !== undefined ? i : 0;
    this.angle = r !== undefined ? r : Math.PI / 3;
    this.penumbra = u !== undefined ? u : 0;
    this.decay = f !== undefined ? f : 1;
    this.shadow = new THREE.SpotLightShadow
};
THREE.SpotLight.prototype = Object.create(THREE.Light.prototype);
THREE.SpotLight.prototype.constructor = THREE.SpotLight;
Object.defineProperty(THREE.SpotLight.prototype, "power", {
    get: function() {
        return this.intensity * Math.PI
    },
    set: function(n) {
        this.intensity = n / Math.PI
    }
});
THREE.SpotLight.prototype.copy = function(n) {
    return THREE.Light.prototype.copy.call(this, n), this.distance = n.distance, this.angle = n.angle, this.penumbra = n.penumbra, this.decay = n.decay, this.target = n.target.clone(), this.shadow = n.shadow.clone(), this
};
THREE.SpotLightShadow = function() {
    THREE.LightShadow.call(this, new THREE.PerspectiveCamera(50, 1, .5, 500))
};
THREE.SpotLightShadow.prototype = Object.create(THREE.LightShadow.prototype);
THREE.SpotLightShadow.prototype.constructor = THREE.SpotLightShadow;
THREE.SpotLightShadow.prototype.update = function(n) {
    var i = THREE.Math.RAD2DEG * 2 * n.angle,
        r = this.mapSize.width / this.mapSize.height,
        u = n.distance || 500,
        t = this.camera;
    (i !== t.fov || r !== t.aspect || u !== t.far) && (t.fov = i, t.aspect = r, t.far = u, t.updateProjectionMatrix())
};
THREE.AudioLoader = function(n) {
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager
};
THREE.AudioLoader.prototype = {
    constructor: THREE.AudioLoader,
    load: function(n, t, i, r) {
        var u = new THREE.XHRLoader(this.manager);
        u.setResponseType("arraybuffer");
        u.load(n, function(n) {
            var i = THREE.AudioContext;
            i.decodeAudioData(n, function(n) {
                t(n)
            })
        }, i, r)
    }
};
THREE.Cache = {
    enabled: !1,
    files: {},
    add: function(n, t) {
        this.enabled !== !1 && (this.files[n] = t)
    },
    get: function(n) {
        if (this.enabled !== !1) return this.files[n]
    },
    remove: function(n) {
        delete this.files[n]
    },
    clear: function() {
        this.files = {}
    }
};
THREE.Loader = function() {
    this.onLoadStart = function() {};
    this.onLoadProgress = function() {};
    this.onLoadComplete = function() {}
};
THREE.Loader.prototype = {
    constructor: THREE.Loader,
    crossOrigin: undefined,
    extractUrlBase: function(n) {
        var t = n.split("/");
        return t.length === 1 ? "./" : (t.pop(), t.join("/") + "/")
    },
    initMaterials: function(n, t, i) {
        for (var u = [], r = 0; r < n.length; ++r) u[r] = this.createMaterial(n[r], t, i);
        return u
    },
    createMaterial: function() {
        var n, t, i;
        return function(r, u, f) {
            function s(n, i, r, e, o) {
                var h = u + n,
                    a = THREE.Loader.Handlers.get(h),
                    s, l;
                return a !== null ? s = a.load(h) : (t.setCrossOrigin(f), s = t.load(h)), i !== undefined && (s.repeat.fromArray(i), i[0] !== 1 && (s.wrapS = THREE.RepeatWrapping), i[1] !== 1 && (s.wrapT = THREE.RepeatWrapping)), r !== undefined && s.offset.fromArray(r), e !== undefined && (e[0] === "repeat" && (s.wrapS = THREE.RepeatWrapping), e[0] === "mirror" && (s.wrapS = THREE.MirroredRepeatWrapping), e[1] === "repeat" && (s.wrapT = THREE.RepeatWrapping), e[1] === "mirror" && (s.wrapT = THREE.MirroredRepeatWrapping)), o !== undefined && (s.anisotropy = o), l = THREE.Math.generateUUID(), c[l] = s, l
            }
            var c, e, h, o;
            n === undefined && (n = new THREE.Color);
            t === undefined && (t = new THREE.TextureLoader);
            i === undefined && (i = new THREE.MaterialLoader);
            c = {};
            e = {
                uuid: THREE.Math.generateUUID(),
                type: "MeshLambertMaterial"
            };
            for (h in r) {
                o = r[h];
                switch (h) {
                    case "DbgColor":
                    case "DbgIndex":
                    case "opticalDensity":
                    case "illumination":
                        break;
                    case "DbgName":
                        e.name = o;
                        break;
                    case "blending":
                        e.blending = THREE[o];
                        break;
                    case "colorAmbient":
                    case "mapAmbient":
                        console.warn("THREE.Loader.createMaterial:", h, "is no longer supported.");
                        break;
                    case "colorDiffuse":
                        e.color = n.fromArray(o).getHex();
                        break;
                    case "colorSpecular":
                        e.specular = n.fromArray(o).getHex();
                        break;
                    case "colorEmissive":
                        e.emissive = n.fromArray(o).getHex();
                        break;
                    case "specularCoef":
                        e.shininess = o;
                        break;
                    case "shading":
                        o.toLowerCase() === "basic" && (e.type = "MeshBasicMaterial");
                        o.toLowerCase() === "phong" && (e.type = "MeshPhongMaterial");
                        break;
                    case "mapDiffuse":
                        e.map = s(o, r.mapDiffuseRepeat, r.mapDiffuseOffset, r.mapDiffuseWrap, r.mapDiffuseAnisotropy);
                        break;
                    case "mapDiffuseRepeat":
                    case "mapDiffuseOffset":
                    case "mapDiffuseWrap":
                    case "mapDiffuseAnisotropy":
                        break;
                    case "mapLight":
                        e.lightMap = s(o, r.mapLightRepeat, r.mapLightOffset, r.mapLightWrap, r.mapLightAnisotropy);
                        break;
                    case "mapLightRepeat":
                    case "mapLightOffset":
                    case "mapLightWrap":
                    case "mapLightAnisotropy":
                        break;
                    case "mapAO":
                        e.aoMap = s(o, r.mapAORepeat, r.mapAOOffset, r.mapAOWrap, r.mapAOAnisotropy);
                        break;
                    case "mapAORepeat":
                    case "mapAOOffset":
                    case "mapAOWrap":
                    case "mapAOAnisotropy":
                        break;
                    case "mapBump":
                        e.bumpMap = s(o, r.mapBumpRepeat, r.mapBumpOffset, r.mapBumpWrap, r.mapBumpAnisotropy);
                        break;
                    case "mapBumpScale":
                        e.bumpScale = o;
                        break;
                    case "mapBumpRepeat":
                    case "mapBumpOffset":
                    case "mapBumpWrap":
                    case "mapBumpAnisotropy":
                        break;
                    case "mapNormal":
                        e.normalMap = s(o, r.mapNormalRepeat, r.mapNormalOffset, r.mapNormalWrap, r.mapNormalAnisotropy);
                        break;
                    case "mapNormalFactor":
                        e.normalScale = [o, o];
                        break;
                    case "mapNormalRepeat":
                    case "mapNormalOffset":
                    case "mapNormalWrap":
                    case "mapNormalAnisotropy":
                        break;
                    case "mapSpecular":
                        e.specularMap = s(o, r.mapSpecularRepeat, r.mapSpecularOffset, r.mapSpecularWrap, r.mapSpecularAnisotropy);
                        break;
                    case "mapSpecularRepeat":
                    case "mapSpecularOffset":
                    case "mapSpecularWrap":
                    case "mapSpecularAnisotropy":
                        break;
                    case "mapAlpha":
                        e.alphaMap = s(o, r.mapAlphaRepeat, r.mapAlphaOffset, r.mapAlphaWrap, r.mapAlphaAnisotropy);
                        break;
                    case "mapAlphaRepeat":
                    case "mapAlphaOffset":
                    case "mapAlphaWrap":
                    case "mapAlphaAnisotropy":
                        break;
                    case "flipSided":
                        e.side = THREE.BackSide;
                        break;
                    case "doubleSided":
                        e.side = THREE.DoubleSide;
                        break;
                    case "transparency":
                        console.warn("THREE.Loader.createMaterial: transparency has been renamed to opacity");
                        e.opacity = o;
                        break;
                    case "depthTest":
                    case "depthWrite":
                    case "colorWrite":
                    case "opacity":
                    case "reflectivity":
                    case "transparent":
                    case "visible":
                    case "wireframe":
                        e[h] = o;
                        break;
                    case "vertexColors":
                        o === !0 && (e.vertexColors = THREE.VertexColors);
                        o === "face" && (e.vertexColors = THREE.FaceColors);
                        break;
                    default:
                        console.error("THREE.Loader.createMaterial: Unsupported", h, o)
                }
            }
            return e.type === "MeshBasicMaterial" && delete e.emissive, e.type !== "MeshPhongMaterial" && delete e.specular, e.opacity < 1 && (e.transparent = !0), i.setTextures(c), i.parse(e)
        }
    }()
};
THREE.Loader.Handlers = {
    handlers: [],
    add: function(n, t) {
        this.handlers.push(n, t)
    },
    get: function(n) {
        for (var i = this.handlers, u, f, t = 0, r = i.length; t < r; t += 2)
            if (u = i[t], f = i[t + 1], u.test(n)) return f;
        return null
    }
};
THREE.XHRLoader = function(n) {
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager
};
THREE.XHRLoader.prototype = {
    constructor: THREE.XHRLoader,
    load: function(n, t, i, r) {
        var f, e, u;
        return (this.path !== undefined && (n = this.path + n), f = this, e = THREE.Cache.get(n), e !== undefined) ? (t && setTimeout(function() {
            t(e)
        }, 0), e) : (u = new XMLHttpRequest, u.overrideMimeType("text/plain"), u.open("GET", n, !0), u.addEventListener("load", function(i) {
            var u = i.target.response;
            THREE.Cache.add(n, u);
            this.status === 200 ? (t && t(u), f.manager.itemEnd(n)) : this.status === 0 ? (console.warn("THREE.XHRLoader: HTTP Status 0 received."), t && t(u), f.manager.itemEnd(n)) : (r && r(i), f.manager.itemError(n))
        }, !1), i !== undefined && u.addEventListener("progress", function(n) {
            i(n)
        }, !1), u.addEventListener("error", function(t) {
            r && r(t);
            f.manager.itemError(n)
        }, !1), this.responseType !== undefined && (u.responseType = this.responseType), this.withCredentials !== undefined && (u.withCredentials = this.withCredentials), u.send(null), f.manager.itemStart(n), u)
    },
    setPath: function(n) {
        this.path = n
    },
    setResponseType: function(n) {
        this.responseType = n
    },
    setWithCredentials: function(n) {
        this.withCredentials = n
    }
};
THREE.FontLoader = function(n) {
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager
};
THREE.FontLoader.prototype = {
    constructor: THREE.FontLoader,
    load: function(n, t, i, r) {
        var u = new THREE.XHRLoader(this.manager);
        u.load(n, function(n) {
            t(new THREE.Font(JSON.parse(n.substring(65, n.length - 2))))
        }, i, r)
    }
};
THREE.ImageLoader = function(n) {
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager
};
THREE.ImageLoader.prototype = {
    constructor: THREE.ImageLoader,
    load: function(n, t, i, r) {
        var u, e, f;
        return (this.path !== undefined && (n = this.path + n), u = this, e = THREE.Cache.get(n), e !== undefined) ? (u.manager.itemStart(n), t ? setTimeout(function() {
            t(e);
            u.manager.itemEnd(n)
        }, 0) : u.manager.itemEnd(n), e) : (f = document.createElement("img"), f.addEventListener("load", function() {
            THREE.Cache.add(n, this);
            t && t(this);
            u.manager.itemEnd(n)
        }, !1), i !== undefined && f.addEventListener("progress", function(n) {
            i(n)
        }, !1), f.addEventListener("error", function(t) {
            r && r(t);
            u.manager.itemError(n)
        }, !1), this.crossOrigin !== undefined && (f.crossOrigin = this.crossOrigin), u.manager.itemStart(n), f.src = n, f)
    },
    setCrossOrigin: function(n) {
        this.crossOrigin = n
    },
    setPath: function(n) {
        this.path = n
    }
};
THREE.JSONLoader = function(n) {
    typeof n == "boolean" && (console.warn("THREE.JSONLoader: showStatus parameter has been removed from constructor."), n = undefined);
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager;
    this.withCredentials = !1
};
THREE.JSONLoader.prototype = {
    constructor: THREE.JSONLoader,
    get statusDomElement() {
        return this._statusDomElement === undefined && (this._statusDomElement = document.createElement("div")), console.warn("THREE.JSONLoader: .statusDomElement has been removed."), this._statusDomElement
    },
    load: function(n, t, i, r) {
        var f = this,
            e = this.texturePath && typeof this.texturePath == "string" ? this.texturePath : THREE.Loader.prototype.extractUrlBase(n),
            u = new THREE.XHRLoader(this.manager);
        u.setWithCredentials(this.withCredentials);
        u.load(n, function(i) {
            var o = JSON.parse(i),
                s = o.metadata,
                r, u;
            if (s !== undefined && (r = s.type, r !== undefined)) {
                if (r.toLowerCase() === "object") {
                    console.error("THREE.JSONLoader: " + n + " should be loaded with THREE.ObjectLoader instead.");
                    return
                }
                if (r.toLowerCase() === "scene") {
                    console.error("THREE.JSONLoader: " + n + " should be loaded with THREE.SceneLoader instead.");
                    return
                }
            }
            u = f.parse(o, e);
            t(u.geometry, u.materials)
        }, i, r)
    },
    setTexturePath: function(n) {
        this.texturePath = n
    },
    parse: function(n, t) {
        function f(t) {
            function p(n, t) {
                return n & 1 << t
            }
            var r, l, a, u, it, v, e, w, d, y, yt, st, ht, ct, lt, at, vt, g, h, s, c, b, nt, k, tt, rt, ut, f = n.faces,
                ft = n.vertices,
                o = n.normals,
                et = n.colors,
                ot = 0;
            if (n.uvs !== undefined) {
                for (r = 0; r < n.uvs.length; r++) n.uvs[r].length && ot++;
                for (r = 0; r < ot; r++) i.faceVertexUvs[r] = []
            }
            for (u = 0, it = ft.length; u < it;) g = new THREE.Vector3, g.x = ft[u++] * t, g.y = ft[u++] * t, g.z = ft[u++] * t, i.vertices.push(g);
            for (u = 0, it = f.length; u < it;)
                if (y = f[u++], yt = p(y, 0), st = p(y, 1), ht = p(y, 3), ct = p(y, 4), lt = p(y, 5), at = p(y, 6), vt = p(y, 7), yt) {
                    if (s = new THREE.Face3, s.a = f[u], s.b = f[u + 1], s.c = f[u + 3], c = new THREE.Face3, c.a = f[u + 1], c.b = f[u + 2], c.c = f[u + 3], u += 4, st && (d = f[u++], s.materialIndex = d, c.materialIndex = d), a = i.faces.length, ht)
                        for (r = 0; r < ot; r++)
                            for (k = n.uvs[r], i.faceVertexUvs[r][a] = [], i.faceVertexUvs[r][a + 1] = [], l = 0; l < 4; l++) w = f[u++], rt = k[w * 2], ut = k[w * 2 + 1], tt = new THREE.Vector2(rt, ut), l !== 2 && i.faceVertexUvs[r][a].push(tt), l !== 0 && i.faceVertexUvs[r][a + 1].push(tt);
                    if (ct && (e = f[u++] * 3, s.normal.set(o[e++], o[e++], o[e]), c.normal.copy(s.normal)), lt)
                        for (r = 0; r < 4; r++) e = f[u++] * 3, nt = new THREE.Vector3(o[e++], o[e++], o[e]), r !== 2 && s.vertexNormals.push(nt), r !== 0 && c.vertexNormals.push(nt);
                    if (at && (v = f[u++], b = et[v], s.color.setHex(b), c.color.setHex(b)), vt)
                        for (r = 0; r < 4; r++) v = f[u++], b = et[v], r !== 2 && s.vertexColors.push(new THREE.Color(b)), r !== 0 && c.vertexColors.push(new THREE.Color(b));
                    i.faces.push(s);
                    i.faces.push(c)
                } else {
                    if (h = new THREE.Face3, h.a = f[u++], h.b = f[u++], h.c = f[u++], st && (d = f[u++], h.materialIndex = d), a = i.faces.length, ht)
                        for (r = 0; r < ot; r++)
                            for (k = n.uvs[r], i.faceVertexUvs[r][a] = [], l = 0; l < 3; l++) w = f[u++], rt = k[w * 2], ut = k[w * 2 + 1], tt = new THREE.Vector2(rt, ut), i.faceVertexUvs[r][a].push(tt);
                    if (ct && (e = f[u++] * 3, h.normal.set(o[e++], o[e++], o[e])), lt)
                        for (r = 0; r < 3; r++) e = f[u++] * 3, nt = new THREE.Vector3(o[e++], o[e++], o[e]), h.vertexNormals.push(nt);
                    if (at && (v = f[u++], h.color.setHex(et[v])), vt)
                        for (r = 0; r < 3; r++) v = f[u++], h.vertexColors.push(new THREE.Color(et[v]));
                    i.faces.push(h)
                }
        }

        function e() {
            var r = n.influencesPerVertex !== undefined ? n.influencesPerVertex : 2,
                t, u;
            if (n.skinWeights)
                for (t = 0, u = n.skinWeights.length; t < u; t += r) {
                    var f = n.skinWeights[t],
                        e = r > 1 ? n.skinWeights[t + 1] : 0,
                        o = r > 2 ? n.skinWeights[t + 2] : 0,
                        s = r > 3 ? n.skinWeights[t + 3] : 0;
                    i.skinWeights.push(new THREE.Vector4(f, e, o, s))
                }
            if (n.skinIndices)
                for (t = 0, u = n.skinIndices.length; t < u; t += r) {
                    var h = n.skinIndices[t],
                        c = r > 1 ? n.skinIndices[t + 1] : 0,
                        l = r > 2 ? n.skinIndices[t + 2] : 0,
                        a = r > 3 ? n.skinIndices[t + 3] : 0;
                    i.skinIndices.push(new THREE.Vector4(h, c, l, a))
                }
            i.bones = n.bones;
            i.bones && i.bones.length > 0 && (i.skinWeights.length !== i.skinIndices.length || i.skinIndices.length !== i.vertices.length) && console.warn("When skinning, number of vertices (" + i.vertices.length + "), skinIndices (" + i.skinIndices.length + "), and skinWeights (" + i.skinWeights.length + ") should match.")
        }

        function o(t) {
            var h, e, u, c, o, s, l, r, f;
            if (n.morphTargets !== undefined)
                for (r = 0, f = n.morphTargets.length; r < f; r++)
                    for (i.morphTargets[r] = {}, i.morphTargets[r].name = n.morphTargets[r].name, i.morphTargets[r].vertices = [], h = i.morphTargets[r].vertices, e = n.morphTargets[r].vertices, u = 0, c = e.length; u < c; u += 3) o = new THREE.Vector3, o.x = e[u] * t, o.y = e[u + 1] * t, o.z = e[u + 2] * t, h.push(o);
            if (n.morphColors !== undefined && n.morphColors.length > 0)
                for (console.warn('THREE.JSONLoader: "morphColors" no longer supported. Using them as face colors.'), s = i.faces, l = n.morphColors[0].colors, r = 0, f = s.length; r < f; r++) s[r].color.fromArray(l, r * 3)
        }

        function s() {
            var r = [],
                t = [],
                u, f, e;
            for (n.animation !== undefined && t.push(n.animation), n.animations !== undefined && (n.animations.length ? t = t.concat(n.animations) : t.push(n.animations)), u = 0; u < t.length; u++) f = THREE.AnimationClip.parseAnimation(t[u], i.bones), f && r.push(f);
            i.morphTargets && (e = THREE.AnimationClip.CreateClipsFromMorphTargetSequences(i.morphTargets, 10), r = r.concat(e));
            r.length > 0 && (i.animations = r)
        }
        var i = new THREE.Geometry,
            r = n.scale !== undefined ? 1 / n.scale : 1,
            u;
        return f(r), e(), o(r), s(), i.computeFaceNormals(), i.computeBoundingSphere(), n.materials === undefined || n.materials.length === 0 ? {
            geometry: i
        } : (u = THREE.Loader.prototype.initMaterials(n.materials, t, this.crossOrigin), {
            geometry: i,
            materials: u
        })
    }
};
THREE.LoadingManager = function(n, t, i) {
    var r = this,
        e = !1,
        u = 0,
        f = 0;
    this.onStart = undefined;
    this.onLoad = n;
    this.onProgress = t;
    this.onError = i;
    this.itemStart = function(n) {
        if (f++, e === !1 && r.onStart !== undefined) r.onStart(n, u, f);
        e = !0
    };
    this.itemEnd = function(n) {
        if (u++, r.onProgress !== undefined) r.onProgress(n, u, f);
        u === f && (e = !1, r.onLoad !== undefined && r.onLoad())
    };
    this.itemError = function(n) {
        if (r.onError !== undefined) r.onError(n)
    }
};
THREE.DefaultLoadingManager = new THREE.LoadingManager;
THREE.BufferGeometryLoader = function(n) {
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager
};
THREE.BufferGeometryLoader.prototype = {
    constructor: THREE.BufferGeometryLoader,
    load: function(n, t, i, r) {
        var u = this,
            f = new THREE.XHRLoader(u.manager);
        f.load(n, function(n) {
            t(u.parse(JSON.parse(n)))
        }, i, r)
    },
    parse: function(n) {
        var t = new THREE.BufferGeometry,
            s = n.data.index,
            a = {
                Int8Array: Int8Array,
                Uint8Array: Uint8Array,
                Uint8ClampedArray: Uint8ClampedArray,
                Int16Array: Int16Array,
                Uint16Array: Uint16Array,
                Int32Array: Int32Array,
                Uint32Array: Uint32Array,
                Float32Array: Float32Array,
                Float64Array: Float64Array
            },
            h, c, r, i, f, e, v, o, u, l;
        s !== undefined && (i = new a[s.type](s.array), t.setIndex(new THREE.BufferAttribute(i, 1)));
        h = n.data.attributes;
        for (c in h) r = h[c], i = new a[r.type](r.array), t.addAttribute(c, new THREE.BufferAttribute(i, r.itemSize, r.normalized));
        if (f = n.data.groups || n.data.drawcalls || n.data.offsets, f !== undefined)
            for (e = 0, v = f.length; e !== v; ++e) o = f[e], t.addGroup(o.start, o.count, o.materialIndex);
        return u = n.data.boundingSphere, u !== undefined && (l = new THREE.Vector3, u.center !== undefined && l.fromArray(u.center), t.boundingSphere = new THREE.Sphere(l, u.radius)), t
    }
};
THREE.MaterialLoader = function(n) {
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager;
    this.textures = {}
};
THREE.MaterialLoader.prototype = {
    constructor: THREE.MaterialLoader,
    load: function(n, t, i, r) {
        var u = this,
            f = new THREE.XHRLoader(u.manager);
        f.load(n, function(n) {
            t(u.parse(JSON.parse(n)))
        }, i, r)
    },
    setTextures: function(n) {
        this.textures = n
    },
    getTexture: function(n) {
        var t = this.textures;
        return t[n] === undefined && console.warn("THREE.MaterialLoader: Undefined texture", n), t[n]
    },
    parse: function(n) {
        var t = new THREE[n.type],
            i, r, u;
        if (n.uuid !== undefined && (t.uuid = n.uuid), n.name !== undefined && (t.name = n.name), n.color !== undefined && t.color.setHex(n.color), n.roughness !== undefined && (t.roughness = n.roughness), n.metalness !== undefined && (t.metalness = n.metalness), n.emissive !== undefined && t.emissive.setHex(n.emissive), n.specular !== undefined && t.specular.setHex(n.specular), n.shininess !== undefined && (t.shininess = n.shininess), n.uniforms !== undefined && (t.uniforms = n.uniforms), n.vertexShader !== undefined && (t.vertexShader = n.vertexShader), n.fragmentShader !== undefined && (t.fragmentShader = n.fragmentShader), n.vertexColors !== undefined && (t.vertexColors = n.vertexColors), n.shading !== undefined && (t.shading = n.shading), n.blending !== undefined && (t.blending = n.blending), n.side !== undefined && (t.side = n.side), n.opacity !== undefined && (t.opacity = n.opacity), n.transparent !== undefined && (t.transparent = n.transparent), n.alphaTest !== undefined && (t.alphaTest = n.alphaTest), n.depthTest !== undefined && (t.depthTest = n.depthTest), n.depthWrite !== undefined && (t.depthWrite = n.depthWrite), n.colorWrite !== undefined && (t.colorWrite = n.colorWrite), n.wireframe !== undefined && (t.wireframe = n.wireframe), n.wireframeLinewidth !== undefined && (t.wireframeLinewidth = n.wireframeLinewidth), n.size !== undefined && (t.size = n.size), n.sizeAttenuation !== undefined && (t.sizeAttenuation = n.sizeAttenuation), n.map !== undefined && (t.map = this.getTexture(n.map)), n.alphaMap !== undefined && (t.alphaMap = this.getTexture(n.alphaMap), t.transparent = !0), n.bumpMap !== undefined && (t.bumpMap = this.getTexture(n.bumpMap)), n.bumpScale !== undefined && (t.bumpScale = n.bumpScale), n.normalMap !== undefined && (t.normalMap = this.getTexture(n.normalMap)), n.normalScale !== undefined && (i = n.normalScale, Array.isArray(i) === !1 && (i = [i, i]), t.normalScale = (new THREE.Vector2).fromArray(i)), n.displacementMap !== undefined && (t.displacementMap = this.getTexture(n.displacementMap)), n.displacementScale !== undefined && (t.displacementScale = n.displacementScale), n.displacementBias !== undefined && (t.displacementBias = n.displacementBias), n.roughnessMap !== undefined && (t.roughnessMap = this.getTexture(n.roughnessMap)), n.metalnessMap !== undefined && (t.metalnessMap = this.getTexture(n.metalnessMap)), n.emissiveMap !== undefined && (t.emissiveMap = this.getTexture(n.emissiveMap)), n.emissiveIntensity !== undefined && (t.emissiveIntensity = n.emissiveIntensity), n.specularMap !== undefined && (t.specularMap = this.getTexture(n.specularMap)), n.envMap !== undefined && (t.envMap = this.getTexture(n.envMap), t.combine = THREE.MultiplyOperation), n.reflectivity && (t.reflectivity = n.reflectivity), n.lightMap !== undefined && (t.lightMap = this.getTexture(n.lightMap)), n.lightMapIntensity !== undefined && (t.lightMapIntensity = n.lightMapIntensity), n.aoMap !== undefined && (t.aoMap = this.getTexture(n.aoMap)), n.aoMapIntensity !== undefined && (t.aoMapIntensity = n.aoMapIntensity), n.materials !== undefined)
            for (r = 0, u = n.materials.length; r < u; r++) t.materials.push(this.parse(n.materials[r]));
        return t
    }
};
THREE.ObjectLoader = function(n) {
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager;
    this.texturePath = ""
};
THREE.ObjectLoader.prototype = {
    constructor: THREE.ObjectLoader,
    load: function(n, t, i, r) {
        this.texturePath === "" && (this.texturePath = n.substring(0, n.lastIndexOf("/") + 1));
        var u = this,
            f = new THREE.XHRLoader(u.manager);
        f.load(n, function(n) {
            u.parse(JSON.parse(n), t)
        }, i, r)
    },
    setTexturePath: function(n) {
        this.texturePath = n
    },
    setCrossOrigin: function(n) {
        this.crossOrigin = n
    },
    parse: function(n, t) {
        var r = this.parseGeometries(n.geometries),
            u = this.parseImages(n.images, function() {
                t !== undefined && t(i)
            }),
            f = this.parseTextures(n.textures, u),
            e = this.parseMaterials(n.materials, f),
            i = this.parseObject(n.object, r, e);
        return n.animations && (i.animations = this.parseAnimations(n.animations)), (n.images === undefined || n.images.length === 0) && t !== undefined && t(i), i
    },
    parseGeometries: function(n) {
        var u = {},
            f, e, r, o, i, t;
        if (n !== undefined)
            for (f = new THREE.JSONLoader, e = new THREE.BufferGeometryLoader, r = 0, o = n.length; r < o; r++) {
                t = n[r];
                switch (t.type) {
                    case "PlaneGeometry":
                    case "PlaneBufferGeometry":
                        i = new THREE[t.type](t.width, t.height, t.widthSegments, t.heightSegments);
                        break;
                    case "BoxGeometry":
                    case "BoxBufferGeometry":
                    case "CubeGeometry":
                        i = new THREE[t.type](t.width, t.height, t.depth, t.widthSegments, t.heightSegments, t.depthSegments);
                        break;
                    case "CircleGeometry":
                    case "CircleBufferGeometry":
                        i = new THREE[t.type](t.radius, t.segments, t.thetaStart, t.thetaLength);
                        break;
                    case "CylinderGeometry":
                    case "CylinderBufferGeometry":
                        i = new THREE[t.type](t.radiusTop, t.radiusBottom, t.height, t.radialSegments, t.heightSegments, t.openEnded, t.thetaStart, t.thetaLength);
                        break;
                    case "SphereGeometry":
                    case "SphereBufferGeometry":
                        i = new THREE[t.type](t.radius, t.widthSegments, t.heightSegments, t.phiStart, t.phiLength, t.thetaStart, t.thetaLength);
                        break;
                    case "DodecahedronGeometry":
                        i = new THREE.DodecahedronGeometry(t.radius, t.detail);
                        break;
                    case "IcosahedronGeometry":
                        i = new THREE.IcosahedronGeometry(t.radius, t.detail);
                        break;
                    case "OctahedronGeometry":
                        i = new THREE.OctahedronGeometry(t.radius, t.detail);
                        break;
                    case "TetrahedronGeometry":
                        i = new THREE.TetrahedronGeometry(t.radius, t.detail);
                        break;
                    case "RingGeometry":
                    case "RingBufferGeometry":
                        i = new THREE[t.type](t.innerRadius, t.outerRadius, t.thetaSegments, t.phiSegments, t.thetaStart, t.thetaLength);
                        break;
                    case "TorusGeometry":
                    case "TorusBufferGeometry":
                        i = new THREE[t.type](t.radius, t.tube, t.radialSegments, t.tubularSegments, t.arc);
                        break;
                    case "TorusKnotGeometry":
                    case "TorusKnotBufferGeometry":
                        i = new THREE[t.type](t.radius, t.tube, t.tubularSegments, t.radialSegments, t.p, t.q);
                        break;
                    case "LatheGeometry":
                    case "LatheBufferGeometry":
                        i = new THREE[t.type](t.points, t.segments, t.phiStart, t.phiLength);
                        break;
                    case "BufferGeometry":
                        i = e.parse(t);
                        break;
                    case "Geometry":
                        i = f.parse(t.data, this.texturePath).geometry;
                        break;
                    default:
                        console.warn('THREE.ObjectLoader: Unsupported geometry type "' + t.type + '"');
                        continue
                }
                i.uuid = t.uuid;
                t.name !== undefined && (i.name = t.name);
                u[t.uuid] = i
            }
        return u
    },
    parseMaterials: function(n, t) {
        var f = {},
            r, i, e, u;
        if (n !== undefined)
            for (r = new THREE.MaterialLoader, r.setTextures(t), i = 0, e = n.length; i < e; i++) u = r.parse(n[i]), f[u.uuid] = u;
        return f
    },
    parseAnimations: function(n) {
        for (var i = [], r, t = 0; t < n.length; t++) r = THREE.AnimationClip.parse(n[t]), i.push(r);
        return i
    },
    parseImages: function(n, t) {
        function c(n) {
            return u.manager.itemStart(n), f.load(n, function() {
                u.manager.itemEnd(n)
            })
        }
        var u = this,
            e = {},
            o, f, r, s, i, h;
        if (n !== undefined && n.length > 0)
            for (o = new THREE.LoadingManager(t), f = new THREE.ImageLoader(o), f.setCrossOrigin(this.crossOrigin), r = 0, s = n.length; r < s; r++) i = n[r], h = /^(\/\/)|([a-z]+:(\/\/)?)/i.test(i.url) ? i.url : u.texturePath + i.url, e[i.uuid] = c(h);
        return e
    },
    parseTextures: function(n, t) {
        function u(n) {
            return typeof n == "number" ? n : (console.warn("THREE.ObjectLoader.parseTexture: Constant should be in numeric form.", n), THREE[n])
        }
        var e = {},
            f, o, i, r;
        if (n !== undefined)
            for (f = 0, o = n.length; f < o; f++) i = n[f], i.image === undefined && console.warn('THREE.ObjectLoader: No "image" specified for', i.uuid), t[i.image] === undefined && console.warn("THREE.ObjectLoader: Undefined image", i.image), r = new THREE.Texture(t[i.image]), r.needsUpdate = !0, r.uuid = i.uuid, i.name !== undefined && (r.name = i.name), i.mapping !== undefined && (r.mapping = u(i.mapping)), i.offset !== undefined && (r.offset = new THREE.Vector2(i.offset[0], i.offset[1])), i.repeat !== undefined && (r.repeat = new THREE.Vector2(i.repeat[0], i.repeat[1])), i.minFilter !== undefined && (r.minFilter = u(i.minFilter)), i.magFilter !== undefined && (r.magFilter = u(i.magFilter)), i.anisotropy !== undefined && (r.anisotropy = i.anisotropy), Array.isArray(i.wrap) && (r.wrapS = u(i.wrap[0]), r.wrapT = u(i.wrap[1])), e[i.uuid] = r;
        return e
    },
    parseObject: function() {
        var n = new THREE.Matrix4;
        return function(t, i, r) {
            function h(n) {
                return i[n] === undefined && console.warn("THREE.ObjectLoader: Undefined geometry", n), i[n]
            }

            function o(n) {
                return n === undefined ? undefined : (r[n] === undefined && console.warn("THREE.ObjectLoader: Undefined material", n), r[n])
            }
            var u, e, c, l, s, a, f;
            switch (t.type) {
                case "Scene":
                    u = new THREE.Scene;
                    break;
                case "PerspectiveCamera":
                    u = new THREE.PerspectiveCamera(t.fov, t.aspect, t.near, t.far);
                    t.focus !== undefined && (u.focus = t.focus);
                    t.zoom !== undefined && (u.zoom = t.zoom);
                    t.filmGauge !== undefined && (u.filmGauge = t.filmGauge);
                    t.filmOffset !== undefined && (u.filmOffset = t.filmOffset);
                    t.view !== undefined && (u.view = Object.assign({}, t.view));
                    break;
                case "OrthographicCamera":
                    u = new THREE.OrthographicCamera(t.left, t.right, t.top, t.bottom, t.near, t.far);
                    break;
                case "AmbientLight":
                    u = new THREE.AmbientLight(t.color, t.intensity);
                    break;
                case "DirectionalLight":
                    u = new THREE.DirectionalLight(t.color, t.intensity);
                    break;
                case "PointLight":
                    u = new THREE.PointLight(t.color, t.intensity, t.distance, t.decay);
                    break;
                case "SpotLight":
                    u = new THREE.SpotLight(t.color, t.intensity, t.distance, t.angle, t.penumbra, t.decay);
                    break;
                case "HemisphereLight":
                    u = new THREE.HemisphereLight(t.color, t.groundColor, t.intensity);
                    break;
                case "Mesh":
                    e = h(t.geometry);
                    c = o(t.material);
                    u = e.bones && e.bones.length > 0 ? new THREE.SkinnedMesh(e, c) : new THREE.Mesh(e, c);
                    break;
                case "LOD":
                    u = new THREE.LOD;
                    break;
                case "Line":
                    u = new THREE.Line(h(t.geometry), o(t.material), t.mode);
                    break;
                case "PointCloud":
                case "Points":
                    u = new THREE.Points(h(t.geometry), o(t.material));
                    break;
                case "Sprite":
                    u = new THREE.Sprite(o(t.material));
                    break;
                case "Group":
                    u = new THREE.Group;
                    break;
                default:
                    u = new THREE.Object3D
            }
            if (u.uuid = t.uuid, t.name !== undefined && (u.name = t.name), t.matrix !== undefined ? (n.fromArray(t.matrix), n.decompose(u.position, u.quaternion, u.scale)) : (t.position !== undefined && u.position.fromArray(t.position), t.rotation !== undefined && u.rotation.fromArray(t.rotation), t.scale !== undefined && u.scale.fromArray(t.scale)), t.castShadow !== undefined && (u.castShadow = t.castShadow), t.receiveShadow !== undefined && (u.receiveShadow = t.receiveShadow), t.visible !== undefined && (u.visible = t.visible), t.userData !== undefined && (u.userData = t.userData), t.children !== undefined)
                for (f in t.children) u.add(this.parseObject(t.children[f], i, r));
            if (t.type === "LOD")
                for (l = t.levels, s = 0; s < l.length; s++) a = l[s], f = u.getObjectByProperty("uuid", a.object), f !== undefined && u.addLevel(f, a.distance);
            return u
        }
    }()
};
THREE.TextureLoader = function(n) {
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager
};
THREE.TextureLoader.prototype = {
    constructor: THREE.TextureLoader,
    load: function(n, t, i, r) {
        var u = new THREE.Texture,
            f = new THREE.ImageLoader(this.manager);
        return f.setCrossOrigin(this.crossOrigin), f.setPath(this.path), f.load(n, function(n) {
            u.image = n;
            u.needsUpdate = !0;
            t !== undefined && t(u)
        }, i, r), u
    },
    setCrossOrigin: function(n) {
        this.crossOrigin = n
    },
    setPath: function(n) {
        this.path = n
    }
};
THREE.CubeTextureLoader = function(n) {
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager
};
THREE.CubeTextureLoader.prototype = {
    constructor: THREE.CubeTextureLoader,
    load: function(n, t, i, r) {
        function s(i) {
            e.load(n[i], function(n) {
                u.images[i] = n;
                o++;
                o === 6 && (u.needsUpdate = !0, t && t(u))
            }, undefined, r)
        }
        var u = new THREE.CubeTexture,
            e = new THREE.ImageLoader(this.manager),
            o, f;
        for (e.setCrossOrigin(this.crossOrigin), e.setPath(this.path), o = 0, f = 0; f < n.length; ++f) s(f);
        return u
    },
    setCrossOrigin: function(n) {
        this.crossOrigin = n
    },
    setPath: function(n) {
        this.path = n
    }
};
THREE.DataTextureLoader = THREE.BinaryTextureLoader = function(n) {
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager;
    this._parser = null
};
THREE.BinaryTextureLoader.prototype = {
    constructor: THREE.BinaryTextureLoader,
    load: function(n, t, i, r) {
        var e = this,
            u = new THREE.DataTexture,
            f = new THREE.XHRLoader(this.manager);
        return f.setResponseType("arraybuffer"), f.load(n, function(n) {
            var i = e._parser(n);
            i && (undefined !== i.image ? u.image = i.image : undefined !== i.data && (u.image.width = i.width, u.image.height = i.height, u.image.data = i.data), u.wrapS = undefined !== i.wrapS ? i.wrapS : THREE.ClampToEdgeWrapping, u.wrapT = undefined !== i.wrapT ? i.wrapT : THREE.ClampToEdgeWrapping, u.magFilter = undefined !== i.magFilter ? i.magFilter : THREE.LinearFilter, u.minFilter = undefined !== i.minFilter ? i.minFilter : THREE.LinearMipMapLinearFilter, u.anisotropy = undefined !== i.anisotropy ? i.anisotropy : 1, undefined !== i.format && (u.format = i.format), undefined !== i.type && (u.type = i.type), undefined !== i.mipmaps && (u.mipmaps = i.mipmaps), 1 === i.mipmapCount && (u.minFilter = THREE.LinearFilter), u.needsUpdate = !0, t && t(u, i))
        }, i, r), u
    }
};
THREE.CompressedTextureLoader = function(n) {
    this.manager = n !== undefined ? n : THREE.DefaultLoadingManager;
    this._parser = null
};
THREE.CompressedTextureLoader.prototype = {
    constructor: THREE.CompressedTextureLoader,
    load: function(n, t, i, r) {
        function l(o) {
            e.load(n[o], function(n) {
                var i = h._parser(n, !0);
                f[o] = {
                    width: i.width,
                    height: i.height,
                    format: i.format,
                    mipmaps: i.mipmaps
                };
                s += 1;
                s === 6 && (i.mipmapCount === 1 && (u.minFilter = THREE.LinearFilter), u.format = i.format, u.needsUpdate = !0, t && t(u))
            }, i, r)
        }
        var h = this,
            f = [],
            u = new THREE.CompressedTexture,
            e, s, o, c;
        if (u.image = f, e = new THREE.XHRLoader(this.manager), e.setPath(this.path), e.setResponseType("arraybuffer"), Array.isArray(n))
            for (s = 0, o = 0, c = n.length; o < c; ++o) l(o);
        else e.load(n, function(n) {
            var i = h._parser(n, !0),
                o, r, e;
            if (i.isCubemap)
                for (o = i.mipmaps.length / i.mipmapCount, r = 0; r < o; r++)
                    for (f[r] = {
                            mipmaps: []
                        }, e = 0; e < i.mipmapCount; e++) f[r].mipmaps.push(i.mipmaps[r * i.mipmapCount + e]), f[r].format = i.format, f[r].width = i.width, f[r].height = i.height;
            else u.image.width = i.width, u.image.height = i.height, u.mipmaps = i.mipmaps;
            i.mipmapCount === 1 && (u.minFilter = THREE.LinearFilter);
            u.format = i.format;
            u.needsUpdate = !0;
            t && t(u)
        }, i, r);
        return u
    },
    setPath: function(n) {
        this.path = n
    }
};
THREE.Material = function() {
    Object.defineProperty(this, "id", {
        value: THREE.MaterialIdCount++
    });
    this.uuid = THREE.Math.generateUUID();
    this.name = "";
    this.type = "Material";
    this.side = THREE.FrontSide;
    this.opacity = 1;
    this.transparent = !1;
    this.blending = THREE.NormalBlending;
    this.blendSrc = THREE.SrcAlphaFactor;
    this.blendDst = THREE.OneMinusSrcAlphaFactor;
    this.blendEquation = THREE.AddEquation;
    this.blendSrcAlpha = null;
    this.blendDstAlpha = null;
    this.blendEquationAlpha = null;
    this.depthFunc = THREE.LessEqualDepth;
    this.depthTest = !0;
    this.depthWrite = !0;
    this.clippingPlanes = null;
    this.clipShadows = !1;
    this.colorWrite = !0;
    this.precision = null;
    this.polygonOffset = !1;
    this.polygonOffsetFactor = 0;
    this.polygonOffsetUnits = 0;
    this.alphaTest = 0;
    this.premultipliedAlpha = !1;
    this.overdraw = 0;
    this.visible = !0;
    this._needsUpdate = !0
};
THREE.Material.prototype = {
    constructor: THREE.Material,
    get needsUpdate() {
        return this._needsUpdate
    },
    set needsUpdate(n) {
        n === !0 && this.update();
        this._needsUpdate = n
    },
    setValues: function(n) {
        var t, i, r;
        if (n !== undefined)
            for (t in n) {
                if (i = n[t], i === undefined) {
                    console.warn("THREE.Material: '" + t + "' parameter is undefined.");
                    continue
                }
                if (r = this[t], r === undefined) {
                    console.warn("THREE." + this.type + ": '" + t + "' is not a property of this material.");
                    continue
                }
                r instanceof THREE.Color ? r.set(i) : r instanceof THREE.Vector3 && i instanceof THREE.Vector3 ? r.copy(i) : this[t] = t === "overdraw" ? Number(i) : i
            }
    },
    toJSON: function(n) {
        function f(n) {
            var i = [],
                r, t;
            for (r in n) t = n[r], delete t.metadata, i.push(t);
            return i
        }
        var u = n === undefined,
            t, i, r;
        return u && (n = {
            textures: {},
            images: {}
        }), t = {
            metadata: {
                version: 4.4,
                type: "Material",
                generator: "Material.toJSON"
            }
        }, t.uuid = this.uuid, t.type = this.type, this.name !== "" && (t.name = this.name), this.color instanceof THREE.Color && (t.color = this.color.getHex()), this.roughness !== .5 && (t.roughness = this.roughness), this.metalness !== .5 && (t.metalness = this.metalness), this.emissive instanceof THREE.Color && (t.emissive = this.emissive.getHex()), this.specular instanceof THREE.Color && (t.specular = this.specular.getHex()), this.shininess !== undefined && (t.shininess = this.shininess), this.map instanceof THREE.Texture && (t.map = this.map.toJSON(n).uuid), this.alphaMap instanceof THREE.Texture && (t.alphaMap = this.alphaMap.toJSON(n).uuid), this.lightMap instanceof THREE.Texture && (t.lightMap = this.lightMap.toJSON(n).uuid), this.bumpMap instanceof THREE.Texture && (t.bumpMap = this.bumpMap.toJSON(n).uuid, t.bumpScale = this.bumpScale), this.normalMap instanceof THREE.Texture && (t.normalMap = this.normalMap.toJSON(n).uuid, t.normalScale = this.normalScale.toArray()), this.displacementMap instanceof THREE.Texture && (t.displacementMap = this.displacementMap.toJSON(n).uuid, t.displacementScale = this.displacementScale, t.displacementBias = this.displacementBias), this.roughnessMap instanceof THREE.Texture && (t.roughnessMap = this.roughnessMap.toJSON(n).uuid), this.metalnessMap instanceof THREE.Texture && (t.metalnessMap = this.metalnessMap.toJSON(n).uuid), this.emissiveMap instanceof THREE.Texture && (t.emissiveMap = this.emissiveMap.toJSON(n).uuid), this.specularMap instanceof THREE.Texture && (t.specularMap = this.specularMap.toJSON(n).uuid), this.envMap instanceof THREE.Texture && (t.envMap = this.envMap.toJSON(n).uuid, t.reflectivity = this.reflectivity), this.size !== undefined && (t.size = this.size), this.sizeAttenuation !== undefined && (t.sizeAttenuation = this.sizeAttenuation), this.vertexColors !== undefined && this.vertexColors !== THREE.NoColors && (t.vertexColors = this.vertexColors), this.shading !== undefined && this.shading !== THREE.SmoothShading && (t.shading = this.shading), this.blending !== undefined && this.blending !== THREE.NormalBlending && (t.blending = this.blending), this.side !== undefined && this.side !== THREE.FrontSide && (t.side = this.side), this.opacity < 1 && (t.opacity = this.opacity), this.transparent === !0 && (t.transparent = this.transparent), this.alphaTest > 0 && (t.alphaTest = this.alphaTest), this.premultipliedAlpha === !0 && (t.premultipliedAlpha = this.premultipliedAlpha), this.wireframe === !0 && (t.wireframe = this.wireframe), this.wireframeLinewidth > 1 && (t.wireframeLinewidth = this.wireframeLinewidth), u && (i = f(n.textures), r = f(n.images), i.length > 0 && (t.textures = i), r.length > 0 && (t.images = r)), t
    },
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        var i, r, u, t;
        if (this.name = n.name, this.side = n.side, this.opacity = n.opacity, this.transparent = n.transparent, this.blending = n.blending, this.blendSrc = n.blendSrc, this.blendDst = n.blendDst, this.blendEquation = n.blendEquation, this.blendSrcAlpha = n.blendSrcAlpha, this.blendDstAlpha = n.blendDstAlpha, this.blendEquationAlpha = n.blendEquationAlpha, this.depthFunc = n.depthFunc, this.depthTest = n.depthTest, this.depthWrite = n.depthWrite, this.colorWrite = n.colorWrite, this.precision = n.precision, this.polygonOffset = n.polygonOffset, this.polygonOffsetFactor = n.polygonOffsetFactor, this.polygonOffsetUnits = n.polygonOffsetUnits, this.alphaTest = n.alphaTest, this.premultipliedAlpha = n.premultipliedAlpha, this.overdraw = n.overdraw, this.visible = n.visible, this.clipShadows = n.clipShadows, i = n.clippingPlanes, r = null, i !== null)
            for (u = i.length, r = new Array(u), t = 0; t !== u; ++t) r[t] = i[t].clone();
        return this.clippingPlanes = r, this
    },
    update: function() {
        this.dispatchEvent({
            type: "update"
        })
    },
    dispose: function() {
        this.dispatchEvent({
            type: "dispose"
        })
    }
};
THREE.EventDispatcher.prototype.apply(THREE.Material.prototype);
THREE.MaterialIdCount = 0;
THREE.LineBasicMaterial = function(n) {
    THREE.Material.call(this);
    this.type = "LineBasicMaterial";
    this.color = new THREE.Color(16777215);
    this.linewidth = 1;
    this.linecap = "round";
    this.linejoin = "round";
    this.blending = THREE.NormalBlending;
    this.vertexColors = THREE.NoColors;
    this.fog = !0;
    this.setValues(n)
};
THREE.LineBasicMaterial.prototype = Object.create(THREE.Material.prototype);
THREE.LineBasicMaterial.prototype.constructor = THREE.LineBasicMaterial;
THREE.LineBasicMaterial.prototype.copy = function(n) {
    return THREE.Material.prototype.copy.call(this, n), this.color.copy(n.color), this.linewidth = n.linewidth, this.linecap = n.linecap, this.linejoin = n.linejoin, this.vertexColors = n.vertexColors, this.fog = n.fog, this
};
THREE.LineDashedMaterial = function(n) {
    THREE.Material.call(this);
    this.type = "LineDashedMaterial";
    this.color = new THREE.Color(16777215);
    this.linewidth = 1;
    this.scale = 1;
    this.dashSize = 3;
    this.gapSize = 1;
    this.blending = THREE.NormalBlending;
    this.vertexColors = THREE.NoColors;
    this.fog = !0;
    this.setValues(n)
};
THREE.LineDashedMaterial.prototype = Object.create(THREE.Material.prototype);
THREE.LineDashedMaterial.prototype.constructor = THREE.LineDashedMaterial;
THREE.LineDashedMaterial.prototype.copy = function(n) {
    return THREE.Material.prototype.copy.call(this, n), this.color.copy(n.color), this.linewidth = n.linewidth, this.scale = n.scale, this.dashSize = n.dashSize, this.gapSize = n.gapSize, this.vertexColors = n.vertexColors, this.fog = n.fog, this
};
THREE.MeshBasicMaterial = function(n) {
    THREE.Material.call(this);
    this.type = "MeshBasicMaterial";
    this.color = new THREE.Color(16777215);
    this.map = null;
    this.aoMap = null;
    this.aoMapIntensity = 1;
    this.specularMap = null;
    this.alphaMap = null;
    this.envMap = null;
    this.combine = THREE.MultiplyOperation;
    this.reflectivity = 1;
    this.refractionRatio = .98;
    this.fog = !0;
    this.shading = THREE.SmoothShading;
    this.blending = THREE.NormalBlending;
    this.wireframe = !1;
    this.wireframeLinewidth = 1;
    this.wireframeLinecap = "round";
    this.wireframeLinejoin = "round";
    this.vertexColors = THREE.NoColors;
    this.skinning = !1;
    this.morphTargets = !1;
    this.setValues(n)
};
THREE.MeshBasicMaterial.prototype = Object.create(THREE.Material.prototype);
THREE.MeshBasicMaterial.prototype.constructor = THREE.MeshBasicMaterial;
THREE.MeshBasicMaterial.prototype.copy = function(n) {
    return THREE.Material.prototype.copy.call(this, n), this.color.copy(n.color), this.map = n.map, this.aoMap = n.aoMap, this.aoMapIntensity = n.aoMapIntensity, this.specularMap = n.specularMap, this.alphaMap = n.alphaMap, this.envMap = n.envMap, this.combine = n.combine, this.reflectivity = n.reflectivity, this.refractionRatio = n.refractionRatio, this.fog = n.fog, this.shading = n.shading, this.wireframe = n.wireframe, this.wireframeLinewidth = n.wireframeLinewidth, this.wireframeLinecap = n.wireframeLinecap, this.wireframeLinejoin = n.wireframeLinejoin, this.vertexColors = n.vertexColors, this.skinning = n.skinning, this.morphTargets = n.morphTargets, this
};
THREE.MeshDepthMaterial = function(n) {
    THREE.Material.call(this);
    this.type = "MeshDepthMaterial";
    this.depthPacking = THREE.BasicDepthPacking;
    this.skinning = !1;
    this.morphTargets = !1;
    this.map = null;
    this.alphaMap = null;
    this.displacementMap = null;
    this.displacementScale = 1;
    this.displacementBias = 0;
    this.wireframe = !1;
    this.wireframeLinewidth = 1;
    this.setValues(n)
};
THREE.MeshDepthMaterial.prototype = Object.create(THREE.Material.prototype);
THREE.MeshDepthMaterial.prototype.constructor = THREE.MeshDepthMaterial;
THREE.MeshDepthMaterial.prototype.copy = function(n) {
    return THREE.Material.prototype.copy.call(this, n), this.depthPacking = n.depthPacking, this.skinning = n.skinning, this.morphTargets = n.morphTargets, this.map = n.map, this.alphaMap = n.alphaMap, this.displacementMap = n.displacementMap, this.displacementScale = n.displacementScale, this.displacementBias = n.displacementBias, this.wireframe = n.wireframe, this.wireframeLinewidth = n.wireframeLinewidth, this
};
THREE.MeshLambertMaterial = function(n) {
    THREE.Material.call(this);
    this.type = "MeshLambertMaterial";
    this.color = new THREE.Color(16777215);
    this.map = null;
    this.lightMap = null;
    this.lightMapIntensity = 1;
    this.aoMap = null;
    this.aoMapIntensity = 1;
    this.emissive = new THREE.Color(0);
    this.emissiveIntensity = 1;
    this.emissiveMap = null;
    this.specularMap = null;
    this.alphaMap = null;
    this.envMap = null;
    this.combine = THREE.MultiplyOperation;
    this.reflectivity = 1;
    this.refractionRatio = .98;
    this.fog = !0;
    this.blending = THREE.NormalBlending;
    this.wireframe = !1;
    this.wireframeLinewidth = 1;
    this.wireframeLinecap = "round";
    this.wireframeLinejoin = "round";
    this.vertexColors = THREE.NoColors;
    this.skinning = !1;
    this.morphTargets = !1;
    this.morphNormals = !1;
    this.setValues(n)
};
THREE.MeshLambertMaterial.prototype = Object.create(THREE.Material.prototype);
THREE.MeshLambertMaterial.prototype.constructor = THREE.MeshLambertMaterial;
THREE.MeshLambertMaterial.prototype.copy = function(n) {
    return THREE.Material.prototype.copy.call(this, n), this.color.copy(n.color), this.map = n.map, this.lightMap = n.lightMap, this.lightMapIntensity = n.lightMapIntensity, this.aoMap = n.aoMap, this.aoMapIntensity = n.aoMapIntensity, this.emissive.copy(n.emissive), this.emissiveMap = n.emissiveMap, this.emissiveIntensity = n.emissiveIntensity, this.specularMap = n.specularMap, this.alphaMap = n.alphaMap, this.envMap = n.envMap, this.combine = n.combine, this.reflectivity = n.reflectivity, this.refractionRatio = n.refractionRatio, this.fog = n.fog, this.wireframe = n.wireframe, this.wireframeLinewidth = n.wireframeLinewidth, this.wireframeLinecap = n.wireframeLinecap, this.wireframeLinejoin = n.wireframeLinejoin, this.vertexColors = n.vertexColors, this.skinning = n.skinning, this.morphTargets = n.morphTargets, this.morphNormals = n.morphNormals, this
};
THREE.MeshNormalMaterial = function(n) {
    THREE.Material.call(this, n);
    this.type = "MeshNormalMaterial";
    this.wireframe = !1;
    this.wireframeLinewidth = 1;
    this.morphTargets = !1;
    this.setValues(n)
};
THREE.MeshNormalMaterial.prototype = Object.create(THREE.Material.prototype);
THREE.MeshNormalMaterial.prototype.constructor = THREE.MeshNormalMaterial;
THREE.MeshNormalMaterial.prototype.copy = function(n) {
    return THREE.Material.prototype.copy.call(this, n), this.wireframe = n.wireframe, this.wireframeLinewidth = n.wireframeLinewidth, this
};
THREE.MeshPhongMaterial = function(n) {
    THREE.Material.call(this);
    this.type = "MeshPhongMaterial";
    this.color = new THREE.Color(16777215);
    this.specular = new THREE.Color(1118481);
    this.shininess = 30;
    this.map = null;
    this.lightMap = null;
    this.lightMapIntensity = 1;
    this.aoMap = null;
    this.aoMapIntensity = 1;
    this.emissive = new THREE.Color(0);
    this.emissiveIntensity = 1;
    this.emissiveMap = null;
    this.bumpMap = null;
    this.bumpScale = 1;
    this.normalMap = null;
    this.normalScale = new THREE.Vector2(1, 1);
    this.displacementMap = null;
    this.displacementScale = 1;
    this.displacementBias = 0;
    this.specularMap = null;
    this.alphaMap = null;
    this.envMap = null;
    this.combine = THREE.MultiplyOperation;
    this.reflectivity = 1;
    this.refractionRatio = .98;
    this.fog = !0;
    this.shading = THREE.SmoothShading;
    this.blending = THREE.NormalBlending;
    this.wireframe = !1;
    this.wireframeLinewidth = 1;
    this.wireframeLinecap = "round";
    this.wireframeLinejoin = "round";
    this.vertexColors = THREE.NoColors;
    this.skinning = !1;
    this.morphTargets = !1;
    this.morphNormals = !1;
    this.setValues(n)
};
THREE.MeshPhongMaterial.prototype = Object.create(THREE.Material.prototype);
THREE.MeshPhongMaterial.prototype.constructor = THREE.MeshPhongMaterial;
THREE.MeshPhongMaterial.prototype.copy = function(n) {
    return THREE.Material.prototype.copy.call(this, n), this.color.copy(n.color), this.specular.copy(n.specular), this.shininess = n.shininess, this.map = n.map, this.lightMap = n.lightMap, this.lightMapIntensity = n.lightMapIntensity, this.aoMap = n.aoMap, this.aoMapIntensity = n.aoMapIntensity, this.emissive.copy(n.emissive), this.emissiveMap = n.emissiveMap, this.emissiveIntensity = n.emissiveIntensity, this.bumpMap = n.bumpMap, this.bumpScale = n.bumpScale, this.normalMap = n.normalMap, this.normalScale.copy(n.normalScale), this.displacementMap = n.displacementMap, this.displacementScale = n.displacementScale, this.displacementBias = n.displacementBias, this.specularMap = n.specularMap, this.alphaMap = n.alphaMap, this.envMap = n.envMap, this.combine = n.combine, this.reflectivity = n.reflectivity, this.refractionRatio = n.refractionRatio, this.fog = n.fog, this.shading = n.shading, this.wireframe = n.wireframe, this.wireframeLinewidth = n.wireframeLinewidth, this.wireframeLinecap = n.wireframeLinecap, this.wireframeLinejoin = n.wireframeLinejoin, this.vertexColors = n.vertexColors, this.skinning = n.skinning, this.morphTargets = n.morphTargets, this.morphNormals = n.morphNormals, this
};
THREE.MeshStandardMaterial = function(n) {
    THREE.Material.call(this);
    this.defines = {
        STANDARD: ""
    };
    this.type = "MeshStandardMaterial";
    this.color = new THREE.Color(16777215);
    this.roughness = .5;
    this.metalness = .5;
    this.map = null;
    this.lightMap = null;
    this.lightMapIntensity = 1;
    this.aoMap = null;
    this.aoMapIntensity = 1;
    this.emissive = new THREE.Color(0);
    this.emissiveIntensity = 1;
    this.emissiveMap = null;
    this.bumpMap = null;
    this.bumpScale = 1;
    this.normalMap = null;
    this.normalScale = new THREE.Vector2(1, 1);
    this.displacementMap = null;
    this.displacementScale = 1;
    this.displacementBias = 0;
    this.roughnessMap = null;
    this.metalnessMap = null;
    this.alphaMap = null;
    this.envMap = null;
    this.envMapIntensity = 1;
    this.refractionRatio = .98;
    this.fog = !0;
    this.shading = THREE.SmoothShading;
    this.blending = THREE.NormalBlending;
    this.wireframe = !1;
    this.wireframeLinewidth = 1;
    this.wireframeLinecap = "round";
    this.wireframeLinejoin = "round";
    this.vertexColors = THREE.NoColors;
    this.skinning = !1;
    this.morphTargets = !1;
    this.morphNormals = !1;
    this.setValues(n)
};
THREE.MeshStandardMaterial.prototype = Object.create(THREE.Material.prototype);
THREE.MeshStandardMaterial.prototype.constructor = THREE.MeshStandardMaterial;
THREE.MeshStandardMaterial.prototype.copy = function(n) {
    return THREE.Material.prototype.copy.call(this, n), this.defines = {
        STANDARD: ""
    }, this.color.copy(n.color), this.roughness = n.roughness, this.metalness = n.metalness, this.map = n.map, this.lightMap = n.lightMap, this.lightMapIntensity = n.lightMapIntensity, this.aoMap = n.aoMap, this.aoMapIntensity = n.aoMapIntensity, this.emissive.copy(n.emissive), this.emissiveMap = n.emissiveMap, this.emissiveIntensity = n.emissiveIntensity, this.bumpMap = n.bumpMap, this.bumpScale = n.bumpScale, this.normalMap = n.normalMap, this.normalScale.copy(n.normalScale), this.displacementMap = n.displacementMap, this.displacementScale = n.displacementScale, this.displacementBias = n.displacementBias, this.roughnessMap = n.roughnessMap, this.metalnessMap = n.metalnessMap, this.alphaMap = n.alphaMap, this.envMap = n.envMap, this.envMapIntensity = n.envMapIntensity, this.refractionRatio = n.refractionRatio, this.fog = n.fog, this.shading = n.shading, this.wireframe = n.wireframe, this.wireframeLinewidth = n.wireframeLinewidth, this.wireframeLinecap = n.wireframeLinecap, this.wireframeLinejoin = n.wireframeLinejoin, this.vertexColors = n.vertexColors, this.skinning = n.skinning, this.morphTargets = n.morphTargets, this.morphNormals = n.morphNormals, this
};
THREE.MeshPhysicalMaterial = function(n) {
    THREE.MeshStandardMaterial.call(this);
    this.defines = {
        PHYSICAL: ""
    };
    this.type = "MeshPhysicalMaterial";
    this.reflectivity = .5;
    this.setValues(n)
};
THREE.MeshPhysicalMaterial.prototype = Object.create(THREE.MeshStandardMaterial.prototype);
THREE.MeshPhysicalMaterial.prototype.constructor = THREE.MeshPhysicalMaterial;
THREE.MeshPhysicalMaterial.prototype.copy = function(n) {
    return THREE.MeshStandardMaterial.prototype.copy.call(this, n), this.defines = {
        PHYSICAL: ""
    }, this.reflectivity = n.reflectivity, this
};
THREE.MultiMaterial = function(n) {
    this.uuid = THREE.Math.generateUUID();
    this.type = "MultiMaterial";
    this.materials = n instanceof Array ? n : [];
    this.visible = !0
};
THREE.MultiMaterial.prototype = {
    constructor: THREE.MultiMaterial,
    toJSON: function(n) {
        for (var i = {
                metadata: {
                    version: 4.2,
                    type: "material",
                    generator: "MaterialExporter"
                },
                uuid: this.uuid,
                type: this.type,
                materials: []
            }, u = this.materials, r, t = 0, f = u.length; t < f; t++) r = u[t].toJSON(n), delete r.metadata, i.materials.push(r);
        return i.visible = this.visible, i
    },
    clone: function() {
        for (var t = new this.constructor, n = 0; n < this.materials.length; n++) t.materials.push(this.materials[n].clone());
        return t.visible = this.visible, t
    }
};
THREE.PointsMaterial = function(n) {
    THREE.Material.call(this);
    this.type = "PointsMaterial";
    this.color = new THREE.Color(16777215);
    this.map = null;
    this.size = 1;
    this.sizeAttenuation = !0;
    this.blending = THREE.NormalBlending;
    this.vertexColors = THREE.NoColors;
    this.fog = !0;
    this.setValues(n)
};
THREE.PointsMaterial.prototype = Object.create(THREE.Material.prototype);
THREE.PointsMaterial.prototype.constructor = THREE.PointsMaterial;
THREE.PointsMaterial.prototype.copy = function(n) {
    return THREE.Material.prototype.copy.call(this, n), this.color.copy(n.color), this.map = n.map, this.size = n.size, this.sizeAttenuation = n.sizeAttenuation, this.vertexColors = n.vertexColors, this.fog = n.fog, this
};
THREE.ShaderMaterial = function(n) {
    THREE.Material.call(this);
    this.type = "ShaderMaterial";
    this.defines = {};
    this.uniforms = {};
    this.vertexShader = "void main() {\n\tgl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n}";
    this.fragmentShader = "void main() {\n\tgl_FragColor = vec4( 1.0, 0.0, 0.0, 1.0 );\n}";
    this.shading = THREE.SmoothShading;
    this.linewidth = 1;
    this.wireframe = !1;
    this.wireframeLinewidth = 1;
    this.fog = !1;
    this.lights = !1;
    this.clipping = !1;
    this.vertexColors = THREE.NoColors;
    this.skinning = !1;
    this.morphTargets = !1;
    this.morphNormals = !1;
    this.extensions = {
        derivatives: !1,
        fragDepth: !1,
        drawBuffers: !1,
        shaderTextureLOD: !1
    };
    this.defaultAttributeValues = {
        color: [1, 1, 1],
        uv: [0, 0],
        uv2: [0, 0]
    };
    this.index0AttributeName = undefined;
    n !== undefined && (n.attributes !== undefined && console.error("THREE.ShaderMaterial: attributes should now be defined in THREE.BufferGeometry instead."), this.setValues(n))
};
THREE.ShaderMaterial.prototype = Object.create(THREE.Material.prototype);
THREE.ShaderMaterial.prototype.constructor = THREE.ShaderMaterial;
THREE.ShaderMaterial.prototype.copy = function(n) {
    return THREE.Material.prototype.copy.call(this, n), this.fragmentShader = n.fragmentShader, this.vertexShader = n.vertexShader, this.uniforms = THREE.UniformsUtils.clone(n.uniforms), this.defines = n.defines, this.shading = n.shading, this.wireframe = n.wireframe, this.wireframeLinewidth = n.wireframeLinewidth, this.fog = n.fog, this.lights = n.lights, this.clipping = n.clipping, this.vertexColors = n.vertexColors, this.skinning = n.skinning, this.morphTargets = n.morphTargets, this.morphNormals = n.morphNormals, this.extensions = n.extensions, this
};
THREE.ShaderMaterial.prototype.toJSON = function(n) {
    var t = THREE.Material.prototype.toJSON.call(this, n);
    return t.uniforms = this.uniforms, t.vertexShader = this.vertexShader, t.fragmentShader = this.fragmentShader, t
};
THREE.RawShaderMaterial = function(n) {
    THREE.ShaderMaterial.call(this, n);
    this.type = "RawShaderMaterial"
};
THREE.RawShaderMaterial.prototype = Object.create(THREE.ShaderMaterial.prototype);
THREE.RawShaderMaterial.prototype.constructor = THREE.RawShaderMaterial;
THREE.SpriteMaterial = function(n) {
    THREE.Material.call(this);
    this.type = "SpriteMaterial";
    this.color = new THREE.Color(16777215);
    this.map = null;
    this.rotation = 0;
    this.fog = !1;
    this.setValues(n)
};
THREE.SpriteMaterial.prototype = Object.create(THREE.Material.prototype);
THREE.SpriteMaterial.prototype.constructor = THREE.SpriteMaterial;
THREE.SpriteMaterial.prototype.copy = function(n) {
    return THREE.Material.prototype.copy.call(this, n), this.color.copy(n.color), this.map = n.map, this.rotation = n.rotation, this.fog = n.fog, this
};
THREE.Texture = function(n, t, i, r, u, f, e, o, s, h) {
    Object.defineProperty(this, "id", {
        value: THREE.TextureIdCount++
    });
    this.uuid = THREE.Math.generateUUID();
    this.name = "";
    this.sourceFile = "";
    this.image = n !== undefined ? n : THREE.Texture.DEFAULT_IMAGE;
    this.mipmaps = [];
    this.mapping = t !== undefined ? t : THREE.Texture.DEFAULT_MAPPING;
    this.wrapS = i !== undefined ? i : THREE.ClampToEdgeWrapping;
    this.wrapT = r !== undefined ? r : THREE.ClampToEdgeWrapping;
    this.magFilter = u !== undefined ? u : THREE.LinearFilter;
    this.minFilter = f !== undefined ? f : THREE.LinearMipMapLinearFilter;
    this.anisotropy = s !== undefined ? s : 1;
    this.format = e !== undefined ? e : THREE.RGBAFormat;
    this.type = o !== undefined ? o : THREE.UnsignedByteType;
    this.offset = new THREE.Vector2(0, 0);
    this.repeat = new THREE.Vector2(1, 1);
    this.generateMipmaps = !0;
    this.premultiplyAlpha = !1;
    this.flipY = !0;
    this.unpackAlignment = 4;
    this.encoding = h !== undefined ? h : THREE.LinearEncoding;
    this.version = 0;
    this.onUpdate = null
};
THREE.Texture.DEFAULT_IMAGE = undefined;
THREE.Texture.DEFAULT_MAPPING = THREE.UVMapping;
THREE.Texture.prototype = {
    constructor: THREE.Texture,
    set needsUpdate(n) {
        n === !0 && this.version++
    },
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        return this.image = n.image, this.mipmaps = n.mipmaps.slice(0), this.mapping = n.mapping, this.wrapS = n.wrapS, this.wrapT = n.wrapT, this.magFilter = n.magFilter, this.minFilter = n.minFilter, this.anisotropy = n.anisotropy, this.format = n.format, this.type = n.type, this.offset.copy(n.offset), this.repeat.copy(n.repeat), this.generateMipmaps = n.generateMipmaps, this.premultiplyAlpha = n.premultiplyAlpha, this.flipY = n.flipY, this.unpackAlignment = n.unpackAlignment, this.encoding = n.encoding, this
    },
    toJSON: function(n) {
        function r(n) {
            var t;
            return n.toDataURL !== undefined ? t = n : (t = document.createElement("canvas"), t.width = n.width, t.height = n.height, t.getContext("2d").drawImage(n, 0, 0, n.width, n.height)), t.width > 2048 || t.height > 2048 ? t.toDataURL("image/jpeg", .6) : t.toDataURL("image/png")
        }
        var i, t;
        return n.textures[this.uuid] !== undefined ? n.textures[this.uuid] : (i = {
            metadata: {
                version: 4.4,
                type: "Texture",
                generator: "Texture.toJSON"
            },
            uuid: this.uuid,
            name: this.name,
            mapping: this.mapping,
            repeat: [this.repeat.x, this.repeat.y],
            offset: [this.offset.x, this.offset.y],
            wrap: [this.wrapS, this.wrapT],
            minFilter: this.minFilter,
            magFilter: this.magFilter,
            anisotropy: this.anisotropy
        }, this.image !== undefined && (t = this.image, t.uuid === undefined && (t.uuid = THREE.Math.generateUUID()), n.images[t.uuid] === undefined && (n.images[t.uuid] = {
            uuid: t.uuid,
            url: r(t)
        }), i.image = t.uuid), n.textures[this.uuid] = i, i)
    },
    dispose: function() {
        this.dispatchEvent({
            type: "dispose"
        })
    },
    transformUv: function(n) {
        if (this.mapping === THREE.UVMapping) {
            if (n.multiply(this.repeat), n.add(this.offset), n.x < 0 || n.x > 1) switch (this.wrapS) {
                case THREE.RepeatWrapping:
                    n.x = n.x - Math.floor(n.x);
                    break;
                case THREE.ClampToEdgeWrapping:
                    n.x = n.x < 0 ? 0 : 1;
                    break;
                case THREE.MirroredRepeatWrapping:
                    n.x = Math.abs(Math.floor(n.x) % 2) === 1 ? Math.ceil(n.x) - n.x : n.x - Math.floor(n.x)
            }
            if (n.y < 0 || n.y > 1) switch (this.wrapT) {
                case THREE.RepeatWrapping:
                    n.y = n.y - Math.floor(n.y);
                    break;
                case THREE.ClampToEdgeWrapping:
                    n.y = n.y < 0 ? 0 : 1;
                    break;
                case THREE.MirroredRepeatWrapping:
                    n.y = Math.abs(Math.floor(n.y) % 2) === 1 ? Math.ceil(n.y) - n.y : n.y - Math.floor(n.y)
            }
            this.flipY && (n.y = 1 - n.y)
        }
    }
};
THREE.EventDispatcher.prototype.apply(THREE.Texture.prototype);
THREE.TextureIdCount = 0;
THREE.DepthTexture = function(n, t, i, r, u, f, e, o, s) {
    THREE.Texture.call(this, null, r, u, f, e, o, THREE.DepthFormat, i, s);
    this.image = {
        width: n,
        height: t
    };
    this.type = i !== undefined ? i : THREE.UnsignedShortType;
    this.magFilter = e !== undefined ? e : THREE.NearestFilter;
    this.minFilter = o !== undefined ? o : THREE.NearestFilter;
    this.flipY = !1;
    this.generateMipmaps = !1
};
THREE.DepthTexture.prototype = Object.create(THREE.Texture.prototype);
THREE.DepthTexture.prototype.constructor = THREE.DepthTexture;
THREE.CanvasTexture = function(n, t, i, r, u, f, e, o, s) {
    THREE.Texture.call(this, n, t, i, r, u, f, e, o, s);
    this.needsUpdate = !0
};
THREE.CanvasTexture.prototype = Object.create(THREE.Texture.prototype);
THREE.CanvasTexture.prototype.constructor = THREE.CanvasTexture;
THREE.CubeTexture = function(n, t, i, r, u, f, e, o, s, h) {
    n = n !== undefined ? n : [];
    t = t !== undefined ? t : THREE.CubeReflectionMapping;
    THREE.Texture.call(this, n, t, i, r, u, f, e, o, s, h);
    this.flipY = !1
};
THREE.CubeTexture.prototype = Object.create(THREE.Texture.prototype);
THREE.CubeTexture.prototype.constructor = THREE.CubeTexture;
Object.defineProperty(THREE.CubeTexture.prototype, "images", {
    get: function() {
        return this.image
    },
    set: function(n) {
        this.image = n
    }
});
THREE.CompressedTexture = function(n, t, i, r, u, f, e, o, s, h, c, l) {
    THREE.Texture.call(this, null, f, e, o, s, h, r, u, c, l);
    this.image = {
        width: t,
        height: i
    };
    this.mipmaps = n;
    this.flipY = !1;
    this.generateMipmaps = !1
};
THREE.CompressedTexture.prototype = Object.create(THREE.Texture.prototype);
THREE.CompressedTexture.prototype.constructor = THREE.CompressedTexture;
THREE.DataTexture = function(n, t, i, r, u, f, e, o, s, h, c, l) {
    THREE.Texture.call(this, null, f, e, o, s, h, r, u, c, l);
    this.image = {
        data: n,
        width: t,
        height: i
    };
    this.magFilter = s !== undefined ? s : THREE.NearestFilter;
    this.minFilter = h !== undefined ? h : THREE.NearestFilter;
    this.flipY = !1;
    this.generateMipmaps = !1
};
THREE.DataTexture.prototype = Object.create(THREE.Texture.prototype);
THREE.DataTexture.prototype.constructor = THREE.DataTexture;
THREE.VideoTexture = function(n, t, i, r, u, f, e, o, s) {
    function h() {
        requestAnimationFrame(h);
        n.readyState >= n.HAVE_CURRENT_DATA && (c.needsUpdate = !0)
    }
    THREE.Texture.call(this, n, t, i, r, u, f, e, o, s);
    this.generateMipmaps = !1;
    var c = this;
    h()
};
THREE.VideoTexture.prototype = Object.create(THREE.Texture.prototype);
THREE.VideoTexture.prototype.constructor = THREE.VideoTexture;
THREE.Group = function() {
    THREE.Object3D.call(this);
    this.type = "Group"
};
THREE.Group.prototype = Object.create(THREE.Object3D.prototype);
THREE.Group.prototype.constructor = THREE.Group;
THREE.Points = function(n, t) {
    THREE.Object3D.call(this);
    this.type = "Points";
    this.geometry = n !== undefined ? n : new THREE.Geometry;
    this.material = t !== undefined ? t : new THREE.PointsMaterial({
        color: Math.random() * 16777215
    })
};
THREE.Points.prototype = Object.create(THREE.Object3D.prototype);
THREE.Points.prototype.constructor = THREE.Points;
THREE.Points.prototype.raycast = function() {
    var i = new THREE.Matrix4,
        n = new THREE.Ray,
        t = new THREE.Sphere;
    return function(r, u) {
        function c(t, i) {
            var o = n.distanceSqToPoint(t),
                f, e;
            if (o < g) {
                if (f = n.closestPointToPoint(t), f.applyMatrix4(h), e = r.ray.origin.distanceTo(f), e < r.near || e > r.far) return;
                u.push({
                    distance: e,
                    distanceToRay: Math.sqrt(o),
                    point: f.clone(),
                    index: i,
                    face: null,
                    object: k
                })
            }
        }
        var k = this,
            e = this.geometry,
            h = this.matrixWorld,
            d = r.params.Points.threshold,
            a, b, v, y, f, o;
        if (e.boundingSphere === null && e.computeBoundingSphere(), t.copy(e.boundingSphere), t.applyMatrix4(h), r.ray.intersectsSphere(t) !== !1) {
            i.getInverse(h);
            n.copy(r.ray).applyMatrix4(i);
            var p = d / ((this.scale.x + this.scale.y + this.scale.z) / 3),
                g = p * p,
                s = new THREE.Vector3;
            if (e instanceof THREE.BufferGeometry) {
                var w = e.index,
                    nt = e.attributes,
                    l = nt.position.array;
                if (w !== null)
                    for (a = w.array, f = 0, b = a.length; f < b; f++) v = a[f], s.fromArray(l, v * 3), c(s, v);
                else
                    for (f = 0, o = l.length / 3; f < o; f++) s.fromArray(l, f * 3), c(s, f)
            } else
                for (y = e.vertices, f = 0, o = y.length; f < o; f++) c(y[f], f)
        }
    }
}();
THREE.Points.prototype.clone = function() {
    return new this.constructor(this.geometry, this.material).copy(this)
};
THREE.Line = function(n, t, i) {
    if (i === 1) return console.warn("THREE.Line: parameter THREE.LinePieces no longer supported. Created THREE.LineSegments instead."), new THREE.LineSegments(n, t);
    THREE.Object3D.call(this);
    this.type = "Line";
    this.geometry = n !== undefined ? n : new THREE.Geometry;
    this.material = t !== undefined ? t : new THREE.LineBasicMaterial({
        color: Math.random() * 16777215
    })
};
THREE.Line.prototype = Object.create(THREE.Object3D.prototype);
THREE.Line.prototype.constructor = THREE.Line;
THREE.Line.prototype.raycast = function() {
    var i = new THREE.Matrix4,
        n = new THREE.Ray,
        t = new THREE.Sphere;
    return function(r, u) {
        var d = r.linePrecision,
            b = d * d,
            s = this.geometry,
            g = this.matrixWorld,
            p, tt, it, a, w, rt, f, h, e;
        if (s.boundingSphere === null && s.computeBoundingSphere(), t.copy(s.boundingSphere), t.applyMatrix4(g), r.ray.intersectsSphere(t) !== !1) {
            i.getInverse(g);
            n.copy(r.ray).applyMatrix4(i);
            var v = new THREE.Vector3,
                y = new THREE.Vector3,
                c = new THREE.Vector3,
                o = new THREE.Vector3,
                k = this instanceof THREE.LineSegments ? 2 : 1;
            if (s instanceof THREE.BufferGeometry) {
                var nt = s.index,
                    ut = s.attributes,
                    l = ut.position.array;
                if (nt !== null)
                    for (p = nt.array, f = 0, a = p.length - 1; f < a; f += k)(tt = p[f], it = p[f + 1], v.fromArray(l, tt * 3), y.fromArray(l, it * 3), h = n.distanceSqToSegment(v, y, o, c), h > b) || (o.applyMatrix4(this.matrixWorld), e = r.ray.origin.distanceTo(o), e < r.near || e > r.far) || u.push({
                        distance: e,
                        point: c.clone().applyMatrix4(this.matrixWorld),
                        index: f,
                        face: null,
                        faceIndex: null,
                        object: this
                    });
                else
                    for (f = 0, a = l.length / 3 - 1; f < a; f += k)(v.fromArray(l, 3 * f), y.fromArray(l, 3 * f + 3), h = n.distanceSqToSegment(v, y, o, c), h > b) || (o.applyMatrix4(this.matrixWorld), e = r.ray.origin.distanceTo(o), e < r.near || e > r.far) || u.push({
                        distance: e,
                        point: c.clone().applyMatrix4(this.matrixWorld),
                        index: f,
                        face: null,
                        faceIndex: null,
                        object: this
                    })
            } else if (s instanceof THREE.Geometry)
                for (w = s.vertices, rt = w.length, f = 0; f < rt - 1; f += k)(h = n.distanceSqToSegment(w[f], w[f + 1], o, c), h > b) || (o.applyMatrix4(this.matrixWorld), e = r.ray.origin.distanceTo(o), e < r.near || e > r.far) || u.push({
                    distance: e,
                    point: c.clone().applyMatrix4(this.matrixWorld),
                    index: f,
                    face: null,
                    faceIndex: null,
                    object: this
                })
        }
    }
}();
THREE.Line.prototype.clone = function() {
    return new this.constructor(this.geometry, this.material).copy(this)
};
THREE.LineStrip = 0;
THREE.LinePieces = 1;
THREE.LineSegments = function(n, t) {
    THREE.Line.call(this, n, t);
    this.type = "LineSegments"
};
THREE.LineSegments.prototype = Object.create(THREE.Line.prototype);
THREE.LineSegments.prototype.constructor = THREE.LineSegments;
THREE.Mesh = function(n, t) {
    THREE.Object3D.call(this);
    this.type = "Mesh";
    this.geometry = n !== undefined ? n : new THREE.Geometry;
    this.material = t !== undefined ? t : new THREE.MeshBasicMaterial({
        color: Math.random() * 16777215
    });
    this.drawMode = THREE.TrianglesDrawMode;
    this.updateMorphTargets()
};
THREE.Mesh.prototype = Object.create(THREE.Object3D.prototype);
THREE.Mesh.prototype.constructor = THREE.Mesh;
THREE.Mesh.prototype.setDrawMode = function(n) {
    this.drawMode = n
};
THREE.Mesh.prototype.updateMorphTargets = function() {
    if (this.geometry.morphTargets !== undefined && this.geometry.morphTargets.length > 0) {
        this.morphTargetBase = -1;
        this.morphTargetInfluences = [];
        this.morphTargetDictionary = {};
        for (var n = 0, t = this.geometry.morphTargets.length; n < t; n++) this.morphTargetInfluences.push(0), this.morphTargetDictionary[this.geometry.morphTargets[n].name] = n
    }
};
THREE.Mesh.prototype.getMorphTargetIndexByName = function(n) {
    return this.morphTargetDictionary[n] !== undefined ? this.morphTargetDictionary[n] : (console.warn("THREE.Mesh.getMorphTargetIndexByName: morph target " + n + " does not exist. Returning 0."), 0)
};
THREE.Mesh.prototype.raycast = function() {
    function a(n, t, i, r, u, f, e) {
        return THREE.Triangle.barycoordFromPoint(n, t, i, r, o), u.multiplyScalar(o.x), f.multiplyScalar(o.y), e.multiplyScalar(o.z), u.add(f).add(e), u.clone()
    }

    function v(n, t, i, r, u, f, e) {
        var s, c = n.material,
            o;
        return (s = c.side === THREE.BackSide ? i.intersectTriangle(f, u, r, !0, e) : i.intersectTriangle(r, u, f, c.side !== THREE.DoubleSide, e), s === null) ? null : (h.copy(e), h.applyMatrix4(n.matrixWorld), o = t.ray.origin.distanceTo(h), o < t.near || o > t.far) ? null : {
            distance: o,
            point: h.clone(),
            object: n
        }
    }

    function y(r, o, h, c, l, y, p, w) {
        n.fromArray(c, y * 3);
        t.fromArray(c, p * 3);
        i.fromArray(c, w * 3);
        var b = v(r, o, h, n, t, i, s);
        return b && (l && (u.fromArray(l, y * 2), f.fromArray(l, p * 2), e.fromArray(l, w * 2), b.uv = a(s, n, t, i, u, f, e)), b.face = new THREE.Face3(y, p, w, THREE.Triangle.normal(n, t, i)), b.faceIndex = y), b
    }
    var l = new THREE.Matrix4,
        r = new THREE.Ray,
        c = new THREE.Sphere,
        n = new THREE.Vector3,
        t = new THREE.Vector3,
        i = new THREE.Vector3,
        p = new THREE.Vector3,
        w = new THREE.Vector3,
        b = new THREE.Vector3,
        u = new THREE.Vector2,
        f = new THREE.Vector2,
        e = new THREE.Vector2,
        o = new THREE.Vector3,
        s = new THREE.Vector3,
        h = new THREE.Vector3;
    return function(o, h) {
        var d = this.geometry,
            lt = this.material,
            ni = this.matrixWorld,
            it, k, ot, g, st, et, fi, nt, dt, gt, ei, ht, oi, ct, yt, pt;
        if (lt !== undefined && (d.boundingSphere === null && d.computeBoundingSphere(), c.copy(d.boundingSphere), c.applyMatrix4(ni), o.ray.intersectsSphere(c) !== !1) && (l.getInverse(ni), r.copy(o.ray).applyMatrix4(l), d.boundingBox === null || r.intersectsBox(d.boundingBox) !== !1))
            if (d instanceof THREE.BufferGeometry) {
                var tt, at, vt, ti = d.index,
                    wt = d.attributes,
                    bt = wt.position.array;
                if (wt.uv !== undefined && (it = wt.uv.array), ti !== null)
                    for (ot = ti.array, g = 0, st = ot.length; g < st; g += 3) tt = ot[g], at = ot[g + 1], vt = ot[g + 2], k = y(this, o, r, bt, it, tt, at, vt), k && (k.faceIndex = Math.floor(g / 3), h.push(k));
                else
                    for (g = 0, st = bt.length; g < st; g += 9) tt = g / 3, at = tt + 1, vt = tt + 2, k = y(this, o, r, bt, it, tt, at, vt), k && (k.index = tt, h.push(k))
            } else if (d instanceof THREE.Geometry) {
            var rt, ut, ft, ii = lt instanceof THREE.MultiMaterial,
                si = ii === !0 ? lt.materials : null,
                kt = d.vertices,
                ri = d.faces,
                ui = d.faceVertexUvs[0];
            for (ui.length > 0 && (it = ui), et = 0, fi = ri.length; et < fi; et++)
                if (nt = ri[et], dt = ii === !0 ? si[nt.materialIndex] : lt, dt !== undefined) {
                    if (rt = kt[nt.a], ut = kt[nt.b], ft = kt[nt.c], dt.morphTargets === !0) {
                        for (gt = d.morphTargets, ei = this.morphTargetInfluences, n.set(0, 0, 0), t.set(0, 0, 0), i.set(0, 0, 0), ht = 0, oi = gt.length; ht < oi; ht++)(ct = ei[ht], ct !== 0) && (yt = gt[ht].vertices, n.addScaledVector(p.subVectors(yt[nt.a], rt), ct), t.addScaledVector(w.subVectors(yt[nt.b], ut), ct), i.addScaledVector(b.subVectors(yt[nt.c], ft), ct));
                        n.add(rt);
                        t.add(ut);
                        i.add(ft);
                        rt = n;
                        ut = t;
                        ft = i
                    }
                    k = v(this, o, r, rt, ut, ft, s);
                    k && (it && (pt = it[et], u.copy(pt[0]), f.copy(pt[1]), e.copy(pt[2]), k.uv = a(s, rt, ut, ft, u, f, e)), k.face = nt, k.faceIndex = et, h.push(k))
                }
        }
    }
}();
THREE.Mesh.prototype.clone = function() {
    return new this.constructor(this.geometry, this.material).copy(this)
};
THREE.Bone = function(n) {
    THREE.Object3D.call(this);
    this.type = "Bone";
    this.skin = n
};
THREE.Bone.prototype = Object.create(THREE.Object3D.prototype);
THREE.Bone.prototype.constructor = THREE.Bone;
THREE.Bone.prototype.copy = function(n) {
    return THREE.Object3D.prototype.copy.call(this, n), this.skin = n.skin, this
};
THREE.Skeleton = function(n, t, i) {
    var r, u, f;
    if (this.useVertexTexture = i !== undefined ? i : !0, this.identityMatrix = new THREE.Matrix4, n = n || [], this.bones = n.slice(0), this.useVertexTexture ? (r = Math.sqrt(this.bones.length * 4), r = THREE.Math.nextPowerOfTwo(Math.ceil(r)), r = Math.max(r, 4), this.boneTextureWidth = r, this.boneTextureHeight = r, this.boneMatrices = new Float32Array(this.boneTextureWidth * this.boneTextureHeight * 4), this.boneTexture = new THREE.DataTexture(this.boneMatrices, this.boneTextureWidth, this.boneTextureHeight, THREE.RGBAFormat, THREE.FloatType)) : this.boneMatrices = new Float32Array(16 * this.bones.length), t === undefined) this.calculateInverses();
    else if (this.bones.length === t.length) this.boneInverses = t.slice(0);
    else
        for (console.warn("THREE.Skeleton bonInverses is the wrong length."), this.boneInverses = [], u = 0, f = this.bones.length; u < f; u++) this.boneInverses.push(new THREE.Matrix4)
};
THREE.Skeleton.prototype.calculateInverses = function() {
    var n, i, t;
    for (this.boneInverses = [], n = 0, i = this.bones.length; n < i; n++) t = new THREE.Matrix4, this.bones[n] && t.getInverse(this.bones[n].matrixWorld), this.boneInverses.push(t)
};
THREE.Skeleton.prototype.pose = function() {
    for (var n, t = 0, i = this.bones.length; t < i; t++) n = this.bones[t], n && n.matrixWorld.getInverse(this.boneInverses[t]);
    for (t = 0, i = this.bones.length; t < i; t++) n = this.bones[t], n && (n.parent ? (n.matrix.getInverse(n.parent.matrixWorld), n.matrix.multiply(n.matrixWorld)) : n.matrix.copy(n.matrixWorld), n.matrix.decompose(n.position, n.quaternion, n.scale))
};
THREE.Skeleton.prototype.update = function() {
    var n = new THREE.Matrix4;
    return function() {
        for (var r, t = 0, i = this.bones.length; t < i; t++) r = this.bones[t] ? this.bones[t].matrixWorld : this.identityMatrix, n.multiplyMatrices(r, this.boneInverses[t]), n.toArray(this.boneMatrices, t * 16);
        this.useVertexTexture && (this.boneTexture.needsUpdate = !0)
    }
}();
THREE.Skeleton.prototype.clone = function() {
    return new THREE.Skeleton(this.bones, this.boneInverses, this.useVertexTexture)
};
THREE.SkinnedMesh = function(n, t, i) {
    var f, e, r, u, o;
    if (THREE.Mesh.call(this, n, t), this.type = "SkinnedMesh", this.bindMode = "attached", this.bindMatrix = new THREE.Matrix4, this.bindMatrixInverse = new THREE.Matrix4, f = [], this.geometry && this.geometry.bones !== undefined) {
        for (u = 0, o = this.geometry.bones.length; u < o; ++u) r = this.geometry.bones[u], e = new THREE.Bone(this), f.push(e), e.name = r.name, e.position.fromArray(r.pos), e.quaternion.fromArray(r.rotq), r.scl !== undefined && e.scale.fromArray(r.scl);
        for (u = 0, o = this.geometry.bones.length; u < o; ++u) r = this.geometry.bones[u], r.parent !== -1 && r.parent !== null && f[r.parent] !== undefined ? f[r.parent].add(f[u]) : this.add(f[u])
    }
    this.normalizeSkinWeights();
    this.updateMatrixWorld(!0);
    this.bind(new THREE.Skeleton(f, undefined, i), this.matrixWorld)
};
THREE.SkinnedMesh.prototype = Object.create(THREE.Mesh.prototype);
THREE.SkinnedMesh.prototype.constructor = THREE.SkinnedMesh;
THREE.SkinnedMesh.prototype.bind = function(n, t) {
    this.skeleton = n;
    t === undefined && (this.updateMatrixWorld(!0), this.skeleton.calculateInverses(), t = this.matrixWorld);
    this.bindMatrix.copy(t);
    this.bindMatrixInverse.getInverse(t)
};
THREE.SkinnedMesh.prototype.pose = function() {
    this.skeleton.pose()
};
THREE.SkinnedMesh.prototype.normalizeSkinWeights = function() {
    var u, t, r, n, i;
    if (this.geometry instanceof THREE.Geometry)
        for (n = 0; n < this.geometry.skinWeights.length; n++) u = this.geometry.skinWeights[n], i = 1 / u.lengthManhattan(), i !== Infinity ? u.multiplyScalar(i) : u.set(1, 0, 0, 0);
    else if (this.geometry instanceof THREE.BufferGeometry)
        for (t = new THREE.Vector4, r = this.geometry.attributes.skinWeight, n = 0; n < r.count; n++) t.x = r.getX(n), t.y = r.getY(n), t.z = r.getZ(n), t.w = r.getW(n), i = 1 / t.lengthManhattan(), i !== Infinity ? t.multiplyScalar(i) : t.set(1, 0, 0, 0), r.setXYZW(n, t.x, t.y, t.z, t.w)
};
THREE.SkinnedMesh.prototype.updateMatrixWorld = function() {
    THREE.Mesh.prototype.updateMatrixWorld.call(this, !0);
    this.bindMode === "attached" ? this.bindMatrixInverse.getInverse(this.matrixWorld) : this.bindMode === "detached" ? this.bindMatrixInverse.getInverse(this.bindMatrix) : console.warn("THREE.SkinnedMesh unrecognized bindMode: " + this.bindMode)
};
THREE.SkinnedMesh.prototype.clone = function() {
    return new this.constructor(this.geometry, this.material, this.useVertexTexture).copy(this)
};
THREE.LOD = function() {
    THREE.Object3D.call(this);
    this.type = "LOD";
    Object.defineProperties(this, {
        levels: {
            enumerable: !0,
            value: []
        }
    })
};
THREE.LOD.prototype = Object.create(THREE.Object3D.prototype);
THREE.LOD.prototype.constructor = THREE.LOD;
THREE.LOD.prototype.addLevel = function(n, t) {
    var r, i;
    for (t === undefined && (t = 0), t = Math.abs(t), r = this.levels, i = 0; i < r.length; i++)
        if (t < r[i].distance) break;
    r.splice(i, 0, {
        distance: t,
        object: n
    });
    this.add(n)
};
THREE.LOD.prototype.getObjectForDistance = function(n) {
    for (var i = this.levels, t = 1, r = i.length; t < r; t++)
        if (n < i[t].distance) break;
    return i[t - 1].object
};
THREE.LOD.prototype.raycast = function() {
    var n = new THREE.Vector3;
    return function(t, i) {
        n.setFromMatrixPosition(this.matrixWorld);
        var r = t.ray.origin.distanceTo(n);
        this.getObjectForDistance(r).raycast(t, i)
    }
}();
THREE.LOD.prototype.update = function() {
    var n = new THREE.Vector3,
        t = new THREE.Vector3;
    return function(i) {
        var u = this.levels,
            e, r, f;
        if (u.length > 1) {
            for (n.setFromMatrixPosition(i.matrixWorld), t.setFromMatrixPosition(this.matrixWorld), e = n.distanceTo(t), u[0].object.visible = !0, r = 1, f = u.length; r < f; r++)
                if (e >= u[r].distance) u[r - 1].object.visible = !1, u[r].object.visible = !0;
                else break;
            for (; r < f; r++) u[r].object.visible = !1
        }
    }
}();
THREE.LOD.prototype.copy = function(n) {
    var i, t, u, r;
    for (THREE.Object3D.prototype.copy.call(this, n, !1), i = n.levels, t = 0, u = i.length; t < u; t++) r = i[t], this.addLevel(r.object.clone(), r.distance);
    return this
};
THREE.LOD.prototype.toJSON = function(n) {
    var i = THREE.Object3D.prototype.toJSON.call(this, n),
        r, t, f, u;
    for (i.object.levels = [], r = this.levels, t = 0, f = r.length; t < f; t++) u = r[t], i.object.levels.push({
        object: u.object.uuid,
        distance: u.distance
    });
    return i
};
THREE.Sprite = function() {
    var t = new Uint16Array([0, 1, 2, 0, 2, 3]),
        i = new Float32Array([-.5, -.5, 0, .5, -.5, 0, .5, .5, 0, -.5, .5, 0]),
        r = new Float32Array([0, 0, 1, 0, 1, 1, 0, 1]),
        n = new THREE.BufferGeometry;
    return n.setIndex(new THREE.BufferAttribute(t, 1)), n.addAttribute("position", new THREE.BufferAttribute(i, 3)), n.addAttribute("uv", new THREE.BufferAttribute(r, 2)),
        function(t) {
            THREE.Object3D.call(this);
            this.type = "Sprite";
            this.geometry = n;
            this.material = t !== undefined ? t : new THREE.SpriteMaterial
        }
}();
THREE.Sprite.prototype = Object.create(THREE.Object3D.prototype);
THREE.Sprite.prototype.constructor = THREE.Sprite;
THREE.Sprite.prototype.raycast = function() {
    var n = new THREE.Vector3;
    return function(t, i) {
        n.setFromMatrixPosition(this.matrixWorld);
        var r = t.ray.distanceSqToPoint(n),
            u = this.scale.x * this.scale.y / 4;
        r > u || i.push({
            distance: Math.sqrt(r),
            point: this.position,
            face: null,
            object: this
        })
    }
}();
THREE.Sprite.prototype.clone = function() {
    return new this.constructor(this.material).copy(this)
};
THREE.Particle = THREE.Sprite;
THREE.LensFlare = function(n, t, i, r, u) {
    THREE.Object3D.call(this);
    this.lensFlares = [];
    this.positionScreen = new THREE.Vector3;
    this.customUpdateCallback = undefined;
    n !== undefined && this.add(n, t, i, r, u)
};
THREE.LensFlare.prototype = Object.create(THREE.Object3D.prototype);
THREE.LensFlare.prototype.constructor = THREE.LensFlare;
THREE.LensFlare.prototype.add = function(n, t, i, r, u, f) {
    t === undefined && (t = -1);
    i === undefined && (i = 0);
    f === undefined && (f = 1);
    u === undefined && (u = new THREE.Color(16777215));
    r === undefined && (r = THREE.NormalBlending);
    i = Math.min(i, Math.max(0, i));
    this.lensFlares.push({
        texture: n,
        size: t,
        distance: i,
        x: 0,
        y: 0,
        z: 0,
        scale: 1,
        rotation: 0,
        opacity: f,
        color: u,
        blending: r
    })
};
THREE.LensFlare.prototype.updateLensFlares = function() {
    for (var i = this.lensFlares.length, n, r = -this.positionScreen.x * 2, u = -this.positionScreen.y * 2, t = 0; t < i; t++) n = this.lensFlares[t], n.x = this.positionScreen.x + r * n.distance, n.y = this.positionScreen.y + u * n.distance, n.wantedRotation = n.x * Math.PI * .25, n.rotation += (n.wantedRotation - n.rotation) * .25
};
THREE.LensFlare.prototype.copy = function(n) {
    THREE.Object3D.prototype.copy.call(this, n);
    this.positionScreen.copy(n.positionScreen);
    this.customUpdateCallback = n.customUpdateCallback;
    for (var t = 0, i = n.lensFlares.length; t < i; t++) this.lensFlares.push(n.lensFlares[t]);
    return this
};
THREE.Scene = function() {
    THREE.Object3D.call(this);
    this.type = "Scene";
    this.fog = null;
    this.overrideMaterial = null;
    this.autoUpdate = !0
};
THREE.Scene.prototype = Object.create(THREE.Object3D.prototype);
THREE.Scene.prototype.constructor = THREE.Scene;
THREE.Scene.prototype.copy = function(n, t) {
    return THREE.Object3D.prototype.copy.call(this, n, t), n.fog !== null && (this.fog = n.fog.clone()), n.overrideMaterial !== null && (this.overrideMaterial = n.overrideMaterial.clone()), this.autoUpdate = n.autoUpdate, this.matrixAutoUpdate = n.matrixAutoUpdate, this
};
THREE.Fog = function(n, t, i) {
    this.name = "";
    this.color = new THREE.Color(n);
    this.near = t !== undefined ? t : 1;
    this.far = i !== undefined ? i : 1e3
};
THREE.Fog.prototype.clone = function() {
    return new THREE.Fog(this.color.getHex(), this.near, this.far)
};
THREE.FogExp2 = function(n, t) {
    this.name = "";
    this.color = new THREE.Color(n);
    this.density = t !== undefined ? t : .00025
};
THREE.FogExp2.prototype.clone = function() {
    return new THREE.FogExp2(this.color.getHex(), this.density)
};
THREE.ShaderChunk = {};
THREE.ShaderChunk.alphamap_fragment = "#ifdef USE_ALPHAMAP\n\tdiffuseColor.a *= texture2D( alphaMap, vUv ).g;\n#endif\n";
THREE.ShaderChunk.alphamap_pars_fragment = "#ifdef USE_ALPHAMAP\n\tuniform sampler2D alphaMap;\n#endif\n";
THREE.ShaderChunk.alphatest_fragment = "#ifdef ALPHATEST\n\tif ( diffuseColor.a < ALPHATEST ) discard;\n#endif\n";
THREE.ShaderChunk.aomap_fragment = "#ifdef USE_AOMAP\n\tfloat ambientOcclusion = ( texture2D( aoMap, vUv2 ).r - 1.0 ) * aoMapIntensity + 1.0;\n\treflectedLight.indirectDiffuse *= ambientOcclusion;\n\t#if defined( USE_ENVMAP ) && defined( PHYSICAL )\n\t\tfloat dotNV = saturate( dot( geometry.normal, geometry.viewDir ) );\n\t\treflectedLight.indirectSpecular *= computeSpecularOcclusion( dotNV, ambientOcclusion, material.specularRoughness );\n\t#endif\n#endif\n";
THREE.ShaderChunk.aomap_pars_fragment = "#ifdef USE_AOMAP\n\tuniform sampler2D aoMap;\n\tuniform float aoMapIntensity;\n#endif";
THREE.ShaderChunk.begin_vertex = "\nvec3 transformed = vec3( position );\n";
THREE.ShaderChunk.beginnormal_vertex = "\nvec3 objectNormal = vec3( normal );\n";
THREE.ShaderChunk.bsdfs = "bool testLightInRange( const in float lightDistance, const in float cutoffDistance ) {\n\treturn any( bvec2( cutoffDistance == 0.0, lightDistance < cutoffDistance ) );\n}\nfloat punctualLightIntensityToIrradianceFactor( const in float lightDistance, const in float cutoffDistance, const in float decayExponent ) {\n\t\tif( decayExponent > 0.0 ) {\n#if defined ( PHYSICALLY_CORRECT_LIGHTS )\n\t\t\tfloat distanceFalloff = 1.0 / max( pow( lightDistance, decayExponent ), 0.01 );\n\t\t\tfloat maxDistanceCutoffFactor = pow2( saturate( 1.0 - pow4( lightDistance / cutoffDistance ) ) );\n\t\t\treturn distanceFalloff * maxDistanceCutoffFactor;\n#else\n\t\t\treturn pow( saturate( -lightDistance / cutoffDistance + 1.0 ), decayExponent );\n#endif\n\t\t}\n\t\treturn 1.0;\n}\nvec3 BRDF_Diffuse_Lambert( const in vec3 diffuseColor ) {\n\treturn RECIPROCAL_PI * diffuseColor;\n}\nvec3 F_Schlick( const in vec3 specularColor, const in float dotLH ) {\n\tfloat fresnel = exp2( ( -5.55473 * dotLH - 6.98316 ) * dotLH );\n\treturn ( 1.0 - specularColor ) * fresnel + specularColor;\n}\nfloat G_GGX_Smith( const in float alpha, const in float dotNL, const in float dotNV ) {\n\tfloat a2 = pow2( alpha );\n\tfloat gl = dotNL + sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNL ) );\n\tfloat gv = dotNV + sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNV ) );\n\treturn 1.0 / ( gl * gv );\n}\nfloat G_GGX_SmithCorrelated( const in float alpha, const in float dotNL, const in float dotNV ) {\n\tfloat a2 = pow2( alpha );\n\tfloat gv = dotNL * sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNV ) );\n\tfloat gl = dotNV * sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNL ) );\n\treturn 0.5 / max( gv + gl, EPSILON );\n}\nfloat D_GGX( const in float alpha, const in float dotNH ) {\n\tfloat a2 = pow2( alpha );\n\tfloat denom = pow2( dotNH ) * ( a2 - 1.0 ) + 1.0;\n\treturn RECIPROCAL_PI * a2 / pow2( denom );\n}\nvec3 BRDF_Specular_GGX( const in IncidentLight incidentLight, const in GeometricContext geometry, const in vec3 specularColor, const in float roughness ) {\n\tfloat alpha = pow2( roughness );\n\tvec3 halfDir = normalize( incidentLight.direction + geometry.viewDir );\n\tfloat dotNL = saturate( dot( geometry.normal, incidentLight.direction ) );\n\tfloat dotNV = saturate( dot( geometry.normal, geometry.viewDir ) );\n\tfloat dotNH = saturate( dot( geometry.normal, halfDir ) );\n\tfloat dotLH = saturate( dot( incidentLight.direction, halfDir ) );\n\tvec3 F = F_Schlick( specularColor, dotLH );\n\tfloat G = G_GGX_SmithCorrelated( alpha, dotNL, dotNV );\n\tfloat D = D_GGX( alpha, dotNH );\n\treturn F * ( G * D );\n}\nvec3 BRDF_Specular_GGX_Environment( const in GeometricContext geometry, const in vec3 specularColor, const in float roughness ) {\n\tfloat dotNV = saturate( dot( geometry.normal, geometry.viewDir ) );\n\tconst vec4 c0 = vec4( - 1, - 0.0275, - 0.572, 0.022 );\n\tconst vec4 c1 = vec4( 1, 0.0425, 1.04, - 0.04 );\n\tvec4 r = roughness * c0 + c1;\n\tfloat a004 = min( r.x * r.x, exp2( - 9.28 * dotNV ) ) * r.x + r.y;\n\tvec2 AB = vec2( -1.04, 1.04 ) * a004 + r.zw;\n\treturn specularColor * AB.x + AB.y;\n}\nfloat G_BlinnPhong_Implicit( ) {\n\treturn 0.25;\n}\nfloat D_BlinnPhong( const in float shininess, const in float dotNH ) {\n\treturn RECIPROCAL_PI * ( shininess * 0.5 + 1.0 ) * pow( dotNH, shininess );\n}\nvec3 BRDF_Specular_BlinnPhong( const in IncidentLight incidentLight, const in GeometricContext geometry, const in vec3 specularColor, const in float shininess ) {\n\tvec3 halfDir = normalize( incidentLight.direction + geometry.viewDir );\n\tfloat dotNH = saturate( dot( geometry.normal, halfDir ) );\n\tfloat dotLH = saturate( dot( incidentLight.direction, halfDir ) );\n\tvec3 F = F_Schlick( specularColor, dotLH );\n\tfloat G = G_BlinnPhong_Implicit( );\n\tfloat D = D_BlinnPhong( shininess, dotNH );\n\treturn F * ( G * D );\n}\nfloat GGXRoughnessToBlinnExponent( const in float ggxRoughness ) {\n\treturn ( 2.0 / pow2( ggxRoughness + 0.0001 ) - 2.0 );\n}\nfloat BlinnExponentToGGXRoughness( const in float blinnExponent ) {\n\treturn sqrt( 2.0 / ( blinnExponent + 2.0 ) );\n}\n";
THREE.ShaderChunk.bumpmap_pars_fragment = "#ifdef USE_BUMPMAP\n\tuniform sampler2D bumpMap;\n\tuniform float bumpScale;\n\tvec2 dHdxy_fwd() {\n\t\tvec2 dSTdx = dFdx( vUv );\n\t\tvec2 dSTdy = dFdy( vUv );\n\t\tfloat Hll = bumpScale * texture2D( bumpMap, vUv ).x;\n\t\tfloat dBx = bumpScale * texture2D( bumpMap, vUv + dSTdx ).x - Hll;\n\t\tfloat dBy = bumpScale * texture2D( bumpMap, vUv + dSTdy ).x - Hll;\n\t\treturn vec2( dBx, dBy );\n\t}\n\tvec3 perturbNormalArb( vec3 surf_pos, vec3 surf_norm, vec2 dHdxy ) {\n\t\tvec3 vSigmaX = dFdx( surf_pos );\n\t\tvec3 vSigmaY = dFdy( surf_pos );\n\t\tvec3 vN = surf_norm;\n\t\tvec3 R1 = cross( vSigmaY, vN );\n\t\tvec3 R2 = cross( vN, vSigmaX );\n\t\tfloat fDet = dot( vSigmaX, R1 );\n\t\tvec3 vGrad = sign( fDet ) * ( dHdxy.x * R1 + dHdxy.y * R2 );\n\t\treturn normalize( abs( fDet ) * surf_norm - vGrad );\n\t}\n#endif\n";
THREE.ShaderChunk.clipping_planes_fragment = "#if NUM_CLIPPING_PLANES > 0\n\tfor ( int i = 0; i < NUM_CLIPPING_PLANES; ++ i ) {\n\t\tvec4 plane = clippingPlanes[ i ];\n\t\tif ( dot( vViewPosition, plane.xyz ) > plane.w ) discard;\n\t}\n#endif\n";
THREE.ShaderChunk.clipping_planes_pars_fragment = "#if NUM_CLIPPING_PLANES > 0\n\t#if ! defined( PHYSICAL ) && ! defined( PHONG )\n\t\tvarying vec3 vViewPosition;\n\t#endif\n\tuniform vec4 clippingPlanes[ NUM_CLIPPING_PLANES ];\n#endif\n";
THREE.ShaderChunk.clipping_planes_pars_vertex = "#if NUM_CLIPPING_PLANES > 0 && ! defined( PHYSICAL ) && ! defined( PHONG )\n\tvarying vec3 vViewPosition;\n#endif\n";
THREE.ShaderChunk.clipping_planes_vertex = "#if NUM_CLIPPING_PLANES > 0 && ! defined( PHYSICAL ) && ! defined( PHONG )\n\tvViewPosition = - mvPosition.xyz;\n#endif\n";
THREE.ShaderChunk.color_fragment = "#ifdef USE_COLOR\n\tdiffuseColor.rgb *= vColor;\n#endif";
THREE.ShaderChunk.color_pars_fragment = "#ifdef USE_COLOR\n\tvarying vec3 vColor;\n#endif\n";
THREE.ShaderChunk.color_pars_vertex = "#ifdef USE_COLOR\n\tvarying vec3 vColor;\n#endif";
THREE.ShaderChunk.color_vertex = "#ifdef USE_COLOR\n\tvColor.xyz = color.xyz;\n#endif";
THREE.ShaderChunk.common = "#define PI 3.14159265359\n#define PI2 6.28318530718\n#define RECIPROCAL_PI 0.31830988618\n#define RECIPROCAL_PI2 0.15915494\n#define LOG2 1.442695\n#define EPSILON 1e-6\n#define saturate(a) clamp( a, 0.0, 1.0 )\n#define whiteCompliment(a) ( 1.0 - saturate( a ) )\nfloat pow2( const in float x ) { return x*x; }\nfloat pow3( const in float x ) { return x*x*x; }\nfloat pow4( const in float x ) { float x2 = x*x; return x2*x2; }\nfloat average( const in vec3 color ) { return dot( color, vec3( 0.3333 ) ); }\nhighp float rand( const in vec2 uv ) {\n\tconst highp float a = 12.9898, b = 78.233, c = 43758.5453;\n\thighp float dt = dot( uv.xy, vec2( a,b ) ), sn = mod( dt, PI );\n\treturn fract(sin(sn) * c);\n}\nstruct IncidentLight {\n\tvec3 color;\n\tvec3 direction;\n\tbool visible;\n};\nstruct ReflectedLight {\n\tvec3 directDiffuse;\n\tvec3 directSpecular;\n\tvec3 indirectDiffuse;\n\tvec3 indirectSpecular;\n};\nstruct GeometricContext {\n\tvec3 position;\n\tvec3 normal;\n\tvec3 viewDir;\n};\nvec3 transformDirection( in vec3 dir, in mat4 matrix ) {\n\treturn normalize( ( matrix * vec4( dir, 0.0 ) ).xyz );\n}\nvec3 inverseTransformDirection( in vec3 dir, in mat4 matrix ) {\n\treturn normalize( ( vec4( dir, 0.0 ) * matrix ).xyz );\n}\nvec3 projectOnPlane(in vec3 point, in vec3 pointOnPlane, in vec3 planeNormal ) {\n\tfloat distance = dot( planeNormal, point - pointOnPlane );\n\treturn - distance * planeNormal + point;\n}\nfloat sideOfPlane( in vec3 point, in vec3 pointOnPlane, in vec3 planeNormal ) {\n\treturn sign( dot( point - pointOnPlane, planeNormal ) );\n}\nvec3 linePlaneIntersect( in vec3 pointOnLine, in vec3 lineDirection, in vec3 pointOnPlane, in vec3 planeNormal ) {\n\treturn lineDirection * ( dot( planeNormal, pointOnPlane - pointOnLine ) / dot( planeNormal, lineDirection ) ) + pointOnLine;\n}\n";
THREE.ShaderChunk.cube_uv_reflection_fragment = "#ifdef ENVMAP_TYPE_CUBE_UV\nconst float cubeUV_textureSize = 1024.0;\nint getFaceFromDirection(vec3 direction) {\n\tvec3 absDirection = abs(direction);\n\tint face = -1;\n\tif( absDirection.x > absDirection.z ) {\n\t\tif(absDirection.x > absDirection.y )\n\t\t\tface = direction.x > 0.0 ? 0 : 3;\n\t\telse\n\t\t\tface = direction.y > 0.0 ? 1 : 4;\n\t}\n\telse {\n\t\tif(absDirection.z > absDirection.y )\n\t\t\tface = direction.z > 0.0 ? 2 : 5;\n\t\telse\n\t\t\tface = direction.y > 0.0 ? 1 : 4;\n\t}\n\treturn face;\n}\nfloat cubeUV_maxLods1 = log2(cubeUV_textureSize*0.25) - 1.0;\nfloat cubeUV_rangeClamp = exp2((6.0 - 1.0) * 2.0);\nvec2 MipLevelInfo( vec3 vec, float roughnessLevel, float roughness ) {\n\tfloat scale = exp2(cubeUV_maxLods1 - roughnessLevel);\n\tfloat dxRoughness = dFdx(roughness);\n\tfloat dyRoughness = dFdy(roughness);\n\tvec3 dx = dFdx( vec * scale * dxRoughness );\n\tvec3 dy = dFdy( vec * scale * dyRoughness );\n\tfloat d = max( dot( dx, dx ), dot( dy, dy ) );\n\td = clamp(d, 1.0, cubeUV_rangeClamp);\n\tfloat mipLevel = 0.5 * log2(d);\n\treturn vec2(floor(mipLevel), fract(mipLevel));\n}\nfloat cubeUV_maxLods2 = log2(cubeUV_textureSize*0.25) - 2.0;\nconst float cubeUV_rcpTextureSize = 1.0 / cubeUV_textureSize;\nvec2 getCubeUV(vec3 direction, float roughnessLevel, float mipLevel) {\n\tmipLevel = roughnessLevel > cubeUV_maxLods2 - 3.0 ? 0.0 : mipLevel;\n\tfloat a = 16.0 * cubeUV_rcpTextureSize;\n\tvec2 exp2_packed = exp2( vec2( roughnessLevel, mipLevel ) );\n\tvec2 rcp_exp2_packed = vec2( 1.0 ) / exp2_packed;\n\tfloat powScale = exp2_packed.x * exp2_packed.y;\n\tfloat scale = rcp_exp2_packed.x * rcp_exp2_packed.y * 0.25;\n\tfloat mipOffset = 0.75*(1.0 - rcp_exp2_packed.y) * rcp_exp2_packed.x;\n\tbool bRes = mipLevel == 0.0;\n\tscale =  bRes && (scale < a) ? a : scale;\n\tvec3 r;\n\tvec2 offset;\n\tint face = getFaceFromDirection(direction);\n\tfloat rcpPowScale = 1.0 / powScale;\n\tif( face == 0) {\n\t\tr = vec3(direction.x, -direction.z, direction.y);\n\t\toffset = vec2(0.0+mipOffset,0.75 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  a : offset.y;\n\t}\n\telse if( face == 1) {\n\t\tr = vec3(direction.y, direction.x, direction.z);\n\t\toffset = vec2(scale+mipOffset, 0.75 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  a : offset.y;\n\t}\n\telse if( face == 2) {\n\t\tr = vec3(direction.z, direction.x, direction.y);\n\t\toffset = vec2(2.0*scale+mipOffset, 0.75 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  a : offset.y;\n\t}\n\telse if( face == 3) {\n\t\tr = vec3(direction.x, direction.z, direction.y);\n\t\toffset = vec2(0.0+mipOffset,0.5 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  0.0 : offset.y;\n\t}\n\telse if( face == 4) {\n\t\tr = vec3(direction.y, direction.x, -direction.z);\n\t\toffset = vec2(scale+mipOffset, 0.5 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  0.0 : offset.y;\n\t}\n\telse {\n\t\tr = vec3(direction.z, -direction.x, direction.y);\n\t\toffset = vec2(2.0*scale+mipOffset, 0.5 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  0.0 : offset.y;\n\t}\n\tr = normalize(r);\n\tfloat texelOffset = 0.5 * cubeUV_rcpTextureSize;\n\tvec2 s = ( r.yz / abs( r.x ) + vec2( 1.0 ) ) * 0.5;\n\tvec2 base = offset + vec2( texelOffset );\n\treturn base + s * ( scale - 2.0 * texelOffset );\n}\nfloat cubeUV_maxLods3 = log2(cubeUV_textureSize*0.25) - 3.0;\nvec4 textureCubeUV(vec3 reflectedDirection, float roughness ) {\n\tfloat roughnessVal = roughness* cubeUV_maxLods3;\n\tfloat r1 = floor(roughnessVal);\n\tfloat r2 = r1 + 1.0;\n\tfloat t = fract(roughnessVal);\n\tvec2 mipInfo = MipLevelInfo(reflectedDirection, r1, roughness);\n\tfloat s = mipInfo.y;\n\tfloat level0 = mipInfo.x;\n\tfloat level1 = level0 + 1.0;\n\tlevel1 = level1 > 5.0 ? 5.0 : level1;\n\tlevel0 += min( floor( s + 0.5 ), 5.0 );\n\tvec2 uv_10 = getCubeUV(reflectedDirection, r1, level0);\n\tvec4 color10 = envMapTexelToLinear(texture2D(envMap, uv_10));\n\tvec2 uv_20 = getCubeUV(reflectedDirection, r2, level0);\n\tvec4 color20 = envMapTexelToLinear(texture2D(envMap, uv_20));\n\tvec4 result = mix(color10, color20, t);\n\treturn vec4(result.rgb, 1.0);\n}\n#endif\n";
THREE.ShaderChunk.defaultnormal_vertex = "#ifdef FLIP_SIDED\n\tobjectNormal = -objectNormal;\n#endif\nvec3 transformedNormal = normalMatrix * objectNormal;\n";
THREE.ShaderChunk.displacementmap_vertex = "#ifdef USE_DISPLACEMENTMAP\n\ttransformed += normal * ( texture2D( displacementMap, uv ).x * displacementScale + displacementBias );\n#endif\n";
THREE.ShaderChunk.displacementmap_pars_vertex = "#ifdef USE_DISPLACEMENTMAP\n\tuniform sampler2D displacementMap;\n\tuniform float displacementScale;\n\tuniform float displacementBias;\n#endif\n";
THREE.ShaderChunk.emissivemap_fragment = "#ifdef USE_EMISSIVEMAP\n\tvec4 emissiveColor = texture2D( emissiveMap, vUv );\n\temissiveColor.rgb = emissiveMapTexelToLinear( emissiveColor ).rgb;\n\ttotalEmissiveRadiance *= emissiveColor.rgb;\n#endif\n";
THREE.ShaderChunk.emissivemap_pars_fragment = "#ifdef USE_EMISSIVEMAP\n\tuniform sampler2D emissiveMap;\n#endif\n";
THREE.ShaderChunk.encodings_pars_fragment = "\nvec4 LinearToLinear( in vec4 value ) {\n  return value;\n}\nvec4 GammaToLinear( in vec4 value, in float gammaFactor ) {\n  return vec4( pow( value.xyz, vec3( gammaFactor ) ), value.w );\n}\nvec4 LinearToGamma( in vec4 value, in float gammaFactor ) {\n  return vec4( pow( value.xyz, vec3( 1.0 / gammaFactor ) ), value.w );\n}\nvec4 sRGBToLinear( in vec4 value ) {\n  return vec4( mix( pow( value.rgb * 0.9478672986 + vec3( 0.0521327014 ), vec3( 2.4 ) ), value.rgb * 0.0773993808, vec3( lessThanEqual( value.rgb, vec3( 0.04045 ) ) ) ), value.w );\n}\nvec4 LinearTosRGB( in vec4 value ) {\n  return vec4( mix( pow( value.rgb, vec3( 0.41666 ) ) * 1.055 - vec3( 0.055 ), value.rgb * 12.92, vec3( lessThanEqual( value.rgb, vec3( 0.0031308 ) ) ) ), value.w );\n}\nvec4 RGBEToLinear( in vec4 value ) {\n  return vec4( value.rgb * exp2( value.a * 255.0 - 128.0 ), 1.0 );\n}\nvec4 LinearToRGBE( in vec4 value ) {\n  float maxComponent = max( max( value.r, value.g ), value.b );\n  float fExp = clamp( ceil( log2( maxComponent ) ), -128.0, 127.0 );\n  return vec4( value.rgb / exp2( fExp ), ( fExp + 128.0 ) / 255.0 );\n}\nvec4 RGBMToLinear( in vec4 value, in float maxRange ) {\n  return vec4( value.xyz * value.w * maxRange, 1.0 );\n}\nvec4 LinearToRGBM( in vec4 value, in float maxRange ) {\n  float maxRGB = max( value.x, max( value.g, value.b ) );\n  float M      = clamp( maxRGB / maxRange, 0.0, 1.0 );\n  M            = ceil( M * 255.0 ) / 255.0;\n  return vec4( value.rgb / ( M * maxRange ), M );\n}\nvec4 RGBDToLinear( in vec4 value, in float maxRange ) {\n    return vec4( value.rgb * ( ( maxRange / 255.0 ) / value.a ), 1.0 );\n}\nvec4 LinearToRGBD( in vec4 value, in float maxRange ) {\n    float maxRGB = max( value.x, max( value.g, value.b ) );\n    float D      = max( maxRange / maxRGB, 1.0 );\n    D            = min( floor( D ) / 255.0, 1.0 );\n    return vec4( value.rgb * ( D * ( 255.0 / maxRange ) ), D );\n}\nconst mat3 cLogLuvM = mat3( 0.2209, 0.3390, 0.4184, 0.1138, 0.6780, 0.7319, 0.0102, 0.1130, 0.2969 );\nvec4 LinearToLogLuv( in vec4 value )  {\n  vec3 Xp_Y_XYZp = value.rgb * cLogLuvM;\n  Xp_Y_XYZp = max(Xp_Y_XYZp, vec3(1e-6, 1e-6, 1e-6));\n  vec4 vResult;\n  vResult.xy = Xp_Y_XYZp.xy / Xp_Y_XYZp.z;\n  float Le = 2.0 * log2(Xp_Y_XYZp.y) + 127.0;\n  vResult.w = fract(Le);\n  vResult.z = (Le - (floor(vResult.w*255.0))/255.0)/255.0;\n  return vResult;\n}\nconst mat3 cLogLuvInverseM = mat3( 6.0014, -2.7008, -1.7996, -1.3320, 3.1029, -5.7721, 0.3008, -1.0882, 5.6268 );\nvec4 LogLuvToLinear( in vec4 value ) {\n  float Le = value.z * 255.0 + value.w;\n  vec3 Xp_Y_XYZp;\n  Xp_Y_XYZp.y = exp2((Le - 127.0) / 2.0);\n  Xp_Y_XYZp.z = Xp_Y_XYZp.y / value.y;\n  Xp_Y_XYZp.x = value.x * Xp_Y_XYZp.z;\n  vec3 vRGB = Xp_Y_XYZp.rgb * cLogLuvInverseM;\n  return vec4( max(vRGB, 0.0), 1.0 );\n}\n";
THREE.ShaderChunk.encodings_fragment = "  gl_FragColor = linearToOutputTexel( gl_FragColor );\n";
THREE.ShaderChunk.envmap_fragment = "#ifdef USE_ENVMAP\n\t#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG )\n\t\tvec3 cameraToVertex = normalize( vWorldPosition - cameraPosition );\n\t\tvec3 worldNormal = inverseTransformDirection( normal, viewMatrix );\n\t\t#ifdef ENVMAP_MODE_REFLECTION\n\t\t\tvec3 reflectVec = reflect( cameraToVertex, worldNormal );\n\t\t#else\n\t\t\tvec3 reflectVec = refract( cameraToVertex, worldNormal, refractionRatio );\n\t\t#endif\n\t#else\n\t\tvec3 reflectVec = vReflect;\n\t#endif\n\t#ifdef DOUBLE_SIDED\n\t\tfloat flipNormal = ( float( gl_FrontFacing ) * 2.0 - 1.0 );\n\t#else\n\t\tfloat flipNormal = 1.0;\n\t#endif\n\t#ifdef ENVMAP_TYPE_CUBE\n\t\tvec4 envColor = textureCube( envMap, flipNormal * vec3( flipEnvMap * reflectVec.x, reflectVec.yz ) );\n\t#elif defined( ENVMAP_TYPE_EQUIREC )\n\t\tvec2 sampleUV;\n\t\tsampleUV.y = saturate( flipNormal * reflectVec.y * 0.5 + 0.5 );\n\t\tsampleUV.x = atan( flipNormal * reflectVec.z, flipNormal * reflectVec.x ) * RECIPROCAL_PI2 + 0.5;\n\t\tvec4 envColor = texture2D( envMap, sampleUV );\n\t#elif defined( ENVMAP_TYPE_SPHERE )\n\t\tvec3 reflectView = flipNormal * normalize((viewMatrix * vec4( reflectVec, 0.0 )).xyz + vec3(0.0,0.0,1.0));\n\t\tvec4 envColor = texture2D( envMap, reflectView.xy * 0.5 + 0.5 );\n\t#endif\n\tenvColor = envMapTexelToLinear( envColor );\n\t#ifdef ENVMAP_BLENDING_MULTIPLY\n\t\toutgoingLight = mix( outgoingLight, outgoingLight * envColor.xyz, specularStrength * reflectivity );\n\t#elif defined( ENVMAP_BLENDING_MIX )\n\t\toutgoingLight = mix( outgoingLight, envColor.xyz, specularStrength * reflectivity );\n\t#elif defined( ENVMAP_BLENDING_ADD )\n\t\toutgoingLight += envColor.xyz * specularStrength * reflectivity;\n\t#endif\n#endif\n";
THREE.ShaderChunk.envmap_pars_fragment = "#if defined( USE_ENVMAP ) || defined( PHYSICAL )\n\tuniform float reflectivity;\n\tuniform float envMapIntenstiy;\n#endif\n#ifdef USE_ENVMAP\n\t#if ! defined( PHYSICAL ) && ( defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG ) )\n\t\tvarying vec3 vWorldPosition;\n\t#endif\n\t#ifdef ENVMAP_TYPE_CUBE\n\t\tuniform samplerCube envMap;\n\t#else\n\t\tuniform sampler2D envMap;\n\t#endif\n\tuniform float flipEnvMap;\n\t#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG ) || defined( PHYSICAL )\n\t\tuniform float refractionRatio;\n\t#else\n\t\tvarying vec3 vReflect;\n\t#endif\n#endif\n";
THREE.ShaderChunk.envmap_pars_vertex = "#ifdef USE_ENVMAP\n\t#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG )\n\t\tvarying vec3 vWorldPosition;\n\t#else\n\t\tvarying vec3 vReflect;\n\t\tuniform float refractionRatio;\n\t#endif\n#endif\n";
THREE.ShaderChunk.envmap_vertex = "#ifdef USE_ENVMAP\n\t#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG )\n\t\tvWorldPosition = worldPosition.xyz;\n\t#else\n\t\tvec3 cameraToVertex = normalize( worldPosition.xyz - cameraPosition );\n\t\tvec3 worldNormal = inverseTransformDirection( transformedNormal, viewMatrix );\n\t\t#ifdef ENVMAP_MODE_REFLECTION\n\t\t\tvReflect = reflect( cameraToVertex, worldNormal );\n\t\t#else\n\t\t\tvReflect = refract( cameraToVertex, worldNormal, refractionRatio );\n\t\t#endif\n\t#endif\n#endif\n";
THREE.ShaderChunk.fog_fragment = "#ifdef USE_FOG\n\t#ifdef USE_LOGDEPTHBUF_EXT\n\t\tfloat depth = gl_FragDepthEXT / gl_FragCoord.w;\n\t#else\n\t\tfloat depth = gl_FragCoord.z / gl_FragCoord.w;\n\t#endif\n\t#ifdef FOG_EXP2\n\t\tfloat fogFactor = whiteCompliment( exp2( - fogDensity * fogDensity * depth * depth * LOG2 ) );\n\t#else\n\t\tfloat fogFactor = smoothstep( fogNear, fogFar, depth );\n\t#endif\n\tgl_FragColor.rgb = mix( gl_FragColor.rgb, fogColor, fogFactor );\n#endif\n";
THREE.ShaderChunk.fog_pars_fragment = "#ifdef USE_FOG\n\tuniform vec3 fogColor;\n\t#ifdef FOG_EXP2\n\t\tuniform float fogDensity;\n\t#else\n\t\tuniform float fogNear;\n\t\tuniform float fogFar;\n\t#endif\n#endif";
THREE.ShaderChunk.lightmap_fragment = "#ifdef USE_LIGHTMAP\n\treflectedLight.indirectDiffuse += PI * texture2D( lightMap, vUv2 ).xyz * lightMapIntensity;\n#endif\n";
THREE.ShaderChunk.lightmap_pars_fragment = "#ifdef USE_LIGHTMAP\n\tuniform sampler2D lightMap;\n\tuniform float lightMapIntensity;\n#endif";
THREE.ShaderChunk.lights_lambert_vertex = "vec3 diffuse = vec3( 1.0 );\nGeometricContext geometry;\ngeometry.position = mvPosition.xyz;\ngeometry.normal = normalize( transformedNormal );\ngeometry.viewDir = normalize( -mvPosition.xyz );\nGeometricContext backGeometry;\nbackGeometry.position = geometry.position;\nbackGeometry.normal = -geometry.normal;\nbackGeometry.viewDir = geometry.viewDir;\nvLightFront = vec3( 0.0 );\n#ifdef DOUBLE_SIDED\n\tvLightBack = vec3( 0.0 );\n#endif\nIncidentLight directLight;\nfloat dotNL;\nvec3 directLightColor_Diffuse;\n#if NUM_POINT_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_POINT_LIGHTS; i ++ ) {\n\t\tgetPointDirectLightIrradiance( pointLights[ i ], geometry, directLight );\n\t\tdotNL = dot( geometry.normal, directLight.direction );\n\t\tdirectLightColor_Diffuse = PI * directLight.color;\n\t\tvLightFront += saturate( dotNL ) * directLightColor_Diffuse;\n\t\t#ifdef DOUBLE_SIDED\n\t\t\tvLightBack += saturate( -dotNL ) * directLightColor_Diffuse;\n\t\t#endif\n\t}\n#endif\n#if NUM_SPOT_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_SPOT_LIGHTS; i ++ ) {\n\t\tgetSpotDirectLightIrradiance( spotLights[ i ], geometry, directLight );\n\t\tdotNL = dot( geometry.normal, directLight.direction );\n\t\tdirectLightColor_Diffuse = PI * directLight.color;\n\t\tvLightFront += saturate( dotNL ) * directLightColor_Diffuse;\n\t\t#ifdef DOUBLE_SIDED\n\t\t\tvLightBack += saturate( -dotNL ) * directLightColor_Diffuse;\n\t\t#endif\n\t}\n#endif\n#if NUM_DIR_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_DIR_LIGHTS; i ++ ) {\n\t\tgetDirectionalDirectLightIrradiance( directionalLights[ i ], geometry, directLight );\n\t\tdotNL = dot( geometry.normal, directLight.direction );\n\t\tdirectLightColor_Diffuse = PI * directLight.color;\n\t\tvLightFront += saturate( dotNL ) * directLightColor_Diffuse;\n\t\t#ifdef DOUBLE_SIDED\n\t\t\tvLightBack += saturate( -dotNL ) * directLightColor_Diffuse;\n\t\t#endif\n\t}\n#endif\n#if NUM_HEMI_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_HEMI_LIGHTS; i ++ ) {\n\t\tvLightFront += getHemisphereLightIrradiance( hemisphereLights[ i ], geometry );\n\t\t#ifdef DOUBLE_SIDED\n\t\t\tvLightBack += getHemisphereLightIrradiance( hemisphereLights[ i ], backGeometry );\n\t\t#endif\n\t}\n#endif\n";
THREE.ShaderChunk.lights_pars = "uniform vec3 ambientLightColor;\nvec3 getAmbientLightIrradiance( const in vec3 ambientLightColor ) {\n\tvec3 irradiance = ambientLightColor;\n\t#ifndef PHYSICALLY_CORRECT_LIGHTS\n\t\tirradiance *= PI;\n\t#endif\n\treturn irradiance;\n}\n#if NUM_DIR_LIGHTS > 0\n\tstruct DirectionalLight {\n\t\tvec3 direction;\n\t\tvec3 color;\n\t\tint shadow;\n\t\tfloat shadowBias;\n\t\tfloat shadowRadius;\n\t\tvec2 shadowMapSize;\n\t};\n\tuniform DirectionalLight directionalLights[ NUM_DIR_LIGHTS ];\n\tvoid getDirectionalDirectLightIrradiance( const in DirectionalLight directionalLight, const in GeometricContext geometry, out IncidentLight directLight ) {\n\t\tdirectLight.color = directionalLight.color;\n\t\tdirectLight.direction = directionalLight.direction;\n\t\tdirectLight.visible = true;\n\t}\n#endif\n#if NUM_POINT_LIGHTS > 0\n\tstruct PointLight {\n\t\tvec3 position;\n\t\tvec3 color;\n\t\tfloat distance;\n\t\tfloat decay;\n\t\tint shadow;\n\t\tfloat shadowBias;\n\t\tfloat shadowRadius;\n\t\tvec2 shadowMapSize;\n\t};\n\tuniform PointLight pointLights[ NUM_POINT_LIGHTS ];\n\tvoid getPointDirectLightIrradiance( const in PointLight pointLight, const in GeometricContext geometry, out IncidentLight directLight ) {\n\t\tvec3 lVector = pointLight.position - geometry.position;\n\t\tdirectLight.direction = normalize( lVector );\n\t\tfloat lightDistance = length( lVector );\n\t\tif ( testLightInRange( lightDistance, pointLight.distance ) ) {\n\t\t\tdirectLight.color = pointLight.color;\n\t\t\tdirectLight.color *= punctualLightIntensityToIrradianceFactor( lightDistance, pointLight.distance, pointLight.decay );\n\t\t\tdirectLight.visible = true;\n\t\t} else {\n\t\t\tdirectLight.color = vec3( 0.0 );\n\t\t\tdirectLight.visible = false;\n\t\t}\n\t}\n#endif\n#if NUM_SPOT_LIGHTS > 0\n\tstruct SpotLight {\n\t\tvec3 position;\n\t\tvec3 direction;\n\t\tvec3 color;\n\t\tfloat distance;\n\t\tfloat decay;\n\t\tfloat coneCos;\n\t\tfloat penumbraCos;\n\t\tint shadow;\n\t\tfloat shadowBias;\n\t\tfloat shadowRadius;\n\t\tvec2 shadowMapSize;\n\t};\n\tuniform SpotLight spotLights[ NUM_SPOT_LIGHTS ];\n\tvoid getSpotDirectLightIrradiance( const in SpotLight spotLight, const in GeometricContext geometry, out IncidentLight directLight  ) {\n\t\tvec3 lVector = spotLight.position - geometry.position;\n\t\tdirectLight.direction = normalize( lVector );\n\t\tfloat lightDistance = length( lVector );\n\t\tfloat angleCos = dot( directLight.direction, spotLight.direction );\n\t\tif ( all( bvec2( angleCos > spotLight.coneCos, testLightInRange( lightDistance, spotLight.distance ) ) ) ) {\n\t\t\tfloat spotEffect = smoothstep( spotLight.coneCos, spotLight.penumbraCos, angleCos );\n\t\t\tdirectLight.color = spotLight.color;\n\t\t\tdirectLight.color *= spotEffect * punctualLightIntensityToIrradianceFactor( lightDistance, spotLight.distance, spotLight.decay );\n\t\t\tdirectLight.visible = true;\n\t\t} else {\n\t\t\tdirectLight.color = vec3( 0.0 );\n\t\t\tdirectLight.visible = false;\n\t\t}\n\t}\n#endif\n#if NUM_HEMI_LIGHTS > 0\n\tstruct HemisphereLight {\n\t\tvec3 direction;\n\t\tvec3 skyColor;\n\t\tvec3 groundColor;\n\t};\n\tuniform HemisphereLight hemisphereLights[ NUM_HEMI_LIGHTS ];\n\tvec3 getHemisphereLightIrradiance( const in HemisphereLight hemiLight, const in GeometricContext geometry ) {\n\t\tfloat dotNL = dot( geometry.normal, hemiLight.direction );\n\t\tfloat hemiDiffuseWeight = 0.5 * dotNL + 0.5;\n\t\tvec3 irradiance = mix( hemiLight.groundColor, hemiLight.skyColor, hemiDiffuseWeight );\n\t\t#ifndef PHYSICALLY_CORRECT_LIGHTS\n\t\t\tirradiance *= PI;\n\t\t#endif\n\t\treturn irradiance;\n\t}\n#endif\n#if defined( USE_ENVMAP ) && defined( PHYSICAL )\n\tvec3 getLightProbeIndirectIrradiance( const in GeometricContext geometry, const in int maxMIPLevel ) {\n\t\t#ifdef DOUBLE_SIDED\n\t\t\tfloat flipNormal = ( float( gl_FrontFacing ) * 2.0 - 1.0 );\n\t\t#else\n\t\t\tfloat flipNormal = 1.0;\n\t\t#endif\n\t\tvec3 worldNormal = inverseTransformDirection( geometry.normal, viewMatrix );\n\t\t#ifdef ENVMAP_TYPE_CUBE\n\t\t\tvec3 queryVec = flipNormal * vec3( flipEnvMap * worldNormal.x, worldNormal.yz );\n\t\t\t#ifdef TEXTURE_LOD_EXT\n\t\t\t\tvec4 envMapColor = textureCubeLodEXT( envMap, queryVec, float( maxMIPLevel ) );\n\t\t\t#else\n\t\t\t\tvec4 envMapColor = textureCube( envMap, queryVec, float( maxMIPLevel ) );\n\t\t\t#endif\n\t\t\tenvMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;\n\t\t#elif defined( ENVMAP_TYPE_CUBE_UV )\n\t\t\tvec3 queryVec = flipNormal * vec3( flipEnvMap * worldNormal.x, worldNormal.yz );\n\t\t\tvec4 envMapColor = textureCubeUV( queryVec, 1.0 );\n\t\t#else\n\t\t\tvec4 envMapColor = vec4( 0.0 );\n\t\t#endif\n\t\treturn PI * envMapColor.rgb * envMapIntensity;\n\t}\n\tfloat getSpecularMIPLevel( const in float blinnShininessExponent, const in int maxMIPLevel ) {\n\t\tfloat maxMIPLevelScalar = float( maxMIPLevel );\n\t\tfloat desiredMIPLevel = maxMIPLevelScalar - 0.79248 - 0.5 * log2( pow2( blinnShininessExponent ) + 1.0 );\n\t\treturn clamp( desiredMIPLevel, 0.0, maxMIPLevelScalar );\n\t}\n\tvec3 getLightProbeIndirectRadiance( const in GeometricContext geometry, const in float blinnShininessExponent, const in int maxMIPLevel ) {\n\t\t#ifdef ENVMAP_MODE_REFLECTION\n\t\t\tvec3 reflectVec = reflect( -geometry.viewDir, geometry.normal );\n\t\t#else\n\t\t\tvec3 reflectVec = refract( -geometry.viewDir, geometry.normal, refractionRatio );\n\t\t#endif\n\t\t#ifdef DOUBLE_SIDED\n\t\t\tfloat flipNormal = ( float( gl_FrontFacing ) * 2.0 - 1.0 );\n\t\t#else\n\t\t\tfloat flipNormal = 1.0;\n\t\t#endif\n\t\treflectVec = inverseTransformDirection( reflectVec, viewMatrix );\n\t\tfloat specularMIPLevel = getSpecularMIPLevel( blinnShininessExponent, maxMIPLevel );\n\t\t#ifdef ENVMAP_TYPE_CUBE\n\t\t\tvec3 queryReflectVec = flipNormal * vec3( flipEnvMap * reflectVec.x, reflectVec.yz );\n\t\t\t#ifdef TEXTURE_LOD_EXT\n\t\t\t\tvec4 envMapColor = textureCubeLodEXT( envMap, queryReflectVec, specularMIPLevel );\n\t\t\t#else\n\t\t\t\tvec4 envMapColor = textureCube( envMap, queryReflectVec, specularMIPLevel );\n\t\t\t#endif\n\t\t\tenvMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;\n\t\t#elif defined( ENVMAP_TYPE_CUBE_UV )\n\t\t\tvec3 queryReflectVec = flipNormal * vec3( flipEnvMap * reflectVec.x, reflectVec.yz );\n\t\t\tvec4 envMapColor = textureCubeUV(queryReflectVec, BlinnExponentToGGXRoughness(blinnShininessExponent));\n\t\t#elif defined( ENVMAP_TYPE_EQUIREC )\n\t\t\tvec2 sampleUV;\n\t\t\tsampleUV.y = saturate( flipNormal * reflectVec.y * 0.5 + 0.5 );\n\t\t\tsampleUV.x = atan( flipNormal * reflectVec.z, flipNormal * reflectVec.x ) * RECIPROCAL_PI2 + 0.5;\n\t\t\t#ifdef TEXTURE_LOD_EXT\n\t\t\t\tvec4 envMapColor = texture2DLodEXT( envMap, sampleUV, specularMIPLevel );\n\t\t\t#else\n\t\t\t\tvec4 envMapColor = texture2D( envMap, sampleUV, specularMIPLevel );\n\t\t\t#endif\n\t\t\tenvMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;\n\t\t#elif defined( ENVMAP_TYPE_SPHERE )\n\t\t\tvec3 reflectView = flipNormal * normalize((viewMatrix * vec4( reflectVec, 0.0 )).xyz + vec3(0.0,0.0,1.0));\n\t\t\t#ifdef TEXTURE_LOD_EXT\n\t\t\t\tvec4 envMapColor = texture2DLodEXT( envMap, reflectView.xy * 0.5 + 0.5, specularMIPLevel );\n\t\t\t#else\n\t\t\t\tvec4 envMapColor = texture2D( envMap, reflectView.xy * 0.5 + 0.5, specularMIPLevel );\n\t\t\t#endif\n\t\t\tenvMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;\n\t\t#endif\n\t\treturn envMapColor.rgb * envMapIntensity;\n\t}\n#endif\n";
THREE.ShaderChunk.lights_phong_fragment = "BlinnPhongMaterial material;\nmaterial.diffuseColor = diffuseColor.rgb;\nmaterial.specularColor = specular;\nmaterial.specularShininess = shininess;\nmaterial.specularStrength = specularStrength;\n";
THREE.ShaderChunk.lights_phong_pars_fragment = "varying vec3 vViewPosition;\n#ifndef FLAT_SHADED\n\tvarying vec3 vNormal;\n#endif\nstruct BlinnPhongMaterial {\n\tvec3\tdiffuseColor;\n\tvec3\tspecularColor;\n\tfloat\tspecularShininess;\n\tfloat\tspecularStrength;\n};\nvoid RE_Direct_BlinnPhong( const in IncidentLight directLight, const in GeometricContext geometry, const in BlinnPhongMaterial material, inout ReflectedLight reflectedLight ) {\n\tfloat dotNL = saturate( dot( geometry.normal, directLight.direction ) );\n\tvec3 irradiance = dotNL * directLight.color;\n\t#ifndef PHYSICALLY_CORRECT_LIGHTS\n\t\tirradiance *= PI;\n\t#endif\n\treflectedLight.directDiffuse += irradiance * BRDF_Diffuse_Lambert( material.diffuseColor );\n\treflectedLight.directSpecular += irradiance * BRDF_Specular_BlinnPhong( directLight, geometry, material.specularColor, material.specularShininess ) * material.specularStrength;\n}\nvoid RE_IndirectDiffuse_BlinnPhong( const in vec3 irradiance, const in GeometricContext geometry, const in BlinnPhongMaterial material, inout ReflectedLight reflectedLight ) {\n\treflectedLight.indirectDiffuse += irradiance * BRDF_Diffuse_Lambert( material.diffuseColor );\n}\n#define RE_Direct\t\t\t\tRE_Direct_BlinnPhong\n#define RE_IndirectDiffuse\t\tRE_IndirectDiffuse_BlinnPhong\n#define Material_LightProbeLOD( material )\t(0)\n";
THREE.ShaderChunk.lights_physical_fragment = "PhysicalMaterial material;\nmaterial.diffuseColor = diffuseColor.rgb * ( 1.0 - metalnessFactor );\nmaterial.specularRoughness = clamp( roughnessFactor, 0.04, 1.0 );\n#ifdef STANDARD\n\tmaterial.specularColor = mix( vec3( 0.04 ), diffuseColor.rgb, metalnessFactor );\n#else\n\tmaterial.specularColor = mix( vec3( 0.16 * pow2( reflectivity ) ), diffuseColor.rgb, metalnessFactor );\n#endif\n";
THREE.ShaderChunk.lights_physical_pars_fragment = "struct PhysicalMaterial {\n\tvec3\tdiffuseColor;\n\tfloat\tspecularRoughness;\n\tvec3\tspecularColor;\n\t#ifndef STANDARD\n\t#endif\n};\nvoid RE_Direct_Physical( const in IncidentLight directLight, const in GeometricContext geometry, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {\n\tfloat dotNL = saturate( dot( geometry.normal, directLight.direction ) );\n\tvec3 irradiance = dotNL * directLight.color;\n\t#ifndef PHYSICALLY_CORRECT_LIGHTS\n\t\tirradiance *= PI;\n\t#endif\n\treflectedLight.directDiffuse += irradiance * BRDF_Diffuse_Lambert( material.diffuseColor );\n\treflectedLight.directSpecular += irradiance * BRDF_Specular_GGX( directLight, geometry, material.specularColor, material.specularRoughness );\n}\nvoid RE_IndirectDiffuse_Physical( const in vec3 irradiance, const in GeometricContext geometry, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {\n\treflectedLight.indirectDiffuse += irradiance * BRDF_Diffuse_Lambert( material.diffuseColor );\n}\nvoid RE_IndirectSpecular_Physical( const in vec3 radiance, const in GeometricContext geometry, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {\n\treflectedLight.indirectSpecular += radiance * BRDF_Specular_GGX_Environment( geometry, material.specularColor, material.specularRoughness );\n}\n#define RE_Direct\t\t\t\tRE_Direct_Physical\n#define RE_IndirectDiffuse\t\tRE_IndirectDiffuse_Physical\n#define RE_IndirectSpecular\t\tRE_IndirectSpecular_Physical\n#define Material_BlinnShininessExponent( material )   GGXRoughnessToBlinnExponent( material.specularRoughness )\nfloat computeSpecularOcclusion( const in float dotNV, const in float ambientOcclusion, const in float roughness ) {\n\treturn saturate( pow( dotNV + ambientOcclusion, exp2( - 16.0 * roughness - 1.0 ) ) - 1.0 + ambientOcclusion );\n}\n";
THREE.ShaderChunk.lights_template = "\nGeometricContext geometry;\ngeometry.position = - vViewPosition;\ngeometry.normal = normal;\ngeometry.viewDir = normalize( vViewPosition );\nIncidentLight directLight;\n#if ( NUM_POINT_LIGHTS > 0 ) && defined( RE_Direct )\n\tPointLight pointLight;\n\tfor ( int i = 0; i < NUM_POINT_LIGHTS; i ++ ) {\n\t\tpointLight = pointLights[ i ];\n\t\tgetPointDirectLightIrradiance( pointLight, geometry, directLight );\n\t\t#ifdef USE_SHADOWMAP\n\t\tdirectLight.color *= all( bvec2( pointLight.shadow, directLight.visible ) ) ? getPointShadow( pointShadowMap[ i ], pointLight.shadowMapSize, pointLight.shadowBias, pointLight.shadowRadius, vPointShadowCoord[ i ] ) : 1.0;\n\t\t#endif\n\t\tRE_Direct( directLight, geometry, material, reflectedLight );\n\t}\n#endif\n#if ( NUM_SPOT_LIGHTS > 0 ) && defined( RE_Direct )\n\tSpotLight spotLight;\n\tfor ( int i = 0; i < NUM_SPOT_LIGHTS; i ++ ) {\n\t\tspotLight = spotLights[ i ];\n\t\tgetSpotDirectLightIrradiance( spotLight, geometry, directLight );\n\t\t#ifdef USE_SHADOWMAP\n\t\tdirectLight.color *= all( bvec2( spotLight.shadow, directLight.visible ) ) ? getShadow( spotShadowMap[ i ], spotLight.shadowMapSize, spotLight.shadowBias, spotLight.shadowRadius, vSpotShadowCoord[ i ] ) : 1.0;\n\t\t#endif\n\t\tRE_Direct( directLight, geometry, material, reflectedLight );\n\t}\n#endif\n#if ( NUM_DIR_LIGHTS > 0 ) && defined( RE_Direct )\n\tDirectionalLight directionalLight;\n\tfor ( int i = 0; i < NUM_DIR_LIGHTS; i ++ ) {\n\t\tdirectionalLight = directionalLights[ i ];\n\t\tgetDirectionalDirectLightIrradiance( directionalLight, geometry, directLight );\n\t\t#ifdef USE_SHADOWMAP\n\t\tdirectLight.color *= all( bvec2( directionalLight.shadow, directLight.visible ) ) ? getShadow( directionalShadowMap[ i ], directionalLight.shadowMapSize, directionalLight.shadowBias, directionalLight.shadowRadius, vDirectionalShadowCoord[ i ] ) : 1.0;\n\t\t#endif\n\t\tRE_Direct( directLight, geometry, material, reflectedLight );\n\t}\n#endif\n#if defined( RE_IndirectDiffuse )\n\tvec3 irradiance = getAmbientLightIrradiance( ambientLightColor );\n\t#ifdef USE_LIGHTMAP\n\t\tvec3 lightMapIrradiance = texture2D( lightMap, vUv2 ).xyz * lightMapIntensity;\n\t\t#ifndef PHYSICALLY_CORRECT_LIGHTS\n\t\t\tlightMapIrradiance *= PI;\n\t\t#endif\n\t\tirradiance += lightMapIrradiance;\n\t#endif\n\t#if ( NUM_HEMI_LIGHTS > 0 )\n\t\tfor ( int i = 0; i < NUM_HEMI_LIGHTS; i ++ ) {\n\t\t\tirradiance += getHemisphereLightIrradiance( hemisphereLights[ i ], geometry );\n\t\t}\n\t#endif\n\t#if defined( USE_ENVMAP ) && defined( PHYSICAL ) && defined( ENVMAP_TYPE_CUBE_UV )\n\t \tirradiance += getLightProbeIndirectIrradiance( geometry, 8 );\n\t#endif\n\tRE_IndirectDiffuse( irradiance, geometry, material, reflectedLight );\n#endif\n#if defined( USE_ENVMAP ) && defined( RE_IndirectSpecular )\n\tvec3 radiance = getLightProbeIndirectRadiance( geometry, Material_BlinnShininessExponent( material ), 8 );\n\tRE_IndirectSpecular( radiance, geometry, material, reflectedLight );\n#endif\n";
THREE.ShaderChunk.logdepthbuf_fragment = "#if defined(USE_LOGDEPTHBUF) && defined(USE_LOGDEPTHBUF_EXT)\n\tgl_FragDepthEXT = log2(vFragDepth) * logDepthBufFC * 0.5;\n#endif";
THREE.ShaderChunk.logdepthbuf_pars_fragment = "#ifdef USE_LOGDEPTHBUF\n\tuniform float logDepthBufFC;\n\t#ifdef USE_LOGDEPTHBUF_EXT\n\t\tvarying float vFragDepth;\n\t#endif\n#endif\n";
THREE.ShaderChunk.logdepthbuf_pars_vertex = "#ifdef USE_LOGDEPTHBUF\n\t#ifdef USE_LOGDEPTHBUF_EXT\n\t\tvarying float vFragDepth;\n\t#endif\n\tuniform float logDepthBufFC;\n#endif";
THREE.ShaderChunk.logdepthbuf_vertex = "#ifdef USE_LOGDEPTHBUF\n\tgl_Position.z = log2(max( EPSILON, gl_Position.w + 1.0 )) * logDepthBufFC;\n\t#ifdef USE_LOGDEPTHBUF_EXT\n\t\tvFragDepth = 1.0 + gl_Position.w;\n\t#else\n\t\tgl_Position.z = (gl_Position.z - 1.0) * gl_Position.w;\n\t#endif\n#endif\n";
THREE.ShaderChunk.map_fragment = "#ifdef USE_MAP\n\tvec4 texelColor = texture2D( map, vUv );\n\ttexelColor = mapTexelToLinear( texelColor );\n\tdiffuseColor *= texelColor;\n#endif\n";
THREE.ShaderChunk.map_pars_fragment = "#ifdef USE_MAP\n\tuniform sampler2D map;\n#endif\n";
THREE.ShaderChunk.map_particle_fragment = "#ifdef USE_MAP\n\tvec4 mapTexel = texture2D( map, vec2( gl_PointCoord.x, 1.0 - gl_PointCoord.y ) * offsetRepeat.zw + offsetRepeat.xy );\n\tdiffuseColor *= mapTexelToLinear( mapTexel );\n#endif\n";
THREE.ShaderChunk.map_particle_pars_fragment = "#ifdef USE_MAP\n\tuniform vec4 offsetRepeat;\n\tuniform sampler2D map;\n#endif\n";
THREE.ShaderChunk.metalnessmap_fragment = "float metalnessFactor = metalness;\n#ifdef USE_METALNESSMAP\n\tvec4 texelMetalness = texture2D( metalnessMap, vUv );\n\tmetalnessFactor *= texelMetalness.r;\n#endif\n";
THREE.ShaderChunk.metalnessmap_pars_fragment = "#ifdef USE_METALNESSMAP\n\tuniform sampler2D metalnessMap;\n#endif";
THREE.ShaderChunk.morphnormal_vertex = "#ifdef USE_MORPHNORMALS\n\tobjectNormal += ( morphNormal0 - normal ) * morphTargetInfluences[ 0 ];\n\tobjectNormal += ( morphNormal1 - normal ) * morphTargetInfluences[ 1 ];\n\tobjectNormal += ( morphNormal2 - normal ) * morphTargetInfluences[ 2 ];\n\tobjectNormal += ( morphNormal3 - normal ) * morphTargetInfluences[ 3 ];\n#endif\n";
THREE.ShaderChunk.morphtarget_pars_vertex = "#ifdef USE_MORPHTARGETS\n\t#ifndef USE_MORPHNORMALS\n\tuniform float morphTargetInfluences[ 8 ];\n\t#else\n\tuniform float morphTargetInfluences[ 4 ];\n\t#endif\n#endif";
THREE.ShaderChunk.morphtarget_vertex = "#ifdef USE_MORPHTARGETS\n\ttransformed += ( morphTarget0 - position ) * morphTargetInfluences[ 0 ];\n\ttransformed += ( morphTarget1 - position ) * morphTargetInfluences[ 1 ];\n\ttransformed += ( morphTarget2 - position ) * morphTargetInfluences[ 2 ];\n\ttransformed += ( morphTarget3 - position ) * morphTargetInfluences[ 3 ];\n\t#ifndef USE_MORPHNORMALS\n\ttransformed += ( morphTarget4 - position ) * morphTargetInfluences[ 4 ];\n\ttransformed += ( morphTarget5 - position ) * morphTargetInfluences[ 5 ];\n\ttransformed += ( morphTarget6 - position ) * morphTargetInfluences[ 6 ];\n\ttransformed += ( morphTarget7 - position ) * morphTargetInfluences[ 7 ];\n\t#endif\n#endif\n";
THREE.ShaderChunk.normal_fragment = "#ifdef FLAT_SHADED\n\tvec3 fdx = vec3( dFdx( vViewPosition.x ), dFdx( vViewPosition.y ), dFdx( vViewPosition.z ) );\n\tvec3 fdy = vec3( dFdy( vViewPosition.x ), dFdy( vViewPosition.y ), dFdy( vViewPosition.z ) );\n\tvec3 normal = normalize( cross( fdx, fdy ) );\n#else\n\tvec3 normal = normalize( vNormal );\n\t#ifdef DOUBLE_SIDED\n\t\tnormal = normal * ( -1.0 + 2.0 * float( gl_FrontFacing ) );\n\t#endif\n#endif\n#ifdef USE_NORMALMAP\n\tnormal = perturbNormal2Arb( -vViewPosition, normal );\n#elif defined( USE_BUMPMAP )\n\tnormal = perturbNormalArb( -vViewPosition, normal, dHdxy_fwd() );\n#endif\n";
THREE.ShaderChunk.normalmap_pars_fragment = "#ifdef USE_NORMALMAP\n\tuniform sampler2D normalMap;\n\tuniform vec2 normalScale;\n\tvec3 perturbNormal2Arb( vec3 eye_pos, vec3 surf_norm ) {\n\t\tvec3 q0 = dFdx( eye_pos.xyz );\n\t\tvec3 q1 = dFdy( eye_pos.xyz );\n\t\tvec2 st0 = dFdx( vUv.st );\n\t\tvec2 st1 = dFdy( vUv.st );\n\t\tvec3 S = normalize( q0 * st1.t - q1 * st0.t );\n\t\tvec3 T = normalize( -q0 * st1.s + q1 * st0.s );\n\t\tvec3 N = normalize( surf_norm );\n\t\tvec3 mapN = texture2D( normalMap, vUv ).xyz * 2.0 - 1.0;\n\t\tmapN.xy = normalScale * mapN.xy;\n\t\tmat3 tsn = mat3( S, T, N );\n\t\treturn normalize( tsn * mapN );\n\t}\n#endif\n";
THREE.ShaderChunk.packing = "vec3 packNormalToRGB( const in vec3 normal ) {\n  return normalize( normal ) * 0.5 + 0.5;\n}\nvec3 unpackRGBToNormal( const in vec3 rgb ) {\n  return 1.0 - 2.0 * rgb.xyz;\n}\nvec4 packDepthToRGBA( const in float value ) {\n\tconst vec4 bit_shift = vec4( 256.0 * 256.0 * 256.0, 256.0 * 256.0, 256.0, 1.0 );\n\tconst vec4 bit_mask = vec4( 0.0, 1.0 / 256.0, 1.0 / 256.0, 1.0 / 256.0 );\n\tvec4 res = mod( value * bit_shift * vec4( 255 ), vec4( 256 ) ) / vec4( 255 );\n\tres -= res.xxyz * bit_mask;\n\treturn res;\n}\nfloat unpackRGBAToDepth( const in vec4 rgba ) {\n\tconst vec4 bitSh = vec4( 1.0 / ( 256.0 * 256.0 * 256.0 ), 1.0 / ( 256.0 * 256.0 ), 1.0 / 256.0, 1.0 );\n\treturn dot( rgba, bitSh );\n}\nfloat viewZToOrthoDepth( const in float viewZ, const in float near, const in float far ) {\n  return ( viewZ + near ) / ( near - far );\n}\nfloat OrthoDepthToViewZ( const in float linearClipZ, const in float near, const in float far ) {\n  return linearClipZ * ( near - far ) - near;\n}\nfloat viewZToPerspectiveDepth( const in float viewZ, const in float near, const in float far ) {\n  return (( near + viewZ ) * far ) / (( far - near ) * viewZ );\n}\nfloat perspectiveDepthToViewZ( const in float invClipZ, const in float near, const in float far ) {\n  return ( near * far ) / ( ( far - near ) * invClipZ - far );\n}\n";
THREE.ShaderChunk.premultiplied_alpha_fragment = "#ifdef PREMULTIPLIED_ALPHA\n\tgl_FragColor.rgb *= gl_FragColor.a;\n#endif\n";
THREE.ShaderChunk.project_vertex = "#ifdef USE_SKINNING\n\tvec4 mvPosition = modelViewMatrix * skinned;\n#else\n\tvec4 mvPosition = modelViewMatrix * vec4( transformed, 1.0 );\n#endif\ngl_Position = projectionMatrix * mvPosition;\n";
THREE.ShaderChunk.roughnessmap_fragment = "float roughnessFactor = roughness;\n#ifdef USE_ROUGHNESSMAP\n\tvec4 texelRoughness = texture2D( roughnessMap, vUv );\n\troughnessFactor *= texelRoughness.r;\n#endif\n";
THREE.ShaderChunk.roughnessmap_pars_fragment = "#ifdef USE_ROUGHNESSMAP\n\tuniform sampler2D roughnessMap;\n#endif";
THREE.ShaderChunk.shadowmap_pars_fragment = "#ifdef USE_SHADOWMAP\n\t#if NUM_DIR_LIGHTS > 0\n\t\tuniform sampler2D directionalShadowMap[ NUM_DIR_LIGHTS ];\n\t\tvarying vec4 vDirectionalShadowCoord[ NUM_DIR_LIGHTS ];\n\t#endif\n\t#if NUM_SPOT_LIGHTS > 0\n\t\tuniform sampler2D spotShadowMap[ NUM_SPOT_LIGHTS ];\n\t\tvarying vec4 vSpotShadowCoord[ NUM_SPOT_LIGHTS ];\n\t#endif\n\t#if NUM_POINT_LIGHTS > 0\n\t\tuniform sampler2D pointShadowMap[ NUM_POINT_LIGHTS ];\n\t\tvarying vec4 vPointShadowCoord[ NUM_POINT_LIGHTS ];\n\t#endif\n\tfloat texture2DCompare( sampler2D depths, vec2 uv, float compare ) {\n\t\treturn step( compare, unpackRGBAToDepth( texture2D( depths, uv ) ) );\n\t}\n\tfloat texture2DShadowLerp( sampler2D depths, vec2 size, vec2 uv, float compare ) {\n\t\tconst vec2 offset = vec2( 0.0, 1.0 );\n\t\tvec2 texelSize = vec2( 1.0 ) / size;\n\t\tvec2 centroidUV = floor( uv * size + 0.5 ) / size;\n\t\tfloat lb = texture2DCompare( depths, centroidUV + texelSize * offset.xx, compare );\n\t\tfloat lt = texture2DCompare( depths, centroidUV + texelSize * offset.xy, compare );\n\t\tfloat rb = texture2DCompare( depths, centroidUV + texelSize * offset.yx, compare );\n\t\tfloat rt = texture2DCompare( depths, centroidUV + texelSize * offset.yy, compare );\n\t\tvec2 f = fract( uv * size + 0.5 );\n\t\tfloat a = mix( lb, lt, f.y );\n\t\tfloat b = mix( rb, rt, f.y );\n\t\tfloat c = mix( a, b, f.x );\n\t\treturn c;\n\t}\n\tfloat getShadow( sampler2D shadowMap, vec2 shadowMapSize, float shadowBias, float shadowRadius, vec4 shadowCoord ) {\n\t\tshadowCoord.xyz /= shadowCoord.w;\n\t\tshadowCoord.z += shadowBias;\n\t\tbvec4 inFrustumVec = bvec4 ( shadowCoord.x >= 0.0, shadowCoord.x <= 1.0, shadowCoord.y >= 0.0, shadowCoord.y <= 1.0 );\n\t\tbool inFrustum = all( inFrustumVec );\n\t\tbvec2 frustumTestVec = bvec2( inFrustum, shadowCoord.z <= 1.0 );\n\t\tbool frustumTest = all( frustumTestVec );\n\t\tif ( frustumTest ) {\n\t\t#if defined( SHADOWMAP_TYPE_PCF )\n\t\t\tvec2 texelSize = vec2( 1.0 ) / shadowMapSize;\n\t\t\tfloat dx0 = - texelSize.x * shadowRadius;\n\t\t\tfloat dy0 = - texelSize.y * shadowRadius;\n\t\t\tfloat dx1 = + texelSize.x * shadowRadius;\n\t\t\tfloat dy1 = + texelSize.y * shadowRadius;\n\t\t\treturn (\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, 0.0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy, shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, 0.0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, dy1 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy1 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, dy1 ), shadowCoord.z )\n\t\t\t) * ( 1.0 / 9.0 );\n\t\t#elif defined( SHADOWMAP_TYPE_PCF_SOFT )\n\t\t\tvec2 texelSize = vec2( 1.0 ) / shadowMapSize;\n\t\t\tfloat dx0 = - texelSize.x * shadowRadius;\n\t\t\tfloat dy0 = - texelSize.y * shadowRadius;\n\t\t\tfloat dx1 = + texelSize.x * shadowRadius;\n\t\t\tfloat dy1 = + texelSize.y * shadowRadius;\n\t\t\treturn (\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx0, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( 0.0, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx1, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx0, 0.0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy, shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx1, 0.0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx0, dy1 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( 0.0, dy1 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx1, dy1 ), shadowCoord.z )\n\t\t\t) * ( 1.0 / 9.0 );\n\t\t#else\n\t\t\treturn texture2DCompare( shadowMap, shadowCoord.xy, shadowCoord.z );\n\t\t#endif\n\t\t}\n\t\treturn 1.0;\n\t}\n\tvec2 cubeToUV( vec3 v, float texelSizeY ) {\n\t\tvec3 absV = abs( v );\n\t\tfloat scaleToCube = 1.0 / max( absV.x, max( absV.y, absV.z ) );\n\t\tabsV *= scaleToCube;\n\t\tv *= scaleToCube * ( 1.0 - 2.0 * texelSizeY );\n\t\tvec2 planar = v.xy;\n\t\tfloat almostATexel = 1.5 * texelSizeY;\n\t\tfloat almostOne = 1.0 - almostATexel;\n\t\tif ( absV.z >= almostOne ) {\n\t\t\tif ( v.z > 0.0 )\n\t\t\t\tplanar.x = 4.0 - v.x;\n\t\t} else if ( absV.x >= almostOne ) {\n\t\t\tfloat signX = sign( v.x );\n\t\t\tplanar.x = v.z * signX + 2.0 * signX;\n\t\t} else if ( absV.y >= almostOne ) {\n\t\t\tfloat signY = sign( v.y );\n\t\t\tplanar.x = v.x + 2.0 * signY + 2.0;\n\t\t\tplanar.y = v.z * signY - 2.0;\n\t\t}\n\t\treturn vec2( 0.125, 0.25 ) * planar + vec2( 0.375, 0.75 );\n\t}\n\tfloat getPointShadow( sampler2D shadowMap, vec2 shadowMapSize, float shadowBias, float shadowRadius, vec4 shadowCoord ) {\n\t\tvec2 texelSize = vec2( 1.0 ) / ( shadowMapSize * vec2( 4.0, 2.0 ) );\n\t\tvec3 lightToPosition = shadowCoord.xyz;\n\t\tvec3 bd3D = normalize( lightToPosition );\n\t\tfloat dp = ( length( lightToPosition ) - shadowBias ) / 1000.0;\n\t\t#if defined( SHADOWMAP_TYPE_PCF ) || defined( SHADOWMAP_TYPE_PCF_SOFT )\n\t\t\tvec2 offset = vec2( - 1, 1 ) * shadowRadius * texelSize.y;\n\t\t\treturn (\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.xyy, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.yyy, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.xyx, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.yyx, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.xxy, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.yxy, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.xxx, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.yxx, texelSize.y ), dp )\n\t\t\t) * ( 1.0 / 9.0 );\n\t\t#else\n\t\t\treturn texture2DCompare( shadowMap, cubeToUV( bd3D, texelSize.y ), dp );\n\t\t#endif\n\t}\n#endif\n";
THREE.ShaderChunk.shadowmap_pars_vertex = "#ifdef USE_SHADOWMAP\n\t#if NUM_DIR_LIGHTS > 0\n\t\tuniform mat4 directionalShadowMatrix[ NUM_DIR_LIGHTS ];\n\t\tvarying vec4 vDirectionalShadowCoord[ NUM_DIR_LIGHTS ];\n\t#endif\n\t#if NUM_SPOT_LIGHTS > 0\n\t\tuniform mat4 spotShadowMatrix[ NUM_SPOT_LIGHTS ];\n\t\tvarying vec4 vSpotShadowCoord[ NUM_SPOT_LIGHTS ];\n\t#endif\n\t#if NUM_POINT_LIGHTS > 0\n\t\tuniform mat4 pointShadowMatrix[ NUM_POINT_LIGHTS ];\n\t\tvarying vec4 vPointShadowCoord[ NUM_POINT_LIGHTS ];\n\t#endif\n#endif\n";
THREE.ShaderChunk.shadowmap_vertex = "#ifdef USE_SHADOWMAP\n\t#if NUM_DIR_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_DIR_LIGHTS; i ++ ) {\n\t\tvDirectionalShadowCoord[ i ] = directionalShadowMatrix[ i ] * worldPosition;\n\t}\n\t#endif\n\t#if NUM_SPOT_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_SPOT_LIGHTS; i ++ ) {\n\t\tvSpotShadowCoord[ i ] = spotShadowMatrix[ i ] * worldPosition;\n\t}\n\t#endif\n\t#if NUM_POINT_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_POINT_LIGHTS; i ++ ) {\n\t\tvPointShadowCoord[ i ] = pointShadowMatrix[ i ] * worldPosition;\n\t}\n\t#endif\n#endif\n";
THREE.ShaderChunk.shadowmask_pars_fragment = "float getShadowMask() {\n\tfloat shadow = 1.0;\n\t#ifdef USE_SHADOWMAP\n\t#if NUM_DIR_LIGHTS > 0\n\tDirectionalLight directionalLight;\n\tfor ( int i = 0; i < NUM_DIR_LIGHTS; i ++ ) {\n\t\tdirectionalLight = directionalLights[ i ];\n\t\tshadow *= bool( directionalLight.shadow ) ? getShadow( directionalShadowMap[ i ], directionalLight.shadowMapSize, directionalLight.shadowBias, directionalLight.shadowRadius, vDirectionalShadowCoord[ i ] ) : 1.0;\n\t}\n\t#endif\n\t#if NUM_SPOT_LIGHTS > 0\n\tSpotLight spotLight;\n\tfor ( int i = 0; i < NUM_SPOT_LIGHTS; i ++ ) {\n\t\tspotLight = spotLights[ i ];\n\t\tshadow *= bool( spotLight.shadow ) ? getShadow( spotShadowMap[ i ], spotLight.shadowMapSize, spotLight.shadowBias, spotLight.shadowRadius, vSpotShadowCoord[ i ] ) : 1.0;\n\t}\n\t#endif\n\t#if NUM_POINT_LIGHTS > 0\n\tPointLight pointLight;\n\tfor ( int i = 0; i < NUM_POINT_LIGHTS; i ++ ) {\n\t\tpointLight = pointLights[ i ];\n\t\tshadow *= bool( pointLight.shadow ) ? getPointShadow( pointShadowMap[ i ], pointLight.shadowMapSize, pointLight.shadowBias, pointLight.shadowRadius, vPointShadowCoord[ i ] ) : 1.0;\n\t}\n\t#endif\n\t#endif\n\treturn shadow;\n}\n";
THREE.ShaderChunk.skinbase_vertex = "#ifdef USE_SKINNING\n\tmat4 boneMatX = getBoneMatrix( skinIndex.x );\n\tmat4 boneMatY = getBoneMatrix( skinIndex.y );\n\tmat4 boneMatZ = getBoneMatrix( skinIndex.z );\n\tmat4 boneMatW = getBoneMatrix( skinIndex.w );\n#endif";
THREE.ShaderChunk.skinning_pars_vertex = "#ifdef USE_SKINNING\n\tuniform mat4 bindMatrix;\n\tuniform mat4 bindMatrixInverse;\n\t#ifdef BONE_TEXTURE\n\t\tuniform sampler2D boneTexture;\n\t\tuniform int boneTextureWidth;\n\t\tuniform int boneTextureHeight;\n\t\tmat4 getBoneMatrix( const in float i ) {\n\t\t\tfloat j = i * 4.0;\n\t\t\tfloat x = mod( j, float( boneTextureWidth ) );\n\t\t\tfloat y = floor( j / float( boneTextureWidth ) );\n\t\t\tfloat dx = 1.0 / float( boneTextureWidth );\n\t\t\tfloat dy = 1.0 / float( boneTextureHeight );\n\t\t\ty = dy * ( y + 0.5 );\n\t\t\tvec4 v1 = texture2D( boneTexture, vec2( dx * ( x + 0.5 ), y ) );\n\t\t\tvec4 v2 = texture2D( boneTexture, vec2( dx * ( x + 1.5 ), y ) );\n\t\t\tvec4 v3 = texture2D( boneTexture, vec2( dx * ( x + 2.5 ), y ) );\n\t\t\tvec4 v4 = texture2D( boneTexture, vec2( dx * ( x + 3.5 ), y ) );\n\t\t\tmat4 bone = mat4( v1, v2, v3, v4 );\n\t\t\treturn bone;\n\t\t}\n\t#else\n\t\tuniform mat4 boneMatrices[ MAX_BONES ];\n\t\tmat4 getBoneMatrix( const in float i ) {\n\t\t\tmat4 bone = boneMatrices[ int(i) ];\n\t\t\treturn bone;\n\t\t}\n\t#endif\n#endif\n";
THREE.ShaderChunk.skinning_vertex = "#ifdef USE_SKINNING\n\tvec4 skinVertex = bindMatrix * vec4( transformed, 1.0 );\n\tvec4 skinned = vec4( 0.0 );\n\tskinned += boneMatX * skinVertex * skinWeight.x;\n\tskinned += boneMatY * skinVertex * skinWeight.y;\n\tskinned += boneMatZ * skinVertex * skinWeight.z;\n\tskinned += boneMatW * skinVertex * skinWeight.w;\n\tskinned  = bindMatrixInverse * skinned;\n#endif\n";
THREE.ShaderChunk.skinnormal_vertex = "#ifdef USE_SKINNING\n\tmat4 skinMatrix = mat4( 0.0 );\n\tskinMatrix += skinWeight.x * boneMatX;\n\tskinMatrix += skinWeight.y * boneMatY;\n\tskinMatrix += skinWeight.z * boneMatZ;\n\tskinMatrix += skinWeight.w * boneMatW;\n\tskinMatrix  = bindMatrixInverse * skinMatrix * bindMatrix;\n\tobjectNormal = vec4( skinMatrix * vec4( objectNormal, 0.0 ) ).xyz;\n#endif\n";
THREE.ShaderChunk.specularmap_fragment = "float specularStrength;\n#ifdef USE_SPECULARMAP\n\tvec4 texelSpecular = texture2D( specularMap, vUv );\n\tspecularStrength = texelSpecular.r;\n#else\n\tspecularStrength = 1.0;\n#endif";
THREE.ShaderChunk.specularmap_pars_fragment = "#ifdef USE_SPECULARMAP\n\tuniform sampler2D specularMap;\n#endif";
THREE.ShaderChunk.tonemapping_fragment = "#if defined( TONE_MAPPING )\n  gl_FragColor.rgb = toneMapping( gl_FragColor.rgb );\n#endif\n";
THREE.ShaderChunk.tonemapping_pars_fragment = "#define saturate(a) clamp( a, 0.0, 1.0 )\nuniform float toneMappingExposure;\nuniform float toneMappingWhitePoint;\nvec3 LinearToneMapping( vec3 color ) {\n  return toneMappingExposure * color;\n}\nvec3 ReinhardToneMapping( vec3 color ) {\n  color *= toneMappingExposure;\n  return saturate( color / ( vec3( 1.0 ) + color ) );\n}\n#define Uncharted2Helper( x ) max( ( ( x * ( 0.15 * x + 0.10 * 0.50 ) + 0.20 * 0.02 ) / ( x * ( 0.15 * x + 0.50 ) + 0.20 * 0.30 ) ) - 0.02 / 0.30, vec3( 0.0 ) )\nvec3 Uncharted2ToneMapping( vec3 color ) {\n  color *= toneMappingExposure;\n  return saturate( Uncharted2Helper( color ) / Uncharted2Helper( vec3( toneMappingWhitePoint ) ) );\n}\nvec3 OptimizedCineonToneMapping( vec3 color ) {\n  color *= toneMappingExposure;\n  color = max( vec3( 0.0 ), color - 0.004 );\n  return pow( ( color * ( 6.2 * color + 0.5 ) ) / ( color * ( 6.2 * color + 1.7 ) + 0.06 ), vec3( 2.2 ) );\n}\n";
THREE.ShaderChunk.uv2_pars_fragment = "#if defined( USE_LIGHTMAP ) || defined( USE_AOMAP )\n\tvarying vec2 vUv2;\n#endif";
THREE.ShaderChunk.uv2_pars_vertex = "#if defined( USE_LIGHTMAP ) || defined( USE_AOMAP )\n\tattribute vec2 uv2;\n\tvarying vec2 vUv2;\n#endif";
THREE.ShaderChunk.uv2_vertex = "#if defined( USE_LIGHTMAP ) || defined( USE_AOMAP )\n\tvUv2 = uv2;\n#endif";
THREE.ShaderChunk.uv_pars_fragment = "#if defined( USE_MAP ) || defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( USE_SPECULARMAP ) || defined( USE_ALPHAMAP ) || defined( USE_EMISSIVEMAP ) || defined( USE_ROUGHNESSMAP ) || defined( USE_METALNESSMAP )\n\tvarying vec2 vUv;\n#endif";
THREE.ShaderChunk.uv_pars_vertex = "#if defined( USE_MAP ) || defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( USE_SPECULARMAP ) || defined( USE_ALPHAMAP ) || defined( USE_EMISSIVEMAP ) || defined( USE_ROUGHNESSMAP ) || defined( USE_METALNESSMAP )\n\tvarying vec2 vUv;\n\tuniform vec4 offsetRepeat;\n#endif\n";
THREE.ShaderChunk.uv_vertex = "#if defined( USE_MAP ) || defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( USE_SPECULARMAP ) || defined( USE_ALPHAMAP ) || defined( USE_EMISSIVEMAP ) || defined( USE_ROUGHNESSMAP ) || defined( USE_METALNESSMAP )\n\tvUv = uv * offsetRepeat.zw + offsetRepeat.xy;\n#endif";
THREE.ShaderChunk.worldpos_vertex = "#if defined( USE_ENVMAP ) || defined( PHONG ) || defined( PHYSICAL ) || defined( LAMBERT ) || defined ( USE_SHADOWMAP )\n\t#ifdef USE_SKINNING\n\t\tvec4 worldPosition = modelMatrix * skinned;\n\t#else\n\t\tvec4 worldPosition = modelMatrix * vec4( transformed, 1.0 );\n\t#endif\n#endif\n";
THREE.UniformsUtils = {
    merge: function(n) {
        for (var u = {}, i, r, t = 0; t < n.length; t++) {
            i = this.clone(n[t]);
            for (r in i) u[r] = i[r]
        }
        return u
    },
    clone: function(n) {
        var r = {},
            i, u, t;
        for (i in n) {
            r[i] = {};
            for (u in n[i]) t = n[i][u], r[i][u] = t instanceof THREE.Color || t instanceof THREE.Vector2 || t instanceof THREE.Vector3 || t instanceof THREE.Vector4 || t instanceof THREE.Matrix3 || t instanceof THREE.Matrix4 || t instanceof THREE.Texture ? t.clone() : Array.isArray(t) ? t.slice() : t
        }
        return r
    }
};
THREE.UniformsLib = {
    common: {
        diffuse: {
            type: "c",
            value: new THREE.Color(15658734)
        },
        opacity: {
            type: "1f",
            value: 1
        },
        map: {
            type: "t",
            value: null
        },
        offsetRepeat: {
            type: "v4",
            value: new THREE.Vector4(0, 0, 1, 1)
        },
        specularMap: {
            type: "t",
            value: null
        },
        alphaMap: {
            type: "t",
            value: null
        },
        envMap: {
            type: "t",
            value: null
        },
        flipEnvMap: {
            type: "1f",
            value: -1
        },
        reflectivity: {
            type: "1f",
            value: 1
        },
        refractionRatio: {
            type: "1f",
            value: .98
        }
    },
    aomap: {
        aoMap: {
            type: "t",
            value: null
        },
        aoMapIntensity: {
            type: "1f",
            value: 1
        }
    },
    lightmap: {
        lightMap: {
            type: "t",
            value: null
        },
        lightMapIntensity: {
            type: "1f",
            value: 1
        }
    },
    emissivemap: {
        emissiveMap: {
            type: "t",
            value: null
        }
    },
    bumpmap: {
        bumpMap: {
            type: "t",
            value: null
        },
        bumpScale: {
            type: "1f",
            value: 1
        }
    },
    normalmap: {
        normalMap: {
            type: "t",
            value: null
        },
        normalScale: {
            type: "v2",
            value: new THREE.Vector2(1, 1)
        }
    },
    displacementmap: {
        displacementMap: {
            type: "t",
            value: null
        },
        displacementScale: {
            type: "1f",
            value: 1
        },
        displacementBias: {
            type: "1f",
            value: 0
        }
    },
    roughnessmap: {
        roughnessMap: {
            type: "t",
            value: null
        }
    },
    metalnessmap: {
        metalnessMap: {
            type: "t",
            value: null
        }
    },
    fog: {
        fogDensity: {
            type: "1f",
            value: .00025
        },
        fogNear: {
            type: "1f",
            value: 1
        },
        fogFar: {
            type: "1f",
            value: 2e3
        },
        fogColor: {
            type: "c",
            value: new THREE.Color(16777215)
        }
    },
    lights: {
        ambientLightColor: {
            type: "3fv",
            value: []
        },
        directionalLights: {
            type: "sa",
            value: [],
            properties: {
                direction: {
                    type: "v3"
                },
                color: {
                    type: "c"
                },
                shadow: {
                    type: "1i"
                },
                shadowBias: {
                    type: "1f"
                },
                shadowRadius: {
                    type: "1f"
                },
                shadowMapSize: {
                    type: "v2"
                }
            }
        },
        directionalShadowMap: {
            type: "tv",
            value: []
        },
        directionalShadowMatrix: {
            type: "m4v",
            value: []
        },
        spotLights: {
            type: "sa",
            value: [],
            properties: {
                color: {
                    type: "c"
                },
                position: {
                    type: "v3"
                },
                direction: {
                    type: "v3"
                },
                distance: {
                    type: "1f"
                },
                coneCos: {
                    type: "1f"
                },
                penumbraCos: {
                    type: "1f"
                },
                decay: {
                    type: "1f"
                },
                shadow: {
                    type: "1i"
                },
                shadowBias: {
                    type: "1f"
                },
                shadowRadius: {
                    type: "1f"
                },
                shadowMapSize: {
                    type: "v2"
                }
            }
        },
        spotShadowMap: {
            type: "tv",
            value: []
        },
        spotShadowMatrix: {
            type: "m4v",
            value: []
        },
        pointLights: {
            type: "sa",
            value: [],
            properties: {
                color: {
                    type: "c"
                },
                position: {
                    type: "v3"
                },
                decay: {
                    type: "1f"
                },
                distance: {
                    type: "1f"
                },
                shadow: {
                    type: "1i"
                },
                shadowBias: {
                    type: "1f"
                },
                shadowRadius: {
                    type: "1f"
                },
                shadowMapSize: {
                    type: "v2"
                }
            }
        },
        pointShadowMap: {
            type: "tv",
            value: []
        },
        pointShadowMatrix: {
            type: "m4v",
            value: []
        },
        hemisphereLights: {
            type: "sa",
            value: [],
            properties: {
                direction: {
                    type: "v3"
                },
                skyColor: {
                    type: "c"
                },
                groundColor: {
                    type: "c"
                }
            }
        }
    },
    points: {
        diffuse: {
            type: "c",
            value: new THREE.Color(15658734)
        },
        opacity: {
            type: "1f",
            value: 1
        },
        size: {
            type: "1f",
            value: 1
        },
        scale: {
            type: "1f",
            value: 1
        },
        map: {
            type: "t",
            value: null
        },
        offsetRepeat: {
            type: "v4",
            value: new THREE.Vector4(0, 0, 1, 1)
        }
    }
};
THREE.ShaderChunk.cube_frag = "uniform samplerCube tCube;\nuniform float tFlip;\nvarying vec3 vWorldPosition;\n#include <common>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tgl_FragColor = textureCube( tCube, vec3( tFlip * vWorldPosition.x, vWorldPosition.yz ) );\n\t#include <logdepthbuf_fragment>\n}\n";
THREE.ShaderChunk.cube_vert = "varying vec3 vWorldPosition;\n#include <common>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\tvWorldPosition = transformDirection( position, modelMatrix );\n\t#include <begin_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n}\n";
THREE.ShaderChunk.depth_frag = "#if DEPTH_PACKING == 3200\n\tuniform float opacity;\n#endif\n#include <common>\n#include <packing>\n#include <uv_pars_fragment>\n#include <map_pars_fragment>\n#include <alphamap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec4 diffuseColor = vec4( 1.0 );\n\t#if DEPTH_PACKING == 3200\n\t\tdiffuseColor.a = opacity;\n\t#endif\n\t#include <map_fragment>\n\t#include <alphamap_fragment>\n\t#include <alphatest_fragment>\n\t#include <logdepthbuf_fragment>\n\t#if DEPTH_PACKING == 3200\n\t\tgl_FragColor = vec4( vec3( gl_FragCoord.z ), opacity );\n\t#elif DEPTH_PACKING == 3201\n\t\tgl_FragColor = packDepthToRGBA( gl_FragCoord.z );\n\t#endif\n}\n";
THREE.ShaderChunk.depth_vert = "#include <common>\n#include <uv_pars_vertex>\n#include <displacementmap_pars_vertex>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <uv_vertex>\n\t#include <skinbase_vertex>\n\t#include <begin_vertex>\n\t#include <displacementmap_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n}\n";
THREE.ShaderChunk.distanceRGBA_frag = "uniform vec3 lightPos;\nvarying vec4 vWorldPosition;\n#include <common>\n#include <packing>\n#include <clipping_planes_pars_fragment>\nvoid main () {\n\t#include <clipping_planes_fragment>\n\tgl_FragColor = packDepthToRGBA( length( vWorldPosition.xyz - lightPos.xyz ) / 1000.0 );\n}\n";
THREE.ShaderChunk.distanceRGBA_vert = "varying vec4 vWorldPosition;\n#include <common>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <skinbase_vertex>\n\t#include <begin_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <worldpos_vertex>\n\t#include <clipping_planes_vertex>\n\tvWorldPosition = worldPosition;\n}\n";
THREE.ShaderChunk.equirect_frag = "uniform sampler2D tEquirect;\nuniform float tFlip;\nvarying vec3 vWorldPosition;\n#include <common>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec3 direction = normalize( vWorldPosition );\n\tvec2 sampleUV;\n\tsampleUV.y = saturate( tFlip * direction.y * -0.5 + 0.5 );\n\tsampleUV.x = atan( direction.z, direction.x ) * RECIPROCAL_PI2 + 0.5;\n\tgl_FragColor = texture2D( tEquirect, sampleUV );\n\t#include <logdepthbuf_fragment>\n}\n";
THREE.ShaderChunk.equirect_vert = "varying vec3 vWorldPosition;\n#include <common>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\tvWorldPosition = transformDirection( position, modelMatrix );\n\t#include <begin_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n}\n";
THREE.ShaderChunk.linedashed_frag = "uniform vec3 diffuse;\nuniform float opacity;\nuniform float dashSize;\nuniform float totalSize;\nvarying float vLineDistance;\n#include <common>\n#include <color_pars_fragment>\n#include <fog_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tif ( mod( vLineDistance, totalSize ) > dashSize ) {\n\t\tdiscard;\n\t}\n\tvec3 outgoingLight = vec3( 0.0 );\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\t#include <logdepthbuf_fragment>\n\t#include <color_fragment>\n\toutgoingLight = diffuseColor.rgb;\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n";
THREE.ShaderChunk.linedashed_vert = "uniform float scale;\nattribute float lineDistance;\nvarying float vLineDistance;\n#include <common>\n#include <color_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <color_vertex>\n\tvLineDistance = scale * lineDistance;\n\tvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n\tgl_Position = projectionMatrix * mvPosition;\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n}\n";
THREE.ShaderChunk.meshbasic_frag = "uniform vec3 diffuse;\nuniform float opacity;\n#ifndef FLAT_SHADED\n\tvarying vec3 vNormal;\n#endif\n#include <common>\n#include <color_pars_fragment>\n#include <uv_pars_fragment>\n#include <uv2_pars_fragment>\n#include <map_pars_fragment>\n#include <alphamap_pars_fragment>\n#include <aomap_pars_fragment>\n#include <envmap_pars_fragment>\n#include <fog_pars_fragment>\n#include <specularmap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\t#include <logdepthbuf_fragment>\n\t#include <map_fragment>\n\t#include <color_fragment>\n\t#include <alphamap_fragment>\n\t#include <alphatest_fragment>\n\t#include <specularmap_fragment>\n\tReflectedLight reflectedLight;\n\treflectedLight.directDiffuse = vec3( 0.0 );\n\treflectedLight.directSpecular = vec3( 0.0 );\n\treflectedLight.indirectDiffuse = diffuseColor.rgb;\n\treflectedLight.indirectSpecular = vec3( 0.0 );\n\t#include <aomap_fragment>\n\tvec3 outgoingLight = reflectedLight.indirectDiffuse;\n\t#include <envmap_fragment>\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n";
THREE.ShaderChunk.meshbasic_vert = "#include <common>\n#include <uv_pars_vertex>\n#include <uv2_pars_vertex>\n#include <envmap_pars_vertex>\n#include <color_pars_vertex>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <uv_vertex>\n\t#include <uv2_vertex>\n\t#include <color_vertex>\n\t#include <skinbase_vertex>\n\t#ifdef USE_ENVMAP\n\t#include <beginnormal_vertex>\n\t#include <morphnormal_vertex>\n\t#include <skinnormal_vertex>\n\t#include <defaultnormal_vertex>\n\t#endif\n\t#include <begin_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <worldpos_vertex>\n\t#include <clipping_planes_vertex>\n\t#include <envmap_vertex>\n}\n";
THREE.ShaderChunk.meshlambert_frag = "uniform vec3 diffuse;\nuniform vec3 emissive;\nuniform float opacity;\nvarying vec3 vLightFront;\n#ifdef DOUBLE_SIDED\n\tvarying vec3 vLightBack;\n#endif\n#include <common>\n#include <packing>\n#include <color_pars_fragment>\n#include <uv_pars_fragment>\n#include <uv2_pars_fragment>\n#include <map_pars_fragment>\n#include <alphamap_pars_fragment>\n#include <aomap_pars_fragment>\n#include <lightmap_pars_fragment>\n#include <emissivemap_pars_fragment>\n#include <envmap_pars_fragment>\n#include <bsdfs>\n#include <lights_pars>\n#include <fog_pars_fragment>\n#include <shadowmap_pars_fragment>\n#include <shadowmask_pars_fragment>\n#include <specularmap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\tReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );\n\tvec3 totalEmissiveRadiance = emissive;\n\t#include <logdepthbuf_fragment>\n\t#include <map_fragment>\n\t#include <color_fragment>\n\t#include <alphamap_fragment>\n\t#include <alphatest_fragment>\n\t#include <specularmap_fragment>\n\t#include <emissivemap_fragment>\n\treflectedLight.indirectDiffuse = getAmbientLightIrradiance( ambientLightColor );\n\t#include <lightmap_fragment>\n\treflectedLight.indirectDiffuse *= BRDF_Diffuse_Lambert( diffuseColor.rgb );\n\t#ifdef DOUBLE_SIDED\n\t\treflectedLight.directDiffuse = ( gl_FrontFacing ) ? vLightFront : vLightBack;\n\t#else\n\t\treflectedLight.directDiffuse = vLightFront;\n\t#endif\n\treflectedLight.directDiffuse *= BRDF_Diffuse_Lambert( diffuseColor.rgb ) * getShadowMask();\n\t#include <aomap_fragment>\n\tvec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + totalEmissiveRadiance;\n\t#include <envmap_fragment>\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n";
THREE.ShaderChunk.meshlambert_vert = "#define LAMBERT\nvarying vec3 vLightFront;\n#ifdef DOUBLE_SIDED\n\tvarying vec3 vLightBack;\n#endif\n#include <common>\n#include <uv_pars_vertex>\n#include <uv2_pars_vertex>\n#include <envmap_pars_vertex>\n#include <bsdfs>\n#include <lights_pars>\n#include <color_pars_vertex>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <shadowmap_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <uv_vertex>\n\t#include <uv2_vertex>\n\t#include <color_vertex>\n\t#include <beginnormal_vertex>\n\t#include <morphnormal_vertex>\n\t#include <skinbase_vertex>\n\t#include <skinnormal_vertex>\n\t#include <defaultnormal_vertex>\n\t#include <begin_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n\t#include <worldpos_vertex>\n\t#include <envmap_vertex>\n\t#include <lights_lambert_vertex>\n\t#include <shadowmap_vertex>\n}\n";
THREE.ShaderChunk.meshphong_frag = "#define PHONG\nuniform vec3 diffuse;\nuniform vec3 emissive;\nuniform vec3 specular;\nuniform float shininess;\nuniform float opacity;\n#include <common>\n#include <packing>\n#include <color_pars_fragment>\n#include <uv_pars_fragment>\n#include <uv2_pars_fragment>\n#include <map_pars_fragment>\n#include <alphamap_pars_fragment>\n#include <aomap_pars_fragment>\n#include <lightmap_pars_fragment>\n#include <emissivemap_pars_fragment>\n#include <envmap_pars_fragment>\n#include <fog_pars_fragment>\n#include <bsdfs>\n#include <lights_pars>\n#include <lights_phong_pars_fragment>\n#include <shadowmap_pars_fragment>\n#include <bumpmap_pars_fragment>\n#include <normalmap_pars_fragment>\n#include <specularmap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\tReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );\n\tvec3 totalEmissiveRadiance = emissive;\n\t#include <logdepthbuf_fragment>\n\t#include <map_fragment>\n\t#include <color_fragment>\n\t#include <alphamap_fragment>\n\t#include <alphatest_fragment>\n\t#include <specularmap_fragment>\n\t#include <normal_fragment>\n\t#include <emissivemap_fragment>\n\t#include <lights_phong_fragment>\n\t#include <lights_template>\n\t#include <aomap_fragment>\n\tvec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;\n\t#include <envmap_fragment>\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n";
THREE.ShaderChunk.meshphong_vert = "#define PHONG\nvarying vec3 vViewPosition;\n#ifndef FLAT_SHADED\n\tvarying vec3 vNormal;\n#endif\n#include <common>\n#include <uv_pars_vertex>\n#include <uv2_pars_vertex>\n#include <displacementmap_pars_vertex>\n#include <envmap_pars_vertex>\n#include <color_pars_vertex>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <shadowmap_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <uv_vertex>\n\t#include <uv2_vertex>\n\t#include <color_vertex>\n\t#include <beginnormal_vertex>\n\t#include <morphnormal_vertex>\n\t#include <skinbase_vertex>\n\t#include <skinnormal_vertex>\n\t#include <defaultnormal_vertex>\n#ifndef FLAT_SHADED\n\tvNormal = normalize( transformedNormal );\n#endif\n\t#include <begin_vertex>\n\t#include <displacementmap_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n\tvViewPosition = - mvPosition.xyz;\n\t#include <worldpos_vertex>\n\t#include <envmap_vertex>\n\t#include <shadowmap_vertex>\n}\n";
THREE.ShaderChunk.meshphysical_frag = "#define PHYSICAL\nuniform vec3 diffuse;\nuniform vec3 emissive;\nuniform float roughness;\nuniform float metalness;\nuniform float opacity;\nuniform float envMapIntensity;\nvarying vec3 vViewPosition;\n#ifndef FLAT_SHADED\n\tvarying vec3 vNormal;\n#endif\n#include <common>\n#include <packing>\n#include <color_pars_fragment>\n#include <uv_pars_fragment>\n#include <uv2_pars_fragment>\n#include <map_pars_fragment>\n#include <alphamap_pars_fragment>\n#include <aomap_pars_fragment>\n#include <lightmap_pars_fragment>\n#include <emissivemap_pars_fragment>\n#include <envmap_pars_fragment>\n#include <fog_pars_fragment>\n#include <bsdfs>\n#include <cube_uv_reflection_fragment>\n#include <lights_pars>\n#include <lights_physical_pars_fragment>\n#include <shadowmap_pars_fragment>\n#include <bumpmap_pars_fragment>\n#include <normalmap_pars_fragment>\n#include <roughnessmap_pars_fragment>\n#include <metalnessmap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\tReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );\n\tvec3 totalEmissiveRadiance = emissive;\n\t#include <logdepthbuf_fragment>\n\t#include <map_fragment>\n\t#include <color_fragment>\n\t#include <alphamap_fragment>\n\t#include <alphatest_fragment>\n\t#include <specularmap_fragment>\n\t#include <roughnessmap_fragment>\n\t#include <metalnessmap_fragment>\n\t#include <normal_fragment>\n\t#include <emissivemap_fragment>\n\t#include <lights_physical_fragment>\n\t#include <lights_template>\n\t#include <aomap_fragment>\n\tvec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n";
THREE.ShaderChunk.meshphysical_vert = "#define PHYSICAL\nvarying vec3 vViewPosition;\n#ifndef FLAT_SHADED\n\tvarying vec3 vNormal;\n#endif\n#include <common>\n#include <uv_pars_vertex>\n#include <uv2_pars_vertex>\n#include <displacementmap_pars_vertex>\n#include <color_pars_vertex>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <shadowmap_pars_vertex>\n#include <specularmap_pars_fragment>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <uv_vertex>\n\t#include <uv2_vertex>\n\t#include <color_vertex>\n\t#include <beginnormal_vertex>\n\t#include <morphnormal_vertex>\n\t#include <skinbase_vertex>\n\t#include <skinnormal_vertex>\n\t#include <defaultnormal_vertex>\n#ifndef FLAT_SHADED\n\tvNormal = normalize( transformedNormal );\n#endif\n\t#include <begin_vertex>\n\t#include <displacementmap_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n\tvViewPosition = - mvPosition.xyz;\n\t#include <worldpos_vertex>\n\t#include <shadowmap_vertex>\n}\n";
THREE.ShaderChunk.normal_frag = "uniform float opacity;\nvarying vec3 vNormal;\n#include <common>\n#include <packing>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tgl_FragColor = vec4( packNormalToRGB( vNormal ), opacity );\n\t#include <logdepthbuf_fragment>\n}\n";
THREE.ShaderChunk.normal_vert = "varying vec3 vNormal;\n#include <common>\n#include <morphtarget_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\tvNormal = normalize( normalMatrix * normal );\n\t#include <begin_vertex>\n\t#include <morphtarget_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n}\n";
THREE.ShaderChunk.points_frag = "uniform vec3 diffuse;\nuniform float opacity;\n#include <common>\n#include <color_pars_fragment>\n#include <map_particle_pars_fragment>\n#include <fog_pars_fragment>\n#include <shadowmap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec3 outgoingLight = vec3( 0.0 );\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\t#include <logdepthbuf_fragment>\n\t#include <map_particle_fragment>\n\t#include <color_fragment>\n\t#include <alphatest_fragment>\n\toutgoingLight = diffuseColor.rgb;\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n";
THREE.ShaderChunk.points_vert = "uniform float size;\nuniform float scale;\n#include <common>\n#include <color_pars_vertex>\n#include <shadowmap_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <color_vertex>\n\t#include <begin_vertex>\n\t#include <project_vertex>\n\t#ifdef USE_SIZEATTENUATION\n\t\tgl_PointSize = size * ( scale / - mvPosition.z );\n\t#else\n\t\tgl_PointSize = size;\n\t#endif\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n\t#include <worldpos_vertex>\n\t#include <shadowmap_vertex>\n}\n";
THREE.ShaderLib = {
    basic: {
        uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.common, THREE.UniformsLib.aomap, THREE.UniformsLib.fog]),
        vertexShader: THREE.ShaderChunk.meshbasic_vert,
        fragmentShader: THREE.ShaderChunk.meshbasic_frag
    },
    lambert: {
        uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.common, THREE.UniformsLib.aomap, THREE.UniformsLib.lightmap, THREE.UniformsLib.emissivemap, THREE.UniformsLib.fog, THREE.UniformsLib.lights, {
            emissive: {
                type: "c",
                value: new THREE.Color(0)
            }
        }]),
        vertexShader: THREE.ShaderChunk.meshlambert_vert,
        fragmentShader: THREE.ShaderChunk.meshlambert_frag
    },
    phong: {
        uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.common, THREE.UniformsLib.aomap, THREE.UniformsLib.lightmap, THREE.UniformsLib.emissivemap, THREE.UniformsLib.bumpmap, THREE.UniformsLib.normalmap, THREE.UniformsLib.displacementmap, THREE.UniformsLib.fog, THREE.UniformsLib.lights, {
            emissive: {
                type: "c",
                value: new THREE.Color(0)
            },
            specular: {
                type: "c",
                value: new THREE.Color(1118481)
            },
            shininess: {
                type: "1f",
                value: 30
            }
        }]),
        vertexShader: THREE.ShaderChunk.meshphong_vert,
        fragmentShader: THREE.ShaderChunk.meshphong_frag
    },
    standard: {
        uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.common, THREE.UniformsLib.aomap, THREE.UniformsLib.lightmap, THREE.UniformsLib.emissivemap, THREE.UniformsLib.bumpmap, THREE.UniformsLib.normalmap, THREE.UniformsLib.displacementmap, THREE.UniformsLib.roughnessmap, THREE.UniformsLib.metalnessmap, THREE.UniformsLib.fog, THREE.UniformsLib.lights, {
            emissive: {
                type: "c",
                value: new THREE.Color(0)
            },
            roughness: {
                type: "1f",
                value: .5
            },
            metalness: {
                type: "1f",
                value: 0
            },
            envMapIntensity: {
                type: "1f",
                value: 1
            }
        }]),
        vertexShader: THREE.ShaderChunk.meshphysical_vert,
        fragmentShader: THREE.ShaderChunk.meshphysical_frag
    },
    points: {
        uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.points, THREE.UniformsLib.fog]),
        vertexShader: THREE.ShaderChunk.points_vert,
        fragmentShader: THREE.ShaderChunk.points_frag
    },
    dashed: {
        uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.common, THREE.UniformsLib.fog, {
            scale: {
                type: "1f",
                value: 1
            },
            dashSize: {
                type: "1f",
                value: 1
            },
            totalSize: {
                type: "1f",
                value: 2
            }
        }]),
        vertexShader: THREE.ShaderChunk.linedashed_vert,
        fragmentShader: THREE.ShaderChunk.linedashed_frag
    },
    depth: {
        uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.common, THREE.UniformsLib.displacementmap]),
        vertexShader: THREE.ShaderChunk.depth_vert,
        fragmentShader: THREE.ShaderChunk.depth_frag
    },
    normal: {
        uniforms: {
            opacity: {
                type: "1f",
                value: 1
            }
        },
        vertexShader: THREE.ShaderChunk.normal_vert,
        fragmentShader: THREE.ShaderChunk.normal_frag
    },
    cube: {
        uniforms: {
            tCube: {
                type: "t",
                value: null
            },
            tFlip: {
                type: "1f",
                value: -1
            }
        },
        vertexShader: THREE.ShaderChunk.cube_vert,
        fragmentShader: THREE.ShaderChunk.cube_frag
    },
    equirect: {
        uniforms: {
            tEquirect: {
                type: "t",
                value: null
            },
            tFlip: {
                type: "1f",
                value: -1
            }
        },
        vertexShader: THREE.ShaderChunk.equirect_vert,
        fragmentShader: THREE.ShaderChunk.equirect_frag
    },
    distanceRGBA: {
        uniforms: {
            lightPos: {
                type: "v3",
                value: new THREE.Vector3
            }
        },
        vertexShader: THREE.ShaderChunk.distanceRGBA_vert,
        fragmentShader: THREE.ShaderChunk.distanceRGBA_frag
    }
};
THREE.ShaderLib.physical = {
    uniforms: THREE.UniformsUtils.merge([THREE.ShaderLib.standard.uniforms, {}]),
    vertexShader: THREE.ShaderChunk.meshphysical_vert,
    fragmentShader: THREE.ShaderChunk.meshphysical_frag
};
THREE.WebGLRenderer = function(n) {
    function ir() {
        return ei === null ? l : 1
    }

    function ai(n, t, r, u) {
        bi === !0 && (n *= u, t *= u, r *= u);
        i.clearColor(n, t, r, u)
    }

    function rr() {
        i.init();
        i.scissor(ct.copy(hi).multiplyScalar(l));
        i.viewport(it.copy(rt).multiplyScalar(l));
        ai(c.r, c.g, c.b, b)
    }

    function ur() {
        fi = null;
        p = null;
        tt = "";
        nt = -1;
        i.reset()
    }

    function or(n) {
        n.preventDefault();
        ur();
        rr();
        u.clear()
    }

    function yi(n) {
        var t = n.target;
        t.removeEventListener("dispose", yi);
        au(t);
        k.textures--
    }

    function sr(n) {
        var t = n.target;
        t.removeEventListener("dispose", sr);
        vu(t);
        k.textures--
    }

    function hr(n) {
        var t = n.target;
        t.removeEventListener("dispose", hr);
        yu(t)
    }

    function au(n) {
        var i = u.get(n);
        if (n.image && i.__image__webglTextureCube) t.deleteTexture(i.__image__webglTextureCube);
        else {
            if (i.__webglInit === undefined) return;
            t.deleteTexture(i.__webglTexture)
        }
        u.delete(n)
    }

    function vu(n) {
        var i = u.get(n),
            f = u.get(n.texture),
            r;
        if (n) {
            if (f.__webglTexture !== undefined && t.deleteTexture(f.__webglTexture), n.depthTexture && n.depthTexture.dispose(), n instanceof THREE.WebGLRenderTargetCube)
                for (r = 0; r < 6; r++) t.deleteFramebuffer(i.__webglFramebuffer[r]), i.__webglDepthbuffer && t.deleteRenderbuffer(i.__webglDepthbuffer[r]);
            else t.deleteFramebuffer(i.__webglFramebuffer), i.__webglDepthbuffer && t.deleteRenderbuffer(i.__webglDepthbuffer);
            u.delete(n.texture);
            u.delete(n)
        }
    }

    function yu(n) {
        cr(n);
        u.delete(n)
    }

    function cr(n) {
        var t = u.get(n).program;
        n.program = undefined;
        t !== undefined && ot.releaseProgram(t)
    }

    function pu(n, r, u, e) {
        var v, y, s, o, p, w, a;
        if (u instanceof THREE.InstancedBufferGeometry && (v = f.get("ANGLE_instanced_arrays"), v === null)) {
            console.error("THREE.WebGLRenderer.setupVertexAttributes: using THREE.InstancedBufferGeometry but hardware does not support extension ANGLE_instanced_arrays.");
            return
        }
        e === undefined && (e = 0);
        i.initAttributes();
        var nt = u.attributes,
            b = r.getAttributes(),
            k = n.defaultAttributeValues;
        for (y in b)
            if (s = b[y], s >= 0)
                if (o = nt[y], o !== undefined) {
                    var h = t.FLOAT,
                        c = o.array,
                        d = o.normalized;
                    if (c instanceof Float32Array ? h = t.FLOAT : c instanceof Float64Array ? console.warn("Unsupported data buffer format: Float64Array") : c instanceof Uint16Array ? h = t.UNSIGNED_SHORT : c instanceof Int16Array ? h = t.SHORT : c instanceof Uint32Array ? h = t.UNSIGNED_INT : c instanceof Int32Array ? h = t.INT : c instanceof Int8Array ? h = t.BYTE : c instanceof Uint8Array && (h = t.UNSIGNED_BYTE), p = o.itemSize, w = et.getAttributeBuffer(o), o instanceof THREE.InterleavedBufferAttribute) {
                        var l = o.data,
                            g = l.stride,
                            tt = o.offset;
                        l instanceof THREE.InstancedInterleavedBuffer ? (i.enableAttributeAndDivisor(s, l.meshPerAttribute, v), u.maxInstancedCount === undefined && (u.maxInstancedCount = l.meshPerAttribute * l.count)) : i.enableAttribute(s);
                        t.bindBuffer(t.ARRAY_BUFFER, w);
                        t.vertexAttribPointer(s, p, h, d, g * l.array.BYTES_PER_ELEMENT, (e * g + tt) * l.array.BYTES_PER_ELEMENT)
                    } else o instanceof THREE.InstancedBufferAttribute ? (i.enableAttributeAndDivisor(s, o.meshPerAttribute, v), u.maxInstancedCount === undefined && (u.maxInstancedCount = o.meshPerAttribute * o.count)) : i.enableAttribute(s), t.bindBuffer(t.ARRAY_BUFFER, w), t.vertexAttribPointer(s, p, h, d, 0, e * p * o.array.BYTES_PER_ELEMENT)
                } else if (k !== undefined && (a = k[y], a !== undefined)) switch (a.length) {
            case 2:
                t.vertexAttrib2fv(s, a);
                break;
            case 3:
                t.vertexAttrib3fv(s, a);
                break;
            case 4:
                t.vertexAttrib4fv(s, a);
                break;
            default:
                t.vertexAttrib1fv(s, a)
        }
        i.disableUnusedAttributes()
    }

    function wu(n, t) {
        return Math.abs(t[0]) - Math.abs(n[0])
    }

    function bu(n, t) {
        return n.object.renderOrder !== t.object.renderOrder ? n.object.renderOrder - t.object.renderOrder : n.material.id !== t.material.id ? n.material.id - t.material.id : n.z !== t.z ? n.z - t.z : n.id - t.id
    }

    function ku(n, t) {
        return n.object.renderOrder !== t.object.renderOrder ? n.object.renderOrder - t.object.renderOrder : n.z !== t.z ? t.z - n.z : n.id - t.id
    }

    function pi(n, t, i, r, u) {
        var e, s, f;
        i.transparent ? (e = g, s = ++ii) : (e = d, s = ++ti);
        f = e[s];
        f !== undefined ? (f.id = n.id, f.object = n, f.geometry = t, f.material = i, f.z = o.z, f.group = u) : (f = {
            id: n.id,
            object: n,
            geometry: t,
            material: i,
            z: o.z,
            group: u
        }, e.push(f))
    }

    function lr(n) {
        var i = n.geometry,
            t;
        if (i.boundingSphere === null && i.computeBoundingSphere(), t = hu.copy(i.boundingSphere).applyMatrix4(n.matrixWorld), !gi.intersectsSphere(t)) return !1;
        if (v === 0) return !0;
        var u = e.clippingPlanes,
            f = t.center,
            o = -t.radius,
            r = 0;
        do
            if (u[r].distanceToPoint(f) < o) return !1; while (++r !== v);
        return !0
    }

    function ar(n, t) {
        var r, f, s, a, h, c, l, i, u;
        if (n.visible !== !1) {
            if (n.layers.test(t.layers))
                if (n instanceof THREE.Light) st.push(n);
                else if (n instanceof THREE.Sprite)(n.frustumCulled === !1 || lr(n) === !0) && ri.push(n);
            else if (n instanceof THREE.LensFlare) ui.push(n);
            else if (n instanceof THREE.ImmediateRenderObject) e.sortObjects === !0 && (o.setFromMatrixPosition(n.matrixWorld), o.applyProjection(wt)), pi(n, null, n.material, o.z, null);
            else if ((n instanceof THREE.Mesh || n instanceof THREE.Line || n instanceof THREE.Points) && (n instanceof THREE.SkinnedMesh && n.skeleton.update(), (n.frustumCulled === !1 || lr(n) === !0) && (r = n.material, r.visible === !0)))
                if (e.sortObjects === !0 && (o.setFromMatrixPosition(n.matrixWorld), o.applyProjection(wt)), f = et.update(n), r instanceof THREE.MultiMaterial)
                    for (s = f.groups, a = r.materials, i = 0, u = s.length; i < u; i++) h = s[i], c = a[h.materialIndex], c.visible === !0 && pi(n, f, c, o.z, h);
                else pi(n, f, r, o.z, null);
            for (l = n.children, i = 0, u = l.length; i < u; i++) ar(l[i], t)
        }
    }

    function kt(n, t, i, r) {
        for (var c, f = 0, h = n.length; f < h; f++) {
            var o = n[f],
                u = o.object,
                l = o.geometry,
                s = r === undefined ? o.material : r,
                a = o.group;
            u.modelViewMatrix.multiplyMatrices(t.matrixWorldInverse, u.matrixWorld);
            u.normalMatrix.getNormalMatrix(u.modelViewMatrix);
            u instanceof THREE.ImmediateRenderObject ? (vr(s), c = yr(t, i, s, u), tt = "", u.render(function(n) {
                e.renderBufferImmediate(n, c, s)
            })) : e.renderBufferDirect(t, i, l, s, u, a)
        }
    }

    function du(n, t, i) {
        var o = u.get(n),
            c = ot.getParameters(n, r, t, v, i),
            w = ot.getProgramCode(n, c),
            h = o.program,
            b = !0,
            l, a, s, f, k, p;
        if (h === undefined) n.addEventListener("dispose", hr);
        else if (h.code !== w) cr(n);
        else {
            if (c.shaderID !== undefined) return;
            b = !1
        }
        if (b && (c.shaderID ? (l = THREE.ShaderLib[c.shaderID], o.__webglShader = {
                name: n.type,
                uniforms: THREE.UniformsUtils.clone(l.uniforms),
                vertexShader: l.vertexShader,
                fragmentShader: l.fragmentShader
            }) : o.__webglShader = {
                name: n.type,
                uniforms: n.uniforms,
                vertexShader: n.vertexShader,
                fragmentShader: n.fragmentShader
            }, n.__webglShader = o.__webglShader, h = ot.acquireProgram(n, c, w), o.program = h, n.program = h), a = h.getAttributes(), n.morphTargets)
            for (n.numSupportedMorphTargets = 0, s = 0; s < e.maxMorphTargets; s++) a["morphTarget" + s] >= 0 && n.numSupportedMorphTargets++;
        if (n.morphNormals)
            for (n.numSupportedMorphNormals = 0, s = 0; s < e.maxMorphNormals; s++) a["morphNormal" + s] >= 0 && n.numSupportedMorphNormals++;
        f = o.__webglShader.uniforms;
        (n instanceof THREE.ShaderMaterial || n instanceof THREE.RawShaderMaterial) && n.clipping !== !0 || (o.numClippingPlanes = v, f.clippingPlanes = y);
        (n instanceof THREE.MeshPhongMaterial || n instanceof THREE.MeshLambertMaterial || n instanceof THREE.MeshStandardMaterial || n.lights) && (o.lightsHash = r.hash, f.ambientLightColor.value = r.ambient, f.directionalLights.value = r.directional, f.spotLights.value = r.spot, f.pointLights.value = r.point, f.hemisphereLights.value = r.hemi, f.directionalShadowMap.value = r.directionalShadowMap, f.directionalShadowMatrix.value = r.directionalShadowMatrix, f.spotShadowMap.value = r.spotShadowMap, f.spotShadowMatrix.value = r.spotShadowMatrix, f.pointShadowMap.value = r.pointShadowMap, f.pointShadowMatrix.value = r.pointShadowMatrix);
        k = o.program.getUniforms();
        p = THREE.WebGLUniforms.seqWithValue(k.seq, f);
        o.uniformsList = p;
        o.dynamicUniforms = THREE.WebGLUniforms.splitDynamic(p, f)
    }

    function vr(n) {
        gu(n);
        n.transparent === !0 ? i.setBlending(n.blending, n.blendEquation, n.blendSrc, n.blendDst, n.blendEquationAlpha, n.blendSrcAlpha, n.blendDstAlpha, n.premultipliedAlpha) : i.setBlending(THREE.NoBlending);
        i.setDepthFunc(n.depthFunc);
        i.setDepthTest(n.depthTest);
        i.setDepthWrite(n.depthWrite);
        i.setColorWrite(n.colorWrite);
        i.setPolygonOffset(n.polygonOffset, n.polygonOffsetFactor, n.polygonOffsetUnits)
    }

    function gu(n) {
        n.side !== THREE.DoubleSide ? i.enable(t.CULL_FACE) : i.disable(t.CULL_FACE);
        i.setFlipSided(n.side === THREE.BackSide)
    }

    function yr(n, i, f, s) {
        var l, tt, g, y, k;
        si = 0;
        l = u.get(f);
        vt && ((yt || n !== p) && (tt = n === p && f.id === nt, af(f.clippingPlanes, f.clipShadows, n, l, tt)), l.numClippingPlanes !== undefined && l.numClippingPlanes !== v && (f.needsUpdate = !0));
        l.program === undefined && (f.needsUpdate = !0);
        l.lightsHash !== undefined && l.lightsHash !== r.hash && (f.needsUpdate = !0);
        f.needsUpdate && (du(f, i, s), f.needsUpdate = !1);
        var it = !1,
            b = !1,
            d = !1,
            w = l.program,
            c = w.getUniforms(),
            h = l.__webglShader.uniforms;
        return w.id !== fi && (t.useProgram(w.program), fi = w.id, it = !0, b = !0, d = !0), f.id !== nt && (nt = f.id, b = !0), (it || n !== p) && (c.set(t, n, "projectionMatrix"), a.logarithmicDepthBuffer && c.setValue(t, "logDepthBufFC", 2 / (Math.log(n.far + 1) / Math.LN2)), n !== p && (p = n, b = !0, d = !0), (f instanceof THREE.ShaderMaterial || f instanceof THREE.MeshPhongMaterial || f instanceof THREE.MeshStandardMaterial || f.envMap) && (g = c.map.cameraPosition, g !== undefined && g.setValue(t, o.setFromMatrixPosition(n.matrixWorld))), (f instanceof THREE.MeshPhongMaterial || f instanceof THREE.MeshLambertMaterial || f instanceof THREE.MeshBasicMaterial || f instanceof THREE.MeshStandardMaterial || f instanceof THREE.ShaderMaterial || f.skinning) && c.setValue(t, "viewMatrix", n.matrixWorldInverse), c.set(t, e, "toneMappingExposure"), c.set(t, e, "toneMappingWhitePoint")), f.skinning && (c.setOptional(t, s, "bindMatrix"), c.setOptional(t, s, "bindMatrixInverse"), y = s.skeleton, y && (a.floatVertexTextures && y.useVertexTexture ? (c.set(t, y, "boneTexture"), c.set(t, y, "boneTextureWidth"), c.set(t, y, "boneTextureHeight")) : c.setOptional(t, y, "boneMatrices"))), b && ((f instanceof THREE.MeshPhongMaterial || f instanceof THREE.MeshLambertMaterial || f instanceof THREE.MeshStandardMaterial || f.lights) && sf(h, d), i && f.fog && uf(h, i), (f instanceof THREE.MeshBasicMaterial || f instanceof THREE.MeshLambertMaterial || f instanceof THREE.MeshPhongMaterial || f instanceof THREE.MeshStandardMaterial || f instanceof THREE.MeshDepthMaterial) && nf(h, f), f instanceof THREE.LineBasicMaterial ? pr(h, f) : f instanceof THREE.LineDashedMaterial ? (pr(h, f), tf(h, f)) : f instanceof THREE.PointsMaterial ? rf(h, f) : f instanceof THREE.MeshLambertMaterial ? ff(h, f) : f instanceof THREE.MeshPhongMaterial ? ef(h, f) : f instanceof THREE.MeshPhysicalMaterial ? of (h, f) : f instanceof THREE.MeshStandardMaterial ? wr(h, f) : f instanceof THREE.MeshDepthMaterial ? f.displacementMap && (h.displacementMap.value = f.displacementMap, h.displacementScale.value = f.displacementScale, h.displacementBias.value = f.displacementBias) : f instanceof THREE.MeshNormalMaterial && (h.opacity.value = f.opacity), THREE.WebGLUniforms.upload(t, l.uniformsList, h, e)), c.set(t, s, "modelViewMatrix"), c.set(t, s, "normalMatrix"), c.setValue(t, "modelMatrix", s.matrixWorld), k = l.dynamicUniforms, k !== null && (THREE.WebGLUniforms.evalDynamic(k, h, s, n), THREE.WebGLUniforms.upload(t, k, h, e)), w
    }

    function nf(n, t) {
        var i, r, u;
        n.opacity.value = t.opacity;
        n.diffuse.value = t.color;
        t.emissive && n.emissive.value.copy(t.emissive).multiplyScalar(t.emissiveIntensity);
        n.map.value = t.map;
        n.specularMap.value = t.specularMap;
        n.alphaMap.value = t.alphaMap;
        t.aoMap && (n.aoMap.value = t.aoMap, n.aoMapIntensity.value = t.aoMapIntensity);
        t.map ? i = t.map : t.specularMap ? i = t.specularMap : t.displacementMap ? i = t.displacementMap : t.normalMap ? i = t.normalMap : t.bumpMap ? i = t.bumpMap : t.roughnessMap ? i = t.roughnessMap : t.metalnessMap ? i = t.metalnessMap : t.alphaMap ? i = t.alphaMap : t.emissiveMap && (i = t.emissiveMap);
        i !== undefined && (i instanceof THREE.WebGLRenderTarget && (i = i.texture), r = i.offset, u = i.repeat, n.offsetRepeat.value.set(r.x, r.y, u.x, u.y));
        n.envMap.value = t.envMap;
        n.flipEnvMap.value = t.envMap instanceof THREE.WebGLRenderTargetCube ? 1 : -1;
        n.reflectivity.value = t.reflectivity;
        n.refractionRatio.value = t.refractionRatio
    }

    function pr(n, t) {
        n.diffuse.value = t.color;
        n.opacity.value = t.opacity
    }

    function tf(n, t) {
        n.dashSize.value = t.dashSize;
        n.totalSize.value = t.dashSize + t.gapSize;
        n.scale.value = t.scale
    }

    function rf(n, t) {
        if (n.diffuse.value = t.color, n.opacity.value = t.opacity, n.size.value = t.size * l, n.scale.value = h.clientHeight * .5, n.map.value = t.map, t.map !== null) {
            var i = t.map.offset,
                r = t.map.repeat;
            n.offsetRepeat.value.set(i.x, i.y, r.x, r.y)
        }
    }

    function uf(n, t) {
        n.fogColor.value = t.color;
        t instanceof THREE.Fog ? (n.fogNear.value = t.near, n.fogFar.value = t.far) : t instanceof THREE.FogExp2 && (n.fogDensity.value = t.density)
    }

    function ff(n, t) {
        t.lightMap && (n.lightMap.value = t.lightMap, n.lightMapIntensity.value = t.lightMapIntensity);
        t.emissiveMap && (n.emissiveMap.value = t.emissiveMap)
    }

    function ef(n, t) {
        n.specular.value = t.specular;
        n.shininess.value = Math.max(t.shininess, .0001);
        t.lightMap && (n.lightMap.value = t.lightMap, n.lightMapIntensity.value = t.lightMapIntensity);
        t.emissiveMap && (n.emissiveMap.value = t.emissiveMap);
        t.bumpMap && (n.bumpMap.value = t.bumpMap, n.bumpScale.value = t.bumpScale);
        t.normalMap && (n.normalMap.value = t.normalMap, n.normalScale.value.copy(t.normalScale));
        t.displacementMap && (n.displacementMap.value = t.displacementMap, n.displacementScale.value = t.displacementScale, n.displacementBias.value = t.displacementBias)
    }

    function wr(n, t) {
        n.roughness.value = t.roughness;
        n.metalness.value = t.metalness;
        t.roughnessMap && (n.roughnessMap.value = t.roughnessMap);
        t.metalnessMap && (n.metalnessMap.value = t.metalnessMap);
        t.lightMap && (n.lightMap.value = t.lightMap, n.lightMapIntensity.value = t.lightMapIntensity);
        t.emissiveMap && (n.emissiveMap.value = t.emissiveMap);
        t.bumpMap && (n.bumpMap.value = t.bumpMap, n.bumpScale.value = t.bumpScale);
        t.normalMap && (n.normalMap.value = t.normalMap, n.normalScale.value.copy(t.normalScale));
        t.displacementMap && (n.displacementMap.value = t.displacementMap, n.displacementScale.value = t.displacementScale, n.displacementBias.value = t.displacementBias);
        t.envMap && (n.envMapIntensity.value = t.envMapIntensity)
    }

    function of (n, t) {
        wr(n, t)
    }

    function sf(n, t) {
        n.ambientLightColor.needsUpdate = t;
        n.directionalLights.needsUpdate = t;
        n.pointLights.needsUpdate = t;
        n.spotLights.needsUpdate = t;
        n.hemisphereLights.needsUpdate = t
    }

    function hf(n) {
        for (var u = 0, i, t = 0, f = n.length; t < f; t++) i = n[t], i.castShadow && (r.shadows[u++] = i);
        r.shadows.length = u
    }

    function cf(n, t) {
        for (var i, p = 0, w = 0, b = 0, s, f, k, h = t.matrixWorldInverse, c = 0, e = 0, l = 0, v = 0, u, a = 0, y = n.length; a < y; a++) i = n[a], s = i.color, f = i.intensity, k = i.distance, i instanceof THREE.AmbientLight ? (p += s.r * f, w += s.g * f, b += s.b * f) : i instanceof THREE.DirectionalLight ? (u = bt.get(i), u.color.copy(i.color).multiplyScalar(i.intensity), u.direction.setFromMatrixPosition(i.matrixWorld), o.setFromMatrixPosition(i.target.matrixWorld), u.direction.sub(o), u.direction.transformDirection(h), u.shadow = i.castShadow, i.castShadow && (u.shadowBias = i.shadow.bias, u.shadowRadius = i.shadow.radius, u.shadowMapSize = i.shadow.mapSize), r.directionalShadowMap[c] = i.shadow.map, r.directionalShadowMatrix[c] = i.shadow.matrix, r.directional[c++] = u) : i instanceof THREE.SpotLight ? (u = bt.get(i), u.position.setFromMatrixPosition(i.matrixWorld), u.position.applyMatrix4(h), u.color.copy(s).multiplyScalar(f), u.distance = k, u.direction.setFromMatrixPosition(i.matrixWorld), o.setFromMatrixPosition(i.target.matrixWorld), u.direction.sub(o), u.direction.transformDirection(h), u.coneCos = Math.cos(i.angle), u.penumbraCos = Math.cos(i.angle * (1 - i.penumbra)), u.decay = i.distance === 0 ? 0 : i.decay, u.shadow = i.castShadow, i.castShadow && (u.shadowBias = i.shadow.bias, u.shadowRadius = i.shadow.radius, u.shadowMapSize = i.shadow.mapSize), r.spotShadowMap[l] = i.shadow.map, r.spotShadowMatrix[l] = i.shadow.matrix, r.spot[l++] = u) : i instanceof THREE.PointLight ? (u = bt.get(i), u.position.setFromMatrixPosition(i.matrixWorld), u.position.applyMatrix4(h), u.color.copy(i.color).multiplyScalar(i.intensity), u.distance = i.distance, u.decay = i.distance === 0 ? 0 : i.decay, u.shadow = i.castShadow, i.castShadow && (u.shadowBias = i.shadow.bias, u.shadowRadius = i.shadow.radius, u.shadowMapSize = i.shadow.mapSize), r.pointShadowMap[e] = i.shadow.map, r.pointShadowMatrix[e] === undefined && (r.pointShadowMatrix[e] = new THREE.Matrix4), o.setFromMatrixPosition(i.matrixWorld).negate(), r.pointShadowMatrix[e].identity().setPosition(o), r.point[e++] = u) : i instanceof THREE.HemisphereLight && (u = bt.get(i), u.direction.setFromMatrixPosition(i.matrixWorld), u.direction.transformDirection(h), u.direction.normalize(), u.skyColor.copy(i.color).multiplyScalar(f), u.groundColor.copy(i.groundColor).multiplyScalar(f), r.hemi[v++] = u);
        r.ambient[0] = p;
        r.ambient[1] = w;
        r.ambient[2] = b;
        r.directional.length = c;
        r.spot.length = l;
        r.point.length = e;
        r.hemi.length = v;
        r.hash = c + "," + e + "," + l + "," + v + "," + r.shadows.length
    }

    function lf(n, t) {
        vt = e.clippingPlanes.length !== 0 || e.localClippingEnabled || ft !== 0 || yt;
        yt = e.localClippingEnabled;
        pt = dt(n, t, 0);
        ft = n !== null ? n.length : 0
    }

    function dt(n, t, i, r) {
        var f = n !== null ? n.length : 0,
            u = null,
            e, o, s;
        if (f !== 0) {
            if (u = y.value, r !== !0 || u === null) {
                var h = i + f * 4,
                    c = t.matrixWorldInverse,
                    l = su.getNormalMatrix(c);
                for ((u === null || u.length < h) && (u = new Float32Array(h)), e = 0, o = i; e !== f; ++e, o += 4) s = cu.copy(n[e]).applyMatrix4(c, l), s.normal.toArray(u, o), u[o + 3] = s.constant
            }
            y.value = u;
            y.needsUpdate = !0
        }
        return v = f, u
    }

    function br() {
        y.value !== pt && (y.value = pt, y.needsUpdate = ft > 0);
        v = ft
    }

    function af(n, t, i, r, u) {
        var f;
        if (yt && n !== null && n.length !== 0 && (!ut || t)) {
            var o = ut ? 0 : ft,
                s = o * 4,
                e = r.clippingState || null;
            for (y.value = e, e = dt(n, i, s, u), f = 0; f !== s; ++f) e[f] = pt[f];
            r.clippingState = e;
            v += o
        } else ut ? dt(null) : br()
    }

    function vf() {
        var n = si;
        return n >= a.maxTextures && console.warn("WebGLRenderer: trying to use " + n + " texture units while this GPU supports only " + a.maxTextures), si += 1, n
    }

    function gt(n, i, r) {
        var o;
        if (r ? (t.texParameteri(n, t.TEXTURE_WRAP_S, s(i.wrapS)), t.texParameteri(n, t.TEXTURE_WRAP_T, s(i.wrapT)), t.texParameteri(n, t.TEXTURE_MAG_FILTER, s(i.magFilter)), t.texParameteri(n, t.TEXTURE_MIN_FILTER, s(i.minFilter))) : (t.texParameteri(n, t.TEXTURE_WRAP_S, t.CLAMP_TO_EDGE), t.texParameteri(n, t.TEXTURE_WRAP_T, t.CLAMP_TO_EDGE), (i.wrapS !== THREE.ClampToEdgeWrapping || i.wrapT !== THREE.ClampToEdgeWrapping) && console.warn("THREE.WebGLRenderer: Texture is not power of two. Texture.wrapS and Texture.wrapT should be set to THREE.ClampToEdgeWrapping.", i), t.texParameteri(n, t.TEXTURE_MAG_FILTER, tu(i.magFilter)), t.texParameteri(n, t.TEXTURE_MIN_FILTER, tu(i.minFilter)), i.minFilter !== THREE.NearestFilter && i.minFilter !== THREE.LinearFilter && console.warn("THREE.WebGLRenderer: Texture is not power of two. Texture.minFilter should be set to THREE.NearestFilter or THREE.LinearFilter.", i)), o = f.get("EXT_texture_filter_anisotropic"), o) {
            if (i.type === THREE.FloatType && f.get("OES_texture_float_linear") === null) return;
            if (i.type === THREE.HalfFloatType && f.get("OES_texture_half_float_linear") === null) return;
            (i.anisotropy > 1 || u.get(i).__currentAnisotropy) && (t.texParameterf(n, o.TEXTURE_MAX_ANISOTROPY_EXT, Math.min(i.anisotropy, e.getMaxAnisotropy())), u.get(i).__currentAnisotropy = i.anisotropy)
        }
    }

    function yf(n, r, u) {
        var h, o, c, p, f, l;
        n.__webglInit === undefined && (n.__webglInit = !0, r.addEventListener("dispose", yi), n.__webglTexture = t.createTexture(), k.textures++);
        i.activeTexture(t.TEXTURE0 + u);
        i.bindTexture(t.TEXTURE_2D, n.__webglTexture);
        t.pixelStorei(t.UNPACK_FLIP_Y_WEBGL, r.flipY);
        t.pixelStorei(t.UNPACK_PREMULTIPLY_ALPHA_WEBGL, r.premultiplyAlpha);
        t.pixelStorei(t.UNPACK_ALIGNMENT, r.unpackAlignment);
        h = dr(r.image, a.maxTextureSize);
        pf(r) && ni(h) === !1 && (h = wf(h));
        var y = ni(h),
            e = s(r.format),
            v = s(r.type);
        if (gt(t.TEXTURE_2D, r, y), c = r.mipmaps, r instanceof THREE.DepthTexture) {
            if (p = t.DEPTH_COMPONENT, r.type === THREE.FloatType) {
                if (!li) throw new Error("Float Depth Texture only supported in WebGL2.0");
                p = t.DEPTH_COMPONENT32F
            } else li && (p = t.DEPTH_COMPONENT16);
            i.texImage2D(t.TEXTURE_2D, 0, p, h.width, h.height, 0, e, v, null)
        } else if (r instanceof THREE.DataTexture)
            if (c.length > 0 && y) {
                for (f = 0, l = c.length; f < l; f++) o = c[f], i.texImage2D(t.TEXTURE_2D, f, e, o.width, o.height, 0, e, v, o.data);
                r.generateMipmaps = !1
            } else i.texImage2D(t.TEXTURE_2D, 0, e, h.width, h.height, 0, e, v, h.data);
        else if (r instanceof THREE.CompressedTexture)
            for (f = 0, l = c.length; f < l; f++) o = c[f], r.format !== THREE.RGBAFormat && r.format !== THREE.RGBFormat ? i.getCompressedTextureFormats().indexOf(e) > -1 ? i.compressedTexImage2D(t.TEXTURE_2D, f, e, o.width, o.height, 0, o.data) : console.warn("THREE.WebGLRenderer: Attempt to load unsupported compressed texture format in .uploadTexture()") : i.texImage2D(t.TEXTURE_2D, f, e, o.width, o.height, 0, e, v, o.data);
        else if (c.length > 0 && y) {
            for (f = 0, l = c.length; f < l; f++) o = c[f], i.texImage2D(t.TEXTURE_2D, f, e, e, v, o);
            r.generateMipmaps = !1
        } else i.texImage2D(t.TEXTURE_2D, 0, e, e, v, h);
        if (r.generateMipmaps && y && t.generateMipmap(t.TEXTURE_2D), n.__version = r.version, r.onUpdate) r.onUpdate(r)
    }

    function kr(n, r) {
        var f, e;
        if (n instanceof THREE.WebGLRenderTarget && (n = n.texture), f = u.get(n), n.version > 0 && f.__version !== n.version) {
            if (e = n.image, e === undefined) {
                console.warn("THREE.WebGLRenderer: Texture marked for update but image is undefined", n);
                return
            }
            if (e.complete === !1) {
                console.warn("THREE.WebGLRenderer: Texture marked for update but image is incomplete", n);
                return
            }
            yf(f, n, r);
            return
        }
        i.activeTexture(t.TEXTURE0 + r);
        i.bindTexture(t.TEXTURE_2D, f.__webglTexture)
    }

    function dr(n, t) {
        var r, i, u;
        return n.width > t || n.height > t ? (r = t / Math.max(n.width, n.height), i = document.createElement("canvas"), i.width = Math.floor(n.width * r), i.height = Math.floor(n.height * r), u = i.getContext("2d"), u.drawImage(n, 0, 0, n.width, n.height, 0, 0, i.width, i.height), console.warn("THREE.WebGLRenderer: image is too big (" + n.width + "x" + n.height + "). Resized to " + i.width + "x" + i.height, n), i) : n
    }

    function ni(n) {
        return THREE.Math.isPowerOfTwo(n.width) && THREE.Math.isPowerOfTwo(n.height)
    }

    function pf(n) {
        return n.wrapS !== THREE.ClampToEdgeWrapping || n.wrapT !== THREE.ClampToEdgeWrapping ? !0 : n.minFilter !== THREE.NearestFilter && n.minFilter !== THREE.LinearFilter ? !0 : !1
    }

    function wf(n) {
        var t, i;
        return n instanceof HTMLImageElement || n instanceof HTMLCanvasElement ? (t = document.createElement("canvas"), t.width = THREE.Math.nearestPowerOfTwo(n.width), t.height = THREE.Math.nearestPowerOfTwo(n.height), i = t.getContext("2d"), i.drawImage(n, 0, 0, t.width, t.height), console.warn("THREE.WebGLRenderer: image is not power of two (" + n.width + "x" + n.height + "). Resized to " + t.width + "x" + t.height, n), t) : n
    }

    function bf(n, r) {
        var l = u.get(n),
            f, c, w, v, g;
        if (n.image.length === 6)
            if (n.version > 0 && l.__version !== n.version) {
                l.__image__webglTextureCube || (n.addEventListener("dispose", yi), l.__image__webglTextureCube = t.createTexture(), k.textures++);
                i.activeTexture(t.TEXTURE0 + r);
                i.bindTexture(t.TEXTURE_CUBE_MAP, l.__image__webglTextureCube);
                t.pixelStorei(t.UNPACK_FLIP_Y_WEBGL, n.flipY);
                var b = n instanceof THREE.CompressedTexture,
                    y = n.image[0] instanceof THREE.DataTexture,
                    h = [];
                for (f = 0; f < 6; f++) h[f] = !e.autoScaleCubemaps || b || y ? y ? n.image[f].image : n.image[f] : dr(n.image[f], a.maxCubemapSize);
                var nt = h[0],
                    d = ni(nt),
                    o = s(n.format),
                    p = s(n.type);
                for (gt(t.TEXTURE_CUBE_MAP, n, d), f = 0; f < 6; f++)
                    if (b)
                        for (w = h[f].mipmaps, v = 0, g = w.length; v < g; v++) c = w[v], n.format !== THREE.RGBAFormat && n.format !== THREE.RGBFormat ? i.getCompressedTextureFormats().indexOf(o) > -1 ? i.compressedTexImage2D(t.TEXTURE_CUBE_MAP_POSITIVE_X + f, v, o, c.width, c.height, 0, c.data) : console.warn("THREE.WebGLRenderer: Attempt to load unsupported compressed texture format in .setCubeTexture()") : i.texImage2D(t.TEXTURE_CUBE_MAP_POSITIVE_X + f, v, o, c.width, c.height, 0, o, p, c.data);
                    else y ? i.texImage2D(t.TEXTURE_CUBE_MAP_POSITIVE_X + f, 0, o, h[f].width, h[f].height, 0, o, p, h[f].data) : i.texImage2D(t.TEXTURE_CUBE_MAP_POSITIVE_X + f, 0, o, o, p, h[f]);
                if (n.generateMipmaps && d && t.generateMipmap(t.TEXTURE_CUBE_MAP), l.__version = n.version, n.onUpdate) n.onUpdate(n)
            } else i.activeTexture(t.TEXTURE0 + r), i.bindTexture(t.TEXTURE_CUBE_MAP, l.__image__webglTextureCube)
    }

    function kf(n, r) {
        i.activeTexture(t.TEXTURE0 + r);
        i.bindTexture(t.TEXTURE_CUBE_MAP, u.get(n).__webglTexture)
    }

    function gr(n, r, f, e) {
        var o = s(r.texture.format),
            h = s(r.texture.type);
        i.texImage2D(e, 0, o, r.width, r.height, 0, o, h, null);
        t.bindFramebuffer(t.FRAMEBUFFER, n);
        t.framebufferTexture2D(t.FRAMEBUFFER, f, e, u.get(r.texture).__webglTexture, 0);
        t.bindFramebuffer(t.FRAMEBUFFER, null)
    }

    function nu(n, i) {
        t.bindRenderbuffer(t.RENDERBUFFER, n);
        i.depthBuffer && !i.stencilBuffer ? (t.renderbufferStorage(t.RENDERBUFFER, t.DEPTH_COMPONENT16, i.width, i.height), t.framebufferRenderbuffer(t.FRAMEBUFFER, t.DEPTH_ATTACHMENT, t.RENDERBUFFER, n)) : i.depthBuffer && i.stencilBuffer ? (t.renderbufferStorage(t.RENDERBUFFER, t.DEPTH_STENCIL, i.width, i.height), t.framebufferRenderbuffer(t.FRAMEBUFFER, t.DEPTH_STENCIL_ATTACHMENT, t.RENDERBUFFER, n)) : t.renderbufferStorage(t.RENDERBUFFER, t.RGBA4, i.width, i.height);
        t.bindRenderbuffer(t.RENDERBUFFER, null)
    }

    function df(n, i) {
        var f = i instanceof THREE.WebGLRenderTargetCube,
            r;
        if (f) throw new Error("Depth Texture with cube render targets is not supported!");
        if (t.bindFramebuffer(t.FRAMEBUFFER, n), !(i.depthTexture instanceof THREE.DepthTexture)) throw new Error("renderTarget.depthTexture must be an instance of THREE.DepthTexture");
        u.get(i.depthTexture).__webglTexture && i.depthTexture.image.width === i.width && i.depthTexture.image.height === i.height || (i.depthTexture.image.width = i.width, i.depthTexture.image.height = i.height, i.depthTexture.needsUpdate = !0);
        e.setTexture(i.depthTexture, 0);
        r = u.get(i.depthTexture).__webglTexture;
        t.framebufferTexture2D(t.FRAMEBUFFER, t.DEPTH_ATTACHMENT, t.TEXTURE_2D, r, 0)
    }

    function gf(n) {
        var i = u.get(n),
            f = n instanceof THREE.WebGLRenderTargetCube,
            r;
        if (n.depthTexture) {
            if (f) throw new Error("target.depthTexture not supported in Cube render targets");
            df(i.__webglFramebuffer, n)
        } else if (f)
            for (i.__webglDepthbuffer = [], r = 0; r < 6; r++) t.bindFramebuffer(t.FRAMEBUFFER, i.__webglFramebuffer[r]), i.__webglDepthbuffer[r] = t.createRenderbuffer(), nu(i.__webglDepthbuffer[r], n);
        else t.bindFramebuffer(t.FRAMEBUFFER, i.__webglFramebuffer), i.__webglDepthbuffer = t.createRenderbuffer(), nu(i.__webglDepthbuffer, n);
        t.bindFramebuffer(t.FRAMEBUFFER, null)
    }

    function ne(n) {
        var f = u.get(n),
            o = u.get(n.texture),
            s, e, r;
        if (n.addEventListener("dispose", sr), o.__webglTexture = t.createTexture(), k.textures++, s = n instanceof THREE.WebGLRenderTargetCube, e = THREE.Math.isPowerOfTwo(n.width) && THREE.Math.isPowerOfTwo(n.height), s)
            for (f.__webglFramebuffer = [], r = 0; r < 6; r++) f.__webglFramebuffer[r] = t.createFramebuffer();
        else f.__webglFramebuffer = t.createFramebuffer();
        if (s) {
            for (i.bindTexture(t.TEXTURE_CUBE_MAP, o.__webglTexture), gt(t.TEXTURE_CUBE_MAP, n.texture, e), r = 0; r < 6; r++) gr(f.__webglFramebuffer[r], n, t.COLOR_ATTACHMENT0, t.TEXTURE_CUBE_MAP_POSITIVE_X + r);
            n.texture.generateMipmaps && e && t.generateMipmap(t.TEXTURE_CUBE_MAP);
            i.bindTexture(t.TEXTURE_CUBE_MAP, null)
        } else i.bindTexture(t.TEXTURE_2D, o.__webglTexture), gt(t.TEXTURE_2D, n.texture, e), gr(f.__webglFramebuffer, n, t.COLOR_ATTACHMENT0, t.TEXTURE_2D), n.texture.generateMipmaps && e && t.generateMipmap(t.TEXTURE_2D), i.bindTexture(t.TEXTURE_2D, null);
        n.depthBuffer && gf(n)
    }

    function te(n) {
        var r = n instanceof THREE.WebGLRenderTargetCube ? t.TEXTURE_CUBE_MAP : t.TEXTURE_2D,
            f = u.get(n.texture).__webglTexture;
        i.bindTexture(r, f);
        t.generateMipmap(r);
        i.bindTexture(r, null)
    }

    function tu(n) {
        return n === THREE.NearestFilter || n === THREE.NearestMipMapNearestFilter || n === THREE.NearestMipMapLinearFilter ? t.NEAREST : t.LINEAR
    }

    function s(n) {
        var i;
        if (n === THREE.RepeatWrapping) return t.REPEAT;
        if (n === THREE.ClampToEdgeWrapping) return t.CLAMP_TO_EDGE;
        if (n === THREE.MirroredRepeatWrapping) return t.MIRRORED_REPEAT;
        if (n === THREE.NearestFilter) return t.NEAREST;
        if (n === THREE.NearestMipMapNearestFilter) return t.NEAREST_MIPMAP_NEAREST;
        if (n === THREE.NearestMipMapLinearFilter) return t.NEAREST_MIPMAP_LINEAR;
        if (n === THREE.LinearFilter) return t.LINEAR;
        if (n === THREE.LinearMipMapNearestFilter) return t.LINEAR_MIPMAP_NEAREST;
        if (n === THREE.LinearMipMapLinearFilter) return t.LINEAR_MIPMAP_LINEAR;
        if (n === THREE.UnsignedByteType) return t.UNSIGNED_BYTE;
        if (n === THREE.UnsignedShort4444Type) return t.UNSIGNED_SHORT_4_4_4_4;
        if (n === THREE.UnsignedShort5551Type) return t.UNSIGNED_SHORT_5_5_5_1;
        if (n === THREE.UnsignedShort565Type) return t.UNSIGNED_SHORT_5_6_5;
        if (n === THREE.ByteType) return t.BYTE;
        if (n === THREE.ShortType) return t.SHORT;
        if (n === THREE.UnsignedShortType) return t.UNSIGNED_SHORT;
        if (n === THREE.IntType) return t.INT;
        if (n === THREE.UnsignedIntType) return t.UNSIGNED_INT;
        if (n === THREE.FloatType) return t.FLOAT;
        if (i = f.get("OES_texture_half_float"), i !== null && n === THREE.HalfFloatType) return i.HALF_FLOAT_OES;
        if (n === THREE.AlphaFormat) return t.ALPHA;
        if (n === THREE.RGBFormat) return t.RGB;
        if (n === THREE.RGBAFormat) return t.RGBA;
        if (n === THREE.LuminanceFormat) return t.LUMINANCE;
        if (n === THREE.LuminanceAlphaFormat) return t.LUMINANCE_ALPHA;
        if (n === THREE.DepthFormat) return t.DEPTH_COMPONENT;
        if (n === THREE.AddEquation) return t.FUNC_ADD;
        if (n === THREE.SubtractEquation) return t.FUNC_SUBTRACT;
        if (n === THREE.ReverseSubtractEquation) return t.FUNC_REVERSE_SUBTRACT;
        if (n === THREE.ZeroFactor) return t.ZERO;
        if (n === THREE.OneFactor) return t.ONE;
        if (n === THREE.SrcColorFactor) return t.SRC_COLOR;
        if (n === THREE.OneMinusSrcColorFactor) return t.ONE_MINUS_SRC_COLOR;
        if (n === THREE.SrcAlphaFactor) return t.SRC_ALPHA;
        if (n === THREE.OneMinusSrcAlphaFactor) return t.ONE_MINUS_SRC_ALPHA;
        if (n === THREE.DstAlphaFactor) return t.DST_ALPHA;
        if (n === THREE.OneMinusDstAlphaFactor) return t.ONE_MINUS_DST_ALPHA;
        if (n === THREE.DstColorFactor) return t.DST_COLOR;
        if (n === THREE.OneMinusDstColorFactor) return t.ONE_MINUS_DST_COLOR;
        if (n === THREE.SrcAlphaSaturateFactor) return t.SRC_ALPHA_SATURATE;
        if (i = f.get("WEBGL_compressed_texture_s3tc"), i !== null) {
            if (n === THREE.RGB_S3TC_DXT1_Format) return i.COMPRESSED_RGB_S3TC_DXT1_EXT;
            if (n === THREE.RGBA_S3TC_DXT1_Format) return i.COMPRESSED_RGBA_S3TC_DXT1_EXT;
            if (n === THREE.RGBA_S3TC_DXT3_Format) return i.COMPRESSED_RGBA_S3TC_DXT3_EXT;
            if (n === THREE.RGBA_S3TC_DXT5_Format) return i.COMPRESSED_RGBA_S3TC_DXT5_EXT
        }
        if (i = f.get("WEBGL_compressed_texture_pvrtc"), i !== null) {
            if (n === THREE.RGB_PVRTC_4BPPV1_Format) return i.COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
            if (n === THREE.RGB_PVRTC_2BPPV1_Format) return i.COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
            if (n === THREE.RGBA_PVRTC_4BPPV1_Format) return i.COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;
            if (n === THREE.RGBA_PVRTC_2BPPV1_Format) return i.COMPRESSED_RGBA_PVRTC_2BPPV1_IMG
        }
        if (i = f.get("WEBGL_compressed_texture_etc1"), i !== null && n === THREE.RGB_ETC1_Format) return i.COMPRESSED_RGB_ETC1_WEBGL;
        if (i = f.get("EXT_blend_minmax"), i !== null) {
            if (n === THREE.MinEquation) return i.MIN_EXT;
            if (n === THREE.MaxEquation) return i.MAX_EXT
        }
        return 0
    }
    var t, ci, li, f, nr, tr, vi, fr, er, wi;
    console.log("THREE.WebGLRenderer", THREE.REVISION);
    n = n || {};
    var h = n.canvas !== undefined ? n.canvas : document.createElement("canvas"),
        iu = n.context !== undefined ? n.context : null,
        ru = n.alpha !== undefined ? n.alpha : !1,
        uu = n.depth !== undefined ? n.depth : !0,
        fu = n.stencil !== undefined ? n.stencil : !0,
        eu = n.antialias !== undefined ? n.antialias : !1,
        bi = n.premultipliedAlpha !== undefined ? n.premultipliedAlpha : !0,
        ou = n.preserveDrawingBuffer !== undefined ? n.preserveDrawingBuffer : !1,
        st = [],
        d = [],
        ti = -1,
        g = [],
        ii = -1,
        ki = new Float32Array(8),
        ri = [],
        ui = [];
    this.domElement = h;
    this.context = null;
    this.autoClear = !0;
    this.autoClearColor = !0;
    this.autoClearDepth = !0;
    this.autoClearStencil = !0;
    this.sortObjects = !0;
    this.clippingPlanes = [];
    this.localClippingEnabled = !1;
    this.gammaFactor = 2;
    this.gammaInput = !1;
    this.gammaOutput = !1;
    this.physicallyCorrectLights = !1;
    this.toneMapping = THREE.LinearToneMapping;
    this.toneMappingExposure = 1;
    this.toneMappingWhitePoint = 1;
    this.maxMorphTargets = 8;
    this.maxMorphNormals = 4;
    this.autoScaleCubemaps = !0;
    var e = this,
        fi = null,
        ei = null,
        ht = null,
        nt = -1,
        tt = "",
        p = null,
        ct = new THREE.Vector4,
        oi = null,
        it = new THREE.Vector4,
        si = 0,
        c = new THREE.Color(0),
        b = 0,
        lt = h.width,
        at = h.height,
        l = 1,
        hi = new THREE.Vector4(0, 0, lt, at),
        di = !1,
        rt = new THREE.Vector4(0, 0, lt, at),
        gi = new THREE.Frustum,
        vt = !1,
        yt = !1,
        ut = !1,
        v = 0,
        y = {
            type: "4fv",
            value: null,
            needsUpdate: !1
        },
        pt = null,
        ft = 0,
        su = new THREE.Matrix3,
        hu = new THREE.Sphere,
        cu = new THREE.Plane,
        wt = new THREE.Matrix4,
        o = new THREE.Vector3,
        r = {
            hash: "",
            ambient: [0, 0, 0],
            directional: [],
            directionalShadowMap: [],
            directionalShadowMatrix: [],
            spot: [],
            spotShadowMap: [],
            spotShadowMatrix: [],
            point: [],
            pointShadowMap: [],
            pointShadowMatrix: [],
            hemi: [],
            shadows: []
        },
        k = {
            geometries: 0,
            textures: 0
        },
        w = {
            calls: 0,
            vertices: 0,
            faces: 0,
            points: 0
        };
    this.info = {
        render: w,
        memory: k,
        programs: null
    };
    try {
        if (ci = {
                alpha: ru,
                depth: uu,
                stencil: fu,
                antialias: eu,
                premultipliedAlpha: bi,
                preserveDrawingBuffer: ou
            }, t = iu || h.getContext("webgl", ci) || h.getContext("experimental-webgl", ci), t === null)
            if (h.getContext("webgl") !== null) throw "Error creating WebGL context with your selected attributes.";
            else throw "Error creating WebGL context.";
        t.getShaderPrecisionFormat === undefined && (t.getShaderPrecisionFormat = function() {
            return {
                rangeMin: 1,
                rangeMax: 1,
                precision: 1
            }
        });
        h.addEventListener("webglcontextlost", or, !1)
    } catch (lu) {
        console.error("THREE.WebGLRenderer: " + lu)
    }
    li = typeof WebGL2RenderingContext != "undefined" && t instanceof WebGL2RenderingContext;
    f = new THREE.WebGLExtensions(t);
    f.get("WEBGL_depth_texture");
    f.get("OES_texture_float");
    f.get("OES_texture_float_linear");
    f.get("OES_texture_half_float");
    f.get("OES_texture_half_float_linear");
    f.get("OES_standard_derivatives");
    f.get("ANGLE_instanced_arrays");
    f.get("OES_element_index_uint") && (THREE.BufferGeometry.MaxIndex = 4294967296);
    var a = new THREE.WebGLCapabilities(t, f, n),
        i = new THREE.WebGLState(t, f, s),
        u = new THREE.WebGLProperties,
        et = new THREE.WebGLObjects(t, u, this.info),
        ot = new THREE.WebGLPrograms(this, a),
        bt = new THREE.WebGLLights;
    this.info.programs = ot.programs;
    nr = new THREE.WebGLBufferRenderer(t, f, w);
    tr = new THREE.WebGLIndexedBufferRenderer(t, f, w);
    rr();
    this.context = t;
    this.capabilities = a;
    this.extensions = f;
    this.properties = u;
    this.state = i;
    vi = new THREE.WebGLShadowMap(this, r, et);
    this.shadowMap = vi;
    fr = new THREE.SpritePlugin(this, ri);
    er = new THREE.LensFlarePlugin(this, ui);
    this.getContext = function() {
        return t
    };
    this.getContextAttributes = function() {
        return t.getContextAttributes()
    };
    this.forceContextLoss = function() {
        f.get("WEBGL_lose_context").loseContext()
    };
    this.getMaxAnisotropy = function() {
        var n;
        return function() {
            if (n !== undefined) return n;
            var i = f.get("EXT_texture_filter_anisotropic");
            return n = i !== null ? t.getParameter(i.MAX_TEXTURE_MAX_ANISOTROPY_EXT) : 0
        }
    }();
    this.getPrecision = function() {
        return a.precision
    };
    this.getPixelRatio = function() {
        return l
    };
    this.setPixelRatio = function(n) {
        n !== undefined && (l = n, this.setSize(rt.z, rt.w, !1))
    };
    this.getSize = function() {
        return {
            width: lt,
            height: at
        }
    };
    this.setSize = function(n, t, i) {
        lt = n;
        at = t;
        h.width = n * l;
        h.height = t * l;
        i !== !1 && (h.style.width = n + "px", h.style.height = t + "px");
        this.setViewport(0, 0, n, t)
    };
    this.setViewport = function(n, t, r, u) {
        i.viewport(rt.set(n, t, r, u))
    };
    this.setScissor = function(n, t, r, u) {
        i.scissor(hi.set(n, t, r, u))
    };
    this.setScissorTest = function(n) {
        i.setScissorTest(di = n)
    };
    this.getClearColor = function() {
        return c
    };
    this.setClearColor = function(n, t) {
        c.set(n);
        b = t !== undefined ? t : 1;
        ai(c.r, c.g, c.b, b)
    };
    this.getClearAlpha = function() {
        return b
    };
    this.setClearAlpha = function(n) {
        b = n;
        ai(c.r, c.g, c.b, b)
    };
    this.clear = function(n, i, r) {
        var u = 0;
        (n === undefined || n) && (u |= t.COLOR_BUFFER_BIT);
        (i === undefined || i) && (u |= t.DEPTH_BUFFER_BIT);
        (r === undefined || r) && (u |= t.STENCIL_BUFFER_BIT);
        t.clear(u)
    };
    this.clearColor = function() {
        this.clear(!0, !1, !1)
    };
    this.clearDepth = function() {
        this.clear(!1, !0, !1)
    };
    this.clearStencil = function() {
        this.clear(!1, !1, !0)
    };
    this.clearTarget = function(n, t, i, r) {
        this.setRenderTarget(n);
        this.clear(t, i, r)
    };
    this.resetGLState = ur;
    this.dispose = function() {
        h.removeEventListener("webglcontextlost", or, !1)
    };
    this.renderBufferImmediate = function(n, r, f) {
        var s, h, e, v;
        if (i.initAttributes(), s = u.get(n), n.hasPositions && !s.position && (s.position = t.createBuffer()), n.hasNormals && !s.normal && (s.normal = t.createBuffer()), n.hasUvs && !s.uv && (s.uv = t.createBuffer()), n.hasColors && !s.color && (s.color = t.createBuffer()), h = r.getAttributes(), n.hasPositions && (t.bindBuffer(t.ARRAY_BUFFER, s.position), t.bufferData(t.ARRAY_BUFFER, n.positionArray, t.DYNAMIC_DRAW), i.enableAttribute(h.position), t.vertexAttribPointer(h.position, 3, t.FLOAT, !1, 0, 0)), n.hasNormals) {
            if (t.bindBuffer(t.ARRAY_BUFFER, s.normal), f.type !== "MeshPhongMaterial" && f.type !== "MeshStandardMaterial" && f.type !== "MeshPhysicalMaterial" && f.shading === THREE.FlatShading)
                for (e = 0, v = n.count * 3; e < v; e += 9) {
                    var o = n.normalArray,
                        c = (o[e + 0] + o[e + 3] + o[e + 6]) / 3,
                        l = (o[e + 1] + o[e + 4] + o[e + 7]) / 3,
                        a = (o[e + 2] + o[e + 5] + o[e + 8]) / 3;
                    o[e + 0] = c;
                    o[e + 1] = l;
                    o[e + 2] = a;
                    o[e + 3] = c;
                    o[e + 4] = l;
                    o[e + 5] = a;
                    o[e + 6] = c;
                    o[e + 7] = l;
                    o[e + 8] = a
                }
            t.bufferData(t.ARRAY_BUFFER, n.normalArray, t.DYNAMIC_DRAW);
            i.enableAttribute(h.normal);
            t.vertexAttribPointer(h.normal, 3, t.FLOAT, !1, 0, 0)
        }
        n.hasUvs && f.map && (t.bindBuffer(t.ARRAY_BUFFER, s.uv), t.bufferData(t.ARRAY_BUFFER, n.uvArray, t.DYNAMIC_DRAW), i.enableAttribute(h.uv), t.vertexAttribPointer(h.uv, 2, t.FLOAT, !1, 0, 0));
        n.hasColors && f.vertexColors !== THREE.NoColors && (t.bindBuffer(t.ARRAY_BUFFER, s.color), t.bufferData(t.ARRAY_BUFFER, n.colorArray, t.DYNAMIC_DRAW), i.enableAttribute(h.color), t.vertexAttribPointer(h.color, 3, t.FLOAT, !1, 0, 0));
        i.disableUnusedAttributes();
        t.drawArrays(t.TRIANGLES, 0, n.count);
        n.count = 0
    };
    this.renderBufferDirect = function(n, r, u, f, e, o) {
        var p, l, y, s, v, a, h, g, c, nt, w, b;
        vr(f);
        var k = yr(n, r, f, e),
            d = !1,
            rt = u.id + "_" + k.id + "_" + f.wireframe;
        if (rt !== tt && (tt = rt, d = !0), p = e.morphTargetInfluences, p !== undefined) {
            for (l = [], s = 0, v = p.length; s < v; s++) a = p[s], l.push([a, s]);
            for (l.sort(wu), l.length > 8 && (l.length = 8), y = u.morphAttributes, s = 0, v = l.length; s < v; s++) a = l[s], ki[s] = a[0], a[0] !== 0 ? (h = a[1], f.morphTargets === !0 && y.position && u.addAttribute("morphTarget" + s, y.position[h]), f.morphNormals === !0 && y.normal && u.addAttribute("morphNormal" + s, y.normal[h])) : (f.morphTargets === !0 && u.removeAttribute("morphTarget" + s), f.morphNormals === !0 && u.removeAttribute("morphNormal" + s));
            k.getUniforms().setValue(t, "morphTargetInfluences", ki);
            d = !0
        }
        h = u.index;
        g = u.attributes.position;
        f.wireframe === !0 && (h = et.getWireframeAttribute(u));
        h !== null ? (c = tr, c.setIndex(h)) : c = nr;
        d && (pu(f, k, u), h !== null && t.bindBuffer(t.ELEMENT_ARRAY_BUFFER, et.getAttributeBuffer(h)));
        nt = 0;
        w = Infinity;
        h !== null ? w = h.count : g !== undefined && (w = g.count);
        var ut = u.drawRange.start,
            st = u.drawRange.count,
            ft = o !== null ? o.start : 0,
            ht = o !== null ? o.count : Infinity,
            it = Math.max(nt, ut, ft),
            ct = Math.min(nt + w, ut + st, ft + ht) - 1,
            ot = Math.max(0, ct - it + 1);
        if (e instanceof THREE.Mesh)
            if (f.wireframe === !0) i.setLineWidth(f.wireframeLinewidth * ir()), c.setMode(t.LINES);
            else switch (e.drawMode) {
                case THREE.TrianglesDrawMode:
                    c.setMode(t.TRIANGLES);
                    break;
                case THREE.TriangleStripDrawMode:
                    c.setMode(t.TRIANGLE_STRIP);
                    break;
                case THREE.TriangleFanDrawMode:
                    c.setMode(t.TRIANGLE_FAN)
            } else e instanceof THREE.Line ? (b = f.linewidth, b === undefined && (b = 1), i.setLineWidth(b * ir()), e instanceof THREE.LineSegments ? c.setMode(t.LINES) : c.setMode(t.LINE_STRIP)) : e instanceof THREE.Points && c.setMode(t.POINTS);
        u instanceof THREE.InstancedBufferGeometry ? u.maxInstancedCount > 0 && c.renderInstances(u, it, ot) : c.render(it, ot)
    };
    this.render = function(n, t, r, u) {
        var f, s, o;
        if (t instanceof THREE.Camera == !1) {
            console.error("THREE.WebGLRenderer.render: camera is not an instance of THREE.Camera.");
            return
        }
        f = n.fog;
        tt = "";
        nt = -1;
        p = null;
        n.autoUpdate === !0 && n.updateMatrixWorld();
        t.parent === null && t.updateMatrixWorld();
        t.matrixWorldInverse.getInverse(t.matrixWorld);
        wt.multiplyMatrices(t.projectionMatrix, t.matrixWorldInverse);
        gi.setFromMatrix(wt);
        st.length = 0;
        ti = -1;
        ii = -1;
        ri.length = 0;
        ui.length = 0;
        lf(this.clippingPlanes, t);
        ar(n, t);
        d.length = ti + 1;
        g.length = ii + 1;
        e.sortObjects === !0 && (d.sort(bu), g.sort(ku));
        vt && (ut = !0, dt(null));
        hf(st);
        vi.render(n, t);
        cf(st, t);
        vt && (ut = !1, br());
        w.calls = 0;
        w.vertices = 0;
        w.faces = 0;
        w.points = 0;
        r === undefined && (r = null);
        this.setRenderTarget(r);
        (this.autoClear || u) && this.clear(this.autoClearColor, this.autoClearDepth, this.autoClearStencil);
        n.overrideMaterial ? (s = n.overrideMaterial, kt(d, t, f, s), kt(g, t, f, s)) : (i.setBlending(THREE.NoBlending), kt(d, t, f), kt(g, t, f));
        fr.render(n, t);
        er.render(n, t, it);
        r && (o = r.texture, o.generateMipmaps && ni(r) && o.minFilter !== THREE.NearestFilter && o.minFilter !== THREE.LinearFilter && te(r));
        i.setDepthTest(!0);
        i.setDepthWrite(!0);
        i.setColorWrite(!0)
    };
    this.setFaceCulling = function(n, r) {
        n === THREE.CullFaceNone ? i.disable(t.CULL_FACE) : (r === THREE.FrontFaceDirectionCW ? t.frontFace(t.CW) : t.frontFace(t.CCW), n === THREE.CullFaceBack ? t.cullFace(t.BACK) : n === THREE.CullFaceFront ? t.cullFace(t.FRONT) : t.cullFace(t.FRONT_AND_BACK), i.enable(t.CULL_FACE))
    };
    wi = !1;
    this.setTexture = function(n, t) {
        wi || (console.warn("THREE.WebGLRenderer: .setTexture is deprecated, use setTexture2D instead."), wi = !0);
        kr(n, t)
    };
    this.allocTextureUnit = vf;
    this.setTexture2D = kr;
    this.setTextureCube = function(n, t) {
        n instanceof THREE.CubeTexture || Array.isArray(n.image) && n.image.length === 6 ? bf(n, t) : kf(n.texture, t)
    };
    this.getCurrentRenderTarget = function() {
        return ei
    };
    this.setRenderTarget = function(n) {
        var f, r, e, o;
        ei = n;
        n && u.get(n).__webglFramebuffer === undefined && ne(n);
        f = n instanceof THREE.WebGLRenderTargetCube;
        n ? (e = u.get(n), r = f ? e.__webglFramebuffer[n.activeCubeFace] : e.__webglFramebuffer, ct.copy(n.scissor), oi = n.scissorTest, it.copy(n.viewport)) : (r = null, ct.copy(hi).multiplyScalar(l), oi = di, it.copy(rt).multiplyScalar(l));
        ht !== r && (t.bindFramebuffer(t.FRAMEBUFFER, r), ht = r);
        i.scissor(ct);
        i.setScissorTest(oi);
        i.viewport(it);
        f && (o = u.get(n.texture), t.framebufferTexture2D(t.FRAMEBUFFER, t.COLOR_ATTACHMENT0, t.TEXTURE_CUBE_MAP_POSITIVE_X + n.activeCubeFace, o.__webglTexture, n.activeMipMapLevel))
    };
    this.readRenderTargetPixels = function(n, i, r, e, o, h) {
        var l, a, c;
        if (n instanceof THREE.WebGLRenderTarget == !1) {
            console.error("THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not THREE.WebGLRenderTarget.");
            return
        }
        if (l = u.get(n).__webglFramebuffer, l) {
            a = !1;
            l !== ht && (t.bindFramebuffer(t.FRAMEBUFFER, l), a = !0);
            try {
                if (c = n.texture, c.format !== THREE.RGBAFormat && s(c.format) !== t.getParameter(t.IMPLEMENTATION_COLOR_READ_FORMAT)) {
                    console.error("THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not in RGBA or implementation defined format.");
                    return
                }
                if (c.type !== THREE.UnsignedByteType && s(c.type) !== t.getParameter(t.IMPLEMENTATION_COLOR_READ_TYPE) && !(c.type === THREE.FloatType && f.get("WEBGL_color_buffer_float")) && !(c.type === THREE.HalfFloatType && f.get("EXT_color_buffer_half_float"))) {
                    console.error("THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not in UnsignedByteType or implementation defined type.");
                    return
                }
                t.checkFramebufferStatus(t.FRAMEBUFFER) === t.FRAMEBUFFER_COMPLETE ? i > 0 && i <= n.width - e && r > 0 && r <= n.height - o && t.readPixels(i, r, e, o, s(c.format), s(c.type), h) : console.error("THREE.WebGLRenderer.readRenderTargetPixels: readPixels from renderTarget failed. Framebuffer not complete.")
            } finally {
                a && t.bindFramebuffer(t.FRAMEBUFFER, ht)
            }
        }
    }
};
THREE.WebGLRenderTarget = function(n, t, i) {
    this.uuid = THREE.Math.generateUUID();
    this.width = n;
    this.height = t;
    this.scissor = new THREE.Vector4(0, 0, n, t);
    this.scissorTest = !1;
    this.viewport = new THREE.Vector4(0, 0, n, t);
    i = i || {};
    i.minFilter === undefined && (i.minFilter = THREE.LinearFilter);
    this.texture = new THREE.Texture(undefined, undefined, i.wrapS, i.wrapT, i.magFilter, i.minFilter, i.format, i.type, i.anisotropy, i.encoding);
    this.depthBuffer = i.depthBuffer !== undefined ? i.depthBuffer : !0;
    this.stencilBuffer = i.stencilBuffer !== undefined ? i.stencilBuffer : !0;
    this.depthTexture = null
};
THREE.WebGLRenderTarget.prototype = {
    constructor: THREE.WebGLRenderTarget,
    setSize: function(n, t) {
        (this.width !== n || this.height !== t) && (this.width = n, this.height = t, this.dispose());
        this.viewport.set(0, 0, n, t);
        this.scissor.set(0, 0, n, t)
    },
    clone: function() {
        return (new this.constructor).copy(this)
    },
    copy: function(n) {
        return this.width = n.width, this.height = n.height, this.viewport.copy(n.viewport), this.texture = n.texture.clone(), this.depthBuffer = n.depthBuffer, this.stencilBuffer = n.stencilBuffer, this.depthTexture = n.depthTexture, this
    },
    dispose: function() {
        this.dispatchEvent({
            type: "dispose"
        })
    }
};
THREE.EventDispatcher.prototype.apply(THREE.WebGLRenderTarget.prototype);
THREE.WebGLRenderTargetCube = function(n, t, i) {
    THREE.WebGLRenderTarget.call(this, n, t, i);
    this.activeCubeFace = 0;
    this.activeMipMapLevel = 0
};
THREE.WebGLRenderTargetCube.prototype = Object.create(THREE.WebGLRenderTarget.prototype);
THREE.WebGLRenderTargetCube.prototype.constructor = THREE.WebGLRenderTargetCube;
THREE.WebGLBufferRenderer = function(n, t, i) {
    function u(n) {
        r = n
    }

    function f(t, u) {
        n.drawArrays(r, t, u);
        i.calls++;
        i.vertices += u;
        r === n.TRIANGLES && (i.faces += u / 3)
    }

    function e(u) {
        var o = t.get("ANGLE_instanced_arrays"),
            e, f;
        if (o === null) {
            console.error("THREE.WebGLBufferRenderer: using THREE.InstancedBufferGeometry but hardware does not support extension ANGLE_instanced_arrays.");
            return
        }
        e = u.attributes.position;
        f = 0;
        e instanceof THREE.InterleavedBufferAttribute ? (f = e.data.count, o.drawArraysInstancedANGLE(r, 0, f, u.maxInstancedCount)) : (f = e.count, o.drawArraysInstancedANGLE(r, 0, f, u.maxInstancedCount));
        i.calls++;
        i.vertices += f * u.maxInstancedCount;
        r === n.TRIANGLES && (i.faces += u.maxInstancedCount * f / 3)
    }
    var r;
    this.setMode = u;
    this.render = f;
    this.renderInstances = e
};
THREE.WebGLIndexedBufferRenderer = function(n, t, i) {
    function e(n) {
        r = n
    }

    function o(i) {
        i.array instanceof Uint32Array && t.get("OES_element_index_uint") ? (u = n.UNSIGNED_INT, f = 4) : (u = n.UNSIGNED_SHORT, f = 2)
    }

    function s(t, e) {
        n.drawElements(r, e, u, t * f);
        i.calls++;
        i.vertices += e;
        r === n.TRIANGLES && (i.faces += e / 3)
    }

    function h(e, o, s) {
        var h = t.get("ANGLE_instanced_arrays");
        if (h === null) {
            console.error("THREE.WebGLBufferRenderer: using THREE.InstancedBufferGeometry but hardware does not support extension ANGLE_instanced_arrays.");
            return
        }
        h.drawElementsInstancedANGLE(r, s, u, o * f, e.maxInstancedCount);
        i.calls++;
        i.vertices += s * e.maxInstancedCount;
        r === n.TRIANGLES && (i.faces += e.maxInstancedCount * s / 3)
    }
    var r, u, f;
    this.setMode = e;
    this.setIndex = o;
    this.render = s;
    this.renderInstances = h
};
THREE.WebGLExtensions = function(n) {
    var t = {};
    this.get = function(i) {
        if (t[i] !== undefined) return t[i];
        var r;
        switch (i) {
            case "WEBGL_depth_texture":
                r = n.getExtension("WEBGL_depth_texture") || n.getExtension("MOZ_WEBGL_depth_texture") || n.getExtension("WEBKIT_WEBGL_depth_texture");
            case "EXT_texture_filter_anisotropic":
                r = n.getExtension("EXT_texture_filter_anisotropic") || n.getExtension("MOZ_EXT_texture_filter_anisotropic") || n.getExtension("WEBKIT_EXT_texture_filter_anisotropic");
                break;
            case "WEBGL_compressed_texture_s3tc":
                r = n.getExtension("WEBGL_compressed_texture_s3tc") || n.getExtension("MOZ_WEBGL_compressed_texture_s3tc") || n.getExtension("WEBKIT_WEBGL_compressed_texture_s3tc");
                break;
            case "WEBGL_compressed_texture_pvrtc":
                r = n.getExtension("WEBGL_compressed_texture_pvrtc") || n.getExtension("WEBKIT_WEBGL_compressed_texture_pvrtc");
                break;
            case "WEBGL_compressed_texture_etc1":
                r = n.getExtension("WEBGL_compressed_texture_etc1");
                break;
            default:
                r = n.getExtension(i)
        }
        return r === null && console.warn("THREE.WebGLRenderer: " + i + " extension not supported."), t[i] = r, r
    }
};
THREE.WebGLCapabilities = function(n, t, i) {
    function u(t) {
        if (t === "highp") {
            if (n.getShaderPrecisionFormat(n.VERTEX_SHADER, n.HIGH_FLOAT).precision > 0 && n.getShaderPrecisionFormat(n.FRAGMENT_SHADER, n.HIGH_FLOAT).precision > 0) return "highp";
            t = "mediump"
        }
        return t === "mediump" && n.getShaderPrecisionFormat(n.VERTEX_SHADER, n.MEDIUM_FLOAT).precision > 0 && n.getShaderPrecisionFormat(n.FRAGMENT_SHADER, n.MEDIUM_FLOAT).precision > 0 ? "mediump" : "lowp"
    }
    this.getMaxPrecision = u;
    this.precision = i.precision !== undefined ? i.precision : "highp";
    this.logarithmicDepthBuffer = i.logarithmicDepthBuffer !== undefined ? i.logarithmicDepthBuffer : !1;
    this.maxTextures = n.getParameter(n.MAX_TEXTURE_IMAGE_UNITS);
    this.maxVertexTextures = n.getParameter(n.MAX_VERTEX_TEXTURE_IMAGE_UNITS);
    this.maxTextureSize = n.getParameter(n.MAX_TEXTURE_SIZE);
    this.maxCubemapSize = n.getParameter(n.MAX_CUBE_MAP_TEXTURE_SIZE);
    this.maxAttributes = n.getParameter(n.MAX_VERTEX_ATTRIBS);
    this.maxVertexUniforms = n.getParameter(n.MAX_VERTEX_UNIFORM_VECTORS);
    this.maxVaryings = n.getParameter(n.MAX_VARYING_VECTORS);
    this.maxFragmentUniforms = n.getParameter(n.MAX_FRAGMENT_UNIFORM_VECTORS);
    this.vertexTextures = this.maxVertexTextures > 0;
    this.floatFragmentTextures = !!t.get("OES_texture_float");
    this.floatVertexTextures = this.vertexTextures && this.floatFragmentTextures;
    var r = u(this.precision);
    r !== this.precision && (console.warn("THREE.WebGLRenderer:", this.precision, "not supported, using", r, "instead."), this.precision = r);
    this.logarithmicDepthBuffer && (this.logarithmicDepthBuffer = !!t.get("EXT_frag_depth"))
};
THREE.WebGLGeometries = function(n, t, i) {
    function e(n) {
        var t = n.geometry,
            u;
        return r[t.id] !== undefined ? r[t.id] : (t.addEventListener("dispose", f), t instanceof THREE.BufferGeometry ? u = t : t instanceof THREE.Geometry && (t._bufferGeometry === undefined && (t._bufferGeometry = (new THREE.BufferGeometry).setFromObject(n)), u = t._bufferGeometry), r[t.id] = u, i.memory.geometries++, u)
    }

    function f(n) {
        var e = n.target,
            o = r[e.id],
            h, c;
        o.index !== null && u(o.index);
        s(o.attributes);
        e.removeEventListener("dispose", f);
        delete r[e.id];
        h = t.get(e);
        h.wireframe && u(h.wireframe);
        t.delete(e);
        c = t.get(o);
        c.wireframe && u(c.wireframe);
        t.delete(o);
        i.memory.geometries--
    }

    function o(n) {
        return n instanceof THREE.InterleavedBufferAttribute ? t.get(n.data).__webglBuffer : t.get(n).__webglBuffer
    }

    function u(t) {
        var i = o(t);
        i !== undefined && (n.deleteBuffer(i), h(t))
    }

    function s(n) {
        for (var t in n) u(n[t])
    }

    function h(n) {
        n instanceof THREE.InterleavedBufferAttribute ? t.delete(n.data) : t.delete(n)
    }
    var r = {};
    this.get = e
};
THREE.WebGLLights = function() {
    var n = {};
    this.get = function(t) {
        if (n[t.id] !== undefined) return n[t.id];
        var i;
        switch (t.type) {
            case "DirectionalLight":
                i = {
                    direction: new THREE.Vector3,
                    color: new THREE.Color,
                    shadow: !1,
                    shadowBias: 0,
                    shadowRadius: 1,
                    shadowMapSize: new THREE.Vector2
                };
                break;
            case "SpotLight":
                i = {
                    position: new THREE.Vector3,
                    direction: new THREE.Vector3,
                    color: new THREE.Color,
                    distance: 0,
                    coneCos: 0,
                    penumbraCos: 0,
                    decay: 0,
                    shadow: !1,
                    shadowBias: 0,
                    shadowRadius: 1,
                    shadowMapSize: new THREE.Vector2
                };
                break;
            case "PointLight":
                i = {
                    position: new THREE.Vector3,
                    color: new THREE.Color,
                    distance: 0,
                    decay: 0,
                    shadow: !1,
                    shadowBias: 0,
                    shadowRadius: 1,
                    shadowMapSize: new THREE.Vector2
                };
                break;
            case "HemisphereLight":
                i = {
                    direction: new THREE.Vector3,
                    skyColor: new THREE.Color,
                    groundColor: new THREE.Color
                }
        }
        return n[t.id] = i, i
    }
};
THREE.WebGLObjects = function(n, t, i) {
    function e(t) {
        var i = f.get(t),
            o, s, h, u, c, e, l;
        t.geometry instanceof THREE.Geometry && i.updateFromObject(t);
        o = i.index;
        s = i.attributes;
        o !== null && r(o, n.ELEMENT_ARRAY_BUFFER);
        for (u in s) r(s[u], n.ARRAY_BUFFER);
        h = i.morphAttributes;
        for (u in h)
            for (c = h[u], e = 0, l = c.length; e < l; e++) r(c[e], n.ARRAY_BUFFER);
        return i
    }

    function r(n, i) {
        var r = n instanceof THREE.InterleavedBufferAttribute ? n.data : n,
            u = t.get(r);
        u.__webglBuffer === undefined ? o(u, r, i) : u.version !== r.version && s(u, r, i)
    }

    function o(t, i, r) {
        t.__webglBuffer = n.createBuffer();
        n.bindBuffer(r, t.__webglBuffer);
        var u = i.dynamic ? n.DYNAMIC_DRAW : n.STATIC_DRAW;
        n.bufferData(r, i.array, u);
        t.version = i.version
    }

    function s(t, i, r) {
        n.bindBuffer(r, t.__webglBuffer);
        i.dynamic === !1 || i.updateRange.count === -1 ? n.bufferSubData(r, 0, i.array) : i.updateRange.count === 0 ? console.error("THREE.WebGLObjects.updateBuffer: dynamic THREE.BufferAttribute marked as needsUpdate but updateRange.count is 0, ensure you are using set methods or updating manually.") : (n.bufferSubData(r, i.updateRange.offset * i.array.BYTES_PER_ELEMENT, i.array.subarray(i.updateRange.offset, i.updateRange.offset + i.updateRange.count)), i.updateRange.count = 0);
        t.version = i.version
    }

    function h(n) {
        return n instanceof THREE.InterleavedBufferAttribute ? t.get(n.data).__webglBuffer : t.get(n).__webglBuffer
    }

    function c(i) {
        var y = t.get(i),
            a, e, f, l, b, v;
        if (y.wireframe !== undefined) return y.wireframe;
        var c = [],
            p = i.index,
            w = i.attributes,
            k = w.position;
        if (p !== null)
            for (a = {}, e = p.array, f = 0, l = e.length; f < l; f += 3) {
                var o = e[f + 0],
                    s = e[f + 1],
                    h = e[f + 2];
                u(a, o, s) && c.push(o, s);
                u(a, s, h) && c.push(s, h);
                u(a, h, o) && c.push(h, o)
            } else
                for (e = w.position.array, f = 0, l = e.length / 3 - 1; f < l; f += 3) {
                    var o = f + 0,
                        s = f + 1,
                        h = f + 2;
                    c.push(o, s, s, h, h, o)
                }
        return b = k.count > 65535 ? Uint32Array : Uint16Array, v = new THREE.BufferAttribute(new b(c), 1), r(v, n.ELEMENT_ARRAY_BUFFER), y.wireframe = v, v
    }

    function u(n, t, i) {
        var u, r;
        return (t > i && (u = t, t = i, i = u), r = n[t], r === undefined) ? (n[t] = [i], !0) : r.indexOf(i) === -1 ? (r.push(i), !0) : !1
    }
    var f = new THREE.WebGLGeometries(n, t, i);
    this.getAttributeBuffer = h;
    this.getWireframeAttribute = c;
    this.update = e
};
THREE.WebGLProgram = function() {
    function r(n) {
        switch (n) {
            case THREE.LinearEncoding:
                return ["Linear", "( value )"];
            case THREE.sRGBEncoding:
                return ["sRGB", "( value )"];
            case THREE.RGBEEncoding:
                return ["RGBE", "( value )"];
            case THREE.RGBM7Encoding:
                return ["RGBM", "( value, 7.0 )"];
            case THREE.RGBM16Encoding:
                return ["RGBM", "( value, 16.0 )"];
            case THREE.RGBDEncoding:
                return ["RGBD", "( value, 256.0 )"];
            case THREE.GammaEncoding:
                return ["Gamma", "( value, float( GAMMA_FACTOR ) )"];
            default:
                throw new Error("unsupported encoding: " + n);
        }
    }

    function n(n, t) {
        var i = r(t);
        return "vec4 " + n + "( vec4 value ) { return " + i[0] + "ToLinear" + i[1] + "; }"
    }

    function o(n, t) {
        var i = r(t);
        return "vec4 " + n + "( vec4 value ) { return LinearTo" + i[0] + i[1] + "; }"
    }

    function s(n, t) {
        var i;
        switch (t) {
            case THREE.LinearToneMapping:
                i = "Linear";
                break;
            case THREE.ReinhardToneMapping:
                i = "Reinhard";
                break;
            case THREE.Uncharted2ToneMapping:
                i = "Uncharted2";
                break;
            case THREE.CineonToneMapping:
                i = "OptimizedCineon";
                break;
            default:
                throw new Error("unsupported toneMapping: " + t);
        }
        return "vec3 " + n + "( vec3 color ) { return " + i + "ToneMapping( color ); }"
    }

    function h(n, i, r) {
        n = n || {};
        var u = [n.derivatives || i.envMapCubeUV || i.bumpMap || i.normalMap || i.flatShading ? "#extension GL_OES_standard_derivatives : enable" : "", (n.fragDepth || i.logarithmicDepthBuffer) && r.get("EXT_frag_depth") ? "#extension GL_EXT_frag_depth : enable" : "", n.drawBuffers && r.get("WEBGL_draw_buffers") ? "#extension GL_EXT_draw_buffers : require" : "", (n.shaderTextureLOD || i.envMap) && r.get("EXT_shader_texture_lod") ? "#extension GL_EXT_shader_texture_lod : enable" : "", ];
        return u.filter(t).join("\n")
    }

    function c(n) {
        var r = [],
            t, i;
        for (t in n)(i = n[t], i !== !1) && r.push("#define " + t + " " + i);
        return r.join("\n")
    }

    function l(n, t) {
        for (var u = {}, e = n.getProgramParameter(t, n.ACTIVE_ATTRIBUTES), f, r, i = 0; i < e; i++) f = n.getActiveAttrib(t, i), r = f.name, u[r] = n.getAttribLocation(t, r);
        return u
    }

    function t(n) {
        return n !== ""
    }

    function u(n, t) {
        return n.replace(/NUM_DIR_LIGHTS/g, t.numDirLights).replace(/NUM_SPOT_LIGHTS/g, t.numSpotLights).replace(/NUM_POINT_LIGHTS/g, t.numPointLights).replace(/NUM_HEMI_LIGHTS/g, t.numHemiLights)
    }

    function i(n) {
        function t(n, t) {
            var r = THREE.ShaderChunk[t];
            if (r === undefined) throw new Error("Can not resolve #include <" + t + ">");
            return i(r)
        }
        return n.replace(/#include +<([\w\d.]+)>/g, t)
    }

    function f(n) {
        function t(n, t, i, r) {
            for (var f = "", u = parseInt(t); u < parseInt(i); u++) f += r.replace(/\[ i \]/g, "[ " + u + " ]");
            return f
        }
        return n.replace(/for \( int i \= (\d+)\; i < (\d+)\; i \+\+ \) \{([\s\S]+?)(?=\})\}/g, t)
    }
    var e = 0;
    return function(r, a, v, y) {
        var p = r.context,
            pt = v.extensions,
            wt = v.defines,
            b = v.__webglShader.vertexShader,
            k = v.__webglShader.fragmentShader,
            g = "SHADOWMAP_TYPE_BASIC",
            ht, ct;
        y.shadowMapType === THREE.PCFShadowMap ? g = "SHADOWMAP_TYPE_PCF" : y.shadowMapType === THREE.PCFSoftShadowMap && (g = "SHADOWMAP_TYPE_PCF_SOFT");
        var d = "ENVMAP_TYPE_CUBE",
            et = "ENVMAP_MODE_REFLECTION",
            nt = "ENVMAP_BLENDING_MULTIPLY";
        if (y.envMap) {
            switch (v.envMap.mapping) {
                case THREE.CubeReflectionMapping:
                case THREE.CubeRefractionMapping:
                    d = "ENVMAP_TYPE_CUBE";
                    break;
                case THREE.CubeUVReflectionMapping:
                case THREE.CubeUVRefractionMapping:
                    d = "ENVMAP_TYPE_CUBE_UV";
                    break;
                case THREE.EquirectangularReflectionMapping:
                case THREE.EquirectangularRefractionMapping:
                    d = "ENVMAP_TYPE_EQUIREC";
                    break;
                case THREE.SphericalReflectionMapping:
                    d = "ENVMAP_TYPE_SPHERE"
            }
            switch (v.envMap.mapping) {
                case THREE.CubeRefractionMapping:
                case THREE.EquirectangularRefractionMapping:
                    et = "ENVMAP_MODE_REFRACTION"
            }
            switch (v.combine) {
                case THREE.MultiplyOperation:
                    nt = "ENVMAP_BLENDING_MULTIPLY";
                    break;
                case THREE.MixOperation:
                    nt = "ENVMAP_BLENDING_MIX";
                    break;
                case THREE.AddOperation:
                    nt = "ENVMAP_BLENDING_ADD"
            }
        }
        var lt = r.gammaFactor > 0 ? r.gammaFactor : 1,
            bt = h(pt, y, r.extensions),
            at = c(wt),
            w = p.createProgram(),
            tt, it;
        v instanceof THREE.RawShaderMaterial ? (tt = "", it = "") : (tt = ["precision " + y.precision + " float;", "precision " + y.precision + " int;", "#define SHADER_NAME " + v.__webglShader.name, at, y.supportsVertexTextures ? "#define VERTEX_TEXTURES" : "", "#define GAMMA_FACTOR " + lt, "#define MAX_BONES " + y.maxBones, y.map ? "#define USE_MAP" : "", y.envMap ? "#define USE_ENVMAP" : "", y.envMap ? "#define " + et : "", y.lightMap ? "#define USE_LIGHTMAP" : "", y.aoMap ? "#define USE_AOMAP" : "", y.emissiveMap ? "#define USE_EMISSIVEMAP" : "", y.bumpMap ? "#define USE_BUMPMAP" : "", y.normalMap ? "#define USE_NORMALMAP" : "", y.displacementMap && y.supportsVertexTextures ? "#define USE_DISPLACEMENTMAP" : "", y.specularMap ? "#define USE_SPECULARMAP" : "", y.roughnessMap ? "#define USE_ROUGHNESSMAP" : "", y.metalnessMap ? "#define USE_METALNESSMAP" : "", y.alphaMap ? "#define USE_ALPHAMAP" : "", y.vertexColors ? "#define USE_COLOR" : "", y.flatShading ? "#define FLAT_SHADED" : "", y.skinning ? "#define USE_SKINNING" : "", y.useVertexTexture ? "#define BONE_TEXTURE" : "", y.morphTargets ? "#define USE_MORPHTARGETS" : "", y.morphNormals && y.flatShading === !1 ? "#define USE_MORPHNORMALS" : "", y.doubleSided ? "#define DOUBLE_SIDED" : "", y.flipSided ? "#define FLIP_SIDED" : "", "#define NUM_CLIPPING_PLANES " + y.numClippingPlanes, y.shadowMapEnabled ? "#define USE_SHADOWMAP" : "", y.shadowMapEnabled ? "#define " + g : "", y.sizeAttenuation ? "#define USE_SIZEATTENUATION" : "", y.logarithmicDepthBuffer ? "#define USE_LOGDEPTHBUF" : "", y.logarithmicDepthBuffer && r.extensions.get("EXT_frag_depth") ? "#define USE_LOGDEPTHBUF_EXT" : "", "uniform mat4 modelMatrix;", "uniform mat4 modelViewMatrix;", "uniform mat4 projectionMatrix;", "uniform mat4 viewMatrix;", "uniform mat3 normalMatrix;", "uniform vec3 cameraPosition;", "attribute vec3 position;", "attribute vec3 normal;", "attribute vec2 uv;", "#ifdef USE_COLOR", "\tattribute vec3 color;", "#endif", "#ifdef USE_MORPHTARGETS", "\tattribute vec3 morphTarget0;", "\tattribute vec3 morphTarget1;", "\tattribute vec3 morphTarget2;", "\tattribute vec3 morphTarget3;", "\t#ifdef USE_MORPHNORMALS", "\t\tattribute vec3 morphNormal0;", "\t\tattribute vec3 morphNormal1;", "\t\tattribute vec3 morphNormal2;", "\t\tattribute vec3 morphNormal3;", "\t#else", "\t\tattribute vec3 morphTarget4;", "\t\tattribute vec3 morphTarget5;", "\t\tattribute vec3 morphTarget6;", "\t\tattribute vec3 morphTarget7;", "\t#endif", "#endif", "#ifdef USE_SKINNING", "\tattribute vec4 skinIndex;", "\tattribute vec4 skinWeight;", "#endif", "\n"].filter(t).join("\n"), it = [bt, "precision " + y.precision + " float;", "precision " + y.precision + " int;", "#define SHADER_NAME " + v.__webglShader.name, at, y.alphaTest ? "#define ALPHATEST " + y.alphaTest : "", "#define GAMMA_FACTOR " + lt, y.useFog && y.fog ? "#define USE_FOG" : "", y.useFog && y.fogExp ? "#define FOG_EXP2" : "", y.map ? "#define USE_MAP" : "", y.envMap ? "#define USE_ENVMAP" : "", y.envMap ? "#define " + d : "", y.envMap ? "#define " + et : "", y.envMap ? "#define " + nt : "", y.lightMap ? "#define USE_LIGHTMAP" : "", y.aoMap ? "#define USE_AOMAP" : "", y.emissiveMap ? "#define USE_EMISSIVEMAP" : "", y.bumpMap ? "#define USE_BUMPMAP" : "", y.normalMap ? "#define USE_NORMALMAP" : "", y.specularMap ? "#define USE_SPECULARMAP" : "", y.roughnessMap ? "#define USE_ROUGHNESSMAP" : "", y.metalnessMap ? "#define USE_METALNESSMAP" : "", y.alphaMap ? "#define USE_ALPHAMAP" : "", y.vertexColors ? "#define USE_COLOR" : "", y.flatShading ? "#define FLAT_SHADED" : "", y.doubleSided ? "#define DOUBLE_SIDED" : "", y.flipSided ? "#define FLIP_SIDED" : "", "#define NUM_CLIPPING_PLANES " + y.numClippingPlanes, y.shadowMapEnabled ? "#define USE_SHADOWMAP" : "", y.shadowMapEnabled ? "#define " + g : "", y.premultipliedAlpha ? "#define PREMULTIPLIED_ALPHA" : "", y.physicallyCorrectLights ? "#define PHYSICALLY_CORRECT_LIGHTS" : "", y.logarithmicDepthBuffer ? "#define USE_LOGDEPTHBUF" : "", y.logarithmicDepthBuffer && r.extensions.get("EXT_frag_depth") ? "#define USE_LOGDEPTHBUF_EXT" : "", y.envMap && r.extensions.get("EXT_shader_texture_lod") ? "#define TEXTURE_LOD_EXT" : "", "uniform mat4 viewMatrix;", "uniform vec3 cameraPosition;", y.toneMapping !== THREE.NoToneMapping ? "#define TONE_MAPPING" : "", y.toneMapping !== THREE.NoToneMapping ? THREE.ShaderChunk.tonemapping_pars_fragment : "", y.toneMapping !== THREE.NoToneMapping ? s("toneMapping", y.toneMapping) : "", y.outputEncoding || y.mapEncoding || y.envMapEncoding || y.emissiveMapEncoding ? THREE.ShaderChunk.encodings_pars_fragment : "", y.mapEncoding ? n("mapTexelToLinear", y.mapEncoding) : "", y.envMapEncoding ? n("envMapTexelToLinear", y.envMapEncoding) : "", y.emissiveMapEncoding ? n("emissiveMapTexelToLinear", y.emissiveMapEncoding) : "", y.outputEncoding ? o("linearToOutputTexel", y.outputEncoding) : "", y.depthPacking ? "#define DEPTH_PACKING " + v.depthPacking : "", "\n"].filter(t).join("\n"));
        b = i(b, y);
        b = u(b, y);
        k = i(k, y);
        k = u(k, y);
        v instanceof THREE.ShaderMaterial == !1 && (b = f(b), k = f(k));
        var kt = tt + b,
            dt = it + k,
            rt = THREE.WebGLShader(p, p.VERTEX_SHADER, kt),
            ut = THREE.WebGLShader(p, p.FRAGMENT_SHADER, dt);
        p.attachShader(w, rt);
        p.attachShader(w, ut);
        v.index0AttributeName !== undefined ? p.bindAttribLocation(w, 0, v.index0AttributeName) : y.morphTargets === !0 && p.bindAttribLocation(w, 0, "position");
        p.linkProgram(w);
        var ft = p.getProgramInfoLog(w),
            ot = p.getShaderInfoLog(rt),
            st = p.getShaderInfoLog(ut),
            vt = !0,
            yt = !0;
        return p.getProgramParameter(w, p.LINK_STATUS) === !1 ? (vt = !1, console.error("THREE.WebGLProgram: shader error: ", p.getError(), "gl.VALIDATE_STATUS", p.getProgramParameter(w, p.VALIDATE_STATUS), "gl.getProgramInfoLog", ft, ot, st)) : ft !== "" ? console.warn("THREE.WebGLProgram: gl.getProgramInfoLog()", ft) : (ot === "" || st === "") && (yt = !1), yt && (this.diagnostics = {
            runnable: vt,
            material: v,
            programLog: ft,
            vertexShader: {
                log: ot,
                prefix: tt
            },
            fragmentShader: {
                log: st,
                prefix: it
            }
        }), p.deleteShader(rt), p.deleteShader(ut), this.getUniforms = function() {
            return ht === undefined && (ht = new THREE.WebGLUniforms(p, w, r)), ht
        }, this.getAttributes = function() {
            return ct === undefined && (ct = l(p, w)), ct
        }, this.destroy = function() {
            p.deleteProgram(w);
            this.program = undefined
        }, Object.defineProperties(this, {
            uniforms: {
                get: function() {
                    return console.warn("THREE.WebGLProgram: .uniforms is now .getUniforms()."), this.getUniforms()
                }
            },
            attributes: {
                get: function() {
                    return console.warn("THREE.WebGLProgram: .attributes is now .getAttributes()."), this.getAttributes()
                }
            }
        }), this.id = e++, this.code = a, this.usedTimes = 1, this.program = w, this.vertexShader = rt, this.fragmentShader = ut, this
    }
}();
THREE.WebGLPrograms = function(n, t) {
    function e(n) {
        if (t.floatVertexTextures && n && n.skeleton && n.skeleton.useVertexTexture) return 1024;
        var r = t.maxVertexUniforms,
            u = Math.floor((r - 20) / 4),
            i = u;
        return n !== undefined && n instanceof THREE.SkinnedMesh && (i = Math.min(n.skeleton.bones.length, i), i < n.skeleton.bones.length && console.warn("WebGLRenderer: too many bones - " + n.skeleton.bones.length + ", this GPU supports just " + i + " (try OpenGL instead of ANGLE)")), i
    }

    function r(n, t) {
        var i;
        return n ? n instanceof THREE.Texture ? i = n.encoding : n instanceof THREE.WebGLRenderTarget && (i = n.texture.encoding) : i = THREE.LinearEncoding, i === THREE.LinearEncoding && t && (i = THREE.GammaEncoding), i
    }
    var i = [],
        f = {
            MeshDepthMaterial: "depth",
            MeshNormalMaterial: "normal",
            MeshBasicMaterial: "basic",
            MeshLambertMaterial: "lambert",
            MeshPhongMaterial: "phong",
            MeshStandardMaterial: "physical",
            MeshPhysicalMaterial: "physical",
            LineBasicMaterial: "basic",
            LineDashedMaterial: "dashed",
            PointsMaterial: "points"
        },
        u = ["precision", "supportsVertexTextures", "map", "mapEncoding", "envMap", "envMapMode", "envMapEncoding", "lightMap", "aoMap", "emissiveMap", "emissiveMapEncoding", "bumpMap", "normalMap", "displacementMap", "specularMap", "roughnessMap", "metalnessMap", "alphaMap", "combine", "vertexColors", "fog", "useFog", "fogExp", "flatShading", "sizeAttenuation", "logarithmicDepthBuffer", "skinning", "maxBones", "useVertexTexture", "morphTargets", "morphNormals", "maxMorphTargets", "maxMorphNormals", "premultipliedAlpha", "numDirLights", "numPointLights", "numSpotLights", "numHemiLights", "shadowMapEnabled", "shadowMapType", "toneMapping", "physicallyCorrectLights", "alphaTest", "doubleSided", "flipSided", "numClippingPlanes", "depthPacking"];
    this.getParameters = function(i, u, o, s, h) {
        var l = f[i.type],
            a = e(h),
            c = n.getPrecision();
        return i.precision !== null && (c = t.getMaxPrecision(i.precision), c !== i.precision && console.warn("THREE.WebGLProgram.getParameters:", i.precision, "not supported, using", c, "instead.")), {
            shaderID: l,
            precision: c,
            supportsVertexTextures: t.vertexTextures,
            outputEncoding: r(n.getCurrentRenderTarget(), n.gammaOutput),
            map: !!i.map,
            mapEncoding: r(i.map, n.gammaInput),
            envMap: !!i.envMap,
            envMapMode: i.envMap && i.envMap.mapping,
            envMapEncoding: r(i.envMap, n.gammaInput),
            envMapCubeUV: !!i.envMap && (i.envMap.mapping === THREE.CubeUVReflectionMapping || i.envMap.mapping === THREE.CubeUVRefractionMapping),
            lightMap: !!i.lightMap,
            aoMap: !!i.aoMap,
            emissiveMap: !!i.emissiveMap,
            emissiveMapEncoding: r(i.emissiveMap, n.gammaInput),
            bumpMap: !!i.bumpMap,
            normalMap: !!i.normalMap,
            displacementMap: !!i.displacementMap,
            roughnessMap: !!i.roughnessMap,
            metalnessMap: !!i.metalnessMap,
            specularMap: !!i.specularMap,
            alphaMap: !!i.alphaMap,
            combine: i.combine,
            vertexColors: i.vertexColors,
            fog: o,
            useFog: i.fog,
            fogExp: o instanceof THREE.FogExp2,
            flatShading: i.shading === THREE.FlatShading,
            sizeAttenuation: i.sizeAttenuation,
            logarithmicDepthBuffer: t.logarithmicDepthBuffer,
            skinning: i.skinning,
            maxBones: a,
            useVertexTexture: t.floatVertexTextures && h && h.skeleton && h.skeleton.useVertexTexture,
            morphTargets: i.morphTargets,
            morphNormals: i.morphNormals,
            maxMorphTargets: n.maxMorphTargets,
            maxMorphNormals: n.maxMorphNormals,
            numDirLights: u.directional.length,
            numPointLights: u.point.length,
            numSpotLights: u.spot.length,
            numHemiLights: u.hemi.length,
            numClippingPlanes: s,
            shadowMapEnabled: n.shadowMap.enabled && h.receiveShadow && u.shadows.length > 0,
            shadowMapType: n.shadowMap.type,
            toneMapping: n.toneMapping,
            physicallyCorrectLights: n.physicallyCorrectLights,
            premultipliedAlpha: i.premultipliedAlpha,
            alphaTest: i.alphaTest,
            doubleSided: i.side === THREE.DoubleSide,
            flipSided: i.side === THREE.BackSide,
            depthPacking: i.depthPacking !== undefined ? i.depthPacking : !1
        }
    };
    this.getProgramCode = function(n, t) {
        var i = [],
            f, r;
        if (t.shaderID ? i.push(t.shaderID) : (i.push(n.fragmentShader), i.push(n.vertexShader)), n.defines !== undefined)
            for (f in n.defines) i.push(f), i.push(n.defines[f]);
        for (r = 0; r < u.length; r++) i.push(t[u[r]]);
        return i.join()
    };
    this.acquireProgram = function(t, r, u) {
        for (var f, o, e = 0, s = i.length; e < s; e++)
            if (o = i[e], o.code === u) {
                f = o;
                ++f.usedTimes;
                break
            }
        return f === undefined && (f = new THREE.WebGLProgram(n, u, t, r), i.push(f)), f
    };
    this.releaseProgram = function(n) {
        if (--n.usedTimes == 0) {
            var t = i.indexOf(n);
            i[t] = i[i.length - 1];
            i.pop();
            n.destroy()
        }
    };
    this.programs = i
};
THREE.WebGLProperties = function() {
    var n = {};
    this.get = function(t) {
        var r = t.uuid,
            i = n[r];
        return i === undefined && (i = {}, n[r] = i), i
    };
    this.delete = function(t) {
        delete n[t.uuid]
    };
    this.clear = function() {
        n = {}
    }
};
THREE.WebGLShader = function() {
    function n(n) {
        for (var i = n.split("\n"), t = 0; t < i.length; t++) i[t] = t + 1 + ": " + i[t];
        return i.join("\n")
    }
    return function(t, i, r) {
        var u = t.createShader(i);
        return t.shaderSource(u, r), t.compileShader(u), t.getShaderParameter(u, t.COMPILE_STATUS) === !1 && console.error("THREE.WebGLShader: Shader couldn't compile."), t.getShaderInfoLog(u) !== "" && console.warn("THREE.WebGLShader: gl.getShaderInfoLog()", i === t.VERTEX_SHADER ? "vertex" : "fragment", t.getShaderInfoLog(u), n(r)), u
    }
}();
THREE.WebGLShadowMap = function(n, t, i) {
    function ot(t, i, r, u) {
        var c = t.geometry,
            f = null,
            l = nt,
            s = t.customDepthMaterial,
            o;
        if (r && (l = tt, s = t.customDistanceMaterial), s) f = s;
        else {
            var w = c.morphTargets !== undefined && c.morphTargets.length > 0 && i.morphTargets,
                b = t instanceof THREE.SkinnedMesh && i.skinning,
                h = 0;
            w && (h |= y);
            b && (h |= p);
            f = l[h]
        }
        if (n.localClippingEnabled && i.clipShadows === !0 && i.clippingPlanes.length !== 0) {
            var a = f.uuid,
                v = i.uuid,
                e = it[a];
            e === undefined && (e = {}, it[a] = e);
            o = e[v];
            o === undefined && (o = f.clone(), e[v] = o);
            f = o
        }
        return f.visible = i.visible, f.wireframe = i.wireframe, f.side = i.side, f.clipShadows = i.clipShadows, f.clippingPlanes = i.clippingPlanes, f.wireframeLinewidth = i.wireframeLinewidth, f.linewidth = i.linewidth, r && f.uniforms.lightPos !== undefined && f.uniforms.lightPos.value.copy(u), f
    }

    function st(n, t, i) {
        var f, u, r, e;
        if (n.visible !== !1)
            for (n.layers.test(t.layers) && (n instanceof THREE.Mesh || n instanceof THREE.Line || n instanceof THREE.Points) && n.castShadow && (n.frustumCulled === !1 || d.intersectsObject(n) === !0) && (f = n.material, f.visible === !0 && (n.modelViewMatrix.multiplyMatrices(i.matrixWorldInverse, n.matrixWorld), l.push(n))), u = n.children, r = 0, e = u.length; r < e; r++) st(u[r], t, i)
    }
    var r = n.context,
        u = n.state,
        d = new THREE.Frustum,
        g = new THREE.Matrix4,
        v = t.shadows,
        f = new THREE.Vector2,
        h = new THREE.Vector3,
        c = new THREE.Vector3,
        l = [],
        y = 1,
        p = 2,
        w = (y | p) + 1,
        nt = new Array(w),
        tt = new Array(w),
        it = {},
        ht = [new THREE.Vector3(1, 0, 0), new THREE.Vector3(-1, 0, 0), new THREE.Vector3(0, 0, 1), new THREE.Vector3(0, 0, -1), new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, -1, 0)],
        ct = [new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, 0, 1), new THREE.Vector3(0, 0, -1)],
        e = [new THREE.Vector4, new THREE.Vector4, new THREE.Vector4, new THREE.Vector4, new THREE.Vector4, new THREE.Vector4],
        b = new THREE.MeshDepthMaterial,
        a, rt, o, et, s;
    for (b.depthPacking = THREE.RGBADepthPacking, b.clipping = !0, a = THREE.ShaderLib.distanceRGBA, rt = THREE.UniformsUtils.clone(a.uniforms), o = 0; o !== w; ++o) {
        var ut = (o & y) != 0,
            ft = (o & p) != 0,
            k = b.clone();
        k.morphTargets = ut;
        k.skinning = ft;
        nt[o] = k;
        et = new THREE.ShaderMaterial({
            defines: {
                USE_SHADOWMAP: ""
            },
            uniforms: rt,
            vertexShader: a.vertexShader,
            fragmentShader: a.fragmentShader,
            morphTargets: ut,
            skinning: ft,
            clipping: !0
        });
        tt[o] = et
    }
    s = this;
    this.enabled = !1;
    this.autoUpdate = !0;
    this.needsUpdate = !1;
    this.type = THREE.PCFShadowMap;
    this.cullFace = THREE.CullFaceFront;
    this.render = function(t, o) {
        var lt, nt, rt, bt, y, p, kt, dt, ut, b, gt, ft, ni, yt, ti, et, ii, pt, wt, it, ri, ui;
        if (s.enabled !== !1 && (s.autoUpdate !== !1 || s.needsUpdate !== !1) && v.length !== 0) {
            for (u.clearColor(1, 1, 1, 1), u.disable(r.BLEND), u.enable(r.CULL_FACE), r.frontFace(r.CCW), r.cullFace(s.cullFace === THREE.CullFaceFront ? r.FRONT : r.BACK), u.setDepthTest(!0), u.setScissorTest(!1), rt = 0, bt = v.length; rt < bt; rt++) {
                var tt = v[rt],
                    w = tt.shadow,
                    a = w.camera;
                for (f.copy(w.mapSize), tt instanceof THREE.PointLight ? (lt = 6, nt = !0, y = f.x, p = f.y, e[0].set(y * 2, p, y, p), e[1].set(0, p, y, p), e[2].set(y * 3, p, y, p), e[3].set(y, p, y, p), e[4].set(y * 3, 0, y, p), e[5].set(y, 0, y, p), f.x *= 4, f.y *= 2) : (lt = 1, nt = !1), w.map === null && (kt = {
                        minFilter: THREE.NearestFilter,
                        magFilter: THREE.NearestFilter,
                        format: THREE.RGBAFormat
                    }, w.map = new THREE.WebGLRenderTarget(f.x, f.y, kt), a.updateProjectionMatrix()), w instanceof THREE.SpotLightShadow && w.update(tt), dt = w.map, ut = w.matrix, c.setFromMatrixPosition(tt.matrixWorld), a.position.copy(c), n.setRenderTarget(dt), n.clear(), b = 0; b < lt; b++)
                    for (nt ? (h.copy(a.position), h.add(ht[b]), a.up.copy(ct[b]), a.lookAt(h), gt = e[b], u.viewport(gt)) : (h.setFromMatrixPosition(tt.target.matrixWorld), a.lookAt(h)), a.updateMatrixWorld(), a.matrixWorldInverse.getInverse(a.matrixWorld), ut.set(.5, 0, 0, .5, 0, .5, 0, .5, 0, 0, .5, .5, 0, 0, 0, 1), ut.multiply(a.projectionMatrix), ut.multiply(a.matrixWorldInverse), g.multiplyMatrices(a.projectionMatrix, a.matrixWorldInverse), d.setFromMatrix(g), l.length = 0, st(t, o, a), ft = 0, ni = l.length; ft < ni; ft++) {
                        var k = l[ft],
                            at = i.update(k),
                            vt = k.material;
                        if (vt instanceof THREE.MultiMaterial)
                            for (yt = at.groups, ti = vt.materials, et = 0, ii = yt.length; et < ii; et++) pt = yt[et], wt = ti[pt.materialIndex], wt.visible === !0 && (it = ot(k, wt, nt, c), n.renderBufferDirect(a, null, at, it, k, pt));
                        else it = ot(k, vt, nt, c), n.renderBufferDirect(a, null, at, it, k, null)
                    }
            }
            ri = n.getClearColor();
            ui = n.getClearAlpha();
            n.setClearColor(ri, ui);
            u.enable(r.BLEND);
            s.cullFace === THREE.CullFaceFront && r.cullFace(r.BACK);
            s.needsUpdate = !1
        }
    }
};
THREE.WebGLState = function(n, t, i) {
    var dt = this,
        h = new THREE.Vector4,
        c = n.getParameter(n.MAX_VERTEX_ATTRIBS),
        f = new Uint8Array(c),
        r = new Uint8Array(c),
        s = new Uint8Array(c),
        e = {},
        o = null,
        l = null,
        a = null,
        v = null,
        y = null,
        p = null,
        w = null,
        b = null,
        it = !1,
        rt = null,
        k = null,
        d = null,
        g = null,
        ut = null,
        ft = null,
        et = null,
        ot = null,
        st = null,
        ht = null,
        nt = null,
        ct = null,
        lt = null,
        at = null,
        vt = null,
        gt = n.getParameter(n.MAX_TEXTURE_IMAGE_UNITS),
        u = undefined,
        tt = {},
        yt = new THREE.Vector4,
        pt = null,
        wt = null,
        bt = new THREE.Vector4,
        kt = new THREE.Vector4;
    this.init = function() {
        this.clearColor(0, 0, 0, 1);
        this.clearDepth(1);
        this.clearStencil(0);
        this.enable(n.DEPTH_TEST);
        n.depthFunc(n.LEQUAL);
        n.frontFace(n.CCW);
        n.cullFace(n.BACK);
        this.enable(n.CULL_FACE);
        this.enable(n.BLEND);
        n.blendEquation(n.FUNC_ADD);
        n.blendFunc(n.SRC_ALPHA, n.ONE_MINUS_SRC_ALPHA)
    };
    this.initAttributes = function() {
        for (var n = 0, t = f.length; n < t; n++) f[n] = 0
    };
    this.enableAttribute = function(i) {
        if (f[i] = 1, r[i] === 0 && (n.enableVertexAttribArray(i), r[i] = 1), s[i] !== 0) {
            var u = t.get("ANGLE_instanced_arrays");
            u.vertexAttribDivisorANGLE(i, 0);
            s[i] = 0
        }
    };
    this.enableAttributeAndDivisor = function(t, i, u) {
        f[t] = 1;
        r[t] === 0 && (n.enableVertexAttribArray(t), r[t] = 1);
        s[t] !== i && (u.vertexAttribDivisorANGLE(t, i), s[t] = i)
    };
    this.disableUnusedAttributes = function() {
        for (var t = 0, i = r.length; t < i; t++) r[t] !== f[t] && (n.disableVertexAttribArray(t), r[t] = 0)
    };
    this.enable = function(t) {
        e[t] !== !0 && (n.enable(t), e[t] = !0)
    };
    this.disable = function(t) {
        e[t] !== !1 && (n.disable(t), e[t] = !1)
    };
    this.getCompressedTextureFormats = function() {
        var r, i;
        if (o === null && (o = [], t.get("WEBGL_compressed_texture_pvrtc") || t.get("WEBGL_compressed_texture_s3tc") || t.get("WEBGL_compressed_texture_etc1")))
            for (r = n.getParameter(n.COMPRESSED_TEXTURE_FORMATS), i = 0; i < r.length; i++) o.push(r[i]);
        return o
    };
    this.setBlending = function(t, r, u, f, e, o, s, h) {
        t === THREE.NoBlending ? this.disable(n.BLEND) : this.enable(n.BLEND);
        (t !== l || h !== it) && (t === THREE.AdditiveBlending ? h ? (n.blendEquationSeparate(n.FUNC_ADD, n.FUNC_ADD), n.blendFuncSeparate(n.ONE, n.ONE, n.ONE, n.ONE)) : (n.blendEquation(n.FUNC_ADD), n.blendFunc(n.SRC_ALPHA, n.ONE)) : t === THREE.SubtractiveBlending ? h ? (n.blendEquationSeparate(n.FUNC_ADD, n.FUNC_ADD), n.blendFuncSeparate(n.ZERO, n.ZERO, n.ONE_MINUS_SRC_COLOR, n.ONE_MINUS_SRC_ALPHA)) : (n.blendEquation(n.FUNC_ADD), n.blendFunc(n.ZERO, n.ONE_MINUS_SRC_COLOR)) : t === THREE.MultiplyBlending ? h ? (n.blendEquationSeparate(n.FUNC_ADD, n.FUNC_ADD), n.blendFuncSeparate(n.ZERO, n.ZERO, n.SRC_COLOR, n.SRC_ALPHA)) : (n.blendEquation(n.FUNC_ADD), n.blendFunc(n.ZERO, n.SRC_COLOR)) : h ? (n.blendEquationSeparate(n.FUNC_ADD, n.FUNC_ADD), n.blendFuncSeparate(n.ONE, n.ONE_MINUS_SRC_ALPHA, n.ONE, n.ONE_MINUS_SRC_ALPHA)) : (n.blendEquationSeparate(n.FUNC_ADD, n.FUNC_ADD), n.blendFuncSeparate(n.SRC_ALPHA, n.ONE_MINUS_SRC_ALPHA, n.ONE, n.ONE_MINUS_SRC_ALPHA)), l = t, it = h);
        t === THREE.CustomBlending ? (e = e || r, o = o || u, s = s || f, (r !== a || e !== p) && (n.blendEquationSeparate(i(r), i(e)), a = r, p = e), (u !== v || f !== y || o !== w || s !== b) && (n.blendFuncSeparate(i(u), i(f), i(o), i(s)), v = u, y = f, w = o, b = s)) : (a = null, v = null, y = null, p = null, w = null, b = null)
    };
    this.setDepthFunc = function(t) {
        if (rt !== t) {
            if (t) switch (t) {
                case THREE.NeverDepth:
                    n.depthFunc(n.NEVER);
                    break;
                case THREE.AlwaysDepth:
                    n.depthFunc(n.ALWAYS);
                    break;
                case THREE.LessDepth:
                    n.depthFunc(n.LESS);
                    break;
                case THREE.LessEqualDepth:
                    n.depthFunc(n.LEQUAL);
                    break;
                case THREE.EqualDepth:
                    n.depthFunc(n.EQUAL);
                    break;
                case THREE.GreaterEqualDepth:
                    n.depthFunc(n.GEQUAL);
                    break;
                case THREE.GreaterDepth:
                    n.depthFunc(n.GREATER);
                    break;
                case THREE.NotEqualDepth:
                    n.depthFunc(n.NOTEQUAL);
                    break;
                default:
                    n.depthFunc(n.LEQUAL)
            } else n.depthFunc(n.LEQUAL);
            rt = t
        }
    };
    this.setDepthTest = function(t) {
        t ? this.enable(n.DEPTH_TEST) : this.disable(n.DEPTH_TEST)
    };
    this.setDepthWrite = function(t) {
        k !== t && (n.depthMask(t), k = t)
    };
    this.setColorWrite = function(t) {
        d !== t && (n.colorMask(t, t, t, t), d = t)
    };
    this.setStencilFunc = function(t, i, r) {
        (ut !== t || ft !== i || et !== r) && (n.stencilFunc(t, i, r), ut = t, ft = i, et = r)
    };
    this.setStencilOp = function(t, i, r) {
        (ot !== t || st !== i || ht !== r) && (n.stencilOp(t, i, r), ot = t, st = i, ht = r)
    };
    this.setStencilTest = function(t) {
        t ? this.enable(n.STENCIL_TEST) : this.disable(n.STENCIL_TEST)
    };
    this.setStencilWrite = function(t) {
        g !== t && (n.stencilMask(t), g = t)
    };
    this.setFlipSided = function(t) {
        nt !== t && (t ? n.frontFace(n.CW) : n.frontFace(n.CCW), nt = t)
    };
    this.setLineWidth = function(t) {
        t !== ct && (n.lineWidth(t), ct = t)
    };
    this.setPolygonOffset = function(t, i, r) {
        t ? this.enable(n.POLYGON_OFFSET_FILL) : this.disable(n.POLYGON_OFFSET_FILL);
        t && (lt !== i || at !== r) && (n.polygonOffset(i, r), lt = i, at = r)
    };
    this.getScissorTest = function() {
        return vt
    };
    this.setScissorTest = function(t) {
        vt = t;
        t ? this.enable(n.SCISSOR_TEST) : this.disable(n.SCISSOR_TEST)
    };
    this.activeTexture = function(t) {
        t === undefined && (t = n.TEXTURE0 + gt - 1);
        u !== t && (n.activeTexture(t), u = t)
    };
    this.bindTexture = function(t, i) {
        u === undefined && dt.activeTexture();
        var r = tt[u];
        r === undefined && (r = {
            type: undefined,
            texture: undefined
        }, tt[u] = r);
        (r.type !== t || r.texture !== i) && (n.bindTexture(t, i), r.type = t, r.texture = i)
    };
    this.compressedTexImage2D = function() {
        try {
            n.compressedTexImage2D.apply(n, arguments)
        } catch (t) {
            console.error(t)
        }
    };
    this.texImage2D = function() {
        try {
            n.texImage2D.apply(n, arguments)
        } catch (t) {
            console.error(t)
        }
    };
    this.clearColor = function(t, i, r, u) {
        h.set(t, i, r, u);
        yt.equals(h) === !1 && (n.clearColor(t, i, r, u), yt.copy(h))
    };
    this.clearDepth = function(t) {
        pt !== t && (n.clearDepth(t), pt = t)
    };
    this.clearStencil = function(t) {
        wt !== t && (n.clearStencil(t), wt = t)
    };
    this.scissor = function(t) {
        bt.equals(t) === !1 && (n.scissor(t.x, t.y, t.z, t.w), bt.copy(t))
    };
    this.viewport = function(t) {
        kt.equals(t) === !1 && (n.viewport(t.x, t.y, t.z, t.w), kt.copy(t))
    };
    this.reset = function() {
        for (var t = 0; t < r.length; t++) r[t] === 1 && (n.disableVertexAttribArray(t), r[t] = 0);
        e = {};
        o = null;
        u = undefined;
        tt = {};
        l = null;
        d = null;
        k = null;
        g = null;
        nt = null
    }
};
THREE.WebGLUniforms = function() {
    var f = function() {
            this.seq = [];
            this.map = {}
        },
        i = [],
        r = [],
        wt = function() {
            i.length = 0;
            r.length = 0
        },
        t = function(n, t, r) {
            var o = n[0],
                f, u, e, s;
            if (o <= 0 || o > 0) return n;
            if (f = t * r, u = i[f], u === undefined && (u = new Float32Array(f), i[f] = u), t !== 0)
                for (o.toArray(u, 0), e = 1, s = 0; e !== t; ++e) s += r, n[e].toArray(u, s);
            return u
        },
        e = function(n, t) {
            var i = r[t],
                u;
            for (i === undefined && (i = new Int32Array(t), r[t] = i), u = 0; u !== t; ++u) i[u] = n.allocTextureUnit();
            return i
        },
        a = function(n, t) {
            n.uniform1f(this.addr, t)
        },
        v = function(n, t) {
            n.uniform1i(this.addr, t)
        },
        y = function(n, t) {
            t.x === undefined ? n.uniform2fv(this.addr, t) : n.uniform2f(this.addr, t.x, t.y)
        },
        p = function(n, t) {
            t.x !== undefined ? n.uniform3f(this.addr, t.x, t.y, t.z) : t.r !== undefined ? n.uniform3f(this.addr, t.r, t.g, t.b) : n.uniform3fv(this.addr, t)
        },
        w = function(n, t) {
            t.x === undefined ? n.uniform4fv(this.addr, t) : n.uniform4f(this.addr, t.x, t.y, t.z, t.w)
        },
        b = function(n, t) {
            n.uniformMatrix2fv(this.addr, !1, t.elements || t)
        },
        k = function(n, t) {
            n.uniformMatrix3fv(this.addr, !1, t.elements || t)
        },
        d = function(n, t) {
            n.uniformMatrix4fv(this.addr, !1, t.elements || t)
        },
        g = function(n, t, i) {
            var r = i.allocTextureUnit();
            n.uniform1i(this.addr, r);
            t && i.setTexture2D(t, r)
        },
        nt = function(n, t, i) {
            var r = i.allocTextureUnit();
            n.uniform1i(this.addr, r);
            t && i.setTextureCube(t, r)
        },
        o = function(n, t) {
            n.uniform2iv(this.addr, t)
        },
        s = function(n, t) {
            n.uniform3iv(this.addr, t)
        },
        h = function(n, t) {
            n.uniform4iv(this.addr, t)
        },
        tt = function(n) {
            switch (n) {
                case 5126:
                    return a;
                case 35664:
                    return y;
                case 35665:
                    return p;
                case 35666:
                    return w;
                case 35674:
                    return b;
                case 35675:
                    return k;
                case 35676:
                    return d;
                case 35678:
                    return g;
                case 35680:
                    return nt;
                case 5124:
                case 35670:
                    return v;
                case 35667:
                case 35671:
                    return o;
                case 35668:
                case 35672:
                    return s;
                case 35669:
                case 35673:
                    return h
            }
        },
        it = function(n, t) {
            n.uniform1fv(this.addr, t)
        },
        rt = function(n, t) {
            n.uniform1iv(this.addr, t)
        },
        ut = function(n, i) {
            n.uniform2fv(this.addr, t(i, this.size, 2))
        },
        ft = function(n, i) {
            n.uniform3fv(this.addr, t(i, this.size, 3))
        },
        et = function(n, i) {
            n.uniform4fv(this.addr, t(i, this.size, 4))
        },
        ot = function(n, i) {
            n.uniformMatrix2fv(this.addr, !1, t(i, this.size, 4))
        },
        st = function(n, i) {
            n.uniformMatrix3fv(this.addr, !1, t(i, this.size, 9))
        },
        ht = function(n, i) {
            n.uniformMatrix4fv(this.addr, !1, t(i, this.size, 16))
        },
        ct = function(n, t, i) {
            var f = t.length,
                o = e(i, f),
                r, u;
            for (n.uniform1iv(this.addr, o), r = 0; r !== f; ++r) u = t[r], u && i.setTexture2D(u, o[r])
        },
        lt = function(n, t, i) {
            var f = t.length,
                o = e(i, f),
                r, u;
            for (n.uniform1iv(this.addr, o), r = 0; r !== f; ++r) u = t[r], u && i.setTextureCube(u, o[r])
        },
        at = function(n) {
            switch (n) {
                case 5126:
                    return it;
                case 35664:
                    return ut;
                case 35665:
                    return ft;
                case 35666:
                    return et;
                case 35674:
                    return ot;
                case 35675:
                    return st;
                case 35676:
                    return ht;
                case 35678:
                    return ct;
                case 35680:
                    return lt;
                case 5124:
                case 35670:
                    return rt;
                case 35667:
                case 35671:
                    return o;
                case 35668:
                case 35672:
                    return s;
                case 35669:
                case 35673:
                    return h
            }
        },
        vt = function(n, t, i) {
            this.id = n;
            this.addr = i;
            this.setValue = tt(t.type)
        },
        yt = function(n, t, i) {
            this.id = n;
            this.addr = i;
            this.size = t.size;
            this.setValue = at(t.type)
        },
        c = function(n) {
            this.id = n;
            f.call(this)
        };
    c.prototype.setValue = function(n, t) {
        for (var u = this.seq, r, i = 0, f = u.length; i !== f; ++i) r = u[i], r.setValue(n, t[r.id])
    };
    var u = /([\w\d_]+)(\])?(\[|\.)?/g,
        l = function(n, t) {
            n.seq.push(t);
            n.map[t.id] = t
        },
        pt = function(n, t, i) {
            var s = n.name,
                a = s.length,
                h, f;
            for (u.lastIndex = 0;;) {
                var e = u.exec(s),
                    v = u.lastIndex,
                    r = e[1],
                    y = e[2] === "]",
                    o = e[3];
                if (y && (r = r | 0), o === undefined || o === "[" && v + 2 === a) {
                    l(i, o === undefined ? new vt(r, n, t) : new yt(r, n, t));
                    break
                } else h = i.map, f = h[r], f === undefined && (f = new c(r), l(i, f)), i = f
            }
        },
        n = function(n, t, i) {
            var u, r;
            for (f.call(this), this.renderer = i, u = n.getProgramParameter(t, n.ACTIVE_UNIFORMS), r = 0; r !== u; ++r) {
                var e = n.getActiveUniform(t, r),
                    o = e.name,
                    s = n.getUniformLocation(t, o);
                pt(e, s, this)
            }
        };
    return n.prototype.setValue = function(n, t, i) {
        var r = this.map[t];
        r !== undefined && r.setValue(n, i, this.renderer)
    }, n.prototype.set = function(n, t, i) {
        var r = this.map[i];
        r !== undefined && r.setValue(n, t[i], this.renderer)
    }, n.prototype.setOptional = function(n, t, i) {
        var r = t[i];
        r !== undefined && this.setValue(n, i, r)
    }, n.upload = function(n, t, i, r) {
        for (var f, e, u = 0, o = t.length; u !== o; ++u) f = t[u], e = i[f.id], e.needsUpdate !== !1 && f.setValue(n, e.value, r)
    }, n.seqWithValue = function(n, t) {
        for (var u = [], r, i = 0, f = n.length; i !== f; ++i) r = n[i], r.id in t && u.push(r);
        return u
    }, n.splitDynamic = function(n, t) {
        for (var u = null, o = n.length, i = 0, f, e, r = 0; r !== o; ++r) f = n[r], e = t[f.id], e && e.dynamic === !0 ? (u === null && (u = []), u.push(f)) : (i < r && (n[i] = f), ++i);
        return i < o && (n.length = i), u
    }, n.evalDynamic = function(n, t, i, r) {
        for (var f, e, u = 0, o = n.length; u !== o; ++u) f = t[n[u].id], e = f.onUpdateCallback, e !== undefined && e.call(f, i, r)
    }, n
}();
THREE.LensFlarePlugin = function(n, t) {
    function a() {
        var n = new Float32Array([-1, -1, 0, 0, 1, -1, 1, 0, 1, 1, 1, 1, -1, 1, 0, 1]),
            t = new Uint16Array([0, 1, 2, 0, 2, 3]);
        s = i.createBuffer();
        h = i.createBuffer();
        i.bindBuffer(i.ARRAY_BUFFER, s);
        i.bufferData(i.ARRAY_BUFFER, n, i.STATIC_DRAW);
        i.bindBuffer(i.ELEMENT_ARRAY_BUFFER, h);
        i.bufferData(i.ELEMENT_ARRAY_BUFFER, t, i.STATIC_DRAW);
        o = i.createTexture();
        c = i.createTexture();
        r.bindTexture(i.TEXTURE_2D, o);
        i.texImage2D(i.TEXTURE_2D, 0, i.RGB, 16, 16, 0, i.RGB, i.UNSIGNED_BYTE, null);
        i.texParameteri(i.TEXTURE_2D, i.TEXTURE_WRAP_S, i.CLAMP_TO_EDGE);
        i.texParameteri(i.TEXTURE_2D, i.TEXTURE_WRAP_T, i.CLAMP_TO_EDGE);
        i.texParameteri(i.TEXTURE_2D, i.TEXTURE_MAG_FILTER, i.NEAREST);
        i.texParameteri(i.TEXTURE_2D, i.TEXTURE_MIN_FILTER, i.NEAREST);
        r.bindTexture(i.TEXTURE_2D, c);
        i.texImage2D(i.TEXTURE_2D, 0, i.RGBA, 16, 16, 0, i.RGBA, i.UNSIGNED_BYTE, null);
        i.texParameteri(i.TEXTURE_2D, i.TEXTURE_WRAP_S, i.CLAMP_TO_EDGE);
        i.texParameteri(i.TEXTURE_2D, i.TEXTURE_WRAP_T, i.CLAMP_TO_EDGE);
        i.texParameteri(i.TEXTURE_2D, i.TEXTURE_MAG_FILTER, i.NEAREST);
        i.texParameteri(i.TEXTURE_2D, i.TEXTURE_MIN_FILTER, i.NEAREST);
        l = {
            vertexShader: "uniform lowp int renderType;\nuniform vec3 screenPosition;\nuniform vec2 scale;\nuniform float rotation;\nuniform sampler2D occlusionMap;\nattribute vec2 position;\nattribute vec2 uv;\nvarying vec2 vUV;\nvarying float vVisibility;\nvoid main() {\nvUV = uv;\nvec2 pos = position;\nif ( renderType == 2 ) {\nvec4 visibility = texture2D( occlusionMap, vec2( 0.1, 0.1 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.5, 0.1 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.9, 0.1 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.9, 0.5 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.9, 0.9 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.5, 0.9 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.1, 0.9 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.1, 0.5 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.5, 0.5 ) );\nvVisibility =        visibility.r / 9.0;\nvVisibility *= 1.0 - visibility.g / 9.0;\nvVisibility *=       visibility.b / 9.0;\nvVisibility *= 1.0 - visibility.a / 9.0;\npos.x = cos( rotation ) * position.x - sin( rotation ) * position.y;\npos.y = sin( rotation ) * position.x + cos( rotation ) * position.y;\n}\ngl_Position = vec4( ( pos * scale + screenPosition.xy ).xy, screenPosition.z, 1.0 );\n}",
            fragmentShader: "uniform lowp int renderType;\nuniform sampler2D map;\nuniform float opacity;\nuniform vec3 color;\nvarying vec2 vUV;\nvarying float vVisibility;\nvoid main() {\nif ( renderType == 0 ) {\ngl_FragColor = vec4( 1.0, 0.0, 1.0, 0.0 );\n} else if ( renderType == 1 ) {\ngl_FragColor = texture2D( map, vUV );\n} else {\nvec4 texture = texture2D( map, vUV );\ntexture.a *= opacity * vVisibility;\ngl_FragColor = texture;\ngl_FragColor.rgb *= color;\n}\n}"
        };
        u = v(l);
        e = {
            vertex: i.getAttribLocation(u, "position"),
            uv: i.getAttribLocation(u, "uv")
        };
        f = {
            renderType: i.getUniformLocation(u, "renderType"),
            map: i.getUniformLocation(u, "map"),
            occlusionMap: i.getUniformLocation(u, "occlusionMap"),
            opacity: i.getUniformLocation(u, "opacity"),
            color: i.getUniformLocation(u, "color"),
            scale: i.getUniformLocation(u, "scale"),
            rotation: i.getUniformLocation(u, "rotation"),
            screenPosition: i.getUniformLocation(u, "screenPosition")
        }
    }

    function v(t) {
        var r = i.createProgram(),
            u = i.createShader(i.FRAGMENT_SHADER),
            f = i.createShader(i.VERTEX_SHADER),
            e = "precision " + n.getPrecision() + " float;\n";
        return i.shaderSource(u, e + t.fragmentShader), i.shaderSource(f, e + t.vertexShader), i.compileShader(u), i.compileShader(f), i.attachShader(r, u), i.attachShader(r, f), i.linkProgram(r), r
    }
    var i = n.context,
        r = n.state,
        s, h, l, u, e, f, o, c;
    this.render = function(l, v, y) {
        var tt, ot, b, it, st, p;
        if (t.length !== 0) {
            var nt = new THREE.Vector3,
                rt = y.w / y.z,
                ft = y.z * .5,
                et = y.w * .5,
                k = 16 / y.w,
                d = new THREE.Vector2(k * rt, k),
                w = new THREE.Vector3(1, 1, 0),
                g = new THREE.Vector2(1, 1),
                ut = new THREE.Box2;
            for (ut.min.set(0, 0), ut.max.set(y.z - 16, y.w - 16), u === undefined && a(), i.useProgram(u), r.initAttributes(), r.enableAttribute(e.vertex), r.enableAttribute(e.uv), r.disableUnusedAttributes(), i.uniform1i(f.occlusionMap, 0), i.uniform1i(f.map, 1), i.bindBuffer(i.ARRAY_BUFFER, s), i.vertexAttribPointer(e.vertex, 2, i.FLOAT, !1, 16, 0), i.vertexAttribPointer(e.uv, 2, i.FLOAT, !1, 16, 8), i.bindBuffer(i.ELEMENT_ARRAY_BUFFER, h), r.disable(i.CULL_FACE), r.setDepthWrite(!1), tt = 0, ot = t.length; tt < ot; tt++)
                if (k = 16 / y.w, d.set(k * rt, k), b = t[tt], nt.set(b.matrixWorld.elements[12], b.matrixWorld.elements[13], b.matrixWorld.elements[14]), nt.applyMatrix4(v.matrixWorldInverse), nt.applyProjection(v.projectionMatrix), w.copy(nt), g.x = y.x + w.x * ft + ft - 8, g.y = y.y + w.y * et + et - 8, ut.containsPoint(g) === !0)
                    for (r.activeTexture(i.TEXTURE0), r.bindTexture(i.TEXTURE_2D, null), r.activeTexture(i.TEXTURE1), r.bindTexture(i.TEXTURE_2D, o), i.copyTexImage2D(i.TEXTURE_2D, 0, i.RGB, g.x, g.y, 16, 16, 0), i.uniform1i(f.renderType, 0), i.uniform2f(f.scale, d.x, d.y), i.uniform3f(f.screenPosition, w.x, w.y, w.z), r.disable(i.BLEND), r.enable(i.DEPTH_TEST), i.drawElements(i.TRIANGLES, 6, i.UNSIGNED_SHORT, 0), r.activeTexture(i.TEXTURE0), r.bindTexture(i.TEXTURE_2D, c), i.copyTexImage2D(i.TEXTURE_2D, 0, i.RGBA, g.x, g.y, 16, 16, 0), i.uniform1i(f.renderType, 1), r.disable(i.DEPTH_TEST), r.activeTexture(i.TEXTURE1), r.bindTexture(i.TEXTURE_2D, o), i.drawElements(i.TRIANGLES, 6, i.UNSIGNED_SHORT, 0), b.positionScreen.copy(w), b.customUpdateCallback ? b.customUpdateCallback(b) : b.updateLensFlares(), i.uniform1i(f.renderType, 2), r.enable(i.BLEND), it = 0, st = b.lensFlares.length; it < st; it++) p = b.lensFlares[it], p.opacity > .001 && p.scale > .001 && (w.x = p.x, w.y = p.y, w.z = p.z, k = p.size * p.scale / y.w, d.x = k * rt, d.y = k, i.uniform3f(f.screenPosition, w.x, w.y, w.z), i.uniform2f(f.scale, d.x, d.y), i.uniform1f(f.rotation, p.rotation), i.uniform1f(f.opacity, p.opacity), i.uniform3f(f.color, p.color.r, p.color.g, p.color.b), r.setBlending(p.blending, p.blendEquation, p.blendSrc, p.blendDst), n.setTexture2D(p.texture, 1), i.drawElements(i.TRIANGLES, 6, i.UNSIGNED_SHORT, 0));
            r.enable(i.CULL_FACE);
            r.enable(i.DEPTH_TEST);
            r.setDepthWrite(!0);
            n.resetGLState()
        }
    }
};
THREE.SpritePlugin = function(n, t) {
    function v() {
        var f = new Float32Array([-.5, -.5, 0, 0, .5, -.5, 1, 0, .5, .5, 1, 1, -.5, .5, 0, 1]),
            c = new Uint16Array([0, 1, 2, 0, 2, 3]),
            n, t;
        o = i.createBuffer();
        s = i.createBuffer();
        i.bindBuffer(i.ARRAY_BUFFER, o);
        i.bufferData(i.ARRAY_BUFFER, f, i.STATIC_DRAW);
        i.bindBuffer(i.ELEMENT_ARRAY_BUFFER, s);
        i.bufferData(i.ELEMENT_ARRAY_BUFFER, c, i.STATIC_DRAW);
        u = y();
        e = {
            position: i.getAttribLocation(u, "position"),
            uv: i.getAttribLocation(u, "uv")
        };
        r = {
            uvOffset: i.getUniformLocation(u, "uvOffset"),
            uvScale: i.getUniformLocation(u, "uvScale"),
            rotation: i.getUniformLocation(u, "rotation"),
            scale: i.getUniformLocation(u, "scale"),
            color: i.getUniformLocation(u, "color"),
            map: i.getUniformLocation(u, "map"),
            opacity: i.getUniformLocation(u, "opacity"),
            modelViewMatrix: i.getUniformLocation(u, "modelViewMatrix"),
            projectionMatrix: i.getUniformLocation(u, "projectionMatrix"),
            fogType: i.getUniformLocation(u, "fogType"),
            fogDensity: i.getUniformLocation(u, "fogDensity"),
            fogNear: i.getUniformLocation(u, "fogNear"),
            fogFar: i.getUniformLocation(u, "fogFar"),
            fogColor: i.getUniformLocation(u, "fogColor"),
            alphaTest: i.getUniformLocation(u, "alphaTest")
        };
        n = document.createElement("canvas");
        n.width = 8;
        n.height = 8;
        t = n.getContext("2d");
        t.fillStyle = "white";
        t.fillRect(0, 0, 8, 8);
        h = new THREE.Texture(n);
        h.needsUpdate = !0
    }

    function y() {
        var t = i.createProgram(),
            r = i.createShader(i.VERTEX_SHADER),
            u = i.createShader(i.FRAGMENT_SHADER);
        return i.shaderSource(r, ["precision " + n.getPrecision() + " float;", "uniform mat4 modelViewMatrix;", "uniform mat4 projectionMatrix;", "uniform float rotation;", "uniform vec2 scale;", "uniform vec2 uvOffset;", "uniform vec2 uvScale;", "attribute vec2 position;", "attribute vec2 uv;", "varying vec2 vUV;", "void main() {", "vUV = uvOffset + uv * uvScale;", "vec2 alignedPosition = position * scale;", "vec2 rotatedPosition;", "rotatedPosition.x = cos( rotation ) * alignedPosition.x - sin( rotation ) * alignedPosition.y;", "rotatedPosition.y = sin( rotation ) * alignedPosition.x + cos( rotation ) * alignedPosition.y;", "vec4 finalPosition;", "finalPosition = modelViewMatrix * vec4( 0.0, 0.0, 0.0, 1.0 );", "finalPosition.xy += rotatedPosition;", "finalPosition = projectionMatrix * finalPosition;", "gl_Position = finalPosition;", "}"].join("\n")), i.shaderSource(u, ["precision " + n.getPrecision() + " float;", "uniform vec3 color;", "uniform sampler2D map;", "uniform float opacity;", "uniform int fogType;", "uniform vec3 fogColor;", "uniform float fogDensity;", "uniform float fogNear;", "uniform float fogFar;", "uniform float alphaTest;", "varying vec2 vUV;", "void main() {", "vec4 texture = texture2D( map, vUV );", "if ( texture.a < alphaTest ) discard;", "gl_FragColor = vec4( color * texture.xyz, texture.a * opacity );", "if ( fogType > 0 ) {", "float depth = gl_FragCoord.z / gl_FragCoord.w;", "float fogFactor = 0.0;", "if ( fogType == 1 ) {", "fogFactor = smoothstep( fogNear, fogFar, depth );", "} else {", "const float LOG2 = 1.442695;", "fogFactor = exp2( - fogDensity * fogDensity * depth * depth * LOG2 );", "fogFactor = 1.0 - clamp( fogFactor, 0.0, 1.0 );", "}", "gl_FragColor = mix( gl_FragColor, vec4( fogColor, gl_FragColor.w ), fogFactor );", "}", "}"].join("\n")), i.compileShader(r), i.compileShader(u), i.attachShader(t, r), i.attachShader(t, u), i.linkProgram(t), t
    }

    function p(n, t) {
        return n.renderOrder !== t.renderOrder ? n.renderOrder - t.renderOrder : n.z !== t.z ? t.z - n.z : t.id - n.id
    }
    var i = n.context,
        f = n.state,
        o, s, u, e, r, h, l = new THREE.Vector3,
        a = new THREE.Quaternion,
        c = new THREE.Vector3;
    this.render = function(y, w) {
        var ut, g, tt, k, b, it;
        if (t.length !== 0) {
            u === undefined && v();
            i.useProgram(u);
            f.initAttributes();
            f.enableAttribute(e.position);
            f.enableAttribute(e.uv);
            f.disableUnusedAttributes();
            f.disable(i.CULL_FACE);
            f.enable(i.BLEND);
            i.bindBuffer(i.ARRAY_BUFFER, o);
            i.vertexAttribPointer(e.position, 2, i.FLOAT, !1, 16, 0);
            i.vertexAttribPointer(e.uv, 2, i.FLOAT, !1, 16, 8);
            i.bindBuffer(i.ELEMENT_ARRAY_BUFFER, s);
            i.uniformMatrix4fv(r.projectionMatrix, !1, w.projectionMatrix.elements);
            f.activeTexture(i.TEXTURE0);
            i.uniform1i(r.map, 0);
            var nt = 0,
                rt = 0,
                d = y.fog;
            for (d ? (i.uniform3f(r.fogColor, d.color.r, d.color.g, d.color.b), d instanceof THREE.Fog ? (i.uniform1f(r.fogNear, d.near), i.uniform1f(r.fogFar, d.far), i.uniform1i(r.fogType, 1), nt = 1, rt = 1) : d instanceof THREE.FogExp2 && (i.uniform1f(r.fogDensity, d.density), i.uniform1i(r.fogType, 2), nt = 2, rt = 2)) : (i.uniform1i(r.fogType, 0), nt = 0, rt = 0), g = 0, tt = t.length; g < tt; g++) k = t[g], k.modelViewMatrix.multiplyMatrices(w.matrixWorldInverse, k.matrixWorld), k.z = -k.modelViewMatrix.elements[14];
            for (t.sort(p), ut = [], g = 0, tt = t.length; g < tt; g++) k = t[g], b = k.material, i.uniform1f(r.alphaTest, b.alphaTest), i.uniformMatrix4fv(r.modelViewMatrix, !1, k.modelViewMatrix.elements), k.matrixWorld.decompose(l, a, c), ut[0] = c.x, ut[1] = c.y, it = 0, y.fog && b.fog && (it = rt), nt !== it && (i.uniform1i(r.fogType, it), nt = it), b.map !== null ? (i.uniform2f(r.uvOffset, b.map.offset.x, b.map.offset.y), i.uniform2f(r.uvScale, b.map.repeat.x, b.map.repeat.y)) : (i.uniform2f(r.uvOffset, 0, 0), i.uniform2f(r.uvScale, 1, 1)), i.uniform1f(r.opacity, b.opacity), i.uniform3f(r.color, b.color.r, b.color.g, b.color.b), i.uniform1f(r.rotation, b.rotation), i.uniform2fv(r.scale, ut), f.setBlending(b.blending, b.blendEquation, b.blendSrc, b.blendDst), f.setDepthTest(b.depthTest), f.setDepthWrite(b.depthWrite), b.map ? n.setTexture2D(b.map, 0) : n.setTexture2D(h, 0), i.drawElements(i.TRIANGLES, 6, i.UNSIGNED_SHORT, 0);
            f.enable(i.CULL_FACE);
            n.resetGLState()
        }
    }
};
Object.defineProperties(THREE.Box2.prototype, {
    empty: {
        value: function() {
            return console.warn("THREE.Box2: .empty() has been renamed to .isEmpty()."), this.isEmpty()
        }
    },
    isIntersectionBox: {
        value: function(n) {
            return console.warn("THREE.Box2: .isIntersectionBox() has been renamed to .intersectsBox()."), this.intersectsBox(n)
        }
    }
});
Object.defineProperties(THREE.Box3.prototype, {
    empty: {
        value: function() {
            return console.warn("THREE.Box3: .empty() has been renamed to .isEmpty()."), this.isEmpty()
        }
    },
    isIntersectionBox: {
        value: function(n) {
            return console.warn("THREE.Box3: .isIntersectionBox() has been renamed to .intersectsBox()."), this.intersectsBox(n)
        }
    },
    isIntersectionSphere: {
        value: function(n) {
            return console.warn("THREE.Box3: .isIntersectionSphere() has been renamed to .intersectsSphere()."), this.intersectsSphere(n)
        }
    }
});
Object.defineProperties(THREE.Matrix3.prototype, {
    multiplyVector3: {
        value: function(n) {
            return console.warn("THREE.Matrix3: .multiplyVector3() has been removed. Use vector.applyMatrix3( matrix ) instead."), n.applyMatrix3(this)
        }
    },
    multiplyVector3Array: {
        value: function(n) {
            return console.warn("THREE.Matrix3: .multiplyVector3Array() has been renamed. Use matrix.applyToVector3Array( array ) instead."), this.applyToVector3Array(n)
        }
    }
});
Object.defineProperties(THREE.Matrix4.prototype, {
    extractPosition: {
        value: function(n) {
            return console.warn("THREE.Matrix4: .extractPosition() has been renamed to .copyPosition()."), this.copyPosition(n)
        }
    },
    setRotationFromQuaternion: {
        value: function(n) {
            return console.warn("THREE.Matrix4: .setRotationFromQuaternion() has been renamed to .makeRotationFromQuaternion()."), this.makeRotationFromQuaternion(n)
        }
    },
    multiplyVector3: {
        value: function(n) {
            return console.warn("THREE.Matrix4: .multiplyVector3() has been removed. Use vector.applyMatrix4( matrix ) or vector.applyProjection( matrix ) instead."), n.applyProjection(this)
        }
    },
    multiplyVector4: {
        value: function(n) {
            return console.warn("THREE.Matrix4: .multiplyVector4() has been removed. Use vector.applyMatrix4( matrix ) instead."), n.applyMatrix4(this)
        }
    },
    multiplyVector3Array: {
        value: function(n) {
            return console.warn("THREE.Matrix4: .multiplyVector3Array() has been renamed. Use matrix.applyToVector3Array( array ) instead."), this.applyToVector3Array(n)
        }
    },
    rotateAxis: {
        value: function(n) {
            console.warn("THREE.Matrix4: .rotateAxis() has been removed. Use Vector3.transformDirection( matrix ) instead.");
            n.transformDirection(this)
        }
    },
    crossVector: {
        value: function(n) {
            return console.warn("THREE.Matrix4: .crossVector() has been removed. Use vector.applyMatrix4( matrix ) instead."), n.applyMatrix4(this)
        }
    },
    translate: {
        value: function() {
            console.error("THREE.Matrix4: .translate() has been removed.")
        }
    },
    rotateX: {
        value: function() {
            console.error("THREE.Matrix4: .rotateX() has been removed.")
        }
    },
    rotateY: {
        value: function() {
            console.error("THREE.Matrix4: .rotateY() has been removed.")
        }
    },
    rotateZ: {
        value: function() {
            console.error("THREE.Matrix4: .rotateZ() has been removed.")
        }
    },
    rotateByAxis: {
        value: function() {
            console.error("THREE.Matrix4: .rotateByAxis() has been removed.")
        }
    }
});
Object.defineProperties(THREE.Plane.prototype, {
    isIntersectionLine: {
        value: function(n) {
            return console.warn("THREE.Plane: .isIntersectionLine() has been renamed to .intersectsLine()."), this.intersectsLine(n)
        }
    }
});
Object.defineProperties(THREE.Quaternion.prototype, {
    multiplyVector3: {
        value: function(n) {
            return console.warn("THREE.Quaternion: .multiplyVector3() has been removed. Use is now vector.applyQuaternion( quaternion ) instead."), n.applyQuaternion(this)
        }
    }
});
Object.defineProperties(THREE.Ray.prototype, {
    isIntersectionBox: {
        value: function(n) {
            return console.warn("THREE.Ray: .isIntersectionBox() has been renamed to .intersectsBox()."), this.intersectsBox(n)
        }
    },
    isIntersectionPlane: {
        value: function(n) {
            return console.warn("THREE.Ray: .isIntersectionPlane() has been renamed to .intersectsPlane()."), this.intersectsPlane(n)
        }
    },
    isIntersectionSphere: {
        value: function(n) {
            return console.warn("THREE.Ray: .isIntersectionSphere() has been renamed to .intersectsSphere()."), this.intersectsSphere(n)
        }
    }
});
Object.defineProperties(THREE.Vector3.prototype, {
    setEulerFromRotationMatrix: {
        value: function() {
            console.error("THREE.Vector3: .setEulerFromRotationMatrix() has been removed. Use Euler.setFromRotationMatrix() instead.")
        }
    },
    setEulerFromQuaternion: {
        value: function() {
            console.error("THREE.Vector3: .setEulerFromQuaternion() has been removed. Use Euler.setFromQuaternion() instead.")
        }
    },
    getPositionFromMatrix: {
        value: function(n) {
            return console.warn("THREE.Vector3: .getPositionFromMatrix() has been renamed to .setFromMatrixPosition()."), this.setFromMatrixPosition(n)
        }
    },
    getScaleFromMatrix: {
        value: function(n) {
            return console.warn("THREE.Vector3: .getScaleFromMatrix() has been renamed to .setFromMatrixScale()."), this.setFromMatrixScale(n)
        }
    },
    getColumnFromMatrix: {
        value: function(n, t) {
            return console.warn("THREE.Vector3: .getColumnFromMatrix() has been renamed to .setFromMatrixColumn()."), this.setFromMatrixColumn(n, t)
        }
    }
});
THREE.Face4 = function(n, t, i, r, u, f, e) {
    return console.warn("THREE.Face4 has been removed. A THREE.Face3 will be created instead."), new THREE.Face3(n, t, i, u, f, e)
};
THREE.Vertex = function(n, t, i) {
    return console.warn("THREE.Vertex has been removed. Use THREE.Vector3 instead."), new THREE.Vector3(n, t, i)
};
Object.defineProperties(THREE.Object3D.prototype, {
    eulerOrder: {
        get: function() {
            return console.warn("THREE.Object3D: .eulerOrder is now .rotation.order."), this.rotation.order
        },
        set: function(n) {
            console.warn("THREE.Object3D: .eulerOrder is now .rotation.order.");
            this.rotation.order = n
        }
    },
    getChildByName: {
        value: function(n) {
            return console.warn("THREE.Object3D: .getChildByName() has been renamed to .getObjectByName()."), this.getObjectByName(n)
        }
    },
    renderDepth: {
        set: function() {
            console.warn("THREE.Object3D: .renderDepth has been removed. Use .renderOrder, instead.")
        }
    },
    translate: {
        value: function(n, t) {
            return console.warn("THREE.Object3D: .translate() has been removed. Use .translateOnAxis( axis, distance ) instead."), this.translateOnAxis(t, n)
        }
    },
    useQuaternion: {
        get: function() {
            console.warn("THREE.Object3D: .useQuaternion has been removed. The library now uses quaternions by default.")
        },
        set: function() {
            console.warn("THREE.Object3D: .useQuaternion has been removed. The library now uses quaternions by default.")
        }
    }
});
Object.defineProperties(THREE, {
    PointCloud: {
        value: function(n, t) {
            return console.warn("THREE.PointCloud has been renamed to THREE.Points."), new THREE.Points(n, t)
        }
    },
    ParticleSystem: {
        value: function(n, t) {
            return console.warn("THREE.ParticleSystem has been renamed to THREE.Points."), new THREE.Points(n, t)
        }
    }
});
Object.defineProperties(THREE.Light.prototype, {
    onlyShadow: {
        set: function() {
            console.warn("THREE.Light: .onlyShadow has been removed.")
        }
    },
    shadowCameraFov: {
        set: function(n) {
            console.warn("THREE.Light: .shadowCameraFov is now .shadow.camera.fov.");
            this.shadow.camera.fov = n
        }
    },
    shadowCameraLeft: {
        set: function(n) {
            console.warn("THREE.Light: .shadowCameraLeft is now .shadow.camera.left.");
            this.shadow.camera.left = n
        }
    },
    shadowCameraRight: {
        set: function(n) {
            console.warn("THREE.Light: .shadowCameraRight is now .shadow.camera.right.");
            this.shadow.camera.right = n
        }
    },
    shadowCameraTop: {
        set: function(n) {
            console.warn("THREE.Light: .shadowCameraTop is now .shadow.camera.top.");
            this.shadow.camera.top = n
        }
    },
    shadowCameraBottom: {
        set: function(n) {
            console.warn("THREE.Light: .shadowCameraBottom is now .shadow.camera.bottom.");
            this.shadow.camera.bottom = n
        }
    },
    shadowCameraNear: {
        set: function(n) {
            console.warn("THREE.Light: .shadowCameraNear is now .shadow.camera.near.");
            this.shadow.camera.near = n
        }
    },
    shadowCameraFar: {
        set: function(n) {
            console.warn("THREE.Light: .shadowCameraFar is now .shadow.camera.far.");
            this.shadow.camera.far = n
        }
    },
    shadowCameraVisible: {
        set: function() {
            console.warn("THREE.Light: .shadowCameraVisible has been removed. Use new THREE.CameraHelper( light.shadow.camera ) instead.")
        }
    },
    shadowBias: {
        set: function(n) {
            console.warn("THREE.Light: .shadowBias is now .shadow.bias.");
            this.shadow.bias = n
        }
    },
    shadowDarkness: {
        set: function() {
            console.warn("THREE.Light: .shadowDarkness has been removed.")
        }
    },
    shadowMapWidth: {
        set: function(n) {
            console.warn("THREE.Light: .shadowMapWidth is now .shadow.mapSize.width.");
            this.shadow.mapSize.width = n
        }
    },
    shadowMapHeight: {
        set: function(n) {
            console.warn("THREE.Light: .shadowMapHeight is now .shadow.mapSize.height.");
            this.shadow.mapSize.height = n
        }
    }
});
Object.defineProperties(THREE.BufferAttribute.prototype, {
    length: {
        get: function() {
            return console.warn("THREE.BufferAttribute: .length has been deprecated. Please use .count."), this.array.length
        }
    }
});
Object.defineProperties(THREE.BufferGeometry.prototype, {
    drawcalls: {
        get: function() {
            return console.error("THREE.BufferGeometry: .drawcalls has been renamed to .groups."), this.groups
        }
    },
    offsets: {
        get: function() {
            return console.warn("THREE.BufferGeometry: .offsets has been renamed to .groups."), this.groups
        }
    },
    addIndex: {
        value: function(n) {
            console.warn("THREE.BufferGeometry: .addIndex() has been renamed to .setIndex().");
            this.setIndex(n)
        }
    },
    addDrawCall: {
        value: function(n, t, i) {
            i !== undefined && console.warn("THREE.BufferGeometry: .addDrawCall() no longer supports indexOffset.");
            console.warn("THREE.BufferGeometry: .addDrawCall() is now .addGroup().");
            this.addGroup(n, t)
        }
    },
    clearDrawCalls: {
        value: function() {
            console.warn("THREE.BufferGeometry: .clearDrawCalls() is now .clearGroups().");
            this.clearGroups()
        }
    },
    computeTangents: {
        value: function() {
            console.warn("THREE.BufferGeometry: .computeTangents() has been removed.")
        }
    },
    computeOffsets: {
        value: function() {
            console.warn("THREE.BufferGeometry: .computeOffsets() has been removed.")
        }
    }
});
Object.defineProperties(THREE.Material.prototype, {
    wrapAround: {
        get: function() {
            console.warn("THREE." + this.type + ": .wrapAround has been removed.")
        },
        set: function() {
            console.warn("THREE." + this.type + ": .wrapAround has been removed.")
        }
    },
    wrapRGB: {
        get: function() {
            return console.warn("THREE." + this.type + ": .wrapRGB has been removed."), new THREE.Color
        }
    }
});
Object.defineProperties(THREE, {
    PointCloudMaterial: {
        value: function(n) {
            return console.warn("THREE.PointCloudMaterial has been renamed to THREE.PointsMaterial."), new THREE.PointsMaterial(n)
        }
    },
    ParticleBasicMaterial: {
        value: function(n) {
            return console.warn("THREE.ParticleBasicMaterial has been renamed to THREE.PointsMaterial."), new THREE.PointsMaterial(n)
        }
    },
    ParticleSystemMaterial: {
        value: function(n) {
            return console.warn("THREE.ParticleSystemMaterial has been renamed to THREE.PointsMaterial."), new THREE.PointsMaterial(n)
        }
    }
});
Object.defineProperties(THREE.MeshPhongMaterial.prototype, {
    metal: {
        get: function() {
            return console.warn("THREE.MeshPhongMaterial: .metal has been removed. Use THREE.MeshStandardMaterial instead."), !1
        },
        set: function() {
            console.warn("THREE.MeshPhongMaterial: .metal has been removed. Use THREE.MeshStandardMaterial instead")
        }
    }
});
Object.defineProperties(THREE.ShaderMaterial.prototype, {
    derivatives: {
        get: function() {
            return console.warn("THREE.ShaderMaterial: .derivatives has been moved to .extensions.derivatives."), this.extensions.derivatives
        },
        set: function(n) {
            console.warn("THREE. ShaderMaterial: .derivatives has been moved to .extensions.derivatives.");
            this.extensions.derivatives = n
        }
    }
});
Object.defineProperties(THREE.WebGLRenderer.prototype, {
    supportsFloatTextures: {
        value: function() {
            return console.warn("THREE.WebGLRenderer: .supportsFloatTextures() is now .extensions.get( 'OES_texture_float' )."), this.extensions.get("OES_texture_float")
        }
    },
    supportsHalfFloatTextures: {
        value: function() {
            return console.warn("THREE.WebGLRenderer: .supportsHalfFloatTextures() is now .extensions.get( 'OES_texture_half_float' )."), this.extensions.get("OES_texture_half_float")
        }
    },
    supportsStandardDerivatives: {
        value: function() {
            return console.warn("THREE.WebGLRenderer: .supportsStandardDerivatives() is now .extensions.get( 'OES_standard_derivatives' )."), this.extensions.get("OES_standard_derivatives")
        }
    },
    supportsCompressedTextureS3TC: {
        value: function() {
            return console.warn("THREE.WebGLRenderer: .supportsCompressedTextureS3TC() is now .extensions.get( 'WEBGL_compressed_texture_s3tc' )."), this.extensions.get("WEBGL_compressed_texture_s3tc")
        }
    },
    supportsCompressedTexturePVRTC: {
        value: function() {
            return console.warn("THREE.WebGLRenderer: .supportsCompressedTexturePVRTC() is now .extensions.get( 'WEBGL_compressed_texture_pvrtc' )."), this.extensions.get("WEBGL_compressed_texture_pvrtc")
        }
    },
    supportsBlendMinMax: {
        value: function() {
            return console.warn("THREE.WebGLRenderer: .supportsBlendMinMax() is now .extensions.get( 'EXT_blend_minmax' )."), this.extensions.get("EXT_blend_minmax")
        }
    },
    supportsVertexTextures: {
        value: function() {
            return this.capabilities.vertexTextures
        }
    },
    supportsInstancedArrays: {
        value: function() {
            return console.warn("THREE.WebGLRenderer: .supportsInstancedArrays() is now .extensions.get( 'ANGLE_instanced_arrays' )."), this.extensions.get("ANGLE_instanced_arrays")
        }
    },
    enableScissorTest: {
        value: function(n) {
            console.warn("THREE.WebGLRenderer: .enableScissorTest() is now .setScissorTest().");
            this.setScissorTest(n)
        }
    },
    initMaterial: {
        value: function() {
            console.warn("THREE.WebGLRenderer: .initMaterial() has been removed.")
        }
    },
    addPrePlugin: {
        value: function() {
            console.warn("THREE.WebGLRenderer: .addPrePlugin() has been removed.")
        }
    },
    addPostPlugin: {
        value: function() {
            console.warn("THREE.WebGLRenderer: .addPostPlugin() has been removed.")
        }
    },
    updateShadowMap: {
        value: function() {
            console.warn("THREE.WebGLRenderer: .updateShadowMap() has been removed.")
        }
    },
    shadowMapEnabled: {
        get: function() {
            return this.shadowMap.enabled
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderer: .shadowMapEnabled is now .shadowMap.enabled.");
            this.shadowMap.enabled = n
        }
    },
    shadowMapType: {
        get: function() {
            return this.shadowMap.type
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderer: .shadowMapType is now .shadowMap.type.");
            this.shadowMap.type = n
        }
    },
    shadowMapCullFace: {
        get: function() {
            return this.shadowMap.cullFace
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderer: .shadowMapCullFace is now .shadowMap.cullFace.");
            this.shadowMap.cullFace = n
        }
    }
});
Object.defineProperties(THREE.WebGLRenderTarget.prototype, {
    wrapS: {
        get: function() {
            return console.warn("THREE.WebGLRenderTarget: .wrapS is now .texture.wrapS."), this.texture.wrapS
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderTarget: .wrapS is now .texture.wrapS.");
            this.texture.wrapS = n
        }
    },
    wrapT: {
        get: function() {
            return console.warn("THREE.WebGLRenderTarget: .wrapT is now .texture.wrapT."), this.texture.wrapT
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderTarget: .wrapT is now .texture.wrapT.");
            this.texture.wrapT = n
        }
    },
    magFilter: {
        get: function() {
            return console.warn("THREE.WebGLRenderTarget: .magFilter is now .texture.magFilter."), this.texture.magFilter
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderTarget: .magFilter is now .texture.magFilter.");
            this.texture.magFilter = n
        }
    },
    minFilter: {
        get: function() {
            return console.warn("THREE.WebGLRenderTarget: .minFilter is now .texture.minFilter."), this.texture.minFilter
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderTarget: .minFilter is now .texture.minFilter.");
            this.texture.minFilter = n
        }
    },
    anisotropy: {
        get: function() {
            return console.warn("THREE.WebGLRenderTarget: .anisotropy is now .texture.anisotropy."), this.texture.anisotropy
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderTarget: .anisotropy is now .texture.anisotropy.");
            this.texture.anisotropy = n
        }
    },
    offset: {
        get: function() {
            return console.warn("THREE.WebGLRenderTarget: .offset is now .texture.offset."), this.texture.offset
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderTarget: .offset is now .texture.offset.");
            this.texture.offset = n
        }
    },
    repeat: {
        get: function() {
            return console.warn("THREE.WebGLRenderTarget: .repeat is now .texture.repeat."), this.texture.repeat
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderTarget: .repeat is now .texture.repeat.");
            this.texture.repeat = n
        }
    },
    format: {
        get: function() {
            return console.warn("THREE.WebGLRenderTarget: .format is now .texture.format."), this.texture.format
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderTarget: .format is now .texture.format.");
            this.texture.format = n
        }
    },
    type: {
        get: function() {
            return console.warn("THREE.WebGLRenderTarget: .type is now .texture.type."), this.texture.type
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderTarget: .type is now .texture.type.");
            this.texture.type = n
        }
    },
    generateMipmaps: {
        get: function() {
            return console.warn("THREE.WebGLRenderTarget: .generateMipmaps is now .texture.generateMipmaps."), this.texture.generateMipmaps
        },
        set: function(n) {
            console.warn("THREE.WebGLRenderTarget: .generateMipmaps is now .texture.generateMipmaps.");
            this.texture.generateMipmaps = n
        }
    }
});
Object.defineProperties(THREE.Audio.prototype, {
    load: {
        value: function(n) {
            console.warn("THREE.Audio: .load has been deprecated. Please use THREE.AudioLoader.");
            var t = this,
                i = new THREE.AudioLoader;
            return i.load(n, function(n) {
                t.setBuffer(n)
            }), this
        }
    }
});
THREE.GeometryUtils = {
    merge: function(n, t, i) {
        console.warn("THREE.GeometryUtils: .merge() has been moved to Geometry. Use geometry.merge( geometry2, matrix, materialIndexOffset ) instead.");
        var r;
        t instanceof THREE.Mesh && (t.matrixAutoUpdate && t.updateMatrix(), r = t.matrix, t = t.geometry);
        n.merge(t, r, i)
    },
    center: function(n) {
        return console.warn("THREE.GeometryUtils: .center() has been moved to Geometry. Use geometry.center() instead."), n.center()
    }
};
THREE.ImageUtils = {
    crossOrigin: undefined,
    loadTexture: function(n, t, i, r) {
        var u, f;
        return console.warn("THREE.ImageUtils.loadTexture has been deprecated. Use THREE.TextureLoader() instead."), u = new THREE.TextureLoader, u.setCrossOrigin(this.crossOrigin), f = u.load(n, i, undefined, r), t && (f.mapping = t), f
    },
    loadTextureCube: function(n, t, i, r) {
        var u, f;
        return console.warn("THREE.ImageUtils.loadTextureCube has been deprecated. Use THREE.CubeTextureLoader() instead."), u = new THREE.CubeTextureLoader, u.setCrossOrigin(this.crossOrigin), f = u.load(n, i, undefined, r), t && (f.mapping = t), f
    },
    loadCompressedTexture: function() {
        console.error("THREE.ImageUtils.loadCompressedTexture has been removed. Use THREE.DDSLoader instead.")
    },
    loadCompressedTextureCube: function() {
        console.error("THREE.ImageUtils.loadCompressedTextureCube has been removed. Use THREE.DDSLoader instead.")
    }
};
THREE.Projector = function() {
    console.error("THREE.Projector has been moved to /examples/js/renderers/Projector.js.");
    this.projectVector = function(n, t) {
        console.warn("THREE.Projector: .projectVector() is now vector.project().");
        n.project(t)
    };
    this.unprojectVector = function(n, t) {
        console.warn("THREE.Projector: .unprojectVector() is now vector.unproject().");
        n.unproject(t)
    };
    this.pickingRay = function() {
        console.error("THREE.Projector: .pickingRay() is now raycaster.setFromCamera().")
    }
};
THREE.CanvasRenderer = function() {
    console.error("THREE.CanvasRenderer has been moved to /examples/js/renderers/CanvasRenderer.js");
    this.domElement = document.createElement("canvas");
    this.clear = function() {};
    this.render = function() {};
    this.setClearColor = function() {};
    this.setSize = function() {}
};
THREE.MeshFaceMaterial = THREE.MultiMaterial;
Object.defineProperties(THREE.LOD.prototype, {
    objects: {
        get: function() {
            return console.warn("THREE.LOD: .objects has been renamed to .levels."), this.levels
        }
    }
});
THREE.CurveUtils = {
    tangentQuadraticBezier: function(n, t, i, r) {
        return 2 * (1 - n) * (i - t) + 2 * n * (r - i)
    },
    tangentCubicBezier: function(n, t, i, r, u) {
        return -3 * t * (1 - n) * (1 - n) + 3 * i * (1 - n) * (1 - n) - 6 * n * i * (1 - n) + 6 * n * r * (1 - n) - 3 * n * n * r + 3 * n * n * u
    },
    tangentSpline: function(n) {
        var t = 6 * n * n - 6 * n,
            i = 3 * n * n - 4 * n + 1,
            r = -6 * n * n + 6 * n,
            u = 3 * n * n - 2 * n;
        return t + i + r + u
    },
    interpolate: function(n, t, i, r, u) {
        var f = (i - n) * .5,
            e = (r - t) * .5,
            o = u * u,
            s = u * o;
        return (2 * t - 2 * i + f + e) * s + (-3 * t + 3 * i - 2 * f - e) * o + f * u + t
    }
};
THREE.SceneUtils = {
    createMultiMaterialObject: function(n, t) {
        for (var r = new THREE.Group, i = 0, u = t.length; i < u; i++) r.add(new THREE.Mesh(n, t[i]));
        return r
    },
    detach: function(n, t, i) {
        n.applyMatrix(t.matrixWorld);
        t.remove(n);
        i.add(n)
    },
    attach: function(n, t, i) {
        var r = new THREE.Matrix4;
        r.getInverse(i.matrixWorld);
        n.applyMatrix(r);
        t.remove(n);
        i.add(n)
    }
};
THREE.ShapeUtils = {
    area: function(n) {
        for (var r = n.length, u = 0, i = r - 1, t = 0; t < r; i = t++) u += n[i].x * n[t].y - n[t].x * n[i].y;
        return u * .5
    },
    triangulate: function() {
        function n(n, t, i, r, u, f) {
            var y, e, o, c, l, a, v, s, h, p, w, b, k, d, g, nt, tt, it, rt, ut, ft, et, ot, st;
            if (e = n[f[t]].x, o = n[f[t]].y, c = n[f[i]].x, l = n[f[i]].y, a = n[f[r]].x, v = n[f[r]].y, Number.EPSILON > (c - e) * (v - o) - (l - o) * (a - e)) return !1;
            for (p = a - c, w = v - l, b = e - a, k = o - v, d = c - e, g = l - o, y = 0; y < u; y++)
                if ((s = n[f[y]].x, h = n[f[y]].y, (s !== e || h !== o) && (s !== c || h !== l) && (s !== a || h !== v)) && (nt = s - e, tt = h - o, it = s - c, rt = h - l, ut = s - a, ft = h - v, st = p * rt - w * it, et = d * tt - g * nt, ot = b * ft - k * ut, st >= -Number.EPSILON && ot >= -Number.EPSILON && et >= -Number.EPSILON)) return !1;
            return !0
        }
        return function(t, i) {
            var s = t.length,
                f, a, y, p, w, v, h;
            if (s < 3) return null;
            var c = [],
                u = [],
                l = [],
                e, r, o;
            if (THREE.ShapeUtils.area(t) > 0)
                for (r = 0; r < s; r++) u[r] = r;
            else
                for (r = 0; r < s; r++) u[r] = s - 1 - r;
            for (f = s, a = 2 * f, r = f - 1; f > 2;) {
                if (a-- <= 0) return (console.warn("THREE.ShapeUtils: Unable to triangulate polygon! in triangulate()"), i) ? l : c;
                if (e = r, f <= e && (e = 0), r = e + 1, f <= r && (r = 0), o = r + 1, f <= o && (o = 0), n(t, e, r, o, f, u)) {
                    for (y = u[e], p = u[r], w = u[o], c.push([t[y], t[p], t[w]]), l.push([u[e], u[r], u[o]]), v = r, h = r + 1; h < f; v++, h++) u[v] = u[h];
                    f--;
                    a = 2 * f
                }
            }
            return i ? l : c
        }
    }(),
    triangulateShape: function(n, t) {
        function a(n, t, i) {
            return n.x !== t.x ? n.x < t.x ? n.x <= i.x && i.x <= t.x : t.x <= i.x && i.x <= n.x : n.y < t.y ? n.y <= i.y && i.y <= t.y : t.y <= i.y && i.y <= n.y
        }

        function v(n, t, i, r, u) {
            var b = t.x - n.x,
                g = t.y - n.y,
                k = r.x - i.x,
                d = r.y - i.y,
                nt = n.x - i.x,
                tt = n.y - i.y,
                f = g * k - b * d,
                e = g * nt - b * tt,
                o, it, rt, ut, c, p, l, s, v, w, y, h;
            if (Math.abs(f) > Number.EPSILON) {
                if (f > 0) {
                    if (e < 0 || e > f) return [];
                    if (o = d * nt - k * tt, o < 0 || o > f) return []
                } else {
                    if (e > 0 || e < f) return [];
                    if (o = d * nt - k * tt, o > 0 || o < f) return []
                }
                return o === 0 ? u && (e === 0 || e === f) ? [] : [n] : o === f ? u && (e === 0 || e === f) ? [] : [t] : e === 0 ? [i] : e === f ? [r] : (it = o / f, [{
                    x: n.x + it * b,
                    y: n.y + it * g
                }])
            }
            return e !== 0 || d * nt != k * tt ? [] : (rt = b === 0 && g === 0, ut = k === 0 && d === 0, rt && ut) ? n.x !== i.x || n.y !== i.y ? [] : [n] : rt ? a(i, r, n) ? [n] : [] : ut ? a(n, t, i) ? [i] : [] : (b !== 0 ? (n.x < t.x ? (c = n, l = n.x, p = t, s = t.x) : (c = t, l = t.x, p = n, s = n.x), i.x < r.x ? (v = i, y = i.x, w = r, h = r.x) : (v = r, y = r.x, w = i, h = i.x)) : (n.y < t.y ? (c = n, l = n.y, p = t, s = t.y) : (c = t, l = t.y, p = n, s = n.y), i.y < r.y ? (v = i, y = i.y, w = r, h = r.y) : (v = r, y = r.y, w = i, h = i.y)), l <= y ? s < y ? [] : s === y ? u ? [] : [v] : s <= h ? [v, p] : [v, w] : l > h ? [] : l === h ? u ? [] : [c] : s <= h ? [c, p] : [c, w])
        }

        function y(n, t, i, r) {
            var e = t.x - n.x,
                o = t.y - n.y,
                s = i.x - n.x,
                h = i.y - n.y,
                c = r.x - n.x,
                l = r.y - n.y,
                a = e * h - o * s,
                u = e * l - o * c,
                f;
            return Math.abs(a) > Number.EPSILON ? (f = c * h - l * s, a > 0 ? u >= 0 && f >= 0 : u >= 0 || f >= 0) : u > 0
        }

        function b(n, t) {
            function it(n, t) {
                var c = i.length - 1,
                    s = n - 1,
                    u, f, h, e, o;
                return (s < 0 && (s = c), u = n + 1, u > c && (u = 0), f = y(i[n], i[s], i[u], r[t]), !f) ? !1 : (h = r.length - 1, e = t - 1, e < 0 && (e = h), o = t + 1, o > h && (o = 0), f = y(r[t], r[e], r[o], i[n]), !f) ? !1 : !0
            }

            function rt(n, t) {
                for (var u, f, r = 0; r < i.length; r++)
                    if (u = r + 1, u %= i.length, f = v(n, t, i[r], i[u], !0), f.length > 0) return !0;
                return !1
            }

            function ut(n, i) {
                for (var r, u, o, s, e = 0; e < f.length; e++)
                    for (r = t[f[e]], u = 0; u < r.length; u++)
                        if (o = u + 1, o %= r.length, s = v(n, i, r[u], r[o], !0), s.length > 0) return !0;
                return !1
            }
            for (var i = n.concat(), r, f = [], o, e, h, c, l, a, b = [], k, d, g, nt, p, w, s, u = 0, tt = t.length; u < tt; u++) f.push(u);
            for (p = 0, w = f.length * 2; f.length > 0;) {
                if (w--, w < 0) {
                    console.log("Infinite Loop! Holes left:" + f.length + ", Probably Hole outside Shape!");
                    break
                }
                for (e = p; e < i.length; e++) {
                    for (h = i[e], o = -1, u = 0; u < f.length; u++)
                        if (l = f[u], a = h.x + ":" + h.y + ":" + l, b[a] === undefined) {
                            for (r = t[l], s = 0; s < r.length; s++)
                                if ((c = r[s], it(e, s)) && !rt(h, c) && !ut(h, c)) {
                                    o = s;
                                    f.splice(u, 1);
                                    k = i.slice(0, e + 1);
                                    d = i.slice(e);
                                    g = r.slice(o);
                                    nt = r.slice(0, o + 1);
                                    i = k.concat(g).concat(nt).concat(d);
                                    p = e;
                                    break
                                }
                            if (o >= 0) break;
                            b[a] = !0
                        }
                    if (o >= 0) break
                }
            }
            return i
        }
        for (var i, f, r, e, u, c, l = {}, o = n.concat(), w, h, s = 0, p = t.length; s < p; s++) Array.prototype.push.apply(o, t[s]);
        for (i = 0, f = o.length; i < f; i++) u = o[i].x + ":" + o[i].y, l[u] !== undefined && console.warn("THREE.Shape: Duplicate point", u), l[u] = i;
        for (w = b(n, t), h = THREE.ShapeUtils.triangulate(w, !1), i = 0, f = h.length; i < f; i++)
            for (e = h[i], r = 0; r < 3; r++) u = e[r].x + ":" + e[r].y, c = l[u], c !== undefined && (e[r] = c);
        return h.concat()
    },
    isClockWise: function(n) {
        return THREE.ShapeUtils.area(n) < 0
    },
    b2: function() {
        function n(n, t) {
            var i = 1 - n;
            return i * i * t
        }

        function t(n, t) {
            return 2 * (1 - n) * n * t
        }

        function i(n, t) {
            return n * n * t
        }
        return function(r, u, f, e) {
            return n(r, u) + t(r, f) + i(r, e)
        }
    }(),
    b3: function() {
        function n(n, t) {
            var i = 1 - n;
            return i * i * i * t
        }

        function t(n, t) {
            var i = 1 - n;
            return 3 * i * i * n * t
        }

        function i(n, t) {
            var i = 1 - n;
            return 3 * i * n * n * t
        }

        function r(n, t) {
            return n * n * n * t
        }
        return function(u, f, e, o, s) {
            return n(u, f) + t(u, e) + i(u, o) + r(u, s)
        }
    }()
};
THREE.Curve = function() {};
THREE.Curve.prototype = {
    constructor: THREE.Curve,
    getPoint: function() {
        return console.warn("THREE.Curve: Warning, getPoint() not implemented!"), null
    },
    getPointAt: function(n) {
        var t = this.getUtoTmapping(n);
        return this.getPoint(t)
    },
    getPoints: function(n) {
        n || (n = 5);
        for (var i = [], t = 0; t <= n; t++) i.push(this.getPoint(t / n));
        return i
    },
    getSpacedPoints: function(n) {
        n || (n = 5);
        for (var i = [], t = 0; t <= n; t++) i.push(this.getPointAt(t / n));
        return i
    },
    getLength: function() {
        var n = this.getLengths();
        return n[n.length - 1]
    },
    getLengths: function(n) {
        if (n || (n = this.__arcLengthDivisions ? this.__arcLengthDivisions : 200), this.cacheArcLengths && this.cacheArcLengths.length === n + 1 && !this.needsUpdate) return this.cacheArcLengths;
        this.needsUpdate = !1;
        var t = [],
            r, u = this.getPoint(0),
            i, f = 0;
        for (t.push(0), i = 1; i <= n; i++) r = this.getPoint(i / n), f += r.distanceTo(u), t.push(f), u = r;
        return this.cacheArcLengths = t, t
    },
    updateArcLengths: function() {
        this.needsUpdate = !0;
        this.getLengths()
    },
    getUtoTmapping: function(n, t) {
        var r = this.getLengths(),
            i = 0,
            e = r.length,
            o, f, u, s, h;
        for (o = t ? t : n * r[e - 1], f = 0, u = e - 1; f <= u;)
            if (i = Math.floor(f + (u - f) / 2), s = r[i] - o, s < 0) f = i + 1;
            else if (s > 0) u = i - 1;
        else {
            u = i;
            break
        }
        if (i = u, r[i] === o) return i / (e - 1);
        var c = r[i],
            l = r[i + 1],
            a = l - c,
            v = (o - c) / a,
            h = (i + v) / (e - 1);
        return h
    },
    getTangent: function(n) {
        var r = .0001,
            t = n - r,
            i = n + r;
        t < 0 && (t = 0);
        i > 1 && (i = 1);
        var u = this.getPoint(t),
            f = this.getPoint(i),
            e = f.clone().sub(u);
        return e.normalize()
    },
    getTangentAt: function(n) {
        var t = this.getUtoTmapping(n);
        return this.getTangent(t)
    }
};
THREE.Curve.create = function(n, t) {
    return n.prototype = Object.create(THREE.Curve.prototype), n.prototype.constructor = n, n.prototype.getPoint = t, n
};
THREE.CurvePath = function() {
    this.curves = [];
    this.autoClose = !1
};
THREE.CurvePath.prototype = Object.create(THREE.Curve.prototype);
THREE.CurvePath.prototype.constructor = THREE.CurvePath;
THREE.CurvePath.prototype.add = function(n) {
    this.curves.push(n)
};
THREE.CurvePath.prototype.closePath = function() {
    var n = this.curves[0].getPoint(0),
        t = this.curves[this.curves.length - 1].getPoint(1);
    n.equals(t) || this.curves.push(new THREE.LineCurve(t, n))
};
THREE.CurvePath.prototype.getPoint = function(n) {
    for (var r = n * this.getLength(), i = this.getCurveLengths(), t = 0; t < i.length;) {
        if (i[t] >= r) {
            var f = i[t] - r,
                u = this.curves[t],
                e = 1 - f / u.getLength();
            return u.getPointAt(e)
        }
        t++
    }
    return null
};
THREE.CurvePath.prototype.getLength = function() {
    var n = this.getCurveLengths();
    return n[n.length - 1]
};
THREE.CurvePath.prototype.getCurveLengths = function() {
    var n, i, t, r;
    if (this.cacheLengths && this.cacheLengths.length === this.curves.length) return this.cacheLengths;
    for (n = [], i = 0, t = 0, r = this.curves.length; t < r; t++) i += this.curves[t].getLength(), n.push(i);
    return this.cacheLengths = n, n
};
THREE.CurvePath.prototype.createPointsGeometry = function(n) {
    var t = this.getPoints(n);
    return this.createGeometry(t)
};
THREE.CurvePath.prototype.createSpacedPointsGeometry = function(n) {
    var t = this.getSpacedPoints(n);
    return this.createGeometry(t)
};
THREE.CurvePath.prototype.createGeometry = function(n) {
    for (var r = new THREE.Geometry, i, t = 0, u = n.length; t < u; t++) i = n[t], r.vertices.push(new THREE.Vector3(i.x, i.y, i.z || 0));
    return r
};
THREE.Font = function(n) {
    this.data = n
};
THREE.Font.prototype = {
    constructor: THREE.Font,
    generateShapes: function(n, t, i) {
        function s(n) {
            for (var f = String(n).split(""), s = t / u.resolution, e = 0, o = [], r, i = 0; i < f.length; i++) r = h(f[i], s, e), e += r.offset, o.push(r.path);
            return o
        }

        function h(n, t, r) {
            var c = u.glyphs[n] || u.glyphs["?"],
                f, e, ut, ft, o, h;
            if (c) {
                var p = new THREE.Path,
                    w = [],
                    it = THREE.ShapeUtils.b2,
                    rt = THREE.ShapeUtils.b3,
                    b, k, l, a, d, g, v, y, nt, tt, s;
                if (c.o)
                    for (f = c._cachedOutline || (c._cachedOutline = c.o.split(" ")), e = 0, ut = f.length; e < ut;) {
                        ft = f[e++];
                        switch (ft) {
                            case "m":
                                b = f[e++] * t + r;
                                k = f[e++] * t;
                                p.moveTo(b, k);
                                break;
                            case "l":
                                b = f[e++] * t + r;
                                k = f[e++] * t;
                                p.lineTo(b, k);
                                break;
                            case "q":
                                if (l = f[e++] * t + r, a = f[e++] * t, v = f[e++] * t + r, y = f[e++] * t, p.quadraticCurveTo(v, y, l, a), s = w[w.length - 1], s)
                                    for (d = s.x, g = s.y, o = 1; o <= i; o++) h = o / i, it(h, d, v, l), it(h, g, y, a);
                                break;
                            case "b":
                                if (l = f[e++] * t + r, a = f[e++] * t, v = f[e++] * t + r, y = f[e++] * t, nt = f[e++] * t + r, tt = f[e++] * t, p.bezierCurveTo(v, y, nt, tt, l, a), s = w[w.length - 1], s)
                                    for (d = s.x, g = s.y, o = 1; o <= i; o++) h = o / i, rt(h, d, v, nt, l), rt(h, g, y, tt, a)
                        }
                    }
                return {
                    offset: c.ha * t,
                    path: p
                }
            }
        }
        var r, o;
        t === undefined && (t = 100);
        i === undefined && (i = 4);
        var u = this.data,
            f = s(n),
            e = [];
        for (r = 0, o = f.length; r < o; r++) Array.prototype.push.apply(e, f[r].toShapes());
        return e
    }
};
THREE.Path = function(n) {
    THREE.CurvePath.call(this);
    this.actions = [];
    n && this.fromPoints(n)
};
THREE.Path.prototype = Object.create(THREE.CurvePath.prototype);
THREE.Path.prototype.constructor = THREE.Path;
THREE.Path.prototype.fromPoints = function(n) {
    this.moveTo(n[0].x, n[0].y);
    for (var t = 1, i = n.length; t < i; t++) this.lineTo(n[t].x, n[t].y)
};
THREE.Path.prototype.moveTo = function(n, t) {
    this.actions.push({
        action: "moveTo",
        args: [n, t]
    })
};
THREE.Path.prototype.lineTo = function(n, t) {
    var i = this.actions[this.actions.length - 1].args,
        r = i[i.length - 2],
        u = i[i.length - 1],
        f = new THREE.LineCurve(new THREE.Vector2(r, u), new THREE.Vector2(n, t));
    this.curves.push(f);
    this.actions.push({
        action: "lineTo",
        args: [n, t]
    })
};
THREE.Path.prototype.quadraticCurveTo = function(n, t, i, r) {
    var u = this.actions[this.actions.length - 1].args,
        f = u[u.length - 2],
        e = u[u.length - 1],
        o = new THREE.QuadraticBezierCurve(new THREE.Vector2(f, e), new THREE.Vector2(n, t), new THREE.Vector2(i, r));
    this.curves.push(o);
    this.actions.push({
        action: "quadraticCurveTo",
        args: [n, t, i, r]
    })
};
THREE.Path.prototype.bezierCurveTo = function(n, t, i, r, u, f) {
    var e = this.actions[this.actions.length - 1].args,
        o = e[e.length - 2],
        s = e[e.length - 1],
        h = new THREE.CubicBezierCurve(new THREE.Vector2(o, s), new THREE.Vector2(n, t), new THREE.Vector2(i, r), new THREE.Vector2(u, f));
    this.curves.push(h);
    this.actions.push({
        action: "bezierCurveTo",
        args: [n, t, i, r, u, f]
    })
};
THREE.Path.prototype.splineThru = function(n) {
    var u = Array.prototype.slice.call(arguments),
        t = this.actions[this.actions.length - 1].args,
        f = t[t.length - 2],
        e = t[t.length - 1],
        i = [new THREE.Vector2(f, e)],
        r;
    Array.prototype.push.apply(i, n);
    r = new THREE.SplineCurve(i);
    this.curves.push(r);
    this.actions.push({
        action: "splineThru",
        args: u
    })
};
THREE.Path.prototype.arc = function(n, t, i, r, u, f) {
    var e = this.actions[this.actions.length - 1].args,
        o = e[e.length - 2],
        s = e[e.length - 1];
    this.absarc(n + o, t + s, i, r, u, f)
};
THREE.Path.prototype.absarc = function(n, t, i, r, u, f) {
    this.absellipse(n, t, i, i, r, u, f)
};
THREE.Path.prototype.ellipse = function(n, t, i, r, u, f, e, o) {
    var s = this.actions[this.actions.length - 1].args,
        h = s[s.length - 2],
        c = s[s.length - 1];
    this.absellipse(n + h, t + c, i, r, u, f, e, o)
};
THREE.Path.prototype.absellipse = function(n, t, i, r, u, f, e, o) {
    var s = [n, t, i, r, u, f, e, o || 0],
        c = new THREE.EllipseCurve(n, t, i, r, u, f, e, o),
        h;
    this.curves.push(c);
    h = c.getPoint(1);
    s.push(h.x);
    s.push(h.y);
    this.actions.push({
        action: "ellipse",
        args: s
    })
};
THREE.Path.prototype.getSpacedPoints = function(n) {
    var t, i;
    for (n || (n = 40), t = [], i = 0; i < n; i++) t.push(this.getPoint(i / n));
    return this.autoClose && t.push(t[0]), t
};
THREE.Path.prototype.getPoints = function(n) {
    var s, vt, wt, i, f, et, ot, st;
    n = n || 12;
    var ht = THREE.ShapeUtils.b2,
        ct = THREE.ShapeUtils.b3,
        r = [],
        w, b, lt, at, k, d, h, c, u, e, o;
    for (s = 0, vt = this.actions.length; s < vt; s++) {
        var yt = this.actions[s],
            kt = yt.action,
            t = yt.args;
        switch (kt) {
            case "moveTo":
                r.push(new THREE.Vector2(t[0], t[1]));
                break;
            case "lineTo":
                r.push(new THREE.Vector2(t[0], t[1]));
                break;
            case "quadraticCurveTo":
                for (w = t[2], b = t[3], k = t[0], d = t[1], r.length > 0 ? (u = r[r.length - 1], h = u.x, c = u.y) : (u = this.actions[s - 1].args, h = u[u.length - 2], c = u[u.length - 1]), i = 1; i <= n; i++) f = i / n, e = ht(f, h, k, w), o = ht(f, c, d, b), r.push(new THREE.Vector2(e, o));
                break;
            case "bezierCurveTo":
                for (w = t[4], b = t[5], k = t[0], d = t[1], lt = t[2], at = t[3], r.length > 0 ? (u = r[r.length - 1], h = u.x, c = u.y) : (u = this.actions[s - 1].args, h = u[u.length - 2], c = u[u.length - 1]), i = 1; i <= n; i++) f = i / n, e = ct(f, h, k, lt, w), o = ct(f, c, d, at, b), r.push(new THREE.Vector2(e, o));
                break;
            case "splineThru":
                u = this.actions[s - 1].args;
                var dt = new THREE.Vector2(u[u.length - 2], u[u.length - 1]),
                    nt = [dt],
                    pt = n * t[0].length;
                for (nt = nt.concat(t[0]), wt = new THREE.SplineCurve(nt), i = 1; i <= pt; i++) r.push(wt.getPointAt(i / pt));
                break;
            case "arc":
                var l = t[0],
                    a = t[1],
                    bt = t[2],
                    y = t[3],
                    tt = t[4],
                    it = !!t[5],
                    rt = tt - y,
                    v, p = n * 2;
                for (i = 1; i <= p; i++) f = i / p, it || (f = 1 - f), v = y + f * rt, e = l + bt * Math.cos(v), o = a + bt * Math.sin(v), r.push(new THREE.Vector2(e, o));
                break;
            case "ellipse":
                var l = t[0],
                    a = t[1],
                    gt = t[2],
                    ni = t[3],
                    y = t[4],
                    tt = t[5],
                    it = !!t[6],
                    g = t[7],
                    rt = tt - y,
                    v, p = n * 2,
                    ut, ft;
                for (g !== 0 && (ut = Math.cos(g), ft = Math.sin(g)), i = 1; i <= p; i++) f = i / p, it || (f = 1 - f), v = y + f * rt, e = l + gt * Math.cos(v), o = a + ni * Math.sin(v), g !== 0 && (et = e, ot = o, e = (et - l) * ut - (ot - a) * ft + l, o = (et - l) * ft + (ot - a) * ut + a), r.push(new THREE.Vector2(e, o))
        }
    }
    return st = r[r.length - 1], Math.abs(st.x - r[0].x) < Number.EPSILON && Math.abs(st.y - r[0].y) < Number.EPSILON && r.splice(r.length - 1, 1), this.autoClose && r.push(r[0]), r
};
THREE.Path.prototype.toShapes = function(n, t) {
    function ht(n) {
        for (var r = [], t = new THREE.Path, i = 0, u = n.length; i < u; i++) {
            var f = n[i],
                o = f.args,
                e = f.action;
            e === "moveTo" && t.actions.length !== 0 && (r.push(t), t = new THREE.Path);
            t[e].apply(t, o)
        }
        return t.actions.length !== 0 && r.push(t), r
    }

    function ut(n) {
        for (var u = [], r, i, t = 0, f = n.length; t < f; t++) r = n[t], i = new THREE.Shape, i.actions = r.actions, i.curves = r.curves, u.push(i);
        return u
    }

    function ct(n, t) {
        for (var c = t.length, o = !1, h, e = c - 1, u = 0; u < c; e = u++) {
            var i = t[e],
                r = t[u],
                s = r.x - i.x,
                f = r.y - i.y;
            if (Math.abs(f) > Number.EPSILON) {
                if (f < 0 && (i = t[u], s = -s, r = t[e], f = -f), n.y < i.y || n.y > r.y) continue;
                if (n.y === i.y) {
                    if (n.x === i.x) return !0
                } else {
                    if (h = f * (n.x - i.x) - s * (n.y - i.y), h === 0) return !0;
                    if (h < 0) continue;
                    o = !o
                }
            } else {
                if (n.y !== i.y) continue;
                if (r.x <= n.x && n.x <= i.x || i.x <= n.x && n.x <= r.x) return !0
            }
        }
        return o
    }
    var ft = THREE.ShapeUtils.isClockWise,
        e = ht(this.actions),
        a, o, s, v, c, et, nt, tt, r, y, it, p, k, d, h, rt, f, ot, g, st;
    if (e.length === 0) return [];
    if (t === !0) return ut(e);
    if (v = [], e.length === 1) return o = e[0], s = new THREE.Shape, s.actions = o.actions, s.curves = o.curves, v.push(s), v;
    c = !ft(e[0].getPoints());
    c = n ? !c : c;
    var w = [],
        i = [],
        l = [],
        u = 0,
        b;
    for (i[u] = undefined, l[u] = [], f = 0, et = e.length; f < et; f++) o = e[f], b = o.getPoints(), a = ft(b), a = n ? !a : a, a ? (!c && i[u] && u++, i[u] = {
        s: new THREE.Shape,
        p: b
    }, i[u].s.actions = o.actions, i[u].s.curves = o.curves, c && u++, l[u] = []) : l[u].push({
        h: o,
        p: b[0]
    });
    if (!i[0]) return ut(e);
    if (i.length > 1) {
        for (nt = !1, tt = [], r = 0, y = i.length; r < y; r++) w[r] = [];
        for (r = 0, y = i.length; r < y; r++)
            for (it = l[r], p = 0; p < it.length; p++) {
                for (k = it[p], d = !0, h = 0; h < i.length; h++) ct(k.p, i[h].p) && (r !== h && tt.push({
                    froms: r,
                    tos: h,
                    hole: p
                }), d ? (d = !1, w[h].push(k)) : nt = !0);
                d && w[r].push(k)
            }
        tt.length > 0 && (nt || (l = w))
    }
    for (f = 0, ot = i.length; f < ot; f++)
        for (s = i[f].s, v.push(s), rt = l[f], g = 0, st = rt.length; g < st; g++) s.holes.push(rt[g].h);
    return v
};
THREE.Shape = function() {
    THREE.Path.apply(this, arguments);
    this.holes = []
};
THREE.Shape.prototype = Object.create(THREE.Path.prototype);
THREE.Shape.prototype.constructor = THREE.Shape;
THREE.Shape.prototype.extrude = function(n) {
    return new THREE.ExtrudeGeometry(this, n)
};
THREE.Shape.prototype.makeGeometry = function(n) {
    return new THREE.ShapeGeometry(this, n)
};
THREE.Shape.prototype.getPointsHoles = function(n) {
    for (var i = [], t = 0, r = this.holes.length; t < r; t++) i[t] = this.holes[t].getPoints(n);
    return i
};
THREE.Shape.prototype.extractAllPoints = function(n) {
    return {
        shape: this.getPoints(n),
        holes: this.getPointsHoles(n)
    }
};
THREE.Shape.prototype.extractPoints = function(n) {
    return this.extractAllPoints(n)
};
THREE.LineCurve = function(n, t) {
    this.v1 = n;
    this.v2 = t
};
THREE.LineCurve.prototype = Object.create(THREE.Curve.prototype);
THREE.LineCurve.prototype.constructor = THREE.LineCurve;
THREE.LineCurve.prototype.getPoint = function(n) {
    var t = this.v2.clone().sub(this.v1);
    return t.multiplyScalar(n).add(this.v1), t
};
THREE.LineCurve.prototype.getPointAt = function(n) {
    return this.getPoint(n)
};
THREE.LineCurve.prototype.getTangent = function() {
    var n = this.v2.clone().sub(this.v1);
    return n.normalize()
};
THREE.QuadraticBezierCurve = function(n, t, i) {
    this.v0 = n;
    this.v1 = t;
    this.v2 = i
};
THREE.QuadraticBezierCurve.prototype = Object.create(THREE.Curve.prototype);
THREE.QuadraticBezierCurve.prototype.constructor = THREE.QuadraticBezierCurve;
THREE.QuadraticBezierCurve.prototype.getPoint = function(n) {
    var t = THREE.ShapeUtils.b2;
    return new THREE.Vector2(t(n, this.v0.x, this.v1.x, this.v2.x), t(n, this.v0.y, this.v1.y, this.v2.y))
};
THREE.QuadraticBezierCurve.prototype.getTangent = function(n) {
    var t = THREE.CurveUtils.tangentQuadraticBezier;
    return new THREE.Vector2(t(n, this.v0.x, this.v1.x, this.v2.x), t(n, this.v0.y, this.v1.y, this.v2.y)).normalize()
};
THREE.CubicBezierCurve = function(n, t, i, r) {
    this.v0 = n;
    this.v1 = t;
    this.v2 = i;
    this.v3 = r
};
THREE.CubicBezierCurve.prototype = Object.create(THREE.Curve.prototype);
THREE.CubicBezierCurve.prototype.constructor = THREE.CubicBezierCurve;
THREE.CubicBezierCurve.prototype.getPoint = function(n) {
    var t = THREE.ShapeUtils.b3;
    return new THREE.Vector2(t(n, this.v0.x, this.v1.x, this.v2.x, this.v3.x), t(n, this.v0.y, this.v1.y, this.v2.y, this.v3.y))
};
THREE.CubicBezierCurve.prototype.getTangent = function(n) {
    var t = THREE.CurveUtils.tangentCubicBezier;
    return new THREE.Vector2(t(n, this.v0.x, this.v1.x, this.v2.x, this.v3.x), t(n, this.v0.y, this.v1.y, this.v2.y, this.v3.y)).normalize()
};
THREE.SplineCurve = function(n) {
    this.points = n == undefined ? [] : n
};
THREE.SplineCurve.prototype = Object.create(THREE.Curve.prototype);
THREE.SplineCurve.prototype.constructor = THREE.SplineCurve;
THREE.SplineCurve.prototype.getPoint = function(n) {
    var t = this.points,
        r = (t.length - 1) * n,
        i = Math.floor(r),
        u = r - i,
        f = t[i === 0 ? i : i - 1],
        e = t[i],
        o = t[i > t.length - 2 ? t.length - 1 : i + 1],
        s = t[i > t.length - 3 ? t.length - 1 : i + 2],
        h = THREE.CurveUtils.interpolate;
    return new THREE.Vector2(h(f.x, e.x, o.x, s.x, u), h(f.y, e.y, o.y, s.y, u))
};
THREE.EllipseCurve = function(n, t, i, r, u, f, e, o) {
    this.aX = n;
    this.aY = t;
    this.xRadius = i;
    this.yRadius = r;
    this.aStartAngle = u;
    this.aEndAngle = f;
    this.aClockwise = e;
    this.aRotation = o || 0
};
THREE.EllipseCurve.prototype = Object.create(THREE.Curve.prototype);
THREE.EllipseCurve.prototype.constructor = THREE.EllipseCurve;
THREE.EllipseCurve.prototype.getPoint = function(n) {
    var t = this.aEndAngle - this.aStartAngle,
        u, i, r;
    if (t < 0 && (t += Math.PI * 2), t > Math.PI * 2 && (t -= Math.PI * 2), u = this.aClockwise === !0 ? this.aEndAngle + (1 - n) * (Math.PI * 2 - t) : this.aStartAngle + n * t, i = this.aX + this.xRadius * Math.cos(u), r = this.aY + this.yRadius * Math.sin(u), this.aRotation !== 0) {
        var f = Math.cos(this.aRotation),
            e = Math.sin(this.aRotation),
            o = i,
            s = r;
        i = (o - this.aX) * f - (s - this.aY) * e + this.aX;
        r = (o - this.aX) * e + (s - this.aY) * f + this.aY
    }
    return new THREE.Vector2(i, r)
};
THREE.ArcCurve = function(n, t, i, r, u, f) {
    THREE.EllipseCurve.call(this, n, t, i, i, r, u, f)
};
THREE.ArcCurve.prototype = Object.create(THREE.EllipseCurve.prototype);
THREE.ArcCurve.prototype.constructor = THREE.ArcCurve;
THREE.LineCurve3 = THREE.Curve.create(function(n, t) {
    this.v1 = n;
    this.v2 = t
}, function(n) {
    var t = new THREE.Vector3;
    return t.subVectors(this.v2, this.v1), t.multiplyScalar(n), t.add(this.v1), t
});
THREE.QuadraticBezierCurve3 = THREE.Curve.create(function(n, t, i) {
    this.v0 = n;
    this.v1 = t;
    this.v2 = i
}, function(n) {
    var t = THREE.ShapeUtils.b2;
    return new THREE.Vector3(t(n, this.v0.x, this.v1.x, this.v2.x), t(n, this.v0.y, this.v1.y, this.v2.y), t(n, this.v0.z, this.v1.z, this.v2.z))
});
THREE.CubicBezierCurve3 = THREE.Curve.create(function(n, t, i, r) {
    this.v0 = n;
    this.v1 = t;
    this.v2 = i;
    this.v3 = r
}, function(n) {
    var t = THREE.ShapeUtils.b3;
    return new THREE.Vector3(t(n, this.v0.x, this.v1.x, this.v2.x, this.v3.x), t(n, this.v0.y, this.v1.y, this.v2.y, this.v3.y), t(n, this.v0.z, this.v1.z, this.v2.z, this.v3.z))
});
THREE.SplineCurve3 = THREE.Curve.create(function(n) {
    console.warn("THREE.SplineCurve3 will be deprecated. Please use THREE.CatmullRomCurve3");
    this.points = n == undefined ? [] : n
}, function(n) {
    var t = this.points,
        h = (t.length - 1) * n,
        i = Math.floor(h),
        r = h - i,
        u = t[i == 0 ? i : i - 1],
        f = t[i],
        e = t[i > t.length - 2 ? t.length - 1 : i + 1],
        o = t[i > t.length - 3 ? t.length - 1 : i + 2],
        s = THREE.CurveUtils.interpolate;
    return new THREE.Vector3(s(u.x, f.x, e.x, o.x, r), s(u.y, f.y, e.y, o.y, r), s(u.z, f.z, e.z, o.z, r))
});
THREE.CatmullRomCurve3 = function() {
    function n() {}
    var t = new THREE.Vector3,
        i = new n,
        r = new n,
        u = new n;
    return n.prototype.init = function(n, t, i, r) {
        this.c0 = n;
        this.c1 = i;
        this.c2 = -3 * n + 3 * t - 2 * i - r;
        this.c3 = 2 * n - 2 * t + i + r
    }, n.prototype.initNonuniformCatmullRom = function(n, t, i, r, u, f, e) {
        var o = (t - n) / u - (i - n) / (u + f) + (i - t) / f,
            s = (i - t) / f - (r - t) / (f + e) + (r - i) / e;
        o *= f;
        s *= f;
        this.init(t, i, o, s)
    }, n.prototype.initCatmullRom = function(n, t, i, r, u) {
        this.init(t, i, u * (i - n), u * (r - t))
    }, n.prototype.calc = function(n) {
        var t = n * n,
            i = t * n;
        return this.c0 + this.c1 * n + this.c2 * t + this.c3 * i
    }, THREE.Curve.create(function(n) {
        this.points = n || [];
        this.closed = !1
    }, function(n) {
        var f = this.points,
            b, e, v, o, s, h, c, l, w;
        if (o = f.length, o < 2 && console.log("duh, you need at least 2 points"), b = (o - (this.closed ? 0 : 1)) * n, e = Math.floor(b), v = b - e, this.closed ? e += e > 0 ? 0 : (Math.floor(Math.abs(e) / f.length) + 1) * f.length : v === 0 && e === o - 1 && (e = o - 2, v = 1), this.closed || e > 0 ? s = f[(e - 1) % o] : (t.subVectors(f[0], f[1]).add(f[0]), s = t), h = f[e % o], c = f[(e + 1) % o], this.closed || e + 2 < o ? l = f[(e + 2) % o] : (t.subVectors(f[o - 1], f[o - 2]).add(f[o - 1]), l = t), this.type === undefined || this.type === "centripetal" || this.type === "chordal") {
            var k = this.type === "chordal" ? .5 : .25,
                y = Math.pow(s.distanceToSquared(h), k),
                a = Math.pow(h.distanceToSquared(c), k),
                p = Math.pow(c.distanceToSquared(l), k);
            a < .0001 && (a = 1);
            y < .0001 && (y = a);
            p < .0001 && (p = a);
            i.initNonuniformCatmullRom(s.x, h.x, c.x, l.x, y, a, p);
            r.initNonuniformCatmullRom(s.y, h.y, c.y, l.y, y, a, p);
            u.initNonuniformCatmullRom(s.z, h.z, c.z, l.z, y, a, p)
        } else this.type === "catmullrom" && (w = this.tension !== undefined ? this.tension : .5, i.initCatmullRom(s.x, h.x, c.x, l.x, w), r.initCatmullRom(s.y, h.y, c.y, l.y, w), u.initCatmullRom(s.z, h.z, c.z, l.z, w));
        return new THREE.Vector3(i.calc(v), r.calc(v), u.calc(v))
    })
}();
THREE.ClosedSplineCurve3 = function(n) {
    console.warn("THREE.ClosedSplineCurve3 has been deprecated. Please use THREE.CatmullRomCurve3.");
    THREE.CatmullRomCurve3.call(this, n);
    this.type = "catmullrom";
    this.closed = !0
};
THREE.ClosedSplineCurve3.prototype = Object.create(THREE.CatmullRomCurve3.prototype);
THREE.BoxGeometry = function(n, t, i, r, u, f) {
    THREE.Geometry.call(this);
    this.type = "BoxGeometry";
    this.parameters = {
        width: n,
        height: t,
        depth: i,
        widthSegments: r,
        heightSegments: u,
        depthSegments: f
    };
    this.fromBufferGeometry(new THREE.BoxBufferGeometry(n, t, i, r, u, f));
    this.mergeVertices()
};
THREE.BoxGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.BoxGeometry.prototype.constructor = THREE.BoxGeometry;
THREE.CubeGeometry = THREE.BoxGeometry;
THREE.BoxBufferGeometry = function(n, t, i, r, u, f) {
    function d(n, t, i) {
        var r = 0;
        return r += n * t * 2, r += n * i * 2, r += i * t * 2, r * 4
    }

    function h(n, t, i, r, u, f, h, l, b, d, g) {
        for (var ct = f / b, lt = h / d, at = f / 2, vt = h / 2, yt = l / 2, rt = b + 1, pt = d + 1, ft = 0, ut = 0, nt = new THREE.Vector3, et, it, ot, tt = 0; tt < pt; tt++)
            for (et = tt * lt - vt, it = 0; it < rt; it++) ot = it * ct - at, nt[n] = ot * r, nt[t] = et * u, nt[i] = yt, a[o] = nt.x, a[o + 1] = nt.y, a[o + 2] = nt.z, nt[n] = 0, nt[t] = 0, nt[i] = l > 0 ? 1 : -1, v[o] = nt.x, v[o + 1] = nt.y, v[o + 2] = nt.z, y[p] = it / b, y[p + 1] = 1 - tt / d, o += 3, p += 2, ft += 1;
        for (tt = 0; tt < d; tt++)
            for (it = 0; it < b; it++) {
                var wt = c + it + rt * tt,
                    st = c + it + rt * (tt + 1),
                    bt = c + (it + 1) + rt * (tt + 1),
                    ht = c + (it + 1) + rt * tt;
                e[s] = wt;
                e[s + 1] = st;
                e[s + 2] = ht;
                e[s + 3] = st;
                e[s + 4] = bt;
                e[s + 5] = ht;
                s += 6;
                ut += 6
            }
        w.addGroup(k, ut, g);
        k += ut;
        c += ft
    }
    var w;
    THREE.BufferGeometry.call(this);
    this.type = "BoxBufferGeometry";
    this.parameters = {
        width: n,
        height: t,
        depth: i,
        widthSegments: r,
        heightSegments: u,
        depthSegments: f
    };
    w = this;
    r = Math.floor(r) || 1;
    u = Math.floor(u) || 1;
    f = Math.floor(f) || 1;
    var l = d(r, u, f),
        b = l / 4 * 6,
        e = new(b > 65535 ? Uint32Array : Uint16Array)(b),
        a = new Float32Array(l * 3),
        v = new Float32Array(l * 3),
        y = new Float32Array(l * 2),
        o = 0,
        p = 0,
        s = 0,
        c = 0,
        k = 0;
    h("z", "y", "x", -1, -1, i, t, n, f, u, 0);
    h("z", "y", "x", 1, -1, i, t, -n, f, u, 1);
    h("x", "z", "y", 1, 1, n, i, t, r, f, 2);
    h("x", "z", "y", 1, -1, n, i, -t, r, f, 3);
    h("x", "y", "z", 1, -1, n, t, i, r, u, 4);
    h("x", "y", "z", -1, -1, n, t, -i, r, u, 5);
    this.setIndex(new THREE.BufferAttribute(e, 1));
    this.addAttribute("position", new THREE.BufferAttribute(a, 3));
    this.addAttribute("normal", new THREE.BufferAttribute(v, 3));
    this.addAttribute("uv", new THREE.BufferAttribute(y, 2))
};
THREE.BoxBufferGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.BoxBufferGeometry.prototype.constructor = THREE.BoxBufferGeometry;
THREE.CircleGeometry = function(n, t, i, r) {
    THREE.Geometry.call(this);
    this.type = "CircleGeometry";
    this.parameters = {
        radius: n,
        segments: t,
        thetaStart: i,
        thetaLength: r
    };
    this.fromBufferGeometry(new THREE.CircleBufferGeometry(n, t, i, r))
};
THREE.CircleGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.CircleGeometry.prototype.constructor = THREE.CircleGeometry;
THREE.CircleBufferGeometry = function(n, t, i, r) {
    var l, a, u;
    THREE.BufferGeometry.call(this);
    this.type = "CircleBufferGeometry";
    this.parameters = {
        radius: n,
        segments: t,
        thetaStart: i,
        thetaLength: r
    };
    n = n || 50;
    t = t !== undefined ? Math.max(3, t) : 8;
    i = i !== undefined ? i : 0;
    r = r !== undefined ? r : Math.PI * 2;
    var o = t + 2,
        f = new Float32Array(o * 3),
        s = new Float32Array(o * 3),
        e = new Float32Array(o * 2);
    s[2] = 1;
    e[0] = .5;
    e[1] = .5;
    for (var h = 0, u = 3, c = 2; h <= t; h++, u += 3, c += 2) l = i + h / t * r, f[u] = n * Math.cos(l), f[u + 1] = n * Math.sin(l), s[u + 2] = 1, e[c] = (f[u] / n + 1) / 2, e[c + 1] = (f[u + 1] / n + 1) / 2;
    for (a = [], u = 1; u <= t; u++) a.push(u, u + 1, 0);
    this.setIndex(new THREE.BufferAttribute(new Uint16Array(a), 1));
    this.addAttribute("position", new THREE.BufferAttribute(f, 3));
    this.addAttribute("normal", new THREE.BufferAttribute(s, 3));
    this.addAttribute("uv", new THREE.BufferAttribute(e, 2));
    this.boundingSphere = new THREE.Sphere(new THREE.Vector3, n)
};
THREE.CircleBufferGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.CircleBufferGeometry.prototype.constructor = THREE.CircleBufferGeometry;
THREE.CylinderBufferGeometry = function(n, t, i, r, u, f, e, o) {
    function nt() {
        var n = (r + 1) * (u + 1);
        return f === !1 && (n += (r + 1) * 2 + r * 2), n
    }

    function tt() {
        var n = r * u * 6;
        return f === !1 && (n += r * 6), n
    }

    function it() {
        for (var b, d = new THREE.Vector3, g = new THREE.Vector3, tt = 0, ot = (t - n) / i, nt, f = 0; f <= u; f++) {
            var rt = [],
                it = f / u,
                ut = it * (t - n) + n;
            for (b = 0; b <= r; b++) nt = b / r, g.x = ut * Math.sin(nt * o + e), g.y = -it * i + k, g.z = ut * Math.cos(nt * o + e), a.setXYZ(h, g.x, g.y, g.z), d.copy(g), (n === 0 && f === 0 || t === 0 && f === u) && (d.x = Math.sin(nt * o + e), d.z = Math.cos(nt * o + e)), d.setY(Math.sqrt(d.x * d.x + d.z * d.z) * ot).normalize(), v.setXYZ(h, d.x, d.y, d.z), y.setXY(h, nt, 1 - it), rt.push(h), h++;
            l.push(rt)
        }
        for (b = 0; b < r; b++)
            for (f = 0; f < u; f++) {
                var st = l[f][b],
                    ft = l[f + 1][b],
                    ht = l[f + 1][b + 1],
                    et = l[f][b + 1];
                c.setX(s, st);
                s++;
                c.setX(s, ft);
                s++;
                c.setX(s, et);
                s++;
                c.setX(s, ft);
                s++;
                c.setX(s, ht);
                s++;
                c.setX(s, et);
                s++;
                tt += 6
            }
        w.addGroup(p, tt, 0);
        p += tt
    }

    function g(i) {
        for (var rt, f = new THREE.Vector2, l = new THREE.Vector3, nt = 0, ut = i === !0 ? n : t, d = i === !0 ? 1 : -1, g, tt, b, it = h, u = 1; u <= r; u++) a.setXYZ(h, 0, k * d, 0), v.setXYZ(h, 0, d, 0), i === !0 ? (f.x = u / r, f.y = 0) : (f.x = (u - 1) / r, f.y = 1), y.setXY(h, f.x, f.y), h++;
        for (rt = h, u = 0; u <= r; u++) g = u / r, l.x = ut * Math.sin(g * o + e), l.y = k * d, l.z = ut * Math.cos(g * o + e), a.setXYZ(h, l.x, l.y, l.z), v.setXYZ(h, 0, d, 0), y.setXY(h, g, i === !0 ? 1 : 0), h++;
        for (u = 0; u < r; u++) tt = it + u, b = rt + u, i === !0 ? (c.setX(s, b), s++, c.setX(s, b + 1), s++, c.setX(s, tt), s++) : (c.setX(s, b + 1), s++, c.setX(s, b), s++, c.setX(s, tt), s++), nt += 3;
        w.addGroup(p, nt, i === !0 ? 1 : 2);
        p += nt
    }
    var w;
    THREE.BufferGeometry.call(this);
    this.type = "CylinderBufferGeometry";
    this.parameters = {
        radiusTop: n,
        radiusBottom: t,
        height: i,
        radialSegments: r,
        heightSegments: u,
        openEnded: f,
        thetaStart: e,
        thetaLength: o
    };
    w = this;
    n = n !== undefined ? n : 20;
    t = t !== undefined ? t : 20;
    i = i !== undefined ? i : 100;
    r = Math.floor(r) || 8;
    u = Math.floor(u) || 1;
    f = f !== undefined ? f : !1;
    e = e !== undefined ? e : 0;
    o = o !== undefined ? o : 2 * Math.PI;
    var b = nt(),
        d = tt(),
        c = new THREE.BufferAttribute(new(d > 65535 ? Uint32Array : Uint16Array)(d), 1),
        a = new THREE.BufferAttribute(new Float32Array(b * 3), 3),
        v = new THREE.BufferAttribute(new Float32Array(b * 3), 3),
        y = new THREE.BufferAttribute(new Float32Array(b * 2), 2),
        h = 0,
        s = 0,
        l = [],
        k = i / 2,
        p = 0;
    it();
    f === !1 && (n > 0 && g(!0), t > 0 && g(!1));
    this.setIndex(c);
    this.addAttribute("position", a);
    this.addAttribute("normal", v);
    this.addAttribute("uv", y)
};
THREE.CylinderBufferGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.CylinderBufferGeometry.prototype.constructor = THREE.CylinderBufferGeometry;
THREE.CylinderGeometry = function(n, t, i, r, u, f, e, o) {
    THREE.Geometry.call(this);
    this.type = "CylinderGeometry";
    this.parameters = {
        radiusTop: n,
        radiusBottom: t,
        height: i,
        radialSegments: r,
        heightSegments: u,
        openEnded: f,
        thetaStart: e,
        thetaLength: o
    };
    this.fromBufferGeometry(new THREE.CylinderBufferGeometry(n, t, i, r, u, f, e, o));
    this.mergeVertices()
};
THREE.CylinderGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.CylinderGeometry.prototype.constructor = THREE.CylinderGeometry;
THREE.EdgesGeometry = function(n, t) {
    function b(n, t) {
        return n - t
    }
    var a, u, v, c, e, p, y, l, i, o, s, r;
    THREE.BufferGeometry.call(this);
    t = t !== undefined ? t : 1;
    var w = Math.cos(THREE.Math.DEG2RAD * t),
        f = [0, 0],
        h = {};
    for (a = ["a", "b", "c"], n instanceof THREE.BufferGeometry ? (u = new THREE.Geometry, u.fromBufferGeometry(n)) : u = n.clone(), u.mergeVertices(), u.computeFaceNormals(), v = u.vertices, c = u.faces, e = 0, p = c.length; e < p; e++)
        for (y = c[e], l = 0; l < 3; l++) f[0] = y[a[l]], f[1] = y[a[(l + 1) % 3]], f.sort(b), o = f.toString(), h[o] === undefined ? h[o] = {
            vert1: f[0],
            vert2: f[1],
            face1: e,
            face2: undefined
        } : h[o].face2 = e;
    i = [];
    for (o in h) s = h[o], (s.face2 === undefined || c[s.face1].normal.dot(c[s.face2].normal) <= w) && (r = v[s.vert1], i.push(r.x), i.push(r.y), i.push(r.z), r = v[s.vert2], i.push(r.x), i.push(r.y), i.push(r.z));
    this.addAttribute("position", new THREE.BufferAttribute(new Float32Array(i), 3))
};
THREE.EdgesGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.EdgesGeometry.prototype.constructor = THREE.EdgesGeometry;
THREE.ExtrudeGeometry = function(n, t) {
    if (typeof n == "undefined") {
        n = [];
        return
    }
    THREE.Geometry.call(this);
    this.type = "ExtrudeGeometry";
    n = Array.isArray(n) ? n : [n];
    this.addShapeList(n, t);
    this.computeFaceNormals()
};
THREE.ExtrudeGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.ExtrudeGeometry.prototype.constructor = THREE.ExtrudeGeometry;
THREE.ExtrudeGeometry.prototype.addShapeList = function(n, t) {
    for (var u = n.length, r, i = 0; i < u; i++) r = n[i], this.addShape(r, t)
};
THREE.ExtrudeGeometry.prototype.addShape = function(n, t) {
    function et(n, t, i) {
        return t || console.error("THREE.ExtrudeGeometry: vec does not exist"), t.clone().multiplyScalar(i).add(n)
    }

    function fi(n, t, i) {
        var e, o, h = 1,
            r = n.x - t.x,
            u = n.y - t.y,
            f = i.x - n.x,
            s = i.y - n.y,
            l = r * r + u * u,
            k = r * s - u * f,
            a, c;
        if (Math.abs(k) > Number.EPSILON) {
            var v = Math.sqrt(l),
                y = Math.sqrt(f * f + s * s),
                p = t.x - u / v,
                w = t.y + r / v,
                d = i.x - s / y,
                g = i.y + f / y,
                b = ((d - p) * s - (g - w) * f) / (r * s - u * f);
            if (e = p + r * b - n.x, o = w + u * b - n.y, a = e * e + o * o, a <= 2) return new THREE.Vector2(e, o);
            h = Math.sqrt(a / 2)
        } else c = !1, r > Number.EPSILON ? f > Number.EPSILON && (c = !0) : r < -Number.EPSILON ? f < -Number.EPSILON && (c = !0) : Math.sign(u) === Math.sign(s) && (c = !0), c ? (e = -u, o = r, h = Math.sqrt(l)) : (e = r, o = u, h = Math.sqrt(l / 2));
        return new THREE.Vector2(e / h, o / h)
    }

    function si() {
        if (st) {
            var t = 0,
                n = v * t;
            for (i = 0; i < vt; i++) o = ft[i], gt(o[2] + n, o[1] + n, o[0] + n);
            for (t = l + tt * 2, n = v * t, i = 0; i < vt; i++) o = ft[i], gt(o[0] + n, o[1] + n, o[2] + n)
        } else {
            for (i = 0; i < vt; i++) o = ft[i], gt(o[2], o[1], o[0]);
            for (i = 0; i < vt; i++) o = ft[i], gt(o[0] + v * l, o[1] + v * l, o[2] + v * l)
        }
    }

    function hi() {
        var n = 0;
        for (ei(a, n), n += a.length, r = 0, h = s.length; r < h; r++) f = s[r], ei(f, n), n += f.length
    }

    function ei(n, t) {
        var f, u, r, e;
        for (i = n.length; --i >= 0;)
            for (f = i, u = i - 1, u < 0 && (u = n.length - 1), r = 0, e = l + tt * 2, r = 0; r < e; r++) {
                var o = v * r,
                    s = v * (r + 1),
                    h = t + f + o,
                    c = t + u + o,
                    a = t + u + s,
                    y = t + f + s;
                ci(h, c, a, y, n, r, e, f, u)
            }
    }

    function d(n, t, i) {
        p.vertices.push(new THREE.Vector3(n, t, i))
    }

    function gt(n, t, i) {
        n += it;
        t += it;
        i += it;
        p.faces.push(new THREE.Face3(n, t, i, null, null, 0));
        var r = ii.generateTopUV(p, n, t, i);
        p.faceVertexUvs[0].push(r)
    }

    function ci(n, t, i, r) {
        n += it;
        t += it;
        i += it;
        r += it;
        p.faces.push(new THREE.Face3(n, t, r, null, null, 1));
        p.faces.push(new THREE.Face3(t, i, r, null, null, 1));
        var u = ii.generateSideWallUV(p, n, t, i, r);
        p.faceVertexUvs[0].push([u[0], u[1], u[3]]);
        p.faceVertexUvs[0].push([u[1], u[2], u[3]])
    }
    var ni = t.amount !== undefined ? t.amount : 100,
        wt = t.bevelThickness !== undefined ? t.bevelThickness : 6,
        bt = t.bevelSize !== undefined ? t.bevelSize : wt - 2,
        tt = t.bevelSegments !== undefined ? t.bevelSegments : 3,
        st = t.bevelEnabled !== undefined ? t.bevelEnabled : !0,
        oi = t.curveSegments !== undefined ? t.curveSegments : 12,
        l = t.steps !== undefined ? t.steps : 1,
        ti = t.extrudePath,
        ht, kt = !1,
        ii = t.UVGenerator !== undefined ? t.UVGenerator : THREE.ExtrudeGeometry.WorldUVGenerator,
        ct, lt, at, y, ft, a, g, w, ot, rt, u, v, o, vt, yt, dt, nt, pt, ut;
    ti && (ht = ti.getSpacedPoints(l), kt = !0, st = !1, ct = t.frames !== undefined ? t.frames : new THREE.TubeGeometry.FrenetFrames(ti, l, !1), lt = new THREE.Vector3, at = new THREE.Vector3, y = new THREE.Vector3);
    st || (tt = 0, wt = 0, bt = 0);
    var f, r, h, p = this,
        it = this.vertices.length,
        ri = n.extractPoints(oi),
        c = ri.shape,
        s = ri.holes,
        ui = !THREE.ShapeUtils.isClockWise(c);
    if (ui) {
        for (c = c.reverse(), r = 0, h = s.length; r < h; r++) f = s[r], THREE.ShapeUtils.isClockWise(f) && (s[r] = f.reverse());
        ui = !1
    }
    for (ft = THREE.ShapeUtils.triangulateShape(c, s), a = c, r = 0, h = s.length; r < h; r++) f = s[r], c = c.concat(f);
    v = c.length;
    vt = ft.length;
    yt = [];
    for (var i = 0, e = a.length, b = e - 1, k = i + 1; i < e; i++, b++, k++) b === e && (b = 0), k === e && (k = 0), yt[i] = fi(a[i], a[b], a[k]);
    for (dt = [], pt = yt.concat(), r = 0, h = s.length; r < h; r++) {
        for (f = s[r], nt = [], i = 0, e = f.length, b = e - 1, k = i + 1; i < e; i++, b++, k++) b === e && (b = 0), k === e && (k = 0), nt[i] = fi(f[i], f[b], f[k]);
        dt.push(nt);
        pt = pt.concat(nt)
    }
    for (g = 0; g < tt; g++) {
        for (ot = g / tt, rt = wt * (1 - ot), w = bt * Math.sin(ot * Math.PI / 2), i = 0, e = a.length; i < e; i++) u = et(a[i], yt[i], w), d(u.x, u.y, -rt);
        for (r = 0, h = s.length; r < h; r++)
            for (f = s[r], nt = dt[r], i = 0, e = f.length; i < e; i++) u = et(f[i], nt[i], w), d(u.x, u.y, -rt)
    }
    for (w = bt, i = 0; i < v; i++) u = st ? et(c[i], pt[i], w) : c[i], kt ? (at.copy(ct.normals[0]).multiplyScalar(u.x), lt.copy(ct.binormals[0]).multiplyScalar(u.y), y.copy(ht[0]).add(at).add(lt), d(y.x, y.y, y.z)) : d(u.x, u.y, 0);
    for (ut = 1; ut <= l; ut++)
        for (i = 0; i < v; i++) u = st ? et(c[i], pt[i], w) : c[i], kt ? (at.copy(ct.normals[ut]).multiplyScalar(u.x), lt.copy(ct.binormals[ut]).multiplyScalar(u.y), y.copy(ht[ut]).add(at).add(lt), d(y.x, y.y, y.z)) : d(u.x, u.y, ni / l * ut);
    for (g = tt - 1; g >= 0; g--) {
        for (ot = g / tt, rt = wt * (1 - ot), w = bt * Math.sin(ot * Math.PI / 2), i = 0, e = a.length; i < e; i++) u = et(a[i], yt[i], w), d(u.x, u.y, ni + rt);
        for (r = 0, h = s.length; r < h; r++)
            for (f = s[r], nt = dt[r], i = 0, e = f.length; i < e; i++) u = et(f[i], nt[i], w), kt ? d(u.x, u.y + ht[l - 1].y, ht[l - 1].x + rt) : d(u.x, u.y, ni + rt)
    }
    si();
    hi()
};
THREE.ExtrudeGeometry.WorldUVGenerator = {
    generateTopUV: function(n, t, i, r) {
        var u = n.vertices,
            f = u[t],
            e = u[i],
            o = u[r];
        return [new THREE.Vector2(f.x, f.y), new THREE.Vector2(e.x, e.y), new THREE.Vector2(o.x, o.y)]
    },
    generateSideWallUV: function(n, t, i, r, u) {
        var o = n.vertices,
            f = o[t],
            e = o[i],
            s = o[r],
            h = o[u];
        return Math.abs(f.y - e.y) < .01 ? [new THREE.Vector2(f.x, 1 - f.z), new THREE.Vector2(e.x, 1 - e.z), new THREE.Vector2(s.x, 1 - s.z), new THREE.Vector2(h.x, 1 - h.z)] : [new THREE.Vector2(f.y, 1 - f.z), new THREE.Vector2(e.y, 1 - e.z), new THREE.Vector2(s.y, 1 - s.z), new THREE.Vector2(h.y, 1 - h.z)]
    }
};
THREE.ShapeGeometry = function(n, t) {
    THREE.Geometry.call(this);
    this.type = "ShapeGeometry";
    Array.isArray(n) === !1 && (n = [n]);
    this.addShapeList(n, t);
    this.computeFaceNormals()
};
THREE.ShapeGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.ShapeGeometry.prototype.constructor = THREE.ShapeGeometry;
THREE.ShapeGeometry.prototype.addShapeList = function(n, t) {
    for (var i = 0, r = n.length; i < r; i++) this.addShape(n[i], t);
    return this
};
THREE.ShapeGeometry.prototype.addShape = function(n, t) {
    var h, c, v, o, y;
    t === undefined && (t = {});
    var k = t.curveSegments !== undefined ? t.curveSegments : 12,
        d = t.material,
        g = t.UVGenerator === undefined ? THREE.ExtrudeGeometry.WorldUVGenerator : t.UVGenerator,
        i, e, f, s = this.vertices.length,
        l = n.extractPoints(k),
        r = l.shape,
        u = l.holes,
        a = !THREE.ShapeUtils.isClockWise(r);
    if (a) {
        for (r = r.reverse(), i = 0, e = u.length; i < e; i++) f = u[i], THREE.ShapeUtils.isClockWise(f) && (u[i] = f.reverse());
        a = !1
    }
    for (h = THREE.ShapeUtils.triangulateShape(r, u), i = 0, e = u.length; i < e; i++) f = u[i], r = r.concat(f);
    for (v = r.length, y = h.length, i = 0; i < v; i++) c = r[i], this.vertices.push(new THREE.Vector3(c.x, c.y, 0));
    for (i = 0; i < y; i++) {
        o = h[i];
        var p = o[0] + s,
            w = o[1] + s,
            b = o[2] + s;
        this.faces.push(new THREE.Face3(p, w, b, null, null, d));
        this.faceVertexUvs[0].push(g.generateTopUV(this, p, w, b))
    }
};
THREE.LatheBufferGeometry = function(n, t, i, r) {
    THREE.BufferGeometry.call(this);
    this.type = "LatheBufferGeometry";
    this.parameters = {
        points: n,
        segments: t,
        phiStart: i,
        phiLength: r
    };
    t = Math.floor(t) || 12;
    i = i || 0;
    r = r || Math.PI * 2;
    r = THREE.Math.clamp(r, 0, Math.PI * 2);
    for (var w = (t + 1) * n.length, b = t * n.length * 6, h = new THREE.BufferAttribute(new(b > 65535 ? Uint32Array : Uint16Array)(b), 1), k = new THREE.BufferAttribute(new Float32Array(w * 3), 3), d = new THREE.BufferAttribute(new Float32Array(w * 2), 2), p = 0, f = 0, e, ot = 1 / (n.length - 1), it = 1 / t, c = new THREE.Vector3, l = new THREE.Vector2, u, o = 0; o <= t; o++) {
        var g = i + o * it * r,
            rt = Math.sin(g),
            ut = Math.cos(g);
        for (u = 0; u <= n.length - 1; u++) c.x = n[u].x * rt, c.y = n[u].y, c.z = n[u].x * ut, k.setXYZ(p, c.x, c.y, c.z), l.x = o / t, l.y = u / (n.length - 1), d.setXY(p, l.x, l.y), p++
    }
    for (o = 0; o < t; o++)
        for (u = 0; u < n.length - 1; u++) {
            e = u + o * n.length;
            var ft = e,
                nt = e + n.length,
                et = e + n.length + 1,
                tt = e + 1;
            h.setX(f, ft);
            f++;
            h.setX(f, nt);
            f++;
            h.setX(f, tt);
            f++;
            h.setX(f, nt);
            f++;
            h.setX(f, et);
            f++;
            h.setX(f, tt);
            f++
        }
    if (this.setIndex(h), this.addAttribute("position", k), this.addAttribute("uv", d), this.computeVertexNormals(), r === Math.PI * 2) {
        var s = this.attributes.normal.array,
            a = new THREE.Vector3,
            v = new THREE.Vector3,
            y = new THREE.Vector3;
        for (e = t * n.length * 3, o = 0, u = 0; o < n.length; o++, u += 3) a.x = s[u + 0], a.y = s[u + 1], a.z = s[u + 2], v.x = s[e + u + 0], v.y = s[e + u + 1], v.z = s[e + u + 2], y.addVectors(a, v).normalize(), s[u + 0] = s[e + u + 0] = y.x, s[u + 1] = s[e + u + 1] = y.y, s[u + 2] = s[e + u + 2] = y.z
    }
};
THREE.LatheBufferGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.LatheBufferGeometry.prototype.constructor = THREE.LatheBufferGeometry;
THREE.LatheGeometry = function(n, t, i, r) {
    THREE.Geometry.call(this);
    this.type = "LatheGeometry";
    this.parameters = {
        points: n,
        segments: t,
        phiStart: i,
        phiLength: r
    };
    this.fromBufferGeometry(new THREE.LatheBufferGeometry(n, t, i, r));
    this.mergeVertices()
};
THREE.LatheGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.LatheGeometry.prototype.constructor = THREE.LatheGeometry;
THREE.PlaneGeometry = function(n, t, i, r) {
    THREE.Geometry.call(this);
    this.type = "PlaneGeometry";
    this.parameters = {
        width: n,
        height: t,
        widthSegments: i,
        heightSegments: r
    };
    this.fromBufferGeometry(new THREE.PlaneBufferGeometry(n, t, i, r))
};
THREE.PlaneGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.PlaneGeometry.prototype.constructor = THREE.PlaneGeometry;
THREE.PlaneBufferGeometry = function(n, t, i, r) {
    var w, b, s, u, f;
    THREE.BufferGeometry.call(this);
    this.type = "PlaneBufferGeometry";
    this.parameters = {
        width: n,
        height: t,
        widthSegments: i,
        heightSegments: r
    };
    var g = n / 2,
        nt = t / 2,
        h = Math.floor(i) || 1,
        c = Math.floor(r) || 1,
        o = h + 1,
        l = c + 1,
        tt = n / h,
        it = t / c,
        a = new Float32Array(o * l * 3),
        p = new Float32Array(o * l * 3),
        v = new Float32Array(o * l * 2),
        e = 0,
        y = 0;
    for (u = 0; u < l; u++)
        for (w = u * it - nt, f = 0; f < o; f++) b = f * tt - g, a[e] = b, a[e + 1] = -w, p[e + 2] = 1, v[y] = f / h, v[y + 1] = 1 - u / c, e += 3, y += 2;
    for (e = 0, s = new(a.length / 3 > 65535 ? Uint32Array : Uint16Array)(h * c * 6), u = 0; u < c; u++)
        for (f = 0; f < h; f++) {
            var rt = f + o * u,
                k = f + o * (u + 1),
                ut = f + 1 + o * (u + 1),
                d = f + 1 + o * u;
            s[e] = rt;
            s[e + 1] = k;
            s[e + 2] = d;
            s[e + 3] = k;
            s[e + 4] = ut;
            s[e + 5] = d;
            e += 6
        }
    this.setIndex(new THREE.BufferAttribute(s, 1));
    this.addAttribute("position", new THREE.BufferAttribute(a, 3));
    this.addAttribute("normal", new THREE.BufferAttribute(p, 3));
    this.addAttribute("uv", new THREE.BufferAttribute(v, 2))
};
THREE.PlaneBufferGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.PlaneBufferGeometry.prototype.constructor = THREE.PlaneBufferGeometry;
THREE.RingBufferGeometry = function(n, t, i, r, u, f) {
    var g;
    THREE.BufferGeometry.call(this);
    this.type = "RingBufferGeometry";
    this.parameters = {
        innerRadius: n,
        outerRadius: t,
        thetaSegments: i,
        phiSegments: r,
        thetaStart: u,
        thetaLength: f
    };
    n = n || 20;
    t = t || 50;
    u = u !== undefined ? u : 0;
    f = f !== undefined ? f : Math.PI * 2;
    i = i !== undefined ? Math.max(3, i) : 8;
    r = r !== undefined ? Math.max(1, r) : 1;
    for (var y = (i + 1) * (r + 1), w = i * r * 6, h = new THREE.BufferAttribute(new(w > 65535 ? Uint32Array : Uint16Array)(w), 1), b = new THREE.BufferAttribute(new Float32Array(y * 3), 3), k = new THREE.BufferAttribute(new Float32Array(y * 3), 3), d = new THREE.BufferAttribute(new Float32Array(y * 2), 2), a = 0, e = 0, o, p = n, it = (t - n) / r, c = new THREE.Vector3, v = new THREE.Vector2, s, l = 0; l <= r; l++) {
        for (s = 0; s <= i; s++) o = u + s / i * f, c.x = p * Math.cos(o), c.y = p * Math.sin(o), b.setXYZ(a, c.x, c.y, c.z), k.setXYZ(a, 0, 0, 1), v.x = (c.x / t + 1) / 2, v.y = (c.y / t + 1) / 2, d.setXY(a, v.x, v.y), a++;
        p += it
    }
    for (l = 0; l < r; l++)
        for (g = l * (i + 1), s = 0; s < i; s++) {
            o = s + g;
            var nt = o,
                rt = o + i + 1,
                tt = o + i + 2,
                ut = o + 1;
            h.setX(e, nt);
            e++;
            h.setX(e, rt);
            e++;
            h.setX(e, tt);
            e++;
            h.setX(e, nt);
            e++;
            h.setX(e, tt);
            e++;
            h.setX(e, ut);
            e++
        }
    this.setIndex(h);
    this.addAttribute("position", b);
    this.addAttribute("normal", k);
    this.addAttribute("uv", d)
};
THREE.RingBufferGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.RingBufferGeometry.prototype.constructor = THREE.RingBufferGeometry;
THREE.RingGeometry = function(n, t, i, r, u, f) {
    THREE.Geometry.call(this);
    this.type = "RingGeometry";
    this.parameters = {
        innerRadius: n,
        outerRadius: t,
        thetaSegments: i,
        phiSegments: r,
        thetaStart: u,
        thetaLength: f
    };
    this.fromBufferGeometry(new THREE.RingBufferGeometry(n, t, i, r, u, f))
};
THREE.RingGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.RingGeometry.prototype.constructor = THREE.RingGeometry;
THREE.SphereGeometry = function(n, t, i, r, u, f, e) {
    THREE.Geometry.call(this);
    this.type = "SphereGeometry";
    this.parameters = {
        radius: n,
        widthSegments: t,
        heightSegments: i,
        phiStart: r,
        phiLength: u,
        thetaStart: f,
        thetaLength: e
    };
    this.fromBufferGeometry(new THREE.SphereBufferGeometry(n, t, i, r, u, f, e))
};
THREE.SphereGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.SphereGeometry.prototype.constructor = THREE.SphereGeometry;
THREE.SphereBufferGeometry = function(n, t, i, r, u, f, e) {
    var w, l, v, o, s;
    THREE.BufferGeometry.call(this);
    this.type = "SphereBufferGeometry";
    this.parameters = {
        radius: n,
        widthSegments: t,
        heightSegments: i,
        phiStart: r,
        phiLength: u,
        thetaStart: f,
        thetaLength: e
    };
    n = n || 50;
    t = Math.max(3, Math.floor(t) || 8);
    i = Math.max(2, Math.floor(i) || 6);
    r = r !== undefined ? r : 0;
    u = u !== undefined ? u : Math.PI * 2;
    f = f !== undefined ? f : 0;
    e = e !== undefined ? e : Math.PI;
    var ut = f + e,
        y = (t + 1) * (i + 1),
        p = new THREE.BufferAttribute(new Float32Array(y * 3), 3),
        k = new THREE.BufferAttribute(new Float32Array(y * 3), 3),
        d = new THREE.BufferAttribute(new Float32Array(y * 2), 2),
        h = 0,
        c = [],
        a = new THREE.Vector3;
    for (o = 0; o <= i; o++) {
        for (w = [], l = o / i, s = 0; s <= t; s++) {
            var b = s / t,
                g = -n * Math.cos(r + b * u) * Math.sin(f + l * e),
                nt = n * Math.cos(f + l * e),
                tt = n * Math.sin(r + b * u) * Math.sin(f + l * e);
            a.set(g, nt, tt).normalize();
            p.setXYZ(h, g, nt, tt);
            k.setXYZ(h, a.x, a.y, a.z);
            d.setXY(h, b, 1 - l);
            w.push(h);
            h++
        }
        c.push(w)
    }
    for (v = [], o = 0; o < i; o++)
        for (s = 0; s < t; s++) {
            var ft = c[o][s + 1],
                it = c[o][s],
                et = c[o + 1][s],
                rt = c[o + 1][s + 1];
            (o !== 0 || f > 0) && v.push(ft, it, rt);
            (o !== i - 1 || ut < Math.PI) && v.push(it, et, rt)
        }
    this.setIndex(new(p.count > 65535 ? THREE.Uint32Attribute : THREE.Uint16Attribute)(v, 1));
    this.addAttribute("position", p);
    this.addAttribute("normal", k);
    this.addAttribute("uv", d);
    this.boundingSphere = new THREE.Sphere(new THREE.Vector3, n)
};
THREE.SphereBufferGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.SphereBufferGeometry.prototype.constructor = THREE.SphereBufferGeometry;
THREE.TextGeometry = function(n, t) {
    var i, r;
    if (t = t || {}, i = t.font, i instanceof THREE.Font == !1) return console.error("THREE.TextGeometry: font parameter is not an instance of THREE.Font."), new THREE.Geometry;
    r = i.generateShapes(n, t.size, t.curveSegments);
    t.amount = t.height !== undefined ? t.height : 50;
    t.bevelThickness === undefined && (t.bevelThickness = 10);
    t.bevelSize === undefined && (t.bevelSize = 8);
    t.bevelEnabled === undefined && (t.bevelEnabled = !1);
    THREE.ExtrudeGeometry.call(this, r, t);
    this.type = "TextGeometry"
};
THREE.TextGeometry.prototype = Object.create(THREE.ExtrudeGeometry.prototype);
THREE.TextGeometry.prototype.constructor = THREE.TextGeometry;
THREE.TorusBufferGeometry = function(n, t, i, r, u) {
    var l, p;
    THREE.BufferGeometry.call(this);
    this.type = "TorusBufferGeometry";
    this.parameters = {
        radius: n,
        tube: t,
        radialSegments: i,
        tubularSegments: r,
        arc: u
    };
    n = n || 100;
    t = t || 40;
    i = Math.floor(i) || 8;
    r = Math.floor(r) || 6;
    u = u || Math.PI * 2;
    for (var w = (i + 1) * (r + 1), g = i * r * 6, o = new(g > 65535 ? Uint32Array : Uint16Array)(g), a = new Float32Array(w * 3), v = new Float32Array(w * 3), b = new Float32Array(w * 2), s = 0, k = 0, h = 0, d = new THREE.Vector3, c = new THREE.Vector3, y = new THREE.Vector3, e, f = 0; f <= i; f++)
        for (e = 0; e <= r; e++) l = e / r * u, p = f / i * Math.PI * 2, c.x = (n + t * Math.cos(p)) * Math.cos(l), c.y = (n + t * Math.cos(p)) * Math.sin(l), c.z = t * Math.sin(p), a[s] = c.x, a[s + 1] = c.y, a[s + 2] = c.z, d.x = n * Math.cos(l), d.y = n * Math.sin(l), y.subVectors(c, d).normalize(), v[s] = y.x, v[s + 1] = y.y, v[s + 2] = y.z, b[k] = e / r, b[k + 1] = f / i, s += 3, k += 2;
    for (f = 1; f <= i; f++)
        for (e = 1; e <= r; e++) {
            var it = (r + 1) * f + e - 1,
                nt = (r + 1) * (f - 1) + e - 1,
                rt = (r + 1) * (f - 1) + e,
                tt = (r + 1) * f + e;
            o[h] = it;
            o[h + 1] = nt;
            o[h + 2] = tt;
            o[h + 3] = nt;
            o[h + 4] = rt;
            o[h + 5] = tt;
            h += 6
        }
    this.setIndex(new THREE.BufferAttribute(o, 1));
    this.addAttribute("position", new THREE.BufferAttribute(a, 3));
    this.addAttribute("normal", new THREE.BufferAttribute(v, 3));
    this.addAttribute("uv", new THREE.BufferAttribute(b, 2))
};
THREE.TorusBufferGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.TorusBufferGeometry.prototype.constructor = THREE.TorusBufferGeometry;
THREE.TorusGeometry = function(n, t, i, r, u) {
    THREE.Geometry.call(this);
    this.type = "TorusGeometry";
    this.parameters = {
        radius: n,
        tube: t,
        radialSegments: i,
        tubularSegments: r,
        arc: u
    };
    this.fromBufferGeometry(new THREE.TorusBufferGeometry(n, t, i, r, u))
};
THREE.TorusGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.TorusGeometry.prototype.constructor = THREE.TorusGeometry;
THREE.TorusKnotBufferGeometry = function(n, t, i, r, u, f) {
    function ht(n, t, i, r, u) {
        var o = Math.cos(n),
            s = Math.sin(n),
            f = i / t * n,
            e = Math.cos(f);
        u.x = r * (2 + e) * .5 * o;
        u.y = r * (2 + e) * s * .5;
        u.z = r * Math.sin(f) * .5
    }
    var g;
    THREE.BufferGeometry.call(this);
    this.type = "TorusKnotBufferGeometry";
    this.parameters = {
        radius: n,
        tube: t,
        tubularSegments: i,
        radialSegments: r,
        p: u,
        q: f
    };
    n = n || 100;
    t = t || 40;
    i = Math.floor(i) || 64;
    r = Math.floor(r) || 8;
    u = u || 2;
    f = f || 3;
    for (var b = (r + 1) * (i + 1), it = r * i * 6, h = new THREE.BufferAttribute(new(it > 65535 ? Uint32Array : Uint16Array)(it), 1), rt = new THREE.BufferAttribute(new Float32Array(b * 3), 3), ut = new THREE.BufferAttribute(new Float32Array(b * 3), 3), ft = new THREE.BufferAttribute(new Float32Array(b * 2), 2), o, y = 0, s = 0, c = new THREE.Vector3, p = new THREE.Vector3, w = new THREE.Vector2, l = new THREE.Vector3, k = new THREE.Vector3, v = new THREE.Vector3, d = new THREE.Vector3, a = new THREE.Vector3, e = 0; e <= i; ++e)
        for (g = e / i * u * Math.PI * 2, ht(g, u, f, n, l), ht(g + .01, u, f, n, k), d.subVectors(k, l), a.addVectors(k, l), v.crossVectors(d, a), a.crossVectors(v, d), v.normalize(), a.normalize(), o = 0; o <= r; ++o) {
            var et = o / r * Math.PI * 2,
                nt = -t * Math.cos(et),
                tt = t * Math.sin(et);
            c.x = l.x + (nt * a.x + tt * v.x);
            c.y = l.y + (nt * a.y + tt * v.y);
            c.z = l.z + (nt * a.z + tt * v.z);
            rt.setXYZ(y, c.x, c.y, c.z);
            p.subVectors(c, l).normalize();
            ut.setXYZ(y, p.x, p.y, p.z);
            w.x = e / i;
            w.y = o / r;
            ft.setXY(y, w.x, w.y);
            y++
        }
    for (o = 1; o <= i; o++)
        for (e = 1; e <= r; e++) {
            var ct = (r + 1) * (o - 1) + (e - 1),
                ot = (r + 1) * o + (e - 1),
                lt = (r + 1) * o + e,
                st = (r + 1) * (o - 1) + e;
            h.setX(s, ct);
            s++;
            h.setX(s, ot);
            s++;
            h.setX(s, st);
            s++;
            h.setX(s, ot);
            s++;
            h.setX(s, lt);
            s++;
            h.setX(s, st);
            s++
        }
    this.setIndex(h);
    this.addAttribute("position", rt);
    this.addAttribute("normal", ut);
    this.addAttribute("uv", ft)
};
THREE.TorusKnotBufferGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.TorusKnotBufferGeometry.prototype.constructor = THREE.TorusKnotBufferGeometry;
THREE.TorusKnotGeometry = function(n, t, i, r, u, f, e) {
    THREE.Geometry.call(this);
    this.type = "TorusKnotGeometry";
    this.parameters = {
        radius: n,
        tube: t,
        tubularSegments: i,
        radialSegments: r,
        p: u,
        q: f
    };
    e !== undefined && console.warn("THREE.TorusKnotGeometry: heightScale has been deprecated. Use .scale( x, y, z ) instead.");
    this.fromBufferGeometry(new THREE.TorusKnotBufferGeometry(n, t, i, r, u, f));
    this.mergeVertices()
};
THREE.TorusKnotGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.TorusKnotGeometry.prototype.constructor = THREE.TorusKnotGeometry;
THREE.TubeGeometry = function(n, t, i, r, u, f) {
    function yt(n, t, i) {
        return at.vertices.push(new THREE.Vector3(n, t, i)) - 1
    }
    THREE.Geometry.call(this);
    this.type = "TubeGeometry";
    this.parameters = {
        path: n,
        segments: t,
        radius: i,
        radialSegments: r,
        closed: u,
        taper: f
    };
    t = t || 64;
    i = i || 1;
    r = r || 8;
    u = u || !1;
    f = f || THREE.TubeGeometry.NoTaper;
    var h = [],
        at = this,
        vt, c, l, rt = t + 1,
        y, p, w, a, v, ut, s = new THREE.Vector3,
        e, o, b, k, ft, d, et, g, ot, nt, st, tt, it = new THREE.TubeGeometry.FrenetFrames(n, t, u),
        ht = it.tangents,
        ct = it.normals,
        lt = it.binormals;
    for (this.tangents = ht, this.normals = ct, this.binormals = lt, e = 0; e < rt; e++)
        for (h[e] = [], y = e / (rt - 1), ut = n.getPointAt(y), vt = ht[e], c = ct[e], l = lt[e], w = i * f(y), o = 0; o < r; o++) p = o / r * 2 * Math.PI, a = -w * Math.cos(p), v = w * Math.sin(p), s.copy(ut), s.x += a * c.x + v * l.x, s.y += a * c.y + v * l.y, s.z += a * c.z + v * l.z, h[e][o] = yt(s.x, s.y, s.z);
    for (e = 0; e < t; e++)
        for (o = 0; o < r; o++) b = u ? (e + 1) % t : e + 1, k = (o + 1) % r, ft = h[e][o], d = h[b][o], et = h[b][k], g = h[e][k], ot = new THREE.Vector2(e / t, o / r), nt = new THREE.Vector2((e + 1) / t, o / r), st = new THREE.Vector2((e + 1) / t, (o + 1) / r), tt = new THREE.Vector2(e / t, (o + 1) / r), this.faces.push(new THREE.Face3(ft, d, g)), this.faceVertexUvs[0].push([ot, nt, tt]), this.faces.push(new THREE.Face3(d, et, g)), this.faceVertexUvs[0].push([nt.clone(), st, tt.clone()]);
    this.computeFaceNormals();
    this.computeVertexNormals()
};
THREE.TubeGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.TubeGeometry.prototype.constructor = THREE.TubeGeometry;
THREE.TubeGeometry.NoTaper = function() {
    return 1
};
THREE.TubeGeometry.SinusoidalTaper = function(n) {
    return Math.sin(Math.PI * n)
};
THREE.TubeGeometry.FrenetFrames = function(n, t, i) {
    function b() {
        f[0] = new THREE.Vector3;
        e[0] = new THREE.Vector3;
        c = Number.MAX_VALUE;
        a = Math.abs(u[0].x);
        v = Math.abs(u[0].y);
        p = Math.abs(u[0].z);
        a <= c && (c = a, l.set(1, 0, 0));
        v <= c && (c = v, l.set(0, 1, 0));
        p <= c && l.set(0, 0, 1);
        o.crossVectors(u[0], l).normalize();
        f[0].crossVectors(u[0], o);
        e[0].crossVectors(u[0], f[0])
    }
    var l = new THREE.Vector3,
        u = [],
        f = [],
        e = [],
        o = new THREE.Vector3,
        y = new THREE.Matrix4,
        s = t + 1,
        h, c, a, v, p, r, w;
    for (this.tangents = u, this.normals = f, this.binormals = e, r = 0; r < s; r++) w = r / (s - 1), u[r] = n.getTangentAt(w), u[r].normalize();
    for (b(), r = 1; r < s; r++) f[r] = f[r - 1].clone(), e[r] = e[r - 1].clone(), o.crossVectors(u[r - 1], u[r]), o.length() > Number.EPSILON && (o.normalize(), h = Math.acos(THREE.Math.clamp(u[r - 1].dot(u[r]), -1, 1)), f[r].applyMatrix4(y.makeRotationAxis(o, h))), e[r].crossVectors(u[r], f[r]);
    if (i)
        for (h = Math.acos(THREE.Math.clamp(f[0].dot(f[s - 1]), -1, 1)), h /= s - 1, u[0].dot(o.crossVectors(f[0], f[s - 1])) > 0 && (h = -h), r = 1; r < s; r++) f[r].applyMatrix4(y.makeRotationAxis(u[r], h * r)), e[r].crossVectors(u[r], f[r])
};
THREE.PolyhedronGeometry = function(n, t, i, r) {
    function o(n) {
        var t = n.normalize().clone(),
            i, r;
        return t.index = e.vertices.push(t) - 1, i = nt(n) / 2 / Math.PI + .5, r = ut(n) / Math.PI + .5, t.uv = new THREE.Vector2(i, 1 - r), t
    }

    function g(n, t, i, r) {
        var f = new THREE.Face3(n.index, t.index, i.index, [n.clone(), t.clone(), i.clone()], undefined, r),
            u;
        e.faces.push(f);
        a.copy(n).add(t).add(i).divideScalar(3);
        u = nt(a);
        e.faceVertexUvs[0].push([w(n.uv, n, u), w(t.uv, t, u), w(i.uv, i, u)])
    }

    function rt(n, t) {
        for (var f = Math.pow(2, t), v = o(e.vertices[n.a]), y = o(e.vertices[n.b]), h = o(e.vertices[n.c]), u = [], c = n.materialIndex, r, s, i = 0; i <= f; i++) {
            u[i] = [];
            var l = o(v.clone().lerp(h, i / f)),
                p = o(y.clone().lerp(h, i / f)),
                a = f - i;
            for (r = 0; r <= a; r++) u[i][r] = r === 0 && i === f ? l : o(l.clone().lerp(p, r / a))
        }
        for (i = 0; i < f; i++)
            for (r = 0; r < 2 * (f - i) - 1; r++) s = Math.floor(r / 2), r % 2 == 0 ? g(u[i][s + 1], u[i + 1][s], u[i][s], c) : g(u[i][s + 1], u[i + 1][s + 1], u[i + 1][s], c)
    }

    function nt(n) {
        return Math.atan2(n.z, -n.x)
    }

    function ut(n) {
        return Math.atan2(-n.y, Math.sqrt(n.x * n.x + n.z * n.z))
    }

    function w(n, t, i) {
        return i < 0 && n.x === 1 && (n = new THREE.Vector2(n.x - 1, n.y)), t.x === 0 && t.z === 0 && (n = new THREE.Vector2(i / 2 / Math.PI + .5, n.y)), n.clone()
    }
    var e, h, c, a, u, f;
    for (THREE.Geometry.call(this), this.type = "PolyhedronGeometry", this.parameters = {
            vertices: n,
            indices: t,
            radius: i,
            detail: r
        }, i = i || 1, r = r || 0, e = this, u = 0, f = n.length; u < f; u += 3) o(new THREE.Vector3(n[u], n[u + 1], n[u + 2]));
    h = this.vertices;
    c = [];
    for (var u = 0, l = 0, f = t.length; u < f; u += 3, l++) {
        var b = h[t[u]],
            k = h[t[u + 1]],
            d = h[t[u + 2]];
        c[l] = new THREE.Face3(b.index, k.index, d.index, [b.clone(), k.clone(), d.clone()], undefined, l)
    }
    for (a = new THREE.Vector3, u = 0, f = c.length; u < f; u++) rt(c[u], r);
    for (u = 0, f = this.faceVertexUvs[0].length; u < f; u++) {
        var s = this.faceVertexUvs[0][u],
            v = s[0].x,
            y = s[1].x,
            p = s[2].x,
            tt = Math.max(v, y, p),
            it = Math.min(v, y, p);
        tt > .9 && it < .1 && (v < .2 && (s[0].x += 1), y < .2 && (s[1].x += 1), p < .2 && (s[2].x += 1))
    }
    for (u = 0, f = this.vertices.length; u < f; u++) this.vertices[u].multiplyScalar(i);
    this.mergeVertices();
    this.computeFaceNormals();
    this.boundingSphere = new THREE.Sphere(new THREE.Vector3, i)
};
THREE.PolyhedronGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.PolyhedronGeometry.prototype.constructor = THREE.PolyhedronGeometry;
THREE.DodecahedronGeometry = function(n, t) {
    var i = (1 + Math.sqrt(5)) / 2,
        r = 1 / i,
        u = [-1, -1, -1, -1, -1, 1, -1, 1, -1, -1, 1, 1, 1, -1, -1, 1, -1, 1, 1, 1, -1, 1, 1, 1, 0, -r, -i, 0, -r, i, 0, r, -i, 0, r, i, -r, -i, 0, -r, i, 0, r, -i, 0, r, i, 0, -i, 0, -r, i, 0, -r, -i, 0, r, i, 0, r];
    THREE.PolyhedronGeometry.call(this, u, [3, 11, 7, 3, 7, 15, 3, 15, 13, 7, 19, 17, 7, 17, 6, 7, 6, 15, 17, 4, 8, 17, 8, 10, 17, 10, 6, 8, 0, 16, 8, 16, 2, 8, 2, 10, 0, 12, 1, 0, 1, 18, 0, 18, 16, 6, 10, 2, 6, 2, 13, 6, 13, 15, 2, 16, 18, 2, 18, 3, 2, 3, 13, 18, 1, 9, 18, 9, 11, 18, 11, 3, 4, 14, 12, 4, 12, 0, 4, 0, 8, 11, 9, 5, 11, 5, 19, 11, 19, 7, 19, 5, 14, 19, 14, 4, 19, 4, 17, 1, 12, 14, 1, 14, 5, 1, 5, 9], n, t);
    this.type = "DodecahedronGeometry";
    this.parameters = {
        radius: n,
        detail: t
    }
};
THREE.DodecahedronGeometry.prototype = Object.create(THREE.PolyhedronGeometry.prototype);
THREE.DodecahedronGeometry.prototype.constructor = THREE.DodecahedronGeometry;
THREE.IcosahedronGeometry = function(n, t) {
    var i = (1 + Math.sqrt(5)) / 2,
        r = [-1, i, 0, 1, i, 0, -1, -i, 0, 1, -i, 0, 0, -1, i, 0, 1, i, 0, -1, -i, 0, 1, -i, i, 0, -1, i, 0, 1, -i, 0, -1, -i, 0, 1];
    THREE.PolyhedronGeometry.call(this, r, [0, 11, 5, 0, 5, 1, 0, 1, 7, 0, 7, 10, 0, 10, 11, 1, 5, 9, 5, 11, 4, 11, 10, 2, 10, 7, 6, 7, 1, 8, 3, 9, 4, 3, 4, 2, 3, 2, 6, 3, 6, 8, 3, 8, 9, 4, 9, 5, 2, 4, 11, 6, 2, 10, 8, 6, 7, 9, 8, 1], n, t);
    this.type = "IcosahedronGeometry";
    this.parameters = {
        radius: n,
        detail: t
    }
};
THREE.IcosahedronGeometry.prototype = Object.create(THREE.PolyhedronGeometry.prototype);
THREE.IcosahedronGeometry.prototype.constructor = THREE.IcosahedronGeometry;
THREE.OctahedronGeometry = function(n, t) {
    THREE.PolyhedronGeometry.call(this, [1, 0, 0, -1, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 1, 0, 0, -1], [0, 2, 4, 0, 4, 3, 0, 3, 5, 0, 5, 2, 1, 2, 5, 1, 5, 3, 1, 3, 4, 1, 4, 2], n, t);
    this.type = "OctahedronGeometry";
    this.parameters = {
        radius: n,
        detail: t
    }
};
THREE.OctahedronGeometry.prototype = Object.create(THREE.PolyhedronGeometry.prototype);
THREE.OctahedronGeometry.prototype.constructor = THREE.OctahedronGeometry;
THREE.TetrahedronGeometry = function(n, t) {
    THREE.PolyhedronGeometry.call(this, [1, 1, 1, -1, -1, 1, -1, 1, -1, 1, -1, -1], [2, 1, 0, 0, 3, 2, 1, 3, 0, 2, 3, 1], n, t);
    this.type = "TetrahedronGeometry";
    this.parameters = {
        radius: n,
        detail: t
    }
};
THREE.TetrahedronGeometry.prototype = Object.create(THREE.PolyhedronGeometry.prototype);
THREE.TetrahedronGeometry.prototype.constructor = THREE.TetrahedronGeometry;
THREE.ParametricGeometry = function(n, t, i) {
    var p, e, w, o, b, s, k, h;
    THREE.Geometry.call(this);
    this.type = "ParametricGeometry";
    this.parameters = {
        func: n,
        slices: t,
        stacks: i
    };
    for (var d = this.vertices, c = this.faces, l = this.faceVertexUvs[0], u, a, v, y, f = t + 1, r = 0; r <= i; r++)
        for (y = r / i, u = 0; u <= t; u++) v = u / t, a = n(v, y), d.push(a);
    for (r = 0; r < i; r++)
        for (u = 0; u < t; u++) p = r * f + u, e = r * f + u + 1, w = (r + 1) * f + u + 1, o = (r + 1) * f + u, b = new THREE.Vector2(u / t, r / i), s = new THREE.Vector2((u + 1) / t, r / i), k = new THREE.Vector2((u + 1) / t, (r + 1) / i), h = new THREE.Vector2(u / t, (r + 1) / i), c.push(new THREE.Face3(p, e, o)), l.push([b, s, h]), c.push(new THREE.Face3(e, w, o)), l.push([s.clone(), k, h.clone()]);
    this.computeFaceNormals();
    this.computeVertexNormals()
};
THREE.ParametricGeometry.prototype = Object.create(THREE.Geometry.prototype);
THREE.ParametricGeometry.prototype.constructor = THREE.ParametricGeometry;
THREE.WireframeGeometry = function(n) {
    function nt(n, t) {
        return n - t
    }
    var e, a, b, d, v, c, p, tt, ut, l, r, i, s, t, u, w, h;
    if (THREE.BufferGeometry.call(this), e = [0, 0], a = {}, b = ["a", "b", "c"], n instanceof THREE.Geometry) {
        var o = n.vertices,
            k = n.faces,
            f = 0,
            c = new Uint32Array(6 * k.length);
        for (i = 0, s = k.length; i < s; i++)
            for (d = k[i], t = 0; t < 3; t++) e[0] = d[b[t]], e[1] = d[b[(t + 1) % 3]], e.sort(nt), l = e.toString(), a[l] === undefined && (c[2 * f] = e[0], c[2 * f + 1] = e[1], a[l] = !0, f++);
        for (r = new Float32Array(f * 6), i = 0, s = f; i < s; i++)
            for (t = 0; t < 2; t++) v = o[c[2 * i + t]], u = 6 * i + 3 * t, r[u + 0] = v.x, r[u + 1] = v.y, r[u + 2] = v.z;
        this.addAttribute("position", new THREE.BufferAttribute(r, 3))
    } else if (n instanceof THREE.BufferGeometry)
        if (n.index !== null) {
            var y = n.index.array,
                o = n.attributes.position,
                g = n.groups,
                f = 0;
            for (g.length === 0 && n.addGroup(0, y.length), c = new Uint32Array(2 * y.length), p = 0, tt = g.length; p < tt; ++p) {
                var it = g[p],
                    rt = it.start,
                    ft = it.count;
                for (i = rt, ut = rt + ft; i < ut; i += 3)
                    for (t = 0; t < 3; t++) e[0] = y[i + t], e[1] = y[i + (t + 1) % 3], e.sort(nt), l = e.toString(), a[l] === undefined && (c[2 * f] = e[0], c[2 * f + 1] = e[1], a[l] = !0, f++)
            }
            for (r = new Float32Array(f * 6), i = 0, s = f; i < s; i++)
                for (t = 0; t < 2; t++) u = 6 * i + 3 * t, h = c[2 * i + t], r[u + 0] = o.getX(h), r[u + 1] = o.getY(h), r[u + 2] = o.getZ(h);
            this.addAttribute("position", new THREE.BufferAttribute(r, 3))
        } else {
            var o = n.attributes.position.array,
                f = o.length / 3,
                et = f / 3,
                r = new Float32Array(f * 6);
            for (i = 0, s = et; i < s; i++)
                for (t = 0; t < 3; t++) u = 18 * i + 6 * t, w = 9 * i + 3 * t, r[u + 0] = o[w], r[u + 1] = o[w + 1], r[u + 2] = o[w + 2], h = 9 * i + 3 * ((t + 1) % 3), r[u + 3] = o[h], r[u + 4] = o[h + 1], r[u + 5] = o[h + 2];
            this.addAttribute("position", new THREE.BufferAttribute(r, 3))
        }
};
THREE.WireframeGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);
THREE.WireframeGeometry.prototype.constructor = THREE.WireframeGeometry;
THREE.AxisHelper = function(n) {
    var i;
    n = n || 1;
    var r = new Float32Array([0, 0, 0, n, 0, 0, 0, 0, 0, 0, n, 0, 0, 0, 0, 0, 0, n]),
        u = new Float32Array([1, 0, 0, 1, .6, 0, 0, 1, 0, .6, 1, 0, 0, 0, 1, 0, .6, 1]),
        t = new THREE.BufferGeometry;
    t.addAttribute("position", new THREE.BufferAttribute(r, 3));
    t.addAttribute("color", new THREE.BufferAttribute(u, 3));
    i = new THREE.LineBasicMaterial({
        vertexColors: THREE.VertexColors
    });
    THREE.LineSegments.call(this, t, i)
};
THREE.AxisHelper.prototype = Object.create(THREE.LineSegments.prototype);
THREE.AxisHelper.prototype.constructor = THREE.AxisHelper;
THREE.ArrowHelper = function() {
    var t = new THREE.Geometry,
        n;
    return t.vertices.push(new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 1, 0)), n = new THREE.CylinderGeometry(0, .5, 1, 5, 1), n.translate(0, -.5, 0),
        function(i, r, u, f, e, o) {
            THREE.Object3D.call(this);
            f === undefined && (f = 16776960);
            u === undefined && (u = 1);
            e === undefined && (e = .2 * u);
            o === undefined && (o = .2 * e);
            this.position.copy(r);
            this.line = new THREE.Line(t, new THREE.LineBasicMaterial({
                color: f
            }));
            this.line.matrixAutoUpdate = !1;
            this.add(this.line);
            this.cone = new THREE.Mesh(n, new THREE.MeshBasicMaterial({
                color: f
            }));
            this.cone.matrixAutoUpdate = !1;
            this.add(this.cone);
            this.setDirection(i);
            this.setLength(u, e, o)
        }
}();
THREE.ArrowHelper.prototype = Object.create(THREE.Object3D.prototype);
THREE.ArrowHelper.prototype.constructor = THREE.ArrowHelper;
THREE.ArrowHelper.prototype.setDirection = function() {
    var n = new THREE.Vector3,
        t;
    return function(i) {
        i.y > .99999 ? this.quaternion.set(0, 0, 0, 1) : i.y < -.99999 ? this.quaternion.set(1, 0, 0, 0) : (n.set(i.z, 0, -i.x).normalize(), t = Math.acos(i.y), this.quaternion.setFromAxisAngle(n, t))
    }
}();
THREE.ArrowHelper.prototype.setLength = function(n, t, i) {
    t === undefined && (t = .2 * n);
    i === undefined && (i = .2 * t);
    this.line.scale.set(1, Math.max(0, n - t), 1);
    this.line.updateMatrix();
    this.cone.scale.set(i, t, i);
    this.cone.position.y = n;
    this.cone.updateMatrix()
};
THREE.ArrowHelper.prototype.setColor = function(n) {
    this.line.material.color.set(n);
    this.cone.material.color.set(n)
};
THREE.BoxHelper = function(n) {
    var i = new Uint16Array([0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 1, 5, 2, 6, 3, 7]),
        r = new Float32Array(24),
        t = new THREE.BufferGeometry;
    t.setIndex(new THREE.BufferAttribute(i, 1));
    t.addAttribute("position", new THREE.BufferAttribute(r, 3));
    THREE.LineSegments.call(this, t, new THREE.LineBasicMaterial({
        color: 16776960
    }));
    n !== undefined && this.update(n)
};
THREE.BoxHelper.prototype = Object.create(THREE.LineSegments.prototype);
THREE.BoxHelper.prototype.constructor = THREE.BoxHelper;
THREE.BoxHelper.prototype.update = function() {
    var n = new THREE.Box3;
    return function(t) {
        if (t instanceof THREE.Box3 ? n.copy(t) : n.setFromObject(t), !n.isEmpty()) {
            var r = n.min,
                u = n.max,
                f = this.geometry.attributes.position,
                i = f.array;
            i[0] = u.x;
            i[1] = u.y;
            i[2] = u.z;
            i[3] = r.x;
            i[4] = u.y;
            i[5] = u.z;
            i[6] = r.x;
            i[7] = r.y;
            i[8] = u.z;
            i[9] = u.x;
            i[10] = r.y;
            i[11] = u.z;
            i[12] = u.x;
            i[13] = u.y;
            i[14] = r.z;
            i[15] = r.x;
            i[16] = u.y;
            i[17] = r.z;
            i[18] = r.x;
            i[19] = r.y;
            i[20] = r.z;
            i[21] = u.x;
            i[22] = r.y;
            i[23] = r.z;
            f.needsUpdate = !0;
            this.geometry.computeBoundingSphere()
        }
    }
}();
THREE.BoundingBoxHelper = function(n, t) {
    var i = t !== undefined ? t : 8947848;
    this.object = n;
    this.box = new THREE.Box3;
    THREE.Mesh.call(this, new THREE.BoxGeometry(1, 1, 1), new THREE.MeshBasicMaterial({
        color: i,
        wireframe: !0
    }))
};
THREE.BoundingBoxHelper.prototype = Object.create(THREE.Mesh.prototype);
THREE.BoundingBoxHelper.prototype.constructor = THREE.BoundingBoxHelper;
THREE.BoundingBoxHelper.prototype.update = function() {
    this.box.setFromObject(this.object);
    this.box.size(this.scale);
    this.box.center(this.position)
};
THREE.CameraHelper = function(n) {
    function t(n, t, i) {
        s(n, i);
        s(t, i)
    }

    function s(n, t) {
        u.vertices.push(new THREE.Vector3);
        u.colors.push(new THREE.Color(t));
        f[n] === undefined && (f[n] = []);
        f[n].push(u.vertices.length - 1)
    }
    var u = new THREE.Geometry,
        h = new THREE.LineBasicMaterial({
            color: 16777215,
            vertexColors: THREE.FaceColors
        }),
        f = {},
        i = 16755200,
        e = 16711680,
        o = 43775,
        r = 3355443;
    t("n1", "n2", i);
    t("n2", "n4", i);
    t("n4", "n3", i);
    t("n3", "n1", i);
    t("f1", "f2", i);
    t("f2", "f4", i);
    t("f4", "f3", i);
    t("f3", "f1", i);
    t("n1", "f1", i);
    t("n2", "f2", i);
    t("n3", "f3", i);
    t("n4", "f4", i);
    t("p", "n1", e);
    t("p", "n2", e);
    t("p", "n3", e);
    t("p", "n4", e);
    t("u1", "u2", o);
    t("u2", "u3", o);
    t("u3", "u1", o);
    t("c", "t", 16777215);
    t("p", "c", r);
    t("cn1", "cn2", r);
    t("cn3", "cn4", r);
    t("cf1", "cf2", r);
    t("cf3", "cf4", r);
    THREE.LineSegments.call(this, u, h);
    this.camera = n;
    this.camera.updateProjectionMatrix();
    this.matrix = n.matrixWorld;
    this.matrixAutoUpdate = !1;
    this.pointMap = f;
    this.update()
};
THREE.CameraHelper.prototype = Object.create(THREE.LineSegments.prototype);
THREE.CameraHelper.prototype.constructor = THREE.CameraHelper;
THREE.CameraHelper.prototype.update = function() {
    function n(n, f, e, o) {
        var s, h, c;
        if (r.set(f, e, o).unproject(u), s = i[n], s !== undefined)
            for (h = 0, c = s.length; h < c; h++) t.vertices[s[h]].copy(r)
    }
    var t, i, r = new THREE.Vector3,
        u = new THREE.Camera;
    return function() {
        t = this.geometry;
        i = this.pointMap;
        var f = 1,
            r = 1;
        u.projectionMatrix.copy(this.camera.projectionMatrix);
        n("c", 0, 0, -1);
        n("t", 0, 0, 1);
        n("n1", -f, -r, -1);
        n("n2", f, -r, -1);
        n("n3", -f, r, -1);
        n("n4", f, r, -1);
        n("f1", -f, -r, 1);
        n("f2", f, -r, 1);
        n("f3", -f, r, 1);
        n("f4", f, r, 1);
        n("u1", f * .7, r * 1.1, -1);
        n("u2", -f * .7, r * 1.1, -1);
        n("u3", 0, r * 2, -1);
        n("cf1", -f, 0, 1);
        n("cf2", f, 0, 1);
        n("cf3", 0, -r, 1);
        n("cf4", 0, r, 1);
        n("cn1", -f, 0, -1);
        n("cn2", f, 0, -1);
        n("cn3", 0, -r, -1);
        n("cn4", 0, r, -1);
        t.verticesNeedUpdate = !0
    }
}();
THREE.DirectionalLightHelper = function(n, t) {
    var i, r;
    THREE.Object3D.call(this);
    this.light = n;
    this.light.updateMatrixWorld();
    this.matrix = n.matrixWorld;
    this.matrixAutoUpdate = !1;
    t = t || 1;
    i = new THREE.Geometry;
    i.vertices.push(new THREE.Vector3(-t, t, 0), new THREE.Vector3(t, t, 0), new THREE.Vector3(t, -t, 0), new THREE.Vector3(-t, -t, 0), new THREE.Vector3(-t, t, 0));
    r = new THREE.LineBasicMaterial({
        fog: !1
    });
    r.color.copy(this.light.color).multiplyScalar(this.light.intensity);
    this.lightPlane = new THREE.Line(i, r);
    this.add(this.lightPlane);
    i = new THREE.Geometry;
    i.vertices.push(new THREE.Vector3, new THREE.Vector3);
    r = new THREE.LineBasicMaterial({
        fog: !1
    });
    r.color.copy(this.light.color).multiplyScalar(this.light.intensity);
    this.targetLine = new THREE.Line(i, r);
    this.add(this.targetLine);
    this.update()
};
THREE.DirectionalLightHelper.prototype = Object.create(THREE.Object3D.prototype);
THREE.DirectionalLightHelper.prototype.constructor = THREE.DirectionalLightHelper;
THREE.DirectionalLightHelper.prototype.dispose = function() {
    this.lightPlane.geometry.dispose();
    this.lightPlane.material.dispose();
    this.targetLine.geometry.dispose();
    this.targetLine.material.dispose()
};
THREE.DirectionalLightHelper.prototype.update = function() {
    var t = new THREE.Vector3,
        i = new THREE.Vector3,
        n = new THREE.Vector3;
    return function() {
        t.setFromMatrixPosition(this.light.matrixWorld);
        i.setFromMatrixPosition(this.light.target.matrixWorld);
        n.subVectors(i, t);
        this.lightPlane.lookAt(n);
        this.lightPlane.material.color.copy(this.light.color).multiplyScalar(this.light.intensity);
        this.targetLine.geometry.vertices[1].copy(n);
        this.targetLine.geometry.verticesNeedUpdate = !0;
        this.targetLine.material.color.copy(this.lightPlane.material.color)
    }
}();
THREE.EdgesHelper = function(n, t, i) {
    var r = t !== undefined ? t : 16777215;
    THREE.LineSegments.call(this, new THREE.EdgesGeometry(n.geometry, i), new THREE.LineBasicMaterial({
        color: r
    }));
    this.matrix = n.matrixWorld;
    this.matrixAutoUpdate = !1
};
THREE.EdgesHelper.prototype = Object.create(THREE.LineSegments.prototype);
THREE.EdgesHelper.prototype.constructor = THREE.EdgesHelper;
THREE.FaceNormalsHelper = function(n, t, i, r) {
    var u, o;
    this.object = n;
    this.size = t !== undefined ? t : 1;
    var s = i !== undefined ? i : 16776960,
        h = r !== undefined ? r : 1,
        f = 0,
        e = this.object.geometry;
    e instanceof THREE.Geometry ? f = e.faces.length : console.warn("THREE.FaceNormalsHelper: only THREE.Geometry is supported. Use THREE.VertexNormalsHelper, instead.");
    u = new THREE.BufferGeometry;
    o = new THREE.Float32Attribute(f * 6, 3);
    u.addAttribute("position", o);
    THREE.LineSegments.call(this, u, new THREE.LineBasicMaterial({
        color: s,
        linewidth: h
    }));
    this.matrixAutoUpdate = !1;
    this.update()
};
THREE.FaceNormalsHelper.prototype = Object.create(THREE.LineSegments.prototype);
THREE.FaceNormalsHelper.prototype.constructor = THREE.FaceNormalsHelper;
THREE.FaceNormalsHelper.prototype.update = function() {
    var n = new THREE.Vector3,
        t = new THREE.Vector3,
        i = new THREE.Matrix3;
    return function() {
        var f, c, u, l;
        this.object.updateMatrixWorld(!0);
        i.getNormalMatrix(this.object.matrixWorld);
        var a = this.object.matrixWorld,
            e = this.geometry.attributes.position,
            s = this.object.geometry,
            o = s.vertices,
            h = s.faces,
            r = 0;
        for (f = 0, c = h.length; f < c; f++) u = h[f], l = u.normal, n.copy(o[u.a]).add(o[u.b]).add(o[u.c]).divideScalar(3).applyMatrix4(a), t.copy(l).applyMatrix3(i).normalize().multiplyScalar(this.size).add(n), e.setXYZ(r, n.x, n.y, n.z), r = r + 1, e.setXYZ(r, t.x, t.y, t.z), r = r + 1;
        return e.needsUpdate = !0, this
    }
}();
THREE.GridHelper = function(n, t) {
    var u = new THREE.Geometry,
        f = new THREE.LineBasicMaterial({
            vertexColors: THREE.VertexColors
        }),
        i, r;
    for (this.color1 = new THREE.Color(4473924), this.color2 = new THREE.Color(8947848), i = -n; i <= n; i += t) u.vertices.push(new THREE.Vector3(-n, 0, i), new THREE.Vector3(n, 0, i), new THREE.Vector3(i, 0, -n), new THREE.Vector3(i, 0, n)), r = i === 0 ? this.color1 : this.color2, u.colors.push(r, r, r, r);
    THREE.LineSegments.call(this, u, f)
};
THREE.GridHelper.prototype = Object.create(THREE.LineSegments.prototype);
THREE.GridHelper.prototype.constructor = THREE.GridHelper;
THREE.GridHelper.prototype.setColors = function(n, t) {
    this.color1.set(n);
    this.color2.set(t);
    this.geometry.colorsNeedUpdate = !0
};
THREE.HemisphereLightHelper = function(n, t) {
    var r, i, u, f;
    for (THREE.Object3D.call(this), this.light = n, this.light.updateMatrixWorld(), this.matrix = n.matrixWorld, this.matrixAutoUpdate = !1, this.colors = [new THREE.Color, new THREE.Color], r = new THREE.SphereGeometry(t, 4, 2), r.rotateX(-Math.PI / 2), i = 0, u = 8; i < u; i++) r.faces[i].color = this.colors[i < 4 ? 0 : 1];
    f = new THREE.MeshBasicMaterial({
        vertexColors: THREE.FaceColors,
        wireframe: !0
    });
    this.lightSphere = new THREE.Mesh(r, f);
    this.add(this.lightSphere);
    this.update()
};
THREE.HemisphereLightHelper.prototype = Object.create(THREE.Object3D.prototype);
THREE.HemisphereLightHelper.prototype.constructor = THREE.HemisphereLightHelper;
THREE.HemisphereLightHelper.prototype.dispose = function() {
    this.lightSphere.geometry.dispose();
    this.lightSphere.material.dispose()
};
THREE.HemisphereLightHelper.prototype.update = function() {
    var n = new THREE.Vector3;
    return function() {
        this.colors[0].copy(this.light.color).multiplyScalar(this.light.intensity);
        this.colors[1].copy(this.light.groundColor).multiplyScalar(this.light.intensity);
        this.lightSphere.lookAt(n.setFromMatrixPosition(this.light.matrixWorld).negate());
        this.lightSphere.geometry.colorsNeedUpdate = !0
    }
}();
THREE.PointLightHelper = function(n, t) {
    this.light = n;
    this.light.updateMatrixWorld();
    var r = new THREE.SphereGeometry(t, 4, 2),
        i = new THREE.MeshBasicMaterial({
            wireframe: !0,
            fog: !1
        });
    i.color.copy(this.light.color).multiplyScalar(this.light.intensity);
    THREE.Mesh.call(this, r, i);
    this.matrix = this.light.matrixWorld;
    this.matrixAutoUpdate = !1
};
THREE.PointLightHelper.prototype = Object.create(THREE.Mesh.prototype);
THREE.PointLightHelper.prototype.constructor = THREE.PointLightHelper;
THREE.PointLightHelper.prototype.dispose = function() {
    this.geometry.dispose();
    this.material.dispose()
};
THREE.PointLightHelper.prototype.update = function() {
    this.material.color.copy(this.light.color).multiplyScalar(this.light.intensity)
};
THREE.SkeletonHelper = function(n) {
    var t, i, r, u;
    for (this.bones = this.getBoneList(n), t = new THREE.Geometry, i = 0; i < this.bones.length; i++) r = this.bones[i], r.parent instanceof THREE.Bone && (t.vertices.push(new THREE.Vector3), t.vertices.push(new THREE.Vector3), t.colors.push(new THREE.Color(0, 0, 1)), t.colors.push(new THREE.Color(0, 1, 0)));
    t.dynamic = !0;
    u = new THREE.LineBasicMaterial({
        vertexColors: THREE.VertexColors,
        depthTest: !1,
        depthWrite: !1,
        transparent: !0
    });
    THREE.LineSegments.call(this, t, u);
    this.root = n;
    this.matrix = n.matrixWorld;
    this.matrixAutoUpdate = !1;
    this.update()
};
THREE.SkeletonHelper.prototype = Object.create(THREE.LineSegments.prototype);
THREE.SkeletonHelper.prototype.constructor = THREE.SkeletonHelper;
THREE.SkeletonHelper.prototype.getBoneList = function(n) {
    var t = [],
        i;
    for (n instanceof THREE.Bone && t.push(n), i = 0; i < n.children.length; i++) t.push.apply(t, this.getBoneList(n.children[i]));
    return t
};
THREE.SkeletonHelper.prototype.update = function() {
    for (var n = this.geometry, f = (new THREE.Matrix4).getInverse(this.root.matrixWorld), t = new THREE.Matrix4, u = 0, r, i = 0; i < this.bones.length; i++) r = this.bones[i], r.parent instanceof THREE.Bone && (t.multiplyMatrices(f, r.matrixWorld), n.vertices[u].setFromMatrixPosition(t), t.multiplyMatrices(f, r.parent.matrixWorld), n.vertices[u + 1].setFromMatrixPosition(t), u += 2);
    n.verticesNeedUpdate = !0;
    n.computeBoundingSphere()
};
THREE.SpotLightHelper = function(n) {
    var t, i, f, e, s;
    THREE.Object3D.call(this);
    this.light = n;
    this.light.updateMatrixWorld();
    this.matrix = n.matrixWorld;
    this.matrixAutoUpdate = !1;
    t = new THREE.BufferGeometry;
    i = [0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, -1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, -1, 1];
    for (var r = 0, o = 1, u = 32; r < u; r++, o++) f = r / u * Math.PI * 2, e = o / u * Math.PI * 2, i.push(Math.cos(f), Math.sin(f), 1, Math.cos(e), Math.sin(e), 1);
    t.addAttribute("position", new THREE.Float32Attribute(i, 3));
    s = new THREE.LineBasicMaterial({
        fog: !1
    });
    this.cone = new THREE.LineSegments(t, s);
    this.add(this.cone);
    this.update()
};
THREE.SpotLightHelper.prototype = Object.create(THREE.Object3D.prototype);
THREE.SpotLightHelper.prototype.constructor = THREE.SpotLightHelper;
THREE.SpotLightHelper.prototype.dispose = function() {
    this.cone.geometry.dispose();
    this.cone.material.dispose()
};
THREE.SpotLightHelper.prototype.update = function() {
    var n = new THREE.Vector3,
        t = new THREE.Vector3;
    return function() {
        var i = this.light.distance ? this.light.distance : 1e3,
            r = i * Math.tan(this.light.angle);
        this.cone.scale.set(r, r, i);
        n.setFromMatrixPosition(this.light.matrixWorld);
        t.setFromMatrixPosition(this.light.target.matrixWorld);
        this.cone.lookAt(t.sub(n));
        this.cone.material.color.copy(this.light.color).multiplyScalar(this.light.intensity)
    }
}();
THREE.VertexNormalsHelper = function(n, t, i, r) {
    var e, o;
    this.object = n;
    this.size = t !== undefined ? t : 1;
    var s = i !== undefined ? i : 16711680,
        h = r !== undefined ? r : 1,
        f = 0,
        u = this.object.geometry;
    u instanceof THREE.Geometry ? f = u.faces.length * 3 : u instanceof THREE.BufferGeometry && (f = u.attributes.normal.count);
    e = new THREE.BufferGeometry;
    o = new THREE.Float32Attribute(f * 6, 3);
    e.addAttribute("position", o);
    THREE.LineSegments.call(this, e, new THREE.LineBasicMaterial({
        color: s,
        linewidth: h
    }));
    this.matrixAutoUpdate = !1;
    this.update()
};
THREE.VertexNormalsHelper.prototype = Object.create(THREE.LineSegments.prototype);
THREE.VertexNormalsHelper.prototype.constructor = THREE.VertexNormalsHelper;
THREE.VertexNormalsHelper.prototype.update = function() {
    var n = new THREE.Vector3,
        t = new THREE.Vector3,
        i = new THREE.Matrix3;
    return function() {
        var b = ["a", "b", "c"],
            s, y, h, p, w, r, o;
        this.object.updateMatrixWorld(!0);
        i.getNormalMatrix(this.object.matrixWorld);
        var a = this.object.matrixWorld,
            e = this.geometry.attributes.position,
            f = this.object.geometry;
        if (f instanceof THREE.Geometry) {
            var k = f.vertices,
                v = f.faces,
                u = 0;
            for (s = 0, y = v.length; s < y; s++)
                for (h = v[s], r = 0, o = h.vertexNormals.length; r < o; r++) p = k[h[b[r]]], w = h.vertexNormals[r], n.copy(p).applyMatrix4(a), t.copy(w).applyMatrix3(i).normalize().multiplyScalar(this.size).add(n), e.setXYZ(u, n.x, n.y, n.z), u = u + 1, e.setXYZ(u, t.x, t.y, t.z), u = u + 1
        } else if (f instanceof THREE.BufferGeometry) {
            var c = f.attributes.position,
                l = f.attributes.normal,
                u = 0;
            for (r = 0, o = c.count; r < o; r++) n.set(c.getX(r), c.getY(r), c.getZ(r)).applyMatrix4(a), t.set(l.getX(r), l.getY(r), l.getZ(r)), t.applyMatrix3(i).normalize().multiplyScalar(this.size).add(n), e.setXYZ(u, n.x, n.y, n.z), u = u + 1, e.setXYZ(u, t.x, t.y, t.z), u = u + 1
        }
        return e.needsUpdate = !0, this
    }
}();
THREE.WireframeHelper = function(n, t) {
    var i = t !== undefined ? t : 16777215;
    THREE.LineSegments.call(this, new THREE.WireframeGeometry(n.geometry), new THREE.LineBasicMaterial({
        color: i
    }));
    this.matrix = n.matrixWorld;
    this.matrixAutoUpdate = !1
};
THREE.WireframeHelper.prototype = Object.create(THREE.LineSegments.prototype);
THREE.WireframeHelper.prototype.constructor = THREE.WireframeHelper;
THREE.ImmediateRenderObject = function(n) {
    THREE.Object3D.call(this);
    this.material = n;
    this.render = function() {}
};
THREE.ImmediateRenderObject.prototype = Object.create(THREE.Object3D.prototype);
THREE.ImmediateRenderObject.prototype.constructor = THREE.ImmediateRenderObject;
THREE.MorphBlendMesh = function(n, t) {
    THREE.Mesh.call(this, n, t);
    this.animationsMap = {};
    this.animationsList = [];
    var i = this.geometry.morphTargets.length,
        r = "__default",
        u = i - 1,
        f = i / 1;
    this.createAnimation(r, 0, u, f);
    this.setAnimationWeight(r, 1)
};
THREE.MorphBlendMesh.prototype = Object.create(THREE.Mesh.prototype);
THREE.MorphBlendMesh.prototype.constructor = THREE.MorphBlendMesh;
THREE.MorphBlendMesh.prototype.createAnimation = function(n, t, i, r) {
    var u = {
        start: t,
        end: i,
        length: i - t + 1,
        fps: r,
        duration: (i - t) / r,
        lastFrame: 0,
        currentFrame: 0,
        active: !1,
        time: 0,
        direction: 1,
        weight: 1,
        directionBackwards: !1,
        mirroredLoop: !1
    };
    this.animationsMap[n] = u;
    this.animationsList.push(u)
};
THREE.MorphBlendMesh.prototype.autoCreateAnimations = function(n) {
    for (var e, u = {}, o = this.geometry, h, f, r, t, i = 0, s = o.morphTargets.length; i < s; i++) h = o.morphTargets[i], f = h.name.match(/([a-z]+)_?(\d+)/i), f && f.length > 1 && (r = f[1], u[r] || (u[r] = {
        start: Infinity,
        end: -Infinity
    }), t = u[r], i < t.start && (t.start = i), i > t.end && (t.end = i), e || (e = r));
    for (r in u) t = u[r], this.createAnimation(r, t.start, t.end, n);
    this.firstAnimation = e
};
THREE.MorphBlendMesh.prototype.setAnimationDirectionForward = function(n) {
    var t = this.animationsMap[n];
    t && (t.direction = 1, t.directionBackwards = !1)
};
THREE.MorphBlendMesh.prototype.setAnimationDirectionBackward = function(n) {
    var t = this.animationsMap[n];
    t && (t.direction = -1, t.directionBackwards = !0)
};
THREE.MorphBlendMesh.prototype.setAnimationFPS = function(n, t) {
    var i = this.animationsMap[n];
    i && (i.fps = t, i.duration = (i.end - i.start) / i.fps)
};
THREE.MorphBlendMesh.prototype.setAnimationDuration = function(n, t) {
    var i = this.animationsMap[n];
    i && (i.duration = t, i.fps = (i.end - i.start) / i.duration)
};
THREE.MorphBlendMesh.prototype.setAnimationWeight = function(n, t) {
    var i = this.animationsMap[n];
    i && (i.weight = t)
};
THREE.MorphBlendMesh.prototype.setAnimationTime = function(n, t) {
    var i = this.animationsMap[n];
    i && (i.time = t)
};
THREE.MorphBlendMesh.prototype.getAnimationTime = function(n) {
    var t = 0,
        i = this.animationsMap[n];
    return i && (t = i.time), t
};
THREE.MorphBlendMesh.prototype.getAnimationDuration = function(n) {
    var t = -1,
        i = this.animationsMap[n];
    return i && (t = i.duration), t
};
THREE.MorphBlendMesh.prototype.playAnimation = function(n) {
    var t = this.animationsMap[n];
    t ? (t.time = 0, t.active = !0) : console.warn("THREE.MorphBlendMesh: animation[" + n + "] undefined in .playAnimation()")
};
THREE.MorphBlendMesh.prototype.stopAnimation = function(n) {
    var t = this.animationsMap[n];
    t && (t.active = !1)
};
THREE.MorphBlendMesh.prototype.update = function(n) {
    for (var t, f, e, i, r, u = 0, o = this.animationsList.length; u < o; u++)(t = this.animationsList[u], t.active) && (f = t.duration / t.length, t.time += t.direction * n, t.mirroredLoop ? (t.time > t.duration || t.time < 0) && (t.direction *= -1, t.time > t.duration && (t.time = t.duration, t.directionBackwards = !0), t.time < 0 && (t.time = 0, t.directionBackwards = !1)) : (t.time = t.time % t.duration, t.time < 0 && (t.time += t.duration)), e = t.start + THREE.Math.clamp(Math.floor(t.time / f), 0, t.length - 1), i = t.weight, e !== t.currentFrame && (this.morphTargetInfluences[t.lastFrame] = 0, this.morphTargetInfluences[t.currentFrame] = 1 * i, this.morphTargetInfluences[e] = 0, t.lastFrame = t.currentFrame, t.currentFrame = e), r = t.time % f / f, t.directionBackwards && (r = 1 - r), t.currentFrame !== t.lastFrame ? (this.morphTargetInfluences[t.currentFrame] = r * i, this.morphTargetInfluences[t.lastFrame] = (1 - r) * i) : this.morphTargetInfluences[t.currentFrame] = i)
}