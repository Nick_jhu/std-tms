var OLD_DATA = {};
//初始化欄位
var initField = function(data) {
    if(data[1].length > 0) {
        for(var i in data[1]) {
            var fieldData = data[1][i];
            var fieldName = fieldData['text'];
            var fieldType = fieldData['filtertype'];
            var fieldWidth = fieldData['dbwidth'];
            var fieldNullable = fieldData['nullable'];

            if(fieldType == 'varchar') {
                $('input[name="'+fieldName+'"]').not('[grid="true"]').attr("maxlength", fieldWidth);
            }
            else if(fieldType == 'int') {
                //$('input[name="'+fieldName+'"]').attr("type", 'number');
            }
            else if(fieldType == 'decimal') {
                $('#myForm input[name="'+fieldName+'"]').not('[grid="true"]').attr("type", 'number');
            }
            
            if(fieldNullable == false) {
                $('#myForm [name="'+fieldName+'"]').not('[grid="true"]').attr("required", 'required');
            }
        }
    }
}

var setField = {
    init: function(formId, fieldObj, editable) {
        if(fieldObj != null) {
            $.each(fieldObj, function(i, v){
                var fieldName = v['name'];
                var title = v['title'];

                if(v['type'] == 'lookup') {
                    var disabled = '';

                    if(editable == true) {
                        disabled = 'disabled="disabled"';
                    }

                    if($('#'+formId+' input[name="'+fieldName+'"]').length > 0) {
                        if($('#'+formId+' input[name="'+fieldName+'"]').not('[grid="true"]').parents('.input-group').length == 0) {
                            $('#'+formId+' input[name="'+fieldName+'"]').not('[grid="true"]').wrap('<div class="input-group input-group-sm"></div>');
                            $('#'+formId+' input[name="'+fieldName+'"]').not('[grid="true"]').after('<span class="input-group-btn"><button type="button" class="btn btn-default btn-flat lookup" info1="'+v['info1']+'" info2="'+v['info2']+'" info3="'+v['info3']+'" info4="'+v['info4']+'" selectionmode="'+v['selectionmode']+'" triggerfunc="'+v['triggerfunc']+'" '+disabled+' btnName="'+fieldName+'"><i class="fa fa-search" aria-hidden="true"></i></button></span>');
                            $('#'+formId+' button[btnName="'+fieldName+'"]').not('[grid="true"]').on('click', function(){
                                $('#lookupModal').modal('show');
                                if(typeof v['triggerfunc'] === "undefined") {
                                    initLookup(fieldName, title);
                                }
                                else {
                                    initLookup(fieldName, title, eval(v['triggerfunc']));
                                }
                                
                            });
                            $('#'+formId+' input[name="'+fieldName+'"]').on('click', function(){
                                var check = $('#'+formId+' input[name="'+fieldName+'"]').data('ui-autocomplete') != undefined;
                                if(check == false) {
                                    if(typeof v['triggerfunc'] === "undefined") {
                                        
                                        initAutocomplete(formId,fieldName);
                                    }
                                    else {
                                        initAutocomplete(formId,fieldName, eval(v['triggerfunc']));
                                    }
                                    
                                }
                            });

                            
                        }
                        else {
                            $('#'+formId+' button[btnName="'+fieldName+'"]').not('[grid="true"]').prop('disabled', editable);
                        }
                    }
                }
                else if(v['type'] == 'date_picker') {
                    $('#'+formId+' input[name="'+fieldName+'"]').not('[grid="true"]').datepicker({
                        autoclose: true,
                        format: 'yyyy-mm-dd'
                    });
                }
                else if(v['type'] == 'datetime_picker') {
                    $('#'+formId+' input[name="'+fieldName+'"]').not('[grid="true"]').datetimepicker({
                        autoclose: true,
                        format: 'YYYY-MM-DD HH:mm:ss'
                    });
                }
        
                if($('#'+formId+' input[name="'+fieldName+'"]').length > 0) {
                    $('#'+formId+' input[name="'+fieldName+'"]').not('[grid="true"]').prop('disabled', editable);
                }
        
                if($('#'+formId+' input[name="'+fieldName+'[]"]').length > 0) {
                    $('#'+formId+' input[name="'+fieldName+'[]"]').not('[grid="true"]').prop('disabled', editable);
                }
        
                if($('#'+formId+' select[name="'+fieldName+'"]').length > 0) {
                    $('#'+formId+' select[name="'+fieldName+'"]').not('[grid="true"]').prop('disabled', editable);
                    var options = '';

                    if(typeof v['options'] != 'undefined') {
                        for(var j=0; j<v['options'].length; j++) {
                            var code = v['options'][j]['code'];
                            var descp = v['options'][j]['descp'];
                            var def = (typeof v['options'][j]['def'] !== "undefined")?v['options'][j]['def']:"";
                            if(def == 'Y') {
                                options += '<option value="'+code+'" def="Y" selected>'+descp+'</option>';
                            }
                            else {
                                options += '<option value="'+code+'">'+descp+'</option>';
                            }
                            
                        }
                        $('#'+formId+' select[name="'+fieldName+'"]').not('[grid="true"]').html(options);
                    }
                    
                }
        
                
                if($('#'+formId+' select[name="'+fieldName+'[]"]').length > 0) {
                    $('#'+formId+' select[name="'+fieldName+'[]"]').not('[grid="true"]').prop('disabled', editable);
                    var options = '';
                    if(typeof v['options'] != 'undefined') {
                        for(var j=0; j<v['options'].length; j++) {
                            var code = v['options'][j]['code'];
                            var descp = v['options'][j]['descp'];
                            options += '<option value="'+code+'">'+descp+'</option>';
                        }
                        $('#'+formId+' select[name="'+fieldName+'[]"]').not('[grid="true"]').html(options);
                    }
                    
                    $('#'+formId+' select[name="'+fieldName+'[]"]').not('[grid="true"]').select2({tags: true});
                }
                
                if($('#'+formId+' textarea[name="'+fieldName+'"]').length > 0) {
                    $('#'+formId+' textarea[name="'+fieldName+'"]').not('[grid="true"]').prop('disabled', editable);
                }
            });
        }
    },
    enabled: function(formId, fieldArray) {

        for(var i=0; i<fieldArray.length; i++) {
            var fieldName = fieldArray[i];
            if($('#'+formId+' input[name="'+fieldName+'"]').length > 0) {
                $('#'+formId+' input[name="'+fieldName+'"]').not('[grid="true"]').attr('switch', "on");
            }
    
            if($('#'+formId+' input[name="'+fieldName+'[]"]').length > 0) {
                $('#'+formId+' input[name="'+fieldName+'[]"]').not('[grid="true"]').attr('switch', "on");
            }
    
            if($('#'+formId+' select[name="'+fieldName+'"]').length > 0) {
                $('#'+formId+' select[name="'+fieldName+'"]').not('[grid="true"]').attr('switch', "on");
            }
    
            
            if($('#'+formId+' select[name="'+fieldName+'[]"]').length > 0) {
                $('#'+formId+' select[name="'+fieldName+'[]"]').not('[grid="true"]').attr('switch', "on");
            }
            
            if($('#'+formId+' textarea[name="'+fieldName+'"]').length > 0) {
                $('#'+formId+' textarea[name="'+fieldName+'"]').not('[grid="true"]').attr('switch', "on");
            }
        }

    },
    disabled: function(formId, fieldArray) {
        for(var i=0; i<fieldArray.length; i++) {
            var fieldName = fieldArray[i];
            if($('#'+formId+' input[name="'+fieldName+'"]').length > 0) {
                $('#'+formId+' input[name="'+fieldName+'"]').not('[grid="true"]').attr('switch', "off");
            }
    
            if($('#'+formId+' input[name="'+fieldName+'[]"]').length > 0) {
                $('#'+formId+' input[name="'+fieldName+'[]"]').not('[grid="true"]').attr('switch', "off");
            }
    
            if($('#'+formId+' select[name="'+fieldName+'"]').length > 0) {
                $('#'+formId+' select[name="'+fieldName+'"]').not('[grid="true"]').attr('switch', "off");
            }
    
            
            if($('#'+formId+' select[name="'+fieldName+'[]"]').length > 0) {
                $('#'+formId+' select[name="'+fieldName+'[]"]').not('[grid="true"]').attr('switch', "off");
            }
            
            if($('#'+formId+' textarea[name="'+fieldName+'"]').length > 0) {
                $('#'+formId+' textarea[name="'+fieldName+'"]').not('[grid="true"]').attr('switch', "off");
            }
        }
    },
    disabledAll: function(formId, fieldObj){
        if(fieldObj != null) {
            $.each(fieldObj, function(i, v){
                var fieldName = v['name'];
                var title = v['title'];

                if(v['type'] == 'lookup') {
                    var disabled = '';

                    if($('#'+formId+' input[name="'+fieldName+'"]').length > 0) {
                        $('#'+formId+' button[btnName="'+fieldName+'"]').not('[grid="true"]').prop('disabled', true);
                    }
                }

                if($('#'+formId+' input[name="'+fieldName+'"]').length > 0) {
                    $('#'+formId+' input[name="'+fieldName+'"]').not('[grid="true"]').prop('disabled', true);
                }
        
                if($('#'+formId+' input[name="'+fieldName+'[]"]').length > 0) {
                    $('#'+formId+' input[name="'+fieldName+'[]"]').not('[grid="true"]').prop('disabled', true);
                }
        
                if($('#'+formId+' select[name="'+fieldName+'"]').length > 0) {
                    $('#'+formId+' select[name="'+fieldName+'"]').not('[grid="true"]').prop('disabled', true);
                }
        
                
                if($('#'+formId+' select[name="'+fieldName+'[]"]').length > 0) {
                    $('#'+formId+' select[name="'+fieldName+'[]"]').not('[grid="true"]').prop('disabled', true);
                }
                
                if($('#'+formId+' textarea[name="'+fieldName+'"]').length > 0) {
                    $('#'+formId+' textarea[name="'+fieldName+'"]').not('[grid="true"]').prop('disabled', true);
                }
            });
        }
    }
}

//把值帶入form中
var setFormData = function(formId, data) {

    $.each(fieldObj, function(i, v){
        if(v['type'] == 'checkbox') {
            if(data[v['name']] != null) {
                var a = data[v['name']].split(',');
                $('#'+formId+' [name="'+v['name']+'[]"]').not('[grid="true"]').each(function(){
                    if($.inArray( $(this).val(), a ) != -1) {
                        $(this).prop('checked', true);
                    }
                    else {
                        $(this).prop('checked', false);
                    }
                });
            }
            
        }
        else if(v['type'] == 'select2_multiple') {
            if(data[v['name']] != null) {
                var a = data[v['name']].split(',');
                $('#'+formId+' [name="'+v['name']+'[]"] > option').not('[grid="true"]').each(function(){
                    if($.inArray( $(this).val(), a ) != -1) {
                        $(this).prop('selected', true);
                    }
                    else {
                        $(this).prop('selected', false);
                    }
                    $('#'+formId+' [name="'+v['name']+'[]"]').trigger("change");
                });
            }
            
        }
        else if(v['type'] == 'radio') {
            $('#'+formId+' [name="'+v['name']+'"]').not('[grid="true"]').each(function(){
                if( $(this).val() == data[v['name']]) {
                    $(this).prop('checked', true);
                }
                else {
                    $(this).prop('checked', false);
                }
            });
        }
        else {
            if($('#'+formId+' [name="'+v['name']+'"]').not('[grid="true"]').length > 0) {
                $('#'+formId+' [name="'+v['name']+'"]').not('[grid="true"]').val(data[v['name']]);
            }
        }
        OLD_DATA[v['name']] = data[v['name']];
    });

}

var getChangeData = function(formId, formData) {
    var changeData = {};
    if(OLD_DATA.length == 0) {
        $('#' + formId + ' input').each(function(){
            var name = $(this).attr('name');
            var val  = $(this).val();
            var d    = $(this).attr('disabled');
            
            // if(d == 'disabled') {
            //     formData.append(name, val);
            // }
            if(typeof name != "undefined") {
                changeData[name] = val;
            }
        });

        $('#' + formId + ' select').each(function(){
            var name = $(this).attr('name');
            var val  = $(this).val();
            var d    = $(this).attr('disabled');
            
            // if(d == 'disabled') {
            //     formData.append(name, val);
            // }
            if(typeof name != "undefined") {
                name = name.replace('[', '');
                name = name.replace(']', '');
                changeData[name] = val;
            }
            
        });

        $('#' + formId + ' textarea').each(function(){
            var name = $(this).attr('name');
            var val  = $(this).val();
            var d    = $(this).attr('disabled');
            
            // if(d == 'disabled') {
            //     formData.append(name, val);
            // }
            if(typeof name != "undefined") {
                changeData[name] = val;
            }
            
        });

        //return formData;
        return changeData;
    }

    $.each(OLD_DATA, function(i, v){
        //var newData = formData.get(i);
        var newData = getFormDataVal(formData, i);
        if(i == "cust_type"){
            newData = $("select[name='cust_type[]']").val();
        } 
        var r = $('#'+formId+' [name="'+i+'"]').attr('required');
        var d = $('#'+formId+' [name="'+i+'"]').attr('disabled');

        if(d == 'disabled' && v != newData) {
            //formData.append(i, v);
            //var fieldObj = {name: i, value: v};
            //changeData.push(fieldObj);
            changeData[i] = v;
        }
        
        if(v == newData && r != 'required') {
            //formData.delete(i);
        }

        if(v != newData && d != 'disabled') {
            //var fieldObj = {name: i, value: newData};
            //changeData.push(fieldObj);
            changeData[i] = newData;
        }
        
    });

    $('#'+formId+' input[name="id"]').each(function(i, v){
        //var fieldObj = {name:$(this).attr("name"), value: $(this).val()};
        //changeData.push(fieldObj);
        changeData[$(this).attr("name")] = $(this).val();
    });

    $('#' + formId + ' select').each(function(){
        var name = $(this).attr('name');
        var val  = $(this).val();
        var d    = $(this).attr('disabled');
        
        if(typeof name != "undefined") {
            name = name.replace('[', '');
            name = name.replace(']', '');
            changeData[name] = val;
        }
        
    });

    //return formData;
    return changeData;
}

var getFormDataVal = function(formData, name) {
    var formDataName = "";
    for(i in formData) {
        formDataName = formData[i].name.replace('[', '');
        formDataName = formDataName.replace(']', '');
        if(formData[i].name == name) {
            return formData[i].value;
        }
    }
}

var deleteFormDataVal = function(formData, name) {
    for(i in formData) {
        if(formData[i].name == name) {
            formData.splice(i, 1);
            return formData;
        }
    }
}

//客制頁面之按鈕操作
var menuBtnFunc = {
    disabled: function(btnArray) {
        for(var i=0; i<btnArray.length; i++) {
            $('#'+btnArray[i]).addClass('disabled');
        }
    },
    enabled: function(btnArray) {
        for(var i=0; i<btnArray.length; i++) {
            $('#'+btnArray[i]).removeClass('disabled');
        }
    },
    init: function() {
        this.disabled(['iSave', 'iCancel']);
        this.enabled(['iAdd', 'iEdit', 'iCopy', 'iDel']);
    },
    addFunc: function() {
        this.enabled(['iSave', 'iCancel']);
        this.disabled(['iAdd', 'iEdit', 'iCopy', 'iDel']);

        $('button.lookup').prop('disabled', false);
    },
    editFunc: function() {
        this.enabled(['iSave', 'iCancel']);
        this.disabled(['iAdd', 'iEdit', 'iCopy', 'iDel']);

        $('button.lookup').prop('disabled', false);
    }
}


var genLookup = function(info1, info2, info3) {
    $.ajax({
        url: BASE_URL,
        type: 'POST',
        data: {info1: info1, info2:info2, info3:info3},
        async: false,
        beforeSend: function () {
            
        },
        error: function (jqXHR, exception) {
            
        },
        success: function (data) {
            
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

var genDetailGrid = function(opt) {
    var theme = 'bootstrap';
    $.jqx.theme = theme;
    fieldData = (typeof opt.fieldData == 'undefined')?null:opt.fieldData;

    if(fieldData != null){ 
        initSpcGrid(fieldData, opt);
    }
        

    $('#'+opt.gridId).on('rowdoubleclick', function (event) 
    { 
        if(opt.gridId=="imgGrid"){
            return;
        }
        if(opt.showtoolbar!=true){
            return;
        }
        var args = event.args;
        // row's bound index.
        var boundIndex = args.rowindex;
        // row's visible index.
        var visibleIndex = args.visibleindex;
        // right click.
        var rightclick = args.rightclick; 
        // original event.
        var ev = args.originalEvent;

        var dataRecord = $("#"+opt.gridId).jqxGrid('getrowdata', boundIndex);

        console.log(dataRecord);
        for(var i in dataRecord) {
            $('#'+opt.formId+' [name="'+i+'"]').val(dataRecord[i]);
        }
        // $.each(dataRecord, function(i, v){
        //     $('#'+opt.formId+' [name="'+i+'"]').val(v);
        // });

        $("#"+opt.showBoxId).show();
    });

    

    $("#"+opt.saveId).on('click', function(){
        editrow = $("#"+opt.gridId).jqxGrid('getselectedrowindex');
        var formData = $("#"+opt.formId).serializeArray();
        var row = {};
        $.each(formData, function(i, v){
            row[v['name']] = v['value'];
        });
        console.log(row);
        if(typeof opt.beforeSave != "undefined") {
            var s = opt.beforeSave(row);
            if(s === false) {
                return;
            }
        }
        if(editrow >= 0) {
            var rowID = $('#'+opt.gridId).jqxGrid('getrowid', editrow);
            $('#'+opt.gridId).jqxGrid('updaterow', rowID, row);
        }
        else {
            var commit = $("#"+opt.gridId).jqxGrid('addrow', null, row); 
        }
        
    });

    $("#"+opt.cancelId).on('click', function(){
        if(typeof opt.beforeCancel != 'undefined') {
            opt.beforeCancel();
        }
        $("#"+opt.showBoxId).hide();
    });
    
}

var initSpcGrid = function (fieldData, opt) {
   
    var getUrl = (typeof opt.getUrl == 'undefined')?null:opt.getUrl;
    var addUrl = (typeof opt.addUrl == 'undefined')?null:opt.addUrl;
    var updateUrl = (typeof opt.updateUrl == 'undefined')?null:opt.updateUrl;
    var delUrl = (typeof opt.delUrl == 'undefined')?null:opt.delUrl;
    var formId = (typeof opt.formId == 'undefined')?null:opt.formId;
    var gridId = (typeof opt.gridId == 'undefined')?null:opt.gridId;
    var saveId = (typeof opt.saveId == 'undefined')?null:opt.saveId;
    var cancelId = (typeof opt.cancelId == 'undefined')?null:opt.cancelId;
    var defaultKey = (typeof opt.defaultKey == 'undefined')?null:opt.defaultKey;
    var beforeSave = (typeof opt.beforeSave == 'undefined')?null:opt.beforeSave;
    var afterSave = (typeof opt.afterSave == 'undefined')?null:opt.afterSave;
    var height = (typeof opt.height == 'undefined')?null:opt.height;
    var showBoxId = (typeof opt.showBoxId == 'undefined')?null:opt.showBoxId;
    var custBtn = (typeof opt.custBtn == 'undefined')?null:opt.custBtn;
    var showaggregates = (typeof opt.showaggregates == 'undefined')?null:opt.showaggregates;
    var commonBtn = (typeof opt.commonBtn == 'undefined')?true:opt.commonBtn;
    var inlineEdit = (typeof opt.inlineEdit == 'undefined')?false:opt.inlineEdit;
    var selectionmode = (typeof opt.selectionmode == 'undefined')?'singlerow':opt.selectionmode;
    var enablebrowserselection = (typeof opt.enablebrowserselection == 'undefined')?true:opt.enablebrowserselection;

    var source =
    {
        datatype: "json",
        datafields: fieldData[0],
        root:"Rows",
        pagenum: 0,
        beforeprocessing: function (data) {

        },
        sort: function () {
            // update the grid and send a request to the server.
            $("#"+gridId).jqxGrid('updatebounddata', 'sort');
        },
        cache: false,
        pagesize: 20,
        url: getUrl,
        addrow: function (rowid, rowdata, position, commit) {
            // synchronize with the server - send insert command
            // call commit with parameter true if the synchronization with the server is successful 
            //and with parameter false if the synchronization failed.
            // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
            if(opt.updatetype=="saveall"){
                if(opt.inlineEdit == false || typeof opt.inlineEdit == 'undefined') {
                    var postData = new FormData($('#'+formId)[0]);
                    if(typeof defaultKey != 'undefined') {
                        $.each(defaultKey, function(i, v){
                            postData.append(i, v);
                        });
                    }
                    commit(true);
                    $('#'+formId+' .form-group').removeClass('has-error');
                    $('#'+formId+' .form-group .help-block').remove();
                    $("#"+showBoxId).hide();
                }
                else {
                    alert('hi');
                    commit(true);
                }
            }else{
                if(opt.inlineEdit == false || typeof opt.inlineEdit == 'undefined') {
                    var postData = new FormData($('#'+formId)[0]);
                    if(typeof defaultKey != 'undefined') {
                        $.each(defaultKey, function(i, v){
                            postData.append(i, v);
                        });
                    }
                    $.ajax({
                        url: addUrl,
                        type: 'POST',
                        data: postData,
                        async: false,
                        beforeSend: function () {
                            console.log("beforesend");
                        },
                        error: function (jqXHR, exception) {
                            commit(false);
                        },
                        success: function (data) {
                            if(data.msg == "success") {
                                $.each(data.data[0], function(i, v){
                                    rowdata[i]=data.data[0][i];
                                });
                                
                                commit(true);
                                $('#'+formId+' .form-group').removeClass('has-error');
                                $('#'+formId+' .form-group .help-block').remove();
                                $("#"+showBoxId).hide();
                            }
                            else {
                                commit(false);
                                if(typeof data.errorLog == 'object') {
                                    $.each(data.errorLog, function(i, v){
                                        $('#'+formId+' [name="'+i+'"]').parent('.form-group').addClass('has-error');
                                        $('#'+formId+' [name="'+i+'"]').parent('.form-group').append('<span class="help-block">'+v+'</span>');
                                    });
                                }
                            }
    
                            if(afterSave != null) {
                                afterSave(data);
                            }
                            return false;
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }
                else {
                    alert('hi');
                    commit(true);
                } 
            }
        },
        deleterow: function (rowid, commit) {
            // synchronize with the server - send delete command
            // call commit with parameter true if the synchronization with the server is successful 
            //and with parameter false if the synchronization failed.
            var rowdata = $("#"+gridId).jqxGrid('getrowdata', rowid);
            commit(true);
        },
        updaterow: function (rowid, newdata, commit) {
            // synchronize with the server - send update command
            // call commit with parameter true if the synchronization with the server is successful 
            // and with parameter false if the synchronization failed.
            if(opt.updatetype=="saveall"){
                if(opt.inlineEdit == false || typeof opt.inlineEdit == 'undefined') {
                    var postData = new FormData($('#'+formId)[0]);
                    commit(true);
                    $('#'+formId+' .form-group').removeClass('has-error');
                    $('#'+formId+' .form-group .help-block').remove();
                    $("#"+showBoxId).hide();
                }
                else {
                    var t = [];
                    if(typeof opt.beforeSave != 'undefined') {
                        t = opt.beforeSave(newdata);
                    }
                    if(t[0] == false) {
                        commit(false);
                    }
                    else {
                        $.ajax({
                            url: updateUrl,
                            type: 'POST',
                            data: t[1],
                            async: false,
                            beforeSend: function () {
                                
                            },
                            error: function (jqXHR, exception) {
                                commit(false);
                            },
                            success: function (data) {
                                if(data.msg == "success") {
                                    if(afterSave != null) {
                                        newdata = afterSave(data, rowid, newdata);
                                    }
                                    commit(true);
                                }
                                else {
                                    commit(false);
                                }
        
                                
                                return false;
                            }
                        });
                        commit(true);
                    }
                }
            }else{
                if(opt.inlineEdit == false || typeof opt.inlineEdit == 'undefined') {
                    var postData = new FormData($('#'+formId)[0]);
    
                    $.ajax({
                        url: updateUrl,
                        type: 'POST',
                        data: postData,
                        async: false,
                        beforeSend: function () {
                            
                        },
                        error: function (jqXHR, exception) {
                            commit(false);
                        },
                        success: function (data) {
                            if(data.msg == "success") {
                                $.each(data.data[0], function(i, v){
                                    newdata[i]=data.data[0][i];
                                });
                                commit(true);
                                $('#'+formId+' .form-group').removeClass('has-error');
                                $('#'+formId+' .form-group .help-block').remove();
                                $("#"+showBoxId).hide();
                            }
                            else {
                                commit(false);
                                if(typeof data.errorLog == 'object') {
                                    $.each(data.errorLog, function(i, v){
                                        $('#'+formId+' [name="'+i+'"]').parent('.form-group').addClass('has-error');
                                        $('#'+formId+' [name="'+i+'"]').parent('.form-group').append('<span class="help-block">'+v+'</span>');
                                    });
                                }
                            }
    
                            if(afterSave != null) {
                                afterSave(data);
                            }
                            return false;
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }
                else {
                    var t = [];
                    if(typeof opt.beforeSave != 'undefined') {
                        t = opt.beforeSave(newdata);
                    }
                    if(t[0] == false) {
                        commit(false);
                    }
                    else {
                        $.ajax({
                            url: updateUrl,
                            type: 'POST',
                            data: t[1],
                            async: false,
                            beforeSend: function () {
                                
                            },
                            error: function (jqXHR, exception) {
                                commit(false);
                            },
                            success: function (data) {
                                if(data.msg == "success") {
                                    if(afterSave != null) {
                                        newdata = afterSave(data, rowid, newdata);
                                    }
                                    commit(true);
                                }
                                else {
                                    commit(false);
                                }
        
                                
                                return false;
                            }
                        });
                        commit(true);
                    }
                }
            }
            
        }

    }
    var dataAdapter = new $.jqx.dataAdapter(source, { async: false, loadError: function (xhr, status, error) { alert('Error loading "' + source.url + '" : ' + error); } });
    var editrow = -1;

    $("#"+gridId).jqxGrid(
    {
        width: '100%',
        height: height,
        source: dataAdapter,
        pageable: false,
        columnsresize: true,
        columnsautoresize: true,
        selectionmode: selectionmode,
        showtoolbar: (typeof opt.showtoolbar == 'undefined')?false:opt.showtoolbar,
        clipboard: true,
        showaggregates: showaggregates,   
        showstatusbar: showaggregates,
        statusbarheight: 20,            
        enablebrowserselection: enablebrowserselection,
        editable: (typeof opt.inlineEdit == 'undefined')?false:opt.inlineEdit,
        rendergridrows: function (params) {
            return params.data;
        },
        columns: fieldData[1],
        rendertoolbar: function (toolbar) {
            var me = this;
            var container = $("<div id='toolbar_"+gridId+"' style='margin: 5px;'></div>");
            toolbar.append(container);

            if(opt.commonBtn == true) {
                var $addBtn = $('<button  type="button">'+transLang["addNewRow"] +'</button>');
                container.append($addBtn);

                var $delBtn = $('<button style="margin-left: 5px;" type="button" >'+transLang["deleteSelectRow"]+'</butoon>')
                container.append($delBtn);

                if(mainId == "") {
                    $addBtn.jqxButton({disabled: false});
                    $delBtn.jqxButton({disabled: false});
                }
                else {
                    $addBtn.jqxButton({disabled: false});
                    $delBtn.jqxButton({disabled: false});
                }
                

                // create new row.
                $addBtn.on('click', function () {
                    if(gridId=="imgGrid"){
                        return;
                    }
                    $('#'+gridId).jqxGrid('clearselection');
                    $('#'+formId).find('input:not(.noClear)').val('');
                    console.log(showBoxId);
                    $("#"+showBoxId).show();
                });

                // delete row.
                $delBtn.on('click', function () {
                    var selectedrowindex = $("#"+gridId).jqxGrid('getselectedrowindex');
                    if(selectedrowindex == -1) {
                        swal(transLang["msg1"], "", "warning");
                        return;
                    }
                    if (confirm("Are you sure you want to delete this item?") == true) {
                        var rowscount = $("#"+gridId).jqxGrid('getdatainformation').rowscount;
                        if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                            var id = $("#"+gridId).jqxGrid('getrowid', selectedrowindex);
                            var rowdata = $("#"+gridId).jqxGrid('getrowdata', selectedrowindex);
                            console.log(rowdata);
                            $("#"+gridId).jqxGrid('deleterow', id);
                            if(opt.updatetype !="saveall"){
                                if(rowdata.id>0){
                                    $("#"+gridId).jqxGrid('deleterow', id);
                                    $.get(delUrl+rowdata.id, {}, function(data){
                                        if(data.msg == "success") {
                                            $("#"+gridId).jqxGrid('deleterow', id);
                                        }
                                        else {
                                            commit(false);
                                        }
                        
                                        if(afterSave != null) {
                                            afterSave(data);
                                        }
                                    });
                                }
                            }
                        }
                    }
                });
            }

            if(custBtn != null) {
                $.each(custBtn, function(i, v){
                    var $custBtn = $('<button  type="button" style="margin-left: 5px;">'+custBtn[i]['title']+'</button>');
                    container.append($custBtn);

                    if(mainId == "") {
                        $custBtn.jqxButton({disabled: true});
                    }
                    else {
                        $custBtn.jqxButton({disabled: false});
                    }

                    $custBtn.on('click', function () {
                        opt.custBtn[i]['func']();
                    });
                });
            }
            

            
        },
    });
}

var loadingFunc = {
    show: function(){
        $("#overlay").fadeIn();
        $(".cssload-wraper").show();
    },
    hide: function(){
        $("#overlay").fadeOut();
        $(".cssload-wraper").hide();
    }
}
