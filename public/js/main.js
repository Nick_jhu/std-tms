
/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var tranConfig = {
  "mapsApiKey": "AIzaSyDMvdoJkRfAEfk6lZ2CbLvxjj0sfLwdZF0",
  "firebaseApiKey": "AIzaSyAoga1_Qddj-AROeeFqZJN67d_gWg4Mcaw",
  "firebaseDatabaseURL": "https://tmsapp-22e76.firebaseio.com/",
};

var app = firebase.initializeApp({
  apiKey: tranConfig.firebaseApiKey,
  databaseURL: tranConfig.firebaseDatabaseURL,
});

var database = app.database();

var icon = {
  path: car,
  scale: .7,
  strokeColor: 'white',
  strokeWeight: .10,
  fillOpacity: 1,
  fillColor: '#404040',
  offset: '5%',
  rotation: 270,
  anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
};

var markers = [];

database.ref('raw-locations/' + C_KEY).on('value', function(data) {
  // console.log(data.val());
  $('#loading').hide();

  var transports = data.val();
  transports = Object.keys(transports).map(function(id) {
    var transport = transports[id][1];

    transport.id = id;
    transport.power = Math.round(transport.power);
    transport.time = transport.time;//moment(transport.time).fromNow();
    transport.map = 'https://maps.googleapis.com/maps/api/staticmap?size=200x200'
        + '&markers=color:blue%7Clabel:' + transport.id + '%7C' + transport.lat
        + ',' + transport.lng + '&key=' + tranConfig.mapsApiKey + '&zoom=15';
    if(typeof transport.speed == "undefined") {
      transport.speed = 0;
    }
    else {
      transport.speed = Math.round(transport.speed);//Math.round(( transport.speed * 3600 ) / 1000);
    }
    
    return transport;
  });

  var html;
  if (!transports) {
    html = '<p class="empty">No transport locations available.</p>';
  } else {
    html = ejs.render($('#transports-template').html(), {transports: transports});
  }
  
  $('#transports').html(html);

  

  for(var i=0; i<transports.length; i++) {
    var result = [transports[i].lat, transports[i].lng];
    if(!markers[i]) {
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(transports[i].lat, transports[i].lng),
        map: map,
        icon: "https://cdn4.iconfinder.com/data/icons/shopping-colorful-flat-long-shadow/136/Shopping_icons-31-24-48.png",
        title: transports[i].id
      });
      markers.push(marker);
    }
    moveMarker(transports[i], markers[i]);
  }

  
  

});

var numDeltas = 100;
var delay = 10; //milliseconds
var i = 0;
var deltaLat;
var deltaLng;
function transition(result, marker){
  i = 0;
  //deltaLat = (result[0] - position[0])/numDeltas;
  //deltaLng = (result[1] - position[1])/numDeltas;
  moveMarker(marker);
}
var lastOpenedInfoWindow;
function moveMarker(transport, marker){
  //position[0] += deltaLat;
  //position[1] += deltaLng;
  var latlng = new google.maps.LatLng(transport.lat, transport.lng);
  marker.setPosition(latlng);

  var postData = {
    car_no: transport.id,
    lat: transport.lat,
    lng: transport.lng,
    user_id: transport.id
  };

  // $.post("http://core.standard-info.com/api/admin/trackingApi/sendGps", postData, function(data){
  //   console.log(data);
  // }, 'JSON');

  //var date = new Date(transport.time);
  //var dateFormat = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
  var infowindow = new google.maps.InfoWindow({
    content: '<table class="table table-bordered"><tr><td colspan="2">車號：'+transport.id+'</td></tr><tr><td>司機</td><td>'+transport.power+'%</td></tr><tr><td>司機電話</td><td>'+transport.speed+' km/h</td></tr><tr><td>更新時間</td><td>'+transport.time+'</td></tr></table>'
  });

  google.maps.event.clearListeners(marker, 'click');
  marker.addListener('click', function() {
    closeLastOpenedInfoWindow();
    infowindow.open(map, marker);
    lastOpenedInfoWindow = infowindow;
  });

  infowindows.push(infowindow);
}

function updatetMarker(transport, marker, markerIndex){
  var latlng = new google.maps.LatLng(transport.lat, transport.lng);
  marker.setPosition(latlng);
  let markerContent = '<table class="table table-bordered"><tr><td colspan="2">車號：'+transport.id+'</td></tr><tr><td>司機</td><td>'+transport.power+'%</td></tr><tr><td>司機電話</td><td>'+transport.speed+' km/h</td></tr><tr><td>更新時間</td><td>'+transport.time+'</td></tr></table>';
  infowindows[markerIndex].setContent(markerContent);
}

function closeLastOpenedInfoWindow() {
  if (lastOpenedInfoWindow) {
      lastOpenedInfoWindow.close();
  }
}