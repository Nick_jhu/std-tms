<?php
    namespace App;
    use Illuminate\Database\Eloquent\Model;

    class TsRecordModel extends Model {
        protected $table = 'mod_ts_record';
    }