<?php

namespace App\Models;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use App\Mail\EdiProblemMail;
use App\Mail\TrackingOMail;
use Illuminate\Support\Facades\Mail;
use App\Models\OrderamtDetailModel;
use DB;
use App\Models\TrackingModel;
use Excel;
use Illuminate\Support\Facades\Crypt;
use App\Models\BaseModel;
use App\Models\CalculateModel;
use Illuminate\Filesystem\FilesystemAdapter;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Ftp as FtpAdapter;
use League\Flysystem\Sftp\SftpAdapter as SFtpAdapter;
class OrderMgmtModel extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mod_order';
	// protected $primaryKey = 'cust_no';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $guarded = array('id');
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/
	public function dailysqlcheck() {
		$data = DB::table('sqlcheck')->where('enabled', 'Y')->get();
		foreach($data as $row) {
			$result = DB::select( 
				DB::raw($row->comment)
			);
			if(count($result) >0) {
				$html = @'<!DOCTYPE html>
				<html lang="en">
				<head>
					<meta charset="UTF-8">
					<meta name="viewport" content="width=device-width, initial-scale=1.0">
					<meta http-equiv="X-UA-Compatible" content="ie=edge">
					<title>Document</title>
				</head>
				<body>'.
					'<p>'.$row->remark.' 檢查到異常</p>.
				</body>
				</html>';
				Mail::send([], [], function ($message) use ($html) {
					$message->to("d1024242031@gmail.com")
						->bcc('anderson@standard-info.com')
						->subject('每日sql檢查')
						->setBody($html, 'text/html');
				});
			}
		}
		$html = @'<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<meta http-equiv="X-UA-Compatible" content="ie=edge">
			<title>Document</title>
		</head>
		<body>'.
			'<p>'.' 已檢查</p>.
		</body>
		</html>';
		Mail::send([], [], function ($message) use ($html) {
			$message->to("d1024242031@gmail.com")
				->subject('每日sql檢查')
				->setBody($html, 'text/html');
		});
		return ;
	}

	public function updateOrdStatusAndFinishDate($sysOrdNo, $ordStatus) {
		//更新訂單狀態
		\Log::info("updateOrdStatusAndFinishDate");
		\Log::info($ordStatus);
		\Log::info($sysOrdNo);
		$ord              = $this->where('sys_ord_no', $sysOrdNo)->first();
		$ordStatus_desc   = DB::table('bscode')->where('cd_type','ORDSTATUS')->where('cd',$ordStatus)->first();
		$ord->status      = $ordStatus;
		$ord->status_desc = $ordStatus_desc->cd_descp;
		$ord->finish_date = Carbon::now()->toDateTimeString();
		\Log::info($ordStatus);
		\Log::info($ordStatus_desc->cd_descp);
		\Log::info("updateOrdStatusAndFinishDate end");
		$ord->save();

		return;
	}

	public function updatePic() {
		//更新 訂單簽收異常照片欄位 目前沒有使用
		$data = DB::table('mod_file')->get();

		foreach($data as $row) {

			if($row->type_no == 'SIGNIN' || $row->type_no == 'FINISH') {
				DB::table('mod_order')->where('sys_ord_no', $row->ref_no)->update(['sign_pic' => 1]);
			}
			else {
				DB::table('mod_order')->where('sys_ord_no', $row->ref_no)->update(['error_pic' => 1]);
			}
			
		}

		return;
	}
	
	public function multiClose() {

        // $user = Auth::user();
		//結單
		$revertadta = DB::table('mod_order')
		->whereDate('created_at', '<=', '2020-06-30 23:59:00')
		->where("status","PICKED")
		->count();
        try{
			$OrderCloseMgmtModel = new OrderCloseMgmtModel();
			foreach($revertadta as $order){
				$delorder = OrderMgmtModel::find($order->id);
				$data = [
					'ord_id' => $order->id,
					'g_key' => $order->g_key,
					'c_key' => $order->c_key,
					's_key' => $order->s_key,
					'd_key' => $order->d_key,
					'ord_no' => $order->ord_no,
					'status' => "FINISHED",
					'loc_cros' => $order->loc_cros,
					'remark' => $order->remark,
					'dlv_cust_nm' => $order->dlv_cust_nm,
					'dlv_cust_no' => $order->dlv_cust_no,
					'dlv_addr' => $order->dlv_addr,
					'dlv_attn' => $order->dlv_attn,
					'dlv_tel' => $order->dlv_tel,
					'dlv_city_no' => $order->dlv_city_no,
					'dlv_city_nm' => $order->dlv_city_nm,
					'dlv_zip' => $order->dlv_zip,
					'dlv_area_id' => $order->dlv_area_id,
					'dlv_area_nm' => $order->dlv_area_nm,
					'dlv_lat' => $order->dlv_lat,
					'dlv_lng' => $order->dlv_lng,
					'pkg_num' => $order->pkg_num,
					'pkg_unit' => $order->pkg_unit,
					'etd' => $order->etd,
					'truck_cmp_no' => $order->truck_cmp_no,
					'truck_cmp_nm' => $order->truck_cmp_nm,
					'truck_no' => $order->truck_no,
					'driver' => $order->driver,
					'exp_reason' => $order->exp_reason,
					'exp_type' => $order->exp_type,
					'updated_by' => $order->updated_by,
					'created_by' => $order->created_by,
					'created_at' => $order->created_at,
					'updated_at' => $order->updated_at,
					'cbm' => $order->cbm,
					'dlv_no' => $order->dlv_no,
					'car_no' => $order->car_no,
					'trs_mode' => $order->trs_mode,
					'pick_cust_no' => $order->pick_cust_no,
					'pick_cust_nm' => $order->pick_cust_nm,
					'pick_addr' => $order->pick_addr,
					'pick_attn' => $order->pick_attn,
					'pick_tel' => $order->pick_tel,
					'pick_city_no' => $order->pick_city_no,
					'pick_city_nm' => $order->pick_city_nm,
					'pick_zip' => $order->pick_zip,
					'pick_area_id' => $order->pick_area_id,
					'pick_area_nm' => $order->pick_area_nm,
					'pick_lat' => $order->pick_lat,
					'pick_lng' => $order->pick_lng,
					'total_cbm' => $order->total_cbm,
					'total_cbm_unit' => $order->total_cbm_unit,
					'dlv_email' => $order->dlv_email,
					'pick_email' => $order->pick_email,
					'distance' => $order->distance,
					'car_type' => $order->car_type,
					'dlv_type' => $order->dlv_type,
					'amt' => $order->amt,
					'total_gw' => $order->total_gw,
					'cust_amt' => $order->cust_amt,
					'amt_remark' => $order->amt_remark,
					'ord_type_cd' => $order->ord_type_cd,
					'ord_type_nm' => $order->ord_type_nm,
					'wh_addr' => $order->wh_addr,
					'owner_cd' => $order->owner_cd,
					'owner_nm' => $order->owner_nm,
					'sys_ord_no' => $order->sys_ord_no,
					'error_remark' => $order->error_remark,
					'dlv_remark' => $order->dlv_remark,
					'pick_remark' => $order->pick_remark,
					'finish_date' => $order->finish_date,
					'owner_send_mail' => $order->owner_send_mail,
					'pick_send_mail' => $order->pick_send_mail,
					'dlv_send_mail' => $order->dlv_send_mail,
					'sign_pic' => $order->sign_pic,
					'error_pic' => $order->error_pic,
					'sys_etd' => $order->sys_etd,
					'cust_ord_no' => $order->cust_ord_no,
					'is_urgent' => $order->is_urgent,
					'temperate' => $order->temperate,
					'status_desc' => "配送完成",
					'temperate_desc' => $order->temperate_desc,
					'trs_mode_desc' => $order->trs_mode_desc,
					'car_type_desc' => $order->car_type_desc,
					'abnormal_remark' => $order->abnormal_remark,
					'pick_addr_info' => $order->pick_addr_info,
					'dlv_addr_info' => $order->dlv_addr_info,
					'collectamt' => $order->collectamt,
					'realnum' => $order->realnum,
					'is_ordered' => $order->is_ordered,
					'order_tally' => $order->order_tally,
					'wms_order_no' => $order->wms_order_no,
					'close_by' => "system",
					'close_date' => \Carbon::now(),
					'close_status' => 'Y',
				];
			    $OrderCloseMgmtModel->insertClose($data);
				// $delorder->delete();
			}
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        return response()->json(array('msg' => 'success'));
    }
	
	public function schedule_sendsyl_img() {
		//每月轉移圖片
		\Log::info("send schedule_sendsyl_img");

		$datecondition = DB::table('bscode')
        ->where('cd', 'sendsylimg')
        ->where('cd_type', 'SENDIMGTIME')
		->first();
		$year = "2020";
		$date = $year."-".$datecondition->value1."-".$datecondition->value2."";
		$date = $datecondition->value3;
		\Log::info($date);
		\Log::info($date." 23:59:59");
		// $fileDataids = DB::table('mod_file')
		// ->where('sendsyl', 'N')
		// ->orderBy('ref_no', 'asc')
		// ->pluck("ref_no")->toArray();
		$edidata = array();
		$closedata = DB::table('mod_order_close')
		->select("created_at","sys_ord_no")
		->where('g_key', 'SYL')
		->where('sendsyl', 'N')
		->whereDate('created_at', '<=', $date)
		->whereDate('created_at', '>=', "2020-09-01")
		->get()->toArray();;
		$orderdata = DB::table('mod_order')
		->select("created_at","sys_ord_no")
		->where('g_key', 'SYL')
		->where('sendsyl', 'N')
		->whereDate('created_at', '<=', $date)
		->whereDate('created_at', '>=', "2020-09-01")
		->get()->toArray();
		$edidata = array_merge($closedata, $orderdata);
		$k = 0;
		$count = 0;
		$oldSysOrdNo = "";
		
		if(count($edidata) > 0) {
			foreach($edidata as $row) {
				//一天最多六千多
				if($count>=6000){
					\Log::info("send limit finish");
					return ;
				}
				$now  = substr($row->created_at,0,8);
				$now  =  $prfer = str_replace('-','',$now);
				$directory = 'tmp';
				$fileData = DB::table('mod_file')
				->where('ref_no', $row->sys_ord_no)
				->orderBy('id', 'asc')
				->get();
				Storage::deleteDirectory($directory);
				Storage::makeDirectory($directory);
				$files = glob(storage_path('app/tmp/*'));
				foreach($fileData as $key => $row2) {

					$orderData = DB::table('mod_order')
					->where('sys_ord_no', $row2->ref_no)
					->first();
					if(!isset($orderData)){
						$orderData = DB::table('mod_order_close')
						->where('sys_ord_no', $row2->ref_no)
						->first();
					}
					$ord_no = $orderData->ord_no;
					$fileName = $row2->guid;
					$typeNo   = $row2->type_no;
					try {
						if($key==0){
							// \Log::info("first img");
							// Storage::disk('custom-ftp')->putFileAs('POD/',new File(Storage::path($fileName)),$ord_no.'.jpg');
							Storage::disk('syl-sftp')->putFileAs('TMS/'.$now."/",new File(Storage::path($fileName)),$row2->ref_no."_".$ord_no.'.jpg');
							$count++;
						}else{
							// \Log::info("multi img");
							// Storage::disk('custom-ftp')->putFileAs('POD/',new File(Storage::path($fileName)),$ord_no.'-'.$k.'.jpg');
							Storage::disk('syl-sftp')->putFileAs('TMS/'.$now."/",new File(Storage::path($fileName)),$row2->ref_no."_".$ord_no.'-'.$key.'.jpg');
							$count++;
						}
						// \Log::info($row2->guid);
						$this->update_edi_status_img($row2,'Y');
						DB::table('mod_file')
						->where('ref_no', $row->sys_ord_no)
						->where("guid", $row2->guid)
						->update([
							"sendsyl" => "Y"
						]);
						DB::table('mod_order_close')
						->where('sys_ord_no', $row->sys_ord_no)
						->update([
							"sendsyl" => "Y"
						]);
						DB::table('mod_order')
						->where('sys_ord_no', $row->sys_ord_no)
						->update([
							"sendsyl" => "Y"
						]);
						$nowforcheck = date('YmdHis');
						DB::table('mod_file_check')
						->insert([
							"guid"        => $row2->guid,
							"ref_no"      => $row2->ref_no,
							"type_no"     => $row2->type_no,
							"file_format" => $row2->file_format,
							"g_key"       => $row2->g_key,
							"c_key"       => $row2->c_key,
							"s_key"       => $row2->s_key,
							"d_key"       => $row2->d_key,
							"updated_at"  => Carbon::now()->toDateTimeString(),
							"created_at"  => Carbon::now()->toDateTimeString(),
							"updated_by"  => "system",
							"created_by"  => "system",
						]);
						// Storage::disk('local')->delete($fileName);	
					}
					catch(\Exception $e) {
						\Log::info('send img error');
						\Log::info($e->getMessage());
						\Log::info('send img error');
						DB::table('mod_file')
						->where('ref_no', $row->sys_ord_no)
						->where("guid", $row2->guid)
						->update([
							"sendsyl" => "N"
						]);
						// $this->update_edi_status_img($row2,'N');
						// return;
					} 
				}
			}
		}
		\Log::info("send finish");
		return;
	}

	public function updatefordrsize() {
		//更新圖片使用空間
        try{
            $file_size = 0;
            $loacl = Storage::disk('local')->allFiles();
            foreach( $loacl as $file)
            {
                $file_size += Storage::size($file);
            }
            DB::table('bscode')
            ->where("cd_type","FORDERSIZE")
            ->where("cd","fordersize")
            ->update([
                "value1"=>$file_size
            ]);
        }		
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return number_format($file_size/1024/1024);
        }
        
		return number_format($file_size/1024/1024);
	}


	public function autocustsendedi(){
		//自動發送訂單資訊ftp
		$now = date('YmdHis');
		$ownernm="";
		$owners = DB::table('mod_ts_order')->select("owner_cd")->where("is_send","N")->groupBy('owner_cd')->get();
		foreach($owners as $row){
			$owners = DB::table('sys_customers')->where("cust_no",$row->owner_cd)->where("g_key","SYL")->first();
			if(isset($owners)){
				$ownernm = $owners->cname;
			}
			try{
				Excel::create($ownernm."-".$now, function ($excel) use ($now,$row) {
					$excel->sheet($row->owner_cd, function ($sheet) use ($now,$row) {
						$owners = DB::table('sys_customers')->where("cust_no",$row->owner_cd)->where("g_key","SYL")->first();
						if(isset($owners)){
							$ownernm = $owners->cname;
						}
						$sheet->row(1, array(
							'貨主名稱','訂單號','客戶訂單編號', '倉庫訂單編號', '系統訂單號', '拋單日期',
							'訂單狀態敘述', '完成日期', '異常原因','異常備註'
						));
						$order= array();
						$sheet->setFontFamily('新細明體');
						// $sheet->setBorder('A1:F10', 'thin');
						$deliverynos = DB::table('mod_ts_order')->where("is_send","N")->where("owner_cd",$row->owner_cd)->get();
						foreach ($deliverynos as $key => $row2) {
							// $order = DB::table('mod_order')->where("sys_ord_no",$row2->sys_ord_no)->first();
							// $findate = "";
							// $expreason = "";
							// $abnormal_remark = "";
							// array_push($order, $row2->ord_no);
							if($row2->status_desc=="配送完成" ||$row2->status_desc=="拒收"){
								
							}else{
								$row2->finish_date ="";
							}
							// if($row2->status_desc=="配送發生問題"){
							// 	$expreason = $order->exp_reason;
							// 	$abnormal_remark = $order->abnormal_remark;
							// }
							// if($expreason=="誤刷完成"){
							// 	$expreason="";
							// }
							$sheet->row(($key + 2), array(
								$row2->owner_nm,$row2->ord_no." ",$row2->cust_ord_no,$row2->wms_ord_no,$row2->sys_ord_no,$row2->ord_created_at,
								$row2->status_desc,$row2->finish_date,$row2->error_remark,$row2->abnormal_remark
							)
						);
						$sheet->setCellValueExplicit("B".($key+2), $row2->ord_no, \PHPExcel_Cell_DataType::TYPE_STRING);
						}
						$sheet->setColumnFormat([		// 其他格式展示
							'B' => '@',	
						]);
						$sheet->getStyle('B1:D599')->getAlignment()->applyFromArray(
							array('horizontal' => 'left')
						);
					});	
				})->store('xls', storage_path('app/'));
				$patht =  Storage::disk('local')->getAdapter()->applyPathPrefix($ownernm."-".$now.'.xls');
				$sylcust  = DB::table('sys_customers')->where("cust_no",$row->owner_cd)->where("g_key","SYL")->first();
				$deliverynos = DB::table('mod_ts_order')->where("is_send","N")->where("owner_cd",$row->owner_cd)->get();
				//判斷客戶是要 mail 還是 ftp 還是都要
				if(isset($sylcust)){
					if($sylcust->ftptype=="EM"){

						if($sylcust->ftpcat=="FTP" && $sylcust->cust_no =="42866618"){
							$contents = Storage::disk('local')->get($ownernm."-".$now.'.xls');
							Storage::disk('IDL')->putFileAs('ToSY/',new File(Storage::path($ownernm."-".$now.'.xls')),$ownernm."-".$now.'.xls');
						}else{

							$filesystem = new Filesystem(new SftpAdapter([
								'driver'   => $sylcust->ftpcat,
								'host'     => $sylcust->ftppath,
								'port'     => $sylcust->ftpport,
								'username' => $sylcust->ftpuser,
								'password' => $sylcust->ftppassword,
								'root'     => $sylcust->ftpforder,
							]));
							$contents = Storage::disk('local')->get($ownernm."-".$now.'.xls');
							$filesystem->put($ownernm."-".$now.'.xls',$contents);

						}
						try{
							$sendmaildata = array($sylcust->cname);
							$sendData = array(
								'Time' => Carbon::now()->toDateTimeString(),
								'filepath' =>$patht,
								'data' => $sendmaildata,
							);
							$emails = explode(",",$sylcust->sendmail);
							Mail::to($emails)
							->send(new TrackingOMail($sendData));
						}catch(\Exception $e) {
							\Log::info($e->getLine());
							\Log::info($e->getMessage());
						} 
						// $emails = explode(",",'nick@standard-info.com,d1024242031@gmail.com');
					}else if($sylcust->ftptype=="E"){
						if($sylcust->ftpcat=="FTP" && $sylcust->cust_no =="42866618"){
							$contents = Storage::disk('local')->get($ownernm."-".$now.'.xls');
							Storage::disk('IDL')->putFileAs('ToSY/',new File(Storage::path($ownernm."-".$now.'.xls')),$ownernm."-".$now.'.xls');
						}else{
						$filesystem = new Filesystem(new SftpAdapter([
							'driver' => $sylcust->ftpcat,
							'host' => $sylcust->ftppath,
							'port'     => $sylcust->ftpport,
							'username' => $sylcust->ftpuser,
							'password' => $sylcust->ftppassword,
							'root' => $sylcust->ftpforder,
						]));
						$contents = Storage::disk('local')->get($ownernm."-".$now.'.xls');
						$filesystem->put($ownernm."-".$now.'.xls',$contents);
						}
					}else if($sylcust->ftptype=="M"){
						$sendmaildata = array($sylcust->cname);
						$sendData = array(
							'Time' => Carbon::now()->toDateTimeString(),
							'filepath' =>$patht,
							'data' => $sendmaildata,
						);
						// $emails = explode(",",'nick@standard-info.com,d1024242031@gmail.com');
						$emails = explode(",",$sylcust->sendmail);
						Mail::to($emails)
						->send(new TrackingOMail($sendData));
					}
				}
				foreach ($deliverynos as $key => $result) {
					DB::table('mod_ts_order')->where('id',$result->id)
					->update([
						'is_send'  => "Y",
						"send_at"=>Carbon::now()->toDateTimeString()
					]);
				}
			Storage::disk('local')->delete($ownernm."-".$now.'.xls');
			}catch(\Exception $e) {
				\Log::info("auto send fail");
				\Log::info($ownernm);
				\Log::info($e->getLine());
				\Log::info($e->getMessage());
			} 
		}


		return ;
	}

	
	public function clearapplogin() {
		//清空登入資料
		DB::table('oauth_access_tokens')
			->where('id',"!=",'0')
			->delete();
		return ;
	}

	private function updateModOrderGw() {
		$orderData = DB::connection('mysql::write')
		->table('mod_order')
		->where('g_key', 'HOM')
		->get();
		foreach ($orderData as $key => $row) {
			$ordId = $row->id;
			$detailData = DB::connection('mysql::write')->table('mod_order_detail')
			->select(DB::raw('SUM(gw) as total_gw'), DB::raw('SUM(pkg_num) as pkg_num'), DB::raw('SUM(cbm) as total_cbm'), DB::raw('sum(price*pkg_num) as total_price'))
			->where('ord_id', $ordId)
			->where('g_key', $row->g_key)
			->where('c_key', $row->c_key)
			->first();

			if(isset($detailData)) {
				OrderMgmtModel::where('id', $ordId)
				->where('g_key', $row->g_key)
				->where('c_key', $row->c_key)
				->update(['total_gw' => $detailData->total_gw, 'pkg_num' => $detailData->pkg_num, 'total_cbm' => $detailData->total_cbm, 'amt' => $detailData->total_price]);
			}
		}

        return;
    }


	public function mpushmessage(){
		// \Log::info("return mpushmessage");
		// return;
		$now = date('YmdHis');
		\Log::info("mpushmessage 123213");
		$data = DB::table('mod_mpush_log')
		->where('status',"=", "N")
		->get(); 
		DB::table('mod_mpush_log')
		->where('status',"=", "N")
		->update([
			"status"=>"T"
		]);
		foreach($data as $row){
			// $orddata =  DB::table('mod_order')
			// ->where('sys_ord_no', $row->sys_ord_no)
			// ->first(); 
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'http://61.20.32.60:6600/mpushapi/smssubmit?xml='.$row->content,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_HTTPHEADER => array(
					'Content-Type: application/x-www-form-urlencoded'
				),
			));
			$response  = curl_exec($curl);
			curl_close($curl);
			$xml_array = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
			try {
				DB::table('mod_mpush_log')
				->where("id",$row->id)
				->update([
					"status"=>"Y",
					'resultcode' => $xml_array->ResultCode,
					'resulttext' => $xml_array->ResultText,
					'messageid'  => $xml_array->MessageId,
				]);
				$orddata =  DB::table('mod_order')
				->where('sys_ord_no', $row->sys_ord_no)
				->update([
					"sendmessage" => "已發送",
					"updated_at"  => $now
				]);
			}catch(\Exception $e) {
				\Log::info("CHECK mpushmessage");
				\Log::info($e->getLine());
				\Log::info($e->getMessage());
				DB::table('mod_mpush_log')
				->where("id",$row->id)
				->update([
					"status"     => "F",
					"updated_at" => $now
				]);
			}  
		}
		return ;
	}

	public function smsquerydrreq(){
		$response = "";
		$now = date('YmdHis');
		$xml_array =array();
		$data = DB::table('mod_mpush_log')
		->where('status',"!=", "delivered")
		->where('count',"<=", 3)
		->where('created_at', '>=' ,'2021-11-01 00:00:00')
		->get();

		foreach($data as $row){
			$strxml = "<?xml version='1.0' encoding='UTF-8'?>". 
			"<SmsQueryDrReq>".
			"<SysId>SUNYOUNG</SysId >". 
			"<MessageId>".$row->messageid."</MessageId>".
			"<DestAddress>".$row->phone."</DestAddress>". 
			"</SmsQueryDrReq>"
			;
			$postdata = urlencode($strxml);

			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'http://61.20.32.60:6600/mpushapi/smsquerydr?xml='.$postdata,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_HTTPHEADER => array(
				'Content-Type: application/x-www-form-urlencoded'
				),
			));
			$response  = curl_exec($curl);
			$xml_array = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
			$result = json_encode($xml_array);
			// dd($xml_array->Receipt->ErrorCode=="000");
			if($xml_array->ResultCode=="00000" || $xml_array->Receipt->ErrorCode=="00000"){
				DB::table('mod_mpush_log')
				->where("id",$row->id)
				->update([
					"status"=>$xml_array->Receipt->DeliveryStatus,
					"count"=>($row->count+1)
				]);
				$orddata =  DB::table('mod_order')
				->where('sys_ord_no', $row->sys_ord_no)
				->update([
					"sendmessage" => "已發送",
					"count"=>($row->count+1),
					"updated_at"  => $now
				]);
			}else{
				\Log::error("REGET MESSAGE ERROR ");
				\Log::error($result);
				\Log::error("REGET MESSAGE ERROR END");
				$orddata =  DB::table('mod_order')
				->where('sys_ord_no', $row->sys_ord_no)
				->update([
					"sendmessage" => "發送失敗",
					"count"       => ($row->count+1),
					"updated_at"  => $now
				]); 
			}
		}
		return ;
	}
	
	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------


	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
