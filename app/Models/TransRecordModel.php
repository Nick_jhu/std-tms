<?php

namespace App\Models;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class TransRecordModel extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mod_ts_record';
	protected $primaryKey = 'id';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $guarded = array('id');
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/
	public function createRecord($obj) {

		$this->insertRecord($obj);

	}

	public function chkIsExist($ts_no, $ref_no1) {
		$this_query = DB::table($this->table);
		$cnt = $this_query->where('ts_no', $ts_no)->where('ref_no1', $ref_no1)->count();

		if($cnt > 0) {
			return true;
		}

		return false;
	}

	public function insertRecord($obj) {
		
		$now = date("Y-m-d H:i:s");
		DB::table('mod_ts_record')->insert([
			'ts_no'   => $obj["ts_no"],
			'ts_type' => $obj["ts_type"],
			'sort'    => $obj["sort"],
			'ts_name' => $obj["ts_name"],
			'ts_desc' => $obj["ts_desc"],
			'ref_no1' => $obj["ref_no1"],
			'ref_no2' => $obj["ref_no2"],
			'ref_no3' => $obj["ref_no3"],
			'ref_no4' => $obj["ref_no4"],
			'g_key'   => $obj["g_key"],
			'c_key'   => $obj["c_key"],
			'd_key'   => $obj["s_key"],
			's_key'   => $obj["d_key"],
			'created_at' => $now,
			'updated_at' => $now,
			'created_by' => $obj["created_by"],
			'updated_by' => $obj["updated_by"]
		]);
		$ordData = DB::table('mod_order')
		->select('ord_no')
		->where('sys_ord_no', $obj["ref_no4"])
		->first();
		if(isset($ordData)) {
			$ordall = DB::table('mod_order')
			->where('sys_ord_no', $obj["ref_no4"])
			->first();
			$statunm="";
			if($ordall->status_desc=="尚未安排"){
				$statunm = "訂單處理中";
				return ;
			}
			if($ordall->status_desc=="已派單未出發"){
				$statunm = "待出貨";
			}
			if($ordall->status_desc=="出發"){
				$statunm = "運送中";
			}
			if($ordall->status_desc=="拒收"){
				$statunm = "拒收";
			}
			if($ordall->status_desc=="配送發生問題"){
				$statunm = "配送發生問題";
			}
			if($ordall->status_desc=="配送完成"){
				$statunm = "配送完成";
			}
			$tsrecord = DB::table('mod_ts_order')
			->where('sys_ord_no', $obj["ref_no4"])
			->where('status_desc', $statunm)
			->where('owner_cd', $ordall->owner_cd)
			->count();
			$custdata = DB::table('sys_customers')
			->where('cust_no', $ordall->owner_cd)
			->whereIn('ftptype',["E","EM","M"])
			->count();
			if( $custdata!=0 && $statunm!="" ){
				DB::table('mod_ts_order')
				->insert([
					"owner_cd"        => $ordall->owner_cd,
					"owner_nm"        => $ordall->owner_nm,
					"ord_no"          => $ordall->ord_no,
					"cust_ord_no"     => $ordall->cust_ord_no,
					"wms_ord_no"      => $ordall->wms_order_no,
					"sys_ord_no"      => $ordall->sys_ord_no,
					"ord_created_at"  => $ordall->created_at,
					"status_desc"     => $statunm,
					"finish_date"     => Carbon::now(),
					"exp_reason"      => "",
					"error_remark"    => "",
					"status"          => $ordall->status,
					"abnormal_remark" => "",
					"is_send"         => "N",
					"updated_at"      => Carbon::now(),
					"updated_by"      => "SYSTEM",
					"created_at"      => Carbon::now(),
					"created_by"      => "SYSTEM",
					"g_key"           => $obj["g_key"],
					"c_key"           => $obj["c_key"],
					"s_key"           => $obj["s_key"],
					"d_key"           => $obj["d_key"],
				]);
			}
		}

	}

    

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
    */


	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}