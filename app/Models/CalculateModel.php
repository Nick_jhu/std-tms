<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CalculateModel extends Model
{
    public function calculateAmt($id) {
        $ordData = DB::table('mod_order')->where('id', $id)->first();
        $amt = 0;
        try {
            if(isset($ordData)) {
                //判斷類型進行不同計價邏輯
                //目前可以使用的只有
                //Piece Cbm Item 三項
                switch($ordData->trs_mode) {
                    case 'C':
                    case 'C01':
                    case 'C02':
                        $amt = $this->carCalculate($ordData->car_type, 'C', $ordData->distance, $ordData->c_key);
                    break;
                    case 'N':
                    case 'N02':
                        $amt = $this->nonCarCalculate($ordData->pkg_num, $ordData->temperate, $ordData->trs_mode, $ordData->c_key);
                    break;
                    case 'N01':
                        $amt = $this->expressCalculate($ordData->total_gw, $ordData->owner_cd, $ordData->c_key);
                    break;
                    case 'Piece':
                        $amt = $this->expressPiece($ordData->pkg_num,$ordData->temperate, $ordData->trs_mode, $ordData->owner_cd, $ordData->c_key);
                    break;
                    case 'Cbm':
                        $amt = $this->expressCbm($id,$ordData->temperate, $ordData->trs_mode, $ordData->owner_cd, $ordData->c_key);
                    break;
                    case 'Gw':
                        $amt = $this->expressGW($id,$ordData->temperate, $ordData->trs_mode, $ordData->owner_cd, $ordData->c_key);
                    break;
                    case 'Item':
                        $amt = $this->expressItem($id,$ordData);
                    break;
                    default:
                    break;
                }
            }
        }catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
        }

        return $amt;
    }

    public function carCalculate($carType, $trsMode, $distance, $c_key) {
        if(!isset($distance)) {
            return 0;
        }
        $amt = 0;

        if(!isset($distance)) {
            $distance = 0;
        }
        //$distance = round(($distance/1000), 1);
        $feeData = DB::table('sys_ref_fee')
                        ->where('fee_from', '<=', $distance)
                        ->where('fee_to', '>=', $distance)
                        ->where('car_type_cd', $carType)
                        ->where('type', $trsMode)
                        ->where('c_key', $c_key)
                        ->first();

        if(isset($feeData)) {
            $regularFee = $feeData->regular_fee;
            if(isset($regularFee)) {
                $amt = ($distance * $feeData->fee) + $regularFee;
            }
            else {
                $amt = $feeData->fee;
            }
        }

        return round($amt);
    }

    public function nonCarCalculate($pkgNum, $temperate, $trsMode, $c_key) {
        $amt = 0;

        while(1) {
            if($pkgNum <= 0) {
                break;
            }
            $feeData = DB::table('sys_ref_fee')
                        ->where('car_type_cd', $temperate)
                        ->where('type', $trsMode)
                        ->where('c_key', $c_key)
                        ->orderBy('fee_from', 'asc')
                        ->get();

            if(count($feeData) > 0) {
                foreach($feeData as $key=>$row) {
                    if($pkgNum > $row->fee_to) {
                        $amt = $amt + ($row->fee_to * $row->fee);
                        $pkgNum = $pkgNum - $row->fee_to;
                    }
                    else {
                        $amt = $amt + ($pkgNum * $row->fee);
                        $pkgNum = $pkgNum - $row->fee_to;
                    }
                }
            }
            else {
                break;
            }

        }
        return $amt;
    }
    public function expressPiece($pkg_num,$temperate,$trs_mode,$custCd, $c_key) {
        //計件
        $now = date('Ymd');
        $amt = 0;
        $feeData = DB::table('sys_ref_fee')
                        ->where('cust_cd', $custCd)
                        ->where('type', 'Piece')
                        ->where('c_key', $c_key)
                        ->where('temperate', $temperate)
                        ->where('start_date','<=',$now)
                        ->where('end_date','>=',$now)
                        ->where('fee_from','<=',$pkg_num)
                        ->where('fee_to','>=',$pkg_num)
                        ->first();

        if(isset($feeData)) {
            //數量*計價金額
            $amt = $pkg_num * $feeData->fee;
            
            if($amt > 0 && $amt < $feeData->regular_fee) {
                $amt = $feeData->regular_fee;
            }
        }
        return round($amt);
    }

    public function expressCbm($id,$temperate,$trs_mode,$custCd, $c_key) {
        //材積
        $now = date('Y-m-d');
        $amt = 0;
        $tmpamt = 0;
        $tmpamt_m = 0;
        $feeData =array();
        $ordData = DB::table('mod_order_detail')
        ->where('ord_id', $id)
        ->get();
        try {
            $maxfeeData = DB::table('sys_ref_fee')
            ->where('cust_cd', $custCd)
            ->where('type', 'Cbm')
            ->where('c_key', $c_key)
            ->where('temperate', $temperate)

            ->orderBy('fee_to', 'desc')
            ->first();
            if(isset($maxfeeData)) {
            foreach($ordData as $key=>$row) {
                if($row->cbm > $maxfeeData->fee_to){
                    //先算出超出多少材積 然後 算每超出多少的金額 最後在乘數量
                    $overnum = $row->cbm - $maxfeeData->fee_to;
                    if($overnum % $maxfeeData->overcbm==0){
                        $overamt = $overnum / $maxfeeData->overcbm ;
                    }else{
                        $overamt = ($overnum / $maxfeeData->overcbm)+1 ;
                    }
                    $finalamt =  $maxfeeData->fee+($overamt * $maxfeeData->overamt);
                    $tmpamt_m =  $finalamt * $row->pkg_num;
                    if($tmpamt_m > 0 && $tmpamt_m < $maxfeeData->regular_fee) {
                        $tmpamt_m = $maxfeeData->regular_fee;
                    }
                    $amt  = $amt +$tmpamt_m;
                }else{
                    $feeData = DB::table('sys_ref_fee')
                    ->where('cust_cd', $custCd)
                    ->where('type', 'Cbm')
                    ->where('c_key', $c_key)
                    ->where('temperate', $temperate)
                    ->wheredate('start_date','<=',$now)
                    ->wheredate('end_date','>=',$now)
                    ->where('fee_from','<=',$row->cbm)
                    ->where('fee_to','>=',$row->cbm)
                    ->first();
                    if(isset($feeData)){
                        if($amt==0){
                            $amt = $row->pkg_num * $feeData->fee;
                            if($amt > 0 && $amt < $feeData->regular_fee) {
                                $amt = $feeData->regular_fee;
                            }
                        }else{
                            $tmpamt = $row->pkg_num * $feeData->fee;
                            if($tmpamt > 0 && $tmpamt < $feeData->regular_fee) {
                                $tmpamt = $feeData->regular_fee;
                            }
                        }
                        $amt  = $amt +$tmpamt;
                    }
                }
            }

            }
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'error_log' => $e->getLine()]);
        }
        return round($amt);
    }
    public function expressItem($id,$ordData) {
        //商品料號
        $now = date('Ymd');
        $amt = 0;
        $orddetail = DB::table('mod_order_detail')
        ->where('ord_id', $id)
        ->get();
        
        foreach($orddetail as $key=>$row) {
            $goodsdata = DB::table('mod_goods')
            ->where('owner_cd', $ordData->owner_cd)
            ->where('goods_no', $row->goods_no)
            ->first();
            if(isset($goodsdata)){
                 //數量*商品建檔金額
                $amt += ($row->pkg_num*$goodsdata->price);
            }else{

            }
        }
        return round($amt);
    }
    public function expressGW($id,$temperate,$trs_mode,$custCd, $c_key) {
        $now = date('Ymd');
        $amt = 0;
        $tmpamt = 0;
        $tmpamt_m = 0;
        $ordData = DB::table('mod_order_detail')
        ->where('ord_id', $id)
        ->get();
        $maxfeeData = DB::table('sys_ref_fee')
                        ->where('cust_cd', $custCd)
                        ->where('type', 'gw')
                        ->where('c_key', $c_key)
                        ->where('temperate', $temperate)
                        ->where('start_date','<=',$now)
                        ->where('end_date','>=',$now)
                        ->orderBy('fee_to', 'desc')
                        ->first();
        if(isset($maxfeeData)) {
            foreach($ordData as $key=>$row) {
                if($row->gw > $maxfeeData->fee_to){
                    //先算出超出多少材積 然後 算每超出多少的金額 最後在乘數量
                    $overnum = $row->gw - $maxfeeData->fee_to;
                    if($overnum % $maxfeeData->overcbm==0){
                        $overamt = $overnum / $maxfeeData->overcbm ;
                    }else{
                        $overamt = ($overnum / $maxfeeData->overcbm)+1 ;
                    }
                    $finalamt =  $maxfeeData->fee+($overamt * $maxfeeData->overamt);
                    $tmpamt_m =  $finalamt * $row->pkg_num;
                    if($tmpamt_m > 0 && $tmpamt_m < $maxfeeData->regular_fee) {
                        $tmpamt_m = $maxfeeData->regular_fee;
                    }
                    $amt  = $amt +$tmpamt_m;
                }else{
                    $feeData = DB::table('sys_ref_fee')
                    ->where('cust_cd', $custCd)
                    ->where('type', 'gw')
                    ->where('c_key', $c_key)
                    ->where('temperate', $temperate)
                    ->where('start_date','<=',$now)
                    ->where('end_date','>=',$now)
                    ->where('fee_from','<=',$row->gw)
                    ->where('fee_to','>=',$row->gw)
                    ->first();
                    if($amt==0){
                        $amt = $row->pkg_num * $feeData->fee;
                        if($amt > 0 && $amt < $feeData->regular_fee) {
                            $amt = $feeData->regular_fee;
                        }
                    }else{
                        $tmpamt = $row->pkg_num * $feeData->fee;
                        if($tmpamt > 0 && $tmpamt < $feeData->regular_fee) {
                        $tmpamt = $feeData->regular_fee;
                        }
                    }
                    $amt  = $amt +$tmpamt;
                }
            }
        }
        return round($amt);
    }

    public function expressCalculate($gw,$custCd, $c_key) {
        if(!isset($gw)) {
            return 0;
        }
        $amt = 0;
        $feeData = DB::table('sys_ref_fee')
                        ->where('fee_from', '<=', $gw)
                        ->where('fee_to', '>=', $gw)
                        ->where('cust_cd', $custCd)
                        ->where('type', 'N01')
                        ->where('c_key', $c_key)
                        ->first();

        if(isset($feeData)) {
            $amt = $feeData->fee * $gw;
            
            if($amt > 0 && $amt < $feeData->regular_fee) {
                $amt = $feeData->regular_fee;
            }
        }
        else {
            $feeData = DB::table('sys_ref_fee')
                        ->where('fee_from', '<=', $gw)
                        ->where('fee_to', '>=', $gw)
                        ->whereNull('cust_cd')
                        ->where('type', 'N01')
                        ->where('c_key', $c_key)
                        ->first();

            if(isset($feeData)) {
                $amt = $feeData->fee * $gw;
            }

            if($amt > 0 && $amt < $feeData->regular_fee) {
                $amt = $feeData->regular_fee;
            }
        }
        return round($amt);
    }
}
