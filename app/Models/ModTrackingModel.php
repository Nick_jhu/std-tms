<?php
namespace App\Models;
use Moloquent;
use DB;

class ModTrackingModel extends Moloquent{    
    protected $connection = 'mongodb';  //库名    
    protected $collection = 'mod_car_real_data';     //文档名    
    protected $primaryKey = '_id';    //设置id   
    protected $guarded = array('id'); 

    public function insertGps($obj) {

        DB::connection('mongodb')       //选择使用mongodb
        ->collection('mod_car_real_data')           //选择使用users集合
        ->insert($obj);
        
        return true;

        //$res = DB::connection('mongodb')->collection('mod_car_real_data')->get();   //查询所有数据
       // dd($res); 

    }
    public function clear_gps() {
        DB::connection('mongodb')//选择使用mongodb
        ->collection('mod_car_real_data') //选择使用users集合
        // ->where('updated_at', '<', $yesterday)
        ->delete();
        return true;
    }
    public function update_gps() {
        $gpsdata = DB::table('mod_car_real_data')
        // ->limit(500000)
        // ->where('id','>','13753729')
        ->get();

        // DB::connection('mongodb')//选择使用mongodb
        // ->collection('mod_car_real_data') //选择使用users集合
        // // ->where('updated_at', '<', $yesterday)
        // ->delete();

        $insertArray = array();
        foreach($gpsdata as $key=>$row)
        {
            array_push($insertArray, array(
                'id' => $row->id,
                'car_no' => $row->car_no,
                'dlv_no' => $row->dlv_no,
                'driver_no' => $row->driver_no,
                'driver_nm' => $row->driver_nm,
                'person_no' => $row->person_no,
                'person_nm' => $row->person_nm,
                'created_at' => $row->created_at,
                'updated_at' => $row->updated_at,
                'updated_by' => $row->updated_by,
                'created_by' => $row->created_by,
                'g_key' => $row->g_key,
                'c_key' => $row->c_key,
                's_key' => $row->s_key,
                'd_key' => $row->d_key,
                'lat' => $row->lat,
                'lng' => $row->lng,
                'location_time' => $row->location_time,
                'speed' => $row->speed,
                'status' => $row->status,
                'addr' => $row->addr,
                'cust_nm' => $row->cust_nm,
            ));

            if($key % 500 == 0 && $key >= 500)
            {
                DB::connection('mongodb')       //选择使用mongodb
                ->collection('mod_car_real_data')           //选择使用users集合
                ->insert($insertArray);

                $insertArray = array();
            }
        }
            DB::connection('mongodb')       //选择使用mongodb
            ->collection('mod_car_real_data')           //选择使用users集合
            ->insert($insertArray);
            $insertArray = array();


    }
}