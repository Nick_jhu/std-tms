<?php
namespace App\Models;
use Moloquent;
use DB;

class ModOrderDetailModel extends Moloquent{    
    protected $connection = 'mongodb';  //库名    
    protected $collection = 'mod_order_detail_view';     //文档名    
    protected $primaryKey = '_id';    //设置id   
    protected $guarded = array('id'); 
    //protected $fillable = ['id', 'name', 'phone'];  //设置字段白名单

    public function updateOrder() {
        $yesterday = date('Ymd',strtotime("-1 days"));
        $ords = DB::table('mod_order_detail_view')->get();


        DB::connection('mongodb')//选择使用mongodb
        ->collection('mod_order_detail_view') //选择使用users集合
        // ->where('updated_at', '<', $yesterday)
        ->delete();

        $insertArray = array();
        foreach($ords as $key=>$row)
        {
            array_push($insertArray, array(
                'status' => $row->status,
                'sys_ord_no' => $row->sys_ord_no,
                'is_urgent' => $row->is_urgent,
                'ord_no' => $row->ord_no,
                'etd' => $row->etd,
                'owner_nm' => $row->owner_nm,
                'amt' => $row->amt,
                'cust_amt' => $row->cust_amt,
                'total_pkg_num' => $row->total_pkg_num,
                'amt_remark' => $row->amt_remark,
                'created_at' => $row->created_at,
                'cust_ord_no' => $row->cust_ord_no,
                'goods_no' => $row->goods_no,
                'goods_nm' => $row->goods_nm,
                'goods_no2' => $row->goods_no2,
                'pkg_num' => $row->pkg_num,
                'pkg_unit' => $row->pkg_unit,
                'sn_no' => $row->sn_no,
                'gw' => $row->gw,
                'gwu' => $row->gwu,
                'cbm' => $row->cbm,
                'cbmu' => $row->cbmu,
                'l' => $row->l,
                'w' => $row->w,
                'h' => $row->h,
                'g_key' => $row->g_key,
                'c_key' => $row->c_key,
                's_key' => $row->s_key,
                'd_key' => $row->d_key,
            ));

            if($key % 500 == 0 && $key >= 500)
            {
                DB::connection('mongodb')       //选择使用mongodb
                ->collection('mod_order_detail_view')           //选择使用users集合
                ->insert($insertArray);

                $insertArray = array();
            }
        }
            DB::connection('mongodb')       //选择使用mongodb
            ->collection('mod_order_detail_view')           //选择使用users集合
            ->insert($insertArray);
            $insertArray = array();

    }
}