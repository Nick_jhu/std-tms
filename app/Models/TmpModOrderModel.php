<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class TmpModOrderModel extends Model {

	use CrudTrait;

    /*d
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'tmp_mod_order';
	// protected $primaryKey = 'id';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $guarded = array('id');
	//protected $fillable = ['dlv_cust_nm','etd', 'driver','remark','ord_no','dlv_attn','pkg_num','total_cbm','dlv_addr','dlv_tel'];
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	public function sumGw() {
		$od = DB::table('mod_order')->where('g_key', '4TH')->get();
		
		foreach($od as $v) {
			$id = $v->id;
			$dd = DB::table('mod_order_detail')->where('ord_id', $id)->get();
			$sum = 0;
			foreach($dd as $row) {
				$gw = $row->gw;
				$pkg_num = $row->pkg_num;
				
				$ttl = $gw * $pkg_num;

				$sum += $ttl;
			}

			DB::table('mod_order')->where('id', $id)->update(['total_gw' => $sum]);
		}
		
	}

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}