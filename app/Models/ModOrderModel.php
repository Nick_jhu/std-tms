<?php
namespace App\Models;
use Moloquent;
use DB;

class ModOrderModel extends Moloquent{    
    protected $connection = 'mongodb';  //库名    
    protected $collection = 'mod_order';     //文档名    
    protected $primaryKey = '_id';    //设置id   
    protected $guarded = array('id'); 
    //protected $fillable = ['id', 'name', 'phone'];  //设置字段白名单

    public function insertOrder() {
        $yesterday = date('Ymd',strtotime("-1 days"));
        $ords = DB::table('mod_order')->where('created_at','>',$yesterday)->get();
        $insertArray = array();
        foreach($ords as $key=>$row)
        {
            array_push($insertArray, array(
                'id' => $row->id,
                'status' => $row->status,
                'sys_ord_no' => $row->sys_ord_no,
                'ord_no' => $row->ord_no,
                'dlv_no' => $row->dlv_no,
                'trs_mode' => $row->trs_mode,
                'owner_nm' => $row->owner_nm,
                'owner_cd' => $row->owner_cd,
                'cust_ord_no' => $row->cust_ord_no,
                'is_urgent' => $row->is_urgent,
                'dlv_cust_nm' => $row->dlv_cust_nm,
                'dlv_info' => $row->dlv_zip.$row->dlv_city_nm.$row->dlv_area_nm.$row->dlv_addr,
                'dlv_attn' => $row->dlv_attn,
                'dlv_tel' => $row->dlv_tel,
                'dlv_email' => $row->dlv_email,
                'dlv_remark' => $row->dlv_remark,
                'pick_cust_nm' => $row->pick_cust_nm,
                'pick_info' => $row->pick_zip.$row->pick_city_nm.$row->pick_area_nm.$row->pick_addr,
                'pick_attn' => $row->pick_attn,
                'pick_tel' => $row->pick_tel,
                'pick_email' => $row->pick_email,
                'pick_remark' => $row->pick_remark,
                'truck_cmp_nm' => $row->truck_cmp_nm,
                'car_no' => $row->car_no,
                'driver' => $row->driver,
                'car_type' => $row->car_type,
                'pkg_num' => $row->pkg_num,
                'total_cbm' => $row->total_cbm,
                'total_gw' => $row->total_gw,
                'distance' => $row->distance,
                'amt' => $row->amt,
                'cust_amt' => $row->cust_amt,
                'amt_remark' => $row->amt_remark,
                'exp_reason' => $row->exp_reason,
                'updated_by' => $row->updated_by,
                'created_at' => $row->created_at,
                'finish_date' => $row->finish_date,
                'dlv_addr_info' => $row->dlv_addr_info,
                'pick_addr_info' => $row->pick_addr_info,
                'temperate_desc' => $row->temperate_desc,
                'trs_mode_desc' => $row->trs_mode_desc,
                'car_type_desc' => $row->car_type_desc,
                'status_desc' => $row->status_desc,
                'g_key' => $row->g_key,
                'c_key' => $row->c_key,
                's_key' => $row->s_key,
                'd_key' => $row->d_key,
            ));

            if($key % 500 == 0 && $key >= 500)
            {
                DB::connection('mongodb')       //选择使用mongodb
                ->collection('mod_order')           //选择使用users集合
                ->insert($insertArray);

                $insertArray = array();
            }
        }
            DB::connection('mongodb')       //选择使用mongodb
            ->collection('mod_order')           //选择使用users集合
            ->insert($insertArray);
            $insertArray = array();
        

        //$res = DB::connection('mongodb')->collection('mod_order')->get();   //查询所有数据
       // dd($res); 

    }
    public function updateOrder() {
        $yesterday = date('Ymd',strtotime("-1 days"));
        $ords = DB::table('mod_order')->get();


        DB::connection('mongodb')//选择使用mongodb
        ->collection('mod_order') //选择使用users集合
        // ->where('updated_at', '<', $yesterday)
        ->delete();

        $insertArray = array();
        foreach($ords as $key=>$row)
        {
            array_push($insertArray, array(
                'id' => $row->id,
                'status' => $row->status,
                'sys_ord_no' => $row->sys_ord_no,
                'ord_no' => $row->ord_no,
                'dlv_no' => $row->dlv_no,
                'trs_mode' => $row->trs_mode,
                'owner_nm' => $row->owner_nm,
                'owner_cd' => $row->owner_cd,
                'cust_ord_no' => $row->cust_ord_no,
                'is_urgent' => $row->is_urgent,
                'dlv_cust_nm' => $row->dlv_cust_nm,
                'dlv_info' => $row->dlv_zip.$row->dlv_city_nm.$row->dlv_area_nm.$row->dlv_addr,
                'dlv_attn' => $row->dlv_attn,
                'dlv_tel' => $row->dlv_tel,
                'dlv_email' => $row->dlv_email,
                'dlv_remark' => $row->dlv_remark,
                'pick_cust_nm' => $row->pick_cust_nm,
                'pick_info' => $row->pick_zip.$row->pick_city_nm.$row->pick_area_nm.$row->pick_addr,
                'pick_attn' => $row->pick_attn,
                'pick_tel' => $row->pick_tel,
                'pick_email' => $row->pick_email,
                'pick_remark' => $row->pick_remark,
                'truck_cmp_nm' => $row->truck_cmp_nm,
                'car_no' => $row->car_no,
                'driver' => $row->driver,
                'car_type' => $row->car_type,
                'pkg_num' => $row->pkg_num,
                'total_cbm' => $row->total_cbm,
                'total_gw' => $row->total_gw,
                'distance' => $row->distance,
                'amt' => $row->amt,
                'cust_amt' => $row->cust_amt,
                'amt_remark' => $row->amt_remark,
                'exp_reason' => $row->exp_reason,
                'updated_by' => $row->updated_by,
                'created_at' => $row->created_at,
                'finish_date' => $row->finish_date,
                'dlv_addr_info' => $row->dlv_addr_info,
                'pick_addr_info' => $row->pick_addr_info,
                'temperate_desc' => $row->temperate_desc,
                'trs_mode_desc' => $row->trs_mode_desc,
                'car_type_desc' => $row->car_type_desc,
                'status_desc' => $row->status_desc,
                'g_key' => $row->g_key,
                'c_key' => $row->c_key,
                's_key' => $row->s_key,
                'd_key' => $row->d_key,
            ));

            if($key % 500 == 0 && $key >= 500)
            {
                DB::connection('mongodb')       //选择使用mongodb
                ->collection('mod_order')           //选择使用users集合
                ->insert($insertArray);

                $insertArray = array();
            }
        }
            DB::connection('mongodb')       //选择使用mongodb
            ->collection('mod_order')           //选择使用users集合
            ->insert($insertArray);
            $insertArray = array();

    }
}