<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;

class CommonModel extends Model {

    public static  function processData($request, $fields) {
        $user = Auth::user();
        
        foreach($fields as $key=>$value) {
            if($value['type']  == 'checkbox' || $value['type'] == 'custom_html' || $value['type'] == 'checklist') {

                $postVal = $request[$key];
                if(count($request[$key]) > 0) {
                    $request[$key] = join(',', $postVal);
                }
                else {
                    $request[$key] = '';
                }

            }
            else if($value['type'] == 'select2_multiple' || $value['type'] == 'select_multiple' || $value['type'] == 'select2_from_ajax') {
                $postVal = $request[$key];
                \Log::info($key);
                \Log::info($postVal);
                if( is_array($request[$key]) && count($request[$key]) > 0) {
                    $request[$key] = join(',', $postVal);
                }
                else {
                    //$request[$key] = '';
                }
            }

            if($value['name'] == 'g_key'){
                $request[$key] = $user->g_key;
            }else if($value['name'] == 'c_key'){
                $request[$key] = $user->c_key;
            }else if($value['name'] == 's_key'){
                $request[$key] = $user->s_key;
            }else if($value['name'] == 'd_key'){
                $request[$key] = $user->d_key;
            }else if($value['name'] == 'created_by' && $request['created_by'] == null){
                $request[$key] = $user->email;
            }else if($value['name'] == 'updated_by'){
                $request[$key] = $user->email;
            }else if($value['name'] == 'created_at' && $request['created_at'] == null){
                $request[$key] = \Carbon::now();
            }else if($value['name'] == 'updated_at'){
                $request[$key] = \Carbon::now();
            }
        }

        return $request;
    }

    public function insertZip() {
        $str = file_get_contents('zip.json');
        $obj = json_decode($str);
        $insertArray = array();
        foreach($obj as $key=>$row) {
            

            $city = mb_substr($row->行政區名,0,3,'utf8');
            $area = mb_substr($row->行政區名,3,6,'utf8');
            $zip = $row->_x0033_碼郵遞區號;
            $a = array(
                "cntry_cd"   => "TW",
                "cntry_nm"   => "台灣",
                "city_nm"    => $city,
                "dist_nm"    => $area,
                "zip_f"      => $zip,
                "zip_e"      => $zip,
                "g_key"      => 'STD',
                "c_key"      => 'STD',
                "s_key"      => 'STD',
                "d_key"      => 'STD',
                "created_by" => 'TIM',
                "updated_by" => 'TIM',
                'created_at'          => \Carbon::now(),
                'updated_at'          => \Carbon::now(),
            );
            array_push($insertArray, $a);
        }
        DB::table('sys_area')->insert($insertArray);
        return true;
    }

    
}