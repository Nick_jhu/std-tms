<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use DateTime;
use Illuminate\Http\Request;
use Backpack\LangFileManager\app\Models\Language;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\Crypt;


class BaseModel extends Model
{
    
    public function getSearchData($request,$table_name,$status=true,$columns=null,$filter=null,$input=null)
    {
        $user = Auth::user();
        $table_name = $table_name;
        $this_query = null;
        if(isset($user)) {
            $this_query = DB::table($table_name);
        }
        else{
            $this_query = DB::table($table_name);
        }
        $startnum = $request->pagenum * $request->pagesize;
        if($columns != null){
            $frist_col = "CONCAT(".str_replace("+", ",' : ',", explode(",", $columns)[0]).")";
        }else{
            $frist_col = "";
        }
        if($table_name=="bscode_kind") {
            if(!Auth::user()->hasRole('Admin')){
                $this_query->where("adminonly", "=", "Y");
            }
        }
        if(isset($request->basecon)) {            
            $filters = explode(')*(', $request->basecon);
            if(count($filters) > 0) {
                for($i=0; $i<count($filters); $i++) {
                    $con_array = explode(";", $filters[$i]);
                    $df = $con_array[0];
                    $cf = $con_array[1];
                    $vf = $con_array[2];

                    switch($cf) {
                        case "EQUAL":
                            $cf = "=";
                            break;
                        case "NOT_EQUAL":
                            $cf = "<>";
                            break;
                        default:
                            $cf = "=";
                            break;
                    }
                    $this_query->where($df, $cf, $vf);
                }
            }
            
        }
        if($table_name!="mod_shippen_temp_view"){

        
        if($table_name == 'mod_order as confirmorder') {
            $today       = new \DateTime();
            $str_date    = $today->format('Ymd');
            $this_query->where("confirm_dt",">=",$str_date);
        }
        if(
            $table_name == 'mod_order_total_view' ||
            $table_name == 'mod_order_total_view as mod_order_total_view' ||
            $table_name=="mod_order_detail_close_view" ||$table_name == 'sys_customers' || 
            $table_name == 'sys_customers_item' || $table_name == 'mod_car' || $table_name == 'bscode' || 
            $table_name == 'bscode_kind' || $table_name == 'mod_goods' ||
            $table_name == 'cust_fee_view' || $table_name == 'cust_fee_view2') {
            $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."') or g_key='*')");
        }
        else {
            if($user->identity == 'G') {
                $this_query->whereRaw("(g_key='".$user->g_key."')");
            }
            else if($user->identity == 'C') {
                $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."') or g_key='*')");
            }
            else if($user->identity == 'S') {
                $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and s_key='".$user->s_key."') or g_key='*')");
            }
            else {
                $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and s_key='".$user->s_key."' and d_key='".$user->d_key."') or g_key='*')");
            }

        }}
        if($table_name == 'mod_order_total_view as mod_order_total_view' ||$table_name=="mod_order_detail_close_view"){
            $ownerdata = str_replace(",","','",$user->owner_cd);
            \Log::info("nicktest");
            \Log::info($ownerdata);
            if($ownerdata !=null && $ownerdata !=""){
                $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and owner_cd in('".$ownerdata."')) or g_key='*')");
            }
        }
        if($table_name == 'mod_order' || $table_name == 'mod_order_view' ||$table_name=="mod_order_detail_close_view" || $table_name == 'mod_order as mod_order_total_view'|| $table_name == 'mod_order_detail_view') {
            $ownerdata = str_replace(",","','",$user->owner_cd);
            if($ownerdata !=null && $ownerdata !=""){
                $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and owner_cd in('".$ownerdata."')) or g_key='*')");
            }
        }

        if(isset($request->dbRaw)) {
            $this_query->whereRaw(Crypt::decrypt($request->dbRaw));
        }

        if(isset($request->defOrder)) {
            $order_array = explode(";", $request->defOrder);
            $df = $order_array[0];
            $cf = $order_array[1];

            $this_query->orderBy($df, $cf);
        }

        //$this_query->orWhere('g_key', '*');
         // filter data.
         if (isset($request->filterscount))
         {
             $filterscount = $request->filterscount;
            //  dd(isset($request->basecon));
            if(($table_name=="mod_dlv_plan_view"
                ||$table_name=="mod_pchme_send_late_m" ||$table_name=="mod_pchme_send_late_d" ||$table_name=="mod_pchme_send_late_c"
                ||$table_name=="mod_order"||$table_name=="mod_order_close"
                ||$table_name =="mod_order_total_view as mod_order_total_view"
                ||$table_name=="mod_order_detail_close_view"
                ||$table_name =="mod_shippen_temp_view"
                ||$table_name =="mod_pchome"
                ||$table_name =="mod_mi_view"
                ||$table_name =="mod_order_detail_view") && !isset($request->dbRaw)){
                if ($filterscount > 0) {
                    $where_type = true; // true means AND, false means OR
                    $tmpdatafield = "";
                    $tmpfilteroperator = "";
                    $valuesPrep = "";
                    $values = [];
                    $date_format = "";
                for ($i = 0; $i < $filterscount; $i++) {
                   // get the filter's value.
                   $filtervalue = $request["filtervalue" . $i];
                   // get the filter's condition.
                   $filtercondition = $request["filtercondition" . $i];
                   // get the filter's column.
                   $filterdatafield = $request["filterdatafield" . $i];
                   // get the filter's operator.
                   $filteroperator = $request["filteroperator" . $i];
                   if ($tmpdatafield == "")
                   {
                   $tmpdatafield = $filterdatafield;
                   }
                   else if ($tmpdatafield <> $filterdatafield)
                   {
                       $where_type = true;
                   }
                   else if ($tmpdatafield == $filterdatafield)
                   {
                   if ($tmpfilteroperator == 0)
                   {
                       $where_type = true;
                   }
                   else $where_type = false;
                   }
                    // build the "WHERE" clause depending on the filter's condition, value and datafield.
                    $multiValue = explode(';', $filtervalue);
                    if(!is_array($multiValue)) {
                        $multiValue = array();
                    }
                   switch ($filtercondition)
                   {
                   case "CONTAINS":
                       $condition = " LIKE ";
                       if(is_array($multiValue) && count($multiValue) > 1) {
                           for($k=0;$k<count($multiValue);$k++) {
                               if($k == 0) {
                                   $value = "%{$multiValue[$k]}%;";
                               }
                               else {
                                   $value .= "%{$multiValue[$k]}%;";
                               }
                               
                           }
                           $value = rtrim($value,";");
                       }
                       else {
                           $value = "%{$multiValue[0]}%";
                       }
                       
                       break;
                   case "DOES_NOT_CONTAIN":
                       $condition = " NOT LIKE ";
                       $value = "%{$filtervalue}%";
                       break;
                   case "EQUAL":
                       $condition = " = ";
                       $value = $filtervalue;
                       break;
                   case "NOT_EQUAL":
                       $condition = "not_in";
                       $value = $filtervalue;
                       break;
                   case "GREATER_THAN":
                       $condition = " > ";
                       $value = $filtervalue;
                       break;
                   case "LESS_THAN":
                       $condition = " < ";
                       $value = $filtervalue;
                       break;
                   case "GREATER_THAN_OR_EQUAL":
                       $condition = " >= ";
                       $value = $filtervalue;
                       break;
                   case "LESS_THAN_OR_EQUAL":
                       $condition = " <= ";
                       $value = $filtervalue;
                       break;
                   case "STARTS_WITH":
                       $condition = " LIKE ";
                       $value = "{$filtervalue}%";
                       break;
                   case "ENDS_WITH":
                       $condition = " LIKE ";
                       $value = "%{$filtervalue}";
                       break;
                   case "NULL":
                       $condition = " IS NULL ";
                       $value = "%{$filtervalue}%";
                       break;
                   case "NOT_NULL":
                       $condition = " IS NOT NULL ";
                       $value = "%{$filtervalue}%";
                       break;
                   }

                   if($this->validateDateTime($value)){
                       switch($filterdatafield){
                           case "created_at":
                           case "updated_at":
                               //$value =  strtotime($value);
                               $date_format = "timestamp";
                           default:
                               $date_format = "datetime";
                           break;
                       }
                   }elseif($this->validateDateTime($value)){
                       $date_format = "date";
                   }
      
                   try{
                    //    dd($filterscount."/".($filterdatafield=="status").$condition."/"."/".$value);
                       if($filterscount==1 &&$filterdatafield=="status" &&$value=="%FINISHED%"){
                        $this_query->where("id","=", "null");
                       }
                       if($where_type){
                           if($date_format == "timestamp" || $date_format == "datetime" || $date_format == "date"){
                               // $d = Carbon::parse($value)->format('Y-m-d');
                               if(preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $value)){
                                   $this_query->whereDate($filterdatafield,trim($condition), $value);
                               }else{
                                   $this_query->where($filterdatafield,trim($condition), $value);
                                   // dd($filterdatafield."/".trim($condition)."/".$value);
                               }
                           }else{
                               $multiValue = explode(';', $value);

                               if(count($multiValue) > 1) {
                                   $this_query->where(function ($query) use ($multiValue, $filterdatafield, $condition) {
                                       for($k=0;$k<count($multiValue); $k++) {
                                           $query->orWhere($filterdatafield,  trim($condition), $multiValue[$k]);
                                       }
                                   });
                               }
                               else {
                                   //$this_query->where($filterdatafield,  trim($condition), $value);
                                    if( trim($condition) == "not_in") {
                                        $this_query->whereNotIn($filterdatafield, explode(',', $multiValue[0]));
                                    } else {
                                        $this_query->where($filterdatafield,  trim($condition), $multiValue[0]);
                                    }
                               }
                               
                           }
                           
                       }else{
                           $this_query->orWhere($filterdatafield, trim($condition), $value);
                       }
                           $tmpfilteroperator = $filteroperator;
                           $tmpdatafield = $filterdatafield;
                       
                   }catch(\Exception $ex){
                       return $ex;
                   }
               }
           }else{
                $data[] = array(
                    'TotalRows' => 0,
                    'Rows' => array(),
                    'StatusCount' => 0
                );
                return $data;
           }
            }else{

             if ($filterscount > 0)
                 {
                 $where_type = true; // true means AND, false means OR
                 $tmpdatafield = "";
                 $tmpfilteroperator = "";
                 $valuesPrep = "";
                 $values = [];
                 $date_format = "";
                 for ($i = 0; $i < $filterscount; $i++)
                {
                    // get the filter's value.
                    $filtervalue = $request["filtervalue" . $i];
                    // get the filter's condition.
                    $filtercondition = $request["filtercondition" . $i];
                    // get the filter's column.
                    $filterdatafield = $request["filterdatafield" . $i];
                    // get the filter's operator.
                    $filteroperator = $request["filteroperator" . $i];
                    if ($tmpdatafield == "")
                    {
                    $tmpdatafield = $filterdatafield;
                    }
                    else if ($tmpdatafield <> $filterdatafield)
                    {
                        $where_type = true;
                    }
                    else if ($tmpdatafield == $filterdatafield)
                    {
                    if ($tmpfilteroperator == 0)
                    {
                        $where_type = true;
                    }
                    else $where_type = false;
                    }
                     // build the "WHERE" clause depending on the filter's condition, value and datafield.
                    $multiValue = explode(';', $filtervalue);
                    switch ($filtercondition)
                    {
                    case "CONTAINS":
                        $condition = " LIKE ";
                        if( is_array($multiValue) && count($multiValue ) > 1) {
                            for($k=0;$k<count($multiValue);$k++) {
                                if($k == 0) {
                                    $value = "%{$multiValue[$k]}%;";
                                }
                                else {
                                    $value .= "%{$multiValue[$k]}%;";
                                }
                                
                            }
                            $value = rtrim($value,";");
                        }
                        else {
                            $value = "%{$multiValue[0]}%";
                        }
                        
                        break;
                    case "DOES_NOT_CONTAIN":
                        $condition = " NOT LIKE ";
                        $value = "%{$filtervalue}%";
                        break;
                    case "EQUAL":
                        $condition = " = ";
                        $value = $filtervalue;
                        break;
                    case "NOT_EQUAL":
                        $condition = "not_in";
                        $value = $filtervalue;
                        break;
                    case "GREATER_THAN":
                        $condition = " > ";
                        $value = $filtervalue;
                        break;
                    case "LESS_THAN":
                        $condition = " < ";
                        $value = $filtervalue;
                        break;
                    case "GREATER_THAN_OR_EQUAL":
                        $condition = " >= ";
                        $value = $filtervalue;
                        break;
                    case "LESS_THAN_OR_EQUAL":
                        $condition = " <= ";
                        $value = $filtervalue;
                        break;
                    case "STARTS_WITH":
                        $condition = " LIKE ";
                        $value = "{$filtervalue}%";
                        break;
                    case "ENDS_WITH":
                        $condition = " LIKE ";
                        $value = "%{$filtervalue}";
                        break;
                    case "NULL":
                        $condition = " IS NULL ";
                        $value = "%{$filtervalue}%";
                        break;
                    case "NOT_NULL":
                        $condition = " IS NOT NULL ";
                        $value = "%{$filtervalue}%";
                        break;
                    }

                    if($this->validateDateTime($value)){
                        switch($filterdatafield){
                            case "created_at":
                            case "updated_at":
                                //$value =  strtotime($value);
                                $date_format = "timestamp";
                            default:
                                $date_format = "datetime";
                            break;
                        }
                    }elseif($this->validateDateTime($value)){
                        $date_format = "date";
                    }
       
                    try{
                        if($where_type){
                            if($date_format == "timestamp" || $date_format == "datetime" || $date_format == "date"){
                                // $d = Carbon::parse($value)->format('Y-m-d');
                                if(preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $value)){
                                    $this_query->whereDate($filterdatafield,trim($condition), $value);
                                }else{
                                    $this_query->where($filterdatafield,trim($condition), $value);
                                    // dd($filterdatafield."/".trim($condition)."/".$value);
                                }
                            }else{
                                $multiValue = explode(';', $value);

                                if(count($multiValue) > 1) {
                                    $this_query->where(function ($query) use ($multiValue, $filterdatafield, $condition) {
                                        for($k=0;$k<count($multiValue); $k++) {
                                            $query->orWhere($filterdatafield,  trim($condition), $multiValue[$k]);
                                        }
                                    });
                                }
                                else {
                                    //$this_query->where($filterdatafield,  trim($condition), $value);
                                    if( trim($condition) == "not_in") {
                                        $this_query->whereNotIn($filterdatafield, explode(',', $multiValue[0]));
                                    } else {
                                        $this_query->where($filterdatafield,  trim($condition), $multiValue[0]);
                                    }
                                }
                                
                            }
                            
                        }else{
                            $this_query->orWhere($filterdatafield, trim($condition), $value);
                        }
                            $tmpfilteroperator = $filteroperator;
                            $tmpdatafield = $filterdatafield;
                        
                    }catch(\Exception $ex){
                        return $ex;
                    }
                }
            }
            }
        }
        if($filter!=null){
            $this_query->whereRaw($filter);
        }

        if($input!=null && $columns!=null){
            $this_query->whereRaw($frist_col." LIKE "."'%$input%'");
        }
        $count_query = clone $this_query;

        if($status){
            $status_count = $count_query->select(DB::raw("count(*) as count, status, '' AS statustext"))
            ->groupBy('status')
            ->orderBy('status', 'ASC' )
            ->get();
            foreach ($status_count as $sc){
                $sc->statustext = trans($this->dashesToCamelCase($table_name).'.STATUS_'.$sc->status);
            }
        }else{
            $status_count = 0;
        }

        if(isset($request->sortdatafield) && isset($request->sortorder)){
            $sortfield = $request->sortdatafield;
            $sortorder = $request->sortorder;
            $this_query->orderBy($sortfield,$sortorder);
        }
       
        if($input==null){
            $total_rows = $this_query->count();
        }
     
        if($columns==null){
            $rows = $this_query->take($request->pagesize)->skip($startnum)->get();
        }else{
            if($input!=null){
                $rows = $this_query->select(DB::raw($frist_col." AS label,".$frist_col." AS value".",".$columns))->take(10)->get();
                $data = $rows;
                return $data;
                
            }else{
                $rows = $this_query->select(DB::raw($columns))->take($request->pagesize)->skip($startnum)->get();//
            }
           
        }
        

       
        $data[] = array(
            'TotalRows' => $total_rows,
            'Rows' => $rows,
            'StatusCount' => $status_count
        );
        return $data;
    }

    public function getSearchDataForMongo($request,$table_name,$status=true,$columns=null,$filter=null,$input=null)
    {
        $user = Auth::user();
        $table_name = $table_name;
        $this_query = null;
        if(isset($user)) {
            $this_query = DB::connection('mongodb')->collection($table_name);
        }
        else{
            $this_query = DB::connection('mongodb')->collection($table_name);
        }
        $startnum = $request->pagenum * $request->pagesize;
        if($columns != null){
            $frist_col = "CONCAT(".str_replace("+", ",' : ',", explode(",", $columns)[0]).")";
        }else{
            $frist_col = "";
        }
        if(isset($request->basecon)) {            
            $filters = explode(')*(', $request->basecon);
            if(count($filters) > 0) {
                for($i=0; $i<count($filters); $i++) {
                    $con_array = explode(";", $filters[$i]);
                    $df = $con_array[0];
                    $cf = $con_array[1];
                    $vf = $con_array[2];

                    switch($cf) {
                        case "EQUAL":
                            $cf = "=";
                            break;
                        case "NOT_EQUAL":
                            $cf = "<>";
                            break;
                        default:
                            $cf = "=";
                            break;
                    }
                    $this_query->where($df, $cf, $vf);
                }
            }
            
        }

        if($table_name == 'sys_customers' || $table_name == 'sys_customers_item' || $table_name == 'mod_car' || $table_name == 'bscode' || $table_name == 'bscode_kind' || $table_name == 'mod_goods') {
            $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."') or g_key='*')");
        }
        else {
            if($user->identity == 'G') {
                $this_query->whereRaw(['g_key' => $user->g_key]);
            }
            else if($user->identity == 'C') {
                $this_query->whereRaw(['g_key' => $user->g_key,'c_key'=>$user->c_key]);
            }
            else if($user->identity == 'S') {
                // $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and s_key='".$user->s_key."') or g_key='*')");
                $this_query->whereRaw(['g_key' => $user->g_key,'c_key'=>$user->c_key,'s_key'=>$user->s_key]);

            }
            else {
                // $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and s_key='".$user->s_key."' and d_key='".$user->d_key."') or g_key='*')");
                $this_query->whereRaw(['g_key' => $user->g_key,'c_key'=>$user->c_key,'s_key'=>$user->s_key,'d_key'=>$user->d_key]);
            }

        }

        if(isset($request->dbRaw)) {
            $this_query->whereRaw(Crypt::decrypt($request->dbRaw));
        }

        if(isset($request->defOrder)) {
            $order_array = explode(";", $request->defOrder);
            $df = $order_array[0];
            $cf = $order_array[1];

            $this_query->orderBy($df, $cf);
        }

        //$this_query->orWhere('g_key', '*');
         // filter data.
         if (isset($request->filterscount))
         {
             $filterscount = $request->filterscount;
             if ($filterscount > 0)
                 {
                 $where_type = true; // true means AND, false means OR
                 $tmpdatafield = "";
                 $tmpfilteroperator = "";
                 $valuesPrep = "";
                 $values = [];
                 $date_format = "";
                 for ($i = 0; $i < $filterscount; $i++)
                {
                    // get the filter's value.
                    $filtervalue = $request["filtervalue" . $i];
                    // get the filter's condition.
                    $filtercondition = $request["filtercondition" . $i];
                    // get the filter's column.
                    $filterdatafield = $request["filterdatafield" . $i];
                    // get the filter's operator.
                    $filteroperator = $request["filteroperator" . $i];
                    if ($tmpdatafield == "")
                    {
                    $tmpdatafield = $filterdatafield;
                    }
                    else if ($tmpdatafield <> $filterdatafield)
                    {
                        $where_type = true;
                    }
                    else if ($tmpdatafield == $filterdatafield)
                    {
                    if ($tmpfilteroperator == 0)
                    {
                        $where_type = true;
                    }
                    else $where_type = false;
                    }
                     // build the "WHERE" clause depending on the filter's condition, value and datafield.
                    $multiValue = explode(';', $filtervalue);
                    switch ($filtercondition)
                    {
                    case "CONTAINS":
                        $condition = " LIKE ";
                        if(count($multiValue > 1)) {
                            for($k=0;$k<count($multiValue);$k++) {
                                if($k == 0) {
                                    $value = "%{$multiValue[$k]}%;";
                                }
                                else {
                                    $value .= "%{$multiValue[$k]}%;";
                                }
                                
                            }
                            $value = rtrim($value,";");
                        }
                        else {
                            $value = "%{$multiValue[0]}%";
                        }
                        
                        break;
                    case "DOES_NOT_CONTAIN":
                        $condition = " NOT LIKE ";
                        $value = "%{$filtervalue}%";
                        break;
                    case "EQUAL":
                        $condition = " = ";
                        $value = $filtervalue;
                        break;
                    case "NOT_EQUAL":
                        $condition = "not_in";
                        $value = $filtervalue;
                        break;
                    case "GREATER_THAN":
                        $condition = " > ";
                        $value = $filtervalue;
                        break;
                    case "LESS_THAN":
                        $condition = " < ";
                        $value = $filtervalue;
                        break;
                    case "GREATER_THAN_OR_EQUAL":
                        $condition = " >= ";
                        $value = $filtervalue;
                        break;
                    case "LESS_THAN_OR_EQUAL":
                        $condition = " <= ";
                        $value = $filtervalue;
                        break;
                    case "STARTS_WITH":
                        $condition = " LIKE ";
                        $value = "{$filtervalue}%";
                        break;
                    case "ENDS_WITH":
                        $condition = " LIKE ";
                        $value = "%{$filtervalue}";
                        break;
                    case "NULL":
                        $condition = " IS NULL ";
                        $value = "%{$filtervalue}%";
                        break;
                    case "NOT_NULL":
                        $condition = " IS NOT NULL ";
                        $value = "%{$filtervalue}%";
                        break;
                    }

                    if($this->validateDateTime($value)){
                        switch($filterdatafield){
                            case "created_at":
                            case "updated_at":
                                //$value =  strtotime($value);
                                $date_format = "timestamp";
                            default:
                                $date_format = "datetime";
                            break;
                        }
                    }elseif($this->validateDateTime($value)){
                        $date_format = "date";
                    }
       
                    try{
                        if($where_type){
                            if($date_format == "timestamp" || $date_format == "datetime" || $date_format == "date"){
                                $d = Carbon::parse($value)->format('Y-m-d');
                                //$this_query->whereBetween($filterdatafield, [strtotime($d+" 00:00:00"),strtotime($d+" 23:59:59")]);
                                $this_query->whereDate($filterdatafield,  trim($condition), $d);
                            }else{
                                $multiValue = explode(';', $value);

                                if(count($multiValue) > 1) {
                                    $this_query->where(function ($query) use ($multiValue, $filterdatafield, $condition) {
                                        for($k=0;$k<count($multiValue); $k++) {
                                            $query->orWhere($filterdatafield,  trim($condition), $multiValue[$k]);
                                        }
                                    });
                                }
                                else {
                                    //$this_query->where($filterdatafield,  trim($condition), $value);
                                    if( trim($condition) == "not_in") {
                                        $this_query->whereNotIn($filterdatafield, explode(',', $multiValue[0]));
                                    } else {
                                        $this_query->where($filterdatafield,  trim($condition), $multiValue[0]);
                                    }
                                }
                                
                            }
                            
                        }else{
                            $this_query->orWhere($filterdatafield, trim($condition), $value);
                        }
                            $tmpfilteroperator = $filteroperator;
                            $tmpdatafield = $filterdatafield;
                        
                    }catch(\Exception $ex){
                        return $ex;
                    }
                }
            }
        }

        if($filter!=null){
            $this_query->whereRaw($filter);
        }

        if($input!=null && $columns!=null){
            $this_query->whereRaw($frist_col." LIKE "."'%$input%'");
        }
        $count_query = clone $this_query;

        if(isset($request->sortdatafield) && isset($request->sortorder)){
            $sortfield = $request->sortdatafield;
            $sortorder = $request->sortorder;
            $this_query->orderBy($sortfield,$sortorder);
        }
       
        if($input==null){
            // dd($this_query);
            $total_rows = $this_query->count();
        }
     
        if($columns==null){
            $pagesize =intval($request->pagesize);
            $rows = $this_query->take($pagesize)->skip($startnum)->get();
        }else{
            if($input!=null){
                $rows = $this_query->select(DB::raw($frist_col." AS label,".$frist_col." AS value".",".$columns))->take(10)->get();
                $data = $rows;
                return $data;
                
            }else{
                $rows = $this_query->select(DB::raw($columns))->take($pagesize)->skip($startnum)->get();//
            }
           
        }
        $data[] = array(
            'TotalRows' => $total_rows,
            'Rows' => $rows,
            'StatusCount' => array()
        );
        return $data;
    }


    public function validateDate($date, $format = 'Y-m-d')
    {
        try{
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }catch(\Exception $ex){
            return ex;
        }
       
    }

    public function validateDateTime($date, $format = 'Y-m-d H:i:s')
    {
        try{
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }catch(\Exception $ex){
            return ex;
        }
       
    }

    public function getColumnInfo($table_name,$columns=null){
        $user = Auth::user();
        $cols_sql = "";
        if($columns!=null){
            $cols_sql = " WHERE Field IN  ('".str_replace(",", "','", $columns)."')";
        }
        $values_source = "";
        $columns = DB::select('show columns from ' . $table_name . $cols_sql);
        foreach ($columns as $value) {
            $this_type = explode("(", $value->Type)[0];
            $filtertype = "input";
            $cellsformat = "";
            $width = filter_var($value->Type, FILTER_SANITIZE_NUMBER_INT);
            $width = $width==""?100:$width;
            $db_width = $width;
            $cellsalign = "left";
            $statusCode = array();
            if($value->Field == 'status' &&$table_name!='mod_order_total_view' &&$table_name!='mod_order_detail_close_view') {
                $status = $this->get_enum_values($value->Type);
                for($i=0; $i<count($status); $i++) {
                    $statusCode[$i] = array (
                        'label' => trans($this->dashesToCamelCase($table_name).'.STATUS_'.$status[$i]),
                        'value' => $status[$i]
                    );
                }
            }

            $trsMode = array();
            if($value->Field == 'trs_mode') {
                $trsData = DB::table('bscode')
                    ->select('cd', 'cd_descp')
                    ->where('cd_type', 'TM')
                    ->where('g_key', $user->g_key)
                    ->where('c_key', $user->c_key)                    
                    ->get();
                
                if(count($trsData) > 0) {
                    foreach($trsData as $key=>$row) {
                        $trsMode[$key] = array (
                            'label' => $row->cd_descp,
                            'value' => $row->cd
                        );
                    }
                    
                }
            }

            $carType = array();
            if($value->Field == 'car_type') {
                $baseData = DB::table('bscode')
                    ->select('cd', 'cd_descp')
                    ->where('cd_type', 'CARTYPE')
                    ->where('g_key', $user->g_key)
                    ->where('c_key', $user->c_key)                    
                    ->get();
                if(count($baseData) > 0) {
                    foreach($baseData as $key=>$row) {
                        $carType[$key] = array (
                            'label' => $row->cd_descp,
                            'value' => $row->cd
                        );
                    }
                    
                }
            }

            $dlvType = array();
            if($value->Field == 'dlv_type') {
                $dlvType[0] = array (
                    'label' => '提貨(P)',
                    'value' => 'P'
                );

                $dlvType[1] = array (
                    'label' => '配送(D)',
                    'value' => 'D'
                );
            }

            switch($this_type){
                case "timestamp":
                    $this_type = "range";
                    $filtertype = "range";
                    $cellsformat = 'yyyy-MM-dd HH:mm:ss';
                    $cellsalign = "left";
                    $width=100;
                    break;
                case "date":
                    $this_type = "range";
                    $filtertype = "range";
                    $cellsformat = 'yyyy-MM-dd';
                    $cellsalign = "left";
                    $width=100;
                    break;
                case "datetime":
                    $this_type = "range";
                    $filtertype = "range";
                    $cellsformat = 'yyyy-MM-dd HH:mm:ss';
                    $cellsalign = "left";
                    $width=100;
                    break;
                case "decimal":
                case "int":
                case "float":
                    $this_type = "number";
                    $filtertype = "number";
                    $cellsalign = "right";
                    $width*=3;
                    break;
                case "text":
                case "varchar":
                    $this_type = "string";
                    $filtertype = "textbox";
                    $cellsalign = "left";
                    $width*=3;
                break;
                case "enum":
                    $this_type = "string";
                    $filtertype = "textbox";
                    $cellsalign = "left";
                    $width*=3;
                    $values_source = array('source' =>  $value->Field."GridObj",
                                        'value' => "val", 
                                        'name'  => "key");
                break;
                default:
                    $width*=3;
                break;
            }

            if($width < 100){
                $width = 100;
            }

           
            $fields_info[] = array(
                'name' => $value->Field,
                'type' => $this_type
            );
         
            // $columns_model[] = array(
            //     'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
            //     'datafield'   => $value->Field,
            //     'width'       => $width,
            //     'dbwidth'     => $db_width,
            //     'nullable'    => $value->Null == "YES"?true: false,
            //     'cellsformat' => $cellsformat,
            //     'filtertype'  => $filtertype,
            //     'cellsalign'  => $cellsalign,
            //     'values'      => $values_source
            // );
            if($value->Field == "status") {
                $columns_model[] = array(
                    'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'statusCode'  => $statusCode,
                    'filterdelay' => 99999999
                );
            }
            else if($value->Field == "trs_mode") {
                $columns_model[] = array(
                    'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'trsMode'     => $trsMode,
                    'filterdelay' => 99999999
                );
            }
            else if($value->Field == "car_type") {
                $columns_model[] = array(
                    'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'carType'     => $carType,
                    'filterdelay' => 99999999
                );
            }
            else if($value->Field == "dlv_type") {
                $columns_model[] = array(
                    'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'dlvType'     => $dlvType,
                    'filterdelay' => 99999999
                );
            }
            else {
                $columns_model[] = array(
                    'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'filterdelay' => 99999999
                );
            }

            $values_source = "";
           // dd($value->Type);
           //echo "'" . $value->Field . "' => '" . $value->Type . "|" . ( $value->Null == "NO" ? 'required' : '' ) ."', <br/>" ;
        }

        /*
        
            { text: 'id', datafield: 'id' , width: 250 },
                { text: 'name', datafield: 'name' , width: 250 },
                { text: 'email', datafield: 'email' , width: 250 },
                { text: 'password', datafield: 'password' , width: 250 },
                { text: 'Group', datafield: 'g_key' , width: 250 },
                { text: 'Company', datafield: 'c_key' , width: 250 },
                { text: 'Station', datafield: 's_key' , width: 250 },
                { text: 'Derpartment', datafield: 'd_key' , width: 250 },
                { text: 'remember_token', datafield: 'remember_token' , width: 250 },
                { text: 'created_at',filtertype: 'date', datafield: 'created_at' , width: 250, cellsformat: 'yyyy-MM-dd' },
                { text: 'updated_at',filtertype: 'date', datafield: 'updated_at' , width: 250 },
        
        */
        $data[0] = $fields_info;
        $data[1] = $columns_model;

        return $data;
    }

    public function getColumnSpec($table_name,$columns=null){
        $cols_sql = "";
        $spec_text = "return [<br />";
        if($columns!=null){
            $cols_sql = " WHERE Field IN  ('".str_replace(",", "','", $columns)."')";
        }

        $columns = DB::select('show columns from ' . $table_name . $cols_sql);
        foreach ($columns as $value) {
            $spec_text.= '"'.$this->dashesToCamelCase($value->Field).'"        =>      "'.trans(''.$this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)).'",<br />';
        }
        $spec_text.="];";

        return $spec_text;
    }

    public function saveLayout($key,$data,$user){

        try{
            $now = date("Y-m-d H:i:s");
            DB::table('sys_layout')
                ->where('key', '=', $key)
                ->where('created_by', $user->email)
                ->where('lang', '=', App::getLocale())->delete();

            $id = DB::table('sys_layout')->insertGetId([
                'key'        => $key,
                'data'       => $data,
                'lang'       => App::getLocale(),
                'g_key'      => $user->g_key,
                'c_key'      => $user->c_key,
                'd_key'      => $user->d_key,
                's_key'      => $user->s_key,
                'created_at' => $now,
                'updated_at' => $now,
                'created_by' => $user->email,
                'updated_by' => $user->email
            ]);
            return "true";
        }catch(\Exception $ex){
            return "false";
        }
    }

    public function getLayout($key){
        $user = Auth::user();
        try{
            $layout = array();
            $checklayout = DB::table('sys_layout')
            ->where('key', '=', $key)
            ->where('created_by', $user->email)
            ->where('lang', '=', App::getLocale())
            ->first();
            if(isset($checklayout)) {
                $layout = DB::table('sys_layout')
                ->where('key', '=', $key)
                ->where('created_by', $user->email)
                ->where('lang', '=', App::getLocale())
                ->pluck('data');
            } else {
                $layout = DB::table('sys_layout')
                ->where('key', '=', $key)
                ->where('created_by', 'standardlayout')
                ->where('lang', '=', App::getLocale())
                ->pluck('data');
            }

            return $layout[0];
        }catch(\Exception $ex){
            return "false";
        }
        
    }

    function dashesToCamelCase($string, $capitalizeFirstCharacter = false) 
    {
    
        $str = str_replace('_', '', ucwords($string, '_'));
    
        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }
    
        return $str;
    }


    public static function getPossbileStatuses($tableName, $colName){
        $type = DB::select(DB::raw('SHOW COLUMNS FROM '.$tableName.' WHERE Field = "'.$colName.'"'))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $values = array();
        foreach(explode(',', $matches[1]) as $value){
            $values[] = trim($value, "'");
        }
        return $values;
    }

    public function getAutoNumber($model,$params){
        $user = Auth::user();
        $format = DB::select(DB::raw('SELECT value1 FROM bscode WHERE cd_type = "autono" and  cd = "'.$model.'" and c_key="'.$user->c_key.'"'))[0]->value1;
        foreach ($params as $key => $value) {
            $format = str_replace('{'.$key.'}',$value,$format);
        }
        $model = new AutoNoModel();
      
        $numObj = $model->create([
            'num_format' => $format
        ]);
        return $numObj->auto_no;
    }

    public function getAutoNumberApi($model,$params,$ckey){
        $format = DB::select(DB::raw('SELECT value1 FROM bscode WHERE cd_type = "autono" and  cd = "'.$model.'" and c_key="'.$ckey.'"'))[0]->value1;
        foreach ($params as $key => $value) {
            $format = str_replace('{'.$key.'}',$value,$format);
        }
        $model = new AutoNoModel();
      
        $numObj = $model->create([
            'num_format' => $format
        ]);
        return $numObj->auto_no;
    }

    public function getAutoNumbersyl($model,$params){
        $format = DB::select(DB::raw('SELECT value1 FROM bscode WHERE cd_type = "autono" and  cd = "'.$model.'" and c_key="'."SYL".'"'))[0]->value1;
        foreach ($params as $key => $value) {
            $format = str_replace('{'.$key.'}',$value,$format);
        }
        $model = new AutoNoModel();
      
        $numObj = $model->create([
            'num_format' => $format
        ]);
        return $numObj->auto_no;
    }
    public function getAutoNumberpchome($model,$params){
        $format = DB::select(DB::raw('SELECT value1 FROM bscode WHERE cd_type = "autono" and  cd = "'.$model.'"'))[0]->value1;
        foreach ($params as $key => $value) {
            $format = str_replace('{'.$key.'}',$value,$format);
        }
        $model = new AutoNoModel();
      
        $numObj = $model->create([
            'num_format' => $format
        ]);
        return $numObj->auto_no;
    }

    function get_enum_values( $type ) {
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    function getStatusCount($tableName) {
        $user = Auth::user();
        $ownerdata = str_replace(",","','",$user->owner_cd);
        $total_rows = 0;
        if($tableName == 'mod_order' || $tableName == 'mod_order_view' || $tableName == 'mod_order_close'|| $tableName == 'mod_order as mod_order_total_view'|| $tableName == 'mod_order_detail_close_view' || $tableName == 'mod_order_detail_view'|| $tableName == 'mod_order_total_view as mod_order_total_view') {
            $this_query = DB::table($tableName);
            $this_query->select(DB::raw("count(*) as count, status, '' AS statustext"));
            if($user->identity == 'G') {
                $this_query->whereRaw("(g_key='".$user->g_key."')");
            }
            else if($user->identity == 'C') {
                $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."') or g_key='*')");
            }
            else if($user->identity == 'S') {
                $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and s_key='".$user->s_key."') or g_key='*')");
            }
            else {
                $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and s_key='".$user->s_key."' and d_key='".$user->d_key."') or g_key='*')");
            }
            if($ownerdata!="" && $ownerdata!=null){
                $owner_cd =explode(',', $user->owner_cd);
                $this_query->whereIn('owner_cd',  $owner_cd);
            }
            if($tableName == 'mod_order') {
                // FIELD( Priority, 'High', 'Normal', 'Low')
                $this_query->groupBy('status');
                $this_query->orderBy(DB::raw("FIELD( status,'UNTREATED', 'READY', 'READY_PICK', 'LOADING', 'SEND', 'SETOFF', 'PICKED', 'ERROR', 'REJECT','FINISHED', 'FAIL' )"));
            } else {
                $this_query->groupBy('status');
                $this_query->orderBy('status','ASC');
            }
            $status_count =$this_query->get();
            foreach ($status_count as $sc){
                $sc->statustext = trans($this->dashesToCamelCase($tableName).'.STATUS_'.$sc->status);
                $total_rows = $total_rows + $sc->count;
            }
        }else{
            $status_count = DB::table($tableName)->select(DB::raw("count(*) as count, status, '' AS statustext"))
            ->where('g_key', $user->g_key)
            ->where('c_key', $user->c_key)
            ->groupBy('status')
            ->orderBy('status', 'ASC' )
            ->get();
            foreach ($status_count as $sc){
                $sc->statustext = trans($this->dashesToCamelCase($tableName).'.STATUS_'.$sc->status);
                $total_rows = $total_rows + $sc->count;
            }
        }
        // $this_query = DB::table($tableName);
        // if($user->identity == 'G') {
        //     $this_query->whereRaw("(g_key='".$user->g_key."')");
        // }
        // else if($user->identity == 'C') {
        //     $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."') or g_key='*')");
        // }
        // else if($user->identity == 'S') {
        //     $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and s_key='".$user->s_key."') or g_key='*')");
        // }
        // else {
        //     $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and s_key='".$user->s_key."' and d_key='".$user->d_key."') or g_key='*')");
        // }
        // if($ownerdata!="" && $ownerdata!=null){
        //     $owner_cd =explode(',', $user->owner_cd);
        //     $this_query->whereIn('owner_cd',  $owner_cd);
        // }
        $data[] = array(
            'TotalRows'   =>$total_rows,
            'StatusCount' => $status_count
        );

        return $data;
    }
}
