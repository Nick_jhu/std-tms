<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use DB;
use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\TransRecordModel;
use App\Mail\TrackingMail;

class TrackingModel extends Model
{
    public function getTrackingDataByOrder($ord_no) {
        return DB::table('mod_order_total_view')->where('ord_no', $ord_no)->first();
    }

    public function getTrackingDataBySysOrder($sys_ord_no) {
        return DB::table('mod_order_total_view')->where('sys_ord_no', $sys_ord_no)->first();
    }

    public function insertGps($obj) {
		DB::table('mod_car_real_data')->insert($obj);
        
        return true;
	}
	
	public function sendTrackingMail($ord_no, $to, $title) {
        //dd(base_path());
        $mData = DB::table("mail_format")->where("type", "TKM")->first();
        $tsData = DB::table("mod_ts_record")->where("ref_no2", $ord_no)->orderBy('sort', 'desc')->get();
        $trackingData = [];
        if(count($tsData) > 0) {
            foreach($tsData as $key=>$val) {
                if($val->created_at != null) {

                    $detail = [
                        "date"   => $val->created_at,
                        "title"  => $val->ts_name,
                        "descp"  => $val->ts_desc,
                        "status" => $val->ts_type
                    ];
                    
                    array_push($trackingData, $detail);
                }
                
            }
        }

        $html = file_get_contents(base_path().'/resources/views/email/trackingMail.blade.php');
        $doc = new \DOMDocument(); 
        $doc->loadHTML($html);
        $docBody = $doc->getElementById('body');

        $bodyChildren = $docBody->childNodes;

        while ($bodyChildren->length > 0) {
            $docBody->removeChild($bodyChildren->item(0));
        }


        $mDoc = new \DOMDocument(); 
        $mDoc->loadHTML(mb_convert_encoding($mData->content, 'HTML-ENTITIES', 'UTF-8'));
        $mDocTbody = $mDoc->getElementsByTagName('tbody')->item(0);
        
        foreach($trackingData as $val) {
            $f = $mDoc->createDocumentFragment();
            $f->appendXML("<tr style='border: 1px solid #9a9a9a;text-align:center'><td>".$val["title"]."</td><td>".$val["date"]."</td></tr>");
            $mDocTbody->appendChild($f);
        }

        $mDocTable = $mDoc->getElementsByTagName('table')->item(0);
        $f = $doc->createDocumentFragment();
        $f->appendXML($mDoc->saveHTML($mDocTable));
        $docBody->appendChild($f);
        
        file_put_contents(base_path().'/resources/views/email/trackingMail.blade.php', $doc->saveHTML());
        
        
        Mail::send('email.trackingMail',['trackingData'=>$doc->saveHTML()],function($message) use ($to,$title){
            $message->sender('iTracking@standard-info.com')->to($to)->subject($title);
        });
        
    }
    // $o = new App\Models\TrackingModel();
    // $o->sendDlvMail("SYLO200700677","D");
    public function sendDlvMail($sys_ord_no, $dlvType) {
        try{


        $ordData = DB::table('mod_order')->where('sys_ord_no', $sys_ord_no)->first();
        \Log::info("sendDlvMail");
        \Log::info($sys_ord_no);
        if(isset($ordData)) {
            $ownerData = DB::table('sys_customers')
                                ->where('cust_no', $ordData->owner_cd)
                                ->where('g_key', $ordData->g_key)
                                ->where('c_key', $ordData->c_key)
                                ->first();

            $ordDetail = DB::table('mod_order_detail')->where('ord_id', $ordData->id)->get();



            $pickSendData = [
                'custNm' => $ordData->pick_cust_nm,
                'ordNo' => $ordData->ord_no,
                'ordDetail' => $ordDetail,
                'tpl' => $ordData->c_key.'_TKMP'
            ];

            $dlvSendData = [
                'custNm' => $ordData->dlv_cust_nm,
                'ordNo' => $ordData->ord_no,
                'ordDetail' => $ordDetail,
                'tpl' => $ordData->c_key.'_TKMD'
            ];

            if($dlvType == 'P') {
                if(isset($ownerData) && $ordData->owner_send_mail == 'Y' && ($ownerData->email_type == 'B' || $ownerData->email_type == 'P')) {
                    \Log::info("owner send start");
                    if($ownerData->email!=""){
                    $ownerEmail = explode(';', $ownerData->email);
                    Mail::to($ownerEmail)
                    // ->bcc('sales@standard-info.com')
                    ->send(new TrackingMail($pickSendData));
                    }

                    \Log::info("owner send end");
                }

                if($ordData->pick_send_mail == 'Y') {
                    \Log::info("pick send start");
                    if($ordData->pick_email!=""){
                    $pickEmail = explode(';', $ordData->pick_email);
                    Mail::to($pickEmail)
                    // ->bcc('sales@standard-info.com')
                    ->send(new TrackingMail($pickSendData));
                    }
                    \Log::info("pick send end");
                }
            }

            if($dlvType == 'D') {
                if(isset($ownerData) && $ordData->owner_send_mail == 'Y' && ($ownerData->email_type == 'B' || $ownerData->email_type == 'D')) {
                    \Log::info("owner send start");
                    if($ownerData->email!=""){
                    $ownerEmail = explode(';', $ownerData->email);
                    Mail::to($ownerEmail)
                    // ->bcc('sales@standard-info.com')
                    ->send(new TrackingMail($dlvSendData));
                    }
                    \Log::info("owner send end");
                }

                if($ordData->pick_send_mail == 'Y') {
                    \Log::info("dlv send start");
                    if($ordData->dlv_email!=""){
                    $dlvEmail = explode(';', $ordData->dlv_email);
                    Mail::to($dlvEmail)
                    // ->bcc('sales@standard-info.com')
                    ->send(new TrackingMail($dlvSendData));
                    }
                    \Log::info("dlv send end");
                }
            }

            
        }
        }catch(\Exception $e) {
            \Log::info("sendDlvMail error");
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            // \Log::info($request->all());
            // return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }

        return;
    }

    public function insertTracking($ref_no1, $ref_no2, $ref_no3, $ref_no4, $ref_no5, $ts_type, $order, $g_key, $c_key, $s_key, $d_key) {
        try {
            //$modDlv = DB::table('mod_dlv')->select('g_key', 'c_key', 's_key', 'd_key')->where('dlv_no', $ref_no1)->first();

            $ordData = DB::table('mod_order')
                        ->select('ord_no')
                        ->where('sys_ord_no', $ref_no4)
                        ->first();
            
            if(isset($ordData)) {
                $ref_no2 = $ordData->ord_no;
            }
            
            $tsData = DB::table('mod_trans_status')
                        ->select('*')
                        ->where('g_key', $g_key)
                        ->where('c_key', $c_key)
                        ->where('ts_type', $ts_type)
                        ->where('order', $order)
                        ->first();
            
            if(isset($tsData)) {
                $modTsRecord          = new TransRecordModel;
                $modTsRecord->ts_no   = $tsData->id;
                $modTsRecord->ts_type = $tsData->ts_type;
                $modTsRecord->ts_name = $tsData->ts_name;
                $modTsRecord->ts_desc = $tsData->ts_desc;
                $modTsRecord->sort    = $tsData->order;
                $modTsRecord->ref_no1 = $ref_no1;
                $modTsRecord->ref_no2 = $ref_no2;
                $modTsRecord->ref_no3 = $ref_no3;
                $modTsRecord->ref_no4 = $ref_no4;
                $modTsRecord->ref_no5 = $ref_no5;
                $modTsRecord->g_key   = $g_key;
                $modTsRecord->c_key   = $c_key;
                $modTsRecord->s_key   = $s_key;
                $modTsRecord->d_key   = $d_key;
                $modTsRecord->save();

                if(isset($ordData)) {
                    $ordall = DB::table('mod_order')
                    ->where('sys_ord_no', $ref_no4)
                    ->first();
                    $statunm="";
                    if($ordall->status_desc=="尚未安排"){
                        $statunm = "訂單處理中";
                    }
                    if($ordall->status_desc=="已派單未出發"){
                        $statunm = "待出貨";
                    }
                    if($ordall->status_desc=="出發"){
                        $statunm = "運送中";
                    }
                    if($ordall->status_desc=="拒收"){
                        $statunm = "拒收";
                    }
                    if($ordall->status_desc=="配送發生問題"){
                        $statunm = "配送發生問題";
                    }
                    if($ordall->status_desc=="配送完成"){
                        $statunm = "配送完成";
                    }
                    if($tsData->ts_name=="待出貨"){
                        $statunm = "待出貨";
                        $ordall->status ="SEND";
                    }
                    if($tsData->ts_name=="運送中"){
                        $statunm = "運送中";
                        $ordall->status="SETOFF";
                    }
                    $tsrecord = DB::table('mod_ts_order')
                    ->where('sys_ord_no', $ref_no4)
                    ->where('status_desc', $statunm)
                    ->where('owner_cd', $ordall->owner_cd)
                    ->count();
                    $custdata = DB::table('sys_customers')
                    ->where('cust_no', $ordall->owner_cd)
                    ->whereIn('ftptype',["E","EM","M"])
                    ->count();
                    if( $custdata!=0 ){
                        DB::table('mod_ts_order')
                        ->insert([
                            "owner_cd" => $ordall->owner_cd,
                            "owner_nm" => $ordall->owner_nm,
                            "ord_no"   => $ordall->ord_no,
                            "cust_ord_no"   => $ordall->cust_ord_no, 
                            "wms_ord_no"   => $ordall->wms_order_no,
                            "sys_ord_no" => $ordall->sys_ord_no,
                            "ord_created_at"   => $ordall->created_at,
                            "status_desc" => $statunm,
                            "finish_date"   => Carbon::now(),
                            "exp_reason" => "",
                            "error_remark" =>  "",
                            "status" => $ordall->status,
                            "abnormal_remark" =>  $ordall->abnormal_remark,
    
                            "is_send"     => "N",
                            "updated_at"  => Carbon::now(),
                            "updated_by"  => "SYSTEM",
                            "created_at"  => Carbon::now(),
                            "created_by"  => "SYSTEM",
                            "g_key"       => $g_key,
                            "c_key"       => $c_key,
                            "s_key"       => $s_key,
                            "d_key"       => $d_key,
                        ]);
                    }
                }
            }
            
            
        }catch(\Exception $ex) {
            \Log::info($ex);
            return;
        }
        
        return;
    }
}
