<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use FcmRecipient;
use FcmNotification;
use FcmClient;
use FcmConfig;
use AndroidConfig;

class FcmModel extends Model {

	public function sendToFcm($title, $msg, $token) {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($msg)
                            ->setSound('sound');
        
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
    }

    public function sendToFcmByCurl() {
        // Client instance should be created with path to service account key file
        $client = new FcmClient(base_path('service_account.json'));
        $recipient = new FcmRecipient();
        // Either Notification or Data (or both) instance should be created
        $notification = new FcmNotification();

        $recipient -> setSingleREcipient('dGrRugDGyV4:APA91bFN3ndNXIognAPcnr-TTXx-BXjnuFHJlpB4SX4uE-9MtjPUgW0dxjJ9HYYHtkHfga0_SPhSfehc9L1lRRXu9CtJsz-apO51qptN61H9xKRHET8lPcpRIUlMp76QY23UT5Wu0aAn');
        // Use phpFCMv1\AndroidConfig Class
        $notification->setNotification('NOTIFICATION_TITLE', 'NOTIFICATION_BODY');
        $androidConfig = new AndroidConfig();
        $androidConfig->setPriority(AndroidConfig::PRIORITY_HIGH);
        $androidConfig->setSound();
        $client -> build($recipient, $notification, null, $androidConfig);
        //$client -> build($recipient, $notification);

        $result = $client -> fire();
        // You can check the result
        // If successful, true will be returned
        // If not, error message will be returned
        echo $result;
    }
}