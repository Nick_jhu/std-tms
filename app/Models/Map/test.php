<?php 
namespace App\Models\Map;

use Illuminate\Support\Facades\DB;
use App\Models\Map\Shape;
use App\Models\Map\ShapePoint;

class test {
    public function test($data) {
        // $data = DB::table('mod_car_real_data')->select('lat','lng', 'speed', 'location_time', 'location_time')
        //             ->where('created_at', '>=', '2018-02-08 09:00:00')
        //             ->where('created_at', '<=', '2018-02-08 23:59:59')
        //             ->where('car_no', 'CJL-1111')
        //             ->orderBy('id', 'asc')
        //             ->get();
        
        $shape = new Shape();

        foreach($data as $key=>$row) {
            $shape->addPoint(
                new ShapePoint($row['lat'], $row['lng'], $key, $row['speed'], $row['location_time'])
            );
        }
        // echo count($shape->points());

        $reducer = new ShapeReducer();
        $shape = $reducer->reduceWithTolerance($shape, 0.00005);

        return $shape->points();
    }
}