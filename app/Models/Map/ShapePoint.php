<?php
namespace App\Models\Map;

class ShapePoint
{
    public $lat;
    public $lng;
    public $speed;
    public $timestamp;
    public $seq;

    public function __construct($lat, $lng, $seq, $speed, $timestamp)
    {
        $this->seq       = $seq;
        $this->lat       = $lat;
        $this->lng       = $lng;
        $this->speed     = $speed;
        $this->timestamp =  $timestamp;
    }
}