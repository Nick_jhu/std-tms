<?php

namespace App\Models;
namespace App\Models;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use App\Mail\EdiProblemMail;
use App\Mail\TrackingOMail;
use Illuminate\Support\Facades\Mail;
use App\Models\OrderamtDetailModel;
use DB;
use App\Models\TrackingModel;
use Excel;
use App\Models\BaseModel;
use App\Models\CalculateModel;
use Illuminate\Filesystem\FilesystemAdapter;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Ftp as FtpAdapter;
use League\Flysystem\Sftp\SftpAdapter as SFtpAdapter;
class ftpModel extends Model {

	use CrudTrait;
	
    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mod_mi';
	// protected $primaryKey = 'cust_no';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $guarded = array('id');
	public $timestamps = true;
	public function putdbbak() {
		try {
			$files = Storage::disk('dbbak')->allFiles();
		// $filesystem = new Filesystem(new SftpAdapter([
		// 	'driver' => 'sftp',
		// 	'host' => '172.105.231.75',
		// 	'port'     => 22,
		// 	'username' => 'standardinfo',
		// 	'password' =>'!standard123',
		// 	'root' => '/var/www/dbbak',
		// ]));
		// $filesystem->put('1.txt',"1.txt");
			foreach($files as $key => $row){
				Storage::disk('backups')->putFileAs("/",  new File(public_path('dbbak/'.$row)),$row);
				Storage::disk('dbbak')->delete($row);
			}
		}catch(\Exception $e) {
			\Log::info($e->getMessage());
			// $this->update_benq_status_img($row->sys_ord_no,'N');
			return $e->getMessage();
		} 
		return;
	}
	public function putlog() {
		try {
			$files = Storage::disk('logbackto')->allFiles();
			foreach($files as $key => $row){
				Storage::disk('logback')->putFileAs("/",  new File(storage_path('logs/'.$row)),$row);
			}
		}catch(\Exception $e) {
			\Log::info($e->getMessage());
			// $this->update_benq_status_img($row->sys_ord_no,'N');
			return $e->getMessage();
		} 
		return;
	}

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
    */
    public function customer()
    {
        return $this->belongsTo('App\Models\CustomerModel', 'cust_no');
    }


	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}