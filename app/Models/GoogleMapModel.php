<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class GoogleMapModel extends Model {

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
    */
    
    public function getDist($origin, $destination, $waypoints="", $way='C') {
		$mapArray = array();
		if($way == 'C01') {
			if($waypoints != "") {
				$mapArray = [
					'origin'          => $waypoints, 
					'destination'     => $waypoints, 
					'waypoints'     => "optimize:false|".$origin.'|'.$destination, 
					'language'     => 'zh-TW'
				];
			}
			else {
				$mapArray = [
					'origin'          => $origin, 
					'destination'     => $destination, 
					//'waypoints'     => "optimize:true|".$origin.'|'.$destination, 
					'language'     => 'zh-TW'
				];
			}
		}
		else if($way == 'C02') {
			if($waypoints != "") {
				$mapArray = [
					'origin'          => $waypoints, 
					'destination'     => $destination, 
					'waypoints'     => "optimize:true|".$origin, 
					'language'     => 'zh-TW'
				];
			}
			else {
				$mapArray = [
					'origin'          => $origin, 
					'destination'     => $destination, 
					//'waypoints'     => "optimize:true|".$origin.'|'.$destination, 
					'language'     => 'zh-TW'
				];
			}
		}
		else {
			$mapArray = [
				'origin'          => $origin, 
				'destination'     => $destination, 
				//'waypoints'     => "optimize:true|".$origin.'|'.$destination, 
				'language'     => 'zh-TW'
			];
		}
		
		
		$response = \GoogleMaps::load('directions')->setParam($mapArray)->get();
		
		
		$distance = $this->computeTotalDistance($response);
		
		return round($distance, 1);
	}
	
	public function computeTotalDistance($response) {
		$totalDist = 0;
		$totalTime = 0;
		$result =  json_decode($response);

		if($result->status == "ok" || $result->status == "OK") {
			$myroute = $result->routes[0];

			for ($i = 0; $i < count($myroute->legs); $i++) {
				$totalDist += $myroute->legs[$i]->distance->value;
				$totalTime += $myroute->legs[$i]->duration->value;
			}
			$totalDist = $totalDist / 1000;
			return $totalDist;
		}
		
		return 0;
	}

	public function getDistanceMatrix($dlvNo) {

		$dlvData = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->orderBy('id', 'asc')->get();


		$oArray = [];
		$dArray = [];
		$addressArray = [];
		array_push($addressArray, urlencode('台北市內湖區新湖二路17號1樓'));
		foreach($dlvData as $key=>$row) {
			array_push($addressArray, urlencode($row->addr));			
		}

		$o = implode('|', $addressArray);

		$url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&language=zh-TW&origins='.$o.'&destinations='.$o.'&key=AIzaSyAhtnGHqsP2qyrhl9PTTI3pO1aK0gjVjsw';

		//echo $url;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);

		$response_a = json_decode($response, true);

		dd($response_a);
	}

	public function getDirectionsResult($dlvNo) {
		$dlvData = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->orderBy('id', 'asc')->get();

		$addressArray = [];
		$des = '';
		foreach($dlvData as $key=>$row) {
			if($key != count($dlvData) - 1) {
				array_push($addressArray, $row->addr);
			}
			else {
				$des = $row->addr;
			}
						
		}

		$waypoints = implode('|', $addressArray);

		$mapArray = [
			'origin'          => '台北市大安區新生南路一段163號', 
			'destination'     => $des, 
			'waypoints'     => 'optimize:false|'.$waypoints, 
			'language'     => 'zh-TW'
		];

		$response = \GoogleMaps::load('directions')->setParam($mapArray)->get();

		$res = json_decode($response);
		
		$lastTime = '';
		$result = [];
		for($i=0; $i<count($res->routes[0]->legs); $i++) {
			$time = $res->routes[0]->legs[$i]->duration->value;
			

			if($i == 0 ) {
				$lastTime = date("Y-m-d H:i:s", time() + $time);
				//echo date("Y-m-d H:i:s", time() + $time).'\n';
			}
			else {
				//echo date("Y-m-d H:i:s",strtotime("+".$time." seconds",strtotime($lastTime))).'\n';
				$lastTime = date("Y-m-d H:i:s",strtotime("+".$time." seconds",strtotime($lastTime)));
			}

			array_push($result, $lastTime);
		}

		return $result;
	}

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
    */


	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}