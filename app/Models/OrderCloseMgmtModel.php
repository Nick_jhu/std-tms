<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use DB;
use DateTime;
use Illuminate\Http\Request;
class OrderCloseMgmtModel extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mod_order_close';
	protected $primaryKey = 'id';
	// protected $primaryKey = 'cust_no';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $guarded = array('id');
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
    */
    public function customer()
    {
        return $this->belongsTo('App\Models\CustomerModel', 'cust_no');
    }

    public function insertClose($obj) {
		DB::table('mod_order_close')->insert($obj);
        
        return true;
	}
	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}