<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use DateTime;
use Illuminate\Http\Request;
use App\Models\BaseModel;


class Example extends BaseModel
{
    //
    public function get_user($request)
    {
        return $this->getSearchData($request,"users");
    }

    public function get_user_column(){
        return $this->getColumnInfo("users");
    }

    public function validateDate($date, $format = 'Y-m-d')
    {
        try{
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }catch(\Exception $ex){
            return ex;
        }
       
    }
}
