<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\TsRecordModel;

class TrackingMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $trackingData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($trackingData)
    {
        $this->trackingData = $trackingData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('貨運通知')->markdown('emails.'.$this->trackingData['tpl'])->with('trackingData', $this->trackingData);;
    }
}
