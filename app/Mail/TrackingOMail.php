<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\TsRecordModel;

class TrackingOMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $trackingData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($trackingData)
    {
        $this->trackingData = $trackingData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('貨況通知')
        ->markdown('emails.'."SYL_TKMO")
        ->attach($this->trackingData['filepath'])
        ->with('trackingData', $this->trackingData);;
    }
}
