<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\TsRecordModel;

class EdiProblemMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $Data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Data)
    {
        $this->Data = $Data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('arrivalnotice@standard-info.com','EDI錯誤通知')->subject('EDI停止通知')->markdown('email.EdiProblemMail')->with('Data', $this->Data);
    }
}
