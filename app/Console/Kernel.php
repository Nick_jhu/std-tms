<?php

namespace App\Console;
use App\Models\OrderMgmtModel;
use App\Models\ftpModel;
use App\Console\Commands\testWebsocket;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
		\Log::info("check schedule");
		$schedule->call(function () {
            \Log::info("pchome edi start");
            try {
				$orderMgmtModel = new OrderMgmtModel;
                $orderMgmtModel->getsendpchome();
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getLine());
            }
            \Log::info("pchome edi end");
    
        })->everyMinute();
		
        $schedule->call(function () {
            \Log::info("Send edi start");
            try {
                $orderMgmtModel = new OrderMgmtModel;
                $orderMgmtModel->checkedi_status();
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
            }
            \Log::info("Send edi end");
    
        })->everyFiveMinutes();
        $schedule->call(function () {
            \Log::info("Send pchme amt");
            try {
                // 在每個禮拜三的 14:00 跑一次...
                $orderMgmtModel = new OrderMgmtModel;
                $orderMgmtModel->sendnoret();
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
            }
        })->weekly()->wednesdays()->at('14:00');
        $schedule->call(function () {
            \Log::info("smsquerydrreq  start");
            try {
                $orderMgmtModel = new OrderMgmtModel;
                $orderMgmtModel->smsquerydrreq();
            }
            catch(\Exception $e) {
                \Log::error($e->getLine());
                \Log::error($e->getMessage());
            }
            \Log::info("smsquerydrreq  end");
    
        })->twiceDaily(12, 23);

        $schedule->call(function () {
            \Log::info("mpushmessage  start");
            try {
                $orderMgmtModel = new OrderMgmtModel;
                $orderMgmtModel->mpushmessage();
            }
            catch(\Exception $e) {
                \Log::error($e->getLine());
                \Log::error($e->getMessage());
            }
            \Log::info("mpushmessage  end");
    
        })->everyFiveMinutes();
        // $schedule->call(function () {
        //     \Log::info("db log bak");
        //     try {
        //         $ftpModel = new ftpModel;
        //         $ftpModel->putdbbak();
        //         $ftpModel->putlog();
        //     }
        //     catch(\Exception $e) {
        //         \Log::error($e->getMessage());
        //     }
        // })->weekly()->fridays()->at('01:00');

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
