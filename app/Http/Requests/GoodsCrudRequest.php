<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class GoodsCrudRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'goods_no' => 'required|unique:mod_goods,goods_no,NULL,id,c_key,'.Auth::user()->c_key.'|min:1|max:20',
                    'goods_nm' => 'required|max:70',
                    'goods_no2' => 'nullable|max:20',
                    'goods_nm2' => 'nullable|max:70',
                    'gw' => 'nullable|numeric|min:0.01|max:9999999999999999.99',
                    'gwu' => 'nullable|min:1|max:3',
                    'cbm' => 'nullable|numeric|min:0.01|max:9999999999999999.99',
                    'cbmu' => 'nullable|min:1|max:3',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [ ];
            }
            default:break;
        }
    }

}