<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CarProfileCrudRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'cust_no' => 'required|min:1|max:20',
                    'car_no' => 'required|min:1|max:20',
                    'car_type' => 'nullable|min:1|max:20',
                    'car_type_nm' => 'nullable|min:1|max:50',
                    'car_ton' => 'nullable|min:1|max:50',
                    'owner' => 'nullable|min:1|max:50',
                    'cbm' => 'nullable|numeric|min:0.01|max:9999999999999999.99',
                    'cbmu' => 'nullable|min:1|max:3',
                    'load_rate' => 'nullable|numeric|min:0.01|max:100.00',
                    'load_weight' => 'nullable|numeric|min:0.01|max:9999999999999999.99',
                    'driver_no' => 'nullable|min:1|max:30',
                    'driver_nm' => 'nullable|min:1|max:30',
                    'person_no' => 'nullable|min:1|max:30',
                    'person_nm' => 'nullable|min:1|max:30',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [ ];
            }
            default:break;
        }
    }

}