<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SysAreaCrudRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'cntry_cd' => 'min:1|max:2',
                    'cntry_nm' => 'min:1|max:50',
                    'state_cd' => 'min:1|max:10',
                    'state_nm' => 'min:1|max:20',
                    'city_cd' => 'min:1|max:6',
                    'city_nm' => 'min:1|max:70',
                    'dist_cd' => 'min:1|max:50',
                    'dist_nm' => 'min:1|max:100',
                    'zip_f' => 'min:1|max:6',
                    'zip_e' => 'min:1|max:6',
                    'remark' => 'min:1|max:255'                    
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [ ];
            }
            default:break;
        }
    }

}