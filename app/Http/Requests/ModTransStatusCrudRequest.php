<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ModTransStatusCrudRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'ts_type' => 'nullable|min:1|max:1',                   
                    'order' => 'nullable|numeric|min:0|max:9999999999',
                    'ts_name' => 'nullable|min:1|max:50',
                    'ts_desc' => 'nullable|min:1|max:200',                   
                    'trigger_code' => 'nullable|min:1|max:50'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [ ];
            }
            default:break;
        }
    }

}