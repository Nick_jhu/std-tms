<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Models\CustomerModel;
use App\Models\SampleDetailModel;
use App\Models\BaseModel;
use App\Models\CommonModel;
use App\Models\CarProfileModel;

use App\Http\Requests\CarProfileCrudRequest as StoreRequest;
use App\Http\Requests\CarProfileCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store as Session;

class CarProfileCrudController extends CrudController
{
    
    public function setup() {
        $user = Auth::user();
        $this->crud->setModel("App\Models\CarProfileModel");
        $this->crud->setEntityNameStrings('車輛建檔', '車輛建檔');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/carProfile');
    
        $this->crud->setColumns(['cust_no']);

        
        $this->crud->setCreateView('carProfile.edit');
        $this->crud->setEditView('carProfile.edit');
        $this->crud->setListView('carProfile.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'cust_no',
            'type' => 'lookup',
            'title' => 'My Lookup',
            'info1' => Crypt::encrypt('sys_customers_item'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname"), //column
            'info3' => Crypt::encrypt("cust_type='CT003'"), //condition
            'info4' => "cust_no=cust_no", //field mapping
        ]);

        $this->crud->addField([
            'name' => 'car_no',
            'type' => 'text'
        ]);
        /*
        $this->crud->addField([
            'name' => 'car_type',
            'type' => 'lookup',
            'title' => 'My Lookup',
            'info1' => Crypt::encrypt('bscode'), //table
            'info2' => Crypt::encrypt("cd+cd_descp,cd,cd_descp"), //column
            'info3' => Crypt::encrypt("cd_type='CARTYPE'"), //condition
            'info4' => "cd=car_type;cd_descp=car_type_nm", //field mapping
        ]);
        $this->crud->addField([
            'name' => 'car_type_nm',
            'type' => 'text'
        ]);
        */

        $this->crud->addField([
            'name' => 'car_type',
            'type' => 'select2_multiple',
            'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')
                            ->where('cd_type', 'CARTYPE')
                            ->where('g_key', $user->g_key)
                            ->where('c_key', $user->c_key)
                            ->get()
        ]);

        $this->crud->addField([
            'name' => 'car_ton',
            'type' => 'text',
            // 'title' => 'My Lookup',
            // 'info1' => Crypt::encrypt('bscode'), //table
            // 'info2' => Crypt::encrypt("cd+cd_descp,cd,cd_descp"), //column
            // 'info3' => Crypt::encrypt("cd_type='TON'"), //condition
            // 'info4' => "cd=car_ton", //field mapping
        ]);

        $this->crud->addField([
            'name' => 'owner',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'cbm',
            'type' => 'number',
            'attributes' => ["step" => "any"], // allow decimals
            'suffix' => ".00"
        ]);
        $this->crud->addField([
            'name' => 'cbmu',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'load_rate',
            'type' => 'number',
            'attributes' => ["step" => "any"], // allow decimals
            'suffix' => ".00"
        ]);
        $this->crud->addField([
            'name' => 'load_weight',
            'type' => 'number',
            'attributes' => ["step" => "any"], // allow decimals
            'suffix' => ".00"
        ]);
        $this->crud->addField([
            'name' => 'driver_no',
            'type' => 'lookup',
            'title' => 'My Lookup',
            'info1' => Crypt::encrypt('users'), //table
            'info2' => Crypt::encrypt("id+name,id,name"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "id=driver_no;name=driver_nm" //field mapping            
        ]);      

        $this->crud->addField([
            'name' => 'driver_nm',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'person_no',
            'type' => 'lookup',
            'title' => 'My Lookup',
            'info1' => Crypt::encrypt('users'), //table
            'info2' => Crypt::encrypt("id+name,id,name"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "id=person_no;name=person_nm" //field mapping            
        ]);      

        $this->crud->addField([
            'name' => 'person_nm',
            'type' => 'text'
        ]);
       
        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'updated_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);

        // $this->crud->addField([
        //     'name' => 'dlv_type',
        //     'type' => 'select',
        //     'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'DLT')->get()
        // ]);

        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 's_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'hidden'
        ]);
    }    
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('carProfile'))
            {
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;
        $user = Auth::user();
        try{
            if(Auth::user()->hasPermissionTo('carProfile'))
            {
                return view($this->crud->getEditView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }
    public function store(StoreRequest $request)
	{
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        
        //dd($request->all());
        try {
            $response = parent::storeCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::updateCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response];
    }

    public function multiDel() {
        $user = Auth::user();
        $ids = request('ids');
        
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $car = CarProfileModel::find($ids[$i]);
                $car->delete();
            }
        }

        return response()->json(array('msg' => 'success'));
    }
}
