<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Models\CustomerModel;
use App\Models\BaseModel;
use App\Models\CommonModel;

use App\Http\Requests\SysCountryCrudRequest as StoreRequest;
use App\Http\Requests\SysCountryCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store as Session;

class SysCountryCrudController extends CrudController
{
    
    public function setup() {
        $this->crud->setModel("App\Models\SysCountryModel");
        $this->crud->setEntityNameStrings('國家建檔', '國家建檔');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/sysCountry');
    
        $this->crud->setColumns(['id']);

        
        $this->crud->setCreateView('sysCountry.edit');
        $this->crud->setEditView('sysCountry.edit');
        $this->crud->setListView('sysCountry.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'cntry_cd',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'cntry_nm',
            'type' => 'text'
        ]);
       
        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'updated_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 's_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'hidden'
        ]);
    }    
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('sysCountry'))
            {
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;
        $user = Auth::user();
        try{
            if(Auth::user()->hasPermissionTo('sysCountry'))
            {
                return view($this->crud->getEditView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }
    public function store(StoreRequest $request)
	{
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        
        //dd($request->all());
        try {
            $response = parent::storeCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::updateCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response];
    }   
}
