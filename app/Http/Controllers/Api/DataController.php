<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Carbon\Carbon;
use App\User;         
use App\Models\OrderMgmtModel;
use App\Models\BaseModel;
use App\Models\SysNoticeModel;
use App\Models\DlvPlanModel;    
use App\Models\OrderamtDetailModel;
use App\Models\Map\Shape;
use App\Models\Map\ShapePoint;
use Validator;
use App\Models\CalculateModel;
use App\Models\TransRecordModel;
use App\Models\DlvModel;
use App\Models\FcmModel;
use App\Models\TrackingModel;
// use App\Models\ModTrackingModel;
class DataController extends Controller
{    

    public function getUserData(Request $request)
    {
        \Log::info('getUserData');
        \Log::info($request->all());
        $type = $request->cust_type;     
        $sample_detail = [];

        $user = Auth::user();

        $this_query = DB::table('users')->where('id', $user->id);            
        $sample_detail = $this_query->get();                    
        $data[] = array(
            'Rows' => $sample_detail,
        );

        return response()->json($data);
    }

    public function getDlvData(Request $request) {   
        \Log::info('getDlvData');
        \Log::info($request->all());
        $user = Auth::user();     
        //$carNo = $request->car_no;
        $status = $request->status;     
        $modDlv = [];  

        $date = date('Y-m-d h:i:s', strtotime('-14 days'));
        $this_query = DB::table('mod_dlv');
        $this_query->select('dlv_no', 'created_at', 'departure_time', 'finished_time');
        $this_query->where('car_no', $user->email);
        if($status == 'FINISHED') {
            $this_query->whereRaw("(status = ? or status = 'ERROR')", [$status]);
            $this_query->Where('created_at', '>', $date);
            $this_query->orderBy('created_at', 'desc');
        }
        else {
            $this_query->whereRaw("(status = ? or status = 'DLV' or status = 'ERROR' or status = 'SEND')", [$status]);
        }
            
        $modDlv = $this_query->get();                           
        foreach($modDlv as $row){
            $ordnum = DB::table('mod_order')->where("dlv_no",$row->dlv_no)
            ->count();
            $row->dlv_no = $row->dlv_no." (".$ordnum.")";
        }
        return response()->json(['msg' => 'success', 'data' => $modDlv]);
    }

    public function getDlvDetailData_NEW(Request $request)
    {
		\Log::info("getDlvDetailData_NEW");
        \Log::info($request->all());
		$user       = Auth::user();
        $dlvNo      = substr($request->dlv_no,0,13);
        $filed      = $request->filed;
        $orderType  = $request->orderType;
        $modDlvPlan = [];

        $date = date('Y-m-d h:i:s', strtotime('-14 days'));
        $this_query = DB::table('mod_dlv_plan');
        $this_query->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no');
        $this_query->select('mod_order.pick_attn', 'mod_order.pick_tel', 'mod_order.pick_cust_nm', 'mod_order.dlv_attn', 'mod_order.dlv_tel', 'mod_order.dlv_cust_nm',
        DB::raw("(CASE WHEN mod_dlv_plan.status in ('FINISHED','REJECT','ERROR') THEN 1 ELSE 0 END) AS tab_status"),
        DB::raw("(select status from mod_dlv where dlv_no = mod_dlv_plan.dlv_no  limit 1 ) AS dlv_status"), 
        
        'mod_dlv_plan.sys_ord_no', 'mod_dlv_plan.remark', 'mod_dlv_plan.addr', 'mod_dlv_plan.status', 'mod_dlv_plan.sort', 'mod_dlv_plan.dlv_type',
        'mod_dlv_plan.ord_no', 'mod_dlv_plan.ord_id','mod_dlv_plan.error_descp','mod_dlv_plan.abnormal_remark', 'mod_dlv_plan.error_cd','mod_dlv_plan.dlv_no'
        );
        $this_query->where('mod_dlv_plan.dlv_no', $dlvNo);
        $this_query->where('mod_dlv_plan.g_key', $user->g_key);
        $this_query->orderBy('mod_dlv_plan.is_urgent', 'desc');


        if(isset($filed) && isset($orderType)) {
            $this_query->orderBy('mod_dlv_plan.'.$filed, $orderType);
        }
        else {
            //$this_query->orderBy('mod_dlv_plan.sys_ord_no', 'asc');
            $this_query->orderBy('mod_dlv_plan.sort', 'asc');
        }
        
        $modDlvPlan = $this_query->get();
		\Log::info("detail data");
		// \Log::info($modDlvPlan);
        return response()->json(['msg' => 'success', 'data' => $modDlvPlan]);
    }

    public function confirmOrderData(Request $request)
    {  
        try
        { 
            \Log::info("confirmOrderData");
            \Log::info($request->all());
            $user = Auth::user(); 
            if(empty($request->ord_no))
            {
                return ["msg"=>"error","data"=>"訂單號為空值，請確認"];  
            }
            else
            {     
                if($request->hasFile('file'))
                {
                    $image = $request->file('file');
                    //Storage::disk('local')->put($request->ord_no.'.jpg', base64_decode($imgData1));
                    $name = $request->ord_no.'.'.$image->getClientOriginalExtension();
                    $destinationPath = storage_path()."/image";;
                    $image->move($destinationPath, $name);
                    //$this->save();
                }
                    OrderMgmtModel::where('ord_no', $request->ord_no)
                    ->update(['status' => $request->status]);
                if(!empty($request->exp_type) && !empty($request->exp_reason))
                {
                    OrderMgmtModel::where('ord_no', $request->ord_no)
                    ->update(['exp_type' => $request->exp_type,'exp_reason'=>$request->exp_reason]);
                }
            }                     
        }
        catch(\Exception $e)
        {
            return ["msg"=>"error","data"=>$e];
        }
        return ["msg"=>"success"];
    }

    public function sendExtraData($data,$collectamt) {

        \Log::info("sendExtraData");
        \Log::info($request->all());
        $user = Auth::user();
        $extradata = array();
        if(isset($data->extradata)) {
            $extradata = $data->extradata;
        }
        $insertData = array();
        $now = date("Y-m-d H:i:s");

        try {
            $cnt = count($extradata);
            for($i=0; $i<$cnt; $i++) {
                $sysordno       = $extradata[$i]['sys_ord_no'];
                $extra_cd       = $extradata[$i]['extra_cd'];
                $extra_nm       = $extradata[$i]['extra_nm'];
                $extra_descp       = $extradata[$i]['extra_descp'];
                $orddata = DB::table('mod_order')
                ->where('sys_ord_no', $sysordno)
                ->first();
                if(isset($orddata)){
                    DB::table('mod_order')
                    ->where('sys_ord_no', $sysordno)
                    ->update(['collectamt' => $collectamt]);
                }

                DB::table('mod_order_fee')
                ->where('ord_id',  $orddata->id)
                ->delete();

                $data = [
                    'ord_id'        => $orddata->id,
                    'fee_cd'        => $extra_cd,
                    'fee_name'      => $extra_nm,
                    'fee_name'      => $extra_nm,
                    'fee_descp'      => $extra_descp,
                    'g_key'         => $user->g_key,
                    'c_key'         => $user->c_key,
                    's_key'         => $user->s_key,
                    'd_key'         => $user->d_key,
                    'created_by'    => $user->email,
                    'updated_by'    => $user->email,
                    'created_at'    => Carbon::now()->toDateTimeString(),
			        'updated_at'    => Carbon::now()->toDateTimeString(),
                ];
                array_push($insertData, $data);
            }
            $OrderamtDetailModel = new OrderamtDetailModel();
            $OrderamtDetailModel->insertfee($insertData);      
        }
        catch(\Exception $e) {
            return response()->json(["msg"=>"error","data"=>$e->getLine()]);
        }

    }

    public function getNoticeData(Request $request)
    {   
        \Log::info("getNoticeData");
        \Log::info($request->all());
        $user = Auth::user();     
        $sysNotice = [];

        $date = date('Y-m-d h:i:s', strtotime('-14 days'));

        $this_query = DB::table('sys_notice');
        $this_query->where('car_no', $user->email);
        // $this_query->where('g_key', $user->g_key);
        // $this_query->where('c_key', $user->c_key);
        // $this_query->where('s_key', $user->s_key);
        // $this_query->where('d_key', $user->d_key);
        $this_query->where('notice_time', '>', $date);  
        $this_query->orderBy('notice_time', 'desc');                         
        $sysNotice = $this_query->get();                           

        return response()->json(['msg' => 'success', 'code' => '00', 'data' => $sysNotice]);
    }

    public function updateNoticeData(Request $request)
    {  
        try
        { 
            \Log::info("updateNoticeData");
            \Log::info($request->all());
            //$user = Auth::user();
            $id = $request->id;
            if(empty($id))
            {
                return ["msg"=>"error","data"=>"id為空值，請確認"];  
            }
            else
            {                     
                SysNoticeModel::where('id', $request->id)                
                ->update(['is_read' => "Y"]);                
            }                                  
        }
        catch(\Exception $e)
        {
            return ["msg"=>"error","data"=>$e];
        }
        return ["msg"=>"success", 'code' => '00', 'data'=>null ];
    }
   

    public function ediblno(Request $request) {
        \Log::info("ediblno");
        \Log::info($request->all());
        $orderdata = array();
        $detail = array();
        $errormsg = "";

        if(isset($request->token)) {
            $token = $request->token;
        }
        else{
            return response()->json(["msg"=>"error","errormsg"=>"token 不可為空","code"=>"01"]);
        }
        $apiuser = DB::table('bscode')
        ->where('token',$token)
        ->where('cd_type', 'OPENAPICUST')
        ->first();
        if(!isset($apiuser)) {
            return response()->json(["msg"=>"error","errormsg"=>"token 錯誤","code"=>"01"]);
        }


        if(isset($request->orderdata)) {
            $orderdata = $request->orderdata;
        }
        else{
            return response()->json(["msg"=>"error","errormsg"=>"orderdata 不存在 請確認","code"=>"01"]);
        }

        try {
            $cnt = count($orderdata);
            for($i=0; $i<$cnt; $i++) {
                if(!isset($orderdata[$i]['ord_no'])){
                    $errormsg ="資料中含有未輸入訂單號之資料 請檢查";
                    return response()->json(["msg"=>"error","errormsg"=>$errormsg,"code"=>"01"]);
                }

                if($orderdata[$i]['ord_no'] == "" || $orderdata[$i]['ord_no'] == null ){
                    $errormsg ="資料中含有未輸入訂單號之資料 請檢查";
                    return response()->json(["msg"=>"error","errormsg"=>$errormsg,"code"=>"01"]);
                }

                if( strlen($orderdata[$i]['ord_no']) > 20){
                    $errormsg ="訂單號".$orderdata[$i]['ord_no']."長度超過請檢查";
                    return response()->json(["msg"=>"error","errormsg"=>$errormsg,"code"=>"01"]);
                }

                $reqiredArray = [
                    'do_status','status','ord_no','etd','is_urgent','trs_mode',
                    'temperate','owner_cd','owner_nm','dlv_zip','dlv_addr','is_forward',
                    's_key','d_key','cust_ord_no',
                ];
                // 'goods_nm','pkg_num'
                foreach ($reqiredArray as $key => $row) {
                    if(empty($orderdata[$i][$row])) {
                        $errormsg .="資料中含有未輸入".$row."之資料 請檢查";
                    }

                    if(isset($orderdata[$i]['detaildata'])){
                        $details = $orderdata[$i]['detaildata'];
                        foreach ($details as $j => $detailrow) {
                            if(empty($detailrow['goods_nm'])) {
                                $errormsg .="資料明細中含有未輸入".'goods_nm'."之資料 請檢查";
                            }
                            if(empty($detailrow['pkg_num'])) {
                                $errormsg .="資料明細中含有未輸入".'pkg_num'."之資料 請檢查";
                            }
                            # code...
                        }
                    }
                }
                if( $errormsg !='' ){
                    return response()->json(["msg"=>"error","errormsg"=>$errormsg,"code"=>"01"]);
                }
                //
                $mainColumnResult = $this->checkopenApiCoulumnType($orderdata[$i]);

                if($mainColumnResult != '') {
                    return response()->json(["msg"=>"error","errormsg"=>rtrim($mainColumnResult, ','),"code"=>"01"]);
                }

                //
                $detailColumnResult = $this->checkopenApiDetailCoulumnType($orderdata[$i]['ord_no'],$orderdata[$i]['detaildata']);

                if($detailColumnResult != '') {
                    return response()->json(["msg"=>"error", "errormsg"=>rtrim($detailColumnResult, ','), "code"=>"01"]);
                }



                // 對應建檔
                //溫藏
                $temperateDesc = DB::table('bscode')->where('cd_type','TEMPERATE')->where('cd',$orderdata[$i]['temperate'])->first();

                //運輸類型
                $tmDesc = DB::table('bscode')->where('cd_type','TM')->where('cd',$orderdata[$i]['trs_mode'])->first();

                // 配送郵遞區號
                $dlvArea = DB::table('sys_area')
                ->where('dist_cd', $orderdata[$i]['dlv_zip'])
                ->first();

                // 提貨郵遞區號
                $pickArea = DB::table('sys_area')
                ->where('dist_cd', $orderdata[$i]['pick_zip'])
                ->first();
                // 對應建檔 end

                // 字串文字 日期等等

                $dlvCityNm   = isset($dlvArea->city_nm) ? $dlvArea->city_nm : null;
                $dlvkDistNm  = isset($dlvArea->dist_nm) ?$dlvArea->dist_nm : null;
                $dlvcPlvAddr = isset($orderdata[$i]['dlv_addr']) ? $orderdata[$i]['dlv_addr'] : null;

                $pickCityNm  = isset($pickArea->city_nm) ? $pickArea->city_nm : null;
                $pickDistNm  = isset($pickArea->dist_nm) ?$pickArea->dist_nm : null;
                $pikcPlvAddr = isset($orderdata[$i]['pick_addr']) ? $orderdata[$i]['pick_addr'] : null;


                if($orderdata[$i]['do_status'] == "insert") {
                    $checkord = DB::table('mod_order')->where('ord_no',$orderdata[$i]['ord_no'])->where('g_key',$apiuser->value2)->count();
                    if($checkord > 0){
                        if($errormsg != "" ){
                            $errormsg .= ',訂單號'.$orderdata[$i]['ord_no']."已新增過";
                        }else{
                            $errormsg .= "訂單號".$orderdata[$i]['ord_no']."已新增過";
                        }
                        continue ;
                    }


                    //取得系統訂單號
                    $today       = new \DateTime();
                    $str_date    = $today->format('Ym');
                    $params = array(
                        "c_key"   => $apiuser->value2,
                        "date_ym" => substr($str_date, 2, strlen($str_date))
                    );
                    $BaseModel = new BaseModel();
                    try {
                        $autoOrdNo = $BaseModel->getAutoNumberApi("order",$params,$apiuser->value2);
                    } catch (\Throwable $th) {
                        $autoOrdNo = $BaseModel->getAutoNumberApi("order",$params,$apiuser->value2);
                    }
                    $sysordno =  $autoOrdNo;
                    //取得系統訂單號 end
                    $status = isset($orderdata[$i]['status']) ? $orderdata[$i]['status'] : null;
                    $status_desc = '';
                    if( $status == 'UNTREATED' ) {
                        $status_desc = '尚未安排';
                    } else if( $status == 'READY' )  {
                        $status_desc = '可派車';
                    } else {
                        $errormsg .= "訂單號".$orderdata[$i]['ord_no']."狀態輸入錯誤";
                        continue ;
                    }
                    //key 
                    DB::table('mod_order')->insert([
                        'sys_ord_no'      => $sysordno,
                        'g_key'           => $apiuser->value2,
                        'c_key'           => $apiuser->value2,
                        's_key'           => isset($orderdata[$i]['s_key']) ? $orderdata[$i]['s_key'] : null,
                        'd_key'           => isset($orderdata[$i]['d_key']) ? $orderdata[$i]['d_key'] : null,
                        'ord_no'          => $orderdata[$i]['ord_no'],
                        'status'          => $status,
                        'status_desc'     => $status_desc,
                        'collectamt'      => isset($orderdata[$i]['collectamt']) ? $orderdata[$i]['collectamt'] : null,
                        'remark'          => isset($orderdata[$i]['remark']) ? $orderdata[$i]['remark'] : null,
                        'dlv_cust_nm'     => isset($orderdata[$i]['dlv_cust_nm']) ? $orderdata[$i]['dlv_cust_nm'] : null,
                        'dlv_cust_no'     => isset($orderdata[$i]['dlv_cust_no']) ? $orderdata[$i]['dlv_cust_no'] : null,
                        'dlv_addr'        => isset($orderdata[$i]['dlv_addr']) ? $orderdata[$i]['dlv_addr'] : null,
                        'dlv_attn'        => isset($orderdata[$i]['dlv_attn']) ? $orderdata[$i]['dlv_attn'] : null,
                        'dlv_tel'         => isset($orderdata[$i]['dlv_tel']) ? $orderdata[$i]['dlv_tel'] : null,
                        'dlv_tel2'        => isset($orderdata[$i]['dlv_tel2']) ? $orderdata[$i]['dlv_tel2'] : null,
                        'dlv_city_nm'     => isset($dlvArea->city_nm) ?  $dlvArea->city_nm : null,
                        'dlv_zip'         => isset($orderdata[$i]['dlv_zip']) ? $orderdata[$i]['dlv_zip'] : null,
                        'dlv_area_nm'     => isset($dlvArea->dist_nm) ?  $dlvArea->dist_nm : null,
                        'etd'             => isset($orderdata[$i]['etd']) ? $orderdata[$i]['etd'] : null,
                        'truck_no'        => isset($orderdata[$i]['car_no']) ? $orderdata[$i]['car_no'] : null,
                        'driver'          => isset($orderdata[$i]['driver']) ? $orderdata[$i]['driver'] : null,
                        'car_no'          => isset($orderdata[$i]['car_no']) ? $orderdata[$i]['car_no'] : null,
                        'trs_mode'        => isset($orderdata[$i]['trs_mode']) ? $orderdata[$i]['trs_mode'] : null,
                        'pick_cust_no'    => isset($orderdata[$i]['pick_cust_no']) ? $orderdata[$i]['pick_cust_no'] : null,
                        'pick_cust_nm'    => isset($orderdata[$i]['pick_cust_nm']) ? $orderdata[$i]['pick_cust_nm'] : null,
                        'pick_addr'       => isset($orderdata[$i]['pick_addr']) ? $orderdata[$i]['pick_addr'] : null,
                        'pick_attn'       => isset($orderdata[$i]['pick_attn']) ? $orderdata[$i]['pick_attn'] : null,
                        'pick_tel'        => isset($orderdata[$i]['pick_tel']) ? $orderdata[$i]['pick_tel'] : null,
                        'pick_tel2'       => isset($orderdata[$i]['pick_tel2']) ? $orderdata[$i]['pick_tel2'] : null,
                        'pick_city_nm'    => isset($pickArea->city_nm) ?  $pickArea->city_nm : null,
                        'pick_zip'        => isset($orderdata[$i]['pick_zip']) ? $orderdata[$i]['pick_zip'] : null,
                        'pick_area_nm'    => isset($pickArea->dist_nm) ?  $pickArea->dist_nm : null,
                        'dlv_email'       => isset($orderdata[$i]['dlv_email']) ? $orderdata[$i]['dlv_email'] : null,
                        'pick_email'      => isset($orderdata[$i]['pick_email']) ? $orderdata[$i]['pick_email'] : null,
                        'wh_addr'         => isset($orderdata[$i]['wh_addr']) ? $orderdata[$i]['wh_addr'] : null,
                        'owner_cd'        => isset($orderdata[$i]['owner_cd']) ? $orderdata[$i]['owner_cd'] : null,
                        'owner_nm'        => isset($orderdata[$i]['owner_nm']) ? $orderdata[$i]['owner_nm'] : null,
                        'dlv_remark'      => isset($orderdata[$i]['dlv_remark']) ? $orderdata[$i]['dlv_remark'] : null,
                        'pick_remark'     => isset($orderdata[$i]['pick_remark']) ? $orderdata[$i]['pick_remark'] : null,
                        'owner_send_mail' => isset($orderdata[$i]['owner_send_mail']) ? $orderdata[$i]['owner_send_mail'] : null,
                        'pick_send_mail'  => isset($orderdata[$i]['pick_send_mail']) ? $orderdata[$i]['pick_send_mail'] : null,
                        'dlv_send_mail'   => isset($orderdata[$i]['dlv_send_mail']) ? $orderdata[$i]['dlv_send_mail'] : null,
                        'cust_ord_no'     => isset($orderdata[$i]['cust_ord_no']) ? $orderdata[$i]['cust_ord_no'] : null,
                        'is_urgent'       => isset($orderdata[$i]['is_urgent']) ? $orderdata[$i]['is_urgent'] : null,
                        'temperate'       => isset($orderdata[$i]['temperate']) ? $orderdata[$i]['temperate'] : null,
                        'temperate_desc'  => isset($temperateDesc->cd_descp) ? $temperateDesc->cd_descp : null,
                        'trs_mode_desc'   => isset($tmDesc->cd_descp) ? $tmDesc->cd_descp : null,
                        'wms_order_no'    => isset($orderdata[$i]['wms_order_no']) ? $orderdata[$i]['wms_order_no'] : null,
                        'truck_cmp_nm'    => isset($orderdata[$i]['truck_cmp_nm']) ? $orderdata[$i]['truck_cmp_nm'] : null,
                        'truck_cmp_no'    => isset($orderdata[$i]['truck_cmp_no']) ? $orderdata[$i]['truck_cmp_no'] : null,
                        'collectamt'      => isset($orderdata[$i]['collectamt']) ? $orderdata[$i]['collectamt'] : null,
                        'amt_remark'      => isset($orderdata[$i]['amt_remark']) ? $orderdata[$i]['amt_remark'] : null,
                        'truck_cmp_nm'    => isset($orderdata[$i]['truck_cmp_nm']) ? $orderdata[$i]['truck_cmp_nm'] : null,
                        'truck_cmp_no'    => isset($orderdata[$i]['truck_cmp_no']) ? $orderdata[$i]['truck_cmp_no'] : null,
                        'collectamt'      => isset($orderdata[$i]['collectamt']) ? $orderdata[$i]['collectamt'] : null,
                        'amt_remark'      => isset($orderdata[$i]['amt_remark']) ? $orderdata[$i]['amt_remark'] : null,
                        'dlv_addr_info'   => $dlvCityNm. $dlvkDistNm.$dlvcPlvAddr,
                        'pick_addr_info'  => $pickCityNm.$pickDistNm.$pikcPlvAddr,
                        'updated_by'      => 'SYSTEM',
                        'created_by'      => 'SYSTEM',
                        'created_at'      => \Carbon::now(),
                        'updated_at'      => \Carbon::now(),
                    ]);

                    $neworder = DB::connection('mysql::write')
                    ->table('mod_order')
                    ->where('ord_no',$orderdata[$i]['ord_no'])
                    ->where('g_key',$apiuser->value2)
                    ->first();
                    if(isset($orderdata[$i]['detaildata'])){
                        $detail = $orderdata[$i]['detaildata'];;

                        for($k=0; $k<count($detail); $k++){

                            DB::table('mod_order_detail')->insert([
                                'ord_no'     => $orderdata[$i]['ord_no'],
                                'goods_no'   => isset($detail[$k]['goods_no']) ? $detail[$k]['goods_no'] : '',
                                'goods_nm'   => isset($detail[$k]['goods_nm']) ? $detail[$k]['goods_nm'] : '',
                                'pkg_num'    => isset($detail[$k]['pkg_num']) ? $detail[$k]['pkg_num'] : '',
                                'pkg_unit'   => isset($detail[$k]['pkg_unit']) ? $detail[$k]['pkg_unit'] : '',
                                'gw'         => isset($detail[$k]['gw']) ? $detail[$k]['gw'] : '',
                                'gwu'        => isset($detail[$k]['gwu']) ? $detail[$k]['gwu'] : '',
                                'cbm'        => isset($detail[$k]['cbm']) ? $detail[$k]['cbm'] : '',
                                'cbmu'       => isset($detail[$k]['cbmu']) ? $detail[$k]['cbmu'] : '',
                                'length'     => isset($detail[$k]['length']) ? $detail[$k]['length'] : '',
                                'weight'     => isset($detail[$k]['weight']) ? $detail[$k]['weight'] : '',
                                'height'     => isset($detail[$k]['height']) ? $detail[$k]['height'] : '',
                                'goods_no2'  => isset($detail[$k]['goods_no2']) ? $detail[$k]['goods_no2'] : '',
                                'ord_id'     => $neworder->id,
                                'sn_no'      => isset($detail[$k]['sn_no']) ? $detail[$k]['sn_no'] : '',
                                'updated_by' => 'SYSTEM',
                                'created_by' => 'SYSTEM',
                                'created_at' => \Carbon::now(),
                                'updated_at' => \Carbon::now(),
                            ]);
                            $this->updateModOrderGw($neworder->id);
                        }
                    }

                }else if($orderdata[$i]['do_status']== "edit" ) {
                    continue ;
                    $neworder = DB::connection('mysql::write')->table('mod_order')
                    ->where('ord_no',$orderdata[$i]['ord_no'])
                    ->where('g_key',$apiuser->value2)
                    ->first();

                    if(!isset($neworder)) {
                        return response()->json(["msg"=>"error","errormsg"=>"訂單號".$orderdata[$i]['ord_no']."不存在","code"=>"01"]);
                    }

                    DB::table('mod_order_detail')
                    ->where('ord_id',  $neworder->id)
                    ->delete();
                    $dlv_area = DB::table('sys_area')
                    ->where('dist_cd', $orderdata[$i]['dlv_zip'])
                    ->first();
                    $pick_area = DB::table('sys_area')
                    ->where('dist_cd', $orderdata[$i]['pick_zip'])
                    ->first();

                    DB::table('mod_order')->where('id',$neworder->id)
                    ->update([
                        'collectamt'      => isset($orderdata[$i]['collectamt']) ? $orderdata[$i]['collectamt'] : null,
                        'remark'          => isset($orderdata[$i]['remark']) ? $orderdata[$i]['remark'] : null,
                        'dlv_cust_nm'     => isset($orderdata[$i]['dlv_cust_nm']) ? $orderdata[$i]['dlv_cust_nm'] : null,
                        'dlv_cust_no'     => isset($orderdata[$i]['dlv_cust_no']) ? $orderdata[$i]['dlv_cust_no'] : null,
                        'dlv_addr'        => isset($orderdata[$i]['dlv_addr']) ? $orderdata[$i]['dlv_addr'] : null,
                        'dlv_attn'        => isset($orderdata[$i]['dlv_attn']) ? $orderdata[$i]['dlv_attn'] : null,
                        'dlv_tel'         => isset($orderdata[$i]['dlv_tel']) ? $orderdata[$i]['dlv_tel'] : null,
                        'dlv_tel2'        => isset($orderdata[$i]['dlv_tel2']) ? $orderdata[$i]['dlv_tel2'] : null,
                        'dlv_city_nm'     => isset($dlvArea->city_nm) ?  $dlvArea->city_nm : null,
                        'dlv_zip'         => isset($orderdata[$i]['dlv_zip']) ? $orderdata[$i]['dlv_zip'] : null,
                        'dlv_area_nm'     => isset($dlvArea->dist_nm) ?  $dlvArea->dist_nm : null,
                        'etd'             => isset($orderdata[$i]['etd']) ? $orderdata[$i]['etd'] : null,
                        'truck_no'        => isset($orderdata[$i]['car_no']) ? $orderdata[$i]['car_no'] : null,
                        'driver'          => isset($orderdata[$i]['driver']) ? $orderdata[$i]['driver'] : null,
                        'car_no'          => isset($orderdata[$i]['car_no']) ? $orderdata[$i]['car_no'] : null,
                        'trs_mode'        => isset($orderdata[$i]['trs_mode']) ? $orderdata[$i]['trs_mode'] : null,
                        'pick_cust_no'    => isset($orderdata[$i]['pick_cust_no']) ? $orderdata[$i]['pick_cust_no'] : null,
                        'pick_cust_nm'    => isset($orderdata[$i]['pick_cust_nm']) ? $orderdata[$i]['pick_cust_nm'] : null,
                        'pick_addr'       => isset($orderdata[$i]['pick_addr']) ? $orderdata[$i]['pick_addr'] : null,
                        'pick_attn'       => isset($orderdata[$i]['pick_attn']) ? $orderdata[$i]['pick_attn'] : null,
                        'pick_tel'        => isset($orderdata[$i]['pick_tel']) ? $orderdata[$i]['pick_tel'] : null,
                        'pick_tel2'       => isset($orderdata[$i]['pick_tel2']) ? $orderdata[$i]['pick_tel2'] : null,
                        'pick_city_nm'    => isset($pickArea->city_nm) ?  $pickArea->city_nm : null,
                        'pick_zip'        => isset($orderdata[$i]['pick_zip']) ? $orderdata[$i]['pick_zip'] : null,
                        'pick_area_nm'    => isset($pickArea->dist_nm) ?  $pickArea->dist_nm : null,
                        'dlv_email'       => isset($orderdata[$i]['dlv_email']) ? $orderdata[$i]['dlv_email'] : null,
                        'pick_email'      => isset($orderdata[$i]['pick_email']) ? $orderdata[$i]['pick_email'] : null,
                        'wh_addr'         => isset($orderdata[$i]['wh_addr']) ? $orderdata[$i]['wh_addr'] : null,
                        'owner_cd'        => isset($orderdata[$i]['owner_cd']) ? $orderdata[$i]['owner_cd'] : null,
                        'owner_nm'        => isset($orderdata[$i]['owner_nm']) ? $orderdata[$i]['owner_nm'] : null,
                        'dlv_remark'      => isset($orderdata[$i]['dlv_remark']) ? $orderdata[$i]['dlv_remark'] : null,
                        'pick_remark'     => isset($orderdata[$i]['pick_remark']) ? $orderdata[$i]['pick_remark'] : null,
                        'owner_send_mail' => isset($orderdata[$i]['owner_send_mail']) ? $orderdata[$i]['owner_send_mail'] : null,
                        'pick_send_mail'  => isset($orderdata[$i]['pick_send_mail']) ? $orderdata[$i]['pick_send_mail'] : null,
                        'dlv_send_mail'   => isset($orderdata[$i]['dlv_send_mail']) ? $orderdata[$i]['dlv_send_mail'] : null,
                        'cust_ord_no'     => isset($orderdata[$i]['cust_ord_no']) ? $orderdata[$i]['cust_ord_no'] : null,
                        'is_urgent'       => isset($orderdata[$i]['is_urgent']) ? $orderdata[$i]['is_urgent'] : null,
                        'temperate'       => isset($orderdata[$i]['temperate']) ? $orderdata[$i]['temperate'] : null,
                        'temperate_desc'  => isset($temperateDesc->cd_descp) ? $temperateDesc->cd_descp : null,
                        'trs_mode_desc'   => isset($tmDesc->cd_descp) ? $tmDesc->cd_descp : null,
                        'wms_order_no'    => isset($orderdata[$i]['wms_order_no']) ? $orderdata[$i]['wms_order_no'] : null,
                        'truck_cmp_nm'    => isset($orderdata[$i]['truck_cmp_nm']) ? $orderdata[$i]['truck_cmp_nm'] : null,
                        'truck_cmp_no'    => isset($orderdata[$i]['truck_cmp_no']) ? $orderdata[$i]['truck_cmp_no'] : null,
                        'collectamt'      => isset($orderdata[$i]['collectamt']) ? $orderdata[$i]['collectamt'] : null,
                        'amt_remark'      => isset($orderdata[$i]['amt_remark']) ? $orderdata[$i]['amt_remark'] : null,
                        'dlv_addr_info'   => isset($dlvArea->city_nm) ? $dlvArea->city_nm : null . isset($dlvArea->dist_nm) ?$dlvArea->dist_nm : null . isset($orderdata[$i]['dlv_addr']) ? $orderdata[$i]['dlv_addr'] : null,
                        'pick_addr_info'  => isset($pickArea->city_nm) ? $pickArea->city_nm : null . isset($pickArea->dist_nm) ?$pickArea->dist_nm : null . isset($orderdata[$i]['pick_addr']) ? $orderdata[$i]['pick_addr'] : null,
                        'updated_by'      => 'SYSTEM',
                        'updated_at'      => \Carbon::now(),
                    ]);
                    if(isset($orderdata[$i]['detaildata'])) {
                        DB::table('mod_order_detail')
                        ->where('ord_id', $neworder->id)
                        ->delete();
                        
                        $detail = $orderdata[$i]['detaildata'];
                        for($k=0; $k<count($detail); $k++){
                            DB::table('mod_order_detail')->insert([
                                'ord_no'     => $orderdata[$i]['ord_no'],
                                'goods_no'   => isset($detail[$k]['goods_no']) ? $detail[$k]['goods_no'] : null,
                                'goods_nm'   => isset($detail[$k]['goods_nm']) ? $detail[$k]['goods_nm'] : null,
                                'pkg_num'    => isset($detail[$k]['pkg_num']) ? $detail[$k]['pkg_num'] : null,
                                'pkg_unit'   => isset($detail[$k]['pkg_unit']) ? $detail[$k]['pkg_unit'] : null,
                                'gw'         => isset($detail[$k]['gw']) ? $detail[$k]['gw'] : null,
                                'gwu'        => isset($detail[$k]['gwu']) ? $detail[$k]['gwu'] : null,
                                'cbm'        => isset($detail[$k]['cbm']) ? $detail[$k]['cbm'] : null,
                                'cbmu'       => isset($detail[$k]['cbmu']) ? $detail[$k]['cbmu'] : null,
                                'length'     => isset($detail[$k]['length']) ? $detail[$k]['length'] : null,
                                'weight'     => isset($detail[$k]['weight']) ? $detail[$k]['weight'] : null,
                                'height'     => isset($detail[$k]['height']) ? $detail[$k]['height'] : null,
                                'goods_no2'  => isset($detail[$k]['goods_no2']) ? $detail[$k]['goods_no2'] : null,
                                'ord_id'     => $neworder->id,
                                'sn_no'      => isset($detail[$k]['sn_no']) ? $detail[$k]['sn_no'] : null,
                                'updated_by' => 'SYSTEM',
                                'created_by' => 'SYSTEM',
                                'created_at' => \Carbon::now(),
                                'updated_at' => \Carbon::now(),
                            ]);
                            $this->updateModOrderGw($neworder->id);
                        }
                    }

                }else if($orderdata[$i]['do_status'] == "fail" ) {
                    $neworder = DB::table('mod_order')->where('ord_no',$orderdata[$i]['ord_no'])->where('g_key',$apiuser->value2)->first();
                    if(!isset($neworder)) {
                        return response()->json(["msg"=>"error","errormsg"=>"訂單號".$orderdata[$i]['ord_no']."不存在","code"=>"01"]);
                    }

                    DB::table('mod_order')
                    ->where('id',  $neworder->id)
                    ->update([
                        'status'      => 'FAIL',
                        'status_desc' => '作廢',
                    ]);
                    // DB::table('mod_order_detail')
                    // ->where('ord_id',  $neworder->id)
                    // ->delete();
                }
                else{
                    return response()->json(["msg"=>"error","errormsg"=>"訂單號".$orderdata[$i]['ord_no']."內容錯誤","code"=>"01"]);
                }
            }
            if($errormsg != ""){
                return response()->json(["msg"=>"error","errormsg"=>$errormsg,"code"=>"01"]);
            }
        }
        catch(\Exception $e) {
            return response()->json(["msg"=>"error","errormsg"=>$e->getMessage(),"errorline"=>$e->getLine(),"code"=>"01"]);
        }

        return response()->json(["msg"=>"success","code"=>"00"]);
    }

    private function updateModOrderGw($ord_id) {
        //材積 重量 重新計算
        \Log::info('updateModOrderGw-api');
        $detailData = DB::connection('mysql::write')->table('mod_order_detail')
                        ->select(DB::raw('SUM(gw) as total_gw'), DB::raw('SUM(pkg_num) as pkg_num'), DB::raw('SUM(cbm) as total_cbm'), DB::raw('sum(price*pkg_num) as total_price'),'pkg_unit')
                        ->where('ord_id', $ord_id)
                        ->first();
        if(isset($detailData)) {
            $pkgUnit = null;
            $detailUnit = DB::connection('mysql::write')->table('mod_order_detail')
                        ->select('pkg_unit')
                        ->whereNotNull('pkg_unit')
                        ->where('ord_id', $ord_id)
                        ->first();
            if(isset($detailUnit)) {
                $pkgUnit = $detailUnit->pkg_unit;
            }
            \Log::info($ord_id);
            \Log::info($detailData->total_gw);
            \Log::info($detailData->pkg_num);
            \Log::info($detailData->total_cbm);
            \Log::info($detailData->total_price);
            OrderMgmtModel::where('id', $ord_id)
                        ->update(['total_gw' => $detailData->total_gw, 'pkg_num' => $detailData->pkg_num, 'total_cbm' => $detailData->total_cbm, 'pkg_unit' => $pkgUnit, 'pkg_unit'  => $pkgUnit]);
        }else{
            \Log::info('updateModOrderGw-order null');
            OrderMgmtModel::where('id', $ord_id)
                        ->update(['total_gw' => null, 'pkg_num' => null, 'total_cbm' => null, 'pkg_unit' => null, 'amt' => null]);
        }
        if(isset($detailData)) {
            OrderMgmtModel::where('id', $ord_id)
                        ->update(['amt' => $detailData->total_price]);
        }
        \Log::info('updateModOrderGw-api end');
        return;
    }

    public function sendGpsData(Request $request) {
        \Log::info("sendGpsData");
        \Log::info($request->all());
        $shape = new Shape();
        $user = Auth::user();
        $gps = array();
        if(isset($request->gps)) {
            $gps = $request->gps;
        }

        $lng = 0;
        $lat = 0;
        $speed = 0;
        $insertData = array();
        $now = date("Y-m-d H:i:s");

        try {
            $cnt = count($gps);
            for($i=0; $i<$cnt; $i++) {
                $lng       = $gps[$i]['lng'];
                $lat       = $gps[$i]['lat'];
                $speed     = $gps[$i]['speed'];
                $timestamp = $gps[$i]['timestamp'];

                // $shape->addPoint(
                //     new ShapePoint($lat, $lng, $i, $speed, $timestamp)
                // );
                
                $data = [
                    'car_no'        => $user->email,
                    'lat'           => $lat,
                    'lng'           => $lng,
                    'speed'         => round($speed),
                    'power'         => $request->power,
                    'location_time' => $timestamp,
                    'driver_no'     => null,
                    'driver_nm'     => $user->name,
                    'person_no'     => null,
                    'person_nm'     => null,
                    'dlv_no'        => null,
                    'location_time' => $timestamp,
                    'speed'      => $speed,
                    'g_key'         => $user->g_key,
                    'c_key'         => $user->c_key,
                    's_key'         => $user->s_key,
                    'd_key'         => $user->d_key,
                    'created_by'    => $user->email,
                    'updated_by'    => $user->email,
                    'created_at'    => $now,
			        'updated_at'    => $now,
                ];

                array_push($insertData, $data);
            }

            $TrackingModel = new TrackingModel();
            $TrackingModel->insertGps($insertData);
            //$ModTrackingModel = new ModTrackingModel();
           // $ModTrackingModel->insertGps($insertData);
            $userData = DB::table('users')
            ->where('id', $user->id)
            ->update(['gps_time' => Carbon::now()->toDateTimeString()]);
            $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/TMSApp-1bf8dcff7c4b.json');

            $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->withDatabaseUri('https://tmsapp-22e76.firebaseio.com/')
                ->create();
            $database = $firebase->getDatabase();
            $reference = $database->getReference('/raw-locations//'.$user->c_key.'/'.$user->email);

            $value = $reference->getValue();
            if($value == null) {
                $database->getReference('/raw-locations//'.$user->c_key) // this is the root reference
                ->update([
                    $user->email => [
                        0 => [
                            'lat'   => $lat,
                            'lng'   => $lng,
                            'power' => $request->power,
                            'speed' => $speed,
                            'time'  => Carbon::now()->toDateTimeString()
                        ],
                        1=> [
                            'lat'   => $lat,
                            'lng'   => $lng,
                            'power' => $request->power,
                            'speed' => $request->speed,
                            'time'  => Carbon::now()->toDateTimeString()
                        ]
                    ]
                ]);
            }
            else {
                $lastInfo = $value[1];
            
                $newData = [
                    'lat'   => $lat,
                    'lng'   => $lng,
                    'power' => $request->power,
                    'speed' => $speed,
                    'time'  => Carbon::now()->toDateTimeString()
                ];


                $updates = [
                    '/raw-locations//'.$user->c_key.'//'.$user->email.'/0' => $lastInfo,
                    '/raw-locations//'.$user->c_key.'//'.$user->email.'/1' => $newData,
                ];

                $database->getReference() // this is the root reference
                ->update($updates);
            }            
        }
        catch(\Exception $e) {
            return response()->json(["msg"=>"error","data"=>$e->getMessage()]);
        }

        return response()->json(["msg"=>"success","code"=>"00"]);
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        if (\Auth::guard('api')->check()) {
            $user = \Auth::guard('api')->user();
            DB::table('users')->where('id', $user->id)->update(['d_token' => null, 'online' => 0]);
            \Auth::guard('api')->user()->token()->delete();
        }

        return ['msg' => 'success',  'data' => '登出成功'];
    }

    public function modifyUser() {
        try {
            $user = Auth::user();
            $update = array();

            if(request('password') != null) {
                $update = array(
                    'name'     => request('name'),
                    'password' => Hash::make(request('password')),
                    'phone'    => request('phone')
                );
            }
            else {
                $update = array(
                    'name'     => request('name'),
                    'phone'    => request('phone')
                );
            }
            
            
            $userData = DB::table('users')
                            ->where('id', $user->id)
                            ->update($update);;
            
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e]);
        }

        return response()->json(['msg' => 'success']);
    }

    public function refreshpass() {
        try {
            $user = Auth::user();      
            $userData = DB::table('users')
                            ->where('id', $user->id)
                            ->update(['final_refresh' => Carbon::now()->toDateTimeString()]);
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e]);
        }

        return response()->json(['msg' => 'success']);
    }


    public function getChatroomList() {
        $user = Auth::user();
        $chatroomList = array();
        try {
            $chatroomList = DB::table('users')
                                ->where('g_key', $user->g_key)
                                ->where('c_key', $user->c_key)
                                // ->where('s_key', $user->s_key)
                                // ->where('d_key', $user->d_key)
                                ->where('identity', 'C')
                                ->select('id', 'name','email', 'phone', 'online')
                                ->get();
            if(count($chatroomList) > 0) {
                foreach($chatroomList as $key=>$row) {
                    $row->img = 'https://placehold.it/160x160/7986CB/ffffff/&text='.mb_substr($row->name, 0, 1);
                }
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e]);
        }

        return response()->json(['msg' => 'success', 'data' => array()]);
    }

    public function trackingSwitch($switch) {
        $user = Auth::user();

        try {
            if($switch != null) {
                // DB::table('users')
                // ->where('email', $user->email)
                // ->update(['online' => $switch]);
            }
            else {
                return response()->json(['msg' => 'error', 'error_log' => '請說明您要開還是關']);
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e]);
        }

        return response()->json(['msg' => 'error', 'error_log' => '登入狀態無法關閉']);
    }

    public function getOrdDetail($sysOrdNo,$type) {
        \Log::info("getOrdDetail 2");
        \Log::info($sysOrdNo);
        \Log::info($type);
        $user   = Auth::user();
        $main   = null;
        $detail = null;
        $fee    = null;
        try {
            $thisQuery = DB::table('mod_order')
                            ->where('sys_ord_no', $sysOrdNo);
            if($type == 'D') {
                if(isset($user->g_key)){
                    if($user->g_key!="SYL"){
                        $thisQuery->select(DB::raw('dlv_attn as attn'), DB::raw('dlv_tel as tel'), DB::raw("CONCAT(dlv_city_nm,dlv_area_nm,dlv_addr) as address"),'collectamt', 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'id', 'sys_ord_no', DB::raw('dlv_remark as remark'), 'dlv_no', 'ord_no', 'status');
                    }else{
                        $thisQuery->select(DB::raw('dlv_attn as attn'), DB::raw('dlv_tel as tel'), DB::raw("dlv_addr as address"), 'pkg_num', 'total_gw', 'cbm','collectamt','total_cbm', 'id', 'sys_ord_no', DB::raw('dlv_remark as remark'), 'dlv_no', 'ord_no', 'status');
                    }
                }else{
                    $thisQuery->select(DB::raw('dlv_attn as attn'), DB::raw('dlv_tel as tel'), DB::raw("dlv_addr as address"), 'pkg_num', 'total_gw', 'cbm','collectamt','total_cbm', 'id', 'sys_ord_no', DB::raw('dlv_remark as remark'), 'dlv_no', 'ord_no', 'status');
                }
            }else {
                if($user->g_key!="SYL"){
                    $thisQuery->select(DB::raw('pick_attn as attn'), DB::raw('pick_tel as tel'), DB::raw("CONCAT(pick_city_nm,pick_area_nm,pick_addr) as address"),'collectamt', 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'id', 'sys_ord_no', DB::raw('pick_remark as remark'), 'dlv_no', 'ord_no', 'status');
                }else{
                    $thisQuery->select(DB::raw('pick_attn as attn'), DB::raw('pick_tel as tel'), DB::raw("pick_addr as address"),'collectamt', 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'id', 'sys_ord_no', DB::raw('pick_remark as remark'), 'dlv_no', 'ord_no', 'status');
                }
            }

            $main = $thisQuery->first();

            if(isset($main)) {
                $detail = DB::table('mod_order_detail')
                            ->where('ord_id', $main->id)
                            ->get();
                $fee = DB::table('mod_order_fee')
                            ->where('ord_id', $main->id)
                            ->get();
            }else{

                $thisQuery = DB::table('mod_order_close')
                ->where('sys_ord_no', $sysOrdNo);
                if($type == 'D') {
                    if(isset($user->g_key)){
                        if($user->g_key!="SYL"){
                            $thisQuery->select(DB::raw('dlv_attn as attn'), DB::raw('dlv_tel as tel'), DB::raw("CONCAT(dlv_city_nm,dlv_area_nm,dlv_addr) as address"),'collectamt', 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'ord_id', 'sys_ord_no', DB::raw('dlv_remark as remark'), 'dlv_no', 'ord_no', 'status');
                        }else{
                            $thisQuery->select(DB::raw('dlv_attn as attn'), DB::raw('dlv_tel as tel'), DB::raw("dlv_addr as address"), 'pkg_num', 'total_gw', 'cbm','collectamt','total_cbm', 'ord_id', 'sys_ord_no', DB::raw('dlv_remark as remark'), 'dlv_no', 'ord_no', 'status');
                        }
                    }else{
                        $thisQuery->select(DB::raw('dlv_attn as attn'), DB::raw('dlv_tel as tel'), DB::raw("dlv_addr as address"), 'pkg_num', 'total_gw', 'cbm','collectamt','total_cbm', 'ord_id', 'sys_ord_no', DB::raw('dlv_remark as remark'), 'dlv_no', 'ord_no', 'status');
                    }
                }else {
                    if($user->g_key!="SYL"){
                        $thisQuery->select(DB::raw('pick_attn as attn'), DB::raw('pick_tel as tel'), DB::raw("CONCAT(pick_city_nm,pick_area_nm,pick_addr) as address"),'collectamt', 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'ord_id', 'sys_ord_no', DB::raw('pick_remark as remark'), 'dlv_no', 'ord_no', 'status');
                    }else{
                        $thisQuery->select(DB::raw('pick_attn as attn'), DB::raw('pick_tel as tel'), DB::raw("pick_addr as address"),'collectamt', 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'ord_id', 'sys_ord_no', DB::raw('pick_remark as remark'), 'dlv_no', 'ord_no', 'status');
                    }
                }

                $main = $thisQuery->first();

                $detail = DB::table('mod_order_detail')
                ->where('ord_id', $main->ord_id)
                ->get();
                $fee = DB::table('mod_order_fee')
                ->where('ord_id', $main->ord_id)
                ->get();
            }
        }
        catch(\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage(), 'error_line' => $e->getLine()]);
        }

        $result = array(
            'msg'    => 'success',
            'main'   => $main,
            'detail' => $detail,
            'fee' => $fee
        );

        return response()->json($result);
    }

    public function getOrdDetail_new($sysOrdNo=null,$type=null,$dlv_no=null) {
        \Log::info("getOrdDetail_new");
        $user   = Auth::user();
        $main   = null;
        $detail = null;
        $fee    = null;
        \Log::info($sysOrdNo);
        \Log::info($type);
        \Log::info($dlv_no);
        try {
            $thisQuery = DB::table('mod_order')
                            ->where('sys_ord_no', $sysOrdNo);
            if($type == 'D') {
                $thisQuery->select(
                    DB::raw('dlv_attn as attn'),
                    DB::raw("(select error_cd from mod_dlv_plan where dlv_type= 'D' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as error_cd"),
                    DB::raw("(select abnormal_remark from mod_dlv_plan where dlv_type= 'D' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as abnormal_remark"),
                    DB::raw("(select error_descp from mod_dlv_plan where dlv_type= 'D' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1) as error_remark"),
                    DB::raw('dlv_tel as tel'), DB::raw("CONCAT(dlv_city_nm,dlv_area_nm,dlv_addr) as address"),
                    'collectamt', 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'id', 'sys_ord_no','dlv_no', 'ord_no', 'status',
                    DB::raw('dlv_remark as remark'), 'dlv_no'
                   );
            }else {
                $thisQuery->select(
                    DB::raw('pick_attn as attn'),
                    DB::raw('pick_tel as tel'), 
                    DB::raw("CONCAT(pick_city_nm,pick_area_nm,pick_addr) as address"),
                    'collectamt', 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'id', 'sys_ord_no',
                     DB::raw('pick_remark as remark'), 'dlv_no', 'ord_no', 'status',
                     DB::raw("(select error_cd from mod_dlv_plan where dlv_type= 'P' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as error_cd"),
                     DB::raw("(select abnormal_remark from mod_dlv_plan where dlv_type= 'P' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as abnormal_remark"),
                     DB::raw("(select error_descp from mod_dlv_plan where dlv_type= 'P' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as error_remark")
                );
            }

            $main = $thisQuery->first();

            if(isset($main)) {
                $detail = DB::table('mod_order_detail')
                ->select(
                    'id','ord_no','goods_no','goods_nm','pkg_num','pkg_unit',
                    'gw','gwu','cbm','cbmu','length','weight','height','goods_no2',
                    'ord_id','sn_no','price')
                            ->where('ord_id', $main->id)
                            ->get();

                $fee = DB::table('mod_order_fee')
                ->select(
                    'id', 'ord_id', 'fee_cd', 'fee_name', 'amount',  
                    'fee_descp', 'fee_type_cd', 'fee_type_nm', 'prod_detail_id', 'fee_goods_nm' 
                )
                ->where('ord_id', $main->id)
                ->get();
            }
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage(), 'error_line' => $e->getLine()]);
        }

        $result = array(
            'msg'    => 'success',
            'main'   => $main,
            'detail' => $detail,
            'fee' => $fee
        );

        return response()->json($result);
    }

    public function missonfind($sysOrdNo) {
        \Log::info("missonfind");
        \Log::info($sysOrdNo);
        $user   = Auth::user();
        $main   = null;
        $detail = null;
        $newsys_ord_no = DB::table('mod_order')->where('ord_no', $sysOrdNo)->orWhere('sys_ord_no', $sysOrdNo)->pluck('sys_ord_no')->toArray();
        $new_ord_no = DB::table('mod_order')->where('car_no',$user->email)->whereIn('sys_ord_no', $newsys_ord_no)->pluck('sys_ord_no')->toArray();
        try {
            $thisQuery = DB::table('mod_dlv_plan')
                            ->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no')
                            ->select(
                            DB::raw("(CASE WHEN mod_dlv_plan.status in ('FINISHED','REJECT','ERROR') THEN 1 ELSE 0 END) AS tab_status"),
                            DB::raw("(select status from mod_dlv where dlv_no = mod_dlv_plan.dlv_no  limit 1 ) AS dlv_status"),
                            'mod_order.pick_attn', 'mod_order.pick_tel', 'mod_order.pick_cust_nm', 'mod_order.dlv_attn', 'mod_order.dlv_tel',
                             'mod_order.dlv_cust_nm',
                             'mod_dlv_plan.sys_ord_no', 'mod_dlv_plan.remark', 'mod_dlv_plan.addr', 'mod_dlv_plan.status', 'mod_dlv_plan.sort', 'mod_dlv_plan.dlv_type',
                             'mod_dlv_plan.ord_no', 'mod_dlv_plan.ord_id','mod_dlv_plan.error_descp','mod_dlv_plan.abnormal_remark', 'mod_dlv_plan.error_cd','mod_dlv_plan.dlv_no'
                             ,'mod_dlv_plan.created_at')
                            ->whereIn('mod_dlv_plan.sys_ord_no', $new_ord_no)
                            ->where('mod_dlv_plan.car_no', $user->email);
            $mainP = DB::table('mod_dlv_plan')
            ->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no')
            ->select(
            DB::raw("(CASE WHEN mod_dlv_plan.status in ('FINISHED','REJECT','ERROR') THEN 1 ELSE 0 END) AS tab_status"),
            DB::raw("(select status from mod_dlv where dlv_no = mod_dlv_plan.dlv_no  limit 1 ) AS dlv_status"),
            'mod_order.pick_attn', 'mod_order.pick_tel', 'mod_order.pick_cust_nm', 'mod_order.dlv_attn', 'mod_order.dlv_tel', 'mod_order.dlv_cust_nm',
            'mod_dlv_plan.sys_ord_no', 'mod_dlv_plan.remark', 'mod_dlv_plan.addr', 'mod_dlv_plan.status', 'mod_dlv_plan.sort', 'mod_dlv_plan.dlv_type',
            'mod_dlv_plan.ord_no', 'mod_dlv_plan.ord_id','mod_dlv_plan.error_descp','mod_dlv_plan.abnormal_remark', 'mod_dlv_plan.error_cd','mod_dlv_plan.dlv_no'
            ,'mod_dlv_plan.created_at'
            )
            ->whereIn('mod_dlv_plan.sys_ord_no', $new_ord_no)
            ->where('mod_dlv_plan.car_no', $user->email)
            ->where('mod_dlv_plan.dlv_type','P')->orderBy('created_at', 'desc')->skip(0)->take(1)->get()->toArray();

            $mainD = DB::table('mod_dlv_plan')
            ->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no')
            ->select(
            DB::raw("(CASE WHEN mod_dlv_plan.status in ('FINISHED','REJECT','ERROR') THEN 1 ELSE 0 END) AS tab_status"),
            DB::raw("(select status from mod_dlv where dlv_no = mod_dlv_plan.dlv_no  limit 1 ) AS dlv_status"),
            'mod_order.pick_attn', 'mod_order.pick_tel', 'mod_order.pick_cust_nm', 'mod_order.dlv_attn', 'mod_order.dlv_tel',
            'mod_order.dlv_cust_nm',
            'mod_dlv_plan.sys_ord_no', 'mod_dlv_plan.remark', 'mod_dlv_plan.addr', 'mod_dlv_plan.status', 'mod_dlv_plan.sort', 'mod_dlv_plan.dlv_type',
            'mod_dlv_plan.ord_no', 'mod_dlv_plan.ord_id','mod_dlv_plan.error_descp','mod_dlv_plan.abnormal_remark', 'mod_dlv_plan.error_cd','mod_dlv_plan.dlv_no'
            ,'mod_dlv_plan.created_at')
            ->whereIn('mod_dlv_plan.sys_ord_no', $new_ord_no)
            ->where('mod_dlv_plan.car_no', $user->email)
            ->where('mod_dlv_plan.dlv_type','D')->orderBy('created_at', 'desc')->skip(0)->take(1)->get()->toArray();

            $main = array_merge($mainP, $mainD);

            // $main = $thisQuery->get();
            $count = $thisQuery->count();
            if($count < 1) {
                return response()->json(['msg' => 'error', 'error_log' => "查無資料","test"=>$user->email]);
            }
        }
        catch(\Exception $e) {
            \Log::info("missonfind");
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }

        $result = array(
            'msg'    => 'success',
            'main'   => $main
        );

        return response()->json($result);
    }


    public function ordConfirm_NEW(Request $request) {
        \Log::info("ordConfirm_NEW");
        \Log::info($request->all());
        $user   = Auth::user();
        $gps = $request->gps;
        $Totalamount    = 0;
        $imgtype = "N";
        // $this->sendExtraData($request,$request->collectamt);
        $sysordno = $request->sysOrdNo != '' ? $request->sysOrdNo : $request->sys_ord_no;
        $checkdlvorder=DB::table('mod_order')
        ->where('sys_ord_no', $sysordno)
        ->where('car_no', $user->email)
        ->count();
        if($checkdlvorder<=0){
            return response()->json(['msg' => 'success','code'=>'00']);
        }
        $extradata = array();
        if(isset($request->extradata)) {
            $extradata = $request->extradata;
        }
        try {
            foreach($request->sninfo as $row2) {
                $newsn =  str_replace( array("\n", "\r"),",",$row2["snNo"]);
                DB::table('mod_order_detail')
                ->where('id',$row2["id"])
                ->update([
                    'sn_no' => $newsn,
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    'updated_by' => $user->email
                ]);
                $user = Auth::user();
                try{
                    DB::table('mod_sn_history')->insert([
                        'ord_id' 		=> $row2["id"],
                        'snno' 			=> $newsn,
                        'created_by' 	=> $user->email,
                        'updated_by' 	=> $user->email,
                        'created_at' 	=> \Carbon::now(),
                        'updated_at' 	=> \Carbon::now(),
                    ]);
                }catch(\Exception $e) {
                    \Log::error($e->getLine());
                    \Log::error($e->getMessage());
                } 
            }
        }
        catch(\Exception $e) {
            \Log::info("ordconfrimnew error");
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'error_log' => $e->getLine(),'code'=>'01']);
        }
        $insertData = array();
        $now = date("Y-m-d H:i:s");

        try {
            $cnt = count($extradata);
			\Log::info("extradata del start");
            $orddata = DB::connection('mysql::write')
            ->table('mod_order')
            ->where('sys_ord_no', $request->sysOrdNo)
            ->first();
            \Log::info($orddata->id);
            DB::table('mod_order_fee')
            ->where('ord_id',  $orddata->id)
            ->delete();
            \Log::info("extradata del end");
            for($i=0; $i<$cnt; $i++) {
                $sysordno       = $extradata[$i]['sys_ord_no'];
                $extra_cd       = $extradata[$i]['extra_cd'];
                $extra_nm       = $extradata[$i]['extra_nm'];
                $extra_descp    = $extradata[$i]['extra_descp'];
                $amount         = 0;
                $custfeedata    = array();


                if(isset($extra_cd)){
                    $custdata = DB::table('sys_customers')
                    ->where('cust_no', $orddata->owner_cd)
                    ->where('g_key', $user->g_key)
                    ->first(); 
                    if(isset($custdata)) {
                        $custfeedata = DB::table('mod_cust_fee')
                        ->where('fee_cd', $extra_cd)
                        ->where('cust_id', $custdata->id)
                        ->where('g_key', $user->g_key)
                        ->first();
                        if(isset($custfeedata)){
                            $amount = $custfeedata->fee_amount;
                            $Totalamount = $Totalamount + $custfeedata->fee_amount;
                        }else{
                            $custfeedata = DB::table('bscode')
                            ->where('cd', $extra_cd)
                            ->where('cd_type', "EXTRAFEE")
                            ->where('g_key', $user->g_key)
                            ->first(); 
                            $amount = $custfeedata->value1;
                            $Totalamount = $Totalamount + $custfeedata->value1;
                        }
                    } else {
                        $custfeedata = DB::table('bscode')
                        ->where('cd', $extra_cd)
                        ->where('cd_type', "EXTRAFEE")
                        ->where('g_key', $user->g_key)
                        ->first(); 
                        $amount = $custfeedata->value1;
                        $Totalamount = $Totalamount + $custfeedata->value1;
                    }

                }
                \Log::info("collectamt update start");
                if(isset($orddata)){
                    \Log::info("content update start");
                    \Log::info($request->collectamt);
                    DB::table('mod_order')
                    ->where('sys_ord_no', $sysordno)
                    ->update(['collectamt' => $request->collectamt,'total_amount' => $Totalamount]);
                }
                \Log::info("collectamt update end");
                $proddetailid  = null;
                $feegoodsnm  = null;
                $detail =  DB::table('mod_order_detail')
                ->where('ord_id', $orddata->id)
                ->get();
                if(count($detail)==1){
                    if($detail[0]->pkg_num==1){
                        $proddetailid = $detail[0]->id."-1";
                        $feegoodsnm =$detail[0]->goods_no."(".$detail[0]->goods_nm.")"."-1";
                    }
                }
                // dd($proddetailid);
                $data = [
                    'ord_id'        => $orddata->id,
                    'fee_cd'        => $extra_cd,
                    'fee_name'      => $extra_nm,
                    'amount'      => $amount,
                    'fee_descp'      => $extra_descp,
                    'g_key'         => $user->g_key,
                    'c_key'         => $user->c_key,
                    's_key'         => $user->s_key,
                    'd_key'         => $user->d_key,
                    'created_by'    => $user->email,
                    'updated_by'    => $user->email,
                    'prod_detail_id'=> $proddetailid,
                    'fee_goods_nm'  => $feegoodsnm,
                    'created_at'    => Carbon::now()->toDateTimeString(),
			        'updated_at'    => Carbon::now()->toDateTimeString(),
                ];
                array_push($insertData, $data);
            }
            $OrderamtDetailModel = new OrderamtDetailModel();
            $OrderamtDetailModel->insertfee($insertData);   
        }
        catch(\Exception $e) {
            \Log::info("ordconfrimnew error");
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(["msg"=>"error","data"=>$e->getLine(),'code'=>'00']);
        }
        $sysOrdNo = $request->sysOrdNo!= '' ? $request->sysOrdNo : $request->sys_ord_no;
        // $dlvNo    = $request->dlv_no;
        $img      = $request->img;
        $signImg  = $request->signImg;
        $lng      = $request->lng;
        $lat      = $request->lat;
        $dlvType  = $request->dlvType != '' ? $request->dlvType : $request->dlv_type;
        $insertData = array();
        $dlvinfo    = DB::table('mod_order')
        ->where('sys_ord_no', $sysOrdNo)
        ->first();
        $dlvNo = $dlvinfo->dlv_no;
        $dlvData = DB::table('mod_dlv')
                            ->select('status')
                            ->where('dlv_no', $dlvNo)
                            ->first();
        if($request->tab_status == 0){
            // if(isset($dlvData) && $dlvData->status != "DLV") {
                // return response()->json(['msg' => 'error', 'msgLog' => '還沒出發不能完成，請先按下出發']);
            // }

            $ordStatus = 'FINISHED';

            if($dlvType == 'D') {

                $dlvStatus = DB::table('mod_dlv_plan')
                                ->select('status')
                                ->where('sys_ord_no', $sysOrdNo)
                                ->where('dlv_type', 'P')
                                ->where('dlv_no', $dlvNo)
                                ->first();
                if(isset($dlvStatus)) {
                    if($dlvStatus->status != 'FINISHED') {
                        return response()->json(['msg' => 'error', 'msgLog' => '尚有提貨未完成，請先完成提貨','code'=>'01']);
                    }
                }
            }
            else {
                $ordStatus = 'PICKED';
                $orderforcar =  DB::table('mod_order')
                ->where('sys_ord_no', $sysOrdNo)
                ->first();
            }
        }
        
        try {
            \Log::info("img start");
            \Log::info("imgnum start");
            DB::table('mod_order')
            ->where('sys_ord_no', $sysOrdNo)
            ->update(['imgnum' =>  $request->imgnum]);
            \Log::info("imgnum end");
            if(is_array($img)) {
                \Log::info("real FINISH img start");
                for($i=0; $i<count($img); $i++) {
                    $guid = $this->guid();
                    Storage::disk('local')->put($guid.'.jpg', base64_decode($img[$i]));
                    $checkcount = DB::table('mod_file')
                    ->where('ref_no', $ordData->sys_ord_no)
                    ->where('guid', $guid.'.jpg')
                    ->where('type_no', 'FINISH')
                    ->count();
                    if($checkcount<1){
                        array_push($insertData, [
                            'guid'        => $guid.'.jpg',
                            'ref_no'      => $sysOrdNo,
                            'type_no'     => 'FINISH',
                            'file_format' => 'jpg',
                            'g_key'       => $user->g_key,
                            'c_key'       => $user->c_key,
                            's_key'       => $user->s_key,
                            'd_key'       => $user->d_key,
                            'created_by'  => $user->email,
                            'updated_by'  => $user->email,
                            'created_at'  => Carbon::now()->toDateTimeString(),
                            'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                    }
                }
            }
    
            if(isset($signImg)) {
                \Log::info("real sign img start");
                $guid = $this->guid();
                Storage::disk('local')->put($guid.'.jpg', base64_decode($signImg));
                $imgsize = $this->getBase64ImageSize($img);
                \Log::info('sign'.$imgsize);
                if($imgsize>=2500){
                    $imgtype = "Y";
                }
                $checkcount = DB::table('mod_file')
                ->where('ref_no', $ordData->sys_ord_no)
                ->where('guid', $guid.'.jpg')
                ->where('type_no', 'SIGNIN')
                ->count();
                if($checkcount<1){
                    array_push($insertData, [
                        'guid'        => $guid.'.jpg',
                        'ref_no'      => $sysOrdNo,
                        'type_no'     => 'SIGNIN',
                        'file_format' => 'jpg',
                        'empty_img'   => 'N',
                        'g_key'       => $user->g_key,
                        'c_key'       => $user->c_key,
                        's_key'       => $user->s_key,
                        'd_key'       => $user->d_key,
                        'created_by'  => $user->email,
                        'updated_by'  => $user->email,
                        'created_at'  => Carbon::now()->toDateTimeString(),
                        'updated_at'  => Carbon::now()->toDateTimeString()
                    ]);
                }else{
                    DB::table('mod_file')
                    ->where('ref_no', $ordData->sys_ord_no)
                    ->where('guid', $guid.'.jpg')
                    ->where('type_no', 'SIGNIN')
                    ->update([
                        'empty_img'   => $imgtype,
                        'updated_by'  => $user->email,
                        'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                }
            }
            
            $signPic = 0;
            \Log::info($insertData);
            if(count($insertData) > 0) {
                $signPic = 1;
                DB::table('mod_file')->insert($insertData);

                // DB::table('mod_order')
                //     ->where('sys_ord_no', $sysOrdNo)
                //     ->update(['sign_pic' => $signPic]);
            }
            \Log::info("end");
            if($request->tab_status >= 1){
                \Log::info("repair end");
                return response()->json(["msg"=>"repair success"]);
            }
            $ordModel = new OrderMgmtModel;

            if($dlvType == 'D') {
                $ordModel->updateOrdStatusAndFinishDate($sysOrdNo, $ordStatus);
            }

            $orderdata = DB::table('mod_order')
            ->where('sys_ord_no',$sysOrdNo)
            ->first();
            DB::table('mod_order')
                ->where('sys_ord_no', $sysOrdNo)
                ->update(['status' => $ordStatus]);

            DB::table('mod_dlv_plan')
                ->where('sys_ord_no', $sysOrdNo)
                ->where('dlv_type', $dlvType)
                ->where('dlv_no', $dlvNo)
                ->update(['status' => 'FINISHED', 'updated_at' => Carbon::now()->toDateTimeString(), 'finish_date' => Carbon::now()->toDateTimeString()]);

            $cnt  = DB::connection('mysql::write')->table('mod_dlv_plan')->where('dlv_no', $dlvNo)->count();
            $fCnt = DB::connection('mysql::write')->table('mod_dlv_plan')->where('dlv_no', $dlvNo)->whereRaw("(status = 'FINISHED' or status = 'ERROR' or status='REJECT')")->count();
            if($cnt == $fCnt) {
                DB::table('mod_dlv')
                    ->where('dlv_no', $dlvNo)
                    ->update(['status' => 'FINISHED','finished_time' => Carbon::now()->toDateTimeString()]);
            }            

            $dlvPlanData = DB::connection('mysql::write')->table('mod_dlv_plan')
                            ->where('sys_ord_no', $sysOrdNo)
                            ->where('dlv_type', $dlvType)
                            ->where('dlv_no', $dlvNo)
                            ->first();

            $dlvData = DB::connection('mysql::write')->table('mod_dlv')
                            ->where('dlv_no', $dlvNo)
                            ->where('g_key', $user->g_key)
                            ->where('c_key', $user->c_key)
                            ->first();

            $addr = null;
            $custNm = null;
            $driverNm = $user->email;

            if(isset($dlvData)) {
                $addr = $dlvPlanData->addr;
                $custNm = $dlvPlanData->cust_nm;
            }

            if(isset($dlvData)) {
                $driverNm = $dlvData->driver_nm;
            }  
            $insertData = array();
            $TrackingModel = new TrackingModel();
            //$ModTrackingModel = new ModTrackingModel();
            \Log::info("gps start");
            if($dlvType == 'D') {
                $tm = new TrackingModel();
                $tm->insertTracking('', '', '', $sysOrdNo, '', 'E', 5, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
            }
                $gps = $request->gps;
                \Log::info($gps);
                $lng       = $gps['lng'];
                $lat       = $gps['lat'];
                $speed     = $gps['speed'];
                $timestamp = $gps['timestamp'];

                $data = [
                    'car_no'     => $user->email,
                    'lat'        => $lat,
                    'lng'        => $lng,
                    'driver_no'  => null,
                    'driver_nm'  => $driverNm,
                    'person_no'  => null,
                    'person_nm'  => null,
                    'dlv_no'     => null,
                    'g_key'      => $user->g_key,
                    'c_key'      => $user->c_key,
                    's_key'      => $user->s_key,
                    'd_key'      => $user->d_key,
                    'created_by' => $user->email,
                    'updated_by' => $user->email,
                    'location_time' => $timestamp,
                    'speed'      => $speed,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    'status'     => 'FINISHED',
                    'cust_nm'    => $custNm,
                    'addr'       => $addr,
                ];

                array_push($insertData, $data);
    
                $TrackingModel->insertGps($insertData);
               // $ModTrackingModel->insertGps($insertData);
                $userData = DB::table('users')
                ->where('id', $user->id)
                ->update(['gps_time' => Carbon::now()->toDateTimeString()]);
            \Log::info("gps end");
            $TrackingModel->sendDlvMail($sysOrdNo, $dlvType); //先發配送完成，直到取貨mail完成
			\Log::info("confirm_new  finished");
			$ordsendbnqdata = DB::table('mod_order')
            ->where('sys_ord_no',$sysOrdNo)
            ->first();
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            \Log::info($request->all());
            return response()->json(['msg' => 'success', 'error_log' => $e->getMessage(),'code'=>'01']);
        }

        return response()->json(['msg' => 'success','code'=>'00']);
    }


    public function checkversion(Request $request) {
        // phone_type
        \Log::info("checkversion");
        \Log::info($request->all());
        $user       = Auth::user();
        if($request->phone_type=="android"){
            $appver = DB::table('bscode')->where('cd_type','appversion')->where('cd','appversion')->first();
            if($request->appversion != $appver->value1 && $request->appversion != $appver->value2 ){
                return response()->json(['msg' => '版本不相符 請更新app版本', "url"=>$appver->cd_descp,"errorcd"=>"-1"]);
            }else{
                return response()->json(['msg' => '當前已是最新版本',"errorcd"=>"0"]);
            }
        } else {
            $appver = DB::table('bscode')->where('cd_type','appversion')->where('cd','iosversion')->first();
            if($request->appversion != $appver->value1 && $request->appversion != $appver->value2 ){
                return response()->json(['msg' => '版本不相符 請更新app版本', "url"=>$appver->cd_descp,"errorcd"=>"-1"]);
            }else{
                return response()->json(['msg' => '當前已是最新版本',"errorcd"=>"0"]);
            }
        }
    }


    public function errorReport_JSON(Request $request) {
        \Log::info("errorReport_JSON");
        \Log::info($request->all());
        ////
        $user           = Auth::user();
        $sysOrdNo       = $request->sysOrdNo!= '' ? $request->sysOrdNo : $request->sys_ord_no;
        $dlvNo          = $request->dlvNo;
        $errorType      = $request->errorType;
        $dlvType        = $request->dlvType;
        $gps            = $request->gps;
        $abnormalRemark = $request->abnormalRemark;
        $name           = $request->name;
        $phone          = $request->phone; 
        $insertData     = array();
        $checkdlvorder  = DB::table('mod_order')
        ->where('sys_ord_no', $sysOrdNo)
        ->where('dlv_no', $dlvNo)
        ->count();
        if($checkdlvorder<=0){
            return response()->json(['msg' => 'success','code'=>'00']);
        }
        $dlvData = DB::table('mod_dlv')
                            ->select('status')
                            ->where('dlv_no', $dlvNo)
                            ->first();
        if($request->tab_status == 0){
            if(isset($dlvData) && $dlvData->status != "DLV") {
                return response()->json(['msg' => 'error', 'msgLog' => '還沒出發不能回報異常，請先按下出發','code'=>'01']);
            }
    
            if($dlvType == 'D') {
    
                $dlvStatus = DB::table('mod_dlv_plan')
                                ->select('status')
                                ->where('sys_ord_no', $sysOrdNo)
                                ->where('dlv_type', 'P')
                                ->where('dlv_no', $dlvNo)
                                ->first();
                if(isset($dlvStatus)) {
                    if($dlvStatus->status != 'FINISHED' && $dlvStatus->status != 'ERROR') {
                        return response()->json(['msg' => 'error', 'msgLog' => '尚有提貨未完成，請先完成提貨','code'=>'01']);
                    }
                }
            }

        }
        
        try {
            $errorPic = 0;
            if(count($insertData) > 0) {
                $errorPic = 1;
                DB::table('mod_file')->insert($insertData);

                DB::table('mod_order')
                    ->where('sys_ord_no', $sysOrdNo)
                    ->update(['error_pic' => $errorPic]);
            }
            $errorStatus = "ERROR";
            $errordesc = "配送發生問題";
            if($errorType=="A999"||$errorType=="A093"){
                $errorStatus ="REJECT";
                $errordesc = "拒收";
            }
            $bscode = array();
            $custowner = DB::table('mod_order')
            ->where('sys_ord_no', $sysOrdNo)
            ->first();
            $owner_cd =  DB::table('sys_customers')
            ->where('cust_no', $custowner->owner_cd)
            ->where('g_key', $user->g_key)
            ->first();
            if(!isset($owner_cd)){
                $thisQuery = DB::table('bscode');
                $thisQuery->where('cd_type', "ERRORTYPE");
                $thisQuery->where('cd', $errorType);
                $thisQuery->where('g_key', $user->g_key);
                $thisQuery->where('c_key', $user->c_key);
                $bscode = $thisQuery->first();
            }else{
                $bscode= DB::table('mod_cust_error')
                ->where('cust_id', $owner_cd->id)
                ->first();
                if(!isset($bscode)){
                    $thisQuery = DB::table('bscode');
                    $thisQuery->where('cd_type', "ERRORTYPE");
                    $thisQuery->where('cd', $errorType);
                    $thisQuery->where('g_key', $user->g_key);
                    $thisQuery->where('c_key', $user->c_key);
                    $bscode = $thisQuery->first();
                }else{
                    $errorStatus =  $bscode->order_status;
                    if( $errorStatus=="REJECT"){
                        $errordesc = "拒收";
                    }else if($errorStatus=="FINISHED") {
                        $errordesc = "配送完成";
                    }else{
                        $errordesc = "配送發生問題";
                    }
                }
            }

            $cdDescp = "";;
            if(isset($bscode)) {
                $cdDescp = $bscode->cd_descp;
            }
            if($request->tab_status >= 1){
                DB::table('mod_order')
                ->where('sys_ord_no', $sysOrdNo)
                ->update(['error_remark' => $cdDescp, 'exp_reason' => $errorType, 'abnormal_remark' => $abnormalRemark]);
                if($dlvType == 'P') {
                    DB::table('mod_dlv_plan')
                        ->where('sys_ord_no', $sysOrdNo)
                        ->where('dlv_no', $dlvNo)
                        ->update(['error_cd' => $errorType,'error_descp' => $cdDescp, 'abnormal_remark' => $abnormalRemark]);
                }
                else {
                    DB::table('mod_dlv_plan')
                        ->where('sys_ord_no', $sysOrdNo)
                        ->where('dlv_no', $dlvNo)
                        ->where('dlv_type', $dlvType)
                        ->update(['error_cd' => $errorType,'error_descp' => $cdDescp, 'abnormal_remark' => $abnormalRemark]);
                }
                return response()->json(['msg' => 'repair success','code'=>'00']);
            }
            if( ($errorType== "A001" || $errorType== "A072") && $user->g_key=="SYL"){
                $pickCnt = DlvPlanModel::where('sys_ord_no', $sysOrdNo)
                ->where('dlv_type', 'P')
                ->count();
                //先判斷 是提貨還是配送 如果是提貨 刷成尚未安排 如果是配送則判斷目前總攬裡面是否有提貨資訊 有的話 刷成已提貨 沒有就刷成尚未安排
                if($dlvType == 'P') {
                    DB::table('mod_order')
                    ->where('sys_ord_no', $sysOrdNo)
                    ->update(['status' => 'UNTREATED', 'status_desc' => '尚未安排', 'dlv_no' => null, 'error_remark' => $cdDescp, 'exp_reason' => $errorType, 'dlv_type' => null, 'abnormal_remark' => $abnormalRemark]);
                }else{
                    if($pickCnt > 0) {
                        DB::table('mod_order')
                        ->where('sys_ord_no', $sysOrdNo)
                        ->update(['status' => 'PICKED', 'status_desc' => '已提貨', 'dlv_no' => null, 'error_remark' => $cdDescp, 'exp_reason' => $errorType, 'dlv_type' => 'P', 'abnormal_remark' => $abnormalRemark]);
                        $orderdata = DB::table('mod_order')
                        ->where('sys_ord_no',$sysOrdNo)
                        ->first();
                    }else{
                        DB::table('mod_order')
                        ->where('sys_ord_no', $sysOrdNo)
                        ->update(['status' => 'UNTREATED', 'status_desc' => '尚未安排', 'dlv_no' => null, 'error_remark' => $cdDescp, 'exp_reason' => $errorType, 'dlv_type' => null, 'abnormal_remark' => $abnormalRemark]);
                    }
                }
            }else{
                DB::table('mod_order')
                ->where('sys_ord_no', $sysOrdNo)
                ->update(['status' => $errorStatus, 'status_desc' => $errordesc, 'error_remark' => $cdDescp, 'exp_reason' => $errorType, 'dlv_type' => null, 'abnormal_remark' => $abnormalRemark]);
            }
            if($dlvType == 'P') {
                DB::table('mod_dlv_plan')
                    ->where('sys_ord_no', $sysOrdNo)
                    ->where('dlv_no', $dlvNo)
                    ->update([
                        'user_name'   => $name,
                        'user_phone'  => $phone,
                        'status' => $errorStatus,
                        'error_cd' => $errorType,
                        'error_descp' => $cdDescp,
                        'abnormal_remark' => $abnormalRemark, 
                        'updated_at' => Carbon::now()->toDateTimeString(), 
                        'finish_date' => Carbon::now()->toDateTimeString()
                    ]);
            }
            else {
                DB::table('mod_dlv_plan')
                    ->where('sys_ord_no', $sysOrdNo)
                    ->where('dlv_no', $dlvNo)
                    ->where('dlv_type', $dlvType)
                    ->update([
                        'user_name'   => $name,
                        'user_phone'  => $phone,
                        'status' => $errorStatus,
                        'error_cd' => $errorType,
                        'error_descp' => $cdDescp,
                        'abnormal_remark' => $abnormalRemark,
                        'updated_at' => Carbon::now()->toDateTimeString(),
                        'finish_date' => Carbon::now()->toDateTimeString()
                    ]);
            }
            $dlvData = DB::table('mod_dlv')
            ->select('status','driver_nm')
            ->where('dlv_no', $dlvNo)
            ->first();
            $dlvPlanData = DB::table('mod_dlv_plan')
            ->where('sys_ord_no', $sysOrdNo)
            ->where('dlv_type', $dlvType)
            ->where('dlv_no', $dlvNo)
            ->first();
            $addr = null;
            $custNm = null;
            $driverNm = $user->email;
            if(isset($dlvData)) {
                $addr = $dlvPlanData->addr;
                $custNm = $dlvPlanData->cust_nm;
            }

            if(isset($dlvData)) {
                $driverNm = $dlvData->driver_nm;
            } 
            $insertData = array();
            $TrackingModel = new TrackingModel();
            //$ModTrackingModel = new ModTrackingModel();
            // $tm = new TrackingModel();
            // $tm->insertTracking('', '', '', $sysOrdNo, '', 'E', 5, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
            $cnt = count($gps);
            $gps = $request->gps;
            \Log::info($gps);
            $lng       = $gps['lng'];
            $lat       = $gps['lat'];
            $speed     = $gps['speed'];
            $timestamp = $gps['timestamp'];

            $data = [
                'car_no'     => $user->email,
                'lat'        => $lat,
                'lng'        => $lng,
                'driver_no'  => null,
                'driver_nm'  => $driverNm,
                'person_no'  => null,
                'person_nm'  => null,
                'dlv_no'     => null,
                'g_key'      => $user->g_key,
                'c_key'      => $user->c_key,
                's_key'      => $user->s_key,
                'd_key'      => $user->d_key,
                'created_by' => $user->email,
                'updated_by' => $user->email,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
                'location_time' => $timestamp,
                'speed'      => $speed,
                'status'     => 'FINISHED',
                'cust_nm'    => $custNm,
                'addr'       => $addr,
            ];

            array_push($insertData, $data);

            $TrackingModel->insertGps($insertData);
			if($errorStatus=="REJECT"){
				$TrackingModel->insertTracking('', '', '', $sysOrdNo, '', 'E', 5, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
			}

            $userData = DB::table('users')
            ->where('id', $user->id)
            ->update(['gps_time' => Carbon::now()->toDateTimeString()]);
            $ordModel = new OrderMgmtModel;
            $orderdata = DB::table('mod_order')
            ->where('sys_ord_no',$sysOrdNo)
            ->first();
            if(isset($orderdata)) {
                $ordall = DB::table('mod_order')
                ->where('sys_ord_no',$sysOrdNo)
                ->first();
                $tsrecord = DB::table('mod_ts_order')
                ->where('sys_ord_no', $sysOrdNo)
                ->where('status_desc', $ordall->status_desc)
                ->where('owner_cd', $ordall->owner_cd)
                ->count();
                $custdata = DB::table('sys_customers')
                ->where('cust_no', $ordall->owner_cd)
                ->whereIn('ftptype',["E","EM"])
                ->count();
                if($custdata!=0 && $errorStatus!="REJECT"){
                    DB::table('mod_ts_order')
                    ->insert([
                        "owner_cd" => $ordall->owner_cd,
                        "owner_nm" => $ordall->owner_nm,
                        "ord_no"   => $ordall->ord_no,
                        "cust_ord_no"   => $ordall->cust_ord_no, 
                        "wms_ord_no"   => $ordall->wms_order_no,
                        "sys_ord_no" => $ordall->sys_ord_no,
                        "ord_created_at"   => $ordall->created_at,
                        "status_desc" => "配送發生問題",
                        "finish_date"   => Carbon::now(),
                        "exp_reason" => $errorType,
                        "error_remark" => $cdDescp,
                        "status" => $ordall->status,
                        "abnormal_remark" => $abnormalRemark,
                        "is_send"     => "N",
                        "updated_at"  => Carbon::now(),
                        "updated_by"  => "SYSTEM",
                        "created_at"  => Carbon::now(),
                        "created_by"  => "SYSTEM",
                        'g_key'      => $user->g_key,
                        'c_key'      => $user->c_key,
                        's_key'      => $user->s_key,
                        'd_key'      => $user->d_key,
                    ]);
                }
            }
            if($orderdata->g_key=="SYL" && $errorStatus=="REJECT"){
                $ordModel->updateOrdStatusAndFinishDate($sysOrdNo, $errorStatus);
            }
            $cnt  = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->count();
            $fCnt = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->whereRaw("(status = 'FINISHED' or status = 'ERROR' or status = 'REJECT')")->count();

            if($cnt == $fCnt) {
                DB::table('mod_dlv')
                    ->where('dlv_no', $dlvNo)
                    ->update(['status' => 'FINISHED','finished_time' => Carbon::now()->toDateTimeString()]);
            }
            $ordsendbnqdata = DB::table('mod_order')
            ->where('sys_ord_no',$sysOrdNo)
            ->first();
			\Log::info("sure finised");
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            \Log::info($request->all());
            return response()->json(['msg' => 'error', 'error_log' => $e->getLine(), 'error_msg' => $e->getMessage(),'code'=>'01']);
        }

        return response()->json(['msg' => 'success','code'=>'00']);
    }


    public function departurenew($dlvNo) {
        try {
            \Log::info("departurenew");
            \Log::info($dlvNo);
            $user = Auth::user();
            $lng = request('lng');
            $lat = request('lat');
            $dlvNo      = substr($dlvNo,0,13);
            DB::table('mod_order')
                ->where('dlv_no', $dlvNo)
                ->update(['status' => 'SETOFF','status_desc' => '出發', 'updated_at' => Carbon::now()->toDateTimeString()]);
            $ordData = DB::table('mod_dlv_plan')->select('sys_ord_no')
            ->where('status',"!=", "ERROR")
            ->where('status',"!=", "FINISHED")
            ->where('status',"!=", "REJECT")
            ->where('dlv_no', $dlvNo)
            ->get();
            //確認出發時派車單內是否還有任務
            //有的話運送中 沒有的直接完成
            if(count($ordData) > 0) {
                DB::table('mod_dlv')
                ->where('dlv_no', $dlvNo)
                ->update(['status' => 'DLV', 'updated_at' => Carbon::now()->toDateTimeString(), 'updated_by' => $user->email, 'departure_time' => Carbon::now()->toDateTimeString()]);
            }else{
                DB::table('mod_dlv')
                ->where('dlv_no', $dlvNo)
                ->update(['status' => 'FINISHED', 'updated_at' => Carbon::now()->toDateTimeString(),  'updated_by' => $user->email, 'departure_time' => Carbon::now()->toDateTimeString()]);
            }
            $checkorddata = DB::table('mod_order')
            ->where('dlv_no', $dlvNo)
            ->get();
            try{
                foreach($checkorddata as $ordrow) {
                    if($ordrow->is_ordered=="Y"){
                        DB::table('mod_dlv_plan')
                        ->where('sys_ord_no', $ordrow->sys_ord_no)
                        ->where('dlv_type', 'P')
                        ->update(['status' => 'FINISHED' , 'updated_by' => $user->email , 'updated_at' => Carbon::now()->toDateTimeString()]);
                    }
                }
            }
            catch(\Exception $e) {
                \Log::info($e->getLine());
                \Log::info($e->getMessage());
                return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
            }
            //派車單任務狀態 出發 
            DB::table('mod_dlv_plan')
                ->where('dlv_no', $dlvNo)
                ->where('c_key', $user->c_key)
                ->where('status',"!=", "ERROR")
                ->where('status',"!=", "FINISHED")
                ->where('status',"!=", "REJECT")
                ->update(['status' => 'DLV', 'updated_by' => $user->email , 'updated_at' => Carbon::now()->toDateTimeString()]);
            if(count($ordData) > 0) {
                $issend = "N";
                foreach($ordData as $row) {
                    $sysOrdNo = $row->sys_ord_no;
                    $ordModel = new OrderMgmtModel;
                    $orderdata = DB::table('mod_order')->where('sys_ord_no',$sysOrdNo)->first();
                    if($orderdata->dlv_no==$dlvNo){

                        DB::table('mod_order')
                            ->where('sys_ord_no', $sysOrdNo)
                            ->where('status',"!=", "ERROR")
                            ->where('status',"!=", "FINISHED")
                            ->where('status',"!=", "REJECT")
                            ->update(['status' => 'SETOFF','status_desc' => '出發', 'updated_by' => $user->email , 'updated_at' => Carbon::now()->toDateTimeString()]);
                        if($orderdata->is_ordered=="Y")
                        {
                            $ordModel = new OrderMgmtModel;
                            $ordModel->updateOrdStatusAndFinishDate($sysOrdNo, 'PICKED');
                            DB::table('mod_dlv_plan')
                            ->where('sys_ord_no', $sysOrdNo)
                            ->where('dlv_no', $dlvNo)
                            ->where('dlv_type','P')
                            ->where('status',"!=", "ERROR")
                            ->where('status',"!=", "FINISHED")
                            ->where('status',"!=", "REJECT")
                            ->update(['status' => 'FINISHED' , 'updated_by' => $user->email , 'updated_at' => Carbon::now()->toDateTimeString() ,'finish_date'=>Carbon::now()->toDateTimeString()]);
                        }
                        $ordNo = DB::table('mod_order')->select('ord_no')->where('sys_ord_no', $sysOrdNo)->first();
                        $orddata = DB::table('mod_order')->select('id')->where('sys_ord_no', $sysOrdNo)->first();
                        $tm = new TrackingModel();
                        $tm->insertTracking('', $ordNo->ord_no, '', $sysOrdNo, '', 'B', 4, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
                    }
                }
            }


            $insertData = [
                'car_no'        => $user->email,
                'lat'           => $lat,
                'lng'           => $lng,
                'driver_no'     => null,
                'driver_nm'     => $user->name,
                'person_no'     => null,
                'person_nm'     => null,
                'dlv_no'        => null,
                'g_key'         => $user->g_key,
                'c_key'         => $user->c_key,
                's_key'         => $user->s_key,
                'd_key'         => $user->d_key,
                'created_by'    => $user->email,
                'updated_by'    => $user->email,
                'location_time' => Carbon::now()->toDateTimeString(),
                'speed'         => '0',
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(),
                'status'        => 'SETOFF'
            ];

            $TrackingModel = new TrackingModel();
            $TrackingModel->insertGps($insertData);
            $userData = DB::table('users')
            ->where('id', $user->id)
            ->update(['gps_time' => Carbon::now()->toDateTimeString()]);
        }
        catch(\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }
        DB::table('mod_order')
        ->where('dlv_no', $dlvNo)
        ->update(['status' => 'SETOFF','status_desc' => '出發', 'updated_at' => Carbon::now()->toDateTimeString()]);
        return response()->json(['msg' => 'success']);
    }

    public function getorddata(Request $request) {
        \Log::info("getorddata");
        \Log::info($request->all());
        $user = Auth::user();
        $returndata = array();
        try { 
            if(isset($request->pickeddata)) {
                $orderdata = $request->pickeddata;
                $cnt = count($orderdata);
                for($i=0; $i<$cnt; $i++) {
                    $realnum       = $orderdata[$i]['realnum'];
                    $orderednum    = $orderdata[$i]['orderednum'];
                    $sys_ord_no    = $orderdata[$i]['sys_ord_no'];
                    if($realnum==$orderednum){
                        DB::table('mod_order')
                        ->where('sys_ord_no', $sys_ord_no)
                        ->update(['is_ordered' => 'Y','realnum' => $realnum]);
                    }
                    else{
                        DB::table('mod_order')
                        ->where('sys_ord_no', $sys_ord_no)
                        ->update(['is_ordered' => 'Y','realnum' => $realnum,'pick_remark' =>'數量不符']);
                    }
                }
            }          
        }
        catch(\Exception $e) {
            return response()->json(["msg"=>"error","data"=>$e->getMessage()]);
        }

        return response()->json(["msg"=>"success"]);
    }

    public function sendpickorder(Request $request) {
        \Log::info("sendpickorder");
        \Log::info($request->all());
        $user = Auth::user();
        \Log::info("點貨");
        $now = date("Y-m-d H:i:s");
        $orderdata = array();
        $order_tally = "";
        $insertData = array();
        try {
            if(isset($request->pickeddata)) {
                \Log::info($request->pickeddata);
                $orderdata = $request->pickeddata;
                $cnt = count($orderdata);
                for($i=0; $i<$cnt; $i++) {
                    $realnum       = isset($orderdata[$i]['real_num']) ?  $orderdata[$i]['real_num'] : $orderdata[$i]['realnum'];
                    $orderednum    = isset($orderdata[$i]['ord_num']) ?  $orderdata[$i]['ord_num'] : $orderdata[$i]['orderednum'];
                    $sys_ord_no    = $orderdata[$i]['sys_ord_no'];
                    $orddata       = DB::table('mod_order')->where('sys_ord_no', $sys_ord_no)->where('car_no', $user->email)->orderBy('created_at', 'desc')->first();
                    if(!isset($orddata)) {
                        $orddata   = DB::table('mod_order')->where('ord_no', $sys_ord_no)->where('car_no', $user->email)->orderBy('created_at', 'desc')->first();
                    }
                    $sys_ord_no = $orddata->sys_ord_no;
                    \Log::info($orddata->sys_ord_no);
                    $dlvdata       = DB::table('mod_dlv')->where('dlv_no', $orddata->dlv_no)->orderBy('created_at', 'desc')->first();
                    $dlvplandata       = DB::table('mod_dlv_plan')->where('dlv_no', $orddata->dlv_no)->where('dlv_type', 'P')->where('sys_ord_no', $sys_ord_no)->orderBy('created_at', 'desc')->first();
                    if($realnum==$orderednum){
                        $order_tally = "Y";
                        DB::table('mod_order')
                        ->where('sys_ord_no', $sys_ord_no)
                        ->update(['is_ordered' => 'Y','realnum' => $realnum,'order_tally' =>'Y']);
                    }
                    else{
                        $order_tally = "N";
                        if($orderdata[$i]['singlepost']=="Y"){
                            //
                            DB::table('mod_order')
                            ->where('sys_ord_no', $sys_ord_no)
                            ->update(['is_ordered' => 'Y','realnum' => $realnum,'order_tally' =>'N' ]);
                        }else{
                            //數量相同才要寫
                            DB::table('mod_order')
                            ->where('sys_ord_no', $sys_ord_no)
                            ->update(['realnum' => $realnum,'order_tally' =>'N' ]);
                        }
                    }
                    if(isset($dlvdata)){
                        if($dlvdata->status == "DLV") {
                            $pickdlvcount = DB::table('mod_dlv_plan')->where('dlv_no', $orddata->dlv_no)->where('dlv_type', 'P')->where('sys_ord_no', $sys_ord_no)->orderBy('created_at', 'desc')->count();
                            if($pickdlvcount >0){
                                $ordModel = new OrderMgmtModel;
                                $ordModel->updateOrdStatusAndFinishDate($sys_ord_no, 'PICKED');
                            }
                            DB::table('mod_dlv_plan')
                            ->where('sys_ord_no', $sys_ord_no)
                            ->where('dlv_no', $orddata->dlv_no)
                            ->where('dlv_type','P')
                            ->update(['status' => 'FINISHED','finish_date'=>Carbon::now()->toDateTimeString()]);
                        }
                    }
                    if($orderdata[$i]['singlepost']=="Y"){
                        array_push($insertData, [
                            'ord_id'      => $orddata->id,
                            'dlv_no'      => $orddata->dlv_no,
                            'order_tally' => $order_tally,
                            'real_num'    => $realnum,
                            'ord_num'     => $orderednum,
                            'g_key'       => $user->g_key,
                            'c_key'       => $user->c_key,
                            's_key'       => $user->s_key,
                            'd_key'       => $user->d_key,
                            'created_by'  => $user->email,
                            'created_at'  => Carbon::now()->toDateTimeString(),
                            'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                    }
                    if($orderdata[$i]['singlepost']=="N" && $realnum==$orderednum){
                        array_push($insertData, [
                            'ord_id'      => $orddata->id,
                            'dlv_no'      => $orddata->dlv_no,
                            'order_tally' => $order_tally,
                            'real_num'    => $realnum,
                            'ord_num'     => $orderednum,
                            'g_key'       => $user->g_key,
                            'c_key'       => $user->c_key,
                            's_key'       => $user->s_key,
                            'd_key'       => $user->d_key,
                            'created_by'  => $user->email,
                            'created_at'  => Carbon::now()->toDateTimeString(),
                            'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                    }
                }
            }
            DB::table('mod_order_goods_check')->insert($insertData);          
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return response()->json(["msg"=>"error","data"=>$e->getLine()]);
        }
        \Log::info("success");
        return response()->json(["msg"=>"success"]);
    }

    public function getorderinfo() {
        try{
            $user = Auth::user();
            $status =['FINISHED','ERROR','REJECT'];
            $data = []; 
            $this_query = DB::table('mod_order');
            $this_query->select(
                'mod_order.sys_ord_no as ord_no',
                'mod_order.realnum as real_num',
                DB::raw('(select sum(pkg_num) from mod_order_detail where ord_id = mod_order.id )as ord_num'),
                'mod_order.ord_no as sys_ord_no'
            );
            $this_query->where('car_no', $user->email);
            $this_query->where('is_ordered','!=', 'Y');
            $this_query->whereNotIn('status',$status);
            $this_query->where('dlv_no','!=','');
            $this_query->orderBy('created_at', 'desc');
            $data = $this_query->get(); 
            \Log::info($data);
        }      
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            return response()->json(["msg"=>"error","data"=>$e->getLine()]);
        }                    
        return response()->json(['msg' => 'success', 'data' => $data]);
    }

    public function getTrackingStatus() {
        $user = Auth::user();
        $userData = DB::table('users')->select('online')->where('email', $user->email)->first();
        
        return response()->json(['msg' => 'success', 'status' => 1]);
    }

    public function guid(){
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $uuid = substr($charid, 0, 8)
            .substr($charid, 8, 4)
            .substr($charid,12, 4)
            .substr($charid,16, 4)
            .substr($charid,20,12);
        return $uuid;
    }

    public function updateDtoken() {
        $user = Auth::user();
        $dToken = request('dToken');
        try {
            DB::table('users')->where('email', $user->email)->update(['d_token' => $dToken]);
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }
        

        return response()->json(['msg' => 'success']);
    }

    public function getNoticeCnt() {
        $user = Auth::user();
        $noticeCnt = 0;
        try {
            $noticeCnt = DB::table('sys_notice')
                            ->where('car_no', $user->email)
                            ->where('is_read', 0)
                            ->count();
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }
        

        return response()->json(['msg' => 'success', 'cnt' => $noticeCnt]);
    }

    public function setNoticeRead() {
        $user = Auth::user();
        try {
            $noticeCnt = DB::table('sys_notice')
                            ->where('car_no', $user->email)
                            ->update(['is_read' => 1]);
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }
        

        return response()->json(['msg' => 'success']);
    }

    public function reorderDlvPlan(Request $request) {
        \Log::info("reorderDlvPlan");
        \Log::info($request->all());
        $user = Auth::user();

        $dlvItem = $request->dlvItem;
        $dlvNo = $request->dlvNo;
        $i = 0;
        foreach($dlvItem as $row) {
            DB::table('mod_dlv_plan')
                ->where('dlv_no', $dlvNo)
                ->where('sys_ord_no', $row)
                ->update(['sort' => $i]);
            $i++;
        }
        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
        // return response()->json(['msg' => 'success']);
    }

    public function uploadSignImg() {
        $user   = Auth::user();
        $id = request('id');
        $img      = request('img');
        $fileName = request('fileName');
        $insertData = array();
        $imgtype = "N";
        /////
        \Log::info("upload img");
        \Log::info($id);
        $ordData = DB::table('mod_order')->where('id', $id)->first();
        if(!isset($ordData)) {
            return response()->json(['msg' => 'success','code' => '00']);
        }
        \Log::info("upload ref_no is ".$ordData->sys_ord_no);
        try {
			 \Log::info("update order sign img");
			 DB::table('mod_order')
			->where('id', $id)
			->update(['sign_pic' => '1']);
			 \Log::info("update order sign end");
            $guid = $this->guid();
            if(!empty($fileName)) {
                Storage::disk('local')->put($fileName, base64_decode($img));
                $imgsize = $this->getBase64ImageSize($img);
                \Log::info($imgsize);
                \Log::info($fileName);
                if(strpos($fileName, '.png') !== false) {
                    //found
                } else {
                    //not found
                    $fileName = $fileName.'.png';
                }
                if($imgsize>=2500){
                    $imgtype = "Y";
                }
                $checkcount = DB::table('mod_file')
                ->where('ref_no', $ordData->sys_ord_no)
                ->where('guid', $fileName)
                ->where('type_no', 'FINISH')
                ->count();
                if($checkcount==0){
                    array_push($insertData, [
                        'guid'        => $fileName,
                        'ref_no'      => $ordData->sys_ord_no,
                        'type_no'     => 'FINISH',
                        'file_format' => 'jpg',
                        'empty_img'   => $imgtype,
                        'g_key'       => $ordData->g_key,
                        'g_key'       => $ordData->g_key,
                        'c_key'       => $ordData->c_key,
                        's_key'       => $ordData->s_key,
                        'd_key'       => $ordData->d_key,
                        'created_by'  => $user->email,
                        'updated_by'  => $user->email,
                        'created_at'  => Carbon::now()->toDateTimeString(),
                        'updated_at'  => Carbon::now()->toDateTimeString()
                    ]);
                    DB::table('mod_file')->insert($insertData);
                    // $orderdetails = DB::table('mod_shippen_temp_detail')->where('customerpo',$ordData->ord_no)->first();
                    // if(isset($orderdetails)){
                    //     $years = substr($ordData->finish_date,0,4);
                    //     $months= substr($ordData->finish_date,5,2);
                    //     Storage::disk('azure')->put($years.'/'.$months.'/'.$ordData->ord_no.'.jpg', base64_decode($img));
                    // }
                }else{
                    DB::table('mod_file')
                    ->where('ref_no', $ordData->sys_ord_no)
                    ->where('guid', $fileName)
                    ->where('type_no', 'FINISH')
                    ->update([
                        'empty_img'   => $imgtype,
                        'updated_by'  => $user->email,
                        'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                }
				\Log::info("sure finised uploadSignImg");
                $signPic = 0;
                \Log::info("update signpiceure uploadSignImg");
                \Log::info($id);
                 DB::table('mod_order')
                    ->where('id', $id)
                    ->update(['sign_pic' => '1']);
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage(),'code' => '01']);
        }

        return response()->json(['msg' => 'success','code' => '00']);
    }

    public function getBase64ImageSize($base64Image){ //return memory size in B, KB, MB
        try{
            $size_in_bytes = (int) (strlen(rtrim($base64Image, '=')) * 3 / 4);
            $size_in_kb    = $size_in_bytes / 1024;
    
            return $size_in_bytes;
        }
        catch(Exception $e){
            return $e;
        }
    }

    public function uploadAbnormalImg(Request $request) {
        $user       = Auth::user();
        $id         = $request->id;
        $img        = $request->img;
        $fileName   = $request->fileName;
        $insertData = array();
        \Log::info("upload error img");
        \Log::info($id);
        \Log::info($fileName);
        $ordData = DB::table('mod_order')->where('id', $id)->first();
        $imgtype = "N";
        try {
            $guid = $this->guid();

            if(!empty($fileName)) {
                Storage::disk('local')->put($fileName, base64_decode($img));
                $imgsize = $this->getBase64ImageSize($img);
                \Log::info($imgsize);
                if($imgsize>=2500){
                    $imgtype = "Y";
                }
                $checkcount = DB::table('mod_file')
                ->where('ref_no', $ordData->sys_ord_no)
                ->where('guid', $fileName)
                ->where('type_no', 'ERROR')
                ->count();
                if($checkcount<1){
                    array_push($insertData, [
                        'guid'        => $fileName,
                        'ref_no'      => $ordData->sys_ord_no,
                        'type_no'     => 'ERROR',
                        'file_format' => 'jpg',
                        'empty_img'   => $imgtype,
                        'g_key'       => $ordData->g_key,
                        'c_key'       => $ordData->c_key,
                        's_key'       => $ordData->s_key,
                        'd_key'       => $ordData->d_key,
                        'created_by'  => $user->email,
                        'updated_by'  => $user->email,
                        'created_at'  => Carbon::now()->toDateTimeString(),
                        'updated_at'  => Carbon::now()->toDateTimeString()
                    ]);
                }else{
                    DB::table('mod_file')
                    ->where('ref_no', $ordData->sys_ord_no)
                    ->where('guid', $fileName)
                    ->where('type_no', 'ERROR')
                    ->update([
                        'empty_img'   => $imgtype,
                        'updated_by'  => $user->email,
                        'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                }
				
				\Log::info("sure error");
				if(($ordData->status=="FINISHED" || $ordData->status=="REJECT") && $ordData->owner_cd=="019"){
					\Log::info("sure finised benq");
                    // $orderdetails = DB::table('mod_shippen_temp_detail')->where('customerpo',$ordData->ord_no)->first();
                    // if(isset($orderdetails)){
                    //     DB::table('sys_benq_edi_log')->insert([
                    //         'sys_ord_no' => $sysOrdNo,
                    //         'count' => 0,
                    //         'status' => 'N',
                    //         'g_key' =>$user->g_key,
                    //         'c_key' =>$user->c_key,
                    //         's_key' =>$user->s_key,
                    //         'd_key' =>$user->d_key,
                    //         'created_by' => $user->email,
                    //         'created_at' => \Carbon::now()
                    //     ]);
                    // }
					\Log::info("sure error end");
				}else{
					\Log::info($ordData->status."-".$ordData->owner_cd);
				}
                
                $error_pic = 0;
                if(count($insertData) > 0) {
                    $error_pic = 1;
                    DB::table('mod_file')->insert($insertData);

                    DB::table('mod_order')
                        ->where('id', $id)
                        ->update(['error_pic' => $error_pic]);
                }
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage(),'code'=>'01']);
        }

        return response()->json(['msg' => 'success','code'=>'00']);
    }

    public function updateSnNo_new(Request $request) {
        \Log::info("updateSnNo_new");
        \Log::info($request->all());
        $snNo = $request->snNo;
        $id   = $request->id;
        $teestnewsn = "";
        $insertData = array();
        $user = Auth::user();
        try {
            $newSnNo = str_replace( array("\n", "\r"),",",$snNo);
            $newSnNo = explode(",",$newSnNo);
            for($i= 0 ; $i<count($newSnNo); $i++){
                if($newSnNo[$i]!=""){
                    $teestnewsn=$teestnewsn.$newSnNo[$i].',';
                }
            }
            // $imSnNo =  str_replace( array("\n", "\r"),",",$snNo);
            $imSnNo = rtrim($teestnewsn,',');
            \Log::info("updateSnNo result");
            \Log::info($imSnNo);
            // \Log::info($imSnNo);
            DB::table('mod_order_detail')
            ->where('id', $id)
            ->update([
                'sn_no' => $imSnNo,
                'updated_at' => Carbon::now()->toDateTimeString(),
                'updated_by' => $user->email
            ]);

            $user = Auth::user();
            try{

                $detailData = DB::connection('mysql::write')->table('mod_order_detail')
                ->where('id', $id)
                ->first();

                $orddata = DB::connection('mysql::write')->table('mod_order')
                ->where('id', $detailData->ord_id)
                ->first();
                
                DB::table('mod_box_sn')
                ->where('sys_ord_no', $orddata->sys_ord_no)
                ->where('ord_detail_id', $id)
                ->delete();
                
                $explodeSnArray = explode(',', $detailData->sn_no);
                foreach ($explodeSnArray as $explodekey => $value) {
                    $data = [
                        'sys_ord_no'    => $orddata->sys_ord_no,
                        'ord_detail_id' => $id,
                        'sn_no'         => $value,
                        'g_key'         => $user->g_key,
                        'c_key'         => $user->c_key,
                        's_key'         => $user->s_key,
                        'd_key'         => $user->d_key,
                        'created_by'    => $user->email,
                        'updated_by'    => $user->email,
                        'created_at'    => \Carbon::now(),
                        'updated_at'    => \Carbon::now(),
                    ];
                    array_push($insertData, $data);
                }

                DB::table('mod_box_sn')->insert($insertData);

                DB::table('mod_sn_history')->insert([
                    'ord_id' 		=> $id,
                    'snno' 			=> $imSnNo,
                    'created_by' 	=> $user->email,
                    'updated_by' 	=> $user->email,
                    'created_at' 	=> \Carbon::now(),
                    'updated_at' 	=> \Carbon::now(),
                ]);
            }catch(\Exception $e) {
                \Log::error($e->getLine());
                \Log::error($e->getMessage());
            }

        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }

        
        return response()->json(['msg' => 'success', 'code' => "00"]);
    }

    public function delPhoto(Request $request)
    {
        \Log::info("delPhoto");
        \Log::info($request->all());
        $fileName = $request->fileName;
        $user = Auth::user();
        $filecount = 999;
        $filetype = "";
        try {
            $filedata = DB::table('mod_file')->where('guid', $fileName)->where('g_key', $user->g_key)->first();
            DB::table('mod_file')->where('guid', $fileName)->where('g_key', $user->g_key)->delete();
            if($filedata->type_no=="ERROR"){
                $filetype = "error_pic";
                $filecount = DB::table('mod_file')->where('ref_no', $filedata->ref_no)->where('type_no', 'ERROR')->count();
            }else{
                $filetype = "sign_pic";
                $filecount = DB::table('mod_file')->where('ref_no', $filedata->ref_no)->whereIn('type_no', ['FINISH','SIGNIN'])->count();
            }
            if($filecount==0 ){
                DB::table('mod_order')
                ->where('sys_ord_no', $filedata->ref_no)
                ->update([$filetype => 0]);
            }
            Storage::disk('local')->delete($fileName);
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage(),'code'=>'01']);
        }

        
        return response()->json(['msg' => 'success','code'=>'00']);
    }

    public function checkdlvorder(Request $request) {

        \Log::info("checkdlvorder");
        \Log::info($request->all());
        $user = Auth::user();
        $ord_no = $request->ord_no ;
        $data = array();
        $isordno = DB::table('mod_order')
        ->Where(function($query) use ($ord_no)
        {
            $query->orwhere("ord_no",$ord_no);
            $query->orwhere("sys_ord_no",$ord_no);
        })
        ->whereNotIn("status",["FINISHED","REJECT"])
        ->count();
        $dlvno=DB::table('mod_dlv')
        ->where("car_no",$user->email)
        ->pluck("dlv_no")->toarray();
        $data = DB::table('mod_order')
        ->select("id","ord_no","pkg_num")
        ->whereNotIn("status",["FINISHED","REJECT"])
        ->Where(function($query) use ($ord_no)
        {
            $query->orwhere("ord_no",$ord_no);
            $query->orwhere("sys_ord_no",$ord_no);
        })
        ->get();
        if(count($data)==0){
            return response()->json(['msg' => '查無資料或訂單狀態是已完成或拒收','code' => "01" ]);
        }

        return response()->json(['msg' => 'success','code' => "00",'data' => $data ]);
    }
    
    public function dlvorder(Request $request) {

        \Log::info("dlvorder");
        \Log::info($request->all());
        $user = Auth::user();
        $ids=$request->ids;
        $dlv_detail  = [];
        $today       = new \DateTime();
        $str_date    = $today->format('Ym');
        $params = array(
            "c_key"   => $user->c_key,
            "date_ym" => substr($str_date, 2, strlen($str_date))
        );
        $BaseModel = new BaseModel();
        $dlv_no = $BaseModel->getAutoNumber("dlv",$params);    
        $status ="DLV";
        $car_no = $user->email;
        $driver_nm = $user->name;
        $ttl_cbm     = 0;
        $ttl_pkg_num = 0;
        $dlv_date = date('Y-m-d',strtotime('+1 day'));
        $car = DB::table('mod_car')->where('car_no', $car_no)->first();
        $errormsg =array();
        try{

            foreach($ids as $key=> $row){
                $check =DB::table('mod_order')
                ->where('id', $row)
                ->first();
                if(!isset($check)){
                    array_push($errormsg, $row);
                }else{
                    if($check->status=="FINISHED" || $check->status=="REJECT" ){
                        array_push($errormsg, $row);
                    }
                }
            }
            if(count($errormsg)>0 ){
                return ["msg"=>"紅色訂單號不存在或狀態有問題，請移除或確認後在送出!",'code' => "01","data"=>$errormsg];    
            }

            foreach($ids as $key=> $row){
                $ord = OrderMgmtModel::find($row);
    
                if($ord->dlv_no!=""){
                    DB::table('mod_dlv_plan')
                    ->where('status',"!=", 'FINISHED')
                    ->where('dlv_no', $ord->dlv_no)
                    ->where('sys_ord_no', $ord->sys_ord_no)
                    ->update([
                        'status' => "ERROR",
                        'error_cd' => "A001",
                        'error_descp' => "貨物--換車 收/送",
                        'abnormal_remark' => "",
                        'updated_at' => Carbon::now()->toDateTimeString(),
                        'finish_date' => Carbon::now()->toDateTimeString()
                    ]);
                    DB::table('mod_order')
                    ->where('id', $ord->id)
                    ->update([
                        'error_remark' =>  "貨物--換車 收/送",
                        'exp_reason' => "A001"
                    ]);
                    if(!empty($ord->pick_addr)){
                        DB::table('mod_order')
                        ->where('id', $ord->id)
                        ->update([
                            'dlv_type' =>  "P",
                        ]); 
                    }

                    $cnt = DB::table('mod_dlv_plan')->where('dlv_no', $ord->dlv_no)->count();
                    $fCnt = DB::table('mod_dlv_plan')->where('dlv_no', $ord->dlv_no)->whereRaw("(status = 'FINISHED' or status = 'ERROR' or status = 'REJECT')")->count();
                    $dlvnum = DB::table('mod_dlv')->where('dlv_no', $ord->dlv_no)->value('dlv_num');
                    $dlvnum = $dlvnum-1;
                    DB::table('mod_dlv')
                    ->where('dlv_no', $ord->dlv_no)
                    ->update(['dlv_num'=>$dlvnum]);
                    if($cnt == $fCnt) {
                        DB::table('mod_dlv')
                            ->where('dlv_no', $ord->dlv_no)
                            ->update(['status' => 'FINISHED']);
                        $checkdepartue =DB::table('mod_dlv')
                        ->where('dlv_no', $ord->dlv_no)
                        ->first();
                        if($checkdepartue->departure_time==null) {
                            DB::table('mod_dlv')
                                ->where('dlv_no', $ord->dlv_no)
                                ->update([
                                    'departure_time' => Carbon::now()->toDateTimeString(),
                                ]);
                        }
                        if($checkdepartue->finished_time==null) {
                            DB::table('mod_dlv')
                                ->where('dlv_no', $ord->dlv_no)
                                ->update([
                                    'finished_time' => Carbon::now()->toDateTimeString(),
                                ]);
                        }
                    }
                }
                $ttl_cbm     += $ord->total_cbm;
                $ttl_pkg_num += $ord->pkg_num;
    
                $ordCnt = DB::table('mod_dlv_plan')
                            ->where('status', 'FINISHED')
                            ->where('dlv_type', 'P')
                            ->where('sys_ord_no', $ord->sys_ord_no)
                            ->count();
                //$ord->dlv_type != 'P' 代表有提貨在其它配送單裡
                $isUrgent = 'N';
                //提貨地址不為空 新增提貨 派車明細
                if(!empty($ord->pick_addr) && $ordCnt == 0 && $ord->dlv_type != 'P') {
                    $address = $ord->pick_zip.$ord->pick_city_nm.$ord->pick_area_nm.$ord->pick_addr;
                    if($ord->g_key == 'SYL') {
                        $address = $ord->pick_zip.$ord->pick_addr;
                    }
                    $detail = array(
                        "ord_id"      => $ord->id,
                        "cust_nm"     => $ord->pick_cust_nm,
                        "dlv_type"    => 'P',
                        "ord_no"      => $ord->ord_no,
                        "sys_ord_no"  => $ord->sys_ord_no,
                        "status"      => "FINISHED",
                        "dlv_no"      => $dlv_no,
                        "addr"        => $address,
                        "etd"         => $ord->etd,
                        "sort"        => $key,
                        "remark"      => htmlspecialchars($ord->pick_remark),
                        "updated_at"  => $today->format('Y-m-d H:i:s'),
                        "updated_by"  => $user->email,
                        "created_at"  => $today->format('Y-m-d H:i:s'),
                        "created_by"  => $user->email,
                        "g_key"       => $user->g_key,
                        "c_key"       => $user->c_key,
                        "s_key"       => $user->s_key,
                        "d_key"       => $user->d_key,
                        'car_no'      => $car_no,
                        'driver_nm'   => $driver_nm,
                        'owner_nm'    => $ord->owner_nm,
                        'is_urgent'   => $isUrgent,
                        'finish_date' => Carbon::now()->toDateTimeString()
                    );
                    array_push($dlv_detail, $detail);
                }
                //配送地址不為空 新增配送 派車明細
                if(!empty($ord->dlv_addr)) {
                    $address = $ord->dlv_zip.$ord->dlv_city_nm.$ord->dlv_area_nm.$ord->dlv_addr;
                    if($ord->g_key == 'SYL') {
                        $address = $ord->dlv_zip.$ord->dlv_addr;
                    }
                    $detail = array(
                        "ord_id"     => $ord->id,
                        "cust_nm"    => $ord->dlv_cust_nm,
                        "dlv_type"   => 'D',
                        "ord_no"     => $ord->ord_no,
                        "sys_ord_no" => $ord->sys_ord_no,
                        "status"     => $status,
                        "dlv_no"     => $dlv_no,
                        "addr"       => $address,
                        "etd"        => $ord->etd,
                        "sort"       => $key,
                        "remark"     => htmlspecialchars($ord->dlv_remark),
                        "updated_at" => $today->format('Y-m-d H:i:s'),
                        "updated_by" => $user->email,
                        "created_at" => $today->format('Y-m-d H:i:s'),
                        "created_by" => $user->email,
                        "g_key"      => $user->g_key,
                        "c_key"      => $user->c_key,
                        "s_key"      => $user->s_key,
                        "d_key"      => $user->d_key,
                        'car_no'      => $car_no,
                        'driver_nm'   => $driver_nm,
                        'owner_nm'    => $ord->owner_nm,
                        'is_urgent'   => $isUrgent,
                        'finish_date' => null
                    );
    
                    array_push($dlv_detail, $detail);
                }
    
                $tsData = DB::table("mod_trans_status")->where("ts_type", "A")->where('order', 3)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->orderBy("order", "desc")->first();
                $data = [
                    'ts_no'      => $tsData->id,
                    'ts_type'    => $tsData->ts_type,
                    'sort'       => $tsData->order,
                    'ts_name'    => $tsData->ts_name,
                    'ts_desc'    => $tsData->ts_desc,
                    'ref_no1'    => $dlv_no,
                    'ref_no2'    => $ord->ord_no,
                    'ref_no3'    => $car_no,
                    'ref_no4'    => $ord->sys_ord_no,
                    'g_key'      => $ord->g_key,
                    'c_key'      => $ord->c_key,
                    's_key'      => $ord->s_key,
                    'd_key'      => $ord->d_key,
                    'created_by' => $user->name,
                    'updated_by' => $user->name,
                ];
                $TransRecordModel = new TransRecordModel();
                $TransRecordModel->createRecord($data);
            }
    
            $modDlv               = new DlvModel;
            $modDlv->dlv_no       = $dlv_no;
            $modDlv->dlv_date     = $dlv_date;
            $modDlv->status       = "SEND";
            $modDlv->load_rate    = $car->load_rate;
            $modDlv->load_tweight = 0;//$car->load_weight;
            $modDlv->load_tcbm    = $ttl_cbm;
            $modDlv->dlv_num      = count($ids);
            $modDlv->cust_no      = $car->cust_no;
            $modDlv->car_no       = $car->car_no;
            $modDlv->car_type_nm  = $car->car_type_nm;
            $modDlv->driver_nm    = $driver_nm;
            $modDlv->driver_phone = $user->phone;
            $modDlv->updated_by   = $user->email;
            $modDlv->created_by   = $user->email;
            $modDlv->g_key        = $user->g_key;
            $modDlv->c_key        = $user->c_key;
            $modDlv->s_key        = $user->s_key;
            $modDlv->d_key        = $user->d_key;
            $modDlv->save();
            DB::table('mod_dlv_plan')->insert($dlv_detail);
            DB::table('mod_order')
            ->whereIn('id', $ids)
            ->update([
                "status"   => "LOADING", 
                "status_desc"   => "貨物裝載中", 
                "car_no"   => $car_no,
                "truck_no" => $car_no,
                "dlv_no"   => $dlv_no,
                "is_ordered" => "N",
                "order_tally" => "N",
                "realnum" => 0,
                "driver"   => $driver_nm
            ]);
            try {
                $fcmnow = date("Y-m-d H:i:s");
                $f = new FcmModel;
                $f->sendToFcm('系統通知 '.$fcmnow , '您有新配送單：'.$dlv_no, $user->d_token);
                $s = new SysNoticeModel;
                $s->insertNotice('系統通知 '.$fcmnow, '您有新配送單：'.$dlv_no, $car_no, $user);
            } catch (\Throwable $th) {
                //throw $th;
            }

        }
        catch(\Exception $e)
        {
            return response()->json(['msg' => 'error','code' => "01"]);
            return ["msg"=>"紅色訂單號不存在或狀態有問題，請移除或確認後在送出!",'code' => "01","data"=>$errormsg, 'line'=>$e->getLine(), 'message'=>$e->getMessage(), ];
        }
        return response()->json(['msg' => '已完成接單程序，請至運輸作業查看!','code' => "00" ]);
    }

    public function getErrorData_new(Request $request) {
        \Log::info("getErrorData_new");
        \Log::info($request->all());
        $user = Auth::user();    
        $errorType = array(
            "data" => null
        );
        try{
            $orderdata =  DB::table('mod_order')
            ->where('sys_ord_no', $request->sys_ord_no)
            ->first();
            if(!isset($request->owner_cd)){
                $request->owner_cd = $orderdata->owner_cd;
            }
            $owner_cd =  DB::table('sys_customers')
            ->where('cust_no', $request->owner_cd)
            ->where('g_key', $user->g_key)
            ->first();
            if(isset($owner_cd)){
                $bscode= DB::table('mod_cust_error')
                ->select('cd', 'cd_descp')
                ->where('cust_id', $owner_cd->id)
                ->orderByRaw("CAST(sorted as UNSIGNED) ASC")
                ->get();
                if(count($bscode)== 0){
                    $this_query = DB::table('bscode');
                    $this_query->select('cd', 'cd_descp');
                    $this_query->where('cd_type', 'ERRORTYPE');
                    $this_query->where('cd',"!=", 'E001');
                    $this_query->where('g_key', $user->g_key);
                    $this_query->where('c_key', $user->c_key); 
                    $this_query->orderByRaw("CAST(value2 as UNSIGNED) ASC");		
                    $errorType["data"] = $this_query->get();
                }else{
                    $errorType["data"] = $bscode;
                }
            }else{
                $this_query = DB::table('bscode');
                $this_query->select('cd', 'cd_descp');
                $this_query->where('cd_type', 'ERRORTYPE');
                $this_query->where('cd',"!=", 'E001');
                $this_query->where('g_key', $user->g_key);
                $this_query->where('c_key', $user->c_key); 
                $this_query->orderByRaw("CAST(value2 as UNSIGNED) ASC");		
                $errorType["data"] = $this_query->get();
            }
        }catch(\Exception $e)
        {
            return response()->json(['msg' => 'error','code' => "01"]);
        }
        return response()->json(['msg' => 'success','code' => "00","data"=>$errorType["data"]]);
    }

    public function getExtraData_new(Request $request) {
        \Log::info("getExtraData_new");
        \Log::info($request->all());
        $user = Auth::user();
        $errorType = array(
            "data" => null
        );
        try{
            $owner_cd =  DB::table('sys_customers')
            ->where('cust_no', $request->owner_cd)
            ->where('g_key', $user->g_key)
            ->first();
            if(isset($owner_cd)){
                $bscode= DB::table('mod_cust_fee')
                ->select(DB::raw('fee_cd as cd'), DB::raw('fee_name as cd_descp') , '0 as value1')
                ->where('cust_id', $owner_cd->id)
                ->get();
                if(count($bscode)== 0){
                    $this_query = DB::table('bscode');
                    $this_query->select('cd', 'cd_descp' , 'value1');
                    $this_query->where('cd_type', 'EXTRAFEE');                           
                    $this_query->orderByRaw("CAST(value2 as UNSIGNED) ASC");
                    $errorType["data"] = $this_query->get();    
                }else{
                    $errorType["data"] = $bscode;
                }
            }else{
                $this_query = DB::table('bscode');
                $this_query->select('cd', 'cd_descp','value1');
                $this_query->where('cd_type', 'EXTRAFEE');
                $this_query->orderByRaw("CAST(value2 as UNSIGNED) ASC");
                $errorType["data"] = $this_query->get();
            }
        }catch(\Exception $e)
        {
            return response()->json(['msg' => '操作發生問題','code' => "01"]);
        }
        return response()->json(['msg' => 'success','code' => "00","data"=>$errorType["data"]]);
    }

    public function menupersion () {
        $user = Auth::user();
        $data = array();
        $dlvcar= "N";
        $dlvorder = "N";
        $setting = "N";
        $fcm_app = "N";
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
        try{
            if($user->hasPermissionTo('dlvcar_app'))
            {
                $dlvcar="Y";
            }else{
                $dlvcar="N";
            }
        }
        catch(\Exception $e) {
            $dlvcar="N";
        }

        try{
            if($user->hasPermissionTo('setting_app'))
            {
                $setting="Y";
            }else{
                $setting="N";
            }
        }
        catch(\Exception $e) {
            $setting="N";
            // return response()->json(['msg' => 'error','code'=>'01', 'error_log' => $e->getMessage(), 'error_line' => $e->getLine()]);
        }
        try{
            if($user->hasPermissionTo('fcm_app'))
            {
                $fcm_app="Y";
            }else{
                $fcm_app="N";
            }
        }
        catch(\Exception $e) {
            $fcm_app="N";
        }

        try{
            if($user->hasPermissionTo('dlvorder_app'))
            {
                $dlvorder="Y";
            }else{
                $dlvorder="N";
            }
        }
        catch(\Exception $e) {
            $dlvorder="N";
        }

        array_push($data, [
            'function'        => "配送作業",
            'code'        => "dlvcar_app",
            'enabled'     => $dlvcar
        ]);

        array_push($data, [
            'function'        => "接單作業",
            'code'        => "dlvorder_app",
            'enabled'     => $dlvorder
        ]);
        array_push($data, [
            'function'        => "推播通知",
            'code'        => "fcm_app",
            'enabled'     => $fcm_app
        ]);
        array_push($data, [
            'function'        => "設定",
            'code'        => "setting_app",
            'enabled'     => $setting
        ]);

        return response()->json([
            'msg' => 'success',
            'code' => '00',
            'data' => $data,
        ]);
    }

    public function geteditoken (Request $request) {
        $key = $request->key ;
        $now = date("YmdHis");
        \Log::info("geteditoken");
        \Log::info($request->all());
        try {
            $custCode = DB::connection('mysql::write')
            ->table('bscode')
            ->where('value1',$request->key)
            ->where('cd_type', 'OPENAPICUST')
            ->first();
    
            if(isset($custCode) ) {
                
                $token = base64_encode($key.$now);
    
                DB::table('bscode')
                ->where('cd_type', 'OPENAPICUST')
                ->update([
                    'token' => $token
                ]);
    
                return response()->json(['msg' => 'success','code' => "00","token"=> $token ]);
            } else {
                return response()->json(['msg' => 'token錯誤','code' => "01"]);
            }
            
        } catch (\Throwable $e) {
            return response()->json(['msg' => 'token錯誤','code' => "012", ]);
        }
    }

    public function checkopenApiCoulumnType($maindata) {
        $errormsg = "";

         //YN 系列
        $ynType  = ['is_urgent', 'owner_send_mail', 'pick_send_mail', 'dlv_send_mail'];
        $intType = ['collectamt', 'pkg_num', 'realnum', 'total_amount'];

        $stringType70 = ['truck_cmp_nm', 'owner_nm', 'pick_cust_nm', 'dlv_cust_nm'];
        $stringType20 = ['truck_cmp_no', 'owner_cd', 'car_no', 'pick_cust_no', 'dlv_cust_no', 'cust_ord_no' ];
        $stringType30 = ['pick_attn', 'driver', 'pick_tel', 'pick_tel2', 'dlv_attn', 'dlv_tel', 'dlv_tel2',  ];
        $stringType10 = ['pick_zip', 'dlv_zip' ];

        $stringType100 = ['pick_email', 'dlv_email'];
        $stringType200 = ['wh_addr', 'pick_addr', 'dlv_addr'  ];
        $stringType500 = ['wms_order_no'];
        
        $format = 'Y-m-d';
        $dateresult =  date($format, strtotime($maindata['etd'])) == $maindata['etd'];
        if(!$dateresult) {
            $errormsg .= "訂單號".$maindata['ord_no'].': '."etd 有問題 請檢查";
        }

        // dd($maindata);
        foreach ($maindata as $key => $value) {

            //YN 系列
            if (in_array($key, $ynType)) {
                if($value != 'N' && $value !='Y') {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 有問題 請檢查";
                }
            }
            //YN 系列 end

            //int 系列
            if (in_array($key, $intType)) {
                if(!is_numeric($value)) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 有問題 請檢查";
                }
            }
            //int 系列 end

            //字串
            if (in_array($key, $stringType10)) {
                if(mb_strlen($value,'UTF-8') > 10) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType20)) {
                if(mb_strlen($value,'UTF-8') > 20) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType30)) {
                if(mb_strlen($value,'UTF-8') > 30) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType70)) {
                if(mb_strlen($value,'UTF-8') > 70) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType100)) {
                if(mb_strlen($value,'UTF-8') > 100) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType200)) {
                if(mb_strlen($value,'UTF-8') > 200) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType500)) {
                if(mb_strlen($value,'UTF-8') > 500) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }
            //字串end
        }

        return $errormsg;
    }

    public function checkopenApiDetailCoulumnType($ordNo ,$detaildata) {

        $errormsg = "";

        $intType      = ['pkg_num', 'gw', 'length', 'weight', 'height'];
        $stringType1  = ['pkg_unit'];
        $stringType2  = ['goods_no', 'goods_no2'];
        $stringType3  = ['cbmu', 'gwu'];
        $stringType7  = ['goods_nm'];
        $stringTypeSn = ['sn_no',];


        for ($i=0; $i < count($detaildata); $i++) { 

            foreach ($detaildata[$i] as $key => $value) {

                //int 系列
                if (in_array($key, $intType)) {
                    if(!is_numeric($value)) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 有問題 請檢查";
                    }
                }
                //int 系列 end
                
                //字串
                if (in_array($key, $stringType1)) {
                    if( mb_strlen($value,'UTF-8') > 10) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 超過長度 請檢查";
                    }
                }
                
                if (in_array($key, $stringType2)) {
                    if(mb_strlen($value,'UTF-8') > 20) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 超過長度 請檢查";
                    }
                }
                
                if (in_array($key, $stringType3)) {
                    if(mb_strlen($value,'UTF-8') > 30) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 超過長度 請檢查";
                    }
                }
                
                if (in_array($key, $stringType7)) {
                    if(mb_strlen($value,'UTF-8') > 70) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 超過長度 請檢查";
                    }
                }
                
                
                if (in_array($key, $stringTypeSn)) {
                    if(mb_strlen($value,'UTF-8') > 10000) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 超過長度 請檢查";
                    }
                }
                //字串end
                
            }
        }
        

        return $errormsg;
    }

    public function getorderimg(Request $request) {
        \Log::info("getorderimg");
        \Log::info($request->all());
        try {
            $user = Auth::user();
            // $dlvdata = DB::connection('mysql::write')->table('mod_dlv')
            // ->where('dlv_no', $request->dlv_no)
            // ->where('car_no', $user->email)
            // ->first();
    
            $data  = array();
            $img = DB::connection('mysql::write')
            ->table('mod_file')
            ->select(
                'id','type_no',
                DB::raw("created_by as candelimg"),
                DB::raw("CONCAT('".env('PRINTER_URL').'storage/'."' , guid)as imgurl")
            )
            ->where('ref_no', $request->sys_ord_no)
            ->get();

            foreach ($img as $key => $row) {
                # code...
                $row->candelimg = $row->candelimg == $user->email ? "Y" :"N";
            }
    
            $data['img']       = $img;
    
            return response()->json([
                'msg' => 'success',
                'code' => '00',
                'data' => $data,
            ]);

            //code...
        } catch (\Throwable $e) {
            \Log::info("getorderimg error");
            \Log::info($e->getMessage());
            return response()->json([
                'msg' => $e->getMessage(),
                'code' => '01',
            ]);
        }
    }
    
    public function delorderimg(Request $request) {
        \Log::info("delorderimg");
        \Log::info($request->all());
        try {    
            $imgdata = DB::connection('mysql::write')
            ->table('mod_file')
            ->where('id', $request->id)
            ->first(); 
    
            DB::table('mod_file')
            ->where('id', $request->id)
            ->delete();
            Storage::disk('local')->delete($imgdata->guid);
    
            return response()->json([
                'msg' => 'success',
                'code' => '00',
            ]);
            
        } catch (\Throwable $e) {
            \Log::info("delorderimg error");
            \Log::info($e->getMessage());
            return response()->json([
                'msg' => $e->getMessage(),
                'code' => '01',
            ]);
        }

    }

    public function stdgetDlvData(Request $request) {   
        \Log::info('stdgetDlvData');
        \Log::info($request->all());
        $user = Auth::user();     
        //$carNo = $request->car_no;
        $status = $request->status;     
        $modDlv = [];  

        $date = date('Y-m-d h:i:s', strtotime('-14 days'));
        $this_query = DB::table('mod_dlv');
        $this_query->select('dlv_no', 'created_at', 'departure_time', 'finished_time');
        $this_query->where('car_no', $user->email);
        if($status == 'FINISHED') {
            $this_query->whereRaw("(status = ? or status = 'ERROR')", [$status]);
            $this_query->Where('created_at', '>', $date);
            $this_query->orderBy('created_at', 'desc');
        }
        else {
            $this_query->whereRaw("(status = ? or status = 'DLV' or status = 'ERROR' or status = 'SEND')", [$status]);
        }
            
        $modDlv = $this_query->get();                           
        foreach($modDlv as $row){
            $ordnum = DB::table('mod_order')->where("dlv_no",$row->dlv_no)
            ->count();
            $row->dlv_no = $row->dlv_no." (".$ordnum.")";
        }
        return response()->json(['msg' => 'success','code' => '00', 'data' => $modDlv]);
    }

    public function stdgetDlvDetailData_NEW(Request $request)
    {
		\Log::info("stdgetDlvDetailData_NEW");
        \Log::info($request->all());
		$user       = Auth::user();
        $dlvNo      = substr($request->dlv_no,0,13);
        $filed      = $request->filed;
        $orderType  = $request->orderType;
        $modDlvPlan = [];

        $date = date('Y-m-d h:i:s', strtotime('-14 days'));
        $this_query = DB::table('mod_dlv_plan');
        $this_query->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no');
        $this_query->select(
        DB::raw("(CASE WHEN mod_order.collectamt >= 0 THEN 'Y' ELSE 'N' END) AS has_collectamt"),
        'mod_order.pick_attn', 'mod_order.pick_tel', 'mod_order.pick_cust_nm', 'mod_order.dlv_attn', 'mod_order.dlv_tel', 'mod_order.dlv_cust_nm',
        DB::raw("(CASE WHEN mod_dlv_plan.status in ('FINISHED','REJECT','ERROR') THEN 1 ELSE 0 END) AS tab_status"),
        DB::raw("(select status from mod_dlv where dlv_no = mod_dlv_plan.dlv_no  limit 1 ) AS dlv_status"), 
        
        'mod_dlv_plan.sys_ord_no', 'mod_dlv_plan.remark', 'mod_dlv_plan.addr', 'mod_dlv_plan.status', 'mod_dlv_plan.sort', 'mod_dlv_plan.dlv_type',
        'mod_dlv_plan.ord_no', 'mod_dlv_plan.ord_id','mod_dlv_plan.error_descp','mod_dlv_plan.abnormal_remark', 'mod_dlv_plan.error_cd','mod_dlv_plan.dlv_no'
        );
        $this_query->where('mod_dlv_plan.dlv_no', $dlvNo);
        $this_query->orderBy('mod_dlv_plan.is_urgent', 'desc');


        if(isset($filed) && isset($orderType)) {
            $this_query->orderBy('mod_dlv_plan.'.$filed, $orderType);
        }
        else {
            //$this_query->orderBy('mod_dlv_plan.sys_ord_no', 'asc');
            $this_query->orderBy('mod_dlv_plan.sort', 'asc');
        }
        $this_query->orderBy('mod_dlv_plan.dlv_type', 'desc');
        
        $modDlvPlan = $this_query->get();
		\Log::info("detail data");
		// \Log::info($modDlvPlan);
        return response()->json(['msg' => 'success', 'code' => '00', 'data' => $modDlvPlan]);
    }

    public function stdsendGpsData(Request $request) {
        \Log::info("stdsendGpsData");
        \Log::info($request->all());
        $shape = new Shape();
        $user = Auth::user();
        $gps = array();
        if(isset($request->gps)) {
            $gps = $request->gps;
        }

        $lng = 0;
        $lat = 0;
        $speed = 0;
        $insertData = array();
        $now = date("Y-m-d H:i:s");

        try {
            $cnt = count($gps);
            for($i=0; $i<$cnt; $i++) {
                $lng       = $gps[$i]['lng'];
                $lat       = $gps[$i]['lat'];
                $speed     = $gps[$i]['speed'];
                $timestamp = $gps[$i]['timestamp'];

                // $shape->addPoint(
                //     new ShapePoint($lat, $lng, $i, $speed, $timestamp)
                // );
                
                $data = [
                    'car_no'        => $user->email,
                    'lat'           => $lat,
                    'lng'           => $lng,
                    'speed'         => round($speed),
                    'power'         => $request->power,
                    'location_time' => $timestamp,
                    'driver_no'     => null,
                    'driver_nm'     => $user->name,
                    'person_no'     => null,
                    'person_nm'     => null,
                    'dlv_no'        => null,
                    'location_time' => $timestamp,
                    'speed'      => $speed,
                    'g_key'         => $user->g_key,
                    'c_key'         => $user->c_key,
                    's_key'         => $user->s_key,
                    'd_key'         => $user->d_key,
                    'created_by'    => $user->email,
                    'updated_by'    => $user->email,
                    'created_at'    => $now,
			        'updated_at'    => $now,
                ];

                array_push($insertData, $data);
            }

            $TrackingModel = new TrackingModel();
            $TrackingModel->insertGps($insertData);
            //$ModTrackingModel = new ModTrackingModel();
           // $ModTrackingModel->insertGps($insertData);
            $userData = DB::table('users')
            ->where('id', $user->id)
            ->update(['gps_time' => Carbon::now()->toDateTimeString()]);
            $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/TMSApp-1bf8dcff7c4b.json');

            $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->withDatabaseUri('https://tmsapp-22e76.firebaseio.com/')
                ->create();
            $database = $firebase->getDatabase();
            $reference = $database->getReference('/raw-locations//'.$user->c_key.'/'.$user->email);

            \Log::info("stdsendGpsData user!!!!!");
            \Log::info($user);
            $value = $reference->getValue();
            if($value == null) {
                $database->getReference('/raw-locations//'.$user->c_key) // this is the root reference
                ->update([
                    $user->email => [
                        0 => [
                            'lat'   => $lat,
                            'lng'   => $lng,
                            'power' => $request->power,
                            'speed' => $speed,
                            'time'  => Carbon::now()->toDateTimeString()
                        ],
                        1=> [
                            'lat'   => $lat,
                            'lng'   => $lng,
                            'power' => $request->power,
                            'speed' => $request->speed,
                            'time'  => Carbon::now()->toDateTimeString()
                        ]
                    ]
                ]);
                $this->sendGpsDataByWebsocket([
                    $user->email => [
                        0=> [
                            'lat'   => $lat,
                            'lng'   => $lng,
                            'power' => $request->power,
                            'speed' => $request->speed,
                            'time'  => Carbon::now()->toDateTimeString()
                        ]
                    ]
                ]);
            }
            else {
                $lastInfo = $value[1];
            
                $newData = [
                    'lat'   => $lat,
                    'lng'   => $lng,
                    'power' => $request->power,
                    'speed' => $speed,
                    'time'  => Carbon::now()->toDateTimeString()
                ];


                $updates = [
                    '/raw-locations//'.$user->c_key.'//'.$user->email.'/0' => $lastInfo,
                    '/raw-locations//'.$user->c_key.'//'.$user->email.'/1' => $newData,
                ];

                $database->getReference() // this is the root reference
                ->update($updates);
                $this->sendGpsDataByWebsocket([
                    $user->email => [
                        0=> [
                            'lat'   => $lat,
                            'lng'   => $lng,
                            'power' => $request->power,
                            'speed' => $speed,
                            'time'  => Carbon::now()->toDateTimeString()
                        ]
                    ]
                ]);
            }            
        }
        catch(\Exception $e) {
            return response()->json(["msg"=>"error", 'code' => '01', "data"=>null]);
        }

        return response()->json(["msg"=>"success", "code"=>"00" , "data"=>null]);
    }

    public function sendGpsDataByWebsocket ($data) {
        try {
            \Log::info("enter GpsDataByWebsocket");
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://dev-wms.target-ai.com:9599/websocket/notifyByGroupId',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "groupId": "groupId_StdTmsGpsDataByWebsocket",
                    "msg": ' . json_encode($data) .'
                }',
                CURLOPT_HTTPHEADER => array(
                    'cache-control: no-cache',
                    'content-type: application/json'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            \Log::info($response);
        }
        catch(\Exception $e) {
            \Log::info("GpsDataByWebsocket error");
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
        }
        \Log::info("GpsDataByWebsocket finish");
        return;
    }

    public function stdmissonfind($sysOrdNo) {
        \Log::info("stdmissonfind");
        \Log::info($sysOrdNo);
        $user   = Auth::user();
        $main   = null;
        $detail = null;
        $newsys_ord_no = DB::table('mod_order')->where('ord_no', $sysOrdNo)->orWhere('sys_ord_no', $sysOrdNo)->pluck('sys_ord_no')->toArray();
        $new_ord_no = DB::table('mod_order')->where('car_no',$user->email)->whereIn('sys_ord_no', $newsys_ord_no)->pluck('sys_ord_no')->toArray();
        try {
            $thisQuery = DB::table('mod_dlv_plan')
                            ->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no')
                            ->select(
                            DB::raw("(CASE WHEN mod_dlv_plan.status in ('FINISHED','REJECT','ERROR') THEN 1 ELSE 0 END) AS tab_status"),
                            DB::raw("(select status from mod_dlv where dlv_no = mod_dlv_plan.dlv_no  limit 1 ) AS dlv_status"),
                            'mod_order.pick_attn', 'mod_order.pick_tel', 'mod_order.pick_cust_nm', 'mod_order.dlv_attn', 'mod_order.dlv_tel',
                             'mod_order.dlv_cust_nm',
                             'mod_dlv_plan.sys_ord_no', 'mod_dlv_plan.remark', 'mod_dlv_plan.addr', 'mod_dlv_plan.status', 'mod_dlv_plan.sort', 'mod_dlv_plan.dlv_type',
                             'mod_dlv_plan.ord_no', 'mod_dlv_plan.ord_id','mod_dlv_plan.error_descp','mod_dlv_plan.abnormal_remark', 'mod_dlv_plan.error_cd','mod_dlv_plan.dlv_no'
                             ,'mod_dlv_plan.created_at')
                            ->whereIn('mod_dlv_plan.sys_ord_no', $new_ord_no)
                            ->where('mod_dlv_plan.car_no', $user->email);
            $mainP = DB::table('mod_dlv_plan')
            ->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no')
            ->select(
            DB::raw("(CASE WHEN mod_dlv_plan.status in ('FINISHED','REJECT','ERROR') THEN 1 ELSE 0 END) AS tab_status"),
            DB::raw("(select status from mod_dlv where dlv_no = mod_dlv_plan.dlv_no  limit 1 ) AS dlv_status"),
            'mod_order.pick_attn', 'mod_order.pick_tel', 'mod_order.pick_cust_nm', 'mod_order.dlv_attn', 'mod_order.dlv_tel', 'mod_order.dlv_cust_nm',
            'mod_dlv_plan.sys_ord_no', 'mod_dlv_plan.remark', 'mod_dlv_plan.addr', 'mod_dlv_plan.status', 'mod_dlv_plan.sort', 'mod_dlv_plan.dlv_type',
            'mod_dlv_plan.ord_no', 'mod_dlv_plan.ord_id','mod_dlv_plan.error_descp','mod_dlv_plan.abnormal_remark', 'mod_dlv_plan.error_cd','mod_dlv_plan.dlv_no'
            ,'mod_dlv_plan.created_at'
            )
            ->whereIn('mod_dlv_plan.sys_ord_no', $new_ord_no)
            ->where('mod_dlv_plan.car_no', $user->email)
            ->where('mod_dlv_plan.dlv_type','P')->orderBy('created_at', 'desc')->skip(0)->take(1)->get()->toArray();

            $mainD = DB::table('mod_dlv_plan')
            ->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no')
            ->select(
            DB::raw("(CASE WHEN mod_dlv_plan.status in ('FINISHED','REJECT','ERROR') THEN 1 ELSE 0 END) AS tab_status"),
            DB::raw("(select status from mod_dlv where dlv_no = mod_dlv_plan.dlv_no  limit 1 ) AS dlv_status"),
            'mod_order.pick_attn', 'mod_order.pick_tel', 'mod_order.pick_cust_nm', 'mod_order.dlv_attn', 'mod_order.dlv_tel',
            'mod_order.dlv_cust_nm',
            'mod_dlv_plan.sys_ord_no', 'mod_dlv_plan.remark', 'mod_dlv_plan.addr', 'mod_dlv_plan.status', 'mod_dlv_plan.sort', 'mod_dlv_plan.dlv_type',
            'mod_dlv_plan.ord_no', 'mod_dlv_plan.ord_id','mod_dlv_plan.error_descp','mod_dlv_plan.abnormal_remark', 'mod_dlv_plan.error_cd','mod_dlv_plan.dlv_no'
            ,'mod_dlv_plan.created_at')
            ->whereIn('mod_dlv_plan.sys_ord_no', $new_ord_no)
            ->where('mod_dlv_plan.car_no', $user->email)
            ->where('mod_dlv_plan.dlv_type','D')->orderBy('created_at', 'desc')->skip(0)->take(1)->get()->toArray();

            $main = array_merge($mainP, $mainD);

            // $main = $thisQuery->get();
            $count = $thisQuery->count();
            if($count < 1) {
                return response()->json(['msg' => "查無資料",'code' => '01','data'=>null ]);
            }
        }
        catch(\Exception $e) {
            \Log::info("missonfind");
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> null]);
        }

        $data = array();
        $result = array(
            'msg'  => 'success',
            'code' => '00',
            'data' => $main
        );

        return response()->json($result);
    }

    public function stdordConfirm_NEW(Request $request) {

        /////
        \Log::info("stdordConfirm_NEW");
        \Log::info($request->all());
        $user   = Auth::user();
        $gps = $request->gps;
        $name       = $request->name;
        $phone      = $request->phone; 
        $Totalamount    = 0;
        $imgtype = "N";
        // $this->sendExtraData($request,$request->collectamt);
        $sysordno = $request->sysOrdNo != '' ? $request->sysOrdNo : $request->sys_ord_no;
        $checkdlvorder=DB::table('mod_order')
        ->where('sys_ord_no', $sysordno)
        ->where('car_no', $user->email)
        ->count();
        if($checkdlvorder<=0){
            return response()->json(['msg' => 'success','code'=>'00','data'=>null ]);
        }
        $extradata = array();
        if(isset($request->extradata)) {
            $extradata = $request->extradata;
        }
        try {
            if(isset($request->sninfo) && is_array($request->sninfo) ) {
                foreach($request->sninfo as $row2) {
                    $newsn = isset($row2["snNo"]) ? str_replace( array("\n", "\r"),",",$row2["snNo"]) : null;
                    DB::table('mod_order_detail')
                    ->where('id',$row2["id"])
                    ->update([
                        'sn_no' => $newsn,
                        'updated_at' => Carbon::now()->toDateTimeString(),
                        'updated_by' => $user->email
                    ]);
                    $user = Auth::user();
                    try{
                        DB::table('mod_sn_history')->insert([
                            'ord_id' 		=> $row2["id"],
                            'snno' 			=> $newsn,
                            'created_by' 	=> $user->email,
                            'updated_by' 	=> $user->email,
                            'created_at' 	=> \Carbon::now(),
                            'updated_at' 	=> \Carbon::now(),
                        ]);
                    }catch(\Exception $e) {
                        \Log::error($e->getLine());
                        \Log::error($e->getMessage());
                    } 
                }
            }
        }
        catch(\Exception $e) {
            \Log::info("ordconfrimnew error");
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return response()->json(['msg' => 'error' ,'code'=>'01' ,'data'=>null]);
        }
        $insertData = array();
        $now = date("Y-m-d H:i:s");

        try {
            $cnt = count($extradata);
			\Log::info("extradata del start");
            $orddata = DB::connection('mysql::write')
            ->table('mod_order')
            ->where('sys_ord_no', $sysordno)
            ->first();
            \Log::info($orddata->id);
            DB::table('mod_order_fee')
            ->where('ord_id',  $orddata->id)
            ->delete();
            \Log::info("extradata del end");
            for($i=0; $i<$cnt; $i++) {
                $sysordno       = $sysordno;
                $extra_cd       = $extradata[$i]['extra_cd'];
                $extra_nm       = isset($extradata[$i]['extra_nm']) ? $extradata[$i]['extra_nm'] : null;
                $extra_descp    = isset($extradata[$i]['extra_descp'])  ? $extradata[$i]['extra_descp'] : null;
                $amount         = isset($extradata[$i]['extra_amount'])  ? $extradata[$i]['extra_amount'] : null;
                $custfeedata    = array();

                $Totalamount = $Totalamount + $amount;
                // if(isset($extra_cd)){
                //     $custdata = DB::table('sys_customers')
                //     ->where('cust_no', $orddata->owner_cd)
                //     ->where('g_key', $user->g_key)
                //     ->first(); 
                //     if(isset($custdata)) {
                //         $custfeedata = DB::table('mod_cust_fee')
                //         ->where('fee_cd', $extra_cd)
                //         ->where('cust_id', $custdata->id)
                //         ->where('g_key', $user->g_key)
                //         ->first();
                //         if(isset($custfeedata)){
                //             $amount = $custfeedata->fee_amount;
                //             $Totalamount = $Totalamount + $custfeedata->fee_amount;
                //         }else{
                //             $custfeedata = DB::table('bscode')
                //             ->where('cd', $extra_cd)
                //             ->where('cd_type', "EXTRAFEE")
                //             ->where('g_key', $user->g_key)
                //             ->first(); 
                //             $amount = $custfeedata->value1;
                //             $Totalamount = $Totalamount + $custfeedata->value1;
                //         }
                //     } else {
                //         $custfeedata = DB::table('bscode')
                //         ->where('cd', $extra_cd)
                //         ->where('cd_type', "EXTRAFEE")
                //         ->where('g_key', $user->g_key)
                //         ->first(); 
                //         $amount = $custfeedata->value1;
                //         $Totalamount = $Totalamount + $custfeedata->value1;
                //     }

                // }
                \Log::info("collectamt update start");
                if(isset($orddata)){
                    \Log::info("content update start");
                    \Log::info($request->collectamt);
                    DB::table('mod_order')
                    ->where('sys_ord_no', $sysordno)
                    ->update(['collectamt' => $request->collectamt,'total_amount' => $Totalamount]);
                }
                \Log::info("collectamt update end");
                $proddetailid  = null;
                $feegoodsnm  = null;
                $detail =  DB::table('mod_order_detail')
                ->where('ord_id', $orddata->id)
                ->get();
                if(count($detail)==1){
                    if($detail[0]->pkg_num==1){
                        $proddetailid = $detail[0]->id."-1";
                        $feegoodsnm =$detail[0]->goods_no."(".$detail[0]->goods_nm.")"."-1";
                    }
                }
                // dd($proddetailid);
                $data = [
                    'ord_id'        => $orddata->id,
                    'fee_cd'        => $extra_cd,
                    'fee_name'      => $extra_nm,
                    'amount'      => $amount,
                    'fee_descp'      => $extra_descp,
                    'g_key'         => $user->g_key,
                    'c_key'         => $user->c_key,
                    's_key'         => $user->s_key,
                    'd_key'         => $user->d_key,
                    'created_by'    => $user->email,
                    'updated_by'    => $user->email,
                    'prod_detail_id'=> $proddetailid,
                    'fee_goods_nm'  => $feegoodsnm,
                    'created_at'    => Carbon::now()->toDateTimeString(),
			        'updated_at'    => Carbon::now()->toDateTimeString(),
                ];
                array_push($insertData, $data);
            }
            $OrderamtDetailModel = new OrderamtDetailModel();
            $OrderamtDetailModel->insertfee($insertData);   
        }
        catch(\Exception $e) {
            \Log::info("ordconfrimnew error");
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(["msg"=>"error" ,'code'=>'01','data'=>null]);
        }
        $sysOrdNo = $request->sysOrdNo!= '' ? $request->sysOrdNo : $request->sys_ord_no;
        // $dlvNo    = $request->dlv_no;
        $img      = $request->img;
        $signImg  = $request->signImg;
        $lng      = $request->lng;
        $lat      = $request->lat;
        $dlvType  = $request->dlvType != '' ? $request->dlvType : $request->dlv_type;
        $insertData = array();
        $dlvinfo    = DB::table('mod_order')
        ->where('sys_ord_no', $sysOrdNo)
        ->first();
        $dlvNo = $dlvinfo->dlv_no;
        $dlvData = DB::table('mod_dlv')
                            ->select('status')
                            ->where('dlv_no', $dlvNo)
                            ->first();
        if($request->tab_status == 0){

        $ordStatus = 'FINISHED';

        if($dlvType == 'D') {

            $dlvStatus = DB::table('mod_dlv_plan')
                            ->select('status')
                            ->where('sys_ord_no', $sysOrdNo)
                            ->where('dlv_type', 'P')
                            ->where('dlv_no', $dlvNo)
                            ->first();
            if(isset($dlvStatus)) {
                if($dlvStatus->status != 'FINISHED') {
                    return response()->json(['msg' => '尚有提貨未完成，請先完成提貨', 'code'=>'01']);
                }
            }
        }
        else {
            $ordStatus = 'PICKED';
            $orderforcar =  DB::table('mod_order')
            ->where('sys_ord_no', $sysOrdNo)
            ->first();
        }
        }
        
        try {
            \Log::info("img start");
            \Log::info("imgnum start");
            DB::table('mod_order')
            ->where('sys_ord_no', $sysOrdNo)
            ->update(['imgnum' =>  $request->imgnum]);
            \Log::info("imgnum end");
            if(is_array($img)) {
                \Log::info("real FINISH img start");
                for($i=0; $i<count($img); $i++) {
                    $guid = $this->guid();
                    Storage::disk('local')->put($guid.'.jpg', base64_decode($img[$i]));
                    $checkcount = DB::table('mod_file')
                    ->where('ref_no', $ordData->sys_ord_no)
                    ->where('guid', $guid.'.jpg')
                    ->where('type_no', 'FINISH')
                    ->count();
                    if($checkcount<1){
                        array_push($insertData, [
                            'guid'        => $guid.'.jpg',
                            'ref_no'      => $sysOrdNo,
                            'type_no'     => 'FINISH',
                            'file_format' => 'jpg',
                            'g_key'       => $user->g_key,
                            'c_key'       => $user->c_key,
                            's_key'       => $user->s_key,
                            'd_key'       => $user->d_key,
                            'created_by'  => $user->email,
                            'updated_by'  => $user->email,
                            'created_at'  => Carbon::now()->toDateTimeString(),
                            'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                    }
                }
            }
    
            if(isset($signImg)) {
                \Log::info("real sign img start");
                $guid = $this->guid();
                Storage::disk('local')->put($guid.'.jpg', base64_decode($signImg));
                $imgsize = $this->getBase64ImageSize($img);
                \Log::info('sign'.$imgsize);
                if($imgsize>=2500){
                    $imgtype = "Y";
                }
                $checkcount = DB::table('mod_file')
                ->where('ref_no', $ordData->sys_ord_no)
                ->where('guid', $guid.'.jpg')
                ->where('type_no', 'SIGNIN')
                ->count();
                if($checkcount<1){
                    array_push($insertData, [
                        'guid'        => $guid.'.jpg',
                        'ref_no'      => $sysOrdNo,
                        'type_no'     => 'SIGNIN',
                        'file_format' => 'jpg',
                        'empty_img'   => 'N',
                        'g_key'       => $user->g_key,
                        'c_key'       => $user->c_key,
                        's_key'       => $user->s_key,
                        'd_key'       => $user->d_key,
                        'created_by'  => $user->email,
                        'updated_by'  => $user->email,
                        'created_at'  => Carbon::now()->toDateTimeString(),
                        'updated_at'  => Carbon::now()->toDateTimeString()
                    ]);
                }else{
                    DB::table('mod_file')
                    ->where('ref_no', $ordData->sys_ord_no)
                    ->where('guid', $guid.'.jpg')
                    ->where('type_no', 'SIGNIN')
                    ->update([
                        'empty_img'   => $imgtype,
                        'updated_by'  => $user->email,
                        'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                }
            }
            
            $signPic = 0;
            \Log::info($insertData);
            if(count($insertData) > 0) {
                $signPic = 1;
                DB::table('mod_file')->insert($insertData);

                // DB::table('mod_order')
                //     ->where('sys_ord_no', $sysOrdNo)
                //     ->update(['sign_pic' => $signPic]);
            }
            \Log::info("end");
            if($request->tab_status >= 1){
                \Log::info("repair end");
                return response()->json(["msg"=>"repair success", "code"=>"00" , "data"=> null ]);
            }
            $ordModel = new OrderMgmtModel;
            if($dlvType == "D") {
                $ordModel->updateOrdStatusAndFinishDate($sysOrdNo, $ordStatus);
            }

            $orderdata = DB::table('mod_order')
            ->where('sys_ord_no',$sysOrdNo)
            ->first();
            DB::table('mod_order')
                ->where('sys_ord_no', $sysOrdNo)
                ->update(['status' => $ordStatus]);

            DB::table('mod_dlv_plan')
                ->where('sys_ord_no', $sysOrdNo)
                ->where('dlv_type', $dlvType)
                ->where('dlv_no', $dlvNo)
                ->update([
                    'user_name'   => $name,
                    'user_phone'  => $phone,
                    'status' => 'FINISHED', 'updated_at' => Carbon::now()->toDateTimeString(), 'finish_date' => Carbon::now()->toDateTimeString()]);

            $cnt  = DB::connection('mysql::write')->table('mod_dlv_plan')->where('dlv_no', $dlvNo)->count();
            $fCnt = DB::connection('mysql::write')->table('mod_dlv_plan')->where('dlv_no', $dlvNo)->whereRaw("(status = 'FINISHED' or status = 'ERROR' or status='REJECT')")->count();
            if($cnt == $fCnt) {
                DB::table('mod_dlv')
                    ->where('dlv_no', $dlvNo)
                    ->update(['status' => 'FINISHED','finished_time' => Carbon::now()->toDateTimeString()]);
            }            

            $dlvPlanData = DB::connection('mysql::write')->table('mod_dlv_plan')
                            ->where('sys_ord_no', $sysOrdNo)
                            ->where('dlv_type', $dlvType)
                            ->where('dlv_no', $dlvNo)
                            ->first();

            $dlvData = DB::connection('mysql::write')->table('mod_dlv')
                            ->where('dlv_no', $dlvNo)
                            ->where('g_key', $user->g_key)
                            ->where('c_key', $user->c_key)
                            ->first();

            $addr = null;
            $custNm = null;
            $driverNm = $user->email;

            if(isset($dlvData)) {
                $addr   = isset($dlvPlanData->addr) ? $dlvPlanData->addr : '';
                $custNm = isset($dlvPlanData->cust_nm) ? $dlvPlanData->cust_nm : '';
            }

            if(isset($dlvData)) {
                $driverNm = $dlvData->driver_nm;
            }
            $insertData = array();
            $TrackingModel = new TrackingModel();
            //$ModTrackingModel = new ModTrackingModel();
            \Log::info("gps start");
            if($dlvType == 'D') {
                $tm = new TrackingModel();
                $tm->insertTracking('', '', '', $sysOrdNo, '', 'E', 5, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
            }
                $gps = $request->gps;
                \Log::info($gps);
                $lng       = $gps['lng'];
                $lat       = $gps['lat'];
                $speed     = $gps['speed'];
                $timestamp = $gps['timestamp'];

                $data = [
                    'car_no'     => $user->email,
                    'lat'        => $lat,
                    'lng'        => $lng,
                    'driver_no'  => null,
                    'driver_nm'  => $driverNm,
                    'person_no'  => null,
                    'person_nm'  => null,
                    'dlv_no'     => null,
                    'g_key'      => $user->g_key,
                    'c_key'      => $user->c_key,
                    's_key'      => $user->s_key,
                    'd_key'      => $user->d_key,
                    'created_by' => $user->email,
                    'updated_by' => $user->email,
                    'location_time' => $timestamp,
                    'speed'      => $speed,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    'status'     => 'FINISHED',
                    'cust_nm'    => $custNm,
                    'addr'       => $addr,
                ];

                array_push($insertData, $data);
    
                $TrackingModel->insertGps($insertData);
               // $ModTrackingModel->insertGps($insertData);
                $userData = DB::table('users')
                ->where('id', $user->id)
                ->update(['gps_time' => Carbon::now()->toDateTimeString()]);
            \Log::info("gps end");
            $TrackingModel->sendDlvMail($sysOrdNo, $dlvType); //先發配送完成，直到取貨mail完成
			\Log::info("confirm_new  finished");
			$ordsendbnqdata = DB::table('mod_order')
            ->where('sys_ord_no',$sysOrdNo)
            ->first();
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            \Log::info($request->all());
            return response()->json(['msg' => 'error', 'code'=>'01', 'data'=> null ]);
        }

        return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
    }

    public function stdcheckversion(Request $request) {
        \Log::info("stdcheckversion");
        \Log::info($request->all());
        $user       = Auth::user();
        if($request->phone_type!="ios") {
            $appver = DB::table('bscode')->where('g_key',$user->g_key)->where('cd_type','appversion')->where('cd','appversion')->first();
            if($request->appversion != $appver->value1 && $request->appversion != $appver->value2 ){
                return response()->json(['msg' => '版本不相符 請更新app版本', "data"=>$appver->cd_descp,"code"=>"01"]);
            } else {
                return response()->json(['msg' => '當前已是最新版本',"code"=>"00"]);
            }
        } else {
            $appver = DB::table('bscode')->where('g_key',$user->g_key)->where('cd_type','appversion')->where('cd','iosversion')->first();
            if($request->appversion != $appver->value1 && $request->appversion != $appver->value2 ){
                return response()->json(['msg' => '版本不相符 請更新app版本', "url"=>$appver->cd_descp,"code"=>"01"]);
            }else{
                return response()->json(['msg' => '當前已是最新版本',"code"=>"00"]);
            }
        }
    }

    public function stderrorReport_JSON(Request $request) {
        \Log::info("stderrorReport_JSON");
        \Log::info($request->all());
        
        $user           = Auth::user();
        $sysOrdNo       = isset($request->sysOrdNo) ? $request->sysOrdNo : $request->sys_ord_no;
        $dlvNo          = isset($request->dlvNo) ? $request->dlvNo : $request->dlv_no;
        $errorType      = isset($request->errorType) ? $request->errorType : $request->error_type;
        $dlvType        = isset($request->dlvType) ? $request->dlvType : $request->dlv_type;
        $gps            = $request->gps;
        $abnormalRemark = isset($request->abnormalRemark) ? $request->abnormalRemark : $request->abnormal_remark;

        if(empty($errorType)) {
            return response()->json(['data' => null, 'msg' => '請選擇回報項目','code'=>'01']);
        }

        $insertData     = array();
        $checkdlvorder  = DB::table('mod_order')
        ->where('sys_ord_no', $sysOrdNo)
        ->where('dlv_no', $dlvNo)
        ->count();
        if($checkdlvorder<=0){
            return response()->json(['msg' => 'success','code'=>'00', 'data'=> null ]);
        }
        
        $dlvData = DB::table('mod_dlv')
                            ->select('status')
                            ->where('dlv_no', $dlvNo)
                            ->first();
        if($request->tab_status == 0){
            if(isset($dlvData) && $dlvData->status == "FINISHED") {
                return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
            }
            if(isset($dlvData) && $dlvData->status != "DLV") {
                return response()->json(['data' => null, 'msg' => '還沒出發不能回報異常，請先按下出發','code'=>'01']);
            }
    
            if($dlvType == 'D') {
    
                $dlvStatus = DB::table('mod_dlv_plan')
                                ->select('status')
                                ->where('sys_ord_no', $sysOrdNo)
                                ->where('dlv_type', 'P')
                                ->where('dlv_no', $dlvNo)
                                ->first();
                if(isset($dlvStatus)) {
                    if($dlvStatus->status != 'FINISHED' && $dlvStatus->status != 'ERROR') {
                        return response()->json(['data' => null, 'msg' => '尚有提貨未完成，請先完成提貨','code'=>'01']);
                    }
                }
            }

        }
        
        try {
            $errorPic = 0;
            if(count($insertData) > 0) {
                $errorPic = 1;
                DB::table('mod_file')->insert($insertData);

                DB::table('mod_order')
                    ->where('sys_ord_no', $sysOrdNo)
                    ->update(['error_pic' => $errorPic]);
            }
            $errorStatus = "ERROR";
            $errordesc = "配送發生問題";
            if($errorType=="A999"||$errorType=="A093"){
                $errorStatus ="REJECT";
                $errordesc = "拒收";
            }
            if($errorType=="PC14"||$errorType=="PC11"){
                $errorStatus ="FINISHED";
                $errordesc = "配送完成";
            }
            $bscode = array();
            $custowner = DB::table('mod_order')
            ->where('sys_ord_no', $sysOrdNo)
            ->first();
            $owner_cd =  DB::table('sys_customers')
            ->where('cust_no', $custowner->owner_cd)
            ->where('g_key', $user->g_key)
            ->first();
            if(!isset($owner_cd)){
                $thisQuery = DB::table('bscode');
                $thisQuery->where('cd_type', "ERRORTYPE");
                $thisQuery->where('cd', $errorType);
                $thisQuery->where('g_key', $user->g_key);
                $thisQuery->where('c_key', $user->c_key);
                $bscode = $thisQuery->first();
            }else{
                $bscode= DB::table('mod_cust_error')
                ->where('cust_id', $owner_cd->id)
                ->first();
                if(!isset($bscode)){
                    $thisQuery = DB::table('bscode');
                    $thisQuery->where('cd_type', "ERRORTYPE");
                    $thisQuery->where('cd', $errorType);
                    $thisQuery->where('g_key', $user->g_key);
                    $thisQuery->where('c_key', $user->c_key);
                    $bscode = $thisQuery->first();
                }else{
                    $errorStatus =  $bscode->order_status;
                    if( $errorStatus=="REJECT"){
                        $errordesc = "拒收";
                    }else if($errorStatus=="FINISHED") {
                        $errordesc = "配送完成";
                    }else{
                        $errordesc = "配送發生問題";
                    }
                }
            }

            $cdDescp = "";;
            if(isset($bscode)) {
                $cdDescp = $bscode->cd_descp;
            }
            if($request->tab_status >= 1){
                DB::table('mod_order')
                ->where('sys_ord_no', $sysOrdNo)
                ->update(['error_remark' => $cdDescp, 'exp_reason' => $errorType, 'abnormal_remark' => $abnormalRemark]);
                if($dlvType == 'P') {
                    DB::table('mod_dlv_plan')
                        ->where('sys_ord_no', $sysOrdNo)
                        ->where('dlv_no', $dlvNo)
                        ->update(['error_cd' => $errorType,'error_descp' => $cdDescp, 'abnormal_remark' => $abnormalRemark]);
                }
                else {
                    DB::table('mod_dlv_plan')
                        ->where('sys_ord_no', $sysOrdNo)
                        ->where('dlv_no', $dlvNo)
                        ->where('dlv_type', $dlvType)
                        ->update(['error_cd' => $errorType,'error_descp' => $cdDescp, 'abnormal_remark' => $abnormalRemark]);
                }
                return response()->json(['msg' => 'repair success','code'=>'00']);
            }
            if( ($errorType== "A001" || $errorType== "A072") && $user->g_key=="SYL"){
                $pickCnt = DlvPlanModel::where('sys_ord_no', $sysOrdNo)
                ->where('dlv_type', 'P')
                ->count();
                //先判斷 是提貨還是配送 如果是提貨 刷成尚未安排 如果是配送則判斷目前總攬裡面是否有提貨資訊 有的話 刷成已提貨 沒有就刷成尚未安排
                if($dlvType == 'P') {
                    DB::table('mod_order')
                    ->where('sys_ord_no', $sysOrdNo)
                    ->update([
                        'updated_at'      => Carbon::now()->toDateTimeString(),
                        'status'          => 'UNTREATED',
                        'status_desc'     => '尚未安排',
                        'dlv_no'          => null,
                        'error_remark'    => $cdDescp,
                        'exp_reason'      => $errorType,
                        'dlv_type'        => null,
                        'abnormal_remark' => $abnormalRemark
                    ]);
                }else{
                    if($pickCnt > 0) {
                        DB::table('mod_order')
                        ->where('sys_ord_no', $sysOrdNo)
                        ->update(['updated_at' => Carbon::now()->toDateTimeString(), 'status' => 'PICKED', 'status_desc' => '已提貨', 'dlv_no' => null, 'error_remark' => $cdDescp, 'exp_reason' => $errorType, 'dlv_type' => 'P', 'abnormal_remark' => $abnormalRemark]);
                        $orderdata = DB::table('mod_order')
                        ->where('sys_ord_no',$sysOrdNo)
                        ->first();
                    }else{
                        DB::table('mod_order')
                        ->where('sys_ord_no', $sysOrdNo)
                        ->update(['updated_at' => Carbon::now()->toDateTimeString(),'status' => 'UNTREATED', 'status_desc' => '尚未安排', 'dlv_no' => null, 'error_remark' => $cdDescp, 'exp_reason' => $errorType, 'dlv_type' => null, 'abnormal_remark' => $abnormalRemark]);
                    }
                }
            }else{
                DB::table('mod_order')
                ->where('sys_ord_no', $sysOrdNo)
                ->update([ 'updated_at' => Carbon::now()->toDateTimeString(), 'status' => $errorStatus, 'status_desc' => $errordesc, 'error_remark' => $cdDescp, 'exp_reason' => $errorType, 'dlv_type' => null, 'abnormal_remark' => $abnormalRemark]);
            }
            if($dlvType == 'P') {
                DB::table('mod_dlv_plan')
                    ->where('sys_ord_no', $sysOrdNo)
                    ->where('dlv_no', $dlvNo)
                    ->update(['updated_at' => Carbon::now()->toDateTimeString(), 'status' => $errorStatus,'error_cd' => $errorType,'error_descp' => $cdDescp,'abnormal_remark' => $abnormalRemark, 'updated_at' => Carbon::now()->toDateTimeString(), 'finish_date' => Carbon::now()->toDateTimeString()]);
            }
            else {
                DB::table('mod_dlv_plan')
                    ->where('sys_ord_no', $sysOrdNo)
                    ->where('dlv_no', $dlvNo)
                    ->where('dlv_type', $dlvType)
                    ->update([ 'updated_at' => Carbon::now()->toDateTimeString(), 'status' => $errorStatus,'error_cd' => $errorType,'error_descp' => $cdDescp,'abnormal_remark' => $abnormalRemark, 'updated_at' => Carbon::now()->toDateTimeString(), 'finish_date' => Carbon::now()->toDateTimeString()]);
            }
            $dlvData = DB::table('mod_dlv')
            ->select('status','driver_nm')
            ->where('dlv_no', $dlvNo)
            ->first();
            $dlvPlanData = DB::table('mod_dlv_plan')
            ->where('sys_ord_no', $sysOrdNo)
            ->where('dlv_type', $dlvType)
            ->where('dlv_no', $dlvNo)
            ->first();
            $addr = null;
            $custNm = null;
            $driverNm = $user->email;
            if(isset($dlvData)) {
                $addr = $dlvPlanData->addr;
                $custNm = $dlvPlanData->cust_nm;
            }

            if(isset($dlvData)) {
                $driverNm = $dlvData->driver_nm;
            } 
            $insertData = array();
            $TrackingModel = new TrackingModel();
            //$ModTrackingModel = new ModTrackingModel();
            // $tm = new TrackingModel();
            // $tm->insertTracking('', '', '', $sysOrdNo, '', 'E', 5, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
            $cnt = count($gps);
            $gps = $request->gps;
            \Log::info($gps);
            $lng       = $gps['lng'];
            $lat       = $gps['lat'];
            $speed     = $gps['speed'];
            $timestamp = $gps['timestamp'];

            $data = [
                'car_no'     => $user->email,
                'lat'        => $lat,
                'lng'        => $lng,
                'driver_no'  => null,
                'driver_nm'  => $driverNm,
                'person_no'  => null,
                'person_nm'  => null,
                'dlv_no'     => null,
                'g_key'      => $user->g_key,
                'c_key'      => $user->c_key,
                's_key'      => $user->s_key,
                'd_key'      => $user->d_key,
                'created_by' => $user->email,
                'updated_by' => $user->email,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
                'location_time' => $timestamp,
                'speed'      => $speed,
                'status'     => 'FINISHED',
                'cust_nm'    => $custNm,
                'addr'       => $addr,
            ];

            array_push($insertData, $data);

            $TrackingModel->insertGps($insertData);
			if($errorStatus=="REJECT"){
				$TrackingModel->insertTracking('', '', '', $sysOrdNo, '', 'E', 5, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
			}

            $userData = DB::table('users')
            ->where('id', $user->id)
            ->update(['gps_time' => Carbon::now()->toDateTimeString()]);
            $ordModel = new OrderMgmtModel;
            $orderdata = DB::table('mod_order')
            ->where('sys_ord_no',$sysOrdNo)
            ->first();
            if(isset($orderdata)) {
                $ordall = DB::table('mod_order')
                ->where('sys_ord_no',$sysOrdNo)
                ->first();
                $tsrecord = DB::table('mod_ts_order')
                ->where('sys_ord_no', $sysOrdNo)
                ->where('status_desc', $ordall->status_desc)
                ->where('owner_cd', $ordall->owner_cd)
                ->count();
                $custdata = DB::table('sys_customers')
                ->where('cust_no', $ordall->owner_cd)
                ->whereIn('ftptype',["E","EM"])
                ->count();
                if($custdata!=0 && $errorStatus!="REJECT"){
                    DB::table('mod_ts_order')
                    ->insert([
                        "owner_cd" => $ordall->owner_cd,
                        "owner_nm" => $ordall->owner_nm,
                        "ord_no"   => $ordall->ord_no,
                        "cust_ord_no"   => $ordall->cust_ord_no, 
                        "wms_ord_no"   => $ordall->wms_order_no,
                        "sys_ord_no" => $ordall->sys_ord_no,
                        "ord_created_at"   => $ordall->created_at,
                        "status_desc" => "配送發生問題",
                        "finish_date"   => Carbon::now(),
                        "exp_reason" => $errorType,
                        "error_remark" => $cdDescp,
                        "status" => $ordall->status,
                        "abnormal_remark" => $abnormalRemark,
                        "is_send"     => "N",
                        "updated_at"  => Carbon::now(),
                        "updated_by"  => "SYSTEM",
                        "created_at"  => Carbon::now(),
                        "created_by"  => "SYSTEM",
                        'g_key'      => $user->g_key,
                        'c_key'      => $user->c_key,
                        's_key'      => $user->s_key,
                        'd_key'      => $user->d_key,
                    ]);
                }
            }
            if($errorStatus=="REJECT"){
                $ordModel->updateOrdStatusAndFinishDate($sysOrdNo, $errorStatus);
            }
            $cnt  = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->count();
            $fCnt = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->whereRaw("(status = 'FINISHED' or status = 'ERROR' or status = 'REJECT')")->count();

            if($cnt == $fCnt) {
                DB::table('mod_dlv')
                    ->where('dlv_no', $dlvNo)
                    ->update(['status' => 'FINISHED','finished_time' => Carbon::now()->toDateTimeString()]);
            }
            $ordsendbnqdata = DB::table('mod_order')
            ->where('sys_ord_no',$sysOrdNo)
            ->first();
			\Log::info("sure finised");
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            \Log::info($request->all());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }

        return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
    }
    public function stddeparturenew($dlvNo) {
        try {
            \Log::info("stddeparturenew");
            \Log::info($dlvNo);
            $user = Auth::user();
            $lng = request('lng');
            $lat = request('lat');
            $name = request('name');
            $phone = request('phone');
            $dlvNo      = substr($dlvNo,0,13);
            DB::table('mod_order')
                ->where('dlv_no', $dlvNo)
                ->update(['status' => 'SETOFF','status_desc' => '出發', 'updated_at' => Carbon::now()->toDateTimeString()]);
            $ordData = DB::table('mod_dlv_plan')->select('sys_ord_no')
            ->where('status',"!=", "ERROR")
            ->where('status',"!=", "FINISHED")
            ->where('status',"!=", "REJECT")
            ->where('dlv_no', $dlvNo)
            ->get();
            //確認出發時派車單內是否還有任務
            //有的話運送中 沒有的直接完成
            if(count($ordData) > 0) {
                DB::table('mod_dlv')
                ->where('dlv_no', $dlvNo)
                ->update(['status' => 'DLV', 'updated_at' => Carbon::now()->toDateTimeString(), 'updated_by' => $user->email, 'departure_time' => Carbon::now()->toDateTimeString()]);
            }else{
                DB::table('mod_dlv')
                ->where('dlv_no', $dlvNo)
                ->update(['status' => 'FINISHED', 'updated_at' => Carbon::now()->toDateTimeString(),  'updated_by' => $user->email, 'departure_time' => Carbon::now()->toDateTimeString()]);
            }
            $checkorddata = DB::table('mod_order')
            ->where('dlv_no', $dlvNo)
            ->get();
            try{
                foreach($checkorddata as $ordrow) {
                    if($ordrow->is_ordered=="Y"){
                        DB::table('mod_dlv_plan')
                        ->where('sys_ord_no', $ordrow->sys_ord_no)
                        ->where('dlv_type', 'P')
                        ->update([
                            'user_name'   => $name,
                            'user_phone'  => $phone,
                            'status' => 'FINISHED' ,
                            'updated_by' => $user->email ,
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ]);
                    }
                }
            }
            catch(\Exception $e) {
                \Log::info($e->getLine());
                \Log::info($e->getMessage());
                return response()->json(['msg' => 'error', 'data'=> null, 'code' => '01' ]);
            }
            //派車單任務狀態 出發 
            DB::table('mod_dlv_plan')
                ->where('dlv_no', $dlvNo)
                ->where('c_key', $user->c_key)
                ->where('status',"!=", "ERROR")
                ->where('status',"!=", "FINISHED")
                ->where('status',"!=", "REJECT")
                ->update([
                    'user_name'   => $name,
                    'user_phone'  => $phone,
                    'status' => 'DLV', 
                    'updated_by' => $user->email , 
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]);
            if(count($ordData) > 0) {
                $issend = "N";
                foreach($ordData as $row) {
                    $sysOrdNo = $row->sys_ord_no;
                    $ordModel = new OrderMgmtModel;
                    $orderdata = DB::table('mod_order')->where('sys_ord_no',$sysOrdNo)->first();
                    if($orderdata->dlv_no==$dlvNo){

                        DB::table('mod_order')
                            ->where('sys_ord_no', $sysOrdNo)
                            ->where('status',"!=", "ERROR")
                            ->where('status',"!=", "FINISHED")
                            ->where('status',"!=", "REJECT")
                            ->update(['status' => 'SETOFF','status_desc' => '出發', 'updated_by' => $user->email , 'updated_at' => Carbon::now()->toDateTimeString()]);
                        if($orderdata->is_ordered=="Y")
                        {
                            $ordModel = new OrderMgmtModel;
                            $ordModel->updateOrdStatusAndFinishDate($sysOrdNo, 'PICKED');
                            DB::table('mod_dlv_plan')
                            ->where('sys_ord_no', $sysOrdNo)
                            ->where('dlv_no', $dlvNo)
                            ->where('dlv_type','P')
                            ->where('status',"!=", "ERROR")
                            ->where('status',"!=", "FINISHED")
                            ->where('status',"!=", "REJECT")
                            ->update([
                                'user_name'   => $name,
                                'user_phone'  => $phone,
                                'status' => 'FINISHED' , 
                                'updated_by' => $user->email , 
                                'updated_at' => Carbon::now()->toDateTimeString(),
                                'finish_date'=>Carbon::now()->toDateTimeString()
                            ]);
                        }
                        $ordNo = DB::table('mod_order')->select('ord_no')->where('sys_ord_no', $sysOrdNo)->first();
                        $orddata = DB::table('mod_order')->select('id')->where('sys_ord_no', $sysOrdNo)->first();
                        $tm = new TrackingModel();
                        $tm->insertTracking('', $ordNo->ord_no, '', $sysOrdNo, '', 'B', 4, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
                    }
                }
            }


            $insertData = [
                'car_no'        => $user->email,
                'lat'           => $lat,
                'lng'           => $lng,
                'driver_no'     => null,
                'driver_nm'     => $user->name,
                'person_no'     => null,
                'person_nm'     => null,
                'dlv_no'        => null,
                'g_key'         => $user->g_key,
                'c_key'         => $user->c_key,
                's_key'         => $user->s_key,
                'd_key'         => $user->d_key,
                'created_by'    => $user->email,
                'updated_by'    => $user->email,
                'location_time' => Carbon::now()->toDateTimeString(),
                'speed'         => '0',
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(),
                'status'        => 'SETOFF'
            ];

            $TrackingModel = new TrackingModel();
            $TrackingModel->insertGps($insertData);
            $userData = DB::table('users')
            ->where('id', $user->id)
            ->update(['gps_time' => Carbon::now()->toDateTimeString()]);
        }
        catch(\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'data'=> null, 'code' => '01' ]);
        }
        DB::table('mod_order')
        ->where('dlv_no', $dlvNo)
        ->update(['status' => 'SETOFF','status_desc' => '出發', 'updated_at' => Carbon::now()->toDateTimeString()]);
        return response()->json(['msg' => 'success', 'data'=> null, 'code' => '00' ]);
    }

    public function stdsendpickorder(Request $request) {
        \Log::info("stdsendpickorder");
        \Log::info($request->all());
        $user = Auth::user();
        \Log::info("點貨");
        $now = date("Y-m-d H:i:s");
        $orderdata = array();
        $order_tally = "";
        $insertData = array();
        try {
            if(isset($request->pickeddata)) {
                \Log::info($request->pickeddata);
                $orderdata = $request->pickeddata;
                $cnt = count($orderdata);
                for($i=0; $i<$cnt; $i++) {
                    $realnum       = isset($orderdata[$i]['real_num']) ?  $orderdata[$i]['real_num'] : $orderdata[$i]['realnum'];
                    $orderednum    = isset($orderdata[$i]['ord_num']) ?  $orderdata[$i]['ord_num'] : $orderdata[$i]['orderednum'];
                    $sys_ord_no    = $orderdata[$i]['sys_ord_no'];
                    $orddata       = DB::table('mod_order')->where('sys_ord_no', $sys_ord_no)->where('car_no', $user->email)->orderBy('created_at', 'desc')->first();
                    if(!isset($orddata)) {
                        $orddata   = DB::table('mod_order')->where('ord_no', $sys_ord_no)->where('car_no', $user->email)->orderBy('created_at', 'desc')->first();
                    }
                    $sys_ord_no = $orddata->sys_ord_no;
                    \Log::info($orddata->sys_ord_no);
                    $dlvdata       = DB::table('mod_dlv')->where('dlv_no', $orddata->dlv_no)->orderBy('created_at', 'desc')->first();
                    $dlvplandata       = DB::table('mod_dlv_plan')->where('dlv_no', $orddata->dlv_no)->where('dlv_type', 'P')->where('sys_ord_no', $sys_ord_no)->orderBy('created_at', 'desc')->first();
                    if($realnum==$orderednum){
                        $order_tally = "Y";
                        DB::table('mod_order')
                        ->where('sys_ord_no', $sys_ord_no)
                        ->update(['is_ordered' => 'Y','realnum' => $realnum,'order_tally' =>'Y']);
                    }
                    else{
                        $order_tally = "N";
                        if($orderdata[$i]['singlepost']=="Y"){
                            //
                            DB::table('mod_order')
                            ->where('sys_ord_no', $sys_ord_no)
                            ->update(['is_ordered' => 'Y','realnum' => $realnum,'order_tally' =>'N' ]);
                        }else{
                            //數量相同才要寫
                            DB::table('mod_order')
                            ->where('sys_ord_no', $sys_ord_no)
                            ->update(['realnum' => $realnum,'order_tally' =>'N' ]);
                        }
                    }
                    if(isset($dlvdata)){
                        if($dlvdata->status == "DLV") {
                            $pickdlvcount = DB::table('mod_dlv_plan')->where('dlv_no', $orddata->dlv_no)->where('dlv_type', 'P')->where('sys_ord_no', $sys_ord_no)->orderBy('created_at', 'desc')->count();
                            if($pickdlvcount >0){
                                $ordModel = new OrderMgmtModel;
                                $ordModel->updateOrdStatusAndFinishDate($sys_ord_no, 'PICKED');
                            }
                            DB::table('mod_dlv_plan')
                            ->where('sys_ord_no', $sys_ord_no)
                            ->where('dlv_no', $orddata->dlv_no)
                            ->where('dlv_type','P')
                            ->update(['status' => 'FINISHED','finish_date'=>Carbon::now()->toDateTimeString()]);
                        }
                    }
                    if($orderdata[$i]['singlepost']=="Y"){
                        array_push($insertData, [
                            'ord_id'      => $orddata->id,
                            'dlv_no'      => $orddata->dlv_no,
                            'order_tally' => $order_tally,
                            'real_num'    => $realnum,
                            'ord_num'     => $orderednum,
                            'user_name'   => $request->name,
                            'user_phone'  => $request->phone,
                            'g_key'       => $user->g_key,
                            'c_key'       => $user->c_key,
                            's_key'       => $user->s_key,
                            'd_key'       => $user->d_key,
                            'created_by'  => $user->email,
                            'created_at'  => Carbon::now()->toDateTimeString(),
                            'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                    }
                    if($orderdata[$i]['singlepost']=="N" && $realnum==$orderednum){
                        array_push($insertData, [
                            'ord_id'      => $orddata->id,
                            'dlv_no'      => $orddata->dlv_no,
                            'order_tally' => $order_tally,
                            'real_num'    => $realnum,
                            'ord_num'     => $orderednum,
                            'user_name'   => $request->name,
                            'user_phone'  => $request->phone,
                            'g_key'       => $user->g_key,
                            'c_key'       => $user->c_key,
                            's_key'       => $user->s_key,
                            'd_key'       => $user->d_key,
                            'created_by'  => $user->email,
                            'created_at'  => Carbon::now()->toDateTimeString(),
                            'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                    }
                }
            }
            DB::table('mod_order_goods_check')->insert($insertData);          
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code' => '01' ]);
        }
        \Log::info("success");
        return response()->json(['msg' => 'success', 'data'=> null, 'code' => '00' ]);
    }

    public function stdgetorderinfo() {
        try{
            \Log::info("stdgetorderinfo");
            $user = Auth::user();
            $status =['FINISHED','ERROR','REJECT'];
            $data = []; 
            $this_query = DB::table('mod_order');
            $this_query->select(
                'mod_order.sys_ord_no as sys_ord_no',
                'mod_order.realnum as real_num',
                DB::raw('(select sum(pkg_num) from mod_order_detail where ord_id = mod_order.id )as ord_num'),
                'mod_order.ord_no as ord_no'
            );
            $this_query->where('car_no', $user->email);
            $this_query->where('is_ordered','!=', 'Y');
            $this_query->whereNotIn('status',$status);
            $this_query->where('dlv_no','!=','');
            $this_query->orderBy('created_at', 'desc');
            $data = $this_query->get(); 

            foreach ($data as $key => $row) {
                $row->ord_num = (int)$row->ord_num ;
            }
            \Log::info($data);
        }      
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'data'=> null, 'code' => '01' ]);
        }          
        return response()->json(['msg' => 'success', 'data'=> $data, 'code' => '00' ]);
    }

    public function stduploadSignImg() {
        $user   = Auth::user();
        $id = request('id');
        $img      = request('img');
        $fileName = request('fileName');
        $insertData = array();
        $imgtype = "N";
        \Log::info("stduploadSignImg");
        \Log::info($id);
        $ordData = DB::table('mod_order')->where('id', $id)->first();
        if(!isset($ordData)) {
            return response()->json(['msg' => 'success','code' => '00']);
        }
        \Log::info("upload ref_no is ".$ordData->sys_ord_no);
        try {
			 \Log::info("update order sign img");
			 DB::table('mod_order')
			->where('id', $id)
			->update(['sign_pic' => '1']);
			 \Log::info("update order sign end");
            $guid = $this->guid();
            if(!empty($fileName)) {
                Storage::disk('local')->put($fileName, base64_decode($img));
                $imgsize = $this->getBase64ImageSize($img);
                \Log::info($imgsize);
                \Log::info($fileName);
                if($imgsize>=2500){
                    $imgtype = "Y";
                }
                $checkcount = DB::table('mod_file')
                ->where('ref_no', $ordData->sys_ord_no)
                ->where('guid', $fileName)
                ->where('type_no', 'FINISH')
                ->count();
                if($checkcount==0){
                    array_push($insertData, [
                        'guid'        => $fileName,
                        'ref_no'      => $ordData->sys_ord_no,
                        'type_no'     => 'FINISH',
                        'file_format' => 'jpg',
                        'empty_img'   => $imgtype,
                        'g_key'       => $ordData->g_key,
                        'g_key'       => $ordData->g_key,
                        'c_key'       => $ordData->c_key,
                        's_key'       => $ordData->s_key,
                        'd_key'       => $ordData->d_key,
                        'created_by'  => $user->email,
                        'updated_by'  => $user->email,
                        'created_at'  => Carbon::now()->toDateTimeString(),
                        'updated_at'  => Carbon::now()->toDateTimeString()
                    ]);
                    DB::table('mod_file')->insert($insertData);
                    // $orderdetails = DB::table('mod_shippen_temp_detail')->where('customerpo',$ordData->ord_no)->first();
                    // if(isset($orderdetails)){
                    //     $years = substr($ordData->finish_date,0,4);
                    //     $months= substr($ordData->finish_date,5,2);
                    //     Storage::disk('azure')->put($years.'/'.$months.'/'.$ordData->ord_no.'.jpg', base64_decode($img));
                    // }
                }else{
                    DB::table('mod_file')
                    ->where('ref_no', $ordData->sys_ord_no)
                    ->where('guid', $fileName)
                    ->where('type_no', 'FINISH')
                    ->update([
                        'empty_img'   => $imgtype,
                        'updated_by'  => $user->email,
                        'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                }
				\Log::info("sure finised uploadSignImg");
                $signPic = 0;
                \Log::info("update signpiceure uploadSignImg");
                \Log::info($id);
                 DB::table('mod_order')
                    ->where('id', $id)
                    ->update(['sign_pic' => '1']);
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'data' => null,'code' => '01']);
        }

        return response()->json(['msg' => 'success', 'data'=> null, 'code' => '00' ]);
    }

    public function stduploadAbnormalImg(Request $request) {
        $user       = Auth::user();
        $id         = $request->id;
        $img        = $request->img;
        $fileName   = $request->fileName;
        $insertData = array();
        \Log::info("stduploadAbnormalImg");
        \Log::info($id);
        \Log::info($fileName);
        $ordData = DB::table('mod_order')->where('id', $id)->first();
        $imgtype = "N";
        try {
            $guid = $this->guid();

            if(!empty($fileName)) {
                Storage::disk('local')->put($fileName, base64_decode($img));
                $imgsize = $this->getBase64ImageSize($img);
                \Log::info($imgsize);
                if($imgsize>=2500){
                    $imgtype = "Y";
                }
                $checkcount = DB::table('mod_file')
                ->where('ref_no', $ordData->sys_ord_no)
                ->where('guid', $fileName)
                ->where('type_no', 'ERROR')
                ->count();
                if($checkcount<1){
                    array_push($insertData, [
                        'guid'        => $fileName,
                        'ref_no'      => $ordData->sys_ord_no,
                        'type_no'     => 'ERROR',
                        'file_format' => 'jpg',
                        'empty_img'   => $imgtype,
                        'g_key'       => $ordData->g_key,
                        'c_key'       => $ordData->c_key,
                        's_key'       => $ordData->s_key,
                        'd_key'       => $ordData->d_key,
                        'created_by'  => $user->email,
                        'updated_by'  => $user->email,
                        'created_at'  => Carbon::now()->toDateTimeString(),
                        'updated_at'  => Carbon::now()->toDateTimeString()
                    ]);
                }else{
                    DB::table('mod_file')
                    ->where('ref_no', $ordData->sys_ord_no)
                    ->where('guid', $fileName)
                    ->where('type_no', 'ERROR')
                    ->update([
                        'empty_img'   => $imgtype,
                        'updated_by'  => $user->email,
                        'updated_at'  => Carbon::now()->toDateTimeString()
                        ]);
                }
				
				\Log::info("sure error");
				if(($ordData->status=="FINISHED" || $ordData->status=="REJECT") && $ordData->owner_cd=="019"){
					\Log::info("sure finised benq");
                    // $orderdetails = DB::table('mod_shippen_temp_detail')->where('customerpo',$ordData->ord_no)->first();
                    // if(isset($orderdetails)){
                    //     DB::table('sys_benq_edi_log')->insert([
                    //         'sys_ord_no' => $sysOrdNo,
                    //         'count' => 0,
                    //         'status' => 'N',
                    //         'g_key' =>$user->g_key,
                    //         'c_key' =>$user->c_key,
                    //         's_key' =>$user->s_key,
                    //         'd_key' =>$user->d_key,
                    //         'created_by' => $user->email,
                    //         'created_at' => \Carbon::now()
                    //     ]);
                    // }
					\Log::info("sure error end");
				}else{
					\Log::info($ordData->status."-".$ordData->owner_cd);
				}
                
                $error_pic = 0;
                if(count($insertData) > 0) {
                    $error_pic = 1;
                    DB::table('mod_file')->insert($insertData);

                    DB::table('mod_order')
                        ->where('id', $id)
                        ->update(['error_pic' => $error_pic]);
                }
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'data'=> null, 'code' => '01' ]);
        }

        return response()->json(['msg' => 'success', 'data'=> null, 'code' => '00' ]);
    }

    public function stdupdateSnNo_new(Request $request) {
        \Log::info("stdupdateSnNo_new");
        \Log::info($request->all());
        $snNo = $request->snNo;
        $id   = $request->id;
        $teestnewsn = "";
        $user = Auth::user();
        try {
            $insertData = array();

            $newSnNo = str_replace( array("\n", "\r"),",",$snNo);
            $newSnNo = explode(",",$newSnNo);
            for($i= 0 ; $i<count($newSnNo); $i++){
                if($newSnNo[$i]!=""){
                    $teestnewsn=$teestnewsn.$newSnNo[$i].',';
                }
            }
            // $imSnNo =  str_replace( array("\n", "\r"),",",$snNo);
            $imSnNo = rtrim($teestnewsn,',');
            \Log::info("updateSnNo result");
            \Log::info($imSnNo);
            // \Log::info($imSnNo);
            DB::table('mod_order_detail')
            ->where('id', $id)
            ->update([
                'sn_no' => $imSnNo,
                'updated_at' => Carbon::now()->toDateTimeString(),
                'updated_by' => $user->email
            ]);

            $user = Auth::user();
            try{
                $detailData = DB::connection('mysql::write')->table('mod_order_detail')
                ->where('id', $id)
                ->first();

                $orddata = DB::connection('mysql::write')->table('mod_order')
                ->where('id', $detailData->ord_id)
                ->first();
                
                DB::table('mod_box_sn')
                ->where('sys_ord_no', $orddata->sys_ord_no)
                ->where('ord_detail_id', $id)
                ->delete();

                $explodeSnArray = explode(',', $detailData->sn_no);
                foreach ($explodeSnArray as $explodekey => $value) {
                    $data = [
                        'sys_ord_no'    => $orddata->sys_ord_no,
                        'ord_detail_id' => $id,
                        'sn_no'         => $value,
                        'g_key'         => $user->g_key,
                        'c_key'         => $user->c_key,
                        's_key'         => $user->s_key,
                        'd_key'         => $user->d_key,
                        'created_by'    => $user->email,
                        'updated_by'    => $user->email,
                        'created_at'    => \Carbon::now(),
                        'updated_at'    => \Carbon::now(),
                    ];
                    array_push($insertData, $data);
                }

                DB::table('mod_box_sn')->insert($insertData);

                DB::table('mod_sn_history')->insert([
                    'ord_id' 		=> $id,
                    'snno' 			=> $imSnNo,
                    'created_by' 	=> $user->email,
                    'updated_by' 	=> $user->email,
                    'created_at' 	=> \Carbon::now(),
                    'updated_at' 	=> \Carbon::now(),
                ]);
                DB::table('mod_order_detail')
                ->where('id',$id)
                ->update([
                    'sn_count'    => count($insertData),
                    'updated_by'  => $user->email,
                    'updated_at'  => \Carbon::now(),
                ]);
            }catch(\Exception $e) {
                \Log::error($e->getLine());
                \Log::error($e->getMessage());
            }

        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'data'=> null, 'code' => '01' ]);
        }

        
        return response()->json(['msg' => 'success', 'data'=> null, 'code' => '00' ]);
    }

    public function stdcheckdlvorder(Request $request) {

        \Log::info("stdcheckdlvorder");
        \Log::info($request->all());
        $user = Auth::user();
        $ord_no = $request->ord_no ;
        $data = array();
        $isordno = DB::table('mod_order')
        ->Where(function($query) use ($ord_no)
        {
            $query->orwhere("ord_no",$ord_no);
            $query->orwhere("sys_ord_no",$ord_no);
        })
        ->whereNotIn("status",["FINISHED","REJECT"])
        ->count();
        $dlvno=DB::table('mod_dlv')
        ->where("car_no",$user->email)
        ->pluck("dlv_no")->toarray();
        $data = DB::table('mod_order')
        ->select("id","ord_no","pkg_num","sys_ord_no")
        ->whereNotIn("status",["FINISHED","REJECT"])
        ->Where(function($query) use ($ord_no)
        {
            $query->orwhere("ord_no",$ord_no);
            $query->orwhere("sys_ord_no",$ord_no);
        })
        ->get();
        if(count($data)==0){
            return response()->json(['msg' => '查無資料或訂單狀態是已完成或拒收','code' => "01" ,'data' =>null ]);
        }

        return response()->json(['msg' => 'success','code' => "00",'data' => $data ]);
    }
    
    public function stddlvorder(Request $request) {
        ////
        \Log::info("stddlvorder");
        \Log::info($request->all());
        $user       = Auth::user();
        $ids        = $request->ids;
        $name       = $request->name;
        $phone      = $request->phone; 
        $dlv_detail = [];
        $today      = new \DateTime();
        $str_date   = $today->format('Ym');
        $params     = array(
            "c_key"   => $user->c_key,
            "date_ym" => substr($str_date, 2, strlen($str_date))
        );
        $BaseModel = new BaseModel();
        $dlv_no = $BaseModel->getAutoNumber("dlv",$params);    
        $status ="DLV";
        $car_no = $user->email;
        $driver_nm = $user->name;
        $ttl_cbm     = 0;
        $ttl_pkg_num = 0;
        $dlv_date = date('Y-m-d',strtotime('+1 day'));
        $car = DB::table('mod_car')->where('car_no', $car_no)->first();
        $errormsg =array();
        try{

            foreach($ids as $key=> $row){
                $check =DB::table('mod_order')
                ->where('id', $row)
                ->first();
                if(!isset($check)){
                    array_push($errormsg, $check->ord_no);
                }else{
                    if($check->status=="FINISHED" || $check->status=="REJECT" ){
                        array_push($errormsg, $check->ord_no);
                    }
                }
            }

            if(count($errormsg)>0 ){
                return ["msg"=> implode(',',$errormsg). " 訂單號不存在或狀態有問題，請移除或確認後送出!",'code' => "01","data"=>null];    
            }

            foreach($ids as $key=> $row){
                $ord = OrderMgmtModel::find($row);
    
                if($ord->dlv_no!=""){
                    DB::table('mod_dlv_plan')
                    ->where('status',"!=", 'FINISHED')
                    ->where('dlv_no', $ord->dlv_no)
                    ->where('sys_ord_no', $ord->sys_ord_no)
                    ->update([
                        'user_name'   => $name,
                        'user_phone'  => $phone,
                        'status' => "ERROR",
                        'error_cd' => "A001",
                        'error_descp' => "貨物--換車 收/送",
                        'abnormal_remark' => "",
                        'updated_at' => Carbon::now()->toDateTimeString(),
                        'finish_date' => Carbon::now()->toDateTimeString()
                    ]);
                    DB::table('mod_order')
                    ->where('id', $ord->id)
                    ->update([
                        'error_remark' =>  "貨物--換車 收/送",
                        'exp_reason' => "A001"
                    ]);
                    if(!empty($ord->pick_addr)){
                        DB::table('mod_order')
                        ->where('id', $ord->id)
                        ->update([
                            'dlv_type' =>  "P",
                        ]); 
                    }

                    $cnt = DB::table('mod_dlv_plan')->where('dlv_no', $ord->dlv_no)->count();
                    $fCnt = DB::table('mod_dlv_plan')->where('dlv_no', $ord->dlv_no)->whereRaw("(status = 'FINISHED' or status = 'ERROR' or status = 'REJECT')")->count();
                    $dlvnum = DB::table('mod_dlv')->where('dlv_no', $ord->dlv_no)->value('dlv_num');
                    $dlvnum = $dlvnum-1;
                    DB::table('mod_dlv')
                    ->where('dlv_no', $ord->dlv_no)
                    ->update(['dlv_num'=>$dlvnum]);
                    if($cnt == $fCnt) {
                        DB::table('mod_dlv')
                            ->where('dlv_no', $ord->dlv_no)
                            ->update(['status' => 'FINISHED']);
                        $checkdepartue =DB::table('mod_dlv')
                        ->where('dlv_no', $ord->dlv_no)
                        ->first();
                        if($checkdepartue->departure_time==null) {
                            DB::table('mod_dlv')
                                ->where('dlv_no', $ord->dlv_no)
                                ->update([
                                    'departure_time' => Carbon::now()->toDateTimeString(),
                                ]);
                        }
                        if($checkdepartue->finished_time==null) {
                            DB::table('mod_dlv')
                                ->where('dlv_no', $ord->dlv_no)
                                ->update([
                                    'finished_time' => Carbon::now()->toDateTimeString(),
                                ]);
                        }
                    }
                }
                $ttl_cbm     += $ord->total_cbm;
                $ttl_pkg_num += $ord->pkg_num;
    
                $ordCnt = DB::table('mod_dlv_plan')
                            ->where('status', 'FINISHED')
                            ->where('dlv_type', 'P')
                            ->where('sys_ord_no', $ord->sys_ord_no)
                            ->count();
                //$ord->dlv_type != 'P' 代表有提貨在其它配送單裡
                $isUrgent = 'N';
                //提貨地址不為空 新增提貨 派車明細
                if(!empty($ord->pick_addr) && $ordCnt == 0 && $ord->dlv_type != 'P') {
                    $address = $ord->pick_zip.$ord->pick_city_nm.$ord->pick_area_nm.$ord->pick_addr;
                    if($ord->g_key == 'SYL') {
                        $address = $ord->pick_zip.$ord->pick_addr;
                    }
                    $detail = array(
                        'user_name'   => $name,
                        'user_phone'  => $phone,
                        "ord_id"      => $ord->id,
                        "cust_nm"     => $ord->pick_cust_nm,
                        "dlv_type"    => 'P',
                        "ord_no"      => $ord->ord_no,
                        "sys_ord_no"  => $ord->sys_ord_no,
                        "status"      => "FINISHED",
                        "dlv_no"      => $dlv_no,
                        "addr"        => $address,
                        "etd"         => $ord->etd,
                        "sort"        => $key,
                        "remark"      => htmlspecialchars($ord->pick_remark),
                        "updated_at"  => $today->format('Y-m-d H:i:s'),
                        "updated_by"  => $user->email,
                        "created_at"  => $today->format('Y-m-d H:i:s'),
                        "created_by"  => $user->email,
                        "g_key"       => $user->g_key,
                        "c_key"       => $user->c_key,
                        "s_key"       => $user->s_key,
                        "d_key"       => $user->d_key,
                        'car_no'      => $car_no,
                        'driver_nm'   => $driver_nm,
                        'owner_nm'    => $ord->owner_nm,
                        'is_urgent'   => $isUrgent,
                        'finish_date' => Carbon::now()->toDateTimeString()
                    );
                    array_push($dlv_detail, $detail);
                }
                //配送地址不為空 新增配送 派車明細
                if(!empty($ord->dlv_addr)) {
                    $address = $ord->dlv_zip.$ord->dlv_city_nm.$ord->dlv_area_nm.$ord->dlv_addr;
                    if($ord->g_key == 'SYL') {
                        $address = $ord->dlv_zip.$ord->dlv_addr;
                    }
                    $detail = array(
                        'user_name'   => $name,
                        'user_phone'  => $phone,
                        "ord_id"     => $ord->id,
                        "cust_nm"    => $ord->dlv_cust_nm,
                        "dlv_type"   => 'D',
                        "ord_no"     => $ord->ord_no,
                        "sys_ord_no" => $ord->sys_ord_no,
                        "status"     => $status,
                        "dlv_no"     => $dlv_no,
                        "addr"       => $address,
                        "etd"        => $ord->etd,
                        "sort"       => $key,
                        "remark"     => htmlspecialchars($ord->dlv_remark),
                        "updated_at" => $today->format('Y-m-d H:i:s'),
                        "updated_by" => $user->email,
                        "created_at" => $today->format('Y-m-d H:i:s'),
                        "created_by" => $user->email,
                        "g_key"      => $user->g_key,
                        "c_key"      => $user->c_key,
                        "s_key"      => $user->s_key,
                        "d_key"      => $user->d_key,
                        'car_no'      => $car_no,
                        'driver_nm'   => $driver_nm,
                        'owner_nm'    => $ord->owner_nm,
                        'is_urgent'   => $isUrgent,
                        'finish_date' => null
                    );
    
                    array_push($dlv_detail, $detail);
                }
    
                $tsData = DB::table("mod_trans_status")->where("ts_type", "A")->where('order', 3)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->orderBy("order", "desc")->first();
                $data = [
                    'ts_no'      => $tsData->id,
                    'ts_type'    => $tsData->ts_type,
                    'sort'       => $tsData->order,
                    'ts_name'    => $tsData->ts_name,
                    'ts_desc'    => $tsData->ts_desc,
                    'ref_no1'    => $dlv_no,
                    'ref_no2'    => $ord->ord_no,
                    'ref_no3'    => $car_no,
                    'ref_no4'    => $ord->sys_ord_no,
                    'g_key'      => $ord->g_key,
                    'c_key'      => $ord->c_key,
                    's_key'      => $ord->s_key,
                    'd_key'      => $ord->d_key,
                    'created_by' => $user->name,
                    'updated_by' => $user->name,
                ];
                $TransRecordModel = new TransRecordModel();
                $TransRecordModel->createRecord($data);
            }
    
            $modDlv               = new DlvModel;
            $modDlv->dlv_no       = $dlv_no;
            $modDlv->dlv_date     = $dlv_date;
            $modDlv->status       = "SEND";
            $modDlv->load_rate    = $car->load_rate;
            $modDlv->load_tweight = 0;//$car->load_weight;
            $modDlv->load_tcbm    = $ttl_cbm;
            $modDlv->dlv_num      = count($ids);
            $modDlv->cust_no      = $car->cust_no;
            $modDlv->car_no       = $car->car_no;
            $modDlv->car_type_nm  = $car->car_type_nm;
            $modDlv->driver_nm    = $driver_nm;
            $modDlv->driver_phone = $user->phone;
            $modDlv->updated_by   = $user->email;
            $modDlv->created_by   = $user->email;
            $modDlv->g_key        = $user->g_key;
            $modDlv->c_key        = $user->c_key;
            $modDlv->s_key        = $user->s_key;
            $modDlv->d_key        = $user->d_key;
            $modDlv->save();
            DB::table('mod_dlv_plan')->insert($dlv_detail);
            DB::table('mod_order')
            ->whereIn('id', $ids)
            ->update([
                "status"   => "LOADING", 
                "status_desc"   => "貨物裝載中", 
                "car_no"   => $car_no,
                "truck_no" => $car_no,
                "dlv_no"   => $dlv_no,
                "is_ordered" => "N",
                "order_tally" => "N",
                "realnum" => 0,
                "driver"   => $driver_nm
            ]);
            try {
                $fcmnow = date("Y-m-d H:i:s");
                $f = new FcmModel;
                $f->sendToFcm('系統通知 '.$fcmnow, '您有新配送單：'.$dlv_no, $user->d_token);
                $s = new SysNoticeModel;
                $s->insertNotice('系統通知 '.$fcmnow, '您有新配送單：'.$dlv_no, $car_no, $user);
            } catch (\Throwable $e) {
                
            }
        }
        catch(\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return ["msg"=>"操作失敗",'code' => "01","data"=>$errormsg];
        }
        return response()->json(['msg' => '已完成接單程序，請至運輸作業查看!','code' => "00",'data'=>null ]);
    }

    public function stdordDetail($sysOrdNo=null,$type=null,$dlv_no=null) {
        \Log::info("stdordDetail");
        $user   = Auth::user();
        $main   = null;
        $detail = null;
        $fee    = null;
        \Log::info($sysOrdNo);
        \Log::info($type);
        \Log::info($dlv_no);
        try {
            $order = DB::table('mod_order')
            ->where('sys_ord_no', $sysOrdNo)
            ->first();
            $thisQuery = DB::table('mod_order')
                            ->where('sys_ord_no', $sysOrdNo);
            if($type == 'D') {
                $thisQuery->select(
                    DB::raw("(select status from mod_dlv_plan where dlv_type= 'D' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as tab_status"),
                    DB::raw('dlv_attn as attn'),
                    DB::raw("(select error_cd from mod_dlv_plan where dlv_type= 'D' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as error_cd"),
                    DB::raw("(select abnormal_remark from mod_dlv_plan where dlv_type= 'D' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as abnormal_remark"),
                    DB::raw("(select error_descp from mod_dlv_plan where dlv_type= 'D' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1) as error_remark"),
                    DB::raw('dlv_tel as tel'),
                    DB::raw("dlv_addr as address"),
                    'collectamt', 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'id', 'sys_ord_no','dlv_no', 'ord_no', 'status',
                    DB::raw('dlv_remark as remark'), 'dlv_no'
                   );
            }else {
                $thisQuery->select(
                    DB::raw("(select status from mod_dlv_plan where dlv_type= 'P' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as tab_status"),
                    DB::raw('pick_attn as attn'),
                    DB::raw('pick_tel as tel'), 
                    DB::raw("pick_addr as address"),
                    'collectamt', 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'id', 'sys_ord_no',
                     DB::raw('pick_remark as remark'), 'dlv_no', 'ord_no', 'status',
                     DB::raw("(select error_cd from mod_dlv_plan where dlv_type= 'P' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as error_cd"),
                     DB::raw("(select abnormal_remark from mod_dlv_plan where dlv_type= 'P' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as abnormal_remark"),
                     DB::raw("(select error_descp from mod_dlv_plan where dlv_type= 'P' and dlv_no='".$dlv_no. "'  and sys_ord_no ='". $sysOrdNo."' limit 1)as error_remark")
                );
            }

            $main = $thisQuery->first();
            $main->dlv_type = $type;
            if($main->tab_status == 'FINISHED' || $main->tab_status == 'REJECT' || $main->tab_status == 'ERROR') {
                $main->tab_status = 1;
            } else {
                $main->tab_status = 0;
            }
            if(isset($main)) {
                $detail = DB::table('mod_order_detail')
                ->select(
                    'id','ord_no','goods_no','goods_nm','pkg_num','pkg_unit',
                    'gw','gwu','cbm','cbmu','length','weight','height','goods_no2',
                    'ord_id','sn_no','price')
                            ->where('ord_id', $main->id)
                            ->get();

                $fee = DB::table('mod_order_fee')
                ->select(
                    'id', 'ord_id', 'fee_cd', 'fee_name', 'amount',  
                    'fee_descp', 'fee_type_cd', 'fee_type_nm', 'prod_detail_id', 'fee_goods_nm' 
                )
                ->where('ord_id', $main->id)
                ->get();

                $img = DB::connection('mysql::write')
                ->table('mod_file')
                ->select(
                    'id','type_no',
                    DB::raw("created_by as candelimg"),
                    DB::raw("CONCAT('".env('PRINTER_URL').'storage/'."' , guid)as imgurl")
                )
                ->where('ref_no', $sysOrdNo)
                ->get();
    
                foreach ($img as $key => $row) {
                    $row->candelimg = $row->candelimg == $user->email ? "Y" :"N";
                }
    
            }
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data' => null]);
        }

        $data     = array();

        $data['main']   = $main;
        $data['detail'] = $detail;
        $data['fee']    = $fee;
        $data['img']    = $img;

        //20220448 異常備註 異常原因額外開
        $dlvplanData = DB::table('mod_dlv_plan')
        ->where('sys_ord_no', $sysOrdNo)
        ->where('dlv_no', $order->dlv_no)
        ->where('dlv_type', $type)
        ->first();
        $data['error']  = array(
            'abnormal_remark' => isset($dlvplanData->abnormal_remark) ? $dlvplanData->abnormal_remark : '',
            'error_type'      => isset($dlvplanData->error_cd) ? $dlvplanData->error_cd : ''
        );

        return response()->json(['msg' => 'success','code' => "00",'data' => $data ]);
    }

    public function stdrefreshpass() {
        try {
            $user = Auth::user();      
            $userData = DB::table('users')
                            ->where('id', $user->id)
                            ->update(['final_refresh' => Carbon::now()->toDateTimeString()]);
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> null]);
        }

        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
    }

    public function stdreorderDlvPlan(Request $request) {
        try {
            \Log::info("reorderDlvPlan");
            \Log::info($request->all());
            $user = Auth::user();
    
            $items = $request->item;
            $dlvNo = $request->dlv_no;
            $i = 0;
            foreach($items as $row) {
                DB::table('mod_dlv_plan')
                    ->where('dlv_no', $dlvNo)
                    ->where('dlv_type', $row['dlv_type'])
                    ->where('sys_ord_no', $row['sys_ord_no'])
                    ->update(['sort' => $i]);
                $i++;
            }
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
        }catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> null]);
        }
        
    }

    public function getimglimit(Request $request) {

        try {
            \Log::info("getimglimit");
            \Log::info($request->all());

            $imglimit = DB::table('bscode')
            ->where('cd_type', 'UPLOADIMGLIMIT')
            ->where('cd', 'uploadimglimit')
            ->value('value1');

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> (int) $imglimit]);
        }catch(\Exception $e) {

            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> null]);
        }

    }

}