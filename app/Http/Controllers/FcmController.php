<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

use App\Models\FcmModel;

class FcmController extends Controller
{
    public function sendMessage() {
        $msg = 'test';
        $token = "cRtne7KVwMg:APA91bGyeAQA7s3a64Rg6xf9aEUd5Umxg-2LYMbLqWLVCIZnGoxXJI0EZd-yLPSzGB4AkN3bcihGjcRHBXxtqmFJXW20I76ZbuKKF02nImNKg1sDU-puLgLsAUj8H7ahiEEEbo4DkpNB";

        $f = new FcmModel;
        $f->sendToFcm('系統通知', $msg, $token);

        return response()->json(['msg' => 'success']);
    }
}
