<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Models\SysSiteModel;
use App\Models\BaseModel;
use Illuminate\Session\Store as Session;


class SysSiteController extends Controller
{
    
    public function index() {
        $title = trans('sysSite.titleName');
        echo "<title>$title</title>";
        return view('SysSite.index');
    }

    public function get(Request $request) {
        $type = $request->type;

        $sysSite = new SysSiteModel;
        $data = [];

        if($type == -1) {
            $data = $sysSite->where('type', 0)
                            ->get();
        }
        else if($type == 0) {
            $data = $sysSite->where('type', 1)
                            ->where('g_key', $request->g_key)
                            ->get();
        }
        else if($type == 1) {
            $data = $sysSite->where('type', 2)
                            ->where('g_key', $request->g_key)
                            ->where('c_key', $request->c_key)
                            ->get();
        }
        else if($type == 2) {
            $data = $sysSite->where('type', 3)
                            ->where('g_key', $request->g_key)
                            ->where('c_key', $request->c_key)
                            ->where('s_key', $request->s_key)
                            ->get();            
        }

        
        return ["msg"=>"success", "data"=>$data];
    }

    public function store(Request $request)
    {

        $sysSite = new SysSiteModel;
        $sysSite->g_key    = $request->g_key;
        if($request->type == 0) {
            $sysSite->c_key    = '*';
            $sysSite->s_key    ='*';
            $sysSite->d_key    = '*';
        }
        else if($request->type == 1) {
            $sysSite->c_key    = $request->c_key;
            $sysSite->s_key    = '*';
            $sysSite->d_key    = '*';
        }
        else if($request->type == 2) {
            $sysSite->c_key    = $request->c_key;
            $sysSite->s_key    = $request->s_key;
            $sysSite->d_key    = '*';
        }
        else {
            $sysSite->c_key    = $request->c_key;
            $sysSite->s_key    = $request->s_key;
            $sysSite->d_key    = $request->d_key;
        }
        
        $sysSite->cname    = $request->cname;
        $sysSite->ename    = $request->ename;
        $sysSite->type    = $request->type;

        $sysSite->save();

        if($request->type == 0) {
            $data = $sysSite->where('type', 0)
                            ->get();
        }
        else if($request->type == 1) {
            $data = $sysSite->where('type', 1)
                            ->where('g_key', $request->g_key)
                            ->get();
        }
        else if($request->type == 2) {
            $data = $sysSite->where('type', 2)
                            ->where('g_key', $request->g_key)
                            ->where('c_key', $request->c_key)
                            ->get();            
        }
        else if($request->type == 3) {
            $data = $sysSite->where('type', 3)
                            ->where('g_key', $request->g_key)
                            ->where('c_key', $request->c_key)
                            ->where('s_key', $request->s_key)
                            ->get();            
        }

        return ["msg"=>"success", "data"=>$data];
    }

    public function update(Request $request)
    {
        $sysSite = SysSiteModel::find($request->id);
        $sysSite->cname      = $request->cname;
        $sysSite->ename      = $request->ename;
        $sysSite->save();


        return ["msg"=>"success", "data"=>$sysSite->where('id', $request->id)->get()];
    }

    public function delete(Request $request)
    {

        $sysSite = new SysSiteModel;      

        if($request->type == 0) {
            $data = $sysSite->where('type', 0)
                            ->where('g_key', $request->g_key)
                            ->delete();
        }
        else if($request->type == 1) {
            $data = $sysSite->where('type', 1)
                            ->where('g_key', $request->g_key)
                            ->where('c_key', $request->c_key)
                            ->delete();
        }
        else if($request->type == 2) {
            $data = $sysSite->where('type', 2)
                            ->where('g_key', $request->g_key)
                            ->where('c_key', $request->c_key)
                            ->where('s_key', $request->s_key)
                            ->delete();            
        }
        else if($request->type == 3) {
            $data = $sysSite->where('type', 3)
                            ->where('g_key', $request->g_key)
                            ->where('c_key', $request->c_key)
                            ->where('s_key', $request->s_key)
                            ->where('d_key', $request->d_key)
                            ->delete();            
        }

        return ["msg"=>"success"];
    }

    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), [
            'g_key' => 'required|max:50'
        ]);

        return $validator;
    }
}
