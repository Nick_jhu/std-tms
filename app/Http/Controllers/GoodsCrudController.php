<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Models\GoodsModel;
use App\Models\BaseModel;
use App\Models\CommonModel;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\GoodsCrudRequest as StoreRequest;
use App\Http\Requests\GoodsCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store as Session;
use Excel;
class GoodsCrudController extends CrudController
{
    public function setup() {
        $user = Auth::user();
        $this->crud->setModel("App\Models\GoodsModel");
        $this->crud->setEntityNameStrings('商品建檔', '商品建檔');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/goodsProfile');
    

        
        $this->crud->setCreateView('goodsProfile.edit');
        $this->crud->setEditView('goodsProfile.edit');
        $this->crud->setListView('goodsProfile.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'goods_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'goods_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'owner_nm',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'goods_type_nm',
            'type' => 'lookup',
            'title' => '貨主查詢',
            'info1' => Crypt::encrypt('bscode'), //table
            'info2' => Crypt::encrypt("cd+cd_descp,cd,cd_descp"), //column
            'info3' => Crypt::encrypt("cd_type='GOODSTYPE' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
            'info4' => "cd=goods_type_cd;cd_descp=goods_type_nm" //field mapping
        ]);
        $this->crud->addField([
            'name' => 'goods_type_cd',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'owner_cd',
            'type' => 'lookup',
            'title' => '貨主查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname,receive_mail"), //column
            'info3' => Crypt::encrypt("status='B' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
            'info4' => "cust_no=owner_cd;cname=owner_nm;receive_mail=owner_send_mail" //field mapping
        ]);
        
        $this->crud->addField([
            'name' => 'goods_no2',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'goods_nm2',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'gw',
            'type' => 'number',
            'attributes' => ["step" => "any"], // allow decimals
            'suffix' => ".00"
        ]);

        $this->crud->addField([
            'name' => 'price',
            'type' => 'number',
            'attributes' => ["step" => "any"], // allow decimals
        ]);
        $this->crud->addField([
            'name' => 'gwu',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'pkg_unit',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'cbm',
            'type' => 'number',
            'attributes' => ["step" => "any"], // allow decimals
            'suffix' => ".00"
        ]);

        $this->crud->addField([
            'name' => 'cbmu',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 's_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'hidden'
        ]);

        $this->crud->addField([
            'name' => 'g_length',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'g_width',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'g_height',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'sn_flag',
            'type' => 'text'
        ]);
    }

    public function index() {
        $user = Auth::user();

        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }
        
        $BaseModel = new BaseModel();
        $obj = null;

        $layout = $BaseModel->getLayout(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_goods'));
        $obj = json_decode($layout);

        $fieldData = $BaseModel->getColumnInfo("mod_goods");
        if($obj != null) {
            array_push($fieldData, $obj);
        }

        $colModel = array('mod_goods' => $fieldData);

        $this->crud->colModel = $fieldData;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('goodsProfile'))
            {
                return view($this->crud->getListView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }

    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;
        $user = Auth::user();
        try{
            if(Auth::user()->hasPermissionTo('goodsProfile'))
            {
                return view($this->crud->getEditView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function store(StoreRequest $request)
	{
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        
        //dd($request->all());
        try {
            $response = parent::storeCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::updateCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response];
    }

    public function multiDel() {
        $user = Auth::user();
        $ids = request('ids');
        
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $car = GoodsModel::find($ids[$i]);
                $car->delete();
            }
        }

        return response()->json(array('msg' => 'success'));
    }
    public function uploadExcel() {
        $user = Auth::user();
        $obj = array(
            '貨主代碼' => 'owner_cd',
            // '貨主名稱' => 'owner_nm',
			'料號1' => 'goods_no',
			'料號名稱1' => 'goods_nm',
			'料號2' => 'goods_no2',
			'料號名稱2' => 'goods_nm2',
            '重量' => 'gw',
            '重量單位' => 'gwu',
            '材積' => 'cbm',
            '材積單位' => 'cbmu',
            '長'  => 'g_length',
            '寬'  => 'g_width',
            '高'  => 'g_height',
            '費用'  => 'price'
            );
            try{
                if(Input::hasFile('import_file')){
                    $path = Input::file('import_file')->getRealPath();
                    $data = Excel::load($path, function($reader) {
                        $reader->setDateFormat('Y-m-d');
                    })->get();
    
                    $i = 0;
                    $dataFirst = $data[0];

                    $header = $dataFirst->first()->keys()->toArray();
                    $insert = array();
                    $exist = "";
                    $now = date("Y-m-d H:i:s");
                    if(!empty($dataFirst) && $dataFirst->count()){
                        foreach ($dataFirst as $key => $value) {
                            //$obj[$header[$i]] = $value->$header[$i];
                            $result = array();
                            for($i=0;$i<count($value);$i++) {
                                if(isset($obj[$header[$i]])) {
                                    $o = array(
                                        $obj[$header[$i]] => $value[$header[$i]]
                                    );
                                    $result = array_merge($result, $o);
                                }
                                
                            }
                            $result['g_key']      = $user->g_key;
                            $result['c_key']      = $user->c_key;
                            $result['s_key']      = $user->s_key;
                            $result['d_key']      = $user->d_key;
                            $result['created_by'] = $user->email;
                            $result['created_at'] = $now;
                            $result['updated_by'] = $user->email;
                            $result['updated_at'] = $now;
                            

                            $custData = DB::table('sys_customers')
                            ->where('cust_no', $result['owner_cd'])
                            ->first();
                            // echo json_encode($custData);
                            // return;
                            if($custData != null){
                            $result['owner_nm'] = $custData->cname ;
                            }
                            $goodsData = DB::table('mod_goods')
                            ->where('goods_no', $result['goods_no'])
                            ->where('owner_cd',$result['owner_cd'])
                            ->count();
                        
                            if( !empty($result['goods_no']) && !empty($result['owner_cd']) && $goodsData == 0 && $custData !=null) {
                                array_push($insert,  $result);
                            }else if(empty($result['owner_cd'])) {
                                $exist =$exist."料號".$result['goods_no'].$result['owner_cd']." 的貨主代碼".$result['owner_cd'].'不得為空<br/>' ;
                            }
                            else if($custData == null) {
                                $exist =$exist."貨主代碼".$result['owner_cd'].'不存在<br/>' ;
                            }
                            else if(empty($result['goods_no'])) {
                                $exist =$exist."料號".$result['goods_no'].'不得為空<br/>' ;
                            }
                            else if($goodsData > 0 ){
                                //存在的修改
                                DB::table('mod_goods')
                                ->where('goods_no', $result['goods_no'])
                                ->where('owner_cd',$result['owner_cd'])
                                ->update([
                                    'goods_nm'  => $result['goods_nm'],
                                    'goods_no2' => $result['goods_no2'],
                                    'goods_nm2' => $result['goods_nm2'],
                                    'gw'        => $result['gw'],
                                    'gwu'       => $result['gwu'],
                                    'cbm'       => $result['cbm'],
                                    'cbmu'      => $result['cbmu'],
                                    'g_length'  => $result['g_length'],
                                    'g_width'   => $result['g_width'],
                                    'g_height'  => $result['g_height'],
                                    'price'     => $result['price'],
                                ]);
                                // $exist =$exist."貨主代碼".$result['owner_cd']."料號".$result['goods_no'].'重復<br/>' ;
                            }
                        }
                        if(!empty($insert)){
                            DB::table('mod_goods')->insert($insert);
                        }
                    }
                }
            }catch (\Exception $e) {
                return ["msg"=>"error", "errorLog"=>$e->getMessage()];
            }
            //return back();
            if (empty($exist)){
                return ["msg"=>"success", "data"=>$insert];
            }else{

                return ["msg"=>"exist", "msg2"=>$exist];
            }
    }
}
