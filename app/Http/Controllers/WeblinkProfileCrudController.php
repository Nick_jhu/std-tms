<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use App\Models\miModel;
use App\Models\BscodeModel;
use App\Models\BaseModel;
use App\Models\CommonModel;
use App\Models\SampleDetailModel;
use Illuminate\Session\Store as Session;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use DOMDocument;
use App\Models\CalculateModel;
use App\Models\TrackingModel;
use SimpleXMLElement;
use App\Models\weblinkModel;
use Carbon\Carbon;
class WeblinkProfileCrudController extends CrudController
{
    
    public function setup() {
        $this->crud->setModel("App\Models\weblinkModel");
        $this->crud->setEntityNameStrings("展碁", "展碁");
        $this->crud->setRoute(config('backpack.base.route_prefix').'/weblinkProfile');
    
        $this->crud->setColumns(['name']);

        
        $this->crud->setCreateView('weblinkProfile.edit');
        $this->crud->setEditView('weblinkProfile.edit');
        $this->crud->setListView('weblinkProfile.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'pick_no',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'cust_no',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'cust_name',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'truck_cmp_name',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'purchase_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'purchase_by',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'product_no',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'goods_no',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'product_name',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'global_barcode',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'quantity',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'qty',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'bos_no',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'total_box_num',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'color',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'shape',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'size',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'category',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'product_length',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'product_width',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'product_high',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'product_weight',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'box_length',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'box_width',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'box_height',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'box_weight',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_at',
            'type' => 'text'
        ]);           

        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);
        
        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 's_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'text'
        ]);
    }

    public function get($mainId=null) 
    {
        $bscode = [];
        $user = Auth::user();
        //dd($cust_no);
        $this_query = DB::table('mod_senao_detail');
        $this_query->where('senao_id', $mainId);
        $bscode = $this_query->get();
        
        $data[] = array(
            'Rows' => $bscode,
        );

        return response()->json($data);
    }
    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        try{
            if(Auth::user()->hasPermissionTo('importByweblink'))
            {
                return view($this->crud->getEditView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('importByweblink'))
            {
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    
    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        // $request->g_key = $user->g_key;
        // $request->c_key = $user->c_key;
        // $request->s_key = $user->s_key;
        // $request->d_key = $user->d_key;       
        //$request['created_by'] = $user->name;
        // $commonFunc = new CommonModel;
        // $request = $commonFunc->processData($request, $this->crud->create_fields);
        
        // //dd($request->all());
        // try {
        //     $response = parent::storeCrud($request);
        // }
        // catch (\Exception $e) {
        //     return ["msg"=>"error", "errorLog"=>$e];
        // }
        
        // $request->session()->forget('alert_messages');
        return ["msg"=>"success"];
	}

	public function update(UpdateRequest $request)
	{
        
        // $commonFunc = new CommonModel;
        // $request = $commonFunc->processData($request, $this->crud->create_fields);

        // try {
        //     $response = parent::updateCrud($request);
        // }
        // catch (\Exception $e) {
        //     return ["msg"=>"error", "errorLog"=>$e];
        // }

        // $request->session()->forget('alert_messages');
        return ["msg"=>"success"];
    }

    public function detailStore(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);        

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $bscode = new BscodeModel;
            $bscode->cd_type    = $request->cd_type;
            $bscode->cd       = $request->cd;
            $bscode->cd_descp      = $request->cd_descp;
            $bscode->value1    = $request->value1;
            $bscode->value2    = $request->value2;
            $bscode->value3    = $request->value3;
            $bscode->g_key    = $user->g_key;
            $bscode->c_key    = $user->c_key;
            $bscode->s_key    = $user->s_key;
            $bscode->d_key    = $user->d_key;
            $bscode->created_by    = $user->email;
            $bscode->updated_by    = $user->email;
            $bscode->save();
        }

        return ["msg"=>"success", "data"=>$bscode->where('id', $bscode->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $user = Auth::user();
        // $validator = $this->detailValidator($request);

        // if ($validator->fails()) {
        //     return ["msg"=>"error", "errorLog"=>$validator->messages()];
        // }
        // else {
        //     $bscode = BscodeModel::find($request->id);
        //     $bscode->cd       = $request->cd;
        //     $bscode->cd_descp      = $request->cd_descp;
        //     $bscode->value1    = $request->value1;
        //     $bscode->value2    = $request->value2;
        //     $bscode->value3    = $request->value3;
        //     $bscode->g_key    = $user->g_key;
        //     $bscode->c_key    = $user->c_key;
        //     $bscode->s_key    = $user->s_key;
        //     $bscode->d_key    = $user->d_key;            
        //     $bscode->updated_by    = $user->email;
        //     $bscode->save();
        // }


        return ["msg"=>"success"];
    }

    public function detailDel($id)
    {
        // $sampleDetail = BscodeModel::find($id);
        // $sampleDetail->delete();

        return ["msg"=>"success"];
    }

    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), [
            // 'cd' => [
            //     'required',
            //     'min:1',
            //     'max:20',
            //     Rule::unique('bscode')->where('cd_type', $request->cd_type) 
            // ],
            'cd' => 'required|min:1|max:20',
            'cd_descp' => 'nullable|min:1|max:300',
            'value1' => 'nullable|min:1|max:50',
            'value2' => 'nullable|min:1|max:50',
            'value3' => 'nullable|min:1|max:50'
        ]);

        return $validator;
    }


    public function upload()
    {
        // if (!$request->hasFile('file')) {
        //     throw new \Exception("文件不存在");
        // }
        // $file = $request->file('file');
        // if (!$file->isValid()) {
        //     throw new \Exception($file->getErrorMessage());
        // }
        // // $path = Storage::put(date('Ymd'), $file);
        $imgData1="";
        try {
            Storage::disk('azure')->put('0521/image1test.jpg', base64_decode($imgData1));
        }
        catch(\Exception $e) {
            return response()->json(["msg"=>"error","line"=>$e->getLine(),"msglog"=>$e->getMessage()]);
        }
        // $url = "https://bqtwecstorage.blob.core.windows.net/tw-ec-image-storage".'/'."0521".'/'.$path;

        return response()->json(["success" => true]);
    }
    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $customerProfile = SenaoModel::find($ids[$i]);
                 DB::table('mod_shippen_temp_detail')
                 ->where('shippen_id', $customerProfile->id)
                 ->delete();
                $customerProfile->delete();
            }
        }

        return response()->json(array('msg' => 'success'));
    }
    public function webcallSenao() {
        $senaoModel = new SenaoModel;
        $result  = $senaoModel->callSenaogetDeliveryOrder();
        
        return response()->json(["msg" => $result]);
    }


    public function genSenaoexcel() {
		try{
			$user = Auth::user();
			$now = date('YmdHis');
			Excel::create('senao_wms_' . $now, function ($excel) use ($now) {
				$excel->sheet('ShipmentAdvice', function ($sheet) use ($now) {

					$ids = DB::table('mod_senao')->pluck('id')->toArray();
					$senaodetaildatas = DB::table('mod_senao_detail')->where('is_send', 'N')->whereIn('senao_id',$ids)->get();

					$sheet->row(1, array(
						'senao資料序號','senao訂單(退貨)編號','購買人姓名',
						'收件人姓名','收件人性別','收件人電話','收件人電話分機','收件人行動電話','收件人郵遞區號','收件人地址',
						'安裝單號 (委託/訂單單號)','接單日期 / 時間','接單來源','接單門市名稱',
						'商品貨號/料號','商品(產品)型號','商品(產品)名稱',
						'商品類別','倉庫','倉別區','交易數量','代收款金額','尾款金額',
						'付款方式名稱','是否處理廢四機','廢四機單號','廢四機備註','是否壁掛',
						'壁掛備註','是否為偏遠地區','偏遠地區備註','是否為特殊安裝','特殊安裝備註',
						'指定運送日期','指定運送時段','運送類別','備註','最後修改日期','批號-流水號'
					));

					foreach ($senaodetaildatas as $key => $row) {
						$mainid = DB::table('mod_senao')->where('id',$row->senao_id)->first();
						$type       = $mainid->logistics_type == 1 ? 1:5 ;
						$itemNo     = $mainid->logistics_type == 1 ? $mainid->order_no : NULL ;
						$backitemNo = $mainid->logistics_type == 3 ? $mainid->order_no : NULL ;
						$feetypeName = '' ;
						// 付款方式1:月結請款 2:現金付款 3:對方付現 4: 對方付款月結請款
						if($mainid->fee_type == 1 ) {
							$feetypeName = '月結請款';
						} else if($mainid->fee_type == 2) {
							$feetypeName = '現金付款';
						} else if($mainid->fee_type == 3) {
							$feetypeName = '對方付現';
						} else {
							$feetypeName = '對方付款月結請款';
						}

						$sheet->row(($key + 2), array(
							$itemNo,$backitemNo,$mainid->ship_name,
							$mainid->receipt_name,NULL,$mainid->receipt_cell,NULL,$mainid->receipt_tel,$mainid->receipt_zip_code,$mainid->receipt_address,
							$mainid->order_no,$mainid->ship_date.'/'.$mainid->ship_time ,NULL,NULL,
							$row->item_no,NULL,$row->item_name,
							NULL,NULL,NULL,$row->qty,$mainid->ship_amount,NULL,
							NULL,$mainid->recycle_yn,$mainid->recycle_doc_no,NULL,$mainid->wall_mount_yn,
							$feetypeName,NULL,NULL,NULL,NULL,
							NULL,NULL,$type,$mainid->receipt_remark,$mainid->updated_at,NULL,
						));
						$sheet->setCellValueExplicit("O".($key+2), $row->item_no, \PHPExcel_Cell_DataType::TYPE_STRING);
					}
				});
			})->store('xls', storage_path('app/'));
			$nowy = date('Y');
			// $nowm = date('m');
			Storage::disk('senao')->putFileAs('SENAO/'.$now."/",new File(Storage::path('senao_wms_' .$now.'.xls')),'senao_wms_' .$now.'.xls');

			$senaodetaildatas = DB::table('mod_senao_detail')->where('is_send', 'N')->whereIn('senao_id',$ids)->update([
				'is_send'     => 'Y',
				'export_by'   => $user->email,
				'export_date' => $now,
			]);
			// Storage::disk('local')->delete('senao_wms_' .$now.'.xls');
		} catch (\Exception $e) {
			\Log::info($e->getMessage());
			\Log::info($e->getLine());
			return ["msg"=>"error", "errorLog"=>$e->getMessage(), "errorline"=>$e->getline()];
		}
		return;

	}


}
