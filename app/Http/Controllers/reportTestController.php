<?php

namespace App\Http\Controllers;
use JasperPHP;
use File;
use Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class reportTestController extends Controller
{
	//...

    public function generateReport()
    {        
        // //jasper ready to call
        // $jasper = new JasperPHP;
        // $jasper->compile(base_path('/vendor/cossou/jasperphp/examples/hello_world.jrxml'))->execute();
        // echo base_path();

        // JasperPHP::process(
        //     base_path('/vendor/cossou/jasperphp/examples/hello_world.jasper'),
        //     false,
        //     array('pdf', 'rtf'),
        //     array('php_version' => phpversion())
        // )->execute();
        return "";
    }

    public function sylreport1()
    {
        try{
        $user = Auth::user();
        $ids = request('ids');
        \Log::info($ids);
        $data2 = ["result"];
        $result= "";
        $ord_data = DB::table('mod_order')
        ->leftJoin('mod_order_detail', 'mod_order.id', '=', 'mod_order_detail.ord_id')
        ->select(
                'mod_order_detail.pkg_num AS detail_pkg_num',
                'mod_order_detail.goods_nm',
                'mod_order_detail.goods_no',
                DB::raw("(select sum(d.pkg_num) from mod_order_detail d where ord_id = mod_order.id) as sum_num "), 
                'mod_order.*'
                )
        ->whereIn('mod_order.id', $ids)
        ->orderBy('mod_order.id', 'asc')
        ->get();

        $data2['result'] = $ord_data;
        $test = json_encode($data2);
            Storage::disk('report')->put('testreport.json',$test);
            $datafile = base_path('/storage/app/report/testreport.json');
            $result = JasperPHP::process(
                base_path('/vendor/cossou/jasperphp/examples/sylrxml.jrxml'),
                base_path('/public/adminreport/'.$user->email),
                array('pdf'),
                array(),
                array("driver"=>"json", "json_query" => "result", "data_file" =>  $datafile)  
            )->execute();
        }
        catch (\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => $result, 'filename'=>'error']);
        }
        return response()->json(['msg' => $result,'filename'=>$user->email]);
    }

    public function sylreport1_new()
    {
        try{
        $user = Auth::user();
        $ids = request('ids');
        \Log::info($ids);
        $data2 = ["result"];
        $result= "";
        $temparray = array();
        $order1 = request('order1');
        $order2 = request('order2');
        $order3 = request('order3');
        $name = $user->email;
        if($order1==""){
            $order1="id";
        }
        if($order2==""){
            $order2="id";
        }
        if($order3==""){
            $order3="id";
        }
        sort($ids);
        $num  =count($ids);
        for($i=0; $i<$num; $i++) {
        DB::statement(DB::raw("set @row=0"));
        $ord_data = DB::table('mod_order')
        ->leftJoin('mod_order_detail', 'mod_order.id', '=', 'mod_order_detail.ord_id')
        ->select(
                DB::raw("@row div 10 as 'row'"),
                DB::raw('@row:=@row+1 as test'),
                DB::raw("(select SUBSTRING(cmp_abbr, 1, 6)  from sys_customers where cust_no = mod_order.owner_cd and status='B' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."' limit 1) owner_abbr "), 
                'mod_order_detail.pkg_num AS detail_pkg_num', 
                'mod_order_detail.pkg_unit AS detail_pkg_unit', 
                DB::raw("case when mod_order_detail.goods_nm ='null' then '' else mod_order_detail.goods_nm end as goods_nm"),
                'mod_order_detail.goods_no',
                DB::raw('SUBSTRING(pick_cust_nm, 1, 10)as pick_custnm'), 
                DB::raw('SUBSTRING(dlv_cust_nm, 1, 10)as dlv_custnm'), 
                DB::raw("(select sum(d.pkg_num) from mod_order_detail d where ord_id = mod_order.id) as sum_num "), 
                'mod_order.*'
                )
        ->where('mod_order.id', $ids[$i])
        ->orderBy('mod_order.'.$order1, 'asc')
        ->orderBy('mod_order.'.$order2, 'asc')
        ->orderBy('mod_order.'.$order3, 'asc')
        ->get();
        foreach($ord_data as $key=>$row){
            $row->row  = (int)($key/10);
            $row->test = (int)(($key %10)+1);
            if($row->etd=="0000-00-00"){
                $row->etd =null;
            }
            $length =  strlen( $row->remark) ;
            if($length >= 60) {
                $row->remark = mb_substr($row->remark,0,60,"utf-8");;
            }

            array_push($temparray, $row);
        }
        }

        $data2['result'] = $temparray;
        $test = json_encode($data2);
            Storage::disk('report')->put('testreport.json',$test);
            $datafile = base_path('/storage/app/report/testreport.json');
            $result = JasperPHP::process(
                base_path('/vendor/cossou/jasperphp/examples/newsylrxml.jrxml'),
                base_path('/public/adminreport/'.$user->email),
                array('pdf'),
                array(),
                array("driver"=>"json", "json_query" => "result", "data_file" =>  $datafile)  
            )->execute();
        }
        catch (\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => $result, 'filename'=>'error']);
        }
        return response()->json(['msg' => $result,'filename'=>$user->email]);
    }
    public function sylreport1list_bag()
    {
        try{
        $user = Auth::user();
        $ids = request('ids');
        $order1 = request('order1');
        $order2 = request('order2');
        $order3 = request('order3');
        // $ids = ['97','98'];
        if($order1==""){
            $order1="id";
        }
        if($order2==""){
            $order2="id";
        }
        if($order3==""){
            $order3="id";
        }
        // $ids = ['97','98'];
        $data2 = ["result"];
        $dataresult= ["result"];
        $result= "";
        $resnum = 0 ;
        $ord_data = DB::table('mod_order_detail')
        ->select(
                DB::raw("(select sum(d.pkg_num) from mod_order_detail d where ord_id = mod_order_detail.ord_id) as sum_num "), 
                "mod_order_detail.goods_no",
                DB::raw("mod_order_detail.pkg_num as newnum"),
                'mod_order.*'
                )
        ->leftJoin('mod_order', 'mod_order_detail.ord_id', '=', 'mod_order.id')
        ->whereIn('mod_order_detail.ord_id', $ids)
        ->orderBy('mod_order.'.$order1, 'asc')
        ->orderBy('mod_order.'.$order2, 'asc')
        ->orderBy('mod_order.'.$order3, 'asc')
        ->get();
        foreach($ord_data  as $key=>$row) {
            for($i=0; $i< $row->newnum; $i++) {
                // TPE->汐-1
                // TAO->桃-2
                // TCH->中-3
                // KAO->高-4
                // $row->dlv_addr =  mb_substr($row->dlv_addr,0,26,"utf-8");;
                if($row->is_forward !="N"){
                    $row->pick_attn=$row->dlv_attn;
                    $row->pick_tel =$row->dlv_tel;
                    $row->pick_tel2=$row->dlv_tel2;
                    $row->pick_addr=$row->dlv_addr;
                }
                if($row->etd=="0000-00-00"){
                    $row->etd =null;
                }
                if($row->s_key=="TPE"){
                    $row->s_key="汐-1";
                }
                if($row->s_key=="TAO"){
                    $row->s_key="桃-2";
                }
                if($row->s_key=="TCH"){
                    $row->s_key="中-3";
                }            
                if($row->s_key=="KAO"){
                    $row->s_key="高-4";
                }
                $infodata = DB::table('sys_area')
                ->where("dist_cd",$row->dlv_zip)
                ->first();
                $row->value1="";
                if(isset($infodata)){
                    $row->dlv_info=$infodata->city_nm.$infodata->dist_nm;
                }else{
                    $row->dlv_info="";
                }
                $dataresult["result"][$resnum]=$row;
                $resnum ++;
            }  
        }
        // dd(json_encode($dataresult));
        $test = json_encode($dataresult);
            Storage::disk('report')->put('syllistbag.json',$test);
            $datafile = base_path('/storage/app/report/syllistbag.json');
            $result = JasperPHP::process(
                base_path('/vendor/cossou/jasperphp/examples/sylbag.jrxml'),
                base_path('/public/adminreport/'.$user->email),
                array('pdf'),
                array(),
                array("driver"=>"json", "json_query" => "result", "data_file" =>  $datafile)  
            )->execute();
        }
        catch (\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => $result, 'filename'=>'error']);
        }
        return response()->json(['msg' => $result,'filename'=>$user->email]);
    }

    public function sylreport1list()
    {
        try{
        $user = Auth::user();
        $ids = request('ids');
        // $ids = ['97','98'];
        $data2 = ["result"];
        $dataresult= ["result"];
        $result= "";
        $resnum = 0 ;
        $ord_data = DB::table('mod_order')
        ->select(
                DB::raw("(select sum(d.pkg_num) from mod_order_detail d where ord_id = mod_order.id) as sum_num "), 
                'mod_order.*'
                )
        ->whereIn('mod_order.id', $ids)
        ->orderBy('mod_order.id', 'asc')
        ->get();
        foreach($ord_data  as $key=>$row) {
            for($i=0; $i< $row->sum_num; $i++) {
                $dataresult["result"][$resnum]=$row;
                $resnum ++;
            }
            
        }
        $test = json_encode($dataresult);
            Storage::disk('report')->put('syllist.json',$test);
            $datafile = base_path('/storage/app/report/syllist.json');
            $result = JasperPHP::process(
                base_path('/vendor/cossou/jasperphp/examples/Blank_A4_2.jrxml'),
                base_path('/public/adminreport/'.$user->email),
                array('pdf'),
                array(),
                array("driver"=>"json", "json_query" => "result", "data_file" =>  $datafile)  
            )->execute();
        }
        catch (\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => $result, 'filename'=>'error']);
        }
        return response()->json(['msg' => $result,'filename'=>$user->email]);
    }
    public function sylreport1list_new()
    {
        try{
        $user = Auth::user();
        $ids = request('ids');
        // $ids = ['97','98'];
        $data2 = ["result"];
        $dataresult= ["result"];
        $result= "";
        $resnum = 0 ;
        $ord_data = DB::table('mod_order')
        ->select(
                DB::raw("(select sum(d.pkg_num) from mod_order_detail d where ord_id = mod_order.id) as sum_num "), 
                'mod_order.*'
                )
        ->whereIn('mod_order.id', $ids)
        ->orderBy('mod_order.id', 'asc')
        ->get();
        foreach($ord_data  as $key=>$row) {
            for($i=0; $i< $row->sum_num; $i++) {
                $dataresult["result"][$resnum]=$row;
                $resnum ++;
            }  
            if($row->etd=="0000-00-00"){
                $row->etd =null;
            }
            // TPE->汐-1
            // TAO->桃-2
            // TCH->中-3
            // KAO->高-4
            if($row->s_key=="TPE"){
                $row->s_key="汐-1";
            }
            if($row->s_key=="TAO"){
                $row->s_key="桃-2";
            }
            if($row->s_key=="TCH"){
                $row->s_key="中-3";
            }            
            if($row->s_key=="KAO"){
                $row->s_key="高-4";
            }
        }
        $test = json_encode($dataresult);
            Storage::disk('report')->put('syllist.json',$test);
            $datafile = base_path('/storage/app/report/syllist.json');
            $result = JasperPHP::process(
                base_path('/vendor/cossou/jasperphp/examples/sylnew.jrxml'),
                base_path('/public/adminreport/'.$user->email),
                array('pdf'),
                array(),
                array("driver"=>"json", "json_query" => "result", "data_file" =>  $datafile)  
            )->execute();
        }
        catch (\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => $result, 'filename'=>'error']);
        }
        return response()->json(['msg' => $result,'filename'=>$user->email]);
    }



    public function generatePdf()
    {        
        //jasper ready to call

        // require __DIR__.'/../vendor/autoload.php';
        $input = __DIR__ . '/vendor/geekcom/phpjasper/examples/hello_world.jasper';  
        $output = __DIR__ . '/vendor/geekcom/phpjasper/examples';    
        $options = [ 
            'format' => ['pdf', 'rtf'] 
        ];
        
        $jasper = new PHPJasper;
        
        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
    }

    public function sylnewreportByowner() {
        try{
            
            $user       = Auth::user();
            $mainid     = request('ids');
            $dataresult = ["result"];
            $result     = "";
            $resnum     = 0 ;
            $totalboxnum = request('totalboxnum');
            $orderdata = DB::table('mod_order')
            ->where('id', $mainid)
            ->first();
            $logarray = array();
            $reportdata = DB::table('mod_order_weblink')
            ->where('pick_no', $orderdata->ord_no)
            ->get();

            foreach($reportdata  as $key=>$row) {
                $detail = DB::table('mod_order_detail')
                ->where('ord_id', $mainid)
                ->where('goods_no', $row->product_no)
                ->first();

                if(isset($detail)) {
                    $row->boxtotal = $detail->box_no;
                    $boxnum = (int)$detail->box_no;
                    for ($i=1; $i <= $totalboxnum; $i++) { 

                        $inboxnum = DB::table('mod_box_detail')
                        ->where('sys_ord_no', $orderdata->sys_ord_no)
                        ->where('box_no', $i)
                        ->count();
                        if ($inboxnum == 0) {
                            $inboxnum = '';
                        }
                        $newarray =  array(
                            "id"             => $row->id,
                            "pick_no"        => $row->pick_no,
                            "cust_no"        => $row->cust_no,
                            "cust_name"      => $row->cust_name,
                            "truck_cmp_name" => $row->truck_cmp_name,
                            "purchase_no"    => $row->purchase_no,
                            "purchase_by"    => $row->purchase_by,
                            "product_no"     => $row->product_no,
                            "goods_no"       => $row->goods_no,
                            "product_name"   => $row->product_name,
                            "global_barcode" => $row->global_barcode,
                            "quantity"       => $row->quantity,
                            "qty"            => $row->qty,
                            "bos_no"         => $row->bos_no,
                            "total_box_num"  => $row->total_box_num,
                            "color"          => $row->color,
                            "shape"          => $row->shape,
                            "size"           => $row->size,
                            "category"       => $row->category,
                            "product_length" => $row->product_length,
                            "product_width"  => $row->product_width,
                            "product_high"   => $row->product_high,
                            "product_weight" => $row->product_weight,
                            "box_length"     => $row->box_length,
                            "box_width"      => $row->box_width,
                            "box_height"     => $row->box_height,
                            "box_weight"     => $row->box_weight,
                            "created_by"     => $row->created_by,
                            "updated_by"     => $row->updated_by,
                            "g_key"          => $row->g_key,
                            "c_key"          => $row->c_key,
                            "s_key"          => $row->s_key,
                            "d_key"          => $row->d_key,
                            "excel_name"     => $row->excel_name,
                            "created_at"     => $row->created_at,
                            "updated_at"     => $row->updated_at,
                            "boxtotal"       => $row->boxtotal,
                            "inboxnum"       => $inboxnum,
                        ) ;
                        $dataresult["result"][] = $newarray;

                    }
                }else {
                    $row->boxtotal = 0;
                    $row->inboxnum = '';
                    $dataresult["result"][] = $row;
                }
            }
            // dd( $dataresult["result"]);
            // foreach($reportdata  as $key=>$row) {
                
            // }

            $ownercode  = $orderdata->owner_cd;
            $reportType = ''; 
            // $reportType = 'pchome';
            if ($ownercode == '2074503') {
                $reportType = 'etmall';
            } else if($ownercode == '2096600') {
                $reportType = 'seano';
            } else if($ownercode == '2201890') {
                $reportType = 'pchome';
            } else {
                return response()->json(['msg' => 'error','filename'=>$user->email]);
            }
            $now         = time();
            // dd(json_encode($dataresult));
            $test = json_encode($dataresult);
            Storage::disk('report')->put('webreport.json',$test);
            $datafile = base_path('/storage/app/report/webreport.json');
            $result = JasperPHP::process(
                base_path('/vendor/cossou/jasperphp/examples/'.$reportType.'.jrxml'),
                base_path('/public/adminreport/'.$user->email.'_'.$now),
                array('pdf'),
                array(),
                array("driver"=>"json", "json_query" => "result", "data_file" =>  $datafile)  
            )->execute();
        }
        catch (\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => $result, 'message'=>$e->getMessage(), 'line'=>$e->getLine() ]);
        }

        // $printerName = 'TSC TE310 (複件 1)';
        $printerName = DB::table('bscode')
        ->where('cd_type', 'PINTERNAME')
        ->where('cd', 'worksheet1')
        ->value('cd_descp');

        if(isset($printerName)) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => env('PRINTER_SERVER').'printer_no_syl1?type=queue',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$printerName.'||'.env('REAL_STORAGE_PATH').'admin/report/'.$user->email.'_'.$now.'.pdf',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: text/plain',
                'Authorization: Basic YWRtaW46YWRtaW4=',
                'Cookie: JSESSIONID=node05c140b8i4ljh1lov1vlxjpdq210.node0; JSESSIONID=node014angzgqbh18l189i9v4a60t6v4.node0'
            ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
        }

        return response()->json(['msg' => $result,'filename'=>$user->email]);
    }

    public function autosylreport1list_new()
    {
        try{
            $nowlog = time();
            \Log::info("start autosylreport1list_new");
            \Log::info($nowlog);
            $user = Auth::user();
            $ids = request('ids');
            $totalboxnum = request('totalboxnum');
            // $ids = ['97','98'];
            $data2 = ["result"];
            $dataresult= ["result"];
            $result= "";
            $resnum = 0 ;
            $ord_data = DB::table('mod_order')
            ->select(
                    DB::raw("(select sum(d.pkg_num) from mod_order_detail d where ord_id = mod_order.id) as sum_num "), 
                    'mod_order.*'
                    )
            ->whereIn('mod_order.id', $ids)
            ->orderBy('mod_order.id', 'asc')
            ->get();

            $order = DB::table('mod_order')
            ->where('id', $ids)
            ->first();

            DB::table('mod_order_detail')
            ->where('ord_id', $ids)
            ->update([
                'box_no' => $totalboxnum
            ]);

            $printflag = request('printflag');
            foreach($ord_data  as $key=>$row) {
                $totalboxnum >=1 ? $totalboxnum : 1;
                $row->sum_num = $totalboxnum;
                for($i=0; $i< $totalboxnum; $i++) {
                    $dataresult["result"][$resnum]=$row;
                    $resnum ++;
                }
                if($row->etd=="0000-00-00"){
                    $row->etd =null;
                }
                // TPE->汐-1
                // TAO->桃-2
                // TCH->中-3
                // KAO->高-4
                if($row->s_key=="TPE"){
                    $row->s_key="汐-1";
                }
                if($row->s_key=="TAO"){
                    $row->s_key="桃-2";
                }
                if($row->s_key=="TCH"){
                    $row->s_key="中-3";
                }            
                if($row->s_key=="KAO"){
                    $row->s_key="高-4";
                }
            }
            $now         = time();
            $test = json_encode($dataresult);
            Storage::disk('report')->put('syllist.json',$test);
            $datafile = base_path('/storage/app/report/syllist.json');
            $result = JasperPHP::process(
                base_path('/vendor/cossou/jasperphp/examples/autosylnew.jrxml'),
                base_path('/public/adminreport/'.$user->email.'_'.$now),
                array('pdf'),
                array(),
                array("driver"=>"json", "json_query" => "result", "data_file" =>  $datafile)  
            )->execute();
            $nowlog = time();
            \Log::info("finish create report autosylreport1list_new");
            \Log::info($nowlog);

            // $printerName = 'TSC TE310 (複件 1)';
            $printerName = DB::table('bscode')
            ->where('cd_type', 'PINTERNAME')
            ->where('cd', 'worksheet1')
            ->value('cd_descp');
    
            if($printflag =="Y" && isset($printerName)) {
                $nowlog = time();
                \Log::info("before call mq");
                \Log::info($nowlog);
                $curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => env('PRINTER_SERVER').'printer_no_syl1?type=queue',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>$printerName.'||'.env('REAL_STORAGE_PATH').'admin/report/'.$user->email.'_'.$now.'.pdf',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: text/plain',
                    'Authorization: Basic YWRtaW46YWRtaW4=',
                    'Cookie: JSESSIONID=node05c140b8i4ljh1lov1vlxjpdq210.node0; JSESSIONID=node014angzgqbh18l189i9v4a60t6v4.node0'
                ),
                ));
                $response = curl_exec($curl);
                $nowlog = time();
                \Log::info("after call mq");
                \Log::info($nowlog);
                curl_close($curl);
            }
        }
        catch (\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => $result, 'filename'=>'error', 'message'=>$e->getMessage() ,  'line'=>$e->getLine()   ]);
        }
        return response()->json(['msg' => $result,'filename'=>$user->email]);
    }

    public function autosylreport1list_bag()
    {
        try{
        $user      = Auth::user();
        $ids       = request('ids');
        $order1    = request('order1');
        $order2    = request('order2');
        $order3    = request('order3');
        $printflag = request('printflag');
        $totalboxnum = request('totalboxnum');

        $nowlog = time();
        \Log::info("start autosylreport1list_bag");
        \Log::info($nowlog);
        // $ids = ['97','98'];
        if($order1==""){
            $order1 = "id";
        }
        if($order2==""){
            $order2 = "id";
        }
        if($order3==""){
            $order3 = "id";
        }
        
        // $ids = ['97','98'];
        $data2      = ["result"];
        $dataresult = ["result"];
        $result     = "";
        $resnum     = 0 ;
        $ord_data   = DB::table('mod_order_detail')
        ->select(
                DB:: raw("(select sum(d.pkg_num) from mod_order_detail d where ord_id = mod_order_detail.ord_id) as sum_num "),
                "mod_order_detail.goods_no",
                'mod_order.*'
                )
        ->leftJoin('mod_order', 'mod_order_detail.ord_id', '=', 'mod_order.id')
        ->whereIn('mod_order_detail.ord_id', $ids)
        ->orderBy('mod_order.'.$order1, 'asc')
        ->orderBy('mod_order.'.$order2, 'asc')
        ->orderBy('mod_order.'.$order3, 'asc')
        ->take(1)
        ->get();

        $order = DB::table('mod_order')
        ->where('id', $ids)
        ->first();

        DB::table('mod_order_detail')
        ->where('ord_id', $ids)
        ->update([
            'box_no' => $totalboxnum
        ]);

        foreach($ord_data  as $key=>$row) {
            $totalboxnum >=1 ? $totalboxnum : 1;
            $row->sum_num = $totalboxnum;
            for($i=0; $i< $totalboxnum; $i++) {
                // TPE->汐-1
                // TAO->桃-2
                // TCH->中-3
                // KAO->高-4
                // $row->dlv_addr =  mb_substr($row->dlv_addr,0,26,"utf-8");;
                if($row->is_forward !="N"){
                    $row->pick_attn = $row->dlv_attn;
                    $row->pick_tel  = $row->dlv_tel;
                    $row->pick_tel2 = $row->dlv_tel2;
                    $row->pick_addr = $row->dlv_addr;
                }
                if($row->is_forward==null){
                    $row->is_forward = "Y";
                }
                if($row->etd=="0000-00-00"){
                    $row->etd = null;
                }
                if($row->s_key=="TPE"){
                    $row->s_key = "汐-1";
                }
                if($row->s_key=="TAO"){
                    $row->s_key = "桃-2";
                }
                if($row->s_key=="TCH"){
                    $row->s_key = "中-3";
                }            
                if($row->s_key=="KAO"){
                    $row->s_key = "高-4";
                }
                $infodata = DB::table('sys_area')
                ->where("dist_cd",$row->dlv_zip)
                ->first();
                $row->value1 = "";
                if(isset($infodata)){
                    $row->dlv_info = $infodata->city_nm.$infodata->dist_nm;
                }else{
                    $row->dlv_info = "";
                }
                $dataresult["result"][$resnum] = $row;
                $resnum ++;
            }  
        }
        $now         = time();
        // dd(json_encode($dataresult));
        $test = json_encode($dataresult);
            Storage:: disk('report')->put('syllistbag.json',$test);
            $datafile = base_path('/storage/app/report/syllistbag.json');
            $result   = JasperPHP::process(
                base_path('/vendor/cossou/jasperphp/examples/autosylbag.jrxml'),
                base_path('/public/adminreport/'.$user->email.'_'.$now),
                array('pdf'),
                array(),
                array("driver"=>"json", "json_query" => "result", "data_file" =>  $datafile)  
            )->execute();
        }
        catch (\Exception $e) {
            \Log:: info($e->getLine());
            \Log:: info($e->getMessage());
            return response()->json(['msg' => $result, 'filename'=>'error', 'message'=>$e->getMessage() ,  'line'=>$e->getLine()   ]);
        }

        $nowlog = time();
        \Log::info("finish create autosylreport1list_bag");
        \Log::info($nowlog);
        // $printerName = 'TSC TE310 (複件 1)';
        $printerName = DB::table('bscode')
        ->where('cd_type', 'PINTERNAME')
        ->where('cd', 'worksheet2')
        ->value('cd_descp');

        if($printflag =="Y" && isset($printerName)) {
            $nowlog = time();
            \Log::info("before call mq autosylreport1list_bag");
            \Log::info($nowlog);
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => env('PRINTER_SERVER').'printer_no_syl1?type=queue',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$printerName.'||'.env('REAL_STORAGE_PATH').'admin/report/'.$user->email.'_'.$now.'.pdf',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: text/plain',
                'Authorization: Basic YWRtaW46YWRtaW4=',
                'Cookie: JSESSIONID=node05c140b8i4ljh1lov1vlxjpdq210.node0; JSESSIONID=node014angzgqbh18l189i9v4a60t6v4.node0'
            ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);

            $nowlog = time();
            \Log::info("after call mq autosylreport1list_bag");
            \Log::info($nowlog);
        }
        return response()->json(['msg' => $result,'filename'=>$user->email]);

    }

    public function autosylreport1list_new_test()
    {
        try{
            $nowlog = time();
            \Log::info("start autosylreport1list_new");
            \Log::info($nowlog);
            $pdfData = array();
            $user = Auth::user();
            $ids = request('ids');
            $totalboxnum = request('totalboxnum');
            // $ids = ['97','98'];
            $data2 = ["result"];
            $dataresult= ["result"];
            $result= "";
            $resnum = 0 ;
            $ord_data = DB::table('mod_order')
            ->select(
                    DB::raw("(select sum(d.pkg_num) from mod_order_detail d where ord_id = mod_order.id) as sum_num "), 
                    'mod_order.*'
                    )
            ->whereIn('mod_order.id', $ids)
            ->orderBy('mod_order.id', 'asc')
            ->get();

            $order = DB::table('mod_order')
            ->where('id', $ids)
            ->first();

            DB::table('mod_order_detail')
            ->where('ord_id', $ids)
            ->update([
                'box_no' => $totalboxnum
            ]);

            $printflag = request('printflag');
            foreach($ord_data  as $key=>$row) {
                $totalboxnum >=1 ? $totalboxnum : 1;
                $row->sum_num = $totalboxnum;
                for($i=0; $i< $totalboxnum; $i++) {
                    $dataresult["result"][$resnum]=$row;
                    $resnum ++;

                    if($row->etd=="0000-00-00"){
                        $row->etd =null;
                    }
                    // TPE->汐-1
                    // TAO->桃-2
                    // TCH->中-3
                    // KAO->高-4
                    if($row->s_key=="TPE"){
                        $row->s_key="汐-1";
                    }
                    if($row->s_key=="TAO"){
                        $row->s_key="桃-2";
                    }
                    if($row->s_key=="TCH"){
                        $row->s_key="中-3";
                    }            
                    if($row->s_key=="KAO"){
                        $row->s_key="高-4";
                    }
                    foreach($row as $index=>$value) {
                        $row->$index == null ? '' : $value;
                    }
                    array_push($pdfData,$row);
                    $resnum ++;
                }
            }
            \Log::info("sql finished autosylreport1list_new");
            \Log::info($nowlog);
            $pdfData = array();
            // $now         = time();
            // $test = json_encode($dataresult);
            // Storage::disk('report')->put('syllist.json',$test);
            // $datafile = base_path('/storage/app/report/syllist.json');
            // $result = JasperPHP::process(
            //     base_path('/vendor/cossou/jasperphp/examples/autosylnew.jrxml'),
            //     base_path('/public/adminreport/'.$user->email.'_'.$now),
            //     array('pdf'),
            //     array(),
            //     array("driver"=>"json", "json_query" => "result", "data_file" =>  $datafile)  
            // )->execute();
            // $nowlog = time();
            // \Log::info("finish create report autosylreport1list_new");
            // \Log::info($nowlog);

            $data2 = ["result"];
            $data2['result'] = $pdfData;
            $test = json_encode($data2);
            Storage::disk('report')->put('syllist.json',$test);
            $datafile = base_path('/storage/app/boxpickinglist.json');
    
            $data2['data'] = $pdfData;
            $now         = time();

            $test = json_encode($data2);
            $postData = array(
                'reportFile'   => env('REAL_STORAGE_PATH_NEW').'report/autosylnew.jasper',
                'jsonFile'     => $test,
                'destFileName' => base_path('/public/adminreport/'.$user->email.'_'.$now.'.pdf'),
            );

            $res = $this->sendReportData($postData);

            // $printerName = 'TSC TE310 (複件 1)';
            $printerName = DB::table('bscode')
            ->where('cd_type', 'PINTERNAME')
            ->where('cd', 'worksheet1')
            ->value('cd_descp');
            
            // if($printflag =="Y" && isset($printerName)) {
            //     $nowlog = time();
            //     \Log::info("before call mq");
            //     \Log::info($nowlog);
            //     $curl = curl_init();
            //     curl_setopt_array($curl, array(
            //     CURLOPT_URL => env('PRINTER_SERVER').'printer_no_syl1?type=queue',
            //     CURLOPT_RETURNTRANSFER => true,
            //     CURLOPT_ENCODING => '',
            //     CURLOPT_MAXREDIRS => 10,
            //     CURLOPT_TIMEOUT => 0,
            //     CURLOPT_FOLLOWLOCATION => true,
            //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            //     CURLOPT_CUSTOMREQUEST => 'POST',
            //     CURLOPT_POSTFIELDS =>$printerName.'||'.env('REAL_STORAGE_PATH').'admin/report/'.$user->email.'_'.$now.'.pdf',
            //     CURLOPT_HTTPHEADER => array(
            //         'Content-Type: text/plain',
            //         'Authorization: Basic YWRtaW46YWRtaW4=',
            //         'Cookie: JSESSIONID=node05c140b8i4ljh1lov1vlxjpdq210.node0; JSESSIONID=node014angzgqbh18l189i9v4a60t6v4.node0'
            //     ),
            //     ));
            //     $response = curl_exec($curl);
            //     $nowlog = time();
            //     \Log::info("after call mq");
            //     \Log::info($nowlog);
            //     curl_close($curl);
            // }
        }
        catch (\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => $result, 'filename'=>'error', 'message'=>$e->getMessage() ,  'line'=>$e->getLine()   ]);
        }
        return response()->json(['msg' => $result,'filename'=>$user->email, 'urlpath'=> $printerName.'||'.env('REAL_STORAGE_PATH').'admin/report/'.$user->email.'_'.$now.'.pdf']);
    }


    public function sendReportData($postData) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL            => 'http://'.env('REPORT_SERVER').':16870/genreport',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POSTFIELDS     => json_encode($postData),
            CURLOPT_HTTPHEADER     => array(
            'Accept: application/json',
            'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function sylreportprivate()
    {
        try{
            $user = Auth::user();
            $ids = request('ids');
            // $ids = ['97','98'];
            $data2 = ["result"];
            $dataresult= ["result"];
            $result= "";
            $resnum = 0 ;
            $ord_data = DB::table('mod_order')
            ->select(
                    DB::raw("(select sum(d.pkg_num) from mod_order_detail d where ord_id = mod_order.id) as sum_num "), 
                    'mod_order.*'
                    )
            ->whereIn('mod_order.id', $ids)
            ->orderBy('mod_order.id', 'asc')
            ->get();
            foreach($ord_data  as $key=>$row) {
                for($i=0; $i< $row->sum_num; $i++) {
                    $dataresult["result"][$resnum]=$row;
                    $resnum ++;
                }  
                if($row->etd=="0000-00-00"){
                    $row->etd =null;
                }

                $dlvaddrlength =  mb_strlen($row->dlv_addr,"utf-8"); 
                $newaddr =  mb_substr($row->dlv_addr,8,$dlvaddrlength,"utf-8");
                $row->dlv_addr = '********'.$newaddr;

                $dlvattnlength =  mb_strlen($row->dlv_attn,"utf-8"); 
                $newattn = "";
                if($dlvattnlength >= 3) {
                    $newattn =  mb_substr($row->dlv_attn,0,1,"utf-8");
                    for ($i=0; $i < $dlvattnlength - 2 ; $i++) { 
                        $newattn .=  '*';
                    }
                    $newattn .=  mb_substr($row->dlv_attn,$dlvattnlength-2,1,"utf-8");
                } else {
                    $newattn =  mb_substr($row->dlv_attn,0,1,"utf-8").'*';
                }
            
                $row->dlv_attn = $newattn;
                if( $row->dlv_tel == null) {
                    $row->dlv_tel = "";
                } else {
                    $dlvdellength = mb_strlen($row->dlv_tel,"utf-8");
                    $newdel = '';
                    for ($i=0; $i < $dlvdellength - 5 ; $i++) { 
                        $newdel.=  '*';
                    }
                    $row->dlv_tel =  $newdel.mb_substr($row->dlv_tel,-5,5,"utf-8");
                }
                
                if($row->s_key=="TPE"){
                    $row->s_key="汐-1";
                }
                if($row->s_key=="TAO"){
                    $row->s_key="桃-2";
                }
                if($row->s_key=="TCH"){
                    $row->s_key="中-3";
                }            
                if($row->s_key=="KAO"){
                    $row->s_key="高-4";
                }
            }
            $test = json_encode($dataresult);
            Storage::disk('report')->put('syllist.json',$test);
            $datafile = base_path('/storage/app/report/syllist.json');
            $result = JasperPHP::process(
                base_path('/vendor/cossou/jasperphp/examples/sylnew.jrxml'),
                base_path('/public/adminreport/'.$user->email),
                array('pdf'),
                array(),
                array("driver"=>"json", "json_query" => "result", "data_file" =>  $datafile)  
            )->execute();
        }
        catch (\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return response()->json(['msg' => $result, 'filename'=>'error','message'=>$e->getMessage(),'errorline'=>$e->getLine() ]);
        }
        return response()->json(['msg' => $result,'filename'=>$user->email]);
    }

}