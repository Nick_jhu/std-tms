<?php

namespace App\Http\Controllers;


use Storage;
use App\Models\TrackingModel;
use App\Models\ModTrackingModel;
use App\Models\TransRecordModel;
use App\Models\Map\test;

use App\TsRecordModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;


class TrackingApiController extends Controller
{
    public function getTrackingByOrder($ord_no=null){
        try{
            $TrackingModel = new TrackingModel();
            $ordData  = $TrackingModel->getTrackingDataByOrder($ord_no);
            $trackingData = [];
            if(isset($ordData)) {
                $tsData = DB::table("mod_ts_record")->where("ref_no2", $ord_no)->orderBy('sort', 'desc')->get();
                if(count($tsData) > 0) {
                    foreach($tsData as $key=>$val) {
                        if($val->created_at != null) {
                            $index = substr($val->created_at, 0, 10);
                            if(!isset($trackingData[$index])) {
                                $trackingData[$index] = [];
                            }

                            $detail = [
                                "date"   => $val->created_at,
                                "title"  => $val->ts_name,
                                "descp"  => $val->ts_desc,
                                "status" => $val->ts_type
                            ];
                            
                            array_push($trackingData[$index], $detail);
                            
                            
                        }
                        
                    }
                }
            }

            if($ord_no == "12345") {
                $trackingData = [
                    [
                        [
                            "date"   => "2017-10-02 08: 00",
                            "title"  => "已抵達",
                            "descp"  => "貨物到達目的地",
                            "status" => "finished"
                        ],
                    ],
                    [
                        [
                            "date"  => "2017-10-01 14:30",
                            "title" => "已出發",
                            "descp" => "貨物已從工廠出發",
                            "status" => "A"
                        ],
                        [
                            "date"  => "2017-10-01 13:05",
                            "title" => "已排車未出發",
                            "descp" => "已排車未出發",
                            "status" => "A"
                        ],
                        [
                            "date"  => "2017-10-01 12:05",
                            "title" => "訂單成立",
                            "descp" => "訂單成立等待排車",
                            "status" => "A"
                        ],
                        
                    ]
                ];
            }
            return response()->json(['msg'=>'success', 'trackingData' => $trackingData]);
        }catch(\Exception $ex){
            return response()
            ->json(['msg'=>'error']);
        }
        
    }
    
    public function createTransRecord() {
        $ref_no1 = request('ref_no1');
        $ref_no2 = request('ref_no2');
        $ref_no3 = request('ref_no3');
        $ref_no4 = request('ref_no4');
        $ref_no5 = request('ref_no5');
        $ts_no   = request('ts_no');

        try {
            $modDlv = DB::table('mod_dlv')->select('g_key', 'c_key', 's_key', 'd_key')->where('dlv_no', $ref_no1)->first();
            
            $tsData = DB::table('mod_trans_status')
                        ->select('*')
                        // ->where('g_key', $modDlv->g_key)
                        // ->where('c_key', $modDlv->c_key)
                        // ->where('s_key', $modDlv->s_key)
                        // ->where('d_key', $modDlv->d_key)
                        ->where('id', $ts_no)
                        ->first();

            if(isset($modDlv) && isset($tsData)) {

                $modTsRecord          = new TransRecordModel;
                $modTsRecord->ts_no   = $ts_no;
                $modTsRecord->ts_type = $tsData->ts_type;
                $modTsRecord->ts_name = $tsData->ts_name;
                $modTsRecord->ts_desc = $tsData->ts_desc;
                $modTsRecord->sort    = $tsData->sort;
                $modTsRecord->ref_no1 = $ref_no1;
                $modTsRecord->ref_no2 = $ref_no2;
                $modTsRecord->ref_no3 = $ref_no3;
                $modTsRecord->ref_no4 = $ref_no4;
                $modTsRecord->ref_no5 = $ref_no5;
                $modTsRecord->g_key   = $modDlv->g_key;
                $modTsRecord->c_key   = $modDlv->c_key;
                $modTsRecord->s_key   = $modDlv->s_key;
                $modTsRecord->d_key   = $modDlv->d_key;
                $modTsRecord->save();

            }
            else {
                return response()->json(['msg'=>'error']);
            }
            
            
        }catch(\Exception $ex) {
            return response()->json(['msg'=>'error']);
        }
        
        return response()->json(['msg'=>'success']);
    }

    public function insertTracking() {
        $data   = request('data');

        $d_data = json_decode($data);
        // print_r($d_data);
        // return;
        $tm = new TrackingModel();
        for($i=0; $i<count($d_data); $i++) {
            $tsid = $d_data[$i]->tsid;
            $sys_ord_no = $d_data[$i]->sys_ord_no;

            $tsData = DB::table("mod_trans_status")->where("id", $tsid)->first();
            $orderData = DB::table("mod_order")->where("sys_ord_no", $sys_ord_no)->first();
            $user = Auth::user();

            $updateData = array();
            if($tsData->ts_type == 'B') {
                $updateData['status']      = 'SETOFF';
                $updateData['status_desc'] = '出發';
            }
            else if($tsData->ts_type == 'E') {
                $updateData['status'] = 'FINISHED';
                $updateData['status_desc'] = '配送完成';
            }
            
            if(count($updateData) > 0) {
                DB::table('mod_order')
                ->where('sys_ord_no', $sys_ord_no)
                ->where('g_key', $orderData->g_key)
                ->where('c_key', $orderData->c_key)
                ->update($updateData);
            }

            
            $tm->insertTracking('', $orderData->ord_no, '', $orderData->sys_ord_no, '', $tsData->ts_type, $tsData->order, $orderData->g_key, $orderData->c_key, $orderData->s_key, $orderData->d_key);
        }

        if(isset($orderData->dlv_email)) {
            //$TrackingModel->sendTrackingMail($ord_no, $orderData->dlv_email, '貨況通知-訂單號：'.$ord_no);
        }
        
        return response()->json(["msg" => "success"]);
    }

    public function sendGps() {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $car_no = request('car_no');
        $user_id = request('user_id');
        $lat = request('lat');
        $lng = request('lng');

        if($car_no) {
            $user = Auth::user();
            $data = [
                'car_no'     => $car_no,
                'lat'        => $lat,
                'lng'        => $lng,
                'driver_no'  => $user_id,
                'driver_nm'  => 'Tim',
                'person_no'  => null,
                'person_nm'  => null,
                'dlv_no'     => null,
                'g_key'      => 'STD',
                'c_key'      => 'STD',
                's_key'      => 'STD',
                'd_key'      => 'STD',
                'created_by' => 'Tim',
                'updated_by' => 'Tim',
            ];
    
            $TrackingModel = new TrackingModel();
            $TrackingModel->insertGps($data);
            $ModTrackingModel = new ModTrackingModel();
            $ModTrackingModel->insertGps($data);
        }
        else {
            return response()->json(["msg" => "error"]);
        }
        

        return response()->json(["msg" => "success"]);
    }

    public function testGpsData(Request $request=null) {
        //$user = Auth::user();

        $lng = 0;
        $lat = 0;
        $insertData = array();
        $now = date("Y-m-d H:i:s");

        try {
            $lng       = $request->lng;
            $lat       = $request->lat;
            $speed     = $request->speed;
            //$timestamp = $request->timestamp;

            $data = [
                'car_no'        => '123-456',
                'lat'           => $lat,
                'lng'           => $lng,
                'speed'         => $speed,
                //'location_time' => $timestamp,
                'driver_no'     => null,
                'driver_nm'     => 'test',
                'person_no'     => null,
                'person_nm'     => null,
                'dlv_no'        => null,
                'g_key'         => '4TH',
                'c_key'         => '4TH',
                's_key'         => '4TH',
                'd_key'         => '4TH',
                'created_by'    => 'test',
                'updated_by'    => 'test',
                'created_at'    => $now,
                'updated_at'    => $now,
            ];

            array_push($insertData, $data);


            $TrackingModel = new TrackingModel();
            $TrackingModel->insertGps($insertData);
            $ModTrackingModel = new ModTrackingModel();
            $ModTrackingModel->insertGps($insertData);
        }
        catch(\Exception $e) {
            return response()->json(["msg"=>"error","data"=>$e->getMessage()]);
        }

        return response()->json(["msg"=>"success"]);
    }

    public function getCarLocation() {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $data = DB::table('mod_car_real_data')->orderBy('created_at', 'desc')->first();
        return response()->json($data);
    }


    public function getCarPath() {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $s_date = request('s_date');
        $e_date = request('e_date');
        $car_no = request('car_no');
        // $data = DB::table('mod_car_real_data')->select('lat','lng', 'speed', 'location_time', 'location_time')->where('created_at', '>=', $s_date)->where('created_at', '<=', $e_date)->where('car_no', $car_no)->orderBy('id', 'asc')->get();
        $result = [];

        $data =  DB::table('mod_car_real_data')           //选择使用users集合
        ->select('lat','lng', 'speed', 'location_time', 'location_time')
        ->where('created_at', '>=', $s_date)
        ->where('created_at', '<=', $e_date)
        ->where('car_no', $car_no)
        ->orderBy('id', 'asc')
        ->get();

        // foreach($data as $key=>$row) {
        //     if($key > 0) {
        //         $lat2 = $row->lat;
        //         $lng2 = $row->lng;

        //         $lat1 = $data[$key - 1]->lat;
        //         $lng1 = $data[$key - 1]->lng;
        //         $s = $this->getdistance($lng1, $lat1, $lng2, $lat2);
                
        //         if($s < 100) {
        //             //array_push($result, $row);
        //         }
        //     }
        // }

        //$t = new test();
        //$data = $t->test();
        // $marksData = DB::table('mod_car_real_data')
        //     ->select('driver_nm','addr', 'cust_nm', 'created_at', 'lat', 'lng')
        //     ->where('created_at', '>=', $s_date)
        //     ->where('created_at', '<=', $e_date)
        //     ->where('car_no', $car_no)
        //     ->where('status', 'FINISHED')
        //     ->orderBy('id', 'asc')
        //     ->get();

        $marksData =  DB::table('mod_car_real_data')           //选择使用users集合
        ->select('driver_nm','addr', 'cust_nm', 'created_at', 'lat', 'lng')
        ->where('created_at', '>=', $s_date)
        ->where('created_at', '<=', $e_date)
        ->where('car_no', $car_no)
        ->where('status', 'FINISHED')
        ->orderBy('id', 'asc')
        ->get();

        $t = new test();
        $gpsData = $t->test($data);

        // $setOffData = DB::table('mod_car_real_data')
        //     ->select('driver_nm','addr', 'cust_nm', 'created_at', 'lat', 'lng')
        //     ->where('created_at', '>=', $s_date)
        //     ->where('created_at', '<=', $e_date)
        //     ->where('car_no', $car_no)
        //     ->where('status', 'SETOFF')
        //     ->orderBy('id', 'asc')
        //     ->get();

        $setOffData =  DB::table('mod_car_real_data')           //选择使用users集合
        ->select('driver_nm','addr', 'cust_nm', 'created_at', 'lat', 'lng')
        ->where('created_at', '>=', $s_date)
        ->where('created_at', '<=', $e_date)
        ->where('car_no', $car_no)
        ->where('status', 'SETOFF')
        ->orderBy('id', 'asc')
        ->get();

        $result = array(
            'gpsData' => $gpsData,
            'marksData' => $marksData,
            'setOffData' => $setOffData
        );

        return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    public function testPath() {
        $s_date = request('s_date');
        $e_date = request('e_date');
        $car_no = request('car_no');
        $data = DB::table('mod_car_real_data')
                    ->select('lat','lng')->where('created_at', '>=', $s_date)
                    ->where('created_at', '<=', $e_date)
                    ->where('car_no', $car_no)
                    ->orderBy('id', 'asc')
                    ->get();
        
        $url = 'https://roads.googleapis.com/v1/snapToRoads?path=';
        $str = '';
        foreach($data as $key=>$row) {
            if($key > 99) {
                break;
            }
            $str = $str.$row->lat.','.$row->lng.'|';
        }

        $str = rtrim($str,"|");

        $url = $url.$str.'&interpolate=true&key=AIzaSyBZ79gLT1qaLfNJDf98OJr8cT5FZ4MJ4_w';

        echo $url;
    }


    public function getCar($car_no=null) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        // $data = DB::table('mod_car_real_data')->select('lat','lng')->where('car_no', '=', $car_no)->orderBy('created_at', 'desc')->first();
        $data =  DB::table('mod_car_real_data')           //选择使用users集合
        ->select('lat','lng','car_no','speed','power','created_at')
        ->where('car_no', $car_no)
        ->orderBy('created_at', 'desc')
        ->first();


        return response()->json($data, 200, [], JSON_NUMERIC_CHECK);
    }

    public function initgetcar() {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $user = Auth::user();
        $car_no = DB::table('mod_car')->where('g_key', '=',$user->g_key)->get();
        $finaldata=[];
        foreach($car_no as $row) {
            $data =  DB::table('mod_car_real_data')           //选择使用users集合
            ->select('lat','lng','car_no','speed','power','created_at')
            ->where('car_no', $row->car_no)
            ->orderBy('created_at', 'desc')
            ->first();
            array_push($finaldata, $data);
        }
        return response()->json($finaldata, 200, [], JSON_NUMERIC_CHECK);
    }

    public function trackingView() {
        $c = request('c');
        $user = Auth::user();
        // if($user == null ) {
        //     return redirect(config('backpack.base.route_prefix', 'admin').'/'.'404');
        // }
        try {
            $dc = Crypt::decrypt($c);
        
            $a = explode(';', $dc);

            $cData = DB::table('sys_customers')->select('cname')->where('c_key', $a[1])->where('cust_no', $a[0])->first();
            return view('queryTracking.index')->with(['cname' => $cData->cname, 'c' => $c]);
        }
        catch(\Exception $ex) {
            $cData = DB::table('sys_customers')->select('cname')->where('c_key', "SYL")->where('cust_no', "SYL")->first();
            return view('queryTracking.index')->with(['cname' => '', 'c' => $c]);
        }
    }
    
    public function getTsRecord($ordNo=null){
        $c = request('c');
        $sysOrdNo = '';
        try{
            $dc = "";
            $a = array();
            if( $c==""|| $c ==null){
                $a = array("SYL","SYL");
            }else{
                $dc = Crypt::decrypt($c);
                $a = explode(';', $dc);
            }
            
            $TrackingModel = new TrackingModel();
            // $ordData  = $TrackingModel->getTrackingDataByOrder($ordNo);
            $trackingData = [];
            $tsStatusData = DB::table("mod_trans_status")->where("c_key", $a[1])->orderBy('order', 'asc')->get();
            $maxdata = array();
            // if(isset($ordData)) {
                if(count($tsStatusData) > 0) {
                    //抓同狀態的最後一筆 建立時間的系統訂單號
                    $maxdata = DB::table("mod_ts_record")->where("ref_no2",$ordNo)->orderBy('sort', 'desc')->orderBy('created_at', 'desc')->first();
                    if(!isset($maxdata)){
                        $maxdata = DB::table("mod_ts_record")->where("ref_no4",$ordNo)->orderBy('sort', 'desc')->orderBy('created_at', 'desc')->first();
                    }
                    if(!isset($maxdata)){
                        //無資料不顯示
                        return response()->json(['msg'=>'success', 'trackingData' => $trackingData, 'sysOrdNo' => $sysOrdNo]);
                    }
                    $tsData = DB::table("mod_ts_record")->where("id","<=",$maxdata->id)->where("ref_no2", $ordNo)->orderBy('sort', 'asc')->orderBy('created_at', 'asc')->get();
                    if(count($tsData)>0 ){

                    }else{
                        $tsData = DB::table("mod_ts_record")->where("ref_no4", $ordNo)->orderBy('sort', 'asc')->orderBy('created_at', 'asc')->get();
                    }
                    foreach($tsStatusData as $key=>$row) {
                        $detail = [
                            "date"    => null,
                            "title"   => $row->ts_name,
                            "descp"   => $row->ts_desc,
                            "status"  => "",
                            "type"    => $row->ts_type,
                            "ordstatus"    => ""
                        ]; 
                        foreach($tsData as $val) {
                            if($val->ts_no == $row->id) {
                                $detail['date']   = substr($val->created_at, 0, 16);
                                $detail['status'] = "done";
                                $detail['ordstatus'] ="";
                            }

                            if($val->ref_no4 != null) {
                                $sysOrdNo = $val->ref_no4;
                            }
                        }
                        array_push($trackingData, $detail);
                    }
                }
            // }
            // else {
            //     // $ordData  = $TrackingModel->getTrackingDataBySysOrder($ordNo);
            //     if(isset($ordData)) {
            //         $tsData = DB::table("mod_ts_record")->where("ref_no4", $ordNo)->orderBy('sort', 'asc')->orderBy('created_at', 'asc')->get();
            //         foreach($tsStatusData as $key=>$row) {
            //             $detail = [
            //                 "date"    => null,
            //                 "title"   => $row->ts_name,
            //                 "descp"   => $row->ts_desc,
            //                 "status"  => "",
            //                 "type"    => $row->ts_type,
            //                 "ordstatus"    => ""
            //             ]; 
            //             foreach($tsData as $val) {
            //                 if($val->ts_no == $row->id) {
            //                     $detail['date']   = substr($val->created_at, 0, 16);
            //                     $detail['status'] = "done";
            //                     $detail['ordstatus'] = "";
            //                 }

            //                 if($val->ref_no4 != null) {
            //                     $sysOrdNo = $val->ref_no4;
            //                 }
            //             }
            //             array_push($trackingData, $detail);
            //         }
            //     }

            // }
            return response()->json(['msg'=>'success', 'trackingData' => $trackingData, 'sysOrdNo' => $sysOrdNo]);
        }catch(\Exception $ex){
            return response()->json(['msg'=>'error', 'error_log' => $ex->getMessage(), 'errorline' => $ex->getLine()]);
        }
        
    }

    function getdistance($lng1,$lat1,$lng2,$lat2){
        //将角度转为狐度
        $radLat1 = deg2rad($lat1);//deg2rad()函数将角度转换为弧度
    
        $radLat2 = deg2rad($lat2);
    
        $radLng1 = deg2rad($lng1);
    
        $radLng2 = deg2rad($lng2);
    
        $a       = $radLat1-$radLat2;
    
        $b       = $radLng1-$radLng2;
    
        $s=2*asin(sqrt(pow(sin($a/2),2)+cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)))*6378.137*1000;
    
        return $s;
    }
        
}

