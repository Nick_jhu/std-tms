<?php

namespace App\Http\Controllers;

use JasperPHP; // put here

class JasperPHPController
{
	//...

    public function generateReport()
    {        
        //jasper ready to call
        JasperPHP::process(
            base_path('/vendor/cossou/jasperphp/examples/hello_world.jasper'),
            false,
            array('pdf', 'rtf'),
            array('php_version' => phpversion())
        )->execute();
    }

    public function generatePdf()
    {        
        //jasper ready to call

        // require __DIR__.'/../vendor/autoload.php';
        $input = __DIR__ . '/vendor/geekcom/phpjasper/examples/hello_world.jasper';  
        $output = __DIR__ . '/vendor/geekcom/phpjasper/examples';    
        $options = [ 
            'format' => ['pdf', 'rtf'] 
        ];
        
        $jasper = new PHPJasper;
        
        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
    }
}