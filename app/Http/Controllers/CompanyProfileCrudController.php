<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\BaseModel;
use App\Http\Requests\CompanyProfileCrudRequest as StoreRequest;
use App\Http\Requests\CompanyProfileCrudRequest as UpdateRequest;
use App\Models\CommonModel;
use App\Models\CompanyProfileModel;
use App\Models\SysSiteModel;
use Illuminate\Support\Facades\Auth;

use HTML;

class CompanyProfileCrudController extends CrudController
{
    
    public function setup() {
        $this->crud->setModel("App\Models\CompanyProfileModel");
        $this->crud->setEntityNameStrings('集團建檔', '集團建檔');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/companyProfile');
    
        $this->crud->setColumns(['name']);

        
        $this->crud->setCreateView('companyProfile.edit');
        $this->crud->setEditView('companyProfile.edit');
        $this->crud->setListView('companyProfile.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'cust_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'cname',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'ename',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'fax',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'address',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'contact',
            'type' => 'text'
        ]);       

        $this->crud->addField([
            'name' => 'cmp_abbr',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'email',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'remark',
            'type' => 'textarea'
        ]);

        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'updated_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);                     

        $this->crud->addField([
            'name' => 'status',
            'type' => 'select'
        ]);

        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'identity',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 's_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'type',
            'type' => 'hidden'
        ]);

        $this->crud->addField([
            'name' => 'city_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'city_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'zip',
            'type' => 'lookup',
            'title' => '郵地區號查詢',
            'info1' => Crypt::encrypt('sys_area'), //table
            'info2' => Crypt::encrypt("dist_cd+city_nm+dist_nm,city_nm,dist_cd,dist_nm"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "dist_cd=zip;city_nm=city_nm;dist_nm=area_nm;" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'area_id',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'area_nm',
            'type' => 'text'
        ]);
    }
    public function index()
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }
        
        $BaseModel = new BaseModel();
        $obj = null;

        $layout = $BaseModel->getLayout(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/sys_customers'));
        $obj = json_decode($layout);

        $fieldData = $BaseModel->getColumnInfo("sys_customers");
        if($obj != null) {
            array_push($fieldData, $obj);
        }

        $colModel = array('sys_customers' => $fieldData);

        $this->crud->colModel = $fieldData;

        try{
            if(Auth::user()->hasPermissionTo('companyProfile'))
            {
                return view('companyProfile.index', $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;
        $user = Auth::user();
        try{
            if(Auth::user()->hasPermissionTo('companyProfile'))
            {
                return view($this->crud->getEditView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }
    public function getData($table=null,$id) {
        $data = DB::table('sys_customers')->where('id', $id)->first();

        return response()->json($data);
    }


    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;        
        $request->type = "SELF";
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        try {
            $response = parent::storeCrud($request);
            
            if(isset($request->identity)) {
                $identity = $request->identity;
                $sysSite = new SysSiteModel;
                $customer = CompanyProfileModel::find($this->data['entry']->getKey());
                $sysSite->ename = isset($request->ename)?$request->ename:null;
                $sysSite->cname = isset($request->cname)?$request->cname:null;
                if($identity == 'G') {
                    $sysSite->g_key = isset($request->cust_no)?$request->cust_no:null;
                    $sysSite->c_key = '*';
                    $sysSite->s_key = '*';
                    $sysSite->d_key = '*';
                    $sysSite->type  = 0;

                    $customer->g_key = isset($request->cust_no)?$request->cust_no:null;
                    $customer->c_key = '*';
                    $customer->s_key = '*';
                    $customer->d_key = '*';
                }
                else if($identity == 'C') {
                    $sysSite->g_key = isset($request->g_key)?$request->g_key:null;
                    $sysSite->c_key = isset($request->cust_no)?$request->cust_no:null;
                    $sysSite->s_key = '*';
                    $sysSite->d_key = '*';
                    $sysSite->type  = 1;

                    $customer->g_key = isset($request->g_key)?$request->g_key:null;
                    $customer->c_key = isset($request->cust_no)?$request->cust_no:null;
                    $customer->s_key = '*';
                    $customer->d_key = '*';
                }
                else if($identity == 'S') {
                    $sysSite->g_key = isset($request->g_key)?$request->g_key:null;
                    $sysSite->c_key = isset($request->c_key)?$request->c_key:null;
                    $sysSite->s_key = isset($request->cust_no)?$request->cust_no:null;
                    $sysSite->d_key = '*';
                    $sysSite->type  = 2;

                    $customer->g_key = isset($request->g_key)?$request->g_key:null;
                    $customer->c_key = isset($request->c_key)?$request->c_key:null;
                    $customer->s_key = isset($request->cust_no)?$request->cust_no:null;
                    $customer->d_key = '*';
                }
                else if($identity == 'D') {
                    $sysSite->g_key = isset($request->g_key)?$request->g_key:null;
                    $sysSite->c_key = isset($request->c_key)?$request->c_key:null;
                    $sysSite->s_key = isset($request->s_key)?$request->s_key:null;
                    $sysSite->d_key = isset($request->cust_no)?$request->cust_no:null;
                    $sysSite->type  = 3;

                    $customer->g_key = isset($request->g_key)?$request->g_key:null;
                    $customer->c_key = isset($request->c_key)?$request->c_key:null;
                    $customer->s_key = isset($request->s_key)?$request->s_key:null;
                    $customer->d_key = isset($request->cust_no)?$request->cust_no:null;
                }
                $sysSite->save();
                $customer->save();
            }
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        unset($request['g_key']);
        unset($request['c_key']);
        unset($request['s_key']);
        unset($request['d_key']);
        try {
            $response = parent::updateCrud($request);            
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        
        return ["msg"=>"success", "response"=>$response];
    }
    
    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        

        return $this->crud->delete($id);
    }


    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $companyProfile = CompanyProfileModel::find($ids[$i]);
                $companyProfile->delete();
            }
        }

        return response()->json(array('msg' => 'success'));
    }
}
