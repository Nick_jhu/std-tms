<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Models\SenaoModel;
use App\Models\DlvModel;
use App\Models\OrderMgmtModel;
use App\Models\DlvPlanModel;
use App\Models\CarLoadModel;
use App\Models\OrderPackModel;
use App\Models\BaseModel;
use App\Models\CommonModel;
use App\Models\TransRecordModel;
use App\Models\TrackingModel;
use App\Models\CalculateModel;
use App\Models\FcmModel;
use App\Models\SysNoticeModel;
use App\Models\GoogleMapModel;

use App\Http\Requests\DlvCrudRequest as StoreRequest;
use App\Http\Requests\DlvCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store as Session;

class DlvCrudController extends CrudController
{
    
    public function setup() {
        $user = Auth::user();
        $this->crud->setModel("App\Models\DlvModel");
        $this->crud->setEntityNameStrings(trans('modDlv.titleName'), trans('modDlv.titleName'));
        $this->crud->setRoute(config('backpack.base.route_prefix').'/TranPlanMgmt/SendCar');
    
        $this->crud->setColumns(['name']);
        
        $this->crud->setCreateView('tranPlan.edit');
        $this->crud->setEditView('tranPlan.edit');
        $this->crud->setListView('tranPlan.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'cust_no',
            'type' => 'lookup',
            'title' => '卡車公司查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname"), //column
            'info3' => Crypt::encrypt("status='B' AND cust_type='CT003' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
            'info4' => "cust_no=cust_no" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'dlv_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'status',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'load_rate',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'load_tweight',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'load_tcbm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'car_no',
            'type' => 'lookup',
            'title' => '車輛查詢',
            'info1' => Crypt::encrypt('mod_car'), //table
            'info2' => Crypt::encrypt("car_no,car_no,car_type,car_type_nm,load_rate,driver_nm"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "car_no=car_no;car_type=car_type;load_rate=load_rate;driver_nm=driver_nm", //field mapping,
            'triggerfunc' => 'carNoCallBack'
        ]);

        $this->crud->addField([
            'name' => 'car_type',
            'type' => 'select',
            'options' => DB::table('bscode')
                            ->select('cd as code', 'cd_descp as descp')
                            ->where('cd_type', 'CARTYPE')
                            ->where('g_key', $user->g_key)
                            ->where('c_key', $user->c_key)
                            ->get()
        ]);

        $this->crud->addField([
            'name' => 'driver_nm',
            'type' => 'text',
            // 'title' => '司機查詢',
            // 'info1' => Crypt::encrypt('users'), //table
            // 'info2' => Crypt::encrypt("name,name,phone"), //column
            // 'info3' => Crypt::encrypt(""), //condition
            // 'info4' => "name=driver_nm;phone=driver_phone" //field mapping
        ]);
        
        $this->crud->addField([
            'name' => 'driver_phone',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_date',
            'type' => 'date_picker'
        ]);

    }

    public function get($cust_no=null) 
    {
        $sample_detail = [];
        //dd($cust_no);
        if($cust_no != null) {
            //dd($this->crud);
            //$sample_detail = CustomerModel::find($cust_no)->sampleDetail;//DB::table('sample_detail')->get()->where('cust_no', $cust_no);
            $this_query = DB::table('sample_detail');
            $this_query->where('cust_no', $cust_no);
            $sample_detail = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $sample_detail,
        );

        return response()->json($data);
    }

    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $today      = new \DateTime();
        $str_date    = $today->format('Ym');
        $params = array(
            "c_key"   => $user->c_key,
            "date_ym" => substr($str_date, 2, strlen($str_date))
        );
        $BaseModel = new BaseModel();
        $dlvNo = $BaseModel->getAutoNumber("dlv",$params);
        $request->merge(array('dlv_no' => $dlvNo));

        try {
            $response = parent::storeCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $driverNm = $request->driver_nm;
            $carNo    = $request->car_no;
            $carType  = $request->car_type;
            $custNo   = $request->cust_no;

            $response = parent::updateCrud($request);

            $dlvData = DlvModel::find($request->id);
            if(isset($driverNm) || isset($carNo) || isset($carType) || isset($custNo)) {
                $updateData = [
                    'driver'       => $dlvData->driver_nm, 
                    'car_no'       => $dlvData->car_no, 
                    'car_type'     => $dlvData->car_type,
                    'truck_cmp_no' => $dlvData->cust_no
                ];
                if(isset($custNo)) {
                    $custData = DB::table('sys_customers')->where('cust_no', $custNo)->first();
                    if(isset($custData)) {
                        $truck_cmp_nm = $custData->cname;
                        $updateData['truck_cmp_nm'] = $truck_cmp_nm;
                    }
                }
                $ordsData = OrderMgmtModel::where('dlv_no', $dlvData->dlv_no)->update($updateData);                
            }
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage(), "id" => $dlvData->dlv_no];
        }

        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response];
    }

    public function getPack() {
        $ordArray = request('ordArray');

        $data[] = array(
            'Rows' => [],
        );

        if(count($ordArray) > 0) {
            
            $ordPack = DB::table('mod_order_pack')
                        ->where('load_comp', 'N')
                        ->whereIn('ord_no', $ordArray)
                        ->orderBy('ord_no', 'asc')
                        ->orderBy('pack_no', 'asc')
                        ->get();
            $data['Rows'] = $ordPack;
        }

        return response()->json($data);
    }

    public function getCarLoad($dlv_no=null) {
        $carLoadData = [];
        if($dlv_no != null) {
            $this_query = DB::table('mod_car_load');
            $this_query->where('dlv_no', $dlv_no);
            $carLoadData = $this_query->get();
            $carLoadData = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $carLoadData,
        );

        return response()->json($data);
    }

    public function handPickup() {
        $data = request('data');

        
        try {
            $carLoadData = json_decode($data);

            
            foreach($carLoadData as $key=>$val) {
                $carLoad = new CarLoadModel;
                $carLoad->dlv_no = $val->dlv_no;
                $carLoad->ord_no = $val->ord_no;
                $carLoad->pack_no = $val->pack_no;
                $carLoad->save();

                OrderPackModel::where('ord_no', $val->ord_no)
                ->where('pack_no', $val->pack_no)
                ->update(['load_comp' => 'Y']);
            }
            
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        return ["msg"=>"success"];
    }


    public function autoPickup() {
        $data = request('data');

        
        try {
            $carLoadData = json_decode($data);

            $dlv_no = request('dlv_no');

            $mod_dlv = DB::table('mod_dlv')->select('load_rate', 'load_tcbm')->where('dlv_no', $dlv_no)->first();

            $truck_cbm  = $mod_dlv->load_rate * $mod_dlv->load_tcbm;
            $ttl_cbm    = 0;
            $old_ord_no = "";
            $ord_cbm    = [];
            foreach($carLoadData as $key=>$val) {
                $ord_no = $val->ord_no;
                $cbm = $val->cbm;

                if($key == 0) {
                    $old_ord_no = $ord_no;
                    $ord_cbm[$ord_no] = 0;
                }

                if($old_ord_no == $ord_no) {
                    $ord_cbm[$ord_no] += $cbm;
                }
                else {
                    $ord_cbm[$ord_no] = $cbm;
                }

                $old_ord_no = $ord_no;
            }
            
            $i = 0;
            $old_key = "";
            $put_key = [];
            $ord_no = "";
            foreach($ord_cbm as $key=>$val) {
                $ttl_cbm += $val;

                if($i == 0) {
                    $old_key = $key;
                }
                
                if($ttl_cbm > $truck_cbm) {
                    /*
                    foreach($carLoadData as $key=>$val) {
                        if(in_array($val->ord_no, $put_key)) {
                            $carLoad          = new CarLoadModel;
                            $carLoad->dlv_no  = $dlv_no;
                            $carLoad->ord_no  = $val->ord_no;
                            $carLoad->pack_no = $val->pack_no;
                            $carLoad->save();
        
                            OrderPackModel::where('ord_no', $val->ord_no)
                            ->where('pack_no', $val->pack_no)
                            ->update(['load_comp' => 'Y']);

                            $ord_no = $val->ord_no;
                        }
                    }
                    $ttl_cbm = 0;
                    $temp_cbm = 0;

                    foreach($ord_cbm as $v) {
                        if(!in_array($ord_no, $put_key)) {
                            $temp_cbm += $v;
                        }
                    }

                    $modDlv    = DlvModel::where('dlv_no', $dlv_no)->first();
                    $newModDlv = $modDlv->replicate();
                    $cust_no   = $newModDlv->cust_no;
                    $car_type  = $newModDlv->car;
                    $modCar    = DB::table('mod_car')->where('cust_no', $cust_no)->get();
                    $near_cbm  = [];
                    $car_no    = "";
                    $b         = 0;
                    foreach($modCar as $k=>$carVal) {
                        $c = ($carVal->cbm * $carVal->load_rate) - $temp_cbm;
                        if($k == 0) {
                            $b = $c;
                        }

                        if($c > 0 && $c < $b) {
                            $car_no = $carVal->car_no;
                        }
                        else {
                            $b = $c;
                            $car_no = $carVal->car_no;
                        }
                    }

                    if($car_no != "") {
                        $modCar = DB::table('mod_car')->select('car_no','driver_nm','car_type')->where('cust_no', $cust_no)->where('car_no', $car_no)->first();

                        $newModDlv->car_no    = $modCar->car_no;
                        $newModDlv->driver_nm = $modCar->driver_nm;
                        $newModDlv->car_type  = $modCar->car_type;
                        $newModDlv->dlv_no    = 'DLV00002';
                        $newModDlv->save();

                        
                    }

                    */
                    break;
                }

                $i++;
                $old_key = $key;
                array_push($put_key, $key);
            }

            $all_ord = [];
            foreach($carLoadData as $key=>$val) {
                if(in_array($val->ord_no, $put_key)) {
                    $carLoad          = new CarLoadModel;
                    $carLoad->dlv_no  = $dlv_no;
                    $carLoad->ord_no  = $val->ord_no;
                    $carLoad->pack_no = $val->pack_no;
                    $carLoad->save();

                    OrderPackModel::where('ord_no', $val->ord_no)
                    ->where('pack_no', $val->pack_no)
                    ->update(['load_comp' => 'Y']);

                    $ord_no = $val->ord_no;
                }
            }
            array_push($all_ord, $val->ord_no);
            
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        $ordPack = DB::table('mod_order_pack')
                        ->where('load_comp', 'N')
                        ->whereIn('ord_no', $all_ord)
                        ->orderBy('ord_no', 'asc')
                        ->orderBy('pack_no', 'asc')
                        ->get();
        $packData['Rows'] = $ordPack;

        return ["msg"=>"success", "data" =>$packData];
    }

    public function getDlvData($dlv_no=null) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $returnData = array();
        $returnData["dlvDatas"] = DB::table('mod_dlv')->get();
        $returnData["msg"] = "success";
        
        return response()->json($returnData);
    }
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('SendCar'))
            {
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function DlvCar() {
        $title = trans('dlvCar.title');
        echo "<title>$title</title>";
        $user = Auth::user();
        //$carData = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->c_key)->where('identity', 'c')->first();
        $carData = DB::table('sys_customers_item')->where('cust_type', 'CT003')->where('c_key', $user->c_key)->where('def', '1')->first();
        $checkData = DB::table('bscode')->where('value1', 'Y')->where('cd', 'autosendtoapp')->where('g_key', $user->g_key)->where('c_key', $user->c_key)->value('value1');
        if($checkData=="Y"){
            $checkData="true";
        }
        $viewData = array(
            'cname'        => $carData->cname,
            'cust_no'        => $carData->cust_no,
            'checkData'      => $checkData
        );
        try{
            if(Auth::user()->hasPermissionTo('DlvCar'))
            {
                return view('tranPlan.dlvCar')->with('viewData', $viewData);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function DlvCartest() {
        $title = trans('dlvCar.title');
        echo "<title>$title</title>";
        $user = Auth::user();
        //$carData = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->c_key)->where('identity', 'c')->first();
        $carData = DB::table('sys_customers_item')->where('cust_type', 'CT003')->where('c_key', $user->c_key)->where('def', '1')->first();
        $carnum = DB::table('mod_car')->where('g_key', $user->g_key)->where('c_key', $user->c_key)->count();
        $dlvlimit = DB::table('bscode')->where('cd', 'DLVLIMIT')->where('cd_type', 'DLVLIMIT')->where('g_key', $user->g_key)->where('c_key', $user->c_key)->value('value1');
        $checkData = DB::table('bscode')->where('value1', 'Y')->where('cd', 'autosendtoapp')->where('g_key', $user->g_key)->where('c_key', $user->c_key)->value('value1');
        $sortData = DB::table('bscode')->where('value1', 'Y')->where('cd', 'sortdlvplan')->where('g_key', $user->g_key)->where('c_key', $user->c_key)->value('value1');
        
        $showStatus  = DB::table('bscode')->where('value1', 'Y')->where('cd_type', 'SHOWDLVORDSTATUS')->get();
        $basecondition = "(";
        
        foreach ($showStatus as $key => $row) {
            if($key == 0) {
                $basecondition = "(status = '".$row->cd."'" ;
            } else {
                $basecondition .= " or status = '".$row->cd."'" ;
            }
        }
        $basecondition .= ') and dlv_no is null ';

        $basecondition = Crypt::encrypt($basecondition);

        if($checkData=="Y"){
            $checkData="true";
        }
        if($sortData=="Y"){
            $sortData="true";
        }

        $viewData = array(
            'dlvlimit'      => (int)$dlvlimit,
            'cname'         => isset($carData->cname) ? $carData->cname : null,
            'cust_no'       => isset($carData->cust_no) ? $carData->cust_no : null,
            'checkData'     => $checkData,
            'carnum'        => $carnum,
            'sortData'      => $sortData,
            'basecondition' => $basecondition
        );
        return view('tranPlan.dlvCartest')->with('viewData', $viewData);
    }


    public function sendDlvCar(Request $request) {

        $user         = Auth::user();
        $car_no       = $request->car_no;
        $driver_nm    = $request->driver_nm;
        $driver_phone = $request->driver_phone;
        $ord_ids      = $request->ord_ids;
        $dlv_date     = $request->dlv_date;
        $carType      = $request->car_type;
        $sendToApp    = $request->sendToApp;
        $status       = "UNTREATED";
        if($sendToApp=="true"){
            $status ="SEND";
        }
        $dlv = DB::table('mod_dlv')->orderBy("id", "desc")->first();
        $dlv_id = "";
        if(isset($dlv)) {
            $id = $dlv->id + 1;
            $dlv_id = str_pad($id,3,'0',STR_PAD_LEFT);
        }
        else {
            $dlv_id = str_pad(1,3,'0',STR_PAD_LEFT);
        }
        

        $ords = DB::table('mod_order')
                    ->whereIn('id', $ord_ids)
                    ->get();

        $car = DB::table('mod_car')->where('car_no', $car_no)->first();
        
        $ttl_cbm     = 0;
        $ttl_pkg_num = 0;
        $dlv_detail  = [];
        $today       = new \DateTime();
        $str_date    = $today->format('Ym');
        $params = array(
            "c_key"   => $user->c_key,
            "date_ym" => substr($str_date, 2, strlen($str_date))
        );
        $BaseModel = new BaseModel();
        $dlv_no = $BaseModel->getAutoNumber("dlv",$params);
        if(count($ords) > 0) {
            foreach($ords as $key=>$ord) {
                // dd($ord->car_no);
                if($ord->dlv_no != null) {
                    return response()->json(["msg" => "error", "errorLog"=>$ord->ord_no.' 訂單已被指派']);
                }
                $ttl_cbm     += $ord->total_cbm;
                $ttl_pkg_num += $ord->pkg_num;

                $ordCnt = DB::table('mod_dlv_plan')
                            ->where('status', 'FINISHED')
                            ->where('dlv_type', 'P')
                            ->where('sys_ord_no', $ord->sys_ord_no)
                            ->count();
                //$ord->dlv_type != 'P' 代表有提貨在其它配送單裡

                $isUrgent = 'N';
                if($ord->is_urgent == 'Y') {
                    $isUrgent = 'Y';
                }

                if(!empty($ord->pick_addr) && $ordCnt == 0 && $ord->dlv_type != 'P') {
                    $address = $ord->pick_zip.$ord->pick_city_nm.$ord->pick_area_nm.$ord->pick_addr;
                    if($ord->g_key == 'SYL') {
                        $address = $ord->pick_zip.$ord->pick_addr;
                    }
                    $detail = array(
                        "ord_id"      => $ord->id,
                        "cust_nm"     => $ord->pick_cust_nm,
                        "dlv_type"    => 'P',
                        "ord_no"      => $ord->ord_no,
                        "sys_ord_no"  => $ord->sys_ord_no,
                        "status"      => $status,
                        "dlv_no"      => $dlv_no,
                        "addr"        => $address,
                        "etd"         => $ord->etd,
                        "sort"        => $key,
                        "remark"      => htmlspecialchars($ord->pick_remark),
                        "updated_at"  => $today->format('Y-m-d H:i:s'),
                        "updated_by"  => $user->email,
                        "created_at"  => $today->format('Y-m-d H:i:s'),
                        "created_by"  => $user->email,
                        "g_key"       => $user->g_key,
                        "c_key"       => $user->c_key,
                        "s_key"       => $user->s_key,
                        "d_key"       => $user->d_key,
                        'car_no'      => $car_no,
                        'driver_nm'   => $driver_nm,
                        'owner_nm'    => $ord->owner_nm,
                        'is_urgent'   => $isUrgent
                    );
                    array_push($dlv_detail, $detail);
                }

                
                if(!empty($ord->dlv_addr)) {
                    $address = $ord->dlv_zip.$ord->dlv_city_nm.$ord->dlv_area_nm.$ord->dlv_addr;
                    if($ord->g_key == 'SYL') {
                        $address = $ord->dlv_zip.$ord->dlv_addr;
                    }

                    $detail = array(
                        "ord_id"     => $ord->id,
                        "cust_nm"    => $ord->dlv_cust_nm,
                        "dlv_type"   => 'D',
                        "ord_no"     => $ord->ord_no,
                        "sys_ord_no" => $ord->sys_ord_no,
                        "status"     => $status,
                        "dlv_no"     => $dlv_no,
                        "addr"       => $address,
                        "etd"        => $ord->etd,
                        "sort"       => $key,
                        "remark"     => htmlspecialchars($ord->dlv_remark),
                        "updated_at" => $today->format('Y-m-d H:i:s'),
                        "updated_by" => $user->email,
                        "created_at" => $today->format('Y-m-d H:i:s'),
                        "created_by" => $user->email,
                        "g_key"      => $user->g_key,
                        "c_key"      => $user->c_key,
                        "s_key"      => $user->s_key,
                        "d_key"      => $user->d_key,
                        'car_no'      => $car_no,
                        'driver_nm'   => $driver_nm,
                        'owner_nm'    => $ord->owner_nm,
                        'is_urgent'   => $isUrgent
                    );
    
                    array_push($dlv_detail, $detail);
                }
                
            }

            $modDlv               = new DlvModel;
            $modDlv->dlv_no       = $dlv_no;
            $modDlv->dlv_date     = $dlv_date;
            $modDlv->status       = $status;
            $modDlv->load_rate    = $car->load_rate;
            $modDlv->load_tweight = 0;//$car->load_weight;
            $modDlv->load_tcbm    = $ttl_cbm;
            $modDlv->dlv_num      = count($ords);
            $modDlv->cust_no      = $car->cust_no;
            $modDlv->car_no       = $car->car_no;
            $modDlv->car_type     = $carType;
            $modDlv->car_type_nm  = $car->car_type_nm;
            $modDlv->driver_nm    = $driver_nm;
            $modDlv->driver_phone = $driver_phone;
            $modDlv->updated_by   = $user->email;
            $modDlv->created_by   = $user->email;
            $modDlv->g_key        = $user->g_key;
            $modDlv->c_key        = $user->c_key;
            $modDlv->s_key        = $user->s_key;
            $modDlv->d_key        = $user->d_key;
            $modDlv->save();

            DB::table('mod_dlv_plan')->insert($dlv_detail);

            DB::table('mod_order')
            ->whereIn('id', $ord_ids)
            ->update([
                "status"      => "SEND",
                "status_desc" => "已派單未出發",
                "car_no"      => $car->car_no,
                "truck_no"    => $car->car_no,
                "dlv_no"      => $dlv_no,
                "car_type"    => $carType,
                "is_ordered"  => "N",
                "order_tally" => "N",
                "realnum"     => 0,
                "driver"      => $driver_nm
            ]);

            foreach($ords as $key=>$ord) {
                $tsData = DB::table("mod_trans_status")->where("ts_type", "A")->where('order', 3)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->orderBy("order", "desc")->first();
                $data = [
                    'ts_no'      => $tsData->id,
                    'ts_type'    => $tsData->ts_type,
                    'sort'       => $tsData->order,
                    'ts_name'    => $tsData->ts_name,
                    'ts_desc'    => $tsData->ts_desc,
                    'ref_no1'    => $dlv_no,
                    'ref_no2'    => $ord->ord_no,
                    'ref_no3'    => $car_no,
                    'ref_no4'    => $ord->sys_ord_no,
                    'g_key'      => $ord->g_key,
                    'c_key'      => $ord->c_key,
                    's_key'      => $ord->s_key,
                    'd_key'      => $ord->d_key,
                    'created_by' => $user->name,
                    'updated_by' => $user->name,
                ];
                // if($ord->owner_cd=="0099123"){
                //     $orderMgmtModel = new OrderMgmtModel;
                //     $orderMgmtModel->insertmiedilog($ord->sys_ord_no,"DLV");
                // }
        
                $TransRecordModel = new TransRecordModel();
                $TransRecordModel->createRecord($data);

                if(isset($ord->dlv_email)) {
                    $TrackingModel = new TrackingModel();
                    //$TrackingModel->sendTrackingMail($ord->ord_no, $ord->dlv_email, '貨況通知-訂單號：'.$ord->ord_no);
                }

                // $c = new CalculateModel(); 
                // $amt = $c->calculateAmt($ord->id);
                // DB::table('mod_order')
                // ->where('id', $ord->id)
                // ->update([
                //     'amt' => $amt
                // ]);
            }
            if($sendToApp=="true"){
                $modplanid = DB::table('mod_dlv')->where('dlv_no',$dlv_no)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->value('id');
                $this->sendapp($modplanid);
            }
        }

        return response()->json(["msg" => "success"]);
    }

    public function getDlvDataByCarNo($car_no=null) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $returnData = array();
        $returnData["dlvDatas"] = DB::table('mod_dlv')->where('car_no', $car_no)->get();
        $returnData["msg"] = "success";
        
        return response()->json($returnData);
    }

    public function getDlvPlan($dlvNo=null) {
        $user = Auth::user();
        // $dlvPlanData = DB::table('mod_dlv_plan')
        //                     ->select('mod_order.dlv_cust_nm', 'mod_order.pick_cust_nm', 'mod_dlv_plan.*')
        //                     ->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no')
        //                     ->where('mod_dlv_plan.dlv_no', $dlvNo)
        //                     ->where('mod_order.c_key', $user->c_key)->get();

        $dlvPlanData = DB::table('mod_dlv_plan_view')
                            ->where('dlv_no', $dlvNo)
                            ->orderBy('id', 'asc')
                            ->where('c_key', $user->c_key)->get();

        return response()->json($dlvPlanData);
    }

    public function getStantiondata() {
        $user = Auth::user();
        $skeydata = DB::table('sys_customers')->where('g_key', $user->g_key)->where('type', 'SELF')->where('identity', 's')->get();

        return response()->json($skeydata);
    }

    public function getDlvEtaPlan($dlvNo=null) {
        $user = Auth::user();
        $g = new GoogleMapModel;

        $eta = $g->getDirectionsResult($dlvNo);

        $dlvPlanData = DB::table('mod_dlv_plan_view')
                            ->where('dlv_no', $dlvNo)
                            ->where('c_key', $user->c_key)
                            ->orderBy('id', 'asc')
                            ->get();

        foreach($dlvPlanData as $key=>$row) {
            DB::table('mod_dlv_plan')->where('id', $row->id)->update(['sys_etd' => $eta[$key]]);
        }

        return response()->json(['msg' => 'success']);
    }
    public function delDlvPlan() {
        $user     = Auth::user();
        $ids      = request('ids');
        $dlvNo    = request('dlvNo');
        if(count($ids) > 0) {          
            for($i=0; $i<count($ids); $i++) {
                $dlvNodata = $dlvNo[$i];
                    
                    DB::table('mod_order')
                    ->where('dlv_no', $dlvNodata)
                    ->update([
                        'status'      => 'UNTREATED',
                        'status_desc' => '尚未安排',
                        'dlv_no'      => null,
                        'car_no'      => null,
                        'truck_no'    => null,
                        'driver'      => null,
                        'amt'         => null,
                        'car_type'    => null,
                        'dlv_type'    => null,
                        'ord_type_nm' => null
                    ]);
                    
                    $dlvPlan = DlvPlanModel::where('dlv_no', $dlvNo[$i]);
                    $dlvPlan->delete();
                                
                    $dlv = DlvModel::where('dlv_no', $dlvNodata)->where('c_key', $user->c_key);
                    $dlv->delete();
                
                DB::table('mod_dlv')
                    ->where('dlv_no', $dlvNodata)
                    ->update(['status' => 'FINISHED']);
            }
        }

        return response()->json(['msg' => 'success']);
    }

    public function rmDlvPlan() {
        $user     = Auth::user();
        $ids      = request('ids');
        $dlvNo    = request('dlvNo');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                //$ordNo = $ids[$i];
                // $dlvPlan = DlvPlanModel::find($ids[$i]);
                $dlvPlan   = DB::connection('mysql::write')->table('mod_dlv_plan')->where('id', $ids[$i])->first();
                if(isset($dlvPlan)) {
                    $olddlvno  = $dlvPlan->dlv_no;
                    $olddlvoid = $dlvPlan->ord_id;
                    if($dlvPlan->status =="FINISHED"){
                        return response()->json(['msg' => 'error',"errorLog"=>'已完成訂單不能取消派車']);
                    }
                    $order = OrderMgmtModel::find($dlvPlan->ord_id);
                    if($dlvPlan->dlv_type == 'P') {
                        $delDlvPlan = DlvPlanModel::where('ord_id', $dlvPlan->ord_id)
                                                    ->where('dlv_no', $dlvPlan->dlv_no);
                        
                        if(isset($delDlvPlan)) {
                            $delDlvPlan->delete();

                            // $remaincount = DB::table('mod_dlv_plan')
                            // ->where('dlv_no', $olddlvno)
                            // ->where('ord_id', $olddlvoid)
                            // ->count();
                            // $order = OrderMgmtModel::find($dlvPlan->ord_id);
                            // if($remaincount==0){
                            //     $order->dlv_no      = null;
                            //     $order->status      = 'UNTREATED';
                            //     $order->status_desc      = '尚未安排';
                            // }else{
                            //     $order->status      = $order->status;
                            //     $order->status_desc      = $order->status_desc;
                            // }
                            $order->dlv_no      = null;
                            $order->status      = 'READY';
                            $order->status_desc = '可派車';
                            $order->dlv_no      = null;
                            $order->car_no      = null;
                            $order->truck_no    = null;
                            $order->driver      = null;
                            $order->amt         = null;
                            $order->car_type    = null;
                            $order->dlv_type    = null;
                            $order->ord_type_nm = null;
                            $order->save();
                            DB::table('mod_ts_order')
                            ->where('sys_ord_no', $order->sys_ord_no)
                            ->where("status_desc","運送中")
                            ->delete();
                        }
                    }
                    else {
                        $pickCnt = DlvPlanModel::where('ord_id', $dlvPlan->ord_id)
                                            ->where('dlv_type', 'P')
                                            ->where('status', 'FINISHED')
                                            ->count();

                        //20220411 
                        if($pickCnt > 0) {

                            $order              = OrderMgmtModel::find($dlvPlan->ord_id);
                            $order->dlv_type    = 'P';
                            $order->status      = 'READY_PICK';
                            $order->status_desc = '已提貨可派車';
                            $order->dlv_no      = null;
                            $order->save();
                            DB::table('mod_ts_order')
                            ->where('sys_ord_no', $order->sys_ord_no)
                            ->where("status_desc","運送中")
                            ->delete();
                        }
                        else {
                            $remaincount = DB::table('mod_dlv_plan')
                            ->where('dlv_no', $olddlvno)
                            ->where('ord_id', $olddlvoid)
                            ->where('dlv_type', "P")
                            ->count();
                            $order = OrderMgmtModel::find($dlvPlan->ord_id);
                            if($remaincount==0){
                                $order->dlv_no      = null;
                                $order->status      = 'READY';
                                $order->status_desc = '可派車';
                            }else{
                                $order->status      = $order->status;
                                $order->status_desc = $order->status_desc;
                            }
                            $order->car_no      = null;
                            $order->truck_no    = null;
                            $order->driver      = null;
                            $order->amt         = null;
                            $order->car_type    = null;
                            $order->dlv_type    = null;
                            $order->ord_type_nm = null;
                            $order->save();
                            DB::table('mod_ts_order')
                            ->where('sys_ord_no', $order->sys_ord_no)
                            ->where("status_desc","運送中")
                            ->delete();
                        }
    
                        $dlvPlan = DlvPlanModel::where('ord_id', $dlvPlan->ord_id)->where('dlv_no', $dlvPlan->dlv_no)->where('dlv_type', 'D');
                        $dlvPlan->delete();
                    }
                }
                
                
            }

            $cnt = DB::connection('mysql::write')->table('mod_dlv_plan')->where('dlv_no', $dlvNo)->count();

            if($cnt == 0) {
                $dlv = DlvModel::where('dlv_no', $dlvNo)->where('c_key', $user->c_key);
                $dlv->delete();
            }
            
            $fCnt = DB::connection('mysql::write')->table('mod_dlv_plan')->where('dlv_no', $dlvNo)->whereRaw("(status = 'FINISHED' or status = 'ERROR' or status = 'REJECT')")->count();
            $dlvnum = DB::connection('mysql::write')->table('mod_dlv')->where('dlv_no', $dlvNo)->value('dlv_num');
            $dlvnum = $dlvnum-count($ids);
            DB::table('mod_dlv')
            ->where('dlv_no', $dlvNo)
            ->update(['dlv_num'=>$dlvnum]);
            if($cnt == $fCnt) {
                DB::table('mod_dlv')
                    ->where('dlv_no', $dlvNo)
                    ->update(['status' => 'FINISHED']);
            }
        }

        return response()->json(['msg' => 'success']);
    }

    public function insertToDlv(Request $request) {
        $user    = Auth::user();
        $dlv_no  = $request->dlv_no;
        $ord_ids = $request->ord_ids;
        $carType = $request->car_type;
        $is_urgent = $request->is_urgent;
        $dlvData = DB::table('mod_dlv')
                        ->where('dlv_no', $dlv_no)
                        ->where('g_key', $user->g_key)
                        ->where('c_key', $user->c_key)
                        ->first();
        
        if(isset($dlvData)) {
            if($is_urgent=="Y"){
                DB::table('mod_order')
                ->whereIn('id', $ord_ids)
                ->update(["is_urgent"   => "Y"]);
            }
            $ords = DB::table('mod_order')
                    ->whereIn('id', $ord_ids)
                    ->get();

            $car = DB::table('mod_car')->where('car_no', $dlvData->car_no)->first();
            $today       = new \DateTime();
            $dlv_detail = array();
            $dlvStatus = "UNTREATED";
            if($dlvData->status == "DLV" || $dlvData->status == "SEND") {
                $dlvStatus = "DLV";
            }
            if($dlvData->status == "FINISHED") {
                return response()->json(["msg" => "ERROR"]);
            }
            if(count($ords) > 0) {
                DB::table('mod_dlv')
                ->where('dlv_no', $dlv_no)
                ->where('g_key', $user->g_key)
                ->where('c_key', $user->c_key)
                ->update([
                    "dlv_num" => $dlvData->dlv_num+count($ords)
                ]);
                foreach($ords as $key=>$ord) { 
                    $ordCnt = DB::table('mod_dlv_plan')
                            ->where('status', 'FINISHED')
                            ->where('dlv_type', 'P')
                            ->where('sys_ord_no', $ord->sys_ord_no)
                            ->count();  
                            
                    if($ord->is_urgent == 'Y') {
                        $is_urgent = 'Y';
                    }
                    else {
                        if($request->is_urgent != 'Y') {
                            $is_urgent = 'N';
                        }
                    }
                    //$ord->dlv_type != 'P' 代表已有提貨再其它配送單裡
                    $pickAddr = $ord->pick_zip.$ord->pick_addr;
                    if($ord->g_key != 'SYL')
                    {
                        $pickAddr = $ord->pick_zip.$ord->pick_city_nm.$ord->pick_area_nm.$ord->pick_addr;
                    }

                    if(!empty($ord->pick_addr) && $ordCnt == 0 && $ord->dlv_type != 'P') {
                        $detail = array(
                            "ord_id"      => $ord->id,
                            "cust_nm"     => $ord->pick_cust_nm,
                            "dlv_type"    => 'P',
                            "ord_no"      => $ord->ord_no,
                            "sys_ord_no"  => $ord->sys_ord_no,
                            "status"      => $dlvStatus,
                            "dlv_no"      => $dlv_no,
                            "addr"        => $pickAddr,
                            "etd"         => $ord->etd,
                            "sort"        => $key,
                            "remark"      => htmlspecialchars($ord->pick_remark),
                            "updated_at" => $today->format('Y-m-d H:i:s'),
                            "updated_by" => $user->email,
                            "created_at" => $today->format('Y-m-d H:i:s'),
                            "created_by" => $user->email,
                            "g_key"      => $user->g_key,
                            "c_key"      => $user->c_key,
                            "s_key"      => $user->s_key,
                            "d_key"      => $user->d_key,
                            'car_no'      => $dlvData->car_no,
                            'driver_nm'   => $dlvData->driver_nm,
                            'owner_nm'    => $ord->owner_nm,
                            'is_urgent'    => $is_urgent,
                        );
                        array_push($dlv_detail, $detail);
                    }
                    
                    $dlvAddr = $ord->dlv_zip.$ord->dlv_addr;
                    if($ord->g_key != 'SYL')
                    {
                        $dlvAddr = $ord->dlv_zip.$ord->dlv_city_nm.$ord->dlv_area_nm.$ord->dlv_addr;
                    }
                    if(!empty($ord->dlv_addr)) {
                        $detail = array(
                            "ord_id"     => $ord->id,
                            "cust_nm"    => $ord->dlv_cust_nm,
                            "dlv_type"   => 'D',
                            "ord_no"     => $ord->ord_no,
                            "sys_ord_no" => $ord->sys_ord_no,
                            "status"     => $dlvStatus,
                            "dlv_no"     => $dlv_no,
                            "addr"       => $dlvAddr,
                            "etd"        => $ord->etd,
                            "sort"       => $key,
                            "remark"     => htmlspecialchars($ord->dlv_remark),
                            "updated_at" => $today->format('Y-m-d H:i:s'),
                            "updated_by" => $user->email,
                            "created_at" => $today->format('Y-m-d H:i:s'),
                            "created_by" => $user->email,
                            "g_key"      => $user->g_key,
                            "c_key"      => $user->c_key,
                            "s_key"      => $user->s_key,
                            "d_key"      => $user->d_key,
                            'car_no'      => $dlvData->car_no,
                            'driver_nm'   => $dlvData->driver_nm,
                            'owner_nm'    => $ord->owner_nm,
                            'is_urgent'   => $is_urgent,
                        );
        
                        array_push($dlv_detail, $detail);
                    }

                    
                    
                }

                DB::table('mod_dlv_plan')->insert($dlv_detail);
                
                $ordStatus = "SEND";
                if($dlvData->status == "DLV") {
                    $ordStatus = "SETOFF";
                }

                if($dlvData->status == "SEND") {
                    $ordStatus = "LOADING";
                }
                $temperate_desc = DB::table('bscode')->where('cd_type','ORDSTATUS')->where('cd',$ordStatus)->first();
                DB::table('mod_order')
                ->whereIn('id', $ord_ids)
                ->update([
                    "status"   => $ordStatus, 
                    "status_desc"   => $temperate_desc->cd_descp, 
                    "car_no"   => $car->car_no,
                    "truck_no" => $car->car_no,
                    "dlv_no"   => $dlv_no,
                    "car_type" => $dlvData->car_type,
                    "dlv_type" => $car->dlv_type,
                    "driver"   => $dlvData->driver_nm,
                    "is_ordered" => "N",
                    "order_tally" => "N",
                    "realnum" => 0,
                    "ord_type_nm" => "插單"
                ]);
                
    
                foreach($ords as $key=>$ord) {
                    $tm = new TrackingModel();
                    $tm->insertTracking('', $ord->ord_no, '', $ord->sys_ord_no, '', 'A', 3, $user->g_key, $user->c_key, $user->s_key, $user->d_key);

                    if($dlvData->status == "DLV") {
                        $tm = new TrackingModel();
                        $tm->insertTracking('', $ord->ord_no, '', $ord->sys_ord_no, '', 'B', 4, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
                    }
                    // if($ord->owner_cd=="0099123"){
                    //     $orderMgmtModel = new OrderMgmtModel;
                    //     $orderMgmtModel->insertmiedilog($ord->sys_ord_no,$ordStatus);
                    // }
                    // $tsData = DB::table("mod_trans_status")->where("ts_type", "A")->where('order', 3)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->orderBy("order", "desc")->first();
                    // $data = [
                    //     'ts_no'      => $tsData->id,
                    //     'ts_type'    => $tsData->ts_type,
                    //     'sort'       => $tsData->order,
                    //     'ts_name'    => $tsData->ts_name,
                    //     'ts_desc'    => $tsData->ts_desc,
                    //     'ref_no1'    => $dlv_no,
                    //     'ref_no2'    => $ord->ord_no,
                    //     'ref_no3'    => $car->car_no,
                    //     'ref_no4'    => null,
                    //     'g_key'      => $ord->g_key,
                    //     'c_key'      => $ord->c_key,
                    //     's_key'      => $ord->s_key,
                    //     'd_key'      => $ord->d_key,
                    //     'created_by' => $user->name,
                    //     'updated_by' => $user->name,
                    // ];
            
                    // $TransRecordModel = new TransRecordModel();
                    // $TransRecordModel->createRecord($data);
    
                    // if(isset($ord->dlv_email)) {
                    //     $TrackingModel = new TrackingModel();
                    //     //$TrackingModel->sendTrackingMail($ord->ord_no, $ord->dlv_email, '貨況通知-訂單號：'.$ord->ord_no);
                    // }
    
                    // $c = new CalculateModel();
                    // $amt = $c->calculateAmt($ord->id);
                    // DB::table('mod_order')
                    // ->where('id', $ord->id)
                    // ->update([
                    //     'amt' => $amt
                    // ]);
                }
            }
        }

        return response()->json(["msg" => "success"]);
    }
    
    public function sendToApp() {
        \Log::info("sendToApp");
        $ids = request('ids');
        $user = Auth::user();
        try {
            if(count($ids) > 0) {
                for($i=0; $i<count($ids); $i++) {
                    //$dlvPlanData = DB::table('mod_dlv_plan')->select('ord_id');
                    $dlv = DlvModel::find($ids[$i]);
                    if($dlv->status != "UNTREATED") {
                        return response()->json(array('msg' => 'error', 'error_log' => '只有「尚未安排」的配送單，才能發送app'));
                    }
                    $dlv->status = 'SEND';
                    $dlv->save();
                    $this->sendapp($ids[$i]);
                }
            }
        }
        catch(\Exception $e) {
            return response()->json(array('msg' => 'error', 'error_log' => $e->getMessage()));
        }
        

        return response()->json(array('msg' => 'success'));
    }
    public function sendapp($ids) {
        $dlv = DlvModel::find($ids);
        $user = Auth::user();
        DB::table('mod_dlv_plan')
        ->where('dlv_no', $dlv->dlv_no)
        ->where('c_key', $user->c_key) 
        ->update(['status' => 'DLV']);
        $dlvPlanData = DB::table('mod_dlv_plan')
                        ->select('ord_id')
                        ->where('status',"!=", "ERROR")
                        ->where('status',"!=", "FINISHED")
                        ->where('status',"!=", "REJECT")
                        ->where('dlv_no', $dlv->dlv_no)
                        ->where('g_key', $dlv->g_key)
                        ->where('c_key', $dlv->c_key)
                        ->get();
        $carData = DB::table('users')->select('d_token')->where('email', $dlv->car_no)->first();
        if(isset($carData)) {
            $fcmnow = date("Y-m-d H-i");
            if(isset($carData->d_token) && $carData->d_token != '') {
                $f = new FcmModel;
                $f->sendToFcm('系統通知 '.$fcmnow, '您有新配送單：'.$dlv->dlv_no, $carData->d_token);
            }

            $s = new SysNoticeModel;
            $s->insertNotice('系統通知 '.$fcmnow, '您有新配送單：'.$dlv->dlv_no, $dlv->car_no, $user);
        } 
        if(count($dlvPlanData)) {
            foreach($dlvPlanData as $row) {
                $order = OrderMgmtModel::find($row->ord_id);
                $order->status = 'LOADING';
                $order->status_desc = '貨物裝載中';
                $order->save();
            }
        }
    }

    public function sendappmsg() {
        $car_no = request('title');
        $content = request('content');
        $user = Auth::user();
        $dlv = DB::table('users')
                   ->where('email', $car_no)
                   ->where('g_key', $user->g_key)
                   ->where('c_key', $user->c_key)
                   ->get();
        $carData = DB::table('users')->select('d_token')->where('email', $car_no)->first();
        if(isset($carData)) {
            $fcmnow = date("Y-m-d H-i");
            if(isset($carData->d_token) && $carData->d_token != '') {
                $f = new FcmModel;
                $f->sendToFcm('系統通知', '您有測試訊息：'.$content, $carData->d_token);
            }
            $s = new SysNoticeModel;
            $s->insertNotice('系統通知', '您有新訊息：'.$content, $car_no, $user);
        } 
    }

    public function exportDetail() {
        $dlvNo = request('dlvNo');
        
        $now = date('Ymd');
        Excel::create('配送單-'.$now, function($excel) use($dlvNo) {
            $dlvNoArray = explode(';', $dlvNo);
            for($i=0; $i<count($dlvNoArray); $i++) {
                $dlvNo = $dlvNoArray[$i];
                $excel->sheet($dlvNoArray[$i], function($sheet) use($dlvNo) {
                    $user = Auth::user();
                    $dlvPlanData = DB::table('mod_dlv_plan')
                                        ->select('mod_order.dlv_cust_nm', 'mod_order.pick_cust_nm', 'mod_order.pkg_num', 'mod_order.owner_nm', 'mod_dlv_plan.*')
                                        ->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no')
                                        ->where('mod_dlv_plan.dlv_no', $dlvNo)
                                        ->where('mod_order.c_key', $user->c_key)
                                        ->get();
                    $dlvData = DB::table('mod_dlv')->where('dlv_no', $dlvNo)->first();
                    $carNo  = '';
                    $driverNm = '';
                    $carTypeNm  = '';
                    $createdAt = '';
                    if(isset($dlvData)) {
                        $carNo = $dlvData->car_no;
                        $driverNm = $dlvData->driver_nm;
                        $carType = $dlvData->car_type;
                        $createdAt = DB::table('mod_dlv')->where('dlv_no', $dlvData->dlv_no)->where('c_key', $user->c_key)->value('created_at');

                        $carTypeData = DB::table('bscode')
                                    ->select('cd', 'cd_descp')
                                    ->where('cd_type', 'CARTYPE')
                                    ->where('cd', $carType)
                                    ->where('g_key', $user->g_key)
                                    ->where('c_key', $user->c_key)
                                    ->first();
                        if(isset($carTypeData))
                            $carTypeNm = $carTypeData->cd_descp;
                    }
                    $sheet->row(1, array('配送單：'.$dlvNo, '', '', '配送人：'.$driverNm, '車號：'.$carNo, '車型：'.$carTypeNm, '', '配送單日期：'.$createdAt));
                    $sheet->row(2, array(
                        '派車別', '預計送達時間','備註','系統訂單號', '訂單號', '件數', '貨主', '客戶名稱', '地址'
                    ));
    
                    foreach($dlvPlanData as $key=>$row) {
                        $dlvType = '提貨';
                        $custNm  = $row->pick_cust_nm;
                        if($row->dlv_type == 'D') {
                            $dlvType = '配送';
                            $custNm = $row->dlv_cust_nm;
                        }
    
                        $sheet->row($key+3, array(
                            $dlvType, $row->etd, $row->remark, $row->sys_ord_no, $row->ord_no, $row->pkg_num, $row->owner_nm, $custNm, $row->addr
                        ));
                    }

                    
                    
                    $sheet->setWidth(array(
                        'B' => 15,
                        'C' => 25,
                        'D' => 20,
                        'E' => 20,
                        'F' => 10,
                        'G' => 35,
                        'H' => 35,
                        'I' => 50
                    ));

                    $sheet->mergeCells('A1:C1');
                    $sheet->mergeCells('F1:G1');
            
                });
            }
            
        })->export('xls');   
        
    }

    public function DlvPlanSearch() {
        $data = array(
            'crud' => (object)array(
                'colModel' => array()
            )
        );
        try{
            if(Auth::user()->hasPermissionTo('DlvPlanSearch'))
            {
                return view('tranPlan.dlvPlan', $data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }

    public function autosend(Request $request) {
        $user     = Auth::user();
        $today    = $request->dlv_date;
        $cardata  = DB::table('mod_car')
            ->where('c_key', $user->c_key)
            ->where('car_ton', '1.75')
            ->get();

        $test =$today;
        foreach($cardata as $key=> $carrow) {
            $ids = array();
            $ids = $this->checkhasorder($carrow->cbm*$carrow->load_rate,$test);
            \Log::info($ids);
            //派單程式 開始
            $ord_ids      = $ids;
            $car_no       = $carrow->car_no;
            $driver_nm    = $carrow->driver_nm;
            $driver_phone = $carrow->phone;
            $dlv_date     = $today;
            $carType      = $carrow->car_type;
            $dlv = DB::table('mod_dlv')->orderBy("id", "desc")->first();
            $dlv_id = "";
            if(isset($dlv)) {
                $id = $dlv->id + 1;
                $dlv_id = str_pad($id,3,'0',STR_PAD_LEFT);
            }
            else {
                $dlv_id = str_pad(1,3,'0',STR_PAD_LEFT);
            }
            
    
            $ords = DB::table('mod_order')
                        ->whereIn('id', $ord_ids)
                        ->get();

            $car = DB::table('mod_car')->where('car_no', $car_no)->first();
            
            $ttl_cbm     = 0;
            $ttl_pkg_num = 0;
            $dlv_detail  = [];
            $today       = new \DateTime();
            $str_date    = $today->format('Ym');
            $params = array(
                "c_key"   => $user->c_key,
                "date_ym" => substr($str_date, 2, strlen($str_date))
            );
            $BaseModel = new BaseModel();
            $dlv_no = $BaseModel->getAutoNumber("dlv",$params);
            if(count($ords) > 0) {
                foreach($ords as $key=>$ord) {
                    $ttl_cbm     += $ord->total_cbm;
                    $ttl_pkg_num += $ord->pkg_num;
    
                    $ordCnt = DB::table('mod_dlv_plan')
                                ->where('status', 'FINISHED')
                                ->where('dlv_type', 'P')
                                ->where('sys_ord_no', $ord->sys_ord_no)
                                ->count();
                    //$ord->dlv_type != 'P' 代表有提貨在其它配送單裡
    
                    $isUrgent = 'N';
                    if($ord->is_urgent == 'Y') {
                        $isUrgent = 'Y';
                    }
    
                    if(!empty($ord->pick_addr) && $ordCnt == 0 && $ord->dlv_type != 'P') {
                        $address = $ord->pick_city_nm.$ord->pick_area_nm.$ord->pick_addr;
                        if($ord->g_key == 'SYL') {
                            $address = $ord->pick_addr;
                        }
                        $detail = array(
                            "ord_id"      => $ord->id,
                            "cust_nm"     => $ord->pick_cust_nm,
                            "dlv_type"    => 'P',
                            "ord_no"      => $ord->ord_no,
                            "sys_ord_no"  => $ord->sys_ord_no,
                            "status"      => 'SEND',
                            "dlv_no"      => $dlv_no,
                            "addr"        => $address,
                            "etd"         => $ord->etd,
                            "sort"        => $key,
                            "remark"      => htmlspecialchars($ord->pick_remark),
                            "updated_at"  => $today->format('Y-m-d H:i:s'),
                            "updated_by"  => $user->email,
                            "created_at"  => $today->format('Y-m-d H:i:s'),
                            "created_by"  => $user->email,
                            "g_key"       => $user->g_key,
                            "c_key"       => $user->c_key,
                            "s_key"       => $user->s_key,
                            "d_key"       => $user->d_key,
                            'car_no'      => $car_no,
                            'driver_nm'   => $driver_nm,
                            'owner_nm'    => $ord->owner_nm,
                            'is_urgent'   => $isUrgent
                        );
                        array_push($dlv_detail, $detail);
                    }
    
                    
                    if(!empty($ord->dlv_addr)) {
                        $address = $ord->dlv_city_nm.$ord->dlv_area_nm.$ord->dlv_addr;
                        if($ord->g_key == 'SYL') {
                            $address = $ord->dlv_addr;
                        }
    
                        $detail = array(
                            "ord_id"     => $ord->id,
                            "cust_nm"    => $ord->dlv_cust_nm,
                            "dlv_type"   => 'D',
                            "ord_no"     => $ord->ord_no,
                            "sys_ord_no" => $ord->sys_ord_no,
                            "status"     => 'SEND',
                            "dlv_no"     => $dlv_no,
                            "addr"       => $address,
                            "etd"        => $ord->etd,
                            "sort"       => $key,
                            "remark"     => htmlspecialchars($ord->dlv_remark),
                            "updated_at" => $today->format('Y-m-d H:i:s'),
                            "updated_by" => $user->email,
                            "created_at" => $today->format('Y-m-d H:i:s'),
                            "created_by" => $user->email,
                            "g_key"      => $user->g_key,
                            "c_key"      => $user->c_key,
                            "s_key"      => $user->s_key,
                            "d_key"      => $user->d_key,
                            'car_no'      => $car_no,
                            'driver_nm'   => $driver_nm,
                            'owner_nm'    => $ord->owner_nm,
                            'is_urgent'   => $isUrgent
                        );
        
                        array_push($dlv_detail, $detail);
                    }
                    
                }
    
                $modDlv               = new DlvModel;
                $modDlv->dlv_no       = $dlv_no;
                $modDlv->dlv_date     = $dlv_date;
                $modDlv->status       = 'SEND';
                $modDlv->load_rate    = $car->load_rate;
                $modDlv->load_tweight = 0;//$car->load_weight;
                $modDlv->load_tcbm    = $ttl_cbm;
                $modDlv->dlv_num      = count($ords);
                $modDlv->cust_no      = $car->cust_no;
                $modDlv->car_no       = $car->car_no;
                $modDlv->car_type     = $carType;
                $modDlv->car_type_nm  = $car->car_type_nm;
                $modDlv->driver_nm    = $driver_nm;
                $modDlv->driver_phone = $driver_phone;
                $modDlv->updated_by   = $user->email;
                $modDlv->created_by   = $user->email;
                $modDlv->g_key        = $user->g_key;
                $modDlv->c_key        = $user->c_key;
                $modDlv->s_key        = $user->s_key;
                $modDlv->d_key        = $user->d_key;
                $modDlv->save();
    
                DB::table('mod_dlv_plan')->insert($dlv_detail);
    
                DB::table('mod_order')
                ->whereIn('id', $ord_ids)
                ->update([
                    "status"   => "SEND", 
                    "car_no"   => $car->car_no,
                    "truck_no" => $car->car_no,
                    "dlv_no"   => $dlv_no,
                    "car_type" => $carType,
                    "dlv_type" => $car->dlv_type,
                    "driver"   => $driver_nm
                ]);
    
                foreach($ords as $key=>$ord) {
                    $tsData = DB::table("mod_trans_status")->where("ts_type", "A")->where('order', 3)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->orderBy("order", "desc")->first();
                    $data = [
                        'ts_no'      => $tsData->id,
                        'ts_type'    => $tsData->ts_type,
                        'sort'       => $tsData->order,
                        'ts_name'    => $tsData->ts_name,
                        'ts_desc'    => $tsData->ts_desc,
                        'ref_no1'    => $dlv_no,
                        'ref_no2'    => $ord->ord_no,
                        'ref_no3'    => $car_no,
                        'ref_no4'    => $ord->sys_ord_no,
                        'g_key'      => $ord->g_key,
                        'c_key'      => $ord->c_key,
                        's_key'      => $ord->s_key,
                        'd_key'      => $ord->d_key,
                        'created_by' => $user->name,
                        'updated_by' => $user->name,
                    ];
            
                    $TransRecordModel = new TransRecordModel();
                    $TransRecordModel->createRecord($data);
    
                    if(isset($ord->dlv_email)) {
                        $TrackingModel = new TrackingModel();
                        //$TrackingModel->sendTrackingMail($ord->ord_no, $ord->dlv_email, '貨況通知-訂單號：'.$ord->ord_no);
                    }
    
                    // $c = new CalculateModel();
                    // $amt = $c->calculateAmt($ord->id);
                    // DB::table('mod_order')
                    // ->where('id', $ord->id)
                    // ->update([
                    //     'amt' => $amt
                    // ]);
                }
            }
            //程式結束
        }        
        return response()->json(["msg" => "success"]);
    }
    public function checkhasorder($totalload,$test) {
        $user     = Auth::user();
        $loading  = 0 ;
        $keynum   = 0 ;
        $city_old = "";
        $ids = array();
        $now = $test;
        $orddata = DB::table('mod_order')
        ->where('g_key', $user->g_key)
        ->where('status','UNTREATED')
        ->where('etd', $now)
        ->orderBy('dlv_zip')
        ->get();
        foreach($orddata as $row) {
            $city = $row->dlv_zip;
            if($keynum==0){
                $city_old=$city;
            }
            if($city_old != $city) {
                $keynum = 0;
            }
            if (($loading+$row->total_cbm) < $totalload &&($city_old == $city) ) {
                $loading +=$row->total_cbm;
                array_push($ids,$row->id);
            }
            else{
                continue;
            }
            $keynum ++;
        }
        return $ids;

    }
    public function readyback() {
        $user = Auth::user();
        $ids  = request('ids');
        try {
            foreach ($ids as $key => $id) {
                $order = DB::connection('mysql::write')
                ->table('mod_order')
                ->select('ord_no', 'status', 'dlv_type', 'sys_ord_no')
                ->where('id', $id)
                ->first();

                if($order->status == 'READY') {
                    DB::table('mod_order')
                    ->where('id', $id)
                    ->update([
                        'status'      => 'UNTREATED',
                        'status_desc' => '尚未安排',
                        'updated_at'  => \Carbon::now(),
                        'updated_by'  => $user->email,
                    ]);

                } else if($order->status == 'READY_PICK') {
                    $checkPickdata = DB::connection('mysql::write')
                    ->table('mod_dlv_plan')
                    ->where('sys_ord_no', $order->sys_ord_no)
                    ->where('dlv_type', 'P')
                    ->orderBy('id', 'desc')
                    ->first();
                    if(isset($checkPickdata) && $checkPickdata->status == "FINISHED") {
                        DB::table('mod_order')
                        ->where('id', $id)
                        ->update([
                            'status'      => 'PICKED',
                            'status_desc' => '已提貨',
                            'updated_at'  => \Carbon::now(),
                            'updated_by'  => $user->email,
                        ]);
                    } else {
                        DB::table('mod_order')
                        ->where('id', $id)
                        ->update([
                            'status'      => 'UNTREATED',
                            'status_desc' => '尚未安排',
                            'updated_at'  => \Carbon::now(),
                            'updated_by'  => $user->email,
                        ]);
                    }

                }

            }

        }catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'error_log' => $e->getLine(), 'error_msg' => $e->getMessage()]);
        }
        return response()->json(['msg' => 'success']);
    }
    public function errorchangecar() {
        $user       = Auth::user();
        $ids = request('ids');
        $today = new \DateTime();
        $insertData = array();
        if(count($ids) > 0) {
            try{
                $dlvcount1 = DB::table('mod_dlv')
                ->whereIn('id', $ids)
                ->where('status',"!=", "UNTREATED")
                ->where('status',"!=", "SEND")
                ->count();
                $dlvcount2 = DB::table('mod_dlv')
                ->whereIn('id', $ids)
                ->where('status', "SEND")
                ->count();
                if($dlvcount1>0){
                    return response()->json(['msg' => 'error', 'errorMsg' => "只有尚未安排或者是發送至APP可以整批換車"]);
                }

                for($i=0; $i<count($ids); $i++) {
                    $dlvdata = DB::table('mod_dlv')
                    ->where('id', $ids[$i])
                    ->first();

                    $dlvNo =  $dlvdata->dlv_no;
                    $errorStatus = "ERROR";
                    $errordesc = "貨物--換車收/送";
                    $errorType ="A001";
    
                    $thisQuery = DB::table('bscode');
                    $thisQuery->where('cd_type', "ERRORTYPE");
                    $thisQuery->where('cd', $errorType);
                    $thisQuery->where('g_key', $user->g_key);
                    $thisQuery->where('c_key', $user->c_key);
                    $bscode = $thisQuery->first();
    
                    $cdDescp = "";;
                    if(isset($bscode)) {
                        $cdDescp = $bscode->cd_descp;
                    }
    
                    DB::table('mod_dlv')
                    ->where('dlv_no', $dlvNo)
                    ->update(['status' => 'FINISHED','finished_time' => $today->format('Y-m-d H:i:s'), 'departure_time' => $today->format('Y-m-d H:i:s')]);

                    DB::table('mod_dlv_plan')
                    ->where('dlv_no', $dlvNo)
                    ->where('dlv_type',"P")
                    ->update(['status' => "FINISHED", 'updated_at' => $today->format('Y-m-d H:i:s'), 'finish_date' => $today->format('Y-m-d H:i:s')]);
    
                    DB::table('mod_dlv_plan')
                    ->where('dlv_no', $dlvNo)
                    ->where('dlv_type',"D")
                    ->update(['status' => $errorStatus,'error_cd' => $errorType,'error_descp' => $cdDescp,'abnormal_remark' => '', 'updated_at' => $today->format('Y-m-d H:i:s'), 'finish_date' =>$today->format('Y-m-d H:i:s')]);
    
                    $orderall = DB::table('mod_order')
                    ->where('dlv_no', $dlvNo)
                    ->get();
                    foreach($orderall as $orderdata){
                        $sysOrdNo = $orderdata->sys_ord_no;
    
                        $ordModel = new OrderMgmtModel;
    
                        if($orderdata->g_key=="SYL"){
                            $ordModel->edi_export_sh($sysOrdNo, '2');
                        }
                        if($orderdata->owner_cd=="70552531"){
                            $orderMgmtModel = new OrderMgmtModel;
                            $orderMgmtModel->insertmiedilog($sysOrdNo,"SETOFF");
                        }
                        if($orderdata->owner_cd=="16606102"){
                            $orderMgmtModel = new OrderMgmtModel;
                            $orderMgmtModel->insertpchomelog($orderdata->sys_ord_no,"PICKED");
                            $orderMgmtModel->insertpchomelog($sysOrdNo,"SETOFF");
                        }
                        $tm = new TrackingModel();
                        $tm->insertTracking('', $orderdata->ord_no, '', $sysOrdNo, '', 'A', 3, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
                        $tm->insertTracking('', $orderdata->ord_no, '', $sysOrdNo, '', 'B', 4, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
                    }
                    DB::table('mod_order')
                    ->where('dlv_no', $dlvdata->dlv_no)
                    ->update([
                    'status' => 'PICKED', 'status_desc' => '已提貨', 'dlv_no' => null,
                    'error_remark' => $cdDescp, 'exp_reason' => $errorType,
                    'dlv_type' => 'P', 'abnormal_remark' => '', 'updated_at' => $today->format('Y-m-d H:i:s')
                    ]);

                    $senaoModel = new SenaoModel;
                    $senaoModel->processSenaoStatus($sysOrdNo, $errorType);
                }
            }catch(\Exception $e) {
                \Log::info($e->getMessage());
                \Log::info($e->getLine());
                return response()->json(['msg' => 'error', 'error_log' => $e->getLine(), 'error_msg' => $e->getMessage()]);
            }
        }

        return response()->json(['msg' => 'success']);
    }
}
