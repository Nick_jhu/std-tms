<?php namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BulletinCrudRequest as StoreRequest;
use App\Http\Requests\BulletinCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\DB;
use App\Models\CommonModel;
use Illuminate\Support\Facades\Auth;
class BulletinCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel("App\Models\BulletinModel");        
        $this->crud->setRoute(config('backpack.base.route_prefix').'/bulletin');
        $this->crud->setEntityNameStrings(trans('bulletin.titleName'), trans('bulletin.titleName'));
        $customers = DB::table('customers')->pluck('name', 'id')->where('id','>=',0);
        //dd($customers);
        $this->crud->setColumns([
            [
                'name' => 'title',
                'label' => trans('bulletin.title'),
                'type'  => 'text'
            ],                
            [   // date_picker
                'name' => 'public_date',
                'type' => 'datetime_picker',
                'label' => trans('bulletin.date'),
                 // optional:
                'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                //'language' => 'en'
                ]            
             ]
            ]);
        //$this->crud->addColumns(['address']);
        $this->crud->addFields([
                [
                    'name' => 'title',
                    'label' => trans('bulletin.title'),
                    'type'  => 'text'
                ],            
                [   // date_picker
                    'name' => 'public_date',
                    'type' => 'datetime_picker',
                    'label' => trans('bulletin.date'),
                     // optional:
                    'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    //'language' => 'en'
                    ]            
                ],               
                [ // Table
                    'name' => 'description',
                    'label' => trans('bulletin.description'),
                    'type' => 'summernote'
                ]
       ]);
    }
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('bulletin'))
            {
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;
        $user = Auth::user();
        try{
            if(Auth::user()->hasPermissionTo('bulletin'))
            {
                return view($this->crud->getEditView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

	public function store(StoreRequest $request)
	{
        //dd($request->all());
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

		return parent::storeCrud($request);
	}

	public function update(UpdateRequest $request)
	{
        //dd($request->all());
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
		return parent::updateCrud($request);
    }
}