<?php

namespace App\Http\Controllers;

use Storage;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;
use League\Flysystem\Filesystem;
use Illuminate\Filesystem\FilesystemAdapter;
class DashboardController extends Controller
{
    
    public function dashboard() {
        $title = trans('dashboard.titleName');
        //echo "<title>$title</title>";
        $user = Auth::user();
        $now = date('Y-m-d');

        $ordSumQuery = DB::table('mod_order')->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);

        if($user->identity == 'G') {
            $ordSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordSumQuery->where('g_key', $user->g_key);
            $ordSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordSumQuery->where('g_key', $user->g_key);
            $ordSumQuery->where('c_key', $user->c_key);
            $ordSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordSumQuery->where('g_key', $user->g_key);
            $ordSumQuery->where('c_key', $user->c_key);
            $ordSumQuery->where('s_key', $user->s_key);
            $ordSumQuery->where('d_key', $user->d_key);
        }
        $ordSum = $ordSumQuery->count();

        $ordcloseSumQuery = DB::table('mod_order_close')->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);

        if($user->identity == 'G') {
            $ordcloseSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordcloseSumQuery->where('g_key', $user->g_key);
            $ordcloseSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordcloseSumQuery->where('g_key', $user->g_key);
            $ordcloseSumQuery->where('c_key', $user->c_key);
            $ordcloseSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordcloseSumQuery->where('g_key', $user->g_key);
            $ordcloseSumQuery->where('c_key', $user->c_key);
            $ordcloseSumQuery->where('s_key', $user->s_key);
            $ordcloseSumQuery->where('d_key', $user->d_key);
        }
        $ordcloseSum = $ordcloseSumQuery->count();

        $ordSum = $ordSum+$ordcloseSum;

        $ordErrorSumQuery = DB::table('mod_order')->where('status', 'ERROR')->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);
        if($user->identity == 'G') {
            $ordErrorSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordErrorSumQuery->where('g_key', $user->g_key);
            $ordErrorSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordErrorSumQuery->where('g_key', $user->g_key);
            $ordErrorSumQuery->where('c_key', $user->c_key);
            $ordErrorSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordErrorSumQuery->where('g_key', $user->g_key);
            $ordErrorSumQuery->where('c_key', $user->c_key);
            $ordErrorSumQuery->where('s_key', $user->s_key);
            $ordErrorSumQuery->where('d_key', $user->d_key);
        }
        $ordErrorSum = $ordErrorSumQuery->count();

        $ordcloseErrorSumQuery = DB::table('mod_order_close')->where('status', 'ERROR')->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);
        if($user->identity == 'G') {
            $ordcloseErrorSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordcloseErrorSumQuery->where('g_key', $user->g_key);
            $ordcloseErrorSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordcloseErrorSumQuery->where('g_key', $user->g_key);
            $ordcloseErrorSumQuery->where('c_key', $user->c_key);
            $ordcloseErrorSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordcloseErrorSumQuery->where('g_key', $user->g_key);
            $ordcloseErrorSumQuery->where('c_key', $user->c_key);
            $ordcloseErrorSumQuery->where('s_key', $user->s_key);
            $ordcloseErrorSumQuery->where('d_key', $user->d_key);
        }
        $ordcloseErrorSum = $ordcloseErrorSumQuery->count();
        $ordErrorSum = $ordErrorSum+$ordcloseErrorSum;

        $ordFinishSumQuery = DB::table('mod_order')
                                ->where('status', 'FINISHED')
                                ->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);
        if($user->identity == 'G') {
            $ordFinishSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordFinishSumQuery->where('g_key', $user->g_key);
            $ordFinishSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordFinishSumQuery->where('g_key', $user->g_key);
            $ordFinishSumQuery->where('c_key', $user->c_key);
            $ordFinishSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordFinishSumQuery->where('g_key', $user->g_key);
            $ordFinishSumQuery->where('c_key', $user->c_key);
            $ordFinishSumQuery->where('s_key', $user->s_key);
            $ordFinishSumQuery->where('d_key', $user->d_key);
        }
        $ordFinishSum = $ordFinishSumQuery->count();

        $ordcloseFinishSumQuery = DB::table('mod_order_close')
            ->where('status', 'FINISHED')
            ->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);
        if($user->identity == 'G') {
            $ordcloseFinishSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordcloseFinishSumQuery->where('g_key', $user->g_key);
            $ordcloseFinishSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordcloseFinishSumQuery->where('g_key', $user->g_key);
            $ordcloseFinishSumQuery->where('c_key', $user->c_key);
            $ordcloseFinishSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordcloseFinishSumQuery->where('g_key', $user->g_key);
            $ordcloseFinishSumQuery->where('c_key', $user->c_key);
            $ordcloseFinishSumQuery->where('s_key', $user->s_key);
            $ordcloseFinishSumQuery->where('d_key', $user->d_key);
        }
        $ordcloseFinishSum = $ordcloseFinishSumQuery->count();
        $ordFinishSum = $ordFinishSum + $ordcloseFinishSum;
        
        $lastTimeQuery = DB::table('mod_car_real_data')
                            ->select('created_at')
                            ->where('status', 'SETOFF')
                            ->whereBetween('created_at', [$now.' 00:00:00', $now.' 23:59:59'])
                            ->orderBy('created_at', 'desc');
        if($user->identity == 'G') {
            $lastTimeQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $lastTimeQuery->where('g_key', $user->g_key);
            $lastTimeQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $lastTimeQuery->where('g_key', $user->g_key);
            $lastTimeQuery->where('c_key', $user->c_key);
            $lastTimeQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $lastTimeQuery->where('g_key', $user->g_key);
            $lastTimeQuery->where('c_key', $user->c_key);
            $lastTimeQuery->where('s_key', $user->s_key);
            $lastTimeQuery->where('d_key', $user->d_key);
        }

        $lastTime = $lastTimeQuery->first();

        $cardata = DB::table('users') 
        ->select('email', 'final_refresh',DB::raw('IFNULL(gps_time, "") as gps_time'),DB::raw('case  when  final_refresh > \''.\Carbon::now()->subMinutes(5).'\' then "Y" else "N"end as status'))
        ->whereNotNull('final_refresh')
        ->where('c_key', $user->c_key)
        ->where('g_key', $user->g_key)
        ->orderBy('id')
        ->get();
    
        $ordCompleteNum = 0;
        if($ordSum > 0) {
            $ordCompleteNum = round(($ordFinishSum/$ordSum) * 100);
        }
        $file_size = 0;
		$foldersize = DB::table('bscode')
		->where("cd_type","FORDERSIZE")
        ->where("cd","fordersize")
        ->first();
        $file_size = (int)$foldersize->value1;
        $file_size = $file_size/1048576;
        $viewData = array(
            'ordSum'         => $ordSum,
            'ordErrorSum'    => $ordErrorSum,
            'ordCompleteNum' => $ordCompleteNum,
            'ordCompleteNum' => $ordCompleteNum,
            'cardata'        => $cardata,
            'foldersize'     => number_format((int)$file_size),
            'lastTime'       => (isset($lastTime))? $lastTime->created_at: ''
        );
        try{
            if(Auth::user()->hasPermissionTo('Dashboard'))
            {
                return view('dashboard.dashboard')->with('viewData', $viewData);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    
    public function map() {
        $title = trans('dashboard.map');
        echo "<title>$title</title>";
        try{
            if(Auth::user()->hasPermissionTo('Map'))
            {
                return view('dashboard.map');
            }
        }
        catch(\Exception $e) {
            return back();
        }
    }
    public function updatefordrsize() {
        try{
            $file_size = 0;
            $loacl = Storage::disk('local')->allFiles();
            foreach( $loacl as $file)
            {
                $file_size += Storage::size($file);
            }
            DB::table('bscode')
            ->where("cd_type","FORDERSIZE")
            ->where("cd","fordersize")
            ->update([
                "value1"=>$file_size
            ]);
        }		
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return number_format($file_size/1024/1024);
        }
		return number_format($file_size/1024/1024);
	}
    public function getInfo() {
        
        
    }

    public function getAmtReportByMonth(Request $request) {
        $user = Auth::user();
        $year = $request->year;
        $label = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
        $data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        $result = DB::table('mod_order')
                    ->selectRaw('MONTH(created_at) as `month`, SUM(CASE WHEN cust_amt is null THEN amt ELSE cust_amt END) amt')
                    ->whereYear('created_at', '=', $year)
                    ->where('c_key', $user->c_key)
                    ->orderBy('created_at')
                    ->groupBy(DB::raw("MONTH(created_at)"))
                    ->get();

        $total = 0;
        foreach($result as $key=>$row) {

            $data[$row->month - 1] = $row->amt;           
            $total += $row->amt;
        }

        $returnData = array();
        $returnData['label'] = $label;
        $returnData['data'] = $data;
        $returnData['total'] = $total;

        return response()->json($returnData);
    }

    public function getDriver(Request $request) {
        $user = Auth::user();
        $newresult = DB::table('users') 
                    ->select('email', 'final_refresh',DB::raw('if(final_refresh>gps_time,final_refresh,gps_time) as gps_time'),
                    DB::raw('case  when  if(final_refresh>gps_time,final_refresh,gps_time) > \''.\Carbon::now()->subMinutes(5).'\' then "Y" else "N"end as status'))
                    ->whereNotNull('final_refresh')
                    ->where('c_key', $user->c_key)
                    ->where('g_key', $user->g_key)
                    ->orderBy('status','desc')
                    ->get();
        $result = DB::table('users')
                    ->whereBetween(DB::raw('if(final_refresh>gps_time,final_refresh,gps_time)'), array(\Carbon::now()->subMinutes(5), \Carbon::now()))
                    ->where('c_key', $user->c_key)
                    ->where('g_key', $user->g_key)
                    ->get();
        $offresult = DB::table('users')
                    ->whereNotBetween(DB::raw('if(final_refresh>gps_time,final_refresh,gps_time)'), array(\Carbon::now()->subMinutes(5), \Carbon::now()))
                    ->where('c_key', $user->c_key)
                    ->where('g_key', $user->g_key)
                    ->get();
        $returnData = array();
        $returnData['online'] = $result;
        $returnData['offline'] = $offresult;
        $returnData['test'] = $newresult;
        return response()->json($returnData);
    }

    public function getAmtReportByDay(Request $request) {
        $user = Auth::user();
        $year = $request->year;
        $month = $request->month;
        $label = array();
        $data = array();

        $result = DB::table('mod_order')
                    ->selectRaw('DAY(created_at) as `day`, SUM(CASE WHEN cust_amt is null THEN amt ELSE cust_amt END) amt')
                    ->whereRaw('YEAR(created_at) = ? and MONTH(created_at) = ?', [$year, $month])
                    ->where('c_key', $user->c_key)
                    ->orderBy('created_at')
                    ->groupBy(DB::raw("DAY(created_at)"))
                    ->get();

        $total = 0;
        foreach($result as $key=>$row) {
            array_push($label, $row->day);
            array_push($data, $row->amt);

            $total += $row->amt;
        }

        $returnData = array();
        $returnData['label'] = $label;
        $returnData['data'] = $data;
        $returnData['total'] = $total;


        return response()->json($returnData);
    }

    public function getOrderReportByMonth(Request $request) {
        $user = Auth::user();
        $year = $request->year;
        $label = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
        $data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        $closeresult = DB::table('mod_order_close')
                    ->selectRaw('MONTH(created_at) as `month`, COUNT(*) as order_number')
                    ->where('status', '!=', 'FAIL')
                    ->whereYear('created_at', '=', $year)
                    ->where('c_key', $user->c_key)
                    ->orderBy('created_at')
                    ->groupBy(DB::raw("MONTH(created_at)"))
                    ->get();

        $result = DB::table('mod_order')
                    ->selectRaw('MONTH(created_at) as `month`, COUNT(*) as order_number')
                    ->where('status', '!=', 'FAIL')
                    ->whereYear('created_at', '=', $year)
                    ->where('c_key', $user->c_key)
                    ->orderBy('created_at')
                    ->groupBy(DB::raw("MONTH(created_at)"))
                    ->get();
        $total = 0;
        foreach($result as $key=>$row) {

            $data[$row->month - 1] = $row->order_number;           
            $total += $row->order_number;
        }
        foreach($closeresult as $key=>$row) {

            $data[$row->month - 1] += $row->order_number;           
            $total += $row->order_number;
        }

        $returnData = array();
        $returnData['label'] = $label;
        $returnData['data'] = $data;
        $returnData['total'] = $total;

        return response()->json($returnData);
    }

    public function getOrderReportByDay(Request $request) {
        $user = Auth::user();
        $year = $request->year;
        $month = $request->month;
        $label = array();
        $data = array();

        // $closeresult = DB::table('mod_order_close')
        //             ->selectRaw('DAY(created_at) as `day`, COUNT(*) order_number')
        //             ->where('status', '!=', 'FAIL')
        //             ->where('c_key', $user->c_key)
        //             ->whereRaw('YEAR(created_at) = ? and MONTH(created_at) = ?', [$year, $month])
        //             ->orderBy('created_at')
        //             ->groupBy(DB::raw("DAY(created_at)"))
        //             ->get();

        // $orgresult = DB::table('mod_order')
        //             ->selectRaw('DAY(created_at) as `day`, COUNT(*) order_number')
        //             ->where('status', '!=', 'FAIL')
        //             ->where('c_key', $user->c_key)
        //             ->whereRaw('YEAR(created_at) = ? and MONTH(created_at) = ?', [$year, $month])
        //             ->orderBy('created_at')
        //             ->groupBy(DB::raw("DAY(created_at)"))
        //             ->get();
        $result = DB::select("SELECT a.day, 
                    Sum(order_number) AS order_number 
            FROM   (SELECT Day(created_at) AS `day`, 
                            Count(*)        order_number 
                    FROM   mod_order 
                    WHERE  status != 'fail' 
                            AND c_key = 'syl' 
                            AND Year(created_at) = $year 
                            AND Month(created_at) = $month 
                    GROUP  BY Day(created_at) 
                    UNION
                    
            (SELECT Day(created_at) AS `day`, 
                    Count(*)        order_number 
            FROM   mod_order_close 
            WHERE  status != 'fail' 
                    AND c_key = 'syl' 
                    AND Year(created_at) = $year 
                    AND Month(created_at) = $month 
            GROUP  BY Day(created_at) 
            ORDER  BY created_at ASC)) a 
            
            GROUP  BY a.day ");


        $total = 0;
        foreach($result as $key=>$row) {
            array_push($label, $row->day);
            array_push($data, $row->order_number);

            $total += $row->order_number;
        }

        $returnData = array();
        $returnData['label'] = $label;
        $returnData['data'] = $data;
        $returnData['total'] = $total;


        return response()->json($returnData);
    }
}
