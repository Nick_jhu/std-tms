<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use App\Models\BscodeKindModel;
use App\Models\BscodeModel;
use App\Models\BaseModel;
use App\Models\OrderMgmtModel;
use App\Models\CommonModel;
use App\Models\SampleDetailModel;
use Illuminate\Session\Store as Session;
use Illuminate\Validation\Rule;
use App\Http\Requests\BscodeKindCrudRequest as StoreRequest;
use App\Http\Requests\BscodeKindCrudRequest as UpdateRequest;

class BscodeKindCrudController extends CrudController
{
    
    public function setup() {
        $this->crud->setModel("App\Models\BscodeKindModel");
        $this->crud->setEntityNameStrings('基本健檔', '基本健檔');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/bscodeKind');
    
        $this->crud->setColumns(['name']);

        
        $this->crud->setCreateView('bscodeKind.edit');
        $this->crud->setEditView('bscodeKind.edit');
        $this->crud->setListView('bscodeKind.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'cd_type',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'cd_descp',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'updated_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);               
        $this->crud->addField([
            'name' => 'adminonly',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);
        
        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 's_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'text'
        ]);
    }

    public function get($cd_type=null) 
    {
        $bscode = [];
        $user = Auth::user();
        //dd($cust_no);
        if($cd_type != null) {            
            $this_query = DB::table('bscode');
            $this_query->where('cd_type', $cd_type);
            $this_query->where('g_key', $user->g_key);
            $this_query->where('c_key', $user->c_key);
            $bscode = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $bscode,
        );

        return response()->json($data);
    }
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('bscodeKind'))
            {
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;
        $user = Auth::user();
        try{
            if(Auth::user()->hasPermissionTo('bscodeKind'))
            {
                return view($this->crud->getEditView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }
    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        // $request->g_key = $user->g_key;
        // $request->c_key = $user->c_key;
        // $request->s_key = $user->s_key;
        // $request->d_key = $user->d_key;       
        //$request['created_by'] = $user->name;
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        
        //dd($request->all());
        try {
            $response = parent::storeCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::updateCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response];
    }

    public function detailStore(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);        

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $bscode = new BscodeModel;
            $bscode->cd_type    = $request->cd_type;
            $bscode->cd       = $request->cd;
            $bscode->cd_descp      = $request->cd_descp;
            $bscode->value1    = $request->value1;
            $bscode->value2    = $request->value2;
            $bscode->value3    = $request->value3;
            $bscode->g_key    = $user->g_key;
            $bscode->c_key    = $user->c_key;
            $bscode->s_key    = $user->s_key;
            $bscode->d_key    = $user->d_key;
            $bscode->created_by    = $user->email;
            $bscode->updated_by    = $user->email;
            $bscode->save();
        }

        return ["msg"=>"success", "data"=>DB::connection('mysql::write')->table('bscode')->where('id', $bscode->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $bscode = BscodeModel::find($request->id);
            $bscode->cd       = $request->cd;
            $bscode->cd_descp      = $request->cd_descp;
            $bscode->value1    = $request->value1;
            $bscode->value2    = $request->value2;
            $bscode->value3    = $request->value3;
            $bscode->g_key    = $user->g_key;
            $bscode->c_key    = $user->c_key;
            $bscode->s_key    = $user->s_key;
            $bscode->d_key    = $user->d_key;            
            $bscode->updated_by    = $user->email;
            $bscode->save();
        }


        return ["msg"=>"success", "data"=>DB::connection('mysql::write')->table('bscode')->where('id', $request->id)->get()];
    }

    public function detailDel($id)
    {
        $sampleDetail = BscodeModel::find($id);
        $sampleDetail->delete();

        return ["msg"=>"success"];
    }

    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), [
            // 'cd' => [
            //     'required',
            //     'min:1',
            //     'max:20',
            //     Rule::unique('bscode')->where('cd_type', $request->cd_type) 
            // ],
            'cd' => 'required|min:1|max:20',
            'cd_descp' => 'nullable|min:1|max:300',
            'value1' => 'nullable|min:1|max:70',
            'value2' => 'nullable|min:1|max:50',
            'value3' => 'nullable|min:1|max:50',
            'remark' => 'nullable|min:1|max:200',
        ]);

        return $validator;
    }

}
