<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CompanyProfileCrudRequest as StoreRequest;
use App\Http\Requests\CompanyProfileCrudRequest as UpdateRequest;
use App\Models\CommonModel;
use App\Models\CompanyProfileModel;
use App\Models\SysSiteModel;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use HTML;

class CustuserController extends CrudController
{
    
    public function setup() {
        $this->crud->setModel(config('backpack.permissionmanager.user_model'));
        $this->crud->setEntityNameStrings('使用者管理', '使用者管理');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/custuser');
    
        $this->crud->setColumns(['name']);
        $this->crud->setCreateView('user.edit');
        $this->crud->setEditView('user.edit');
        $this->crud->setListView('user.index');
        $this->crud->enableAjaxTable();

        $this->crud->addFields([
            [
                'name'  => 'name',
                'label' => trans('backpack::permissionmanager.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'sendmail',
                'label' => trans('backpack::permissionmanager.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => trans('backpack::permissionmanager.email'),
                'type'  => 'text',
            ],
            [
                'name'  => 'password',
                'label' => trans('backpack::permissionmanager.password'),
                'type'  => 'password',
            ],
            [
                'name'  => 'password_confirmation',
                'label' => trans('backpack::permissionmanager.password_confirmation'),
                'type'  => 'password',
            ],
            [ // select_from_array
                'name'  => 'identity',
                'label' => trans('backpack::permissionmanager.gkey'),
                'type'  => 'text',
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [
                'name'  => 'g_key',
                'label' => trans('backpack::permissionmanager.gkey'),
                'type'  => 'text',
            ],
            [
                'name'  => 'c_key',
                'label' => trans('backpack::permissionmanager.ckey'),
                'type'  => 'text',
            ],
            [
                'name'  => 's_key',
                'label' => trans('backpack::permissionmanager.skey'),
                'type'  => 'text',
            ],
            [
                'name'  => 'd_key',
                'label' => trans('backpack::permissionmanager.dkey'),
                'type'  => 'text',
            ],
            [
                'name' => 'session_id',
                'type' => 'hidden',
                'value' => 'session'
            ],
            [
                'name' => 'account_enable',
                'type' => 'select'
            ],
            [
                'name' => 'owner_cd',
                'type' => 'select2_multiple',
                'options' => DB::table('sys_customers')->select('cust_no as code', 'cmp_abbr as descp')
                                ->where('g_key', Auth::user()->g_key)
                                ->where('type', "other")
                                ->get()
            ],
            [
                'name' => 'check_station',
                'type' => 'select2_multiple',
                'options' => DB::table('sys_customers')->select('cust_no as code', 'cmp_abbr as descp')
                                ->where('g_key', Auth::user()->g_key)
                                ->where('type', "self")
                                ->get()
            ],
        ]);

    }
    public function index()
    {
        $this->crud->hasAccessOrFail('list');
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('NormalPermissions'))
            {
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function getData($table=null,$id) {
        $data = DB::table('users')->where('id', $id)->first();
        return response()->json($data);
    }


    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;   
        $response ="";     
        // $ownercd = $request->owner_cd;
        // $ownercd =  str_replace('*', '\'', $ownercd);
        // $request->merge(['owner_cd' => $ownercd]);

        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();

        $g_key = $request->g_key;
        $c_key = $request->c_key;
        $s_key = $request->s_key;
        $d_key = $request->d_key;
        $password = $request->password;
        $password_confirmation = $request->password_confirmation;
        $password_Hash = bcrypt($password);
        if($password!=null){
            if($password==$password_confirmation){
                $request->merge(['password' => $password_Hash]);
            }else{
                return ["msg"=>"error", "errorLog"=>"密碼不一致"];
            }
        }else{
            unset($request['password']);
        }
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        unset($request['password_confirmation']);
        $request->merge(['updated_at' => \Carbon::now()]);
        $request->merge(['g_key' => $g_key]);
        $request->merge(['c_key' => $c_key]);
        $request->merge(['s_key' => $s_key]);
        $request->merge(['d_key' => $d_key]);
        if($g_key==null){
            unset($request['g_key']);
        }
        if($c_key==null){
            unset($request['c_key']);
        }
        if($s_key==null){
            unset($request['s_key']);
        }
        if($d_key==null){
            unset($request['d_key']);
        }
        // dd($request->all());
        try {
            $response = parent::updateCrud($request);            
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        return ["msg"=>"success", "response"=>$response];
    }
    
    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        

        return $this->crud->delete($id);
    }


    public function multiDel() {
        $ids = request('ids');
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                DB::table('users')->where("id", $ids[$i])->delete();
            }
        }

        return response()->json(array('msg' => 'success'));
    }
}
