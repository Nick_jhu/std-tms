<?php namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MailFormatCrudRequest as StoreRequest;
use App\Http\Requests\MailFormatCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\CommonModel;
class MailFormatCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel("App\Models\MailFormatModel");        
        $this->crud->setRoute(config('backpack.base.route_prefix').'/mailFormat');
        $this->crud->setEntityNameStrings(trans('mailFormat.titleName'), trans('mailFormat.titleName'));
        $bscode = DB::table('bscode')->where('cd_type','=','MF')->pluck('cd_descp', 'cd');
        //dd($bscode);
        $this->crud->setColumns([
            [
                'name' => 'title',
                'label' => trans('mailFormat.title'),
                'type'  => 'text'
            ],                
            [  // Select
                'label' => trans('mailFormat.type'),
                'type' => 'select_from_array',
                'name' => 'type', 
                'options' => $bscode,
                'allows_null' => false                
             ]           
            ]);
        //$this->crud->addColumns(['address']);
        $this->crud->addFields([
            [
                'name' => 'title',
                'label' => trans('mailFormat.title'),
                'type'  => 'text'
            ],                
            [  // Select
                'label' => trans('mailFormat.type'),
                'type' => 'select_from_array',
                'name' => 'type', 
                'options' => $bscode,
                'allows_null' => false                
             ],           
            [ // Table
                'name' => 'content',
                'label' => trans('mailFormat.content'),
                'type' => 'summernote'
            ],
            [ // Table
                'name' => 'g_key',
                'label' => 'g_key',
                'type' => 'hidden'
            ],
            [ // Table
                'name' => 'c_key',
                'label' => 'c_key',
                'type' => 'hidden'
            ],
            [ // Table
                'name' => 's_key',
                'label' => 's_key',
                'type' => 'hidden'
            ],
            [ // Table
                'name' => 'd_key',
                'label' => 'd_key',
                'type' => 'hidden'
            ],
            [ // Table`
                'name' => 'created_by',
                'label' => 'created_by',
                'type' => 'hidden'
            ],
            [ // Table
                'name' => 'updated_by',
                'label' => 'updated_by',
                'type' => 'hidden'
            ]
       ]);
    }
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('mailFormat'))
            {
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;
        $user = Auth::user();
        try{
            if(Auth::user()->hasPermissionTo('mailFormat'))
            {
                return view($this->crud->getEditView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }
	public function store(StoreRequest $request)
	{
        //dd($request->all());
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        file_put_contents(resource_path('views/emails/'.$user->c_key.'_'.$request->type.'.blade.php'), $request->content);

		return parent::storeCrud($request);
	}

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        //dd($request->all());
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        file_put_contents(resource_path('views/emails/'.$user->c_key.'_'.$request->type.'.blade.php'), $request->content);
        
		return parent::updateCrud($request);
    }
}