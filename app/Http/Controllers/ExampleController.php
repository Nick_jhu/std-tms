<?php

namespace App\Http\Controllers;

//use Illuminate\Routing\Controller;
use Storage;
use App\Example;
use Auth;
use Illuminate\Http\Request;
//for packer
use DVDoug\BoxPacker\Packer;
use DVDoug\BoxPacker\Test\TestBox;  // use your own object
use DVDoug\BoxPacker\Test\TestItem; // use your own object

class ExampleController extends Controller
{

    /**
     * Lists all log files.
     */
    public function index()
    {
        //print_r(Auth::user()->g_key);
        // reverse the logs, so the newest one would be on top
    
        $this->data['title'] = trans('backpack::logmanager.log_manager');

        return view('example.index', $this->data);
    }

    /**
     * Google Directions
     */
     public function googleMapExample()
     {
        $response = \GoogleMaps::load('directions')->setParam([
            'origin'          => '新北市永和區中正路286巷10號', 
            'destination'     => '台北市大安區新生南路1段163號', 
            //'waypoints'     => "optimize:true|九龍旺角亞皆老街79號|新界青衣城一樓121號|九龍旺角西洋菜南街78號", 
            'language'     => 'zh-TW'
        ])->get();


        return $response;
     }


    /**
    * Google Directions
    */
    public function packerExample()
    {
        $packer = new Packer();
        
        /*
            * Add choices of box type - in this example the dimensions are passed in directly via constructor,
            * but for real code you would probably pass in objects retrieved from a database instead
        $reference,
        $outerWidth,
        $outerLength,
        $outerDepth,
        $emptyWeight,
        $innerWidth,
        $innerLength,
        $innerDepth,
        $maxWeight
            */
        $packer->addBox(new TestBox('Le petite box', 3000, 3000, 100, 100, 2960, 2960, 120, 10000));
        $packer->addBox(new TestBox('Le grande box', 3000, 3000, 100, 100, 2960, 2960, 120, 10000));
    
        /*
            * Add items to be packed - e.g. from shopping cart stored in user session. Again, the dimensional information
            * (and keep-flat requirement) would normally come from a DB
            $description, $width, $length, $depth, $weight, $keepFlat
            */
        $packer->addItem(new TestItem('Item 1', 250, 250, 12, 200, true));
        $packer->addItem(new TestItem('Item 2', 250, 250, 12, 200, true));
        
        $packer->addItem(new TestItem('Item 3', 250, 250, 24, 200, false));
        $packer->addItem(new TestItem('Item 4', 250, 250, 12, 200, true));
        $packer->addItem(new TestItem('Item 5', 250, 250, 12, 200, true));
        $packer->addItem(new TestItem('Item 6', 250, 250, 24, 200, false));
        $packer->addItem(new TestItem('Item 7', 250, 250, 12, 200, true));
        $packer->addItem(new TestItem('Item 8', 250, 250, 12, 200, true));
        $packer->addItem(new TestItem('Item 9', 250, 250, 24, 200, false));
        $packer->addItem(new TestItem('Item 10', 250, 250, 12, 200, true));
        $packer->addItem(new TestItem('Item 11', 250, 250, 12, 200, true));
        $packer->addItem(new TestItem('Item 12', 250, 250, 24, 200, false));
        $packer->addItem(new TestItem('Item 13', 250, 250, 12, 200, true));
        $packer->addItem(new TestItem('Item 14', 250, 250, 12, 200, true));
        $packer->addItem(new TestItem('Item 15', 250, 250, 24, 200, false));
        $packer->addItem(new TestItem('Item 16', 250, 250, 12, 200, true));
        $packer->addItem(new TestItem('Item 17', 250, 250, 12, 200, true));
        $packer->addItem(new TestItem('Item 18', 250, 250, 24, 200, false));
        
        $packedBoxes = $packer->pack();
    
        echo "These items fitted into " . count($packedBoxes) . " box(es) <br />" . PHP_EOL;
        foreach ($packedBoxes as $packedBox) {
            $boxType = $packedBox->getBox(); // your own box object, in this case TestBox
            echo "This box is a {$boxType->getReference()}, it is {$boxType->getOuterWidth()}mm wide, {$boxType->getOuterLength()}mm long and {$boxType->getOuterDepth()}mm high <br />" . PHP_EOL;
            echo "The combined weight of this box and the items inside it is {$packedBox->getWeight()}g <br />" . PHP_EOL;
    
            echo "The items in this box are: <br />" . PHP_EOL;
            $itemsInTheBox = $packedBox->getItems();
            foreach ($itemsInTheBox as $item) { // your own item object, in this case TestItem
                echo $item->getDescription() . "<br />".PHP_EOL;
            }
        }

        $this->data['title'] = trans('backpack::logmanager.log_manager');
        return view('example.packer', $this->data);
    }
  


    public function getData(Request $request){
        try{
            $example = new Example();
            return $this->data['users'] =  $example->get_user($request);
        }catch(\Exception $ex){
            dd($ex);
        }
        
    }

    public function getFieldsJson(Request $request){
        try{
            $example = new Example();
            return $example->get_user_column();
        }catch(\Exception $ex){
            dd($ex);
        }
        
    }
    /**
     * Previews a log file.
     *
     * TODO: make it work no matter the flysystem driver (S3 Bucket, etc).
     */
    public function preview($file_name)
    {
        $disk = Storage::disk('storage');

        if ($disk->exists('logs/'.$file_name)) {
            $this->data['log'] = [
                                    'file_path'     => 'logs/'.$file_name,
                                    'file_name'     => $file_name,
                                    'file_size'     => $disk->size('logs/'.$file_name),
                                    'last_modified' => $disk->lastModified('logs/'.$file_name),
                                    'content'       => $disk->get('logs/'.$file_name),
                                    ];
            $this->data['title'] = trans('backpack::logmanager.preview').' '.trans('backpack::logmanager.logs');

            return view('logmanager::log_item', $this->data);
        } else {
            abort(404, trans('backpack::logmanager.log_file_doesnt_exist'));
        }
    }

    /**
     * Downloads a log file.
     *
     * TODO: make it work no matter the flysystem driver (S3 Bucket, etc).
     */
    public function download($file_name)
    {
        $disk = Storage::disk('storage');

        if ($disk->exists('logs/'.$file_name)) {
            return response()->download(storage_path('logs/'.$file_name));
        } else {
            abort(404, trans('backpack::logmanager.log_file_doesnt_exist'));
        }
    }

    /**
     * Deletes a log file.
     *
     * TODO: make it work no matter the flysystem driver (S3 Bucket, etc).
     */
    public function delete($file_name)
    {
        $disk = Storage::disk('storage');

        if ($disk->exists('logs/'.$file_name)) {
            $disk->delete('logs/'.$file_name);

            return 'success';
        } else {
            abort(404, trans('backpack::logmanager.log_file_doesnt_exist'));
        }
    }

    public function testApi()
	{ 
		return view('example.testApi');
	}
}
