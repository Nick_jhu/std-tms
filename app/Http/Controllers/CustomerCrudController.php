<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Models\CustomerModel;
use App\Models\SampleDetailModel;
use App\Models\BaseModel;
use App\Models\CommonModel;

use App\Http\Requests\CustomerCrudRequest as StoreRequest;
use App\Http\Requests\CustomerCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store as Session;

class CustomerCrudController extends CrudController
{
    
    public function setup() {
        $this->crud->setModel("App\Models\CustomerModel");
        $this->crud->setEntityNameStrings("customer", "customers");
        $this->crud->setRoute(config('backpack.base.route_prefix').'/customer');
    
        $this->crud->setColumns(['name']);

        
        $this->crud->setCreateView('customer.edit');
        $this->crud->setEditView('customer.edit');
        $this->crud->setListView('customer.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'cust_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'address',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'cust_contact',
            'type' => 'lookup',
            'title' => 'My Lookup',
            'info1' => Crypt::encrypt('customers'), //table
            'info2' => Crypt::encrypt("id+name,id,name"), //column
            'info3' => Crypt::encrypt("status='A'"), //condition
            'info4' => "id=cust_contact;name=address", //field mapping
            'selectionmode' => "checkbox", //lookup mode
            'triggerfunc' => "custContactFunc" //after selection trigger js function
        ]);

        $this->crud->addField([
            'name' => 'gender',
            'type' => 'radio'
        ]);

        $this->crud->addField([
            'name' => 'email',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'cust_date',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'state',
            'type' => 'select2_multiple',
            'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'TEST')->get()
        ]);


        $this->crud->addField([
            'name' => 'spec_service',
            'type' => 'checkbox'
        ]);


        $this->crud->addField([
            'name' => 'city',
            'type' => 'select',
            'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'TEST')->get()
        ]);

    }

    public function get($cust_no=null) 
    {
        $sample_detail = [];
        //dd($cust_no);
        if($cust_no != null) {
            //dd($this->crud);
            //$sample_detail = CustomerModel::find($cust_no)->sampleDetail;//DB::table('sample_detail')->get()->where('cust_no', $cust_no);
            $this_query = DB::table('sample_detail');
            $this_query->where('cust_no', $cust_no);
            $sample_detail = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $sample_detail,
        );

        return response()->json($data);
    }

    public function store(StoreRequest $request)
	{
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        
        //dd($request->all());
        try {
            $response = parent::storeCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::updateCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response];
    }

    public function detailStore(Request $request)
    {
        $validator = $this->detailValidator($request);        

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sampleDetail = new SampleDetailModel;
            $sampleDetail->cust_no    = $request->cust_no;
            $sampleDetail->name       = $request->name;
            $sampleDetail->phone      = $request->phone;
            $sampleDetail->address    = $request->address;
            $sampleDetail->save();
        }

        return ["msg"=>"success", "data"=>$sampleDetail->where('type', $request->type)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sampleDetail = SampleDetailModel::find($request->id);
            $sampleDetail->name       = $request->name;
            $sampleDetail->phone      = $request->phone;
            $sampleDetail->address    = $request->address;
            $sampleDetail->save();
        }


        return ["msg"=>"success", "data"=>$sampleDetail->where('id', $request->id)->get()];
    }

    public function detailDel($id)
    {
        $sampleDetail = SampleDetailModel::find($id);
        $sampleDetail->delete();

        return ["msg"=>"success"];
    }

    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50'
        ]);

        return $validator;
    }
    
    
    /*
    public function index()
    {
        $this->data['title'] = trans('backpack::logmanager.log_manager');

        return view('customer.index', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = trans('backpack::logmanager.log_manager');

        return view('customer.edit', $this->data);
    }
    */
}
