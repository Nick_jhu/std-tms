<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\CommonModel;
use App\Models\CustomerItemModel;
use App\Models\CustomerProfileModel;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Hash;
use HTML;
use Excel;

class CommonController extends Controller
{
    
    public function getData($table=null,$id=null) {
        $data = DB::table($table)->where('id', $id)->first();

        return response()->json($data);
    }

    public function getCompnayList() {
        $gData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'G')->get();
        $cData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'C')->get();
        $sData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'S')->get();
        $dData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'D')->get();

        $data = array(
            'gData' => $gData,
            'cData' => $cData,
            'sData' => $sData,
            'dData' => $dData,
        );
        
        return response()->json($data);
    }

    public function getSearchLayoutList($key) {
        $user = Auth::user();

        $data = DB::table('sys_search_layout')
                    ->select('id', 'title', 'layout_default')
                    ->where('key', $key)
                    ->where('created_by', $user->email)
                    ->get();
        
        return response()->json(['msg' => 'success', 'data' => $data]);
    }

    public function getSearchHtml($id=null) {
        return response()->json(['msg' => 'success', 'data' => DB::table('sys_search_layout')->where('id', $id)->first()]);
    }
    public function Layout() {
        $title = "layout";
        echo "<title>$title</title>";
        $user = Auth::user();
        //$carData = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->c_key)->where('identity', 'c')->first();
        try{
            if(Auth::user()->hasPermissionTo('LayoutSetting'))
            {
                return view('layout.index');
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function changerror($msg)
    {
        $newmsg = "";
        if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_customers?basecon=type;EQUAL;SELF&"){
            $newmsg="集團建檔";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_dlv_plan_view"){
            $newmsg="派車作業總覽";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_close"){
            $newmsg="結案管理";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_confirm"){
            $newmsg="回單作業";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order"){
            $newmsg="訂單作業";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_car"){
            $newmsg="車輛建檔";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_detail_view"){
            $newmsg="訂單明細總覽";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_total_view"){
            $newmsg="訂單總覽";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_total_view as mod_order_total_view"){
            $newmsg="訂單總覽";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_users_view"){
            $newmsg="客戶使用者權限";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_dlv"){
            $newmsg="派車作業";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJsonForMongo/mod_order"){
            $newmsg="訂單作業";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_area"){
            $newmsg="區域建檔";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_country"){
            $newmsg="國家建檔";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/bscode_kind"){
            $newmsg="基本建檔";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_goods"){
            $newmsg="料號建檔";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_customers?basecon=type;EQUAL;OTHER&"){
            $newmsg="客戶建檔";
        }else if($msg=="layoutGrid"){
            $newmsg="Layout管理";
        }else if($msg=="dlvCarGrid"){
            $newmsg="訂單匯入";
        }else if($msg=="excelOrderGrid"){
            $newmsg="派車作業";
        }else if($msg=="https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_customer_view?basecon=type;EQUAL;OTHER&"){
            $newmsg="客戶建檔";
        }

        return $newmsg;
    }
    public function copylayout(Request $request) {
        $user         = Auth::user();
        $copytouser   = $request->copytouser;
        $layoutids    = $request->ids;
        $insertData   = array();
        $msg          = "";
        $keydata = DB::table('sys_layout')->whereIn('id', $layoutids)->pluck("key");
        $copytouser_layout = DB::table('sys_layout')->where('created_by', $copytouser)->whereIn('key',$keydata)->count();
        if($copytouser_layout>0){
            $errormsg = DB::table('sys_layout')->where('created_by', $copytouser)->whereIn('key',$keydata)->get();
            foreach($errormsg as $row2) {
                $generror = $this->changerror($row2->key);        
                $msg = $msg.$generror.",";
            }
            $msg = rtrim($msg, ", ");
            return response()->json(["msg" => "error","errormsg" => $msg]);
        }else{
            $newkey = DB::table('sys_layout')->whereIn('id', $layoutids)->get();
            $cnt =count($newkey);
            for($i=0; $i<$cnt; $i++) {
                $data = [
                    'key'           => $newkey[$i]->key,
                    'data'          => $newkey[$i]->data,
                    'g_key'         => $user->g_key,
                    'c_key'         => $user->c_key,
                    's_key'         => $user->s_key,
                    'd_key'         => $user->d_key,
                    'lang'          => $newkey[$i]->lang,
                    'created_by'    => $copytouser,
                    'updated_by'    => $copytouser,
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ];
                array_push($insertData, $data);
            }
            DB::table('sys_layout')->insert($insertData);
            return response()->json(["msg" => "success"]);
        }
    }
    
    public function copylayoutconfirm(Request $request) {
        $user         = Auth::user();
        $insertData = array();
        $copytouser   = $request->copytouser;
        $layoutids    = $request->ids;
        $keydata = DB::table('sys_layout')->whereIn('id', $layoutids)->pluck("key");
        $newkey = DB::table('sys_layout')->whereIn('id', $layoutids)->get();
        $copytouser_layout = DB::table('sys_layout')->where('created_by', $copytouser)->whereIn('key',$keydata)->delete();
        $cnt =count($newkey);

        for($i=0; $i<$cnt; $i++) {
            $data = [
                'key'           => $newkey[$i]->key,
                'data'          => $newkey[$i]->data,
                'g_key'         => $user->g_key,
                'c_key'         => $user->c_key,
                's_key'         => $user->s_key,
                'd_key'         => $user->d_key,
                'lang'         => $newkey[$i]->lang,
                'created_by'    => $copytouser,
                'updated_by'    => $copytouser,
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(),
            ];
            array_push($insertData, $data);
        }
        DB::table('sys_layout')->insert($insertData);
        return response()->json(["msg" => "success"]);
    }

    public function saveSearchLayout(Request $request) {
        $user = Auth::user();

        $data  = $request->data;
        $title = $request->title;
        $id    = $request->id;
        $key   = $request->key;

        if($id == null) {
            $id = DB::table('sys_search_layout')->insertGetId([
                'key'        => $key,
                'data'       => $data,
                'title'      => $title,
                'g_key'      => $user->g_key,
                'c_key'      => $user->c_key,
                's_key'      => $user->s_key,
                'd_key'      => $user->d_key,
                'created_by' => $user->email,
                'updated_by' => $user->email,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ]);
        }
        else {
            DB::table('sys_search_layout')
                ->where('id', $id)
                ->update([
                    'data'       => $data,
                    'updated_by' => $user->email,
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]);
        }

        return response()->json(['msg' => 'success', 'id' => $id]);
    }

    public function delSearchLayout($id=null) {
        DB::table('sys_search_layout')
            ->where('id', $id)
            ->delete();
            
        return response()->json(['msg' => 'success']);    
    }

    public function setSearchDefault($key, $id) {
        $user = Auth::user();

        DB::table('sys_search_layout')
                ->where('key', $key)
                ->where('created_by', $user->email)
                ->update([
                    'layout_default' => 'N'
                ]);
        
        DB::table('sys_search_layout')
                ->where('id', $id)
                ->update([
                    'layout_default' => 'Y'
                ]);

        return response()->json(['msg' => 'success']);
    }

    public function getCarType() {
        $user = Auth::user();
        $carType = request('carType');
        $result = array();
        if(isset($carType)) {
            $a = explode(',', $carType);

            $result = DB::table('bscode')
                ->select('cd', 'cd_descp')
                ->where('cd_type', 'CARTYPE')
                ->whereIn('cd', $a)
                ->where('g_key', $user->g_key)
                ->where('c_key', $user->c_key)
                ->get();
                
            if(is_array($result) && count($result) == 0) {
                $result = DB::table('bscode')
                ->select('cd', 'cd_descp')
                ->where('cd_type', 'CARTYPE')
                ->whereIn('cd_descp', $a)
                ->where('g_key', $user->g_key)
                ->where('c_key', $user->c_key)
                ->get();
            }
        }

        return response()->json($result);
    }

    public function updatePwd() {
        $password = request('password');
        $confirmPassword = request('confirmPassword');
        $originalPassword = request('originalPassword');

        if($password != $confirmPassword) {
            return response()->json(['msg' => 'error', 'msgLog' => '密碼不一致，請再檢查']);
        }

        $user = Auth::user();

        if (!(Hash::check($originalPassword, $user->password))) {
            return response()->json(['msg' => 'error', 'msgLog' => '原始密碼錯誤']);
        }
        $user->password = bcrypt($password);
        $user->save();

        return response()->json(['msg' => 'success']);
    }
    public function exportData(Request $request) {
        ini_set('memory_limit', '1024M');
        $user = Auth::user();
        // $d = json_decode($request->content);
        // dd($request->content);
        // $d = $request->content;

        $today     = date('Y-m-d');
        $fileName  = $request->filename;
        $headers   = array();
        $headerdb  = array();
        $headorder = "";
        $dlvphone  = "";
        $pickphone = "";
        $snno      = "";
        foreach($request->header as $key=> $header){
            array_push($headers, $header['filed_text']);
            array_push($headerdb, $header['filed_name']);
            if($header['filed_text']=="訂單號") {
                $headorder = $key;
            }
            if($header['filed_text']=="配送電話"){
                $dlvphone = $key;
            }
            if($header['filed_text']=="提貨電話"){
                $pickphone = $key;
            }
            if($header['filed_text']=="產品序號"){
                $snno = $key;
            }
        }
        $jsoncolumn = json_encode($request->columndata);
        $logid =  DB::table('export_grid_excel')
        ->insertGetId([
            'gridtable'  => $request->table,
            'columndata' => $jsoncolumn,
            'g_key'      => $user->g_key,
            'c_key'      => $user->c_key,
            's_key'      => $user->s_key,
            'd_key'      => $user->d_key,
            'created_by' => $user->email,
            'updated_by' => $user->email,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);

        Excel::create($fileName.'-'.$today, function($excel) use($request,$fileName,$user,$headers,$headerdb,$headorder,$pickphone,$dlvphone,$snno) {
            $excel->sheet($fileName, function($sheet) use($request,$user,$headers,$headerdb,$headorder,$pickphone,$dlvphone,$snno) {
                $this_query = DB::table($request->table);
                $this_query->select($headerdb);

                if(is_array($request->columndata) && count($request->columndata)>0) {
                    foreach($request->columndata as $row) {
                        if($row['condition'] == "GREATER_THAN_OR_EQUAL") {
                            $this_query->where($row['column'], ">=", date('Y-m-d H:i:s',($row['value']/1000) ));
                        }else if($row['condition'] == "LESS_THAN_OR_EQUAL"){
                            $this_query->where($row['column'], "<=", date('Y-m-d H:i:s',($row['value']/1000) ));
                        }else if($row['condition'] == "CONTAINS") {
                            $this_query->where($row['column'], "like", "%".$row['value']."%");
                        }else if ( $row['condition']== "NOT_EQUAL" ) {
                            $this_query->whereNotIn($row['column'], explode(',', $row['value']));
                        }else if ( $row['condition']== "IN" ) {
                            $this_query->whertIn($row['column'], explode(',', $row['value']));
                        }
                    }
                }
                if($request->basecon!=""){
                    $basecon= explode(';', $request->basecon);
                    $this_query->where($basecon[0],$basecon[2]);
                }
                $table_name = $request->table;
                if($table_name == 'mod_order_total_view' || $table_name == 'mod_order'|| $table_name=="mod_order_close" || $table_name == 'mod_order_view'|| $table_name == 'mod_order as mod_order_total_view'|| $table_name == 'mod_order_detail_view'){
                    $ownerdata = str_replace(",","','",$user->owner_cd);
                    if($ownerdata !=null && $ownerdata !=""){
                        $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and owner_cd in('".$ownerdata."')) or g_key='*')");
                    }
                }
                //特殊處理 回單作業
                if($table_name == 'mod_order as confirmorder') {
                    $today       = new \DateTime();
                    $str_date    = $today->format('Ymd');
                    $this_query->where("confirm_dt",">=",$str_date);
                }
                //特殊處理end
                if($request->sorttype!="" && $request->sortfield!="" ){
                    $sorttype =explode(',', $request->sorttype);
                    $sortfield =explode(',', $request->sortfield);
                    for($i = 0; $i< count($sorttype) ; $i++){
                        $this_query->orderBy($sortfield[$i], $sorttype[$i]);
                    }
                }
                $prod = $this_query->get();

                $sheet->row(1, $headers);
                foreach($prod as $key=>$row) {
                    $body = array();
                    foreach($row as $val) {
                        if(preg_match("/^[0-9,.]+$/", $val)) {
                            $val = str_replace(',', '', $val);
                        }
                        array_push($body, $val);
                    }
                    $sheet->row($key+2, $body);
                    if($headorder!=""){
                        if(isset($row->ord_no)){
                            $nonum= $this->num2alpha($headorder);
                            $sheet->setCellValueExplicit($nonum.($key+2),$row->ord_no, \PHPExcel_Cell_DataType::TYPE_STRING);
                        }
                    }
                    if($pickphone!=""){
                        if(isset($row->pick_tel)){
                            $nonum= $this->num2alpha($pickphone);
                            $sheet->setCellValueExplicit($nonum.($key+2),$row->pick_tel, \PHPExcel_Cell_DataType::TYPE_STRING);
                        }
                    }
                    if($dlvphone!=""){
                        if(isset($row->dlv_tel)){
                            $nonum= $this->num2alpha($dlvphone);
                            $sheet->setCellValueExplicit($nonum.($key+2),$row->dlv_tel, \PHPExcel_Cell_DataType::TYPE_STRING);
                        }
                    }
                    if($snno!=""){
                        if(isset($row->sn_no)){
                            $nonum= $this->num2alpha($snno);
                            $sheet->setCellValueExplicit($nonum.($key+2),$row->sn_no, \PHPExcel_Cell_DataType::TYPE_STRING);
                        }
                    }
                    // $pickphone,$dlvphone
                }
        
            });
            
        })->store('xls', storage_path('app/excel'));

        DB::table('export_grid_excel')
        ->where('id', $logid)
        ->update([
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);

        return [
            "msg"=>"success", "downlink"=>$fileName.'-'.$today.".xls"
        ];;   

    }
    public function exportDataold(Request $request) {

        //dd(json_encode($request->content));
        $d = json_decode($request->content);

        $today = date('Y-m-d');
        $fileName = $request->filename;        
        $headerordno =0;
        $ordheadercount = 0;
        Excel::create($fileName.'-'.$today, function($excel) use($d,$fileName,$ordheadercount,$headerordno) {
            $excel->sheet($fileName, function($sheet) use($d,$fileName,$ordheadercount,$headerordno) {
                $ordarray = array();
                $header = array();
                if(count($d) > 0) {
                    foreach($d[0] as $key=>$val) {
                        $ordheadercount++;
                        if($key=="訂單號"){
                            $headerordno =$ordheadercount;
                        }
                        array_push($header, $key);
                    }
                }
                $nonum= $this->num2alpha($headerordno-1);
                $sheet->row(1, $header);
                foreach($d as $key=>$row) {
                    $body = array();
                    $i=0;
                    foreach($d[$key] as $val) {
                        $i=$i+1;
                        if(preg_match("/^[0-9,.]+$/", $val)) {
                            $val = str_replace(',', '', $val);
                        }
                        if($i==$headerordno && ($fileName =="訂單作業" ||$fileName=="回單作業")){
                            array_push($body, strval($val)." ");
                            array_push($ordarray, strval($val));
                        }else
                        {
                            array_push($body, strval($val));
                        }
                    }

                    $sheet->row($key+2, $body);
                    foreach($ordarray as $keyvalue=> $val) {
                        // \Log::info($nonum.($keyvalue+2)."/".$val);
                        $sheet->setCellValueExplicit($nonum.($keyvalue+2), $val, \PHPExcel_Cell_DataType::TYPE_STRING);
                    }
                }
            });
            
        })->export('xlsx');   

    }
    function num2alpha($n)  //數字轉英文(0=>A、1=>B、26=>AA...以此類推)
    {
        for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
            $r = chr($n%26 + 0x41) . $r; 
        return $r; 
    } 
}
