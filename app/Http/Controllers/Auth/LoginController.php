<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Backpack\Base\app\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
class LoginController extends Controller
{
    protected $data = []; // the information we send to the view
    protected $redirectTo = "admin/board";
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers {
        logout as defaultLogout;
        login as defaultLogin;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);

        // ----------------------------------
        // Use the admin prefix in all routes

        // If not logged in redirect here.
        $this->loginPath = property_exists($this, 'loginPath') ? $this->loginPath
            : config('backpack.base.route_prefix', 'admin').'/login';

        // Redirect here after successful login.
        $this->redirectTo = property_exists($this, 'redirectTo') ? $this->redirectTo
            : config('backpack.base.route_prefix', 'admin').'/board';

        // Redirect here after logout.
        $this->redirectAfterLogout = property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout
            : config('backpack.base.route_prefix', 'admin').'/board';
        // ----------------------------------
    }

    // -------------------------------------------------------
    // Laravel overwrites for loading backpack views
    // -------------------------------------------------------

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $this->data['title'] = trans('backpack::base.login'); // set the page title

        if(Session::get('loginStatus')){
            $this->data['loginStatus'] = Session::get('loginStatus');
            Session::forget('loginStatus');
        }else{
            $this->data['loginStatus'] = null;
        }
        return view('backpack::auth.login', $this->data);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
     protected function credentials(Request $request)
     {
        $request->merge(['password' => trim($request->password)]);
        //  dd($request->password);
        //  return $request->only($this->username(), 'password', 'c_key');
        return array_merge($request->only($this->username(), 'password', 'c_key'), ['account_enable' => 'Y']);
     }

    /**
     * Log the user out and redirect him to specific location.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $user = Auth::user();
        // Do the default logout procedure
        if($user != null ) {
            DB::table('users')->where('email', $user->email)->update(['online' => '0']);
            $this->defaultLogout($request);
        }
        // And redirect to custom location
        return redirect($this->redirectAfterLogout);
    }

    /**
     * Log the user in and redirect him to specific location.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
     public function login(Request $request)
     {

        if (array_key_exists($request->input('lang'), config('app.locales'))) {
            session(['applocale' => $request->input('lang')]);
        }

        
         // Do the default login procedure
         
         $this->defaultLogin($request);
         $user = Auth::user();

         if($user == null){
            session(['loginStatus' => "login failed."]);
            return redirect($this->loginPath);
         }else{
            $ueserrole= DB::table('role_users')->where('user_id', $user->id)->value('role_id');
            if(isset($ueserrole)){
                $redirectto= DB::table('bscode')->where('cd_type', 'LOGINPAGE')->where('g_key', $user->c_key)->where('cd', $ueserrole)->value('value1');
                DB::table('users')->where('email', $user->email)->update(['online' => '1']);
                if(isset($redirectto)){
                    return redirect(config('backpack.base.route_prefix', 'admin').'/'.$redirectto);
                }else{
                    return redirect(config('backpack.base.route_prefix', 'admin').'/'.'board');
                }
            }else{
                return redirect(config('backpack.base.route_prefix', 'admin').'/'.'board');
            }
         }
         // And redirect to custom location
         

         
     }
     public function checklogin(Request $request){
        $userdata = DB::table('users')->where('email', $request->email)->first();
        if($userdata->online =="1"){
            return ["msg"=>"error"];
        }else{
            return ["msg"=>"success","data"=>"Password Error"];
        }
     }
}
