<?php

namespace App\Http\Controllers;
use Response;
use Excel;
use Carbon\Carbon;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Models\CommonModel;
use App\Models\OrderDetailModel;
use App\Models\TmpModOrderModel;
use App\Models\TrackingModel;
use App\Models\OrderMgmtModel;
use App\Models\SenaoModel;
use App\Models\BaseModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\GoogleMapModel;
use Illuminate\Http\UploadedFile;
use File;
class ExcelImportController extends Controller
{
    public function import()
	{
        $title = trans('excel.titleName');
        echo "<title>$title</title>";
		return view('excelImport/Import');
	}

    public function importByweblink()
	{
        $title = trans('excel.titleName');
        echo "<title>$title</title>";
		return view('excelImport/weblinkImport');
	}
	public function downloadExcel($type)
	{
		$data = Item::get()->toArray();
		return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download($type);
    }
    public function checkExcelname(){
        $user = Auth::user();
        $name = request('name');
        $isexist =DB::table('sys_excelimport_log')
        ->where('excelname', $name)
        ->count();
        $isexist2 =DB::table('tmp_mod_order')
        ->where('excelname', $name)
        ->where('created_by', '!=', $user->email)
        ->count();
        if($isexist>0){
            return response()->json(array('msg' => 'error',"errorLog"=>"此檔案名稱已經匯入過"));
        }
        if($isexist2>0){
            $usermail =DB::table('tmp_mod_order')
            ->where('excelname', $name)
            ->first();
            $username = DB::table('users')
            ->where('email', $usermail->created_by)
            ->first();
            $result = $username->name;
            return response()->json(array('msg' => 'error',"errorLog"=>"此檔案正在被「".$result."」處理中，請進行確認!"));
        }
        return response()->json(array('msg' => 'success'));
    }

    public function checkExcelnameByWeblink(){
        $user = Auth::user();
        $name = request('name');
        $isexist =DB::table('sys_excelimport_log')
        ->where('excelname', $name)
        ->count();
        $isexist2 =DB::table('tmp_mod_order_weblink')
        ->where('excelname', $name)
        ->count();
        if($isexist>0){
            return response()->json(array('msg' => 'error',"errorLog"=>"此檔案名稱已經匯入過"));
        }
        if($isexist2>0){
            $usermail =DB::table('tmp_mod_order_weblink')
            ->where('excelname', $name)
            ->first();
            $username = DB::table('users')
            ->where('email', $usermail->created_by)
            ->first();
            $result = $username->name;
            return response()->json(array('msg' => 'error',"errorLog"=>"此檔案正在被「".$result."」處理中，請進行確認!"));
        }
        return response()->json(array('msg' => 'success'));
    }

    public function importExcelname(){
        $user = Auth::user();
        $name = request('name');
        DB::table('tmp_mod_order')
        ->where('created_by', $user->email)
        ->where('g_key',$user->g_key)
        ->update([
            'excelname' =>$name,
        ]);
        return response()->json(array('msg' => 'success'));
    }
	public function importExcel()
	{
        ini_set('memory_limit', '512M');
        $user = Auth::user();
        DB::table('tmp_mod_order')
                ->where('created_by', $user->email)
                ->where('g_key',$user->g_key)
                ->delete();
                
        $obj = array(
            "到貨日期"     => "etd",
            "訂單號"      => "ord_no",
            "貨主代碼"     => "owner_cd",
            "貨主名稱"     => "owner_nm",
            "溫層"       => "temperate",
            "運輸類型"     => "trs_mode",
            "備註"       => "remark",
            "提貨客戶代碼"   => "pick_cust_no",
            "提貨客戶名稱"   => "pick_cust_nm",
            "提貨聯絡人"    => "pick_attn",
            "提貨郵地區號"   => "pick_zip",
            "提貨地址"     => "pick_addr",
            "提貨電話"     => "pick_tel",
            "提貨電話2"    => "pick_tel2",
            "提貨e_mail" => "pick_email",
            "提貨備註"     => "pick_remark",
            "配送客戶代碼"   => "dlv_cust_no",
            "配送客戶名稱"   => "dlv_cust_nm",
            "配送聯絡人"    => "dlv_attn",
            "配送郵地區號"   => "dlv_zip",
            "配送地址"     => "dlv_addr",
            "配送電話"     => "dlv_tel",
            "配送電話2"    => "dlv_tel2",
            "配送e_mail" => "dlv_email",
            "配送備註"     => "dlv_remark",
            "商品編號1"    => "goods_no",
            "商品編號2"    => "goods_no2",
            "商品名稱"     => "goods_nm",
            "件數"       => "pkg_num",
            "數量單位"     => "pkg_unit",
            "總材積"      => "total_cbm",
            "總重量"      => "total_gw",
            "代收款"      => "collectamt",
            "修正費用"     => "cust_amt",
            "費用備註"     => "amt_remark",
            "發送mail貨主" => "owner_send_mail",
            "發送mail提貨" => "pick_send_mail",
            "發送mail配送" => "dlv_send_mail",
            "站別"       => "s_key",
            "運輸公司代碼"   => "truck_cmp_no",
            "運輸公司名稱"   => "truck_cmp_nm",
            "客戶訂單編號"   => "cust_ord_no",
            "倉庫訂單號碼"   => "wms_order_no",
            "料品類別"     => "goods_type",
            "aspcode"  => "asp_code",
        );
        try{
            if(Input::hasFile('import_file')){
                $path = Input::file('import_file')->getRealPath();
                Input::file('import_file')->storeAs('tmpexcel', Input::file('import_file')->getClientOriginalName());
                $data = Excel::load($path, function($reader) {
                    $reader->setDateFormat('Y-m-d');
                })->get();

                $i = 0;
                $dataFirst = $data[0];
                $header = $dataFirst->first()->keys()->toArray();
                $insert = array();
                // dd( $header);
                if(!empty($dataFirst) && $dataFirst->count()){
                    foreach ($dataFirst as $key => $value) {
                        //$obj[$header[$i]] = $value->$header[$i];
                        $result = array();
                        for($i=0;$i<count($value);$i++) {
                            if(isset($obj[$header[$i]])) {
                                \Log::info($value[$header[$i]]);
                                $o = array(
                                    $obj[$header[$i]] => $value[$header[$i]]
                                );
                                $result = array_merge($result, $o);
                            }
                            
                        }
                        $result['g_key']      = $user->g_key;
                        $result['c_key']      = $user->c_key;
                        $result['s_key']      = isset($result['s_key'])?$result['s_key']:$user->s_key;
                        $result['d_key']      = $user->d_key;
                        $result['created_by'] = $user->email;
                        $result['created_at'] = date("Y-m-d H: i: s");
                        $result['status']     = "UNTREATED";
                        if(!empty($result['ord_no'])) {
                            array_push($insert,  $result);
                        }
                        // dd($result);
                        \Log::info($result);
                        // $goodsno = $result['goods_no'];
                        // if( $goodsno==null && (!empty($result['ord_no'])) ){
                        //     return ["msg"=>"error","errorLog"=>"商品編號不可為空"];
                        // }
                        //$insert[] = ['title' => $value->title, 'description' => $value->description];
                        if(!empty($insert) ){
                            DB::table('tmp_mod_order')->insert($insert);
                            $insert = [];
                            //dd($insert);
                        }
                    }

                    if(!empty($insert)){
                        DB::table('tmp_mod_order')->insert($insert);
                        //dd($insert);
                    }
                }
            }
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage(), "errorLog"=>$e->getLine()];
        }
        //return back();
        return ["msg"=>"success", "data"=>$insert];
	}
    public function importExcelByWeblink()
	{
        ini_set('memory_limit', '512M');
        $user = Auth::user();
        DB::table('tmp_mod_order_weblink')
                ->where('created_by', $user->email)
                ->where('g_key',$user->g_key)
                ->delete();

        $obj = array(
            '揀料單號' => 'pick_no',
            '客戶代號' => 'cust_no',
            '客戶名稱' => 'cust_name',
            '廠商名稱' => 'truck_cmp_name',
            '採購單號' => 'purchase_no',
            '採購人員' => 'purchase_by',
            '商品編號' => 'product_no',
            '廠商料號' => 'goods_no',
            '商品名稱' => 'product_name',
            '國際條碼' => 'global_barcode',
            '數量'     => 'quantity',
            '裝箱數'   => 'qty',
            '目前箱號' => 'bos_no',
            '總箱數'   => 'total_box_num',
            '顏色'     => 'color',
            '款式'     => 'shape',
            '尺寸'     => 'size',
            '類別'     => 'category',
            '單品長'   => 'product_length',
            '單品寬'   => 'product_width',
            '單品高'   => 'product_high',
            '箱長'     => 'product_weight',
            '箱寬'     => 'box_length',
            '箱高'     => 'box_width',
            '單品重量' => 'box_height',
            '箱重量'   => 'box_weight'
        );

        try{
            if(Input::hasFile('import_file')){
                $path = Input::file('import_file')->getRealPath();
                Input::file('import_file')->storeAs('tmpexcel', Input::file('import_file')->getClientOriginalName());
                $excelName = Input::file('import_file')->getClientOriginalName();
                $data = Excel::load($path, function($reader) {
                    $reader->setDateFormat('Y-m-d');
                })->get();

                $i = 0;
                $dataFirst = $data[0];
                $header = $dataFirst->first()->keys()->toArray();
                $insert = array();
                // dd( $header);
                if(!empty($dataFirst) && $dataFirst->count()){
                    foreach ($dataFirst as $key => $value) {
                        //$obj[$header[$i]] = $value->$header[$i];
                        $result = array();
                        for($i=0;$i<count($value);$i++) {
                            if(isset($obj[$header[$i]])) {
                                \Log::info($value[$header[$i]]);
                                $o = array(
                                    $obj[$header[$i]] => $value[$header[$i]]
                                );
                                $result = array_merge($result, $o);
                            }
                        }

                        $result['g_key']      = $user->g_key;
                        $result['c_key']      = $user->c_key;
                        $result['s_key']      = isset($result['s_key'])?$result['s_key']:$user->s_key;
                        $result['d_key']      = $user->d_key;
                        $result['created_by'] = $user->email;
                        $result['created_at'] = date("Y-m-d H: i: s");
                        $result['excelname']  = $excelName;

                        array_push($insert,  $result);

                        if(!empty($insert) ){
                            DB::table('tmp_mod_order_weblink')->insert($insert);
                            $insert = [];
                        }
                    }

                    if(!empty($insert)){
                        DB::table('tmp_mod_order_weblink')->insert($insert);
                    }
                }
            }
        }catch (\Exception $e) {
            return ["msg"=>"error", "errormsg"=>$e->getMessage(), "errorLog"=>$e->getLine()];
        }
        //return back();
        return ["msg"=>"success", "data"=>$insert];
	}

    public function get() {
        //TmpModOrderModel::getQuery()->delete();
        $tmpModOrder = [];
        $user = Auth::user();
        
        $this_query = DB::table('tmp_mod_order')
                        ->where('created_by', $user->email)
                        ->where('g_key',$user->g_key);
    
        $tmpModOrder = $this_query->get();
            
        
        
        $data[] = array(
            'Rows' => $tmpModOrder,
        );

        return response()->json($data);
    }

    public function saveToModOrder() {
        try {
            $json = request('obj');
            $data = json_decode($json, true);
            $sports = array();
            $user = Auth::user();
            $flag = "N";
            $tmpIds = array();
            foreach($data as $row) {
                array_push($tmpIds, $row['id']);
            }

            $tmpData = DB::table('tmp_mod_order')
                            ->whereIn('id', $tmpIds)
                            ->where('created_by', $user->email)
                            ->orderBy('ord_no', 'asc')
                            ->get();
            $excelname = "";
            if(count($tmpData) >  0) {
                $oldOrdNo = '';
                $ordId = '';
                $detailOrdId = '';
                foreach ($tmpData as $key=>$item) {
                    //新增主檔
                    if($oldOrdNo != $item->ord_no) {
                        if($ordId != '') {
                            //$this->updateModOrderGw($ordId, $user->g_key, $user->c_key);
                        }

                        $ordData = DB::table('mod_order')
                                ->where('ord_no', $item->ord_no)
                                ->where('g_key', $user->g_key)
                                ->where('c_key', $user->c_key)
                                ->where('owner_cd', $item->owner_cd)
                                ->whereIn('status', ['UNTREATED', 'SEND', 'LOADING'])
                                ->get();
                        if(count($ordData) == 0) {
                            $today       = new \DateTime();
                            $str_date    = $today->format('Ym');
                            $params = array(
                                "c_key"   => $user->c_key,
                                "date_ym" => substr($str_date, 2, strlen($str_date))
                            );
                            $BaseModel = new BaseModel();
                            $autoOrdNo = $BaseModel->getAutoNumber("order",$params);

                            $dataddsc = date('Y-m-d H:i:s');
                            $modOrder = new OrderMgmtModel;
                            $modOrder->dlv_cust_nm    = $item->dlv_cust_nm;
                            if(!empty($item->etd)){
                                $date = Carbon::parse($item->etd)->timezone(config('app.timezone'))->format('Y-m-d');
                                $modOrder->etd    = $date;                            
                            }
            
                            $dlvData = null;
                            if(isset($item->dlv_cust_no)) {
                                $dlvData = DB::table('sys_customers')
                                                ->where('cust_no', $item->dlv_cust_no)
                                                ->where('g_key', $user->g_key)
                                                ->where('c_key', $user->c_key)
                                                ->first();
                            }
                            $pickData = null;
                            if(isset($item->pick_cust_no)) {
                                $pickData = DB::table('sys_customers')
                                                ->where('cust_no', $item->pick_cust_no)
                                                ->where('g_key', $user->g_key)
                                                ->where('c_key', $user->c_key)
                                                ->first();
                            }

                            $ownerData = null;
                            if(isset($item->owner_cd)) {
                                $ownerData = DB::table('sys_customers')
                                                ->where('cust_no', $item->owner_cd)
                                                ->where('g_key', $user->g_key)
                                                ->where('c_key', $user->c_key)
                                                ->first();
                            }

                            $modOrder->owner_cd = $item->owner_cd;
                            if(isset($ownerData)) {
                                $modOrder->owner_nm = $ownerData->cname;
                            }
                            else {
                                $modOrder->owner_nm = $item->owner_nm;
                            }
            
                            $modOrder->driver     = $item->driver;
                            $modOrder->remark     = $item->remark;
                            $modOrder->ord_no     = $item->ord_no;
                            $modOrder->dlv_cust_no = $item->dlv_cust_no;
                            $pick_addr = "";
                            $dlv_addr  = "";

                            if(isset($dlvData)) {
                                $modOrder->dlv_cust_nm = $dlvData->cname;
                                $modOrder->dlv_zip     = (isset($item->dlv_zip))?$item->dlv_zip:$dlvData->zip;
                                $modOrder->dlv_city_no = $dlvData->city_no;
                                
                                $distData = DB::table('sys_area')->where('dist_cd', $item->dlv_zip)->first();


                                $modOrder->dlv_area_id = $dlvData->area_id;
                                $modOrder->dlv_addr    = (isset($item->dlv_addr))?$item->dlv_addr:$dlvData->address;
                                $modOrder->dlv_attn    = (isset($item->dlv_attn))?$item->dlv_attn:$dlvData->contact;
                                $modOrder->dlv_email   = (isset($item->dlv_email))?$item->dlv_email:$dlvData->email;
                                $modOrder->dlv_tel     = (isset($item->dlv_tel))?$item->dlv_tel:$dlvData->phone;
                                $modOrder->dlv_tel2     = (isset($item->dlv_tel2))?$item->dlv_tel2:$dlvData->phone2;
                                $modOrder->dlv_remark  = (isset($item->dlv_remark))?$item->dlv_remark:$dlvData->remark;
                                $dlv_addr = (isset($item->dlv_addr))?$item->dlv_addr:$dlvData->address;
                            }
                            else {
                                $modOrder->dlv_cust_nm = $item->dlv_cust_nm;
                                $modOrder->dlv_zip     = $item->dlv_zip;
                                $distData = DB::table('sys_area')->where('dist_cd', $item->dlv_zip)->first();
                                if(isset($distData)) {
                                    $modOrder->dlv_city_no = (isset($item->dlv_city_no))?$item->dlv_city_no:$distData->city_cd;
                                    // $modOrder->dlv_city_nm = (isset($item->dlv_city_nm))?$item->dlv_city_nm:$distData->city_nm;
                                    $modOrder->dlv_area_id = (isset($item->dlv_area_id))?$item->dlv_area_id:$distData->dist_cd;
                                    // $modOrder->dlv_area_nm = (isset($item->dlv_area_nm))?$item->dlv_area_nm:$distData->dist_nm;
                                }
                                else {
                                    $modOrder->dlv_city_no = $item->dlv_city_no;
                                    // $modOrder->dlv_city_nm = $item->dlv_city_nm;
                                    $modOrder->dlv_area_id = $item->dlv_area_id;
                                    // $modOrder->dlv_area_nm = $item->dlv_area_nm;
                                }
                                
                                $modOrder->dlv_addr    = $item->dlv_addr;
                                $modOrder->dlv_attn    = $item->dlv_attn;
                                $modOrder->dlv_email   = $item->dlv_email;
                                $modOrder->dlv_tel     = $item->dlv_tel;
                                $modOrder->dlv_tel2    = $item->dlv_tel2;
                                $modOrder->dlv_remark  = $item->dlv_remark;
                                if($item->temperate==""){
                                    $modOrder->temperate   = "N";
                                    $modOrder->temperate_desc = "常溫";
                                }else{
                                    $modOrder->temperate   = $item->temperate;
                                }
                                $temperate_desc = DB::table('bscode')->where('cd_type','TEMPERATE')->where('cd',$item->temperate)->first();
                                if(isset($temperate_desc)) {
                                    $modOrder->temperate_desc = $temperate_desc->cd_descp;
                                }
                                $dlv_addr = $item->dlv_addr;
                            }
                            \Log::info($item->cust_ord_no);
                            $modOrder->collectamt  = $item->collectamt;
                            $modOrder->cust_amt  = $item->cust_amt;
                            $modOrder->cust_ord_no  = $item->cust_ord_no;
                            $modOrder->wms_order_no  = $item->wms_order_no;
                            // if($item->is_forward==5) {
                            //     $dlvaspData = DB::table('sys_customers')
                            //     ->where('cust_no', $item->asp_code)
                            //     ->first();
                            //     if(isset($dlvaspData)){
                            //         $modOrder->dlv_cust_nm = $dlvaspData->cname;
                            //         $modOrder->dlv_zip     = $dlvaspData->zip;
                            //         $modOrder->dlv_city_no = $dlvaspData->city_no;
                            //         // $modOrder->dlv_city_nm = $dlvaspData->city_nm;
                            //         $modOrder->dlv_area_id = $dlvaspData->area_id;
                            //         // $modOrder->dlv_area_nm = $dlvaspData->area_nm;
                            //         $modOrder->dlv_addr    = $dlvaspData->address;
                            //         $modOrder->dlv_attn    = $dlvaspData->contact;
                            //         $modOrder->dlv_email   = $dlvaspData->email;
                            //         $modOrder->dlv_tel     = $dlvaspData->phone;
                            //         $modOrder->dlv_tel2    = $dlvaspData->phone2;
                            //         $modOrder->dlv_remark  = $dlvaspData->remark;
                            //         $dlv_addr =$dlvaspData->address;
                            //     }
                            //     $modOrder->is_forward  = "N";
                            // }else{
                            //     $modOrder->is_forward  = "Y";
                            // }

                            $dlvaddr = $modOrder->dlv_addr;
                            // $dlvaddr  = str_replace($modOrder->dlv_city_nm,"",$dlvaddr);
                            // $dlvaddr  = str_replace($modOrder->dlv_area_nm,"",$dlvaddr);
                            // $modOrder->dlv_addr_info = $modOrder->dlv_city_nm.$modOrder->dlv_area_nm.$dlvaddr;
                            // $modOrder->dlv_addr_info = $modOrder->dlv_zip.$modOrder->dlv_city_nm.$modOrder->dlv_area_nm.$modOrder->dlv_addr;
                            $modOrder->pick_cust_no = $item->pick_cust_no;
                            if(isset($pickData)) {
                                // $modOrder->pick_cust_nm = $pickData->cname;
                                // $modOrder->pick_zip     = $pickData->zip;
                                // $modOrder->pick_city_no = $pickData->city_no;
                                // $modOrder->pick_city_nm = $pickData->city_nm;
                                // $modOrder->pick_area_id = $pickData->area_id;
                                // $modOrder->pick_area_nm = $pickData->area_nm;
                                // $modOrder->pick_addr    = $pickData->address;
                                // $modOrder->pick_attn    = $pickData->contact;
                                // $modOrder->pick_email   = $pickData->email;
                                // $modOrder->pick_tel     = $pickData->phone;
                                $pick_addr = $pickData->address;
                                $distData = DB::table('sys_area')->where('dist_cd', $item->pick_zip)->first();

                                $modOrder->pick_cust_nm = $pickData->cname;
                                $modOrder->pick_zip     = (isset($item->pick_zip))?$item->pick_zip:$pickData->zip;
                                $modOrder->pick_city_no = $pickData->city_no;
                                try {
                                    if(isset($distData)) {
                                        // $modOrder->pick_city_nm = (isset($item->pick_city_nm))?$item->pick_city_nm:$distData->city_nm;
                                        // $modOrder->pick_area_nm = (isset($item->pick_area_nm))?$item->pick_area_nm:$distData->area_nm;
                                    } else {
                                        // $modOrder->pick_city_nm = (isset($item->pick_city_nm))?$item->pick_city_nm:$pickData->city_nm;
                                        // $modOrder->pick_area_nm = (isset($item->pick_area_nm))?$item->pick_area_nm:$pickData->area_nm;
                                    }
                                } catch (\Throwable $th) {
                                    // $modOrder->pick_area_nm = (isset($item->pick_area_nm))?$item->pick_area_nm:$pickData->area_nm;
                                }
                                $modOrder->pick_area_id = $pickData->area_id;
                                $modOrder->pick_addr    = (isset($item->pick_addr))?$item->pick_addr:$pickData->address;
                                $modOrder->pick_attn    = (isset($item->pick_attn))?$item->pick_attn:$pickData->contact;
                                $modOrder->pick_email   = (isset($item->pick_email))?$item->pick_email:$pickData->email;
                                $modOrder->pick_tel     = (isset($item->pick_tel))?$item->pick_tel:$pickData->phone;
                                $modOrder->pick_tel2     = (isset($item->pick_tel2))?$item->pick_tel2:$pickData->phone2;
                                $modOrder->pick_remark  = (isset($item->pick_remark))?$item->pick_remark:$pickData->remark;
                                $pick_addr = (isset($item->pick_addr))?$item->pick_addr:$pickData->address;
                            }
                            else {
                                $modOrder->pick_cust_nm = $item->pick_cust_nm;
                                $modOrder->pick_zip     = $item->pick_zip;
                                $distData = DB::table('sys_area')->where('dist_cd', $item->pick_zip)->first();
                                if(isset($distData)) {
                                    $modOrder->pick_city_no = (isset($item->pick_city_no))?$item->pick_city_no:$distData->city_cd;
                                    // $modOrder->pick_city_nm = (isset($item->pick_city_nm))?$item->pick_city_nm:$distData->city_nm;
                                    $modOrder->pick_area_id = (isset($item->pick_area_id))?$item->pick_area_id:$distData->dist_cd;
                                    // $modOrder->pick_area_nm = (isset($item->pick_area_nm))?$item->pick_area_nm:$distData->dist_nm;
                                }
                                else {
                                    $modOrder->pick_city_no = $item->pick_city_no;
                                    // $modOrder->pick_city_nm = $item->pick_city_nm;
                                    $modOrder->pick_area_id = $item->pick_area_id;
                                    // $modOrder->pick_area_nm = $item->pick_area_nm;
                                }
                                
                                // $modOrder->pick_city_no = $item->pick_city_no;
                                // $modOrder->pick_city_nm = $item->pick_city_nm;
                                // $modOrder->pick_area_id = $item->pick_area_id;
                                // $modOrder->pick_area_nm = $item->pick_area_nm;
                                $modOrder->pick_addr    = $item->pick_addr;
                                $modOrder->pick_attn    = $item->pick_attn;
                                $modOrder->pick_email   = $item->pick_email;
                                $modOrder->pick_tel     = $item->pick_tel;
                                $modOrder->pick_tel2     = $item->pick_tel2;
                                $modOrder->pick_remark  = $item->pick_remark;
                                $pick_addr = $item->pick_addr;
                            }
                            $pickaddr = $modOrder->pick_addr;
                            // $pickaddr  = str_replace($modOrder->pick_city_nm,"",$pickaddr);
                            // $pickaddr  = str_replace($modOrder->pick_area_nm,"",$pickaddr);
                            // $modOrder->pick_addr_info = $modOrder->pick_city_nm.$modOrder->pick_area_nm.$pickaddr;
                            
                            //$modOrder->pkg_num    = $item->pkg_num;
                            $modOrder->amt_remark = $item->amt_remark;
                            $modOrder->total_cbm  = $item->total_cbm;
                            $modOrder->total_gw   = $item->total_gw;
                            $modOrder->g_key      = $item->g_key;
                            $modOrder->c_key      = $item->c_key;
                            $modOrder->s_key      = $item->s_key;
                            $modOrder->d_key      = $item->d_key;
                            $modOrder->created_by = $user->email;
                            $modOrder->updated_by = $user->email;
                            $modOrder->status     = "UNTREATED";
                            $modOrder->status_desc= "尚未安排";
                            $modOrder->trs_mode   = $item->trs_mode;
                            $TM_desc = DB::table('bscode')->where('cd_type','TM')->where('cd',$item->trs_mode)->first();
                            if(isset($TM_desc)) {
                                $modOrder->trs_mode_desc = $TM_desc->cd_descp;
                            }
                            //是否寄發email
                            $ownerSendMail = "N";
                            $pickSendMail = "N";
                            $dlvSendMail = "N";

                            
                            if(isset($item->owner_send_mail)) {
                                $ownerSendMail = $item->owner_send_mail;
                            }
                            else {
                                if(isset($item->owner_cd)) {
                                    $ownerSendMail = DB::table('sys_customers')
                                                        ->where('cust_no', $item->owner_cd)
                                                        ->where('g_key', $user->g_key)
                                                        ->where('c_key', $user->c_key)
                                                        ->value('receive_mail');
                                }
                            }
                            
                            if(isset($item->pick_send_mail)) {
                                $pickSendMail = $item->pick_send_mail;
                            }
                            else {
                                if(isset($item->pick_cust_no)) {
                                    $pickSendMail = DB::table('sys_customers')
                                                        ->where('cust_no', $item->pick_cust_no)
                                                        ->where('g_key', $user->g_key)
                                                        ->where('c_key', $user->c_key)
                                                        ->value('receive_mail');
                                }
                            }
                            
                            if(isset($item->dlv_send_mail)) {
                                $dlvSendMail = $item->dlv_send_mail;
                            }
                            else {
                                if(isset($item->dlv_cust_no)) {
                                    $dlvSendMail = DB::table('sys_customers')
                                                        ->where('cust_no', $item->dlv_cust_no)
                                                        ->where('g_key', $user->g_key)
                                                        ->where('c_key', $user->c_key)
                                                        ->value('receive_mail');
                                }
                            }

                            $modOrder->owner_send_mail = $ownerSendMail;
                            $modOrder->pick_send_mail = $pickSendMail;
                            $modOrder->dlv_send_mail = $dlvSendMail;
                            
                            if( $item->truck_cmp_no != '' ||  $item->truck_cmp_nm != '') {
                                $modOrder->truck_cmp_no = $item->truck_cmp_no;
                                $modOrder->truck_cmp_nm = $item->truck_cmp_nm;
                            } else {
                                $carData = DB::table('sys_customers_item')->where('cust_type', 'CT003')->where('def', 1)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->first();
                                if(isset($carData)) {
                                    $modOrder->truck_cmp_no = $carData->cust_no;
                                    $modOrder->truck_cmp_nm = $carData->cname;
                                }
                            }
                            // $carData = DB::table('sys_customers_item')->where('cust_type', 'CT003')->where('cust_no', $item->truck_cmp_no)->where('def', 1)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->first();
                            // // $carData = DB::table('sys_customers_item')->where('cust_type', 'CT003')->where('def', 1)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->first();
                            // if(isset($carData)) {
                            //     $modOrder->truck_cmp_no = $carData->cust_no;
                            //     if(isset($carData->cname)) {
                            //         $modOrder->truck_cmp_nm = $carData->cname;
                            //     }
                            //     else {
                            //         $modOrder->truck_cmp_nm = $carData->ename;
                            //     }
                            // }
                            // else {
                            //     $carData = DB::table('sys_customers_item')->where('cust_type', 'CT003')->where('def', 1)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->first();
                            //     if(isset($carData)) {
                            //         $modOrder->truck_cmp_no = $carData->cust_no;
                            //         $modOrder->truck_cmp_nm = $carData->cname;
                            //     }
                            // }

                            $whData = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->s_key)->where('identity', 'S')->first();
                            if(isset($whData)) {
                                $modOrder->wh_addr  = $whData->city_nm.$whData->area_nm.$whData->address;
                            }
                            else {
                                $modOrder->wh_addr = '';
                            }

                            $dist = 0;
                            if(!empty($dlv_addr) && !empty($pick_addr) && ($item->trs_mode == 'C' || $item->trs_mode == 'C01' || $item->trs_mode == 'C02'))
                            {
                                $g = new GoogleMapModel;                        
                                $dist = $g->getDist($modOrder->pick_city_nm.$modOrder->pick_area_nm.$pick_addr, $modOrder->dlv_city_nm.$modOrder->dlv_area_nm.$dlv_addr, $modOrder->wh_addr, $item->trs_mode);
                            }

                            $modOrder->distance   = $dist;
                            $modOrder->sys_ord_no = $autoOrdNo;

                            $modOrder->save();

                            $tm = new TrackingModel();
                            $tm->insertTracking('', $item->ord_no, '', $autoOrdNo, '', 'A', 1, $user->g_key, $user->c_key, $user->s_key, $user->d_key);


                            $ordId = $modOrder->id;
                            $detailOrdId = $modOrder->id;
                        }else{
                            $ordId = $ordData[0]->id;
                        }
                    }         

                    $oldOrdNo = $item->ord_no;
            
                    
                    //新增明細檔
                    $orderDetail             = new OrderDetailModel;
                    $orderDetail->goods_type = $item->goods_type;
                    $orderDetail->ord_no     = $item->ord_no;
                    $orderDetail->goods_no   = $item->goods_no;
                    $orderDetail->goods_no2  = $item->goods_no2;
                    $orderDetail->pkg_num    = $item->pkg_num;
                    $orderDetail->gw         = (isset($item->total_gw))?$item->total_gw:0;
                    $orderDetail->cbm        = (isset($item->total_cbm))?$item->total_cbm:0;
                    $orderDetail->g_key      = $user->g_key;
                    $orderDetail->c_key      = $user->c_key;
                    $orderDetail->s_key      = $user->s_key;
                    $orderDetail->d_key      = $user->d_key;
                    if(!empty($ordId)) {
                        $orderDetail->ord_id = $ordId;
                    }
                    else {
                        $orderDetail->ord_id = $ordData[0]->id;
                        $detailOrdId = $orderDetail->ord_id;
                    }
                    $goodsData = DB::table('mod_goods')
                                ->where('g_key', $user->g_key)
                                ->where('c_key', $user->c_key)
                                ->where('goods_no', $item->goods_no)
                                ->where('owner_cd', $item->owner_cd)
                                ->first();
                    if(isset($goodsData)) {
                        $orderDetail->goods_nm  = $goodsData->goods_nm;
                        $orderDetail->goods_no2 = $goodsData->goods_no2;
                        $orderDetail->pkg_unit  = $goodsData->pkg_unit;
                        //$orderDetail->goods_nm2 = $goodsData->goods_nm2;
                        $orderDetail->gwu       = $goodsData->gwu;
                        $orderDetail->cbm       = $goodsData->cbm;
                        $orderDetail->cbmu      = $goodsData->cbmu;
                        $orderDetail->length    = $goodsData->g_length;
                        $orderDetail->height    = $goodsData->g_height;
                        $orderDetail->weight    = $goodsData->g_width;
                        if(isset($goodsData->gw)) {
                            $orderDetail->gw       = $goodsData->gw * $item->pkg_num;
                        }
                        else {
                            $orderDetail->gw = 0;
                        }
                        if(isset($goodsData->cbm)) {
                            $orderDetail->cbm       = $goodsData->cbm * $item->pkg_num;
                        }
                        else {
                            $orderDetail->cbm = 0;
                        }
                        if(isset($goodsData->price)) {
                            $orderDetail->price     = $goodsData->price;
                        }else{
                            $orderDetail->price = 0;
                        }
                    }
                    else {
                        $orderDetail->goods_nm = $item->goods_nm;
                        $orderDetail->gw       = $item->total_gw;
                        $orderDetail->pkg_unit = $item->pkg_unit;
                    }
                    if(!empty($item->goods_nm)){
                        $orderDetail->goods_nm = $item->goods_nm;
                    }
                    if(!empty($item->pkg_unit)){
                        $orderDetail->pkg_unit = $item->pkg_unit;
                    }
                    if(!empty($item->goods_no2)){
                        $orderDetail->goods_no2 = $item->goods_no2;
                    }
                    if(!empty($item->gwu)){
                        $orderDetail->gwu = $item->gwu;
                    }
                    if(!empty($item->cbm)){
                        $orderDetail->cbm = $item->cbm;
                    }
                    if(!empty($item->cbmu)){
                        $orderDetail->cbmu = $item->cbmu;
                    }
                    if(!empty($item->length)){
                        $orderDetail->length = $item->length;
                    }
                    if(!empty($item->height)){
                        $orderDetail->height = $item->height;
                    }
                    if(!empty($item->weight)){
                        $orderDetail->weight = $item->weight;
                    }
                    $orderDetail->created_by = $user->email;
                    $orderDetail->excel_name = $item->excelname;
                    // $excelname =DB::table('tmp_mod_order')
                    // ->where('created_by', $user->email)
                    // ->where('g_key',$user->g_key)->first();
                    // if(isset($excelname)){
                    //     $orderDetail->excel_name = $item->excelname;
                    // }
                    $orderDetail->save();

                    $this->updateModOrderGw($orderDetail->ord_id, $user->g_key, $user->c_key);
                    $excelname = DB::table('tmp_mod_order')
                    ->where('created_by', $user->email)
                    ->where('g_key',$user->g_key)->first();
                    DB::table('tmp_mod_order')
                    ->where('id', $item->id)
                    ->delete();
                }

                if(!empty($detailOrdId)) {
                    //$this->updateModOrderGw($detailOrdId, $user->g_key, $user->c_key);
                }
                DB::table('sys_excelimport_log')->insert([
                    'excelname' =>$excelname->excelname,
                    'g_key' =>$user->g_key,
                    'c_key' =>$user->c_key,
                    's_key' =>$user->s_key,
                    'd_key' =>$user->d_key,
                    'created_by' => $user->email,
                    'created_at' => \Carbon::now()
                ]);
                DB::table('tmp_mod_order')
                        ->where('created_by', $user->email)
                        ->where('g_key',$user->g_key)
                        ->delete();
            }
            
        }catch (\Exception $e) {
            \Log::info("excel save to order error");
            \Log::info($e->getline());
            \Log::info($e->getMessage());
            return ["msg"=>"error", "errorLog"=>$e->getMessage(), "line"=>$e->getline()];
        }
        
        return ["msg"=>"success"];

    }
    
    public function getOrderExcel()
    {
        //PDF file is stored under project/public/download/info.pdf
        try{
        $file= storage_path(). "/excel/OrderImportSample.xlsx";
    
        $headers = array(
                    'Content-Type: application/xlsx',
                );
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        return Response::download($file, 'ExampleForCar.xlsx', $headers);
    }

    private function updateModOrderGw($ordId, $g_key, $c_key) {
        $detailData = DB::connection('mysql::write')->table('mod_order_detail')
                        ->select(
                            DB::raw('SUM(gw) as total_gw'),
                            DB::raw('SUM(pkg_num) as pkg_num'),
                            DB::raw('SUM(cbm) as total_cbm'),
                            DB::raw('sum(price*pkg_num) as total_price'),
                            'pkg_unit'
                        )
                        ->where('ord_id', $ordId)
                        ->where('g_key', $g_key)
                        ->where('c_key', $c_key)
                        ->first();

        if(isset($detailData)) {
            OrderMgmtModel::where('id', $ordId)
            ->where('g_key', $g_key)
            ->where('c_key', $c_key)
            ->update([
                'total_gw'  => $detailData->total_gw,
                'pkg_num'   => $detailData->pkg_num,
                'total_cbm' => $detailData->total_cbm,
                'amt'       => $detailData->total_price,
                'pkg_unit'  => $detailData->pkg_unit,
            ]);
        }

        return;
    }

    public function clearTmpOrder() {
        $user = Auth::user();

        DB::table('tmp_mod_order')
            ->where('created_by', $user->email)
            ->where('g_key', $user->g_key)
            ->delete();

        return response()->json(['msg' => 'success']);
    }

    public function importMed()
	{
        $user = Auth::user();
        DB::table('tmp_medical_terms')
                ->delete();
        $obj = array(
                "案號"   => "no",
                "流水號"     => "uid",
                "結案指標"       => "resultflag",
                "病患姓名"      => "name",
                "疾病代碼1"    => "code1",
                "疾病代碼2" => "code2",
                "疾病代碼3"       => "code3",
                "中文診斷"      => "chresult",
                "醫令代碼"    => "medical_cd",
                "醫令項目說明"     => "medical_descp",
                "爭審理由"     => "arrgu",
                "醫令審議結果"    => "result",
                "健保局初核意見"     => "Initialnuclear",
                "健保局複核意見"     => "Reviewlnuclear",
                "審查醫師意見"     => "Physicianopinion"
                );
        try{
            if(Input::hasFile('import_file')){
                $path = Input::file('import_file')->getRealPath();
                $data = Excel::load($path, function($reader) {
                    $reader->setDateFormat('Y-m-d');
                })->get();

                $i = 0;
                $dataFirst = $data[0];
                $header = $dataFirst->first()->keys()->toArray();
                $insert = array();
                if(!empty($dataFirst)){
                    foreach ($dataFirst as $key => $value) {
                        //$obj[$header[$i]] = $value->$header[$i];
                        $result = array();
                        for($i=0;$i<count($value);$i++) {
                            if(isset($obj[$header[$i]])) {
                                $o = array(
                                    $obj[$header[$i]] => $value[$header[$i]]
                                );
                                $result = array_merge($result, $o);
                            }
                            
                        }
                        // $result['created_by'] = $user->email;
                        // $result['created_at'] = date("Y-m-d H: i: s");
                        array_push($insert,  $result);
                        // dd($insert);
                        //$insert[] = ['title' => $value->title, 'description' => $value->description];
                        if(!empty($insert) ){
                            DB::table('tmp_medical_terms')->insert($insert);
                            $insert = [];
                            // dd($insert);
                        }
                    }

                    if(!empty($insert)){
                        DB::table('tmp_medical_terms')->insert($insert);
                        // dd($insert);
                    }
                }
            }
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        return ["msg"=>"success132", "data"=>$insert];
    }

    public function getMed(){
        $now = date('Ymd');
        try{
        Excel::create('醫療-' . $now, function ($excel) use ( $now) {
            $idsArray = "";

            $excel->sheet('醫療-' . $now, function ($sheet) use ($idsArray) {
                $user = Auth::user();
                $orderData = DB::table('tmp_medical_terms')
                    ->get();
                $sheet->row(1, array(
                    '合併','案號', '流水號', '結案指標', '病患姓名', '疾病代碼1', '疾病代碼2', '疾病代碼3', '中文診斷', '醫令代碼', '醫令項目說明', '爭審理由', '醫令審議結果', '健保局初核意見', '健保局複核意見', '審查醫師意見',
                ));
                $sheet->setFontFamily('標楷體');
                $sheet->setStyle(array(
                    'font' => array(
                        'size'      =>  14
                    )
                ));
                foreach ($orderData as $key => $row) {
                    $row = $this->newdata($row);
                    $sheet->row(($key + 2), array(
                        $row->resultstr,$row->no, $row->uid, $row->resultflag, $row->name,$row->code1,$row->code2,$row->code3,$row->chresult,$row->medical_cd,$row->medical_descp,$row->arrgu,$row->result,$row->Initialnuclear,$row->Reviewlnuclear,$row->Physicianopinion,
                    ));
                }

            });

        })->export('xls');
        
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage(), "line"=>$e->getLine()];
        }
    }
    
    public function newdata($row){
        $codedata1 = DB::table('medical_terms')->where('medical_code', $row->code1)->first();
        $codedata2 = DB::table('medical_terms')->where('medical_code', $row->code2)->first();
        $codedata3 = DB::table('medical_terms')->where('medical_code', $row->code3)->first();
        $codedata1_cnm= "";
        $codedata1_enm= "";
        $codedata2_cnm= "";
        $codedata2_enm= "";
        $codedata3_cnm= "";
        $codedata3_enm= "";
        if($codedata1!=null){
            $codedata1_cnm = $codedata1->medical_cnm;
            $codedata1_enm = $codedata1->medical_enm;
        }
        if($codedata2!=null){
            $codedata2_cnm = " ".$codedata2->medical_cnm;
            $codedata2_enm = "、2.".$codedata2->medical_enm;
        }
        if($codedata3!=null){
            $codedata3_cnm =  " ".$codedata3->medical_cnm;
            $codedata3_enm = "、3.".$codedata3->medical_enm;
        }


        $resultstr = "";
        try{
            if($row->result =="1"){
                $resultstr = $row->name."案，查卷附資料，健保署初、複核意見為「".$row->Initialnuclear."」、「".$row->Reviewlnuclear."」，申請理由略稱：「".$row->arrgu."」，依病歷紀錄，病人診斷為「1.".$codedata1_enm." ".$codedata1_cnm.$codedata2_enm.$codedata2_cnm.$codedata3_enm.$codedata3_cnm."」等，依病情記載：「」，".$row->Physicianopinion."，施行開立處方系爭「".$row->medical_descp."」藥物檢驗檢查項目，符合病情所需，尚屬合理，所請同意給付，其金額由保險人依規定核算後補付。";
            }else if ($row->result == "2"){
                $resultstr = "一、	相關規定
                行為時全民健康保險醫療費用申報與核付及醫療服務審查辦法第5條第1項規定：「保險醫事服務機構所申報之醫療費用，未有全民健康保險醫事服務機構特約及管理辦法規定應扣減醫療費用十倍金額、停止特約、或終止特約者，自保險人受理申報醫療費用案件、申復案件之日起逾二年時，保險人不得追扣。」。
                二、	依前揭醫療服務審查辦法規定，醫療服務點數追扣期間係以保險人受理申報案件2年內為之，查渠等2案申請人於106年2月23日申報106年1月之醫療費用，保險人受理日期為何？卷無資料可稽，則健保署於108年2月26日以健保北字第1084501910號函追扣申請人上開期間之醫療費用，是否已逾受理申報案件2年？申請人有無前揭行為時全民健康保險醫事服務機構特約及管理辦法規定應扣減醫療費用十倍金額、停止特約、或終止特約之情事？因攸關本件追扣期間之計算，有查明之必要。
                三、	綜上，爰將原核定撤銷，由原核定機關查明後另為適法之核定。
                ";
            }else if ($row->result == "3"){
                $resultstr = $row->name."案，查卷附資料，健保署初、複核意見以「".$row->Initialnuclear."」、「".$row->Reviewlnuclear."」為由，不予給付，申請理由略稱：「".$row->arrgu."」，依病歷紀錄，病人診斷為「1.".$codedata1_enm." ".$codedata1_cnm.$codedata2_enm.$codedata2_cnm.$codedata3_enm.$codedata3_cnm."」等，依病情記載及病人之CCr值，".$row->Physicianopinion."，部分所請尚屬合理，同意給付3次，其金額由保險人依規定核算後補付。其餘部分，依所附病歷資料，宜再評估停止HD之可能性，無法顯示需給付所請費用之正當理由，原核定並無不合，應予維持。";
            }   
            else if ($row->result =="6"){
                $resultstr =$row->name."案，查卷附資料，健保署初、複核意見為「".$row->Initialnuclear."」、「".$row->Reviewlnuclear."」，改以「()」項目給付，申請理由略稱：「".$row->arrgu."」，依病歷紀錄，病人診斷為「1.".$codedata1_enm." ".$codedata1_cnm.$codedata2_enm.$codedata2_cnm.$codedata3_enm.$codedata3_cnm."」等，依病情記載，".$row->Physicianopinion."，同意健保署意見，原給付項目及數量，已足敷診療所需，不足以支持系爭「".$row->medical_descp."」項目之必要性，無法顯示需給付所請費用之正當理由，原核定並無不合，應予維持。";
            }
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getline()];
        }
        $row->resultstr = $resultstr;
        return $row ;
    }
    
}
