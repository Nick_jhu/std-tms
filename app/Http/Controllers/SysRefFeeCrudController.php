<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Excel;
use Response;
use Illuminate\Support\Facades\Input;
use App\Models\SysRefFeeModel;
use App\Models\CarLoadModel;
use App\Models\OrderPackModel;
use App\Models\BaseModel;
use App\Models\CommonModel;
use App\Models\TransRecordModel;

use App\Http\Requests\DlvCrudRequest as StoreRequest;
use App\Http\Requests\DlvCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store as Session;

class SysRefFeeCrudController extends CrudController
{
    
    public function setup() {
        $this->crud->setModel("App\Models\SysRefFeeModel");
        $this->crud->setEntityNameStrings("專車費用設定", "專車費用設定");
        $this->crud->setRoute(config('backpack.base.route_prefix').'/sysRefFeeCar');
    
        $this->crud->setColumns(['name']);
        
        $this->crud->setCreateView('tranPlan.edit');
        $this->crud->setEditView('tranPlan.edit');
        $this->crud->setListView('tranPlan.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'car_type_nm',
            'type' => 'lookup',
            'title' => '車型查詢',
            'info1' => Crypt::encrypt('bscode'), //table
            'info2' => Crypt::encrypt("cd,cd_descp"), //column
            'info3' => Crypt::encrypt("cd_type='CARTYPE'"), //condition
            'info4' => "cd=car_type_cd;cd_descp=car_type_nm" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'car_type_cd',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 'temperate',
            'type' => 'select',
            'options' => DB::table('bscode')
                            ->select('cd as code', 'cd_descp as descp')
                            ->where('cd_type', 'TEMPERATE')
                            ->orderBy('value1', 'asc')
                            ->where('g_key', Auth::user()->g_key)
                            ->where('c_key', Auth::user()->c_key)
                            ->get()
        ]);
        // $this->crud->addField([
        //     'name' => 'start_date',
        //     'type' => 'date_picker'
        // ]);
        // $this->crud->addField([
        //     'name' => 'end_date',
        //     'type' => 'date_picker'
        // ]);
        $this->crud->addField([
            'name' => 'type',
            'type' => 'hidden'
        ]);

        $this->crud->addField([
            'name' => 'distance',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'regular_fee',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'fee_from',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'fee_to',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'fee',
            'type' => 'text'
        ]);

        
        $this->crud->addField([
            'name' => 'remark',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);
        
        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 's_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'text'
        ]);
    }

    public function sysRefFeeCar() {
        $title = trans('sysRefFee.titleName');
        echo "<title>$title</title>";
        $user = Auth::user();
        $TEMPERATEData = DB::table('bscode')
        ->select('cd as code', 'cd_descp as descp')
        ->where('cd_type', 'TEMPERATE')
        ->orderBy('value1', 'asc')
        ->where('g_key', Auth::user()->g_key)
        ->where('c_key', Auth::user()->c_key)
        ->get();

        $viewData = array(
            'TEMPERATEData'        => $TEMPERATEData
        );
        try{
            if(Auth::user()->hasPermissionTo('Quote'))
            {
                return view('sysRefFee.sysRefFeeCar')->with('viewData', $viewData);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function sysRefFeeNonCar() {
        $title = trans('sysRefFee.titleNameNonCar');
        echo "<title>$title</title>";
        return view('sysRefFee.sysRefFeeNonCar');
    }
    public function sysRefFeeDelivery() {
        $title = trans('sysRefFee.titleNameDelivery');
        echo "<title>$title</title>";
        return view('sysRefFee.sysRefFeeDelivery');
    }

    public function CarGet() 
    {
        $user = Auth::user();

        $carDetail = [];        
        $this_query = DB::table('sys_ref_fee');                        
        $this_query->where('type', "C");
        $this_query->where('g_key', $user->g_key);
        $this_query->where('c_key', $user->c_key);
        $carDetail = $this_query->get();
        
        
        $data[] = array(
            'Rows' => $carDetail,
        );

        return response()->json($data);
    }

    public function CarStore(Request $request)
    {
        $validator = $this->carValidator($request);        
        $user = Auth::user();
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = new SysRefFeeModel;
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key] = request($key);
                $sysRefFee["g_key"] =  $user->g_key;
                $sysRefFee["c_key"]    = $user->c_key;
                $sysRefFee["s_key"]    = $user->s_key;
                $sysRefFee["d_key"]    = $user->d_key;
                $sysRefFee["created_by"]    = $user->email;
                $sysRefFee["type"] = "C";
            }
            //dd($orderDetail);
            $sysRefFee->save();
        }

        return ["msg"=>"success", "data"=>DB::connection('mysql::write')->table('sys_ref_fee')->where('id', $sysRefFee->id)->get()];
    }

    public function CarUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->carValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = SysRefFeeModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key]         = request($key);
                $sysRefFee["type"]       = "C";
                $sysRefFee["updated_by"] = $user->email;
            }
            $sysRefFee->save();
        }


        return ["msg"=>"success", "data"=>$sysRefFee->where('id', $request->id)->get()];
    }


    public function CarDel($id)
    {
        $sysRefFee = SysRefFeeModel::find($id);
        $sysRefFee->delete();
       

        return ["msg"=>"success"];
    }

    public function carValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }

    public function NonCarGet() 
    {
        $user = Auth::user();
        $nonCarDetail = [];        
        $this_query = DB::table('sys_ref_fee');                        
        $this_query->where('type', "N");
        $this_query->where('g_key', $user->g_key);
        $this_query->where('c_key', $user->c_key);
        $nonCarDetail = $this_query->get();
        
        
        $data[] = array(
            'Rows' => $nonCarDetail,
        );

        return response()->json($data);
    }

    public function NonCarStore(Request $request)
    {
        $validator = $this->nonCarValidator($request);        
        $user = Auth::user();
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = new SysRefFeeModel;
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key]         = request($key);
                $sysRefFee["g_key"]      = $user->g_key;
                $sysRefFee["c_key"]      = $user->c_key;
                $sysRefFee["s_key"]      = $user->s_key;
                $sysRefFee["d_key"]      = $user->d_key;
                $sysRefFee["created_by"] = $user->email;
                $sysRefFee["type"]       = "N";
            }
            //dd($orderDetail);
            $sysRefFee->save();
        }

        return ["msg"=>"success", "data"=>DB::connection('mysql::write')->table('sys_ref_fee')->where('id', $sysRefFee->id)->get()];
    }

    public function NonCarUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->nonCarValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = SysRefFeeModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key] = request($key);
                $sysRefFee["updated_by"]    = $user->email;
                $sysRefFee["type"]       = "N";
            }
            $sysRefFee->save();
        }


        return ["msg"=>"success", "data"=>$sysRefFee->where('id', $request->id)->get()];
    }


    public function NonCarDel($id)
    {
        $sysRefFee = SysRefFeeModel::find($id);
        $sysRefFee->delete();
       

        return ["msg"=>"success"];
    }

    public function NonCarNoDiscountGet() 
    {
        $user = Auth::user();
        $nonCarDetail = [];        
        $this_query = DB::table('sys_ref_fee');                        
        $this_query->where('type', "N02");
        $this_query->where('g_key', $user->g_key);
        $this_query->where('c_key', $user->c_key);
        $nonCarDetail = $this_query->get();
        
        
        $data[] = array(
            'Rows' => $nonCarDetail,
        );

        return response()->json($data);
    }

    public function NonCarNoDiscountStore(Request $request)
    {
        $validator = $this->nonCarValidator($request);        
        $user = Auth::user();
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = new SysRefFeeModel;
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key]         = request($key);
                $sysRefFee["g_key"]      = $user->g_key;
                $sysRefFee["c_key"]      = $user->c_key;
                $sysRefFee["s_key"]      = $user->s_key;
                $sysRefFee["d_key"]      = $user->d_key;
                $sysRefFee["created_by"] = $user->email;
                $sysRefFee["type"]       = "N02";
            }
            //dd($orderDetail);
            $sysRefFee->save();
        }

        return ["msg"=>"success", "data"=>DB::connection('mysql::write')->table('sys_ref_fee')->where('id', $sysRefFee->id)->get()];
    }

    public function NonCarNoDiscountUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->nonCarValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = SysRefFeeModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key] = request($key);
                $sysRefFee["updated_by"]    = $user->email;
                $sysRefFee["type"]       = "N02";
            }
            $sysRefFee->save();
        }


        return ["msg"=>"success", "data"=>$sysRefFee->where('id', $request->id)->get()];
    }


    public function NonCarNoDiscountDel($id)
    {
        $sysRefFee = SysRefFeeModel::find($id);
        $sysRefFee->delete();
       

        return ["msg"=>"success"];
    }

    public function nonCarValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }
    public function GwGet() 
    {
        $user = Auth::user();   
        $deliveryDetail = DB::connection('mysql::write')->table('sys_ref_fee')
        ->where('type', "Gw")
        ->where('g_key', $user->g_key)
        ->where('c_key', $user->c_key)
        ->get();
        
        $data[] = array(
            'Rows' => $deliveryDetail,
        );

        return response()->json($data);
    }

    public function GwStore(Request $request)
    {
        $validator = $this->deliveryValidator($request);        
        $user = Auth::user();
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = new SysRefFeeModel;
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key]         = request($key);
                $sysRefFee["g_key"]      = $user->g_key;
                $sysRefFee["c_key"]      = $user->c_key;
                $sysRefFee["s_key"]      = $user->s_key;
                $sysRefFee["d_key"]      = $user->d_key;
                $sysRefFee["created_by"] = $user->email;
                $sysRefFee["type"]       = "Gw";
            }
            //dd($orderDetail);
            $sysRefFee->save();
        }
        
        return ["msg"=>"success", "data"=>DB::connection('mysql::write')->table('sys_ref_fee')->where('id', $sysRefFee->id)->get()];
    }

    public function GwUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->deliveryValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = SysRefFeeModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key]         = request($key);
                $sysRefFee["type"]       = "Gw";
                $sysRefFee["updated_by"] = $user->email;
            }
            $sysRefFee->save();
        }


        return ["msg"=>"success", "data"=>$sysRefFee->where('id', $request->id)->get(), "data1"=>$request->all()];
    }


    public function GwDel($id)
    {
        $sysRefFee = SysRefFeeModel::find($id);
        $sysRefFee->delete();
       

        return ["msg"=>"success"];
    }
    public function CbmGet() 
    {
        $user = Auth::user();
        $deliveryDetail = [];        
        $this_query = DB::table('sys_ref_fee');                        
        $this_query->where('type', "Cbm");
        $this_query->where('g_key', $user->g_key);
        $this_query->where('c_key', $user->c_key);
        $deliveryDetail = $this_query->get();
        
        
        $data[] = array(
            'Rows' => $deliveryDetail,
        );

        return response()->json($data);
    }

    public function CbmStore(Request $request)
    {
        $validator = $this->deliveryValidator($request);        
        $user = Auth::user();
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = new SysRefFeeModel;
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key]         = request($key);
                $sysRefFee["g_key"]      = $user->g_key;
                $sysRefFee["c_key"]      = $user->c_key;
                $sysRefFee["s_key"]      = $user->s_key;
                $sysRefFee["d_key"]      = $user->d_key;
                $sysRefFee["created_by"] = $user->email;
                $sysRefFee["type"]       = "Cbm";
            }
            //dd($orderDetail);
            $sysRefFee->save();
        }

        return ["msg"=>"success", "data"=>DB::connection('mysql::write')->table('sys_ref_fee')->where('id', $sysRefFee->id)->get()];
    }

    public function CbmUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->deliveryValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = SysRefFeeModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key]         = request($key);
                $sysRefFee["type"]       = "Cbm";
                $sysRefFee["updated_by"] = $user->email;
            }
            $sysRefFee->save();
        }


        return ["msg"=>"success", "data"=>$sysRefFee->where('id', $request->id)->get(), "data1"=>$request->all()];
    }


    public function CbmDel($id)
    {
        $sysRefFee = SysRefFeeModel::find($id);
        $sysRefFee->delete();
       

        return ["msg"=>"success"];
    }

    public function PieceGet() 
    {
        $user = Auth::user();
        $deliveryDetail = [];        
        $this_query = DB::table('sys_ref_fee');                        
        $this_query->where('type', "Piece");
        $this_query->where('g_key', $user->g_key);
        $this_query->where('c_key', $user->c_key);
        $deliveryDetail = $this_query->get();
        
        
        $data[] = array(
            'Rows' => $deliveryDetail,
        );

        return response()->json($data);
    }

    public function PieceStore(Request $request)
    {
        $validator = $this->deliveryValidator($request);        
        $user = Auth::user();
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = new SysRefFeeModel;
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key]         = request($key);
                $sysRefFee["g_key"]      = $user->g_key;
                $sysRefFee["c_key"]      = $user->c_key;
                $sysRefFee["s_key"]      = $user->s_key;
                $sysRefFee["d_key"]      = $user->d_key;
                $sysRefFee["created_by"] = $user->email;
                $sysRefFee["type"]       = "Piece";
            }
            //dd($orderDetail);
            $sysRefFee->save();
        }

        return ["msg"=>"success", "data"=>DB::connection('mysql::write')->table('sys_ref_fee')->where('id', $sysRefFee->id)->get()];
    }

    public function PieceUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->deliveryValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = SysRefFeeModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key]         = request($key);
                $sysRefFee["type"]       = "Piece";
                $sysRefFee["updated_by"] = $user->email;
                $sysRefFee["start_date"] = $request->start_date;
                $sysRefFee["end_date"] = $request->end_date;
                $sysRefFee["temperate"] = $request->temperate;
            }
            $sysRefFee->save();
        }


        return ["msg"=>"success", "data"=>$sysRefFee->where('id', $request->id)->get(), "data1"=>$request->all()];
    }


    public function PieceDel($id)
    {
        $sysRefFee = SysRefFeeModel::find($id);
        $sysRefFee->delete();
       

        return ["msg"=>"success"];
    }

	public function DeliveryGet() 
    {
        $user = Auth::user();
        $deliveryDetail = [];        
        $this_query = DB::table('sys_ref_fee');                        
        $this_query->where('type', "N01");
        $this_query->where('g_key', $user->g_key);
        $this_query->where('c_key', $user->c_key);
        $deliveryDetail = $this_query->get();
        
        
        $data[] = array(
            'Rows' => $deliveryDetail,
        );

        return response()->json($data);
    }

    public function DeliveryStore(Request $request)
    {
        $validator = $this->deliveryValidator($request);        
        $user = Auth::user();
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = new SysRefFeeModel;
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key]         = request($key);
                $sysRefFee["g_key"]      = $user->g_key;
                $sysRefFee["c_key"]      = $user->c_key;
                $sysRefFee["s_key"]      = $user->s_key;
                $sysRefFee["d_key"]      = $user->d_key;
                $sysRefFee["created_by"] = $user->email;
                $sysRefFee["type"]       = "N01";
            }
            //dd($orderDetail);
            $sysRefFee->save();
        }

        return ["msg"=>"success", "data"=>DB::connection('mysql::write')->table('sys_ref_fee')->where('id', $sysRefFee->id)->get()];
    }

    public function DeliveryUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->deliveryValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $sysRefFee = SysRefFeeModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $sysRefFee[$key]         = request($key);
                $sysRefFee["type"]       = "N01";
                $sysRefFee["updated_by"] = $user->email;
            }
            $sysRefFee->save();
        }


        return ["msg"=>"success", "data"=>$sysRefFee->where('id', $request->id)->get()];
    }


    public function DeliveryDel($id)
    {
        $sysRefFee = SysRefFeeModel::find($id);
        $sysRefFee->delete();
       

        return ["msg"=>"success"];
    }

    public function deliveryValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }

    public function importCarExcel()
	{
        
        $user = Auth::user();
      
        $obj = array(
                     "類別"=>"type",
                     "車型"=>"car_type_nm",
                     "最大距離"=>"distance",
                     "固定費用"=>"regular_fee",
                     "from"=>"fee_from",
                     "to"=>"fee_to",
                     "費用"=>"fee",
                     "備註"=>"remark");
    try{
		if(Input::hasFile('import_file')){
			$path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {
                //$reader->setDateFormat('Y-m-d');
            })->get();
            $i = 0;           
            $dataFirst = $data[0];            
            $header = $dataFirst->first()->keys()->toArray();
            $insert = array();
            
			if(!empty($dataFirst) && $dataFirst->count()){
				foreach ($dataFirst as $key => $value) {
                    //$obj[$header[$i]] = $value->$header[$i];
                  $result = array();
                  for($i=0;$i<count($value);$i++){
                    $o = array(
                        $obj[$header[$i]] => $value[$header[$i]]
                    );
                    $result = array_merge($result, $o);
                }
                    if(!empty($result['car_type_nm']))
                    {
                        switch($result['car_type_nm']){
                            case "1.75噸小型貨車":
                                $result['car_type_cd'] = "A";
                                break;
                            case "1.9噸冷藏專車":
                                $result['car_type_cd'] = "B";
                                break;
                            case "3.5噸中型貨車":
                                $result['car_type_cd'] = "C";
                                break;
                        }
                    }
                    $result['g_key'] = $user->g_key;
                    $result['c_key'] = $user->c_key;
                    $result['s_key'] = $user->s_key;
                    $result['d_key'] = $user->d_key;
                    $result['created_by'] = $user->email;
                    $result['created_at'] = date("Y-m-d H:i:s");
                    $result['type'] = "C";
                    if(!empty($result['car_type_nm'])){
                    array_push($insert,  $result);
                    }
                
					//$insert[] = ['title' => $value->title, 'description' => $value->description];
				}
				if(!empty($insert)){
					DB::table('sys_ref_fee')->insert($insert);
					//dd($insert);
				    }
			    }
            }
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        //return back();
        return ["msg"=>"success", "data"=>$insert];
    }
    
    public function importNonCarExcel()
	{
        
        $user = Auth::user();
      
        $obj = array(
                     "類別"=>"type",
                     "車型"=>"car_type_nm",
                     "計費單位"=>"fee_unit",
                     "from"=>"fee_from",
                     "to"=>"fee_to",
                     "費用"=>"fee",
                     "備註"=>"remark");
    try{
		if(Input::hasFile('import_file')){
			$path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {
                //$reader->setDateFormat('Y-m-d');
            })->get();
            $i = 0;            
            $insert = array();
            $dataFirst = $data[0];
            $header = $dataFirst->first()->keys()->toArray();
			if(!empty($dataFirst) && $dataFirst->count()){
				foreach ($dataFirst as $key => $value) {
                    //$obj[$header[$i]] = $value->$header[$i];
                  $result = array();
                  for($i=0;$i<count($value);$i++){
                    $o = array(
                        $obj[$header[$i]] => $value[$header[$i]]
                    );
                    $result = array_merge($result, $o);
                }
                    if(!empty($result['car_type_nm']))
                    {
                        switch($result['car_type_nm']){
                            case "1.75噸小型貨車":
                                $result['car_type_cd'] = "A";
                                break;
                            case "1.9噸冷藏專車":
                                $result['car_type_cd'] = "B";
                                break;
                            case "3.5噸中型貨車":
                                $result['car_type_cd'] = "C";
                                break;
                        }
                    }
                    $result['g_key'] = $user->g_key;
                    $result['c_key'] = $user->c_key;
                    $result['s_key'] = $user->s_key;
                    $result['d_key'] = $user->d_key;
                    $result['created_by'] = $user->email;
                    $result['created_at'] = date("Y-m-d H:i:s");
                    $result['type'] = "N";
                    if(!empty($result['car_type_nm'])){
                    array_push($insert,  $result);
                    }
                
					//$insert[] = ['title' => $value->title, 'description' => $value->description];
				}
				if(!empty($insert)){
					DB::table('sys_ref_fee')->insert($insert);
					//dd($insert);
				    }
			    }
            }
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        //return back();
        return ["msg"=>"success", "data"=>$insert];
	}

    public function importDeliveryExcel()
	{
        
        $user = Auth::user();
      
        $obj = array(
                     "類別"=>"type",
                     "運送類型"=>"trans_type_nm",
                     "運算子"=>"fee_op",
                     "重量公斤"=>"fee_weight",
                     "費用"=>"fee",
                     "備註"=>"remark");
    try{
		if(Input::hasFile('import_file')){
			$path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {
                //$reader->setDateFormat('Y-m-d');
            })->get();
            $i = 0;
            $dataFirst = $data[0];
            $header = $dataFirst->first()->keys()->toArray();
            $insert = array();
            
			if(!empty($dataFirst) && $dataFirst->count()){
				foreach ($dataFirst as $key => $value) {
                    //$obj[$header[$i]] = $value->$header[$i];
                  $result = array();
                  for($i=0;$i<count($value);$i++){
                    $o = array(
                        $obj[$header[$i]] => $value[$header[$i]]
                    );
                    $result = array_merge($result, $o);
                }
                    if(!empty($result['trans_type_nm']))
                    {
                        switch($result['trans_type_nm']){
                            case "文件類--機車快遞":
                                $result['trans_type_cd'] = "A";
                                break;
                            case "500g(25cmⅩ35cm)以下":
                                $result['trans_type_cd'] = "B";
                                break;
                            case "1公斤(30cmⅩ40cm)以下":
                                $result['trans_type_cd'] = "C";
                                break;
                            case "3公斤(35cmⅩ5cm)以下":
                                $result['trans_type_cd'] = "D";
                                break;
                            case "5公斤(箱40cmⅩ35cmⅩ25cm)以下":
                                $result['trans_type_cd'] = "E";
                                break;
                        }
                    }
                    $result['g_key'] = $user->g_key;
                    $result['c_key'] = $user->c_key;
                    $result['s_key'] = $user->s_key;
                    $result['d_key'] = $user->d_key;
                    $result['created_by'] = $user->email;
                    $result['created_at'] = date("Y-m-d H:i:s");
                    $result['type'] = "D";
                    if(!empty($result['trans_type_nm'])){
                    array_push($insert,  $result);
                    }
                
					//$insert[] = ['title' => $value->title, 'description' => $value->description];
				}
				if(!empty($insert)){
					DB::table('sys_ref_fee')->insert($insert);
					//dd($insert);
				    }
			    }
            }
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        //return back();
        return ["msg"=>"success", "data"=>$insert];
    }
    
    public function getCarDownload()
    {
        //PDF file is stored under project/public/download/info.pdf
        try{
        $file= storage_path(). "/excel/專車費用設定範本.xlsx";
    
        $headers = array(
                  'Content-Type: application/xlsx',
                );
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        return Response::download($file, 'ExampleForCar.xlsx', $headers);
    }

    public function getNonCarDownload()
    {
        //PDF file is stored under project/public/download/info.pdf
        try{
        $file= storage_path(). "/excel/非專車費用設定範本.xlsx";
    
        $headers = array(
                  'Content-Type: application/xlsx',
                );
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        return Response::download($file, 'ExampleForNonCar.xlsx', $headers);
    }

    public function getDeliveryDownload()
    {
        //PDF file is stored under project/public/download/info.pdf
        try{
        $file= storage_path(). "/excel/快遞費用設定範本.xlsx";
    
        $headers = array(
                  'Content-Type: application/xlsx',
                );
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        return Response::download($file, 'ExampleForDelivery.xlsx', $headers);
    }
}
