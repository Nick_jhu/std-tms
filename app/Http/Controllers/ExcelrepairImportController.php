<?php

namespace App\Http\Controllers;
use Response;
use Excel;
use Carbon\Carbon;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Models\CommonModel;
use App\Models\OrderDetailModel;
use App\Models\TmpModOrderModel;
use App\Models\TrackingModel;
use App\Models\OrderMgmtModel;
use App\Models\BaseModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\GoogleMapModel;
use DateTime;
class ExcelrepairImportController extends Controller
{
    public function repairimport()
	{
        $title = trans('excel.titleName');
        echo "<title>$title</title>";
        try{
            if(Auth::user()->hasPermissionTo('repairImport'))
            {
                return view('excelImport/Importrepair');
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
	}
	public function downloadExcel($type)
	{
		$data = Item::get()->toArray();
		return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download($type);
	}
	public function importExcel()
	{
        $user = Auth::user();

        DB::table('tmp_mod_order')
                ->where('created_by', $user->email)
                ->where('g_key',$user->g_key)
                ->delete();

        
                

        $obj = array(
                "配送客戶名稱"   => "dlv_cust_nm",
                "到貨日期"     => "etd",
                //"司機"     => "driver",
                "備註"       => "remark",
                "訂單號"      => "ord_no",
                "配送聯絡人"    => "dlv_attn",
                "配送e_mail" => "dlv_email",
                "件數"       => "pkg_num",
                "總材積"      => "total_cbm",
                "總重量"    => "total_gw",
                "配送地址"     => "dlv_addr",
                "配送電話"     => "dlv_tel",
                "提貨聯絡人"    => "pick_attn",
                "提貨地址"     => "pick_addr",
                "提貨電話"     => "pick_tel",
                "商品名稱"     => "goods_nm",
                "商品編號1"      => "goods_no",
                "商品編號2"      => "goods_no2",
                "提貨e_mail" => "pick_email",
                "車型"       => "car_type",
                "溫層"       => "temperate",
                "單位"       => "pkg_unit",
                "運輸類型"     => "trs_mode",
                "提貨客戶代碼"   => "pick_cust_no",
                "提貨客戶名稱"   => "pick_cust_nm",
                "提貨郵地區號"   => "pick_zip",
                "提貨城市名稱"   => "pick_city_nm",
                "提貨區域名稱"   => "pick_area_nm",
                "配送客戶代碼"   => "dlv_cust_no",
                "運輸公司名稱"   => "truck_cmp_nm",
                "運輸公司代碼"   => "truck_cmp_no",
                "配送郵地區號"   => "dlv_zip",
                "配送城市名稱"   => "dlv_city_nm",
                "配送區域名稱"   => "dlv_area_nm",
                "訂單類型代碼"     => "ord_type_nm",
                "訂單類型"     => "ord_type_nm",
                "貨主代碼"     => "owner_cd",
                "貨主名稱"     => "owner_nm",
                "配送備註"  => "dlv_remark",
                "提貨備註" => "pick_remark",
                "發送mail貨主" => "owner_send_mail",
                "發送mail提貨" => "pick_send_mail",
                "發送mail配送" => "dlv_send_mail",
                "站別" => "s_key",
                "費用備註" => "amt_remark",
                "客戶訂單編號" => "cust_ord_no",
                "代收款" => "collectamt",
                "修正費用" => "cust_amt",
                "倉庫訂單號碼" => "wms_order_no"
                );
        try{
            if(Input::hasFile('import_file')){
                $path = Input::file('import_file')->getRealPath();
                // $data = Excel::load($path, function($reader) {
                //     $reader->setDateFormat('Y-m-d');
                // })->get();
                // dd(request()->file('import_file'));
                $data = Excel::toArray(new UsersImport, request()->file('import_file'));
                $i = 0;
                $dataFirst = $data[0];
                $header = $dataFirst[0];             
                $insert = array();
                if(!empty($dataFirst)){
                    foreach ($dataFirst as $key => $value) {
                        if($key>0){
                        //$obj[$header[$i]] = $value->$header[$i];
                        $result = array();
                        $content = $dataFirst[$key];
                        for($i=0;$i<count($value);$i++) {
                            if(isset($obj[$header[$i]])) {
                                $o = array(
                                    $obj[$header[$i]] => $content[$i]
                                );
                                $result = array_merge($result, $o);
                            }
                            
                        }

                        $result['g_key']      = $user->g_key;
                        $result['c_key']      = $user->c_key;
                        $result['s_key']      = isset($result['s_key'])?$result['s_key']:$user->s_key;
                        $result['d_key']      = $user->d_key;
                        $result['created_by'] = $user->email;
                        $result['created_at'] = date("Y-m-d H: i: s");
                        $result['status']     = "UNTREATED";
                        if(!empty($result['ord_no'])) {
                            array_push($insert,  $result);
                        }

                        //$insert[] = ['title' => $value->title, 'description' => $value->description];
                        if(!empty($insert) ){
                            DB::table('tmp_mod_order')->insert($insert);
                            $insert = [];
                            //dd($insert);
                        }
                        }
                    }

                    if(!empty($insert)){
                        DB::table('tmp_mod_order')->insert($insert);
                        //dd($insert);
                    }
                }
            }
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        //return back();
        return ["msg"=>"success", "data"=>$insert];
	}

    public function get() {
        //TmpModOrderModel::getQuery()->delete();
        $tmpModOrder = [];
        $user = Auth::user();
        
        $this_query = DB::table('tmp_mod_order')
                        ->where('created_by', $user->email)
                        ->where('g_key',$user->g_key);
    
        $tmpModOrder = $this_query->get();
            
        
        
        $data[] = array(
            'Rows' => $tmpModOrder,
        );

        return response()->json($data);
    }

    public function saveToModOrder()
    {
        try {
            $json = request('obj');
            $data = json_decode($json, true);
            $sports = array();
            $user = Auth::user();
            
            $tmpIds = array();
            foreach($data as $row) {
                array_push($tmpIds, $row['id']);
            }

            $tmpData = DB::table('tmp_mod_order')
                            ->whereIn('id', $tmpIds)
                            ->orderBy('ord_no', 'asc')
                            ->get();
            if(count($tmpData) >  0) {
                $oldOrdNo = '';
                $ordId = '';
                $detailOrdId = '';
                foreach ($tmpData as $key=>$item) {
                    //新增主檔
                    if($oldOrdNo != $item->ord_no) {
                        if($ordId != '') {
                            //$this->updateModOrderGw($ordId, $user->g_key, $user->c_key);
                        }

                        $ordData = DB::table('mod_order')
                                ->where('ord_no', $item->ord_no)
                                ->where('g_key', $user->g_key)
                                ->where('c_key', $user->c_key)
                                ->where('owner_cd', $item->owner_cd)
                                ->whereIn('status', ['UNTREATED', 'SEND', 'LOADING'])
                                ->get();
                        if(count($ordData) == 0) {
                            $today       = new \DateTime();
                            $str_date    = $today->format('Ym');
                            $params = array(
                                "c_key"   => $user->c_key,
                                "date_ym" => substr($str_date, 2, strlen($str_date))
                            );
                            $BaseModel = new BaseModel();
                            $autoOrdNo = $BaseModel->getAutoNumber("order",$params);

                            $dataddsc = date('Y-m-d H:i:s');
                            $modOrder = new OrderMgmtModel;
                            $modOrder->dlv_cust_nm    = $item->dlv_cust_nm;
                            if(!empty($item->etd)){
                                $date = Carbon::parse($item->etd)->timezone(config('app.timezone'))->format('Y-m-d');
                                $modOrder->etd    = $date;                            
                            }
            
                            $dlvData = null;
                            if(isset($item->dlv_cust_no)) {
                                $dlvData = DB::table('sys_customers')
                                                ->where('cust_no', $item->dlv_cust_no)
                                                ->where('g_key', $user->g_key)
                                                ->where('c_key', $user->c_key)
                                                ->first();
                            }
                            $pickData = null;
                            if(isset($item->pick_cust_no)) {
                                $pickData = DB::table('sys_customers')
                                                ->where('cust_no', $item->pick_cust_no)
                                                ->where('g_key', $user->g_key)
                                                ->where('c_key', $user->c_key)
                                                ->first();
                            }

                            $ownerData = null;
                            if(isset($item->owner_cd)) {
                                $ownerData = DB::table('sys_customers')
                                                ->where('cust_no', $item->owner_cd)
                                                ->where('g_key', $user->g_key)
                                                ->where('c_key', $user->c_key)
                                                ->first();
                            }

                            $modOrder->owner_cd = $item->owner_cd;
                            if(isset($ownerData)) {
                                $modOrder->owner_nm = $ownerData->cname;
                            }
                            else {
                                $modOrder->owner_nm = $item->owner_nm;
                            }
            
                            $modOrder->driver     = $item->driver;
                            $modOrder->remark     = $item->remark;
                            $modOrder->ord_no     = $item->ord_no;
                            $modOrder->dlv_cust_no = $item->dlv_cust_no;
                            $pick_addr = "";
                            $dlv_addr  = "";

                            if(isset($dlvData)) {
                                $modOrder->dlv_cust_nm = $dlvData->cname;
                                $modOrder->dlv_zip     = (isset($item->zip))?$item->zip:$dlvData->zip;
                                $modOrder->dlv_city_no = $dlvData->city_no;
                                $modOrder->dlv_city_nm = (isset($item->dlv_city_nm))?$item->dlv_city_nm:$dlvData->city_nm;
                                $modOrder->dlv_area_id = $dlvData->area_id;
                                $modOrder->dlv_area_nm = (isset($item->dlv_area_nm))?$item->dlv_area_nm:$dlvData->area_nm;
                                $modOrder->dlv_addr    = (isset($item->dlv_addr))?$item->dlv_addr:$dlvData->address;
                                $modOrder->dlv_attn    = (isset($item->dlv_attn))?$item->dlv_attn:$dlvData->contact;
                                $modOrder->dlv_email   = (isset($item->dlv_email))?$item->dlv_email:$dlvData->email;
                                $modOrder->dlv_tel     = (isset($item->dlv_tel))?$item->dlv_tel:$dlvData->phone;
                                $modOrder->dlv_remark  = (isset($item->dlv_remark))?$item->dlv_remark:$dlvData->remark;
                                $dlv_addr = (isset($item->dlv_addr))?$item->dlv_addr:$dlvData->address;
                            }
                            else {
                                $modOrder->dlv_cust_nm = $item->dlv_cust_nm;
                                $modOrder->dlv_zip     = $item->dlv_zip;
                                $distData = DB::table('sys_area')->where('dist_cd', $item->dlv_zip)->first();
                                if(isset($distData)) {
                                    $modOrder->dlv_city_no = (isset($item->dlv_city_no))?$item->dlv_city_no:$distData->city_cd;
                                    $modOrder->dlv_city_nm = (isset($item->dlv_city_nm))?$item->dlv_city_nm:$distData->city_nm;
                                    $modOrder->dlv_area_id = (isset($item->dlv_area_id))?$item->dlv_area_id:$distData->dist_cd;
                                    $modOrder->dlv_area_nm = (isset($item->dlv_area_nm))?$item->dlv_area_nm:$distData->dist_nm;
                                }
                                else {
                                    $modOrder->dlv_city_no = $item->dlv_city_no;
                                    $modOrder->dlv_city_nm = $item->dlv_city_nm;
                                    $modOrder->dlv_area_id = $item->dlv_area_id;
                                    $modOrder->dlv_area_nm = $item->dlv_area_nm;
                                }
                                
                                $modOrder->dlv_addr     = $item->dlv_addr;
                                $modOrder->dlv_attn     = $item->dlv_attn;
                                $modOrder->dlv_email    = $item->dlv_email;
                                $modOrder->dlv_tel      = $item->dlv_tel;
                                $modOrder->dlv_remark   = $item->dlv_remark;
                                $modOrder->finish_date  = $item->finish_date;
                                $modOrder->car_no       = $item->car_no;
                                $modOrder->amt_remark   = $item->amt_remark;
                                if($item->temperate==""){
                                    $modOrder->temperate   = "N";
                                    $modOrder->temperate_desc = "常溫";
                                }else{
                                    $modOrder->temperate   = $item->temperate;
                                }
                                $temperate_desc = DB::table('bscode')->where('cd_type','TEMPERATE')->where('cd',$item->temperate)->first();
                                if(isset($temperate_desc)) {
                                    $modOrder->temperate_desc = $temperate_desc->cd_descp;
                                }
                                $modOrder->cust_ord_no  = $item->cust_ord_no;
                                $dlv_addr = $item->dlv_addr;
                            }
                            $modOrder->dlv_addr_info = $modOrder->dlv_zip.$modOrder->dlv_city_nm.$item->dlv_area_nm.$modOrder->dlv_addr;
                            $modOrder->pick_cust_no = $item->pick_cust_no;
                            if(isset($pickData)) {
                                // $modOrder->pick_cust_nm = $pickData->cname;
                                // $modOrder->pick_zip     = $pickData->zip;
                                // $modOrder->pick_city_no = $pickData->city_no;
                                // $modOrder->pick_city_nm = $pickData->city_nm;
                                // $modOrder->pick_area_id = $pickData->area_id;
                                // $modOrder->pick_area_nm = $pickData->area_nm;
                                // $modOrder->pick_addr    = $pickData->address;
                                // $modOrder->pick_attn    = $pickData->contact;
                                // $modOrder->pick_email   = $pickData->email;
                                // $modOrder->pick_tel     = $pickData->phone;
                                $pick_addr = $pickData->address;

                                $modOrder->pick_cust_nm = $pickData->cname;
                                $modOrder->pick_zip     = (isset($item->zip))?$item->zip:$pickData->zip;
                                $modOrder->pick_city_no = $pickData->city_no;
                                $modOrder->pick_city_nm = (isset($item->pick_city_nm))?$item->pick_city_nm:$pickData->city_nm;
                                $modOrder->pick_area_id = $pickData->area_id;
                                $modOrder->pick_area_nm = (isset($item->pick_area_nm))?$item->pick_area_nm:$pickData->area_nm;
                                $modOrder->pick_addr    = (isset($item->pick_addr))?$item->pick_addr:$pickData->address;
                                $modOrder->pick_attn    = (isset($item->pick_attn))?$item->pick_attn:$pickData->contact;
                                $modOrder->pick_email   = (isset($item->pick_email))?$item->pick_email:$pickData->email;
                                $modOrder->pick_tel     = (isset($item->pick_tel))?$item->pick_tel:$pickData->phone;
                                $modOrder->pick_remark  = (isset($item->pick_remark))?$item->pick_remark:$pickData->remark;
                                $pick_addr = (isset($item->pick_addr))?$item->pick_addr:$pickData->address;
                            }
                            else {
                                $modOrder->pick_cust_nm = $item->pick_cust_nm;
                                $modOrder->pick_zip     = $item->pick_zip;
                                $distData = DB::table('sys_area')->where('dist_cd', $item->pick_zip)->first();
                                if(isset($distData)) {
                                    $modOrder->pick_city_no = (isset($item->pick_city_no))?$item->pick_city_no:$distData->city_cd;
                                    $modOrder->pick_city_nm = (isset($item->pick_city_nm))?$item->pick_city_nm:$distData->city_nm;
                                    $modOrder->pick_area_id = (isset($item->pick_area_id))?$item->pick_area_id:$distData->dist_cd;
                                    $modOrder->pick_area_nm = (isset($item->pick_area_nm))?$item->pick_area_nm:$distData->dist_nm;
                                }
                                else {
                                    $modOrder->pick_city_no = $item->pick_city_no;
                                    $modOrder->pick_city_nm = $item->pick_city_nm;
                                    $modOrder->pick_area_id = $item->pick_area_id;
                                    $modOrder->pick_area_nm = $item->pick_area_nm;
                                }
                                
                                $modOrder->pick_city_no = $item->pick_city_no;
                                $modOrder->pick_city_nm = $item->pick_city_nm;
                                $modOrder->pick_area_id = $item->pick_area_id;
                                $modOrder->pick_area_nm = $item->pick_area_nm;
                                $modOrder->pick_addr    = $item->pick_addr;
                                $modOrder->pick_attn    = $item->pick_attn;
                                $modOrder->pick_email   = $item->pick_email;
                                $modOrder->pick_tel     = $item->pick_tel;
                                $modOrder->pick_remark  = $item->pick_remark;
                                $pick_addr = $item->pick_addr;
                            }
                            $modOrder->pick_addr_info = $modOrder->pick_zip.$modOrder->pick_city_nm.$modOrder->pick_area_nm.$modOrder->pick_addr;
                            
                            //$modOrder->pkg_num    = $item->pkg_num;
                            $modOrder->total_cbm  = $item->total_cbm;
                            $modOrder->total_gw   = $item->total_gw;
                            $modOrder->g_key      = $item->g_key;
                            $modOrder->c_key      = $item->c_key;
                            $modOrder->s_key      = $item->s_key;
                            $modOrder->d_key      = $item->d_key;
                            $modOrder->created_by = $user->email;
                            $modOrder->updated_by = $user->email;
                            if(empty($item->finish_date)){
                                $modOrder->status          = "UNTREATED";
                                $modOrder->status_desc     = "尚未安排";
                            }else{
                                $modOrder->status          = "FINISHED";
                                $modOrder->status_desc     = "配送完成";
                            }
                            $modOrder->trs_mode   = $item->trs_mode;
                            $TM_desc = DB::table('bscode')->where('cd_type','TM')->where('cd',$item->trs_mode)->first();
                            if(isset($TM_desc)) {
                                $modOrder->trs_mode_desc = $TM_desc->cd_descp;
                            }
                            //是否寄發email
                            $ownerSendMail = "N";
                            $pickSendMail = "N";
                            $dlvSendMail = "N";

                            
                            if(isset($item->owner_send_mail)) {
                                $ownerSendMail = $item->owner_send_mail;
                            }
                            else {
                                if(isset($item->owner_cd)) {
                                    $ownerSendMail = DB::table('sys_customers')
                                                        ->where('cust_no', $item->owner_cd)
                                                        ->where('g_key', $user->g_key)
                                                        ->where('c_key', $user->c_key)
                                                        ->value('receive_mail');
                                }
                            }
                            
                            if(isset($item->pick_send_mail)) {
                                $pickSendMail = $item->pick_send_mail;
                            }
                            else {
                                if(isset($item->pick_cust_no)) {
                                    $pickSendMail = DB::table('sys_customers')
                                                        ->where('cust_no', $item->pick_cust_no)
                                                        ->where('g_key', $user->g_key)
                                                        ->where('c_key', $user->c_key)
                                                        ->value('receive_mail');
                                }
                            }
                            
                            if(isset($item->dlv_send_mail)) {
                                $dlvSendMail = $item->dlv_send_mail;
                            }
                            else {
                                if(isset($item->dlv_cust_no)) {
                                    $dlvSendMail = DB::table('sys_customers')
                                                        ->where('cust_no', $item->dlv_cust_no)
                                                        ->where('g_key', $user->g_key)
                                                        ->where('c_key', $user->c_key)
                                                        ->value('receive_mail');
                                }
                            }

                            $modOrder->owner_send_mail = $ownerSendMail;
                            $modOrder->pick_send_mail = $pickSendMail;
                            $modOrder->dlv_send_mail = $dlvSendMail;
                            
                            //$carData = DB::table('sys_customers_item')->where('cust_type', 'CT003')->where('cust_no', $item->truck_cmp_no)->where('def', 1)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->first();
                            $carData = DB::table('sys_customers_item')->where('cust_type', 'CT003')->where('def', 1)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->first();
                            if(isset($carData)) {
                                $modOrder->truck_cmp_no = $carData->cust_no;
                                if(isset($carData->cname)) {
                                    $modOrder->truck_cmp_nm = $carData->cname;
                                }
                                else {
                                    $modOrder->truck_cmp_nm = $carData->ename;
                                }
                            }
                            else {
                                $carData = DB::table('sys_customers_item')->where('cust_type', 'CT003')->where('def', 1)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->first();
                                if(isset($carData)) {
                                    $modOrder->truck_cmp_no = $carData->cust_no;
                                    $modOrder->truck_cmp_nm = $carData->cname;
                                }
                            }

                            $whData = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->s_key)->where('identity', 'S')->first();
                            if(isset($whData)) {
                                $modOrder->wh_addr  = $whData->city_nm.$whData->area_nm.$whData->address;
                            }
                            else {
                                $modOrder->wh_addr = '';
                            }

                            $dist = 0;
                            if(!empty($dlv_addr) && !empty($pick_addr) && ($item->trs_mode == 'C' || $item->trs_mode == 'C01' || $item->trs_mode == 'C02'))
                            {
                                $g = new GoogleMapModel;                        
                                $dist = $g->getDist($modOrder->pick_city_nm.$modOrder->pick_area_nm.$pick_addr, $modOrder->dlv_city_nm.$modOrder->dlv_area_nm.$dlv_addr, $modOrder->wh_addr, $item->trs_mode);
                            }

                            $modOrder->distance   = $dist;
                            $modOrder->sys_ord_no = $autoOrdNo;

                            $modOrder->save();

                            $tm = new TrackingModel();
                            $tm->insertTracking('', $item->ord_no, '', $autoOrdNo, '', 'A', 1, $user->g_key, $user->c_key, $user->s_key, $user->d_key);

                            $ordId = $modOrder->id;
                            $detailOrdId = $modOrder->id;
                        }
                    }         

                    $oldOrdNo = $item->ord_no;
            
                    
                    //新增明細檔
                    $orderDetail             = new OrderDetailModel;
                    $orderDetail->ord_no     = $item->ord_no;
                    $orderDetail->goods_no   = $item->goods_no;
                    $orderDetail->goods_no2  = $item->goods_no2;
                    $orderDetail->pkg_num    = $item->pkg_num;
                    $orderDetail->gw         = (isset($item->total_gw))?$item->total_gw:0;
                    $orderDetail->cbm        = (isset($item->total_cbm))?$item->total_cbm*$item->pkg_num:0;
                    $orderDetail->g_key      = $user->g_key;
                    $orderDetail->c_key      = $user->c_key;
                    $orderDetail->s_key      = $user->s_key;
                    $orderDetail->d_key      = $user->d_key;
                    if(!empty($ordId)) {
                        $orderDetail->ord_id = $ordId;
                    }
                    else {
                        $orderDetail->ord_id = $ordData[0]->id;
                        $detailOrdId = $orderDetail->ord_id;
                    }
                    $goodsData = DB::table('mod_goods')
                                ->where('g_key', $user->g_key)
                                ->where('c_key', $user->c_key)
                                ->where('goods_no', $item->goods_no)
                                ->first();
                    if(isset($goodsData)) {
                        $orderDetail->goods_nm  = $goodsData->goods_nm;
                        $orderDetail->goods_no2 = $goodsData->goods_no2;
                        $orderDetail->pkg_unit  = $goodsData->pkg_unit;
                        //$orderDetail->goods_nm2 = $goodsData->goods_nm2;
                        $orderDetail->gwu       = $goodsData->gwu;
                        $orderDetail->cbm       = $goodsData->cbm;
                        $orderDetail->cbmu      = $goodsData->cbmu;
                        $orderDetail->length    = $goodsData->g_length;
                        $orderDetail->height    = $goodsData->g_height;
                        $orderDetail->weight    = $goodsData->g_width;
                        if(isset($goodsData->gw)) {
                            $orderDetail->gw       = $goodsData->gw * $item->pkg_num;
                        }
                        else {
                            $orderDetail->gw = 0;
                        }
                        if(isset($goodsData->cbm)) {
                            $orderDetail->cbm       = $goodsData->cbm * $item->pkg_num;
                        }
                        else {
                            $orderDetail->cbm = 0;
                        }
                    }
                    else {
                        $orderDetail->goods_nm = $item->goods_nm;
                        $orderDetail->gw       = $item->total_gw;
                        $orderDetail->goods_nm = $item->goods_nm;
                    }
                    
                    $orderDetail->created_by = $user->email;
                    $orderDetail->save();

                    $this->updateModOrderGw($orderDetail->ord_id, $user->g_key, $user->c_key);
                }

                if(!empty($detailOrdId)) {
                    //$this->updateModOrderGw($detailOrdId, $user->g_key, $user->c_key);
                }

                DB::table('tmp_mod_order')
                        ->where('created_by', $user->email)
                        ->where('g_key',$user->g_key)
                        ->delete();
            }
            
	}catch (\Exception $e) {
        return ["msg"=>"error", "errorLog"=>$e->getMessage()];
    }
        
        return ["msg"=>"success"];

    }

    public function getOrderExcel()
    {
        //PDF file is stored under project/public/download/info.pdf
        try{
        $file= storage_path(). "/excel/OrderImportSample.xlsx";
    
        $headers = array(
                    'Content-Type: application/xlsx',
                );
        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        return Response::download($file, 'ExampleForCar.xlsx', $headers);
    }

    private function updateModOrderGw($ordId, $g_key, $c_key) {
        $detailData = DB::table('mod_order_detail')
                        ->select(DB::raw('SUM(gw) as total_gw'), DB::raw('SUM(pkg_num) as pkg_num'), DB::raw('SUM(cbm) as total_cbm'))
                        ->where('ord_id', $ordId)
                        ->where('g_key', $g_key)
                        ->where('c_key', $c_key)
                        ->first();

        if(isset($detailData)) {
            OrderMgmtModel::where('id', $ordId)
                        ->where('g_key', $g_key)
                        ->where('c_key', $c_key)
                        ->update(['total_gw' => $detailData->total_gw, 'pkg_num' => $detailData->pkg_num, 'total_cbm' => $detailData->total_cbm]);
        }

        return;
    }

    public function clearTmpOrder() {
        $user = Auth::user();

        DB::table('tmp_mod_order')
            ->where('created_by', $user->email)
            ->where('g_key', $user->g_key)
            ->delete();

        return response()->json(['msg' => 'success']);
    }

}
