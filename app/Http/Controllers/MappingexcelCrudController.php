<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use App\Models\CustomerModel;
use App\Models\SampleDetailModel;
use App\Models\BaseModel;
use App\Models\CommonModel;
use App\Models\MappingexcelModel;
use App\Models\MappingdetailModel;
use App\Http\Requests\MappingexcelCrudRequest as StoreRequest;
use App\Http\Requests\MappingexcelCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store as Session;
use Illuminate\Support\Facades\Input;
use Excel;
class MappingexcelCrudController extends CrudController
{
    
    public function setup() {
        $user = Auth::user();
        $this->crud->setModel("App\Models\MappingexcelModel");
        $this->crud->setEntityNameStrings(trans("modExcelMapping.titleName") , trans("modExcelMapping.titleName") );
        $this->crud->setRoute(config('backpack.base.route_prefix').'/mappingexcel');
    
        
        $this->crud->setCreateView('mappingexcel.edit');
        $this->crud->setEditView('mappingexcel.edit');
        $this->crud->setListView('mappingexcel.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'mapping_code',
            'type' => 'lookup',
            'title' => 'My Lookup',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "cust_no=mapping_code", //field mapping
        ]);

        $this->crud->addField([
            'name' => 'mapping_name',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'station_tranfer_type',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'person_nm',
            'type' => 'text'
        ]);
       
        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'updated_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 's_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'hidden'
        ]);
    }    
    public function index() {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud']     = $this->crud;
        $this->data['title']    = $this->crud->entity_name_plural;
        $this->data['custinfo'] = DB::table('mod_excel_mapping')
        ->get();

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('mappingexcel'))
            {
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    
    public function edit($id) {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;
        $user = Auth::user();
        try{
            if(Auth::user()->hasPermissionTo('mappingexcel'))
            {
                return view($this->crud->getEditView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }
    public function store(StoreRequest $request) {
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        
        $mainmapping = DB::table('mod_excel_mapping')
        ->where('mapping_code', $request->mapping_code)
        ->where('mapping_name', $request->mapping_name)
        ->first();
        if (isset($mainmapping) ) {
            return ["msg"=>"error", "errorLog"=>"已存在同代碼的名稱"];
        }
        //dd($request->all());
        try {
            $response = parent::storeCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request) {
        
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $mainmapping = DB::table('mod_excel_mapping')
        ->where('id', '!=', $request->id)
        ->where('mapping_code', $request->mapping_code)
        ->where('mapping_name', $request->mapping_name)
        ->first();
        if (isset($mainmapping) ) {
            return ["msg"=>"error", "errorLog"=>"已存在同代碼的名稱"];
        }
        try {
            $response = parent::updateCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response];
    }

    public function destroy($id) {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('delete');

        $mappingdetail = MappingdetailModel::where('mapping_id',$id);
        $mappingdetail->delete();

        return $this->crud->delete($id);
    }

    public function multiDel() {
        $user = Auth::user();
        $ids = request('ids');
        
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $mappingexcel = MappingexcelModel::find($ids[$i]);

                $mappingdetail = MappingdetailModel::where('mapping_id',$ids[$i]);
                $mappingdetail->delete();

                $mappingexcel->delete();
            }
        }

        return response()->json(array('msg' => 'success'));
    }


    public function detailValidator($request) {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }

    public function detailStore(Request $request) {
        $validator = $this->detailValidator($request);    
        $user = Auth::user();
        $num = 0;
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $mappingdetail = new MappingdetailModel;
            foreach($request->all() as $key=>$val) {
                $mappingdetail[$key] = request($key);
            }
            $mappingdetail->save();
        }

        return ["msg"=>"success", "data"=>$mappingdetail->where('id', $mappingdetail->id)->get()];
    }

    public function detailUpdate(Request $request) {
        $validator = $this->detailValidator($request);
        $user = Auth::user();
        if($request->sn_no!=null){
            $request->sn_no = str_replace("，", ",",$request->sn_no); 
        }
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $mappingdetail = MappingdetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $mappingdetail[$key] = request($key);
            }
            $mappingdetail->updated_at = \Carbon::now();
            $mappingdetail->updated_by = $user->email;
            $mappingdetail->save();
        }


        return ["msg"=>"success", "data"=>$mappingdetail->where('id', $request->id)->get()];
    }

    public function detailDel($id) {

        $mappingdetail = MappingdetailModel::find($id);
        $mappingdetail->delete();


        return ["msg"=>"success"];
    }

    public function detailget($mapping_id=null) {
        $mappingdetail = [];
        if($mapping_id != null) {
            $this_query = DB::table('mod_excel_mapping_detail');
            $this_query->where('mapping_id', $mapping_id);
            $mappingdetail = $this_query->get();
        }

        foreach ($mappingdetail as $key => $row) {
            # code...
            if( $row->data_type == "TEXT" ) {
                $row->data_type_str = "字串";
            } else  if( $row->data_type == "INT" ) {
                $row->data_type_str = "數值";
            } else {
                $row->data_type_str = "";
            }
            
        }
        
        $data[] = array(
            'Rows' => $mappingdetail,
        );

        return response()->json($data);
    }

    public function changeExcel($custid) {
        ini_set('memory_limit', '512M');
        $now = date("YmdHis");
        $user = Auth:: user();
        $errormsg = '';

        try{
            $herderarray   = array();
            $ogherderarray = array();
            $checkheader = array();
            $excelcolumn = array();
            $headerdefaultvalue = array();
            $stationdefaultvalue = array();
            $dataarray = array();

            $logid =  DB::table('export_grid_excel')
            ->insertGetId([
                'gridtable'  => 'export_grid_excel',
                'g_key'      => $user->g_key,
                'c_key'      => $user->c_key,
                's_key'      => $user->s_key,
                'd_key'      => $user->d_key,
                'created_by' => $user->email,
                'updated_by' => $user->email,
                'created_at' => Carbon::now()->toDateTimeString(),
            ]);

            $mainmapping = DB::table('mod_excel_mapping')
            ->where('id', $custid)
            ->first();

            $excelmapping  = DB::table('mod_excel_mapping_detail')
            ->where('mapping_id', $custid)
            ->get()
            ->toArray();

            $sysarea = DB::table('sys_area')
            ->get();

            $areaarray = array();
            foreach ($sysarea as $areakey => $arearow) {
                $areaarray[$arearow->dist_cd] = $arearow->station_cd;
            }

            $specialstation  = array ();
            $resultHeader  = array ();
            $columntype  = array ();
            // excel_col_name
            foreach ($excelmapping as $key => $excelrow) {
                array_push($herderarray ,$excelrow->excel_col_name);
                array_push($excelcolumn ,$excelrow->col_name);
                $resultHeader[$excelrow->excel_col_name] = $excelrow->col_name;
                $checkvalue = array($excelrow->col_name => $excelrow->exportvalue);
                $checkheader = $checkheader + $checkvalue;

                $checktype = array($excelrow->excel_col_name => $excelrow->data_type);
                $columntype = $columntype + $checktype;

                $defaultvalue = array($excelrow->excel_col_name => $excelrow->exportvalue);
                $headerdefaultvalue = $headerdefaultvalue + $defaultvalue;

                $newogarray = array($excelrow->col_name => $excelrow->excel_col_name);
                $ogherderarray = $ogherderarray + $newogarray;

                if( $excelrow->station_tranfer_type != null) {
                    $specialstation[$excelrow->excel_col_name] = $excelrow->station_tranfer_type;
                }
                $stationdefault = array($excelrow->excel_col_name => $excelrow->exportvalue);
                $stationdefaultvalue = $stationdefaultvalue + $stationdefault;
            }
            // dd($resultHeader);
            // dd($checkheader); //導出excel title && 預設值
            if(Input::hasFile('import_file')){

                $path = Input::file('import_file')->getRealPath();
                Input::file('import_file')->storeAs('tmpexcel', Input::file('import_file')->getClientOriginalName());

                $path = Input::file('import_file')->getRealPath();
                $data = Excel::load($path, function($reader) {
                    $reader->setDateFormat('Y-m-d');
                })->get();
                
                $i         = 0;
                $dataFirst = $data[0];
                $header    = $dataFirst->first()->keys()->toArray();
                $insert    = array();

                if(!empty($dataFirst) && $dataFirst->count()){
                    foreach ($dataFirst as $key => $value) {
                        foreach ($resultHeader as $exportkey => $exportheader) {
                            for($i=0;$i<count($value);$i++) {
                                if(isset($resultHeader[$exportkey]) && $header[$i] == $exportheader) {
                                    if(isset($value[$header[$i]]) && !empty($value[$header[$i]])) {
                                        //原excel有值
                                        if($columntype[$exportkey] == "TEXT") {
                                            // $dataarray[$key][$exportkey] = "TEXT";
                                            $dataarray[$key][$exportkey] = (string) empty($value[$header[$i]]) ? '' : $value[$header[$i]];
                                        } else {
                                            // $dataarray[$key][$exportkey] = "INT";
                                            $dataarray[$key][$exportkey] = (float) empty($value[$header[$i]]) ? '0' : $value[$header[$i]];
                                        }
                                    } else {
                                        //原excel有欄位無值 使用預設值
                                        if($columntype[$exportkey] == "TEXT") {
                                            $dataarray[$key][$exportkey] = (string) empty($value[$header[$i]]) ? '' : $value[$header[$i]];
                                        } else {
                                            $dataarray[$key][$exportkey] = (float)  empty($value[$header[$i]]) ? '0' : $value[$header[$i]];
                                        }
                                        // $dataarray[$key][$exportkey]  = $headerdefaultvalue[$header[$i]];
                                    }
                                    
                                    if(isset($specialstation[$exportkey]) && $specialstation[$exportkey] != "") {
                                        if($specialstation[$exportkey] == "郵遞區號") {
                                            $valueLength = strlen($value[$header[$i]]);
                                            if( $valueLength >= 3) {
                                                $newvalue = substr($value[$header[$i]],0,3);
                                                $searchvale =  empty( $newvalue) ? '' :  $newvalue;
                                                $stationvalue = isset($areaarray[$searchvale]) ? $areaarray[$searchvale] : "";
                                            } else {
                                                $stationvalue = "";
                                            }

                                        } else {
                                            $stationvalue = empty($value[$header[$i]]) ? '' : $value[$header[$i]];
                                            $stationvalue = str_replace("台中", "臺中", $stationvalue); 
                                            $stationvalue = str_replace("台北", "臺北", $stationvalue); 
                                            $stationvalue = str_replace("台南", "臺南", $stationvalue); 
                                            $resultvalue = '';
                                            $checkcountresult = array();
                                            foreach ($sysarea as $checkkey => $checkrow) {
                                                $pos = strpos($stationvalue, $checkrow->dist_nm);
                                                if($pos) {
                                                    $checkcountresult[] = $checkrow->dist_cd; 
                                                }
                                            }
                                            
                                            if(count($checkcountresult) >= 1 ) {
                                                // 區域存在
                                                $checksysarea = DB::table('sys_area')
                                                ->whereIn('dist_cd', $checkcountresult)
                                                ->get();
                                                if( count($checksysarea) == 1) {
                                                    //只有一筆區域對應到
                                                    $resultvalue  = isset($areaarray[$checksysarea[0]->dist_cd]) ? $areaarray[$checksysarea[0]->dist_cd] : "";
                                                } else if( count($checksysarea) == 0 ) {
                                                    //沒有區域對應到
                                                    $resultvalue = '';
                                                } else {
                                                    \Log::info($stationvalue);
                                                    //多筆對應到
                                                    foreach ($checksysarea as $detailkey => $detailvalue) {
                                                        $keyvalue = mb_substr($detailvalue->city_nm,0,2);
                                                        $stationvalue = str_replace("台中", "臺中", $stationvalue); 
                                                        $stationvalue = str_replace("台北", "臺北", $stationvalue); 
                                                        $stationvalue = str_replace("台南", "臺南", $stationvalue); 
                                                        $posdetail = strpos($stationvalue, $keyvalue);

                                                        if( $posdetail === 0) {
                                                            $posdetail = true;
                                                        }
                                                        if( $posdetail != false) {
                                                            //市名稱
                                                            $checksysareadetail = array();
                                                            \Log::info($detailvalue->city_nm);
                                                           
                                                            $checksysareadetail = DB::table('sys_area')
                                                            ->whereIn('dist_cd', $checkcountresult)
                                                            ->where('city_nm', 'like' , "%{$keyvalue}%")
                                                            ->orderBy('id' , 'desc')
                                                            ->get();
                                                            \Log::info('sql area result');
                                                            \Log::info(count($checksysareadetail));
                                                            if( count($checksysareadetail) == 0 ) {
                                                                //找不對對應城市
                                                                $resultvalue = '';
                                                            } else if(count($checksysareadetail) == 1) {
                                                                //
                                                                $resultvalue = isset($areaarray[$checksysareadetail[0]->dist_cd]) ? $areaarray[$checksysareadetail[0]->dist_cd] : "";
                                                            } else {
                                                                //還是有多筆城市對應到
                                                                $resultvalue = '';
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                //區域不存在
                                                $resultvalue = '';
                                            }
                                            $stationvalue =  $resultvalue;
                                        }
                                        $dataarray[$key][$exportkey]  = $stationvalue;
                                    }
                                }
                            }
                        }
                    }

                    for ($j=0; $j < count($dataarray) ; $j++) { 
                        //原excel 沒有 但是要導出的欄位
                        foreach ($herderarray as $checkkey => $checkvalue) {
                            if(!isset($dataarray[$j][$checkvalue])) {

                                if($columntype[$exportkey] == "TEXT") {
                                    $dataarray[$j][$checkvalue] = (string) empty( $headerdefaultvalue[$checkvalue]) ? '' : $headerdefaultvalue[$checkvalue];
                                } else {
                                    $dataarray[$j][$checkvalue] = (float) empty( $headerdefaultvalue[$checkvalue]) ? '0' : $headerdefaultvalue[$checkvalue];
                                }
                            }
                        }
                    }

                }
            }

            $resultArray = array();
            foreach ($herderarray as $seq => $header) {
                if(isset($dataarray[0][$header])) {
                    $resultArray[$seq] = $header;
                }
            }
            // dd($dataarray);
            Excel::create("mappingresult-".$now, function($excel) use($herderarray, $dataarray, $resultArray) {
                $excel->sheet("Sheet1", function($sheet) use($herderarray, $dataarray, $resultArray)  {
                    $id = request('id');
                    $detailfeeoption = array();
                    $data =  DB::table('mod_cust_fee')
                    ->where('cust_id', $id)
                    ->get();
    
                    $sheet->row(1, $herderarray);
                    foreach($dataarray as $key=>$row) {

                        $columncount = count($resultArray);
                        $rowdetail = array();
                        foreach($resultArray as $detail=>$detailname) {
                            $rowdetail[$detail] = $row[$detailname];
                        }
                        // $excelmapping
                        $sheet->row($key+2, $rowdetail);
                        
                    }

    
                });
            })->store('xlsx', storage_path('app/excel'));

            DB::table('export_grid_excel')
            ->where('id', $logid)
            ->update([
                'updated_at' => Carbon::now()->toDateTimeString()
            ]);
            return [
                "msg"=>"success", "donwloadLink"=>'mappingresult-'.$now.".xlsx"
            ];

        }catch (\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return ["msg"=>"error", "errorMsg"=>'格式錯誤', "errorline"=>$e->getLine(), "message"=>$e->getMessage()];
        }
        return ["msg"=>"success", "data"=>'success'];
	}

}
