<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\BaseModel;
use App\Http\Requests\CustomerProfileCrudRequest as StoreRequest;
use App\Http\Requests\CustomerProfileCrudRequest as UpdateRequest;
use App\Models\CommonModel;
use App\Models\CustomerItemModel;
use App\Models\CustomerProfileModel;
use App\Mail\TrackingOMail;
use Illuminate\Support\Facades\Auth;
use App\Models\CustfeeModel;
use App\Models\CusterrorModel;
use App\Models\LongwayModel;
use Illuminate\Http\Request;
use Validator;
use HTML;
use Excel;
use Illuminate\Filesystem\FilesystemAdapter;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Ftp as FtpAdapter;
use League\Flysystem\Sftp\SftpAdapter as SFtpAdapter;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
class CustomerProfileCrudController extends CrudController
{
    
    public function setup() {
        $user = Auth::user();
        $this->crud->setModel("App\Models\CustomerProfileModel");
        $this->crud->setEntityNameStrings('客戶建檔', '客戶建檔');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/customerProfile');
    
        $this->crud->setColumns(['name']);

        
        $this->crud->setCreateView('customerProfile.edit');
        $this->crud->setEditView('customerProfile.edit');
        $this->crud->setListView('customerProfile.index');
        $this->crud->enableAjaxTable();
        $this->crud->addField([
            'name' => 'phone2',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'cust_no',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'sendmail',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'ftppath',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'ftpport',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'ftpuser',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'ftppassword',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'ftptype',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'ftpcat',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'ftpforder',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'cname',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'ename',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'fax',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'address',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'en_address',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'industry',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'zip_code',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'website',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'contact',
            'type' => 'text'
        ]);       

        $this->crud->addField([
            'name' => 'cmp_abbr',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'email',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'remark',
            'type' => 'textarea'
        ]);

        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'updated_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);                     

        $this->crud->addField([
            'name' => 'status',
            'type' => 'select'
        ]);

        $this->crud->addField([
            'name' => 'receive_mail',
            'type' => 'select'
        ]);

        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'cust_type_desc',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'type_desc',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'cust_type',
            'type' => 'select2_multiple',
            'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'CT')->where('c_key', $user->c_key)->get()
        ]);

        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 's_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'type',
            'type' => 'hidden'
        ]);

        $this->crud->addField([
            'name' => 'city_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'city_nm',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'custer_info',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'zip',
            'type' => 'lookup',
            'title' => '郵地區號查詢',
            'info1' => Crypt::encrypt('sys_area'), //table
            'info2' => Crypt::encrypt("dist_cd+city_nm+dist_nm,city_nm,dist_cd,dist_nm"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "dist_cd=zip;city_nm=city_nm;dist_nm=area_nm;dist_nm=custer_info;" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'area_id',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'receive_mail_desc',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'email_type_desc',
            'type' => 'text'
        ]);


        $this->crud->addField([
            'name' => 'area_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'def',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'tax_id',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'email_type',
            'type' => 'text'
        ]);
    }

    public function getData($table=null,$id) {
        $data = DB::table('sys_customers')->where('id', $id)->first();

        return response()->json($data);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        $custdata = DB::table('sys_customers')->where('id', $id)->first();
        $this->data['ownerinfo3'] = Crypt::encrypt("owner_cd='".$custdata->cust_no."'");
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }


    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;
        unset($request['custer_info']); 
        $request->merge(['type' => "OTHER"]);
        $request->merge(['type_desc' => "其他"]);

        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        $checkExist = DB::table('sys_customers')
        ->where('cust_no', $request->cust_no)
        ->count();
        if($checkExist >= 1)
        {
            return ["msg"=>"error", "errorLog"=>'此客戶代碼已存在'];
        }

        if(isset($request->cust_type)) {
            $custTypeData =  DB::table('bscode')
            ->select(
                DB::raw('group_concat(cd_descp) as name')
            )
            ->whereIn('cd', explode(',', $request->cust_type) )
            ->where('cd_type', 'CT')
            ->where('c_key', $user->c_key)
            ->first();
            if(isset($custTypeData)) {
                $request->merge(['cust_type_desc' => $custTypeData->name]);
            }
        } else {
            $request->merge(['cust_type_desc' => '']);
        }

        if($request->receive_mail == "Y") {
            $request->merge(['receive_mail_desc' => '是']);
        } else if ($request->receive_mail == "N") {
            $request->merge(['receive_mail_desc' => '否']);
        } else {
            $request->merge(['receive_mail_desc' => '']);
        }

        if($request->email_type == "B") {
            $request->merge(['email_type_desc' => '提+配']);
        } else if ($request->email_type == "P") {
            $request->merge(['email_type_desc' => '提貨']);
        } else if ($request->email_type == "D") {
            $request->merge(['email_type_desc' => '配送']);
        } else {
            $request->merge(['email_type_desc' => '']);
        }


        $check_cust_no =$request->cust_no;
        if(strpos($check_cust_no, '\'')!== false)
        {
            return ["msg"=>"error", "errorLog"=>'cust_no 不可包含單引號'];
        }

        try {
            if(isset($type) && count($type) > 0) {
                if(in_array('CT003', $type) && $request->def == 1) {
                    DB::table('sys_customers')
                        ->where('cust_type', $value)
                        ->where('g_key', $user->g_key)
                        ->where('c_key', $user->c_key)
                        ->update(['def' => 0]);
                }
            }
            $response = parent::storeCrud($request);
            if(isset($type) && count($type) > 0) {
                if(in_array('CT003', $type) && $request->def == 1) {
                    DB::table('sys_customers_item')
                        ->where('cust_no', $request->cust_no)
                        ->where('cust_type', $value)
                        ->where('g_key', $user->g_key)
                        ->where('c_key', $user->c_key)
                        ->update(['def' => 1]);
                }
                foreach($type as $value) {
                    $customerItem             = new CustomerItemModel;
                    $customerItem->cust_type  = $value;
                    $customerItem->cust_no    = $request->cust_no;
                    $customerItem->cname      = $request->cname;
                    $customerItem->ename      = $request->ename;
                    $customerItem->phone      = $request->phone;
                    $customerItem->address    = $request->address;
                    $customerItem->contact    = $request->contact;
                    $customerItem->cmp_abbr   = $request->cmp_abbr;
                    $customerItem->email      = $request->email;
                    $customerItem->remark     = $request->remark;
                    $customerItem->g_key      = $user->g_key;
                    $customerItem->c_key      = $user->c_key;
                    $customerItem->s_key      = $user->s_key;
                    $customerItem->d_key      = $user->d_key;
                    $customerItem->created_by = $user->email;
                    $customerItem->def        = $request->def;
                    $customerItem->save();
                }
            }
            
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
    }
    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }
    public function longwayStore(Request $request)
    {
        try{
            $validator = $this->detailValidator($request);        
            $commonFunc = new CommonModel;
            $request    = $commonFunc->processData($request, $this->crud->create_fields);
            if ($validator->fails()) {
                return ["msg"=>"error", "errorLog"=>$validator->messages()];
            }
            else {
                $orderPack = new LongwayModel;
                foreach($request->all() as $key=>$val) {
                    $orderPack[$key] = request($key);
                }
                $orderPack->save();
            }
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e->getmessage()];
        }

        return ["msg"=>"success", "data"=>$orderPack->where('id', $orderPack->id)->get()];
    }
    public function longwayUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);
        $commonFunc = new CommonModel;
        $request    = $commonFunc->processData($request, $this->crud->create_fields);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderPack = LongwayModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $orderPack[$key] = request($key);
            }
            $orderPack->save();
        }


        return ["msg"=>"success", "data"=>$orderPack->where('id', $request->id)->get()];
    }

    public function longwayDel($id)
    {
        $orderPack = LongwayModel::find($id);
        $orderPack->delete();        
        return ["msg"=>"success"];
    }

    public function longwayGet($cust_id=null) 
    {
        $orderDetail = [];
        if($cust_id != null) {
            $this_query = DB::table('mod_longway');
            $this_query->where('cust_id', $cust_id);
            $orderDetail = $this_query->get();
            
        }
        $data[] = array(
            'Rows' => $orderDetail,
        );
        return response()->json($data);
    }

    public function feeStore(Request $request)
    {
        try{
            $request->merge(['goods_no' => $request['goodsno'] ]);
            unset($request['goodsno']);
            $validator = $this->detailValidator($request);        
            $commonFunc = new CommonModel;
            $request    = $commonFunc->processData($request, $this->crud->create_fields);
            $check =DB::table('mod_cust_fee')
            ->where("cust_id",$request->cust_id)
            ->where("fee_cd",$request->fee_cd)
            ->where("goods_no",$request->goods_no)
            ->count();
            if($check>0){
                return ["msg"=>"error", "errorLog"=>"不可新增相同代碼的資料"];
            }
            if ($validator->fails()) {
                return ["msg"=>"error", "errorLog"=>$validator->messages()];
            }
            else {
                $orderPack = new CustfeeModel;
                foreach($request->all() as $key=>$val) {
                    $orderPack[$key] = request($key);
                }
                $orderPack->save();
            }
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e->getmessage()];
        }

        return ["msg"=>"success", "data"=>$orderPack->where('id', $orderPack->id)->get()];
    }
    public function feeUpdate(Request $request)
    {
        $request->merge(['goods_no' => $request['goodsno'] ]);
        unset($request['goodsno']);
        $validator = $this->detailValidator($request);
        $commonFunc = new CommonModel;
        $request    = $commonFunc->processData($request, $this->crud->create_fields);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderPack = CustfeeModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $orderPack[$key] = request($key);
            }
            $orderPack->save();
        }

        return ["msg"=>"success", "data"=>DB::connection('mysql::write')->table('mod_cust_fee')->where('id', $request->id)->get()];
    }

    public function feeDel($id)
    {
        $orderPack = CustfeeModel::find($id);
        $orderPack->delete();        
        return ["msg"=>"success"];
    }

    public function feeGet($cust_id=null) 
    {
        $orderDetail = [];
        if($cust_id != null) {
            $this_query = DB::table('mod_cust_fee');
            $this_query->where('cust_id', $cust_id);
            $orderDetail = $this_query->get();
            
        }
        foreach ($orderDetail as $key => $row) {
            $row->goodsno = $row->goods_no ;
        }
        $data[] = array(
            'Rows' => $orderDetail,
        );
        return response()->json($data);
    }

    public function errorStore(Request $request)
    {
        try{
            $validator = $this->detailValidator($request);        
            $commonFunc = new CommonModel;
            $request    = $commonFunc->processData($request, $this->crud->create_fields);
            $check =DB::table('mod_cust_error')
            ->where("cust_id",$request->cust_id)
            ->where("cd",$request->cd)
            ->count();
            if( $check>0){
                return ["msg"=>"error", "errorLog"=>"不可新增相同代碼的資料"];
            }
    
            if ($validator->fails()) {
                return ["msg"=>"error", "errorLog"=>$validator->messages()];
            }
            else {
                $orderPack = new CusterrorModel;
                foreach($request->all() as $key=>$val) {
                    $orderPack[$key] = request($key);
                }
                $orderPack->save();
            }
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e->getmessage()];
        }

        return ["msg"=>"success", "data"=>$orderPack->where('id', $orderPack->id)->get()];
    }
    public function errorUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);
        $commonFunc = new CommonModel;
        $request    = $commonFunc->processData($request, $this->crud->create_fields);
        $ogdata =DB::table('mod_cust_error')
        ->where("id","!=",$request->id)->first();
        $check =DB::table('mod_cust_error')
        ->where("id","!=",$request->id)
        ->where("cust_id",$ogdata->cust_id)
        ->where("cd",$request->cd)
        ->count();
        if($check>0){
            return ["msg"=>"error", "errorLog"=>"不可新增相同代碼的資料"];
        }

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderPack = CusterrorModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $orderPack[$key] = request($key);
            }
            $orderPack->save();
        }


        return ["msg"=>"success", "data"=>$orderPack->where('id', $request->id)->get(),"count"=>$check];
    }

    public function errorDel($id)
    {
        $orderPack = CusterrorModel::find($id);
        $orderPack->delete();        
        return ["msg"=>"success"];
    }

    public function errorGet($cust_id=null) 
    {
        $orderDetail = [];
        if($cust_id != null) {
            $this_query = DB::table('mod_cust_error');
            $this_query->where('cust_id', $cust_id);
            $orderDetail = $this_query->get();
            
        }
        $data[] = array(
            'Rows' => $orderDetail,
        );
        return response()->json($data);
    }
    
	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $check_cust_no =$request->cust_no;
        if(strpos($check_cust_no, '\'')!== false)
        {
            return ["msg"=>"error", "errorLog"=>'cust_no 不可包含單引號'];
        }
        $checkExist = DB::table('sys_customers')
        ->where('cust_no', $request->cust_no)
        ->where('id', '!=', $request->id)
        ->count();
        if($checkExist >= 1)
        {
            return ["msg"=>"error", "errorLog"=>'此客戶代碼已存在'];
        }

        unset($request['custer_info']);
        unset($request['created_at']);
        unset($request['created_by']);
        $type = $request->cust_type;
        if(isset($type)) {
            $custTypeData =  DB::table('bscode')
            ->select(
                DB::raw('group_concat(cd_descp) as name')
            )
            ->whereIn('cd', explode(',', $type) )
            ->where('cd_type', 'CT')
            ->where('c_key', $user->c_key)
            ->first();
            if(isset($custTypeData)) {
                $request->merge(['cust_type_desc' => $custTypeData->name]);
            }
        } else {
            $request->merge(['cust_type_desc' => '']);
        }

        if($request->receive_mail == "Y") {
            $request->merge(['receive_mail_desc' => '是']);
        } else if ($request->receive_mail == "N") {
            $request->merge(['receive_mail_desc' => '否']);
        } else {
            $request->merge(['receive_mail_desc' => '']);
        }

        if($request->email_type == "B") {
            $request->merge(['email_type_desc' => '提+配']);
        } else if ($request->email_type == "P") {
            $request->merge(['email_type_desc' => '提貨']);
        } else if ($request->email_type == "D") {
            $request->merge(['email_type_desc' => '配送']);
        } else {
            $request->merge(['email_type_desc' => '']);
        }

        try {
            if($request->def == 1) {
                DB::table('sys_customers')
                    ->where('cust_type', 'CT003')
                    ->where('g_key', $user->g_key)
                    ->where('c_key', $user->c_key)
                    ->update(['def' => 0]);
            }
            $response = parent::updateCrud($request);
            $customerProfile = CustomerProfileModel::find($request->id);
            //$customerProfile = DB::table('sys_customers')->select('*')->where('cust_no', $customer->cust_no)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->where('type', '=', 'OTHER')->first();           
            $customerItem = CustomerItemModel::where("cust_no",$customerProfile->cust_no)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->delete();                
            //$customerItem->delete();
            $type = $customerProfile->cust_type;

            if($type!=""){
                $types = explode(",", $type); 
                if($request->def == 1) {
                    DB::table('sys_customers_item')
                        ->where('cust_no', $request->cust_no)
                        ->where('cust_type', 'CT003')
                        ->where('g_key', $user->g_key)
                        ->where('c_key', $user->c_key)
                        ->update(['def' => 1]);
                }              
                foreach($types as $value) {
                    $customerItem             = new CustomerItemModel;
                    $customerItem->cust_type  = $value;
                    $customerItem->cust_no    = $customerProfile->cust_no;
                    $customerItem->cname      = $customerProfile->cname;
                    $customerItem->ename      = $customerProfile->ename;
                    $customerItem->phone      = $customerProfile->phone;
                    $customerItem->address    = $customerProfile->address;
                    $customerItem->contact    = $customerProfile->contact;
                    $customerItem->cmp_abbr   = $customerProfile->cmp_abbr;
                    $customerItem->email      = $customerProfile->email;
                    $customerItem->remark     = $customerProfile->remark;
                    $customerItem->g_key      = $user->g_key;
                    $customerItem->c_key      = $user->c_key;
                    $customerItem->s_key      = $user->s_key;
                    $customerItem->d_key      = $user->d_key;
                    $customerItem->created_by = $user->email;
                    $customerItem->updated_by = $user->email;
                    $customerItem->def        = $customerProfile->def;
                    $customerItem->save();
                }  
            }
            
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        
        
        return ["msg"=>"success", "response"=>$response];
    }
    
    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        $cust_no = DB::table('sys_customers')->where('id', $id)->pluck('cust_no');

        if(isset($cust_no[0])) {
            $customerItem = CustomerItemModel::where('cust_no', $cust_no[0]);
            
            $customerItem->delete();
            
        }

        return $this->crud->delete($id);
    }
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('customerProfile'))
            {
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    /*
    public function index()
    {
        $this->data['title'] = trans('backpack::logmanager.log_manager');

        return view('customer.index', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = trans('backpack::logmanager.log_manager');

        return view('customer.edit', $this->data);
    }
    */

    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $customerProfile = CustomerProfileModel::find($ids[$i]);
                $customerProfile->delete();
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function custsendedi(){
        $ids = request('ids');
        // dd($ids);
        // $ids = explode(",",$ids);
		$now = date('YmdHis');
		$ownernm="";
		$owners = DB::table('sys_customers')->whereIn("id",$ids)->get();
		foreach($owners as $row){
            // mod_ts_order
            $ownernm = $row->cname;
			try{
				Excel::create($ownernm."-".$now, function ($excel) use ($now,$row,$ids) {
					$excel->sheet($row->cname, function ($sheet) use ($now,$row,$ids) {
						$owners = DB::table('sys_customers')->where("id",$row->id)->where("g_key","CJL")->first();
						if(isset($owners)){
							$ownernm = $owners->cname;
						}
						$sheet->row(1, array(
							'貨主名稱','訂單號','客戶訂單編號', '倉庫訂單編號', '系統訂單號', '拋單日期',
							'訂單狀態敘述', '完成日期', '異常原因','異常備註'
						));
						$sheet->setFontFamily('新細明體');
                        // $sheet->setBorder('A1:F10', 'thin');
                        $deliverynos = DB::table('mod_ts_order')->where("is_send","N")->where("owner_cd",$row->cust_no)->get();
                        
						foreach ($deliverynos as $key => $row2) {
							$order = DB::table('mod_order')->where("sys_ord_no",$row2->sys_ord_no)->first();
							$findate = "";
							$expreason = "";
							$abnormal_remark = "";
							if($row2->status_desc=="配送完成" ||$row2->status_desc=="拒收"){
								$findate = $order->finish_date;
								$expreason = $order->exp_reason;
								$abnormal_remark = $order->abnormal_remark;
							}
							if($row2->status_desc=="配送發生問題"){
								$expreason = $order->exp_reason;
								$abnormal_remark = $order->abnormal_remark;
							}
							$sheet->row(($key + 2), array(
								$order->owner_nm,$order->ord_no,$order->cust_ord_no,$order->wms_order_no,$order->sys_ord_no,$order->created_at,
								$row2->status_desc,$findate,$expreason,$abnormal_remark
							)
						);
						}
					});	
				})->store('xls', storage_path('app/'));
				$patht =  Storage::disk('local')->getAdapter()->applyPathPrefix($ownernm."-".$now.'.xls');
				$sylcust  = DB::table('sys_customers')->where("id",$row->id)->where("g_key","CJL")->first();
				$deliverynos = DB::table('mod_ts_order')->where("is_send","N")->where("owner_cd",$row->cust_no)->get();
				if(isset($sylcust)){
					if($sylcust->ftptype=="EM"){
						$filesystem = new Filesystem(new SftpAdapter([
							'driver' => $sylcust->ftpcat,
							'host' => $sylcust->ftppath,
							'port'     => $sylcust->ftpport,
							'username' => $sylcust->ftpuser,
							'password' => $sylcust->ftppassword,
							'root' => $sylcust->ftpforder,
						]));
						$contents = Storage::disk('local')->get($ownernm."-".$now.'.xls');
						$filesystem->put($ownernm."-".$now.'.xls',$contents);
						$sendmaildata = DB::table('mod_ts_order')->where("is_send","N")->where("owner_cd",$row->cust_no)->get();
						$sendData = array(
							'Time' => Carbon::now()->toDateTimeString(),
							'filepath' =>$patht,
							'data' => $sendmaildata,
						);
						// Mail::to('nick@standard-info.com')
						// ->send(new TrackingOMail($sendData));
					}else if($sylcust->ftptype=="E"){
						$filesystem = new Filesystem(new SftpAdapter([
							'driver' => $sylcust->ftpcat,
							'host' => $sylcust->ftppath,
							'port'     => $sylcust->ftpport,
							'username' => $sylcust->ftpuser,
							'password' => $sylcust->ftppassword,
							'root' => $sylcust->ftpforder,
						]));
						$contents = Storage::disk('local')->get($ownernm."-".$now.'.xls');
						$filesystem->put($ownernm."-".$now.'.xls',$contents);
					}else if($sylcust->ftptype=="M"){
						
						$sendData = array(
							'Time' => Carbon::now()->toDateTimeString(),
							'filepath' =>$patht,
							'data' => $sendmaildata,
						);
	
						// Mail::to('nick@standard-info.com')
						// ->send(new TrackingOMail($sendData));
					}
                }
                foreach ($deliverynos as $key => $result) {
					DB::table('mod_ts_order')->where('id',$result->id)
					->update([
						'is_send'  => "Y",
					]);
				}
			Storage::disk('local')->delete($ownernm."-".$now.'.xls');
			}catch(\Exception $e) {
				\Log::info($e->getLine());
                \Log::info($e->getMessage());
                return response()->json(array('msg' => 'error','message' =>$e->getMessage(),'line' => $e->getLine()));
			} 
		}


        return response()->json(array('msg' => 'success'));
	}

	public function uploadExcel($custid)
	{
        ini_set('memory_limit', '512M');
        $user = Auth:: user();
        $errormsg = '';
        $obj  = array(
            '商品料號'   => 'goods_no',
            '費用代碼'   => 'fee_cd',
            '服務項目名稱' => 'fee_name',
            '金額'     => 'fee_amount',
        );
        try{
            $custdata = DB::table('sys_customers')->where('id', $custid)->first();
            if(Input::hasFile('import_file')){
                $path = Input::file('import_file')->getRealPath();
                $data = Excel::load($path, function($reader) {
                    $reader->setDateFormat('Y-m-d');
                })->get();

                $i         = 0;
                $dataFirst = $data[0];
                $header    = $dataFirst->first()->keys()->toArray();
                $insert    = array();
                if(!empty($dataFirst) && $dataFirst->count()){
                    foreach ($dataFirst as $key => $value) {
                        $result = array();
                        for($i=0;$i<count($value);$i++) {
                            if(isset($obj[$header[$i]])) {
                                // \Log::info($value[$header[$i]]);
                                $o = array(
                                    $obj[$header[$i]] => $value[$header[$i]]
                                );
                                $result = array_merge($result, $o);
                            }
                        }
                        $result['cust_id']    = $custid;
                        $result['g_key']      = $user->g_key;
                        $result['c_key']      = $user->c_key;
                        $result['s_key']      = $user->s_key;
                        $result['d_key']      = $user->d_key;
                        $result['created_by'] = $user->email;
                        $result['updated_by'] = $user->email;
                        $result['created_at'] = date("Y-m-d H:i:s");
                        $result['updated_at'] = date("Y-m-d H:i:s");

                        $checkExist = DB::table('mod_cust_fee')
                        ->where('g_key', $user->g_key)
                        ->where('cust_id', $custid)
                        ->where('goods_no', $result['goods_no'])
                        ->where('fee_cd', $result['fee_cd'])
                        ->first();

                        $goods = DB::table('mod_goods')
                        ->where('goods_no', $result['goods_no'])
                        ->where('owner_cd', $custdata->cust_no)
                        ->first();

                        if( $result['goods_no'] == '' || $result['fee_cd'] == '' || $result['fee_name'] == '') {
                            return ["msg"=>"error", "errorMsg"=> '第'.($key+1).'筆料號 費用代碼 服務項目名稱 不可為空' ];
                        }


                        if (!isset($goods)) {
                            $errormsg = $errormsg .'第'.($key+1).'筆料號'.$result['goods_no'].'不存在'."\r\n";
                        }

                        if(!isset($checkExist)) {
                            array_push($insert,  $result);
                        }else{
                            $errormsg = $errormsg .'第'.($key+1).'筆料號'.$result['goods_no'].'費用代碼'.$result['fee_cd'].'重復'."\r\n";
                        }
                        //  fee_cd, fee_name, goods_no
                    }

                    if($errormsg != ''){
                        return ["msg"=>"error", "errorMsg"=>$errormsg];
                    }
                    if(!empty($insert)){
                        DB::table('mod_cust_fee')->insert($insert);
                    }
                }
            }
        }catch (\Exception $e) {
            \Log::info($e->getLine());
            \Log::info($e->getMessage());
            return ["msg"=>"error", "errorMsg"=>'格式錯誤', "errorline"=>$e->getLine()];
        }
        return ["msg"=>"success", "data"=>'success'];
	}
}
