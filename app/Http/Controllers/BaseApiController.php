<?php

namespace App\Http\Controllers;

//use Illuminate\Routing\Controller;
use Storage;
use App\Models\BaseModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class BaseApiController extends Controller
{
    

    public function getGridJson(Request $request,$table,$status=true){
        try{
            $BaseModel = new BaseModel();            
            return $this->data[$table] =  $BaseModel->getSearchData($request,$table,($status === 'true'));
        }catch(\Exception $ex){
            dd($ex);
        }
        
    }

    public function getGridJsonForMongo(Request $request,$table,$status=true){
        try {
            // $startnum = $request->pagenum * $request->pagesize;
            // $data[] = array(
            //     'TotalRows' => DB::connection('mongodb')->collection('mod_order')->count(),
            //     'Rows' => DB::connection('mongodb')->collection('mod_order')->take(intval($request->pagesize))->skip(intval($startnum))->get(),
            //     'StatusCount' => array()
            // );
            $BaseModel = new BaseModel();
            return $this->data[$table] =  $BaseModel->getSearchDataForMongo($request, $table, ($status === 'true'));
        } catch (\Exception $ex) {
            dd($ex);
        }
    }
    public function getStatusCount($tableName) {
        try {
            $BaseModel = new BaseModel();            
            return $BaseModel->getStatusCount($tableName);
        }
        catch(\Exception $ex) {
            dd($ex->getMessage());
        }
    }

    public function getFieldsJson(Request $request,$table){
        try{
            $BaseModel = new BaseModel();
            $obj = null;

            if(isset($request['key'])) {
                $layout = $BaseModel->getLayout($request['key']);
                $obj = json_decode($layout);
            }

            $fieldData = $BaseModel->getColumnInfo($table);
            if($obj != null) {
                array_push($fieldData, $obj);
            }

            $result = array("layout" => $obj);

            array_push($fieldData, $result);
            return $fieldData;
        }catch(\Exception $ex){
            dd($ex);
        }
        
    }
    public function getFieldsLangSepc(Request $request,$table){
        try{
            $BaseModel = new BaseModel();
            echo $BaseModel->getColumnSpec($table);
        }catch(\Exception $ex){
            dd($ex);
        }
        
    }
    public function getLookupFieldsJson(Request $request,$info1,$info2){
        try{
            $BaseModel = new BaseModel();
            return $BaseModel->getColumnInfo(Crypt::decrypt($info1),Crypt::decrypt($info2));
        }catch(\Exception $ex){
            dd($ex);
        }
        
    }
    public function getLookupGridJson(Request $request,$info1,$info2,$info3){
        try{
            $BaseModel = new BaseModel();
            return $this->data[Crypt::decrypt($info1)] =  $BaseModel->getSearchData($request,Crypt::decrypt($info1),false,Crypt::decrypt($info2),Crypt::decrypt($info3));
        }catch(\Exception $ex){
            dd($ex);
        }
        
    }
    public function getAutocompleteJson(Request $request,$info1,$info2,$info3,$input){
        try{
            $BaseModel = new BaseModel();
            return $this->data[Crypt::decrypt($info1)] =  $BaseModel->getSearchData($request,Crypt::decrypt($info1),false,Crypt::decrypt($info2),Crypt::decrypt($info3),$input);
        }catch(\Exception $ex){
            dd($ex);
        }
        
    }
    public function saveLayoutJson(Request $request){
        try{
            $user = Auth::user();
            $BaseModel = new BaseModel();
            return $BaseModel->saveLayout($request['key'],$request['data'],$user);
        }catch(\Exception $ex){
            dd($ex);
            return "error";
        }
        
    }
    public function getLayoutJson(Request $request){
        try{
            $BaseModel = new BaseModel();
            return $BaseModel->getLayout($request['key']);
        }catch(\Exception $ex){
            dd($ex);
            return "error";
        }
        
    }

    public function getAutoNumber(Request $request)
    {
        $params = array(
            "name"=>"AAA",
            "location"=>"BBB"
        );
        $BaseModel = new BaseModel();
        echo $BaseModel->getAutoNumber("order",$params);
    }

    public function clearLayout() {
        $user = Auth::user();
        $key = request('key');

        if(isset($key)) {
            $layout = DB::table('sys_layout')->where('key', $key)->where('created_by', $user->email);
            $layout->delete();
        }
        
        return "true";
    }    
}
