<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\UploadedFile;
use Storage;
use File;
use Response;
use Excel;
use App\Models\DlvPlanModel;
use App\Models\OrderMgmtModel;
use App\Models\OrderDetailModel;
use App\Models\OrderPackModel;
use App\Models\TransRecordModel;
use App\Models\OrderPackDetailModel;
use App\Models\OrderamtDetailModel;
use App\Models\OrderCloseMgmtModel;
use App\Models\BaseModel;
use App\Models\CommonModel;
use App\Models\TrackingModel;
use App\Models\GoogleMapModel;
use App\Models\CalculateModel;
use Carbon\Carbon;
use Illuminate\Session\Store as Session;

use App\Http\Requests\OrderMgmtCrudRequest as StoreRequest;
use App\Http\Requests\OrderMgmtCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;

class OrderMgmtCrudController extends CrudController
{
    
    public function setup() {
        $this->crud->setModel("App\Models\OrderMgmtModel");
        $this->crud->setEntityNameStrings(trans('modOrder.titleNamenew'), trans('modOrder.titleNamenew'));
        $this->crud->setRoute(config('backpack.base.route_prefix').'/OrderMgmt1');
        $this->crud->pmsName = "modOrder";
        
        $this->crud->setCreateView('order.edit_test');
        $this->crud->setEditView('order.edit_test');
        $this->crud->setListView('order.ordOverView');
        //$this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'ord_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'status',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'trs_mode_desc',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'order_tally',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'trs_mode',
            'type' => 'select',
            'options' => DB::table('bscode')
            ->select('cd as code', 'cd_descp as descp', 'value2 as def')
            ->where('cd_type', 'TM')
            ->whereRaw('(value3 is null or value3 ="Y")')
            ->orderBy('value1', 'asc')
            ->where('g_key', Auth::user()->g_key)
            ->where('c_key', Auth::user()->c_key)
            ->get()
        ]);

        $this->crud->addField([
            'name' => 'temperate',
            'type' => 'select',
            'options' => DB::table('bscode')
                            ->select('cd as code', 'cd_descp as descp')
                            ->where('cd_type', 'TEMPERATE')
                            ->orderBy('value1', 'asc')
                            ->where('g_key', Auth::user()->g_key)
                            ->where('c_key', Auth::user()->c_key)
                            ->get()
        ]);


        $this->crud->addField([
            'name' => 'truck_cmp_nm',
            'type' => 'lookup',
            'title' => '卡車公司查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname"), //column
            'info3' => Crypt::encrypt("status='B' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
            'info4' => "cust_no=truck_cmp_no;cname=truck_cmp_nm" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'truck_cmp_no',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'cust_ord_no',
            'type' => 'text'
        ]);
        
        $this->crud->addField([
            'name' => 'car_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'driver',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'exp_reason',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'exp_type',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_cust_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'is_inbound',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_cust_nm',
            'type' => 'lookup',
            'title' => '客戶查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname,contact,address,zip,city_nm,area_id,area_nm,email,phone,phone2,remark,receive_mail"), //column
            'info3' => Crypt::encrypt("status='B' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
            'info4' => "cust_no=dlv_cust_no;cname=dlv_cust_nm;address=dlv_addr;contact=dlv_attn;zip=dlv_zip;city_nm=dlv_city_nm;area_id=dlv_area_id;area_nm=dlv_area_nm;email=dlv_email;phone=dlv_tel;phone2=dlv_tel2;remark=dlv_remark;receive_mail=dlv_send_mail;area_nm=dlv_info;" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'dlv_addr',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'dlv_info',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'dlv_addr_info',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_attn',
            'type' => 'text'
        ]);
        
        $this->crud->addField([
            'name' => 'dlv_tel',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'dlv_tel2',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'dlv_email',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_city_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_city_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_zip',
            'type' => 'lookup',
            'title' => '郵地區號查詢',
            'info1' => Crypt::encrypt('sys_area'), //table
            'info2' => Crypt::encrypt("dist_cd+city_nm+dist_nm,city_nm,dist_cd,dist_nm"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "dist_cd=dlv_zip;city_nm=dlv_city_nm;dist_nm=dlv_area_nm;dist_nm=dlv_info;" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'dlv_area_id',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_area_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_lat',
            'type' => 'text'
        ]);
        

        $this->crud->addField([
            'name' => 'dlv_lng',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_cust_nm',
            'type' => 'lookup',
            'title' => '客戶查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname,contact,address,zip,city_nm,area_id,area_nm,email,phone,phone2,remark,receive_mail"), //column
            'info3' => Crypt::encrypt("status='B' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
            'info4' => "area_nm=pick_info;cust_no=pick_cust_no;cname=pick_cust_nm;address=pick_addr;contact=pick_attn;zip=pick_zip;city_nm=pick_city_nm;area_id=pick_area_id;area_nm=pick_area_nm;email=pick_email;phone=pick_tel;phone2=pick_tel2;remark=pick_remark;receive_mail=pick_send_mail" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'pick_cust_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_addr',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_attn',
            'type' => 'text'
        ]);
        
        $this->crud->addField([
            'name' => 'pick_tel',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'pick_tel2',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_email',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_city_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_city_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_zip',
            'type' => 'lookup',
            'title' => '郵地區號查詢',
            'info1' => Crypt::encrypt('sys_area'), //table
            'info2' => Crypt::encrypt("dist_cd+city_nm+dist_nm,dist_cd,dist_nm,city_nm"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "city_nm=pick_info;dist_cd=pick_zip;city_nm=pick_city_nm;dist_nm=pick_area_nm;" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'pick_info',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'pick_addr_info',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_area_id',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_area_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_lat',
            'type' => 'text'
        ]);
        

        $this->crud->addField([
            'name' => 'pick_lng',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pkg_num',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pkg_unit',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 's_key',
            'type' => 'select'
        ]);

        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'remark',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_remark',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_remark',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'amt',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'wms_order_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'cust_amt',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'collectamt',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'amt_remark',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'total_gw',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'total_cbm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'owner_cd',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'owner_nm',
            'type' => 'lookup',
            'title' => '貨主查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname,receive_mail"), //column
            'info3' => Crypt::encrypt("status='B' AND type='OTHER' and cust_type like('%CT004%') AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
            'info4' => "cust_no=owner_cd;cname=owner_nm;receive_mail=owner_send_mail", //field mapping
            'triggerfunc' => 'ownerCallBack'
        ]);

        $this->crud->addField([
            'name' => 'sys_ord_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'distance',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'wh_addr',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'etd',
            'type' => 'date_picker'
        ]);

        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'owner_send_mail',
            'type' => 'select'
        ]);

        $this->crud->addField([
            'name' => 'pick_send_mail',
            'type' => 'select'
        ]);

        $this->crud->addField([
            'name' => 'dlv_send_mail',
            'type' => 'select'
        ]);
        $this->crud->addField([
            'name' => 'is_urgent',
            'type' => 'select'
        ]);
        $this->crud->addField([
            'name' => 'total_amount',
            'type' => 'text'
        ]);
    }

    public function index() {
        $user = Auth::user();

        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }
        
        $BaseModel = new BaseModel();
        $obj = null;

        $layout = $BaseModel->getLayout(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_view'));
        $obj = json_decode($layout);

        $fieldData = $BaseModel->getColumnInfo("mod_order_view");
        if($obj != null) {
            array_push($fieldData, $obj);
        }

        $colModel = array('mod_order_view' => $fieldData);

        $this->crud->colModel = $fieldData;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

    public function create()
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;
        $this->data['skeydata'] = DB::table('sys_customers')->where('type', 'SELF')->where('g_key', $user->g_key)->where('identity', 's')->get();
        $this->data['carData'] = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->c_key)->where('identity', 'c')->first();
        $this->data['info1']=Crypt::encrypt("cust_fee_view2");
        $this->data['info3']=  Crypt::encrypt("ownercd =''");
        $this->data['detailfeeoption'] =array();
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }

    public function trackingadd(Request $request=null)
    {
        //目前沒使用
        try{
        $user = Auth::user();
        $ids = request('ids');
        $data2 = ["result"];
        $result= "";
        $orderdata = DB::table('mod_order')
        ->where('id', $ids)
        ->first();        
        $postData = array(
            "ata"=> "string",
            "atd"=> "string",
            "booking_id"=> 0,
            "booking_no"=> "string",
            "cbm"=> (int)$orderdata->total_cbm,
            "container_no"=> "string",
            "date_event"=> "string",
            "date_event_local"=> "string",
            "eta"=> "string",
            "etd"=> "string",
            "is_enabled"=> 0,
            "location_code"=> "string",
            "location_name"=> "string",
            "qty"=> $orderdata->pkg_num,
            "qty_unit"=> $orderdata->pkg_unit,
            "remark"=> $orderdata->remark,
            "rmh_id"=> 0,
            "status_code"=> $orderdata->status,
            "status_desc"=> $orderdata->status_desc,
            "tracking_no"=> "string",
            "vessel_name"=> "string",
            "voyage"=> "string",
            "weight"=>(int) $orderdata->total_gw,
            "weight_unit"=> 'weight_unit'
        );
        $ch = curl_init();

        $server_output = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($server_output);
        
        $test = json_encode($postData);
            Storage::disk('public')->put('trackingadd.json',$test);
            Storage::disk('report')->put('trackingadd.json',$test);
        }
        catch (\Exception $e) {
            return response()->json(['msg' => $e->getMessage(),'filename'=>$user->email]);
        }
        return response()->json(['msg' => "success",'filename'=>$user->email]);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        $entry = $this->crud->getEntry($id);

        // get the info for that entry
        $this->data['entry'] = $entry;
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;
        $this->data['ord_id'] = $entry->ord_id;
        $this->data['ord_no'] = $entry->ord_no;
        $this->data['sys_ord_no'] = $entry->sys_ord_no;
        $this->data['carData'] = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->c_key)->where('identity', 'c')->first();
        $this->data['skeydata'] = DB::table('sys_customers')->where('type', 'SELF')->where('g_key', $user->g_key)->where('identity', 's')->get();
        $ownerdata = DB::table('sys_customers')->where('g_key', $user->g_key)->where('cust_no',  $entry->owner_cd)->first();
        if(isset($ownerdata) ) {
            $custfee = DB::table('mod_cust_fee')->where('g_key', $user->g_key)->where('cust_id',  $ownerdata->id)->count();
            if( isset($ownerdata)&& $custfee>0){
                $this->data['info1']= Crypt::encrypt("cust_fee_view");
                $this->data['info3']= Crypt::encrypt("ownercd='".$ownerdata->cust_no."'");
            }else{
                $this->data['info1']= Crypt::encrypt("cust_fee_view2");
                $this->data['info3']= Crypt::encrypt("ownercd =''");
            }
        } else {
            $this->data['info1']= Crypt::encrypt("cust_fee_view2");
            $this->data['info3']= Crypt::encrypt("");
        }
        $detailfeeoption = array();
        $detail =  DB::table('mod_order_detail')
        ->where('ord_id', $id)
        ->get();
        foreach($detail as $key => $row) {
            // $detailfeeoption[$row->id]
            for($i=1;$i<=$row->pkg_num ;$i++){
                $detailfeeoption[$row->id."-".$i] =$row->goods_no."(".$row->goods_nm.")"."-".$i;
            }
        }
        $this->data['detailfeeoption'] = $detailfeeoption;
        // $custcddata =DB::table('mod_cust_fee')->where('g_key', $user->g_key)->where('cust_id', $ownerdata->id)->pluck('fee_cd');
        // $this->data['custfeedata'] =  $custfeedata =DB::table('mod_cust_fee')->where('g_key', $user->g_key)->where('cust_id', $ownerdata->id)->get();;
        // $this->data['feedata'] = DB::table('bscode')->where('g_key', $user->g_key)->where('cd_type', 'EXTRAFEE')->whereNotIn('cd', $custcddata)->get();
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('OrderMgmt1'))
            {
                return view($this->crud->getEditView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return view($this->crud->getEditView(), $this->data);
    }

    public function get($ord_id=null) {
        $orderDetail = [];
        if($ord_id != null) {
            $this_query = DB::table('mod_order_detail');
            $this_query->where('ord_id', $ord_id);
            $orderDetail = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $orderDetail,
        );

        return response()->json($data);
    }

    public function packGet($ord_no=null) 
    {
        $orderDetail = [];
        if($ord_no != null) {
            $this_query = DB::table('mod_order_pack');
            $this_query->where('ord_no', $ord_no);
            $orderDetail = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $orderDetail,
        );

        return response()->json($data);
    }

    public function packDetailGet($ord_no=null) 
    {
        $packDetail = [];
        
        $packDetail = $this->getPackDetail($ord_no);

        //dd($packDetail);
        
        $data[] = array(
            'Rows' => $packDetail,
        );

        return response()->json($data);
    }


    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $newskey =  $request->s_key;
        $request    = $commonFunc->processData($request, $this->crud->create_fields);

        $pickaddr = $request->pick_addr;
        $pickaddr  = str_replace($request->pick_city_nm,'',$pickaddr);
        $pickaddr  = str_replace($request->pick_area_nm,'',$pickaddr);
        $pickaddr  = $request->pick_city_nm.$request->pick_area_nm.$pickaddr;

        $dlvaddr  =  $request->dlv_addr;
        $dlvaddr  = str_replace($request->dlv_city_nm,'',$dlvaddr);
        $dlvaddr  = str_replace($request->dlv_area_nm,'',$dlvaddr);
        $dlvaddr  = $request->dlv_city_nm.$request->dlv_area_nm.$dlvaddr;

        if($request->temperate== null) {
            $request->merge(['temperate' => ""]);
            $request->merge(['temperate_desc' => "常溫"]);
        }
        $request->merge(['dlv_addr_info' => $dlvaddr]);
        $request->merge(['pick_addr_info' => $pickaddr]);
        if($request->trs_mode!= null){
            $tmdata = DB::table('bscode')
            ->where('cd_type', 'TM')
            ->where('g_key', $user->g_key)
            ->where('c_key', $user->c_key)
            ->where('cd',$request->trs_mode)
            ->first();
            $request->merge(['trs_mode_desc' => $tmdata->cd_descp]);
        }
        if($request->temperate!= null){
            $temperatedata = DB::table('bscode')
            ->where('cd_type', 'temperate')
            ->where('g_key', $user->g_key)
            ->where('c_key', $user->c_key)
            ->where('cd',$request->temperate)
            ->first();
            $request->merge(['temperate_desc' => $temperatedata->cd_descp]);
        }
        unset($request['pick_info']);
        unset($request['dlv_info']);
        $today      = new \DateTime();
        $str_date    = $today->format('Ym');
        $params = array(
            "c_key" => $user->c_key,
            "date_ym" => substr($str_date, 2, strlen($str_date))
        );
        $BaseModel = new BaseModel();
        $sysOrdNo = $BaseModel->getAutoNumber("order",$params);
        $request->merge(array('sys_ord_no' => $sysOrdNo));
        $request->merge(array('status_desc' => '尚未安排'));
        if($request->ord_no == "" || $request->ord_no == null) {
            $request->merge(array('ord_no' => $sysOrdNo));
        }
        if($newskey != null){
            $request->merge(['s_key' => $newskey]);
        }
        try {
            $response = parent::storeCrud($request);

            $tm = new TrackingModel();
            $tm->insertTracking('', $request->ord_no, '', $sysOrdNo, '', 'A', 1, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
            $ordData = DB::connection('mysql::write')->table('mod_order')->where('sys_ord_no', $sysOrdNo)->first();
            if(isset($request->dlv_addr) && isset($request->pick_addr) && ($ordData->trs_mode == 'C' || $ordData->trs_mode == 'C01' || $ordData->trs_mode == 'C02')) {
                $g = new GoogleMapModel;
                $wh_addr   = (isset($ordData->wh_addr))?$ordData->wh_addr: '';
                $dlv_city  = $ordData->dlv_city_nm;
                $dlv_area  = $ordData->dlv_area_nm;
                $pick_city = $ordData->pick_city;
                $pick_area = $ordData->pick_area_nm;
                $dist = $g->getDist($pick_city.$pick_area.$ordData->pick_addr, $dlv_city.$dlv_area.$ordData->dlv_addr, $wh_addr, $ordData->trs_mode);
                OrderMgmtModel::where('id', $ordData->id)
                                ->update(['distance' => $dist]);
            }
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage(), "errorLine"=>$e->getLine()];
        }

        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
    {   
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $newskey =  $request->s_key;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        unset($request['status']);
        $dlvaddr =  "";
        $pickaddr = "";
        if($request->trs_mode!= null){
            $tmdata = DB::table('bscode')
            ->where('cd_type', 'TM')
            ->where('g_key', Auth::user()->g_key)
            ->where('c_key', Auth::user()->c_key)
            ->where('cd',$request->trs_mode)
            ->first();
            $request->merge(['trs_mode_desc' => $tmdata->cd_descp]);
        }
        if($request->temperate!= null){
            $temperatedata = DB::table('bscode')
            ->where('cd_type', 'temperate')
            ->where('g_key', $user->g_key)
            ->where('c_key', $user->c_key)
            ->where('cd',$request->temperate)
            ->first();
            $request->merge(['temperate_desc' => $temperatedata->cd_descp]);
        }
        if($request->dlv_zip!=null){
            $areadata = DB::table('sys_area')->where('dist_cd', $request->pick_zip)->first();
            if(isset($areadata)){
                $ordData = OrderMgmtModel::where('id', $request->id)->first();
                $zip = $areadata->dist_cd;
                $city = $areadata->city_nm;
                $area = $areadata->dist_nm;
                $addr = $ordData->dlv_addr;
                if($request->dlv_addr!=null){
                    $dlvaddr  = str_replace($city,"",$request->dlv_addr);
                    $dlvaddr  = str_replace($area,"",$dlvaddr);
                    $dlvaddr = $city.$area.$dlvaddr;
                }else{
                    $dlvaddr  = str_replace($city,"",$addr);
                    $dlvaddr  = str_replace($area,"",$addr);
                    $dlvaddr = $city.$area.$addr;
                }
            }
        }else{
            $ordData = OrderMgmtModel::where('id', $request->id)->first();
            $zip = $ordData->dlv_zip;
            $city = $ordData->dlv_city_nm;
            $area = $ordData->dlv_area_nm;
            $addr = $ordData->dlv_addr;
            $dlvaddr  = str_replace($city,"",$request->dlv_addr);
            $dlvaddr  = str_replace($area,"",$dlvaddr);
            $dlvaddr = $city.$area.$dlvaddr;
        }

        $request->merge(['dlv_addr_info' => $dlvaddr]);

        if($request->pick_zip!=null){
            $areadata = DB::table('sys_area')->where('dist_cd', $request->pick_zip)->first();
            if(isset($areadata)){
                $ordData = OrderMgmtModel::where('id', $request->id)->first();
                $zip = $areadata->dist_cd;
                $city = $areadata->city_nm;
                $area = $areadata->dist_nm;
                $addr = $ordData->pick_addr;
                $pickaddr  = str_replace($city,"",$request->pick_addr);
                $pickaddr  = str_replace($area,"",$pickaddr);
                $pickaddr = $city.$area.$request->pick_addr;
            }
        }else{
            $ordData = OrderMgmtModel::where('id', $request->id)->first();
            $zip = $ordData->pick_zip;
            $city = $ordData->pick_city_nm;
            $area = $ordData->pick_area_nm;
            $addr = $ordData->pick_addr;
            if($request->pick_addr!=null){
                $pickaddr  = str_replace($city,"",$request->pick_addr);
                $pickaddr  = str_replace($area,"",$pickaddr);
                $pickaddr = $city.$area.$pickaddr;
            }else{
                $pickaddr  = str_replace($city,"",$addr);
                $pickaddr  = str_replace($area,"",$addr);
                $pickaddr = $city.$area.$addr;
            }
        }
        $request->merge(['pick_addr_info' => $pickaddr]);
        // dd($request->all());
        unset($request['pick_info']);
        unset($request['dlv_info']);
        $dist = 0;
        $ordData = null;
        if($newskey != null){
            $request->merge(['s_key' => $newskey]);
        }
        try {
            $response = parent::updateCrud($request);

            $dlv_addr  = $request->dlv_addr;
            $pick_addr = $request->pick_addr;
            $ordData = OrderMgmtModel::where('id', $request->id)->first();
            if($dlv_addr == null) {
                $city = $ordData->dlv_city_nm;
                $area = $ordData->dlv_area_nm;
                $dlv_addr = $city.$area.$ordData->dlv_addr;
            }

            if($pick_addr == null) {
                $city = $ordData->pick_city_nm;
                $area = $ordData->pick_area_nm;
                $pick_addr = $city.$area.$ordData->pick_addr;
            }
            if(isset($dlv_addr) && isset($pick_addr) && ($ordData->trs_mode == 'C' || $ordData->trs_mode == 'C01' || $ordData->trs_mode == 'C02')) {
                $g = new GoogleMapModel;

                $dist = $g->getDist($pick_addr, $dlv_addr, $ordData->wh_addr, $ordData->trs_mode);
                OrderMgmtModel::where('id', $request->id)
                                ->update(['distance' => $dist]);
            }
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        $ordData = DB::table('mod_order')->where('id', $request->id)->first();
        $dlv_plandata = DB::table('mod_dlv_plan')->where('sys_ord_no', $ordData->sys_ord_no)->where('dlv_no', $ordData->dlv_no)->orderBy('id', 'desc')->first();
        if(isset($dlv_plandata)){
            DB::table('mod_dlv_plan')
            ->where('dlv_no', $ordData->dlv_no)
            ->where('sys_ord_no', $ordData->sys_ord_no)
            ->where('dlv_type', 'P')
            ->update(['addr' => $ordData->pick_addr_info,'cust_nm' => $ordData->pick_cust_nm,'created_by' => $user->email,'created_at' => \Carbon::now()]);

            DB::table('mod_dlv_plan')
            ->where('dlv_no', $ordData->dlv_no)
            ->where('sys_ord_no', $ordData->sys_ord_no)
            ->where('dlv_type', 'D')
            ->update(['addr' => $ordData->dlv_addr_info,'cust_nm' => $ordData->dlv_cust_nm,'created_by' => $user->email,'created_at' => \Carbon::now()]);
            
        }


        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "dist" => $dist];
    }

    public function destroy($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('delete');

        $sysOrdNo = DB::table('mod_order')->where('id', $id)->pluck('sys_ord_no');

        if(isset($sysOrdNo[0])) {
            $orderDetail = OrderDetailModel::where('ord_id', $id);
            //$orderPack = OrderPackModel::where('ord_no', $ord_no[0]);
            //$orderPackDetail = OrderPackDetailModel::where('ord_no', $ord_no[0]);
            $tsRecord = DB::table('mod_ts_record')->where('ref_no4', $sysOrdNo[0])->where('g_key', $user->g_key)->where('c_key', $user->c_key);

            $orderDetail->delete();
            $tsRecord->delete();
            //$orderPack->delete();
            //$orderPackDetail->delete();
        }

        return $this->crud->delete($id);
    }

    public function detailStore(Request $request)
    {
        $validator = $this->detailValidator($request);        

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderDetail = new OrderDetailModel;
            foreach($request->all() as $key=>$val) {
                $orderDetail[$key] = request($key);
            }
            //dd($orderDetail);
            $orderDetail->save();
            $this->updateModOrderGw($orderDetail->ord_id);
        }

        return ["msg"=>"success", "data"=>$orderDetail->where('id', $orderDetail->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderDetail = OrderDetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $orderDetail[$key] = request($key);
            }
            $orderDetail->save();
            $this->updateModOrderGw($orderDetail->ord_id);
            $this->updateforboxSn($orderDetail->id,"order");
        }


        return ["msg"=>"success", "data"=>$orderDetail->where('id', $request->id)->get()];
    }
    private function updateforboxSn($id ,$type) {
        //
        \Log::info('updateforboxSn-order');
        $user = Auth::user();
        $insertData = array();
        $detailSnArray = array();
        $insertHistory = array();
        $detailData = DB::connection('mysql::write')->table('mod_order_detail')
        ->where('id', $id)
        ->first();

        $orddata = DB::connection('mysql::write')->table('mod_order')
        ->where('id', $detailData->ord_id)
        ->first();

        DB::table('mod_box_sn')
        ->where('sys_ord_no', $orddata->sys_ord_no)
        ->where('ord_detail_id', $id)
        ->delete();

        $explodeSnArray = explode(',', $detailData->sn_no);
        foreach ($explodeSnArray as $explodekey => $value) {
            $data = [
                'sys_ord_no'    => $orddata->sys_ord_no,
                'ord_detail_id' => $id,
                'goods_no' 		=> $detailData->goods_no,
                'goods_nm' 		=> $detailData->goods_nm,
                'sn_no'         => $value,
                'g_key'         => $user->g_key,
                'c_key'         => $user->c_key,
                's_key'         => $user->s_key,
                'd_key'         => $user->d_key,
                'created_by'    => $user->email,
                'updated_by'    => $user->email,
                'created_at'    => \Carbon::now(),
                'updated_at'    => \Carbon::now(),
            ];

            array_push($insertData, $data);
            if( $type != "order") {
                $snhistorydata = [
                    'ord_id' 		=> $orddata->id,
                    'snno' 			=> $value,
                    'created_by' 	=> $user->email,
                    'updated_by' 	=> $user->email,
                    'created_at' 	=> \Carbon::now(),
                    'updated_at' 	=> \Carbon::now(),
                ];
                array_push($insertHistory, $snhistorydata);
            }

        }

        DB::table('mod_box_sn')->insert($insertData);
        DB::table('mod_sn_history')->insert($insertHistory);

        DB::table('mod_order_detail')
        ->where('id',$id)
        ->update([
            'sn_count'    => count($insertData),
            'updated_by'  => $user->email,
            'updated_at'  => \Carbon::now(),
        ]);

        \Log::info('updateforboxSn-order end');

        return;
    }

    public function detailDel($id)
    {
        $orderDetail = OrderDetailModel::find($id);
        $orderDetail->delete();

        $orderPackDetail = OrderPackDetailModel::where('ord_detail_id', $id);
        $ordId = $orderDetail->ord_id;
        $orderPackDetail->delete();

        $this->updateModOrderGw($ordId);

        return ["msg"=>"success"];
    }


    public function packStore(Request $request)
    {
        $validator = $this->detailValidator($request);        

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderPack = new OrderPackModel;
            foreach($request->all() as $key=>$val) {
                $orderPack[$key] = request($key);
            }
            $orderPack->save();
        }

        return ["msg"=>"success", "data"=>$orderPack->where('id', $orderPack->id)->get()];
    }

    public function packUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderPack = OrderPackModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $orderPack[$key] = request($key);
            }
            $orderPack->save();
        }


        return ["msg"=>"success", "data"=>$orderPack->where('id', $request->id)->get()];
    }

    public function packDel($id)
    {
        $packData = DB::table('mod_order_pack')->where('id', $id)->first();
        
        $orderPack = OrderPackModel::find($id);
        $orderPack->delete();

        if(isset($packData->pack_no)) {
            $orderPackDetail = OrderPackDetailModel::where('pack_no',$packData->pack_no);
            $orderPackDetail->delete();
        }
        

        return ["msg"=>"success"];
    }

    public function packDetailUpdate(Request $request)
    {
        
        $order_detail_id = $request->id;
        $pack_no = $request->pack_no;
        $ord_no = $request->ord_no;
        $num = $request->num;
        $goods_no = $request->goods_no;

        $cnt = DB::table('mod_order_pack_detail')
                        ->where('ord_no', $ord_no)
                        ->where('pack_no', $pack_no)
                        ->where('ord_detail_id', $order_detail_id)
                        ->count();
        if($cnt == 0) {
            $orderPackDetail = new OrderPackDetailModel;
            $orderPackDetail->ord_no = $ord_no;
            $orderPackDetail->pack_no = $pack_no;
            $orderPackDetail->ord_detail_id = $order_detail_id;
            $orderPackDetail->num = $num;
            $orderPackDetail->save();
        }
        else {
            $id = DB::table('mod_order_pack_detail')
                        ->where('ord_no', $ord_no)
                        ->where('pack_no', $pack_no)
                        ->where('ord_detail_id', $order_detail_id)->pluck('id');
                        
            if(isset($id[0])) {
                $orderPackDetail = OrderPackDetailModel::find($id[0]);
                $orderPackDetail->num = $num;
                $orderPackDetail->save();
            }
        }

        $packDetailNum = DB::table('mod_order_pack_detail')
                            ->where('ord_no', $ord_no)
                            ->where('ord_detail_id', $order_detail_id)->sum('num');

        $ordDetailNum = DB::table('mod_order_detail')
                ->where('ord_no', $ord_no)
                ->where('goods_no', $goods_no)->sum('pkg_num');

        return ["msg"=>"success", "data"=>$ordDetailNum - $packDetailNum];
    }

    public function getPackDetail($ord_no) {
        $packDetail = [];
        // if($ord_no != null) {
        //     $this_query = DB::table('mod_order_pack_detail');
        //     $this_query->join('mod_order_detail', 'mod_order_detail.id', '=', 'mod_order_pack_detail.ord_detail_id');
        //     $this_query->select('mod_order_detail.id', 'mod_order_detail.goods_no', 'mod_order_detail.goods_nm', DB::raw('0 AS num'), DB::raw('sum(mod_order_pack_detail.num) as last_num'));
        //     $this_query->where('mod_order_pack_detail.ord_no', $ord_no);
        //     $this_query->groupBy('mod_order_detail.id', 'mod_order_detail.goods_no', 'mod_order_detail.goods_nm');
        //     $packDetail = $this_query->get();
            
        // }

        // //dd($packDetail);

        // if(count($packDetail) == 0) {
        //     $this_query = DB::table('mod_order_detail');
        //     $this_query->select('mod_order_detail.id', 'goods_no', 'goods_nm', DB::raw('0 AS num'), DB::raw('pkg_num AS last_num'));
        //     $this_query->where('ord_no', $ord_no);
        //     $packDetail = $this_query->get();
        // }
        // else {
            
        //     foreach($packDetail as $key=>$val) {
        //         $goods_no = $val->goods_no;

        //         $this_query = DB::table('mod_order_detail');
        //         $this_query->select(DB::raw('sum(pkg_num) AS ttl_num'));
        //         $this_query->where('ord_no', $ord_no);
        //         $this_query->where('goods_no', $goods_no);
        //         $orderDetail = $this_query->get();

        //         if(count($orderDetail) > 0) {
        //             $val->last_num = $orderDetail[0]->ttl_num - $val->last_num;
        //         }
               
        //     }
        // }

        $this_query = DB::table('mod_order_detail');
        $this_query->select('mod_order_detail.id', 'goods_no', 'goods_nm', DB::raw('0 AS num'), DB::raw('(select sum(num) from mod_order_pack_detail where mod_order_pack_detail.ord_detail_id=mod_order_detail.id) AS last_num'));
        $this_query->where('ord_no', $ord_no);
        $packDetail = $this_query->get();
        
        foreach($packDetail as $key=>$val) {
            $goods_no = $val->goods_no;

            $this_query = DB::table('mod_order_detail');
            $this_query->select(DB::raw('sum(pkg_num) AS ttl_num'));
            $this_query->where('ord_no', $ord_no);
            $this_query->where('goods_no', $goods_no);
            $orderDetail = $this_query->get();

            if(count($orderDetail) > 0) {
                $val->last_num = $orderDetail[0]->ttl_num - $val->last_num;
            }
           
        }

        return $packDetail;
    }


    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }

    public function getTsData() {
        $user = Auth::user();
        $data = [];

        $bsData = DB::table('bscode')
                    ->where('cd_type','TRANSTYPE')
                    ->where('g_key', $user->g_key)
                    ->where('c_key', $user->c_key)
                    ->orderBy('cd', 'asc')
                    ->get();

        foreach($bsData as $val) {
            $cd = $val->cd;

            $tsData = DB::table('mod_trans_status')
                        ->where('ts_type', $cd)
                        ->where('g_key', $user->g_key)
                        ->where('c_key', $user->c_key)
                        ->orderBy('order', 'asc')
                        ->get();
            $data[$cd]["status"] = [];
            $data[$cd]["ts_type"] = $val->cd_descp;
            foreach($tsData as $tsVal) {
                array_push($data[$cd]["status"], $tsVal);
            }

            if(count($tsData) == 0) {
                unset($data[$cd]);
            }
        }

        return response()->json($data);
    }

    public function getOrderData($dlv_no=null) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $returnData = array();
        $returnData["ordDatas"] = DB::table('mod_dlv_plan')->where('dlv_no', $dlv_no)->where('status', '<>', 'FINISHED')->orderBy('updated_at', 'desc')->get();
        $returnData["msg"] = "success";
        
        return response()->json($returnData);
    }

    public function getOrderDetail($ord_no=null) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $returnData = array();
        $returnData["ordDetail"] = DB::table('mod_order')->where('ord_no', $ord_no)->first();
        $returnData["msg"] = "success";
        
        return response()->json($returnData);
    }
    public function ordOverViewN() {
        $data = array(
            'crud' => (object)array(
                'colModel' => array()
            )
        );
        try{
            if(Auth::user()->hasPermissionTo('OrderMgmt'))
            {
                return view('order.index', $data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function orderUpload(Request $request) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');

        $imgData1 = substr($request->photo, 1+strrpos($request->photo, ','));
        Storage::disk('local')->put($request->ord_no.'.jpg', base64_decode($imgData1));

        $ord_no      = $request->ord_no;
        $ord_id      = $request->id;

        $ordData = DB::table("mod_order")->where("id", $ord_id)->first();

        DB::table('mod_order')
        ->where('id', $ord_id)
        ->update([
            'status' => "FINISHED",
            'status_desc' => "配送完成"
        ]);

        DB::table('mod_dlv_plan')
        ->where('ord_no', $ord_no)
        ->where('dlv_no', $ordData->dlv_no)
        ->update([
            'status' => "FINISHED"
        ]);
        
        
        $tsData = DB::table("mod_trans_status")->where("ts_type", "E")->orderBy("order", "desc")->first();
        $data = [
            'ts_no'      => $tsData->id,
            'ts_type'    => $tsData->ts_type,
            'sort'       => $tsData->order,
            'ts_name'    => $tsData->ts_name,
            'ts_desc'    => $tsData->ts_desc,
            'ref_no1'    => $ordData->dlv_no,
            'ref_no2'    => $ord_no,
            'ref_no3'    => $ordData->car_no,
            'ref_no4'    => null,
            'g_key'      => $ordData->g_key,
            'c_key'      => $ordData->c_key,
            's_key'      => $ordData->s_key,
            'd_key'      => $ordData->d_key,
            'created_by' => $ordData->car_no,
            'updated_by' => $ordData->car_no,
        ];

        $TransRecordModel = new TransRecordModel();
        $TransRecordModel->createRecord($data);

        if(isset($ordData->dlv_email)) {
            $TrackingModel = new TrackingModel();
            //$TrackingModel->sendTrackingMail($ord_no, $ordData->dlv_email, '貨況通知-訂單號：'.$ord_no);
        }


        return response()->json(["success" => true]);
    }


    public function showChkImg($ord_no=null, $type=null) {
        $imgData = [];
        try {
            /*
            $path = storage_path('app/' . $ord_no.'.jpg');
            if (!File::exists($path)) {
                return "尚未有圖片";
            }
            
            $file = File::get($path);
            $type = File::mimeType($path);
        
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
            */
            $user = Auth::user();
            $thisQuery = DB::table('mod_file')
                            ->where('g_key', $user->g_key)
                            ->where('c_key', $user->c_key)
                            ->where('ref_no', $ord_no);
            if($type == 'FINISH') {
                $thisQuery->whereIn('type_no', ['FINISH', 'SIGNIN']);
            }
            else {
                $thisQuery->where('type_no', $type);
            }

            $imgData = $thisQuery->get();

            foreach($imgData as $row) {
                $img = Storage::get($row->guid);
            }
        }
        catch(\Exception $e) {
            //echo $e->getMessage();
            return view('files.showImg')->with('imgData', []);
        }
    
        return view('files.showImg')->with('imgData', $imgData);
    }

    public function failOrder() {
        $ids = request('ids');
        $errorMsg = "";

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);
                if($order->status != "UNTREATED") {
                    $errorMsg = "系統訂單號：".$order->sys_ord_no."，狀態不在「尚未安排」，故無法作廢";
                    return response()->json(array('msg' => 'error', 'errorMsg' => $errorMsg));
                }
            }

            for($i=0; $i<count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);
                $order->status = 'FAIL';
                $order->status_desc = '作廢';
                $order->save();
            }
        }

        return response()->json(array('msg' => 'success', 'errorMsg' => $errorMsg));
    }

    public function closeOrder() {
        //結單
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);
                $order->status = 'CLOSE';
                $order->save();
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function multiDel() {
        $user = Auth::user();
        $ids = request('ids');
        $errorMsg = "";
        
        if(count($ids) > 0) {

            for($i=0; $i<count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);
                if($order->status != "UNTREATED") {
                    $errorMsg = "系統訂單號：".$order->sys_ord_no."，狀態不在「尚未安排」，故無法刪除";
                    return response()->json(array('msg' => 'error', 'errorMsg' => $errorMsg));
                }
                
            }
            for($i=0; $i<count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);
                $ordId = $order->id;
                $order->delete();

                $ordDetail = OrderDetailModel::where('ord_id',$ordId);
                $ordDetail->delete();

                $sysOrdNo = DB::table('mod_order')->where('id', $ordId)->pluck('sys_ord_no');

                if(isset($sysOrdNo[0])) {
                    $tsRecord = DB::table('mod_ts_record')->where('ref_no4', $sysOrdNo[0])->where('g_key', $user->g_key)->where('c_key', $user->c_key);
                    $tsRecord->delete();
                }
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function compulsiveDel() {
        //強制刪除
        $user = Auth::user();
        $ids = request('ids');

        
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);
                $ordId = $order->id;

                $DlvPlan = DB::table('mod_dlv_plan')->where('sys_ord_no', $order->sys_ord_no);
                $DlvPlan->delete();
                $order->delete();

                $ordDetail = OrderDetailModel::where('ord_id',$ordId);
                $ordDetail->delete();

                $sysOrdNo = DB::table('mod_order')->where('id', $ordId)->pluck('sys_ord_no');

                if(isset($sysOrdNo[0])) {
                    $tsRecord = DB::table('mod_ts_record')->where('ref_no4', $sysOrdNo[0])->where('g_key', $user->g_key)->where('c_key', $user->c_key);
                    $tsRecord->delete();


                }
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function reCalculateFee() {
        //重新計價
        $ids = request('ids');
        try {
            for($i=0; $i<count($ids); $i++) {
                $ordData = DB::table('mod_order')->where('id', $ids[$i])->first();
                if(isset($ordData) && $ordData->car_no != null) {
                    $c = new CalculateModel();
                    $amt = $c->calculateAmt($ids[$i]);
                    DB::table('mod_order')
                    ->where('id', $ids[$i])
                    ->update([
                        'amt' => $amt
                    ]);
                }
                else {
                    return response()->json(['msg' => 'error', 'showMsg' => '您有未派車的訂單，無法計價']);
                }
                
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e]);
        }
        
        return response()->json(['msg' => 'success']);
    }

    public function cancelError() {
        //解除異常
        $ids          = request('ids');
        $pickIds      = array();
        $readypickIds = array();
        try {
            if(is_array($ids)) {
                
                foreach ($ids as $key => $id) {
                    # code...
                    $ord =  DB::table('mod_order')->where('id', $id)->first();

                    $ordCnt = DB::table('mod_dlv_plan')
                                ->where('status', 'FINISHED')
                                ->where('dlv_type', 'P')
                                ->where('sys_ord_no', $ord->sys_ord_no)
                                ->count();
 
                    if($ordCnt >= 1) {
                        DB::table('mod_order')->where('id', $id)
                        ->update([
                            'status'      => 'READY_PICK',
                            'status_desc' => '已提貨可派車',
                            'dlv_no'      => null
                        ]);
                    } else {
                        DB::table('mod_order')->where('id', $id)
                        ->update([
                            'status'      => 'READY',
                            'status_desc' => '可派車',
                            'dlv_no'      => null
                        ]);
                    }
                }




            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }
        
        return response()->json(['msg' => 'success']);
    }

    private function updateModOrderGw($ord_id) {
        //材積 重量 重新計算
        \Log::info('updateModOrderGw-order');
        $detailData = DB::connection('mysql::write')->table('mod_order_detail')
                        ->select(DB::raw('SUM(gw) as total_gw'), DB::raw('SUM(pkg_num) as pkg_num'), DB::raw('SUM(cbm) as total_cbm'), DB::raw('sum(price*pkg_num) as total_price'))
                        ->where('ord_id', $ord_id)
                        ->first();
        if(isset($detailData)) {
            $pkgUnit = null;
            $detailUnit = DB::connection('mysql::write')->table('mod_order_detail')
                        ->select('pkg_unit')
                        ->whereNotNull('pkg_unit')
                        ->where('ord_id', $ord_id)
                        ->first();
            if(isset($detailUnit)) {
                $pkgUnit = $detailUnit->pkg_unit;
            }
            \Log::info($ord_id);
            \Log::info($detailData->total_gw);
            \Log::info($detailData->pkg_num);
            \Log::info($detailData->total_cbm);
            \Log::info($detailData->total_price);
            OrderMgmtModel::where('id', $ord_id)
                        ->update(['total_gw' => $detailData->total_gw, 'pkg_num' => $detailData->pkg_num, 'total_cbm' => $detailData->total_cbm, 'pkg_unit' => $pkgUnit]);
        }else{
            \Log::info('updateModOrderGw-order null');
            OrderMgmtModel::where('id', $ord_id)
                        ->update(['total_gw' => null, 'pkg_num' => null, 'total_cbm' => null, 'pkg_unit' => null, 'amt' => null]);
        }
        if(isset($detailData)) {
            OrderMgmtModel::where('id', $ord_id)
                        ->update(['amt' => $detailData->total_price,'pkg_unit' => $pkgUnit]);
        }
        \Log::info('updateModOrderGw-order end');
        return;
    }

    public function ordOverView() {
        $data = array(
            'crud' => (object)array(
                'colModel' => array()
            )
        );
        return view('order.index', $data);
    }

    public function confirmordOverView() {
        $this->data['crud'] = $this->crud;
        try{
            if(Auth::user()->hasPermissionTo('ConfirmOrderMgmt'))
            {
                return view('order.confirm',  $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function snnostore(Request $request) {
        //序號綁定 更新
        $user = Auth::user();
        $ids = request('ids');
        $nos = request('nos');
        try {
            for($i=0; $i<count($ids); $i++) {

                if(!empty($nos[$i])) {
                    $detail = DB::table('mod_order_detail')
                    ->where('id', $ids[$i])
                    ->update([
                        "sn_no"=> str_replace("，", ",",$nos[$i])
                    ]);
        
                    DB::table('mod_sn_history')->insert([
                        'ord_id' 		=> $ids[$i],
                        'snno' 			=> str_replace("，", ",",$nos[$i]),
                        'created_by' 	=> $user->email,
                        'updated_by' 	=> $user->email,
                        'created_at' 	=> \Carbon::now(),
                        'updated_at' 	=> \Carbon::now(),
                    ]);
                    
                    //
                    $this->updateforboxSn($ids[$i],"snnostore");
                }

            }
            return response()->json(array('msg' => 'success'));
        } catch (\Throwable $e) {

            return response()->json(array('msg' => 'error', "message"=>$e->getMessage(), "line"=>$e->getLine() ));
        }

    }
    
    public function saveboxdetail(Request $request) {
        //序號綁定 更新
        $user = Auth::user();

        $sysordno    = request('sysordno');
        $orddetailid = request('orddetailid');
        $goodsno     = request('goodsno');
        $goodsnm     = request('goodsnm');
        $pkgnum      = request('pkgnum');
        $swipecount  = request('swipecount');
        $boxno       = request('boxno');
        $snno        = request('snno');


        $detailnow = DB::table('mod_order_detail')
        ->where('id',$orddetailid)
        ->first();

        $sumBybeforesave =  DB::table('mod_box_detail')
        ->where('goods_no',$goodsno)
        ->where('ord_detail_id',$orddetailid)
        ->sum('swipe_count');

        $subtotal =  (int) $sumBybeforesave + (int)$swipecount ;

        if( $subtotal >  $detailnow->pkg_num  ) {
            return response()->json(array('msg' => '數量超出或小於等於0'));
        }

        if( $swipecount <=0  ) {
            return response()->json(array('msg' => '數量超出或小於等於0'));
        }

        $checkSnExist =  DB::table('mod_box_sn')
        ->where('sn_no',$snno)
        ->where('sys_ord_no',$sysordno)
        ->count();

        if( $checkSnExist > 0  ) {
            return response()->json(array('msg' => '此序號已存在'));
        }

        for ($i=1; $i <= $swipecount; $i++) { 
            DB::table('mod_box_detail')->insert([
                'sys_ord_no'    => $sysordno,
                'ord_detail_id' => $orddetailid,
                'pkg_num'       => $pkgnum,
                'swipe_count'   => 1,
                'box_no'        => $boxno,
                'goods_no'      => $goodsno,
                'goods_nm'      => $goodsnm,
                'g_key'         => $user->g_key,
                'c_key'         => $user->c_key,
                's_key'         => $user->s_key,
                'd_key'         => $user->d_key,
                'created_by'    => $user->email,
                'updated_by'    => $user->email,
                'created_at'    => \Carbon::now(),
                'updated_at'    => \Carbon::now(),
            ]);
        }

        if(!empty($snno)) {
            DB::table('mod_box_sn')->insert([
                'sys_ord_no'    => $sysordno,
                'ord_detail_id' => $orddetailid,
                'goods_no'      => $goodsno,
                'goods_nm'      => $goodsnm,
                'sn_no'         => $snno,
                'g_key'         => $user->g_key,
                'c_key'         => $user->c_key,
                's_key'         => $user->s_key,
                'd_key'         => $user->d_key,
                'created_by'    => $user->email,
                'updated_by'    => $user->email,
                'created_at'    => \Carbon::now(),
                'updated_at'    => \Carbon::now(),
            ]);
        }

        $allsnArray =  DB::connection('mysql::write')->table('mod_box_sn')
        ->where('ord_detail_id',$orddetailid)
        ->where('sn_no', '!=', '')
        ->pluck('sn_no')
        ->toArray();

        $allsn = implode(',', $allsnArray);


        DB::table('mod_order_detail')
        ->where('id',$orddetailid)
        ->update([
            'swipe_count' => (int)$detailnow->swipe_count + (int)$swipecount,
            'sn_count'    => count($allsnArray),
            'sn_no'       => $allsn,
            'box_no'      => $boxno,
            'updated_by'  => $user->email,
            'updated_at'  => \Carbon::now(),
        ]);
        
        return response()->json(array('msg' => 'success'));
    }
    public function changeboxcheck(Request $request) {

        $ordid = request('id');
        $boxno = request('boxno');

        try {
            $result = DB::table('mod_order_detail')
            ->where('ord_id',$ordid)
            ->where('box_no', $boxno)
            ->first();
    
            if(isset($result)) {
                return response()->json(array('msg' => 'success'));
            } else {
                return response()->json(array('msg' => 'error'));
            }
        } catch (\Throwable $e) {
            return response()->json(array('msg' => 'error', 'message'=>$e->getMessage() ));
        }



        return response()->json(array('msg' => 'success'));
    }
    public function clearbotask() {
        $user = Auth::user();
        $ordid = request('id');

        $order = DB::table('mod_order')
        ->where('id', $ordid)
        ->first();

        DB::table('mod_box_detail')
        ->where('sys_ord_no',$order->sys_ord_no)
        ->delete();

        DB::table('mod_box_sn')
        ->where('sys_ord_no',$order->sys_ord_no)
        ->delete();

        DB::table('mod_order_detail')
        ->where('ord_id',$ordid)
        ->update([
            'swipe_count' => null,
            'sn_count'    => null,
            'sn_no'       => null,
            'box_no'      => null,
            'updated_by'  => $user->email,
            'updated_at'  => \Carbon::now(),
        ]);

        return response()->json(array('msg' => 'success'));
    }

    public function changeboxno(Request $request) {
        //序號綁定 更新
        $user = Auth::user();

        $sysordno    = request('sysordno');
        $orddetailid = request('orddetailid');
        $goodsno     = request('goodsno');
        $goodsnm     = request('goodsnm');
        $pkgnum      = request('pkgnum');
        $swipecount  = request('swipecount');
        $boxno       = request('boxno');
        $snno        = request('snno');
        


        DB::table('mod_order_detail')
        ->where('id',$orddetailid)
        ->update([
            'box_no'      => ($boxno+1),
            'updated_by'  => $user->email,
            'updated_at'  => \Carbon::now(),
        ]);
        
        return response()->json(array('msg' => 'success'));
    }

    public function checkforsn() {
        //序號綁定 訂單確認
        $user = Auth::user();
        $ord_no = request('ord_no');
        $onwer = "";
        $ownercode = "";
        $order = DB::table('mod_order')
        ->where('sys_ord_no', $ord_no)
        ->where('g_key',$user->g_key)
        ->first();
        if(!isset($order)){
            $order = DB::table('mod_order')
            ->where('ord_no', $ord_no)
            ->where('g_key',$user->g_key)
            ->first();
        }
        if(!isset($order)){
            return "單號不存在";
        }
        $mainid    = $order->id;
        $onwer     = $order->owner_nm;
        $ownercode = $order->owner_cd;

        if( $order->owner_cd == "010" ) {
            $senaoorder = DB::table('mod_order_weblink')
            ->where('pick_no', $order->ord_no)
            ->orderBy('id', 'desc')
            ->first();
            if(isset($senaoorder)) {
                $onwer     = $senaoorder->cust_name;
                $ownercode = $senaoorder->cust_no;
            }
        }

        $data      = DB::table('mod_order_detail')
        ->where('ord_id', $order->id)
        ->get();

        foreach ($data as $key => $row) {
            $goodsdata  = DB::table('mod_goods')
            ->where('g_key', $user->g_key )
            ->where('goods_no', $row->goods_no)
            ->first();
            if(isset($goodsdata)) {
                $row->sn_flag = $goodsdata->sn_flag;
            } else {
                $row->sn_flag = 'N';
            }

        }
        
        $totalboxnum = DB::table('mod_box_detail')
        ->where('sys_ord_no', $ord_no)
        ->orderBy('box_no','desc')
        ->value('box_no');

        return response()->json(array('msg' => 'success','data'=>$data,'owner'=>$onwer, 'ownercode'=>$ownercode, 'sysordno'=>$order->sys_ord_no ,'mainid'=> $mainid ,'totalboxnum'=> $totalboxnum ));
    }

    public function checkasnrask() {
        //序號綁定 訂單確認
        $user = Auth::user();
        $sysordno = request('sysordno');
        $goodsno = request('goodsno');
        $onwer = "";
        
        $order = DB::table('mod_order')
        ->where('sys_ord_no', $sysordno)
        ->where('g_key',$user->g_key)
        ->first();

        $proddata  = DB::table('mod_goods')
        ->where('goods_no', $goodsno)
        ->where('g_key',$user->g_key)
        ->first();

        if(!isset($order)){
            return response()->json(array('msg' => 'error','data'=>null, 'proddata'=>null, 'remainnum'=>null ));
        }
        if(!isset($proddata)){
            return response()->json(array('msg' => 'error','data'=>null, 'proddata'=>null, 'remainnum'=>null ));
        }


        $onwer = $order->owner_nm;
        $data = DB::table('mod_order_detail')
        ->where('ord_id', $order->id)
        ->where('goods_no', $goodsno)
        ->first();

        if(!isset($data)){
            return response()->json(array('msg' => 'error','data'=>null, 'proddata'=>null, 'remainnum'=>null ));
        }

        $nowdetail = DB::table('mod_box_detail')
        ->where('ord_detail_id', $data->id)
        ->where('goods_no', $goodsno)
        ->get();

        $remainnum = 0;
        foreach ($nowdetail as $key => $row) {
           $remainnum += (int)$row->swipe_count;
        }

        $maxboxno = DB::table('mod_box_detail')
        ->where('sys_ord_no', $sysordno)
        ->orderby('box_no','desc')
        ->value('box_no');
        
        $data->box_no = $maxboxno ;

        $remainnum = $data->pkg_num - $remainnum;
        // remainnum

        return response()->json(array('msg' => 'success','data'=>$data, 'proddata'=>$proddata, 'remainnum'=>$remainnum ));
    }
    public function ordercheck() {
        $user = Auth::user();
        $ord_no = request('ord_no');
        $order = DB::table('mod_order')
        ->where('sys_ord_no', $ord_no)
        ->where('g_key',$user->g_key)
        ->first();
        if(!isset($order)){
            $order = DB::table('mod_order')
            ->where('ord_no', $ord_no)
            ->where('g_key',$user->g_key)
            ->first();
        }
        if($order->status=="ERROR"){
            if( $order->exp_reason!="A041" && $order->exp_reason!="A093")
            {
                return "checkerror";
            }
        }else{
            if($order->status!="FINISHED" && $order->status!="REJECT")
            {
                return "checkerror";
            }
        }
        if(!isset($order)){
            return "單號不存在";
        }
        return response()->json(array('msg' => 'success'));
    }
    
    public function ordDetailOverCloseView() {
        $data = array(
            'crud' => (object)array(
                'colModel' => array()
            )
        );
        try{
            if(Auth::user()->hasPermissionTo('ordDetailOverCloseView'))
            {
                return view('order.ordDetailOverCloseView', $data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }

    public function dlvordercheck(Request $request) {
        //刷單新增
        $user = Auth::user();
        $ord_no = $request->ord_no;
        $order = DB::table('mod_order')
        ->where('ord_no', $ord_no)
        ->where('g_key', $user->g_key)
        ->first();
        if(!isset($order)){
            $order = DB::table('mod_order')
            ->where('sys_ord_no', $ord_no)
            ->where('g_key', $user->g_key)
            ->first();
            if(!isset($order)){
                return response()->json(array('msg' => 'error'));
            }
        }
        // if(!isset($order)){
        //     $order = DB::table('mod_order')
        //     ->where('ord_no', $ord_no)
        //     ->where('status','PICKED')
        //     ->where('dlv_type','P')
        //     ->first();
        //     if(!isset($order)){
        //         $order = DB::table('mod_order')
        //         ->where('sys_ord_no', $ord_no)
        //         ->where('status','UNTREATED')
        //         ->where('g_key',$user->g_key)
        //         ->first();
        //         if(!isset($order)){
        //             $order = DB::table('mod_order')
        //             ->where('sys_ord_no', $ord_no)
        //             ->where('status','PICKED')
        //             ->where('dlv_type','P')
        //             ->first();
        //             if(!isset($order)){
        //                 return response()->json(array('msg' => 'error'));
        //             }
        //         }
        //     }
        // }
        //只有可派車 已提貨可派車 可以顯示 其他皆跳出異常訊息
        if( $order->status != 'READY' && $order->status != 'READY_PICK' ) {
            return response()->json(array('msg' => 'error'));
        }


        return response()->json(array('msg' => 'success','data'=>$order));
    }
    public function orderconfirm() {
        $user = Auth::user();
        $ids = request('ids');
        OrderMgmtModel::where('id', $ids)
        ->update(['is_confirm' => "Y", 'confirm_dt' =>\Carbon::now(), 'confirm_by' =>$user->email]);

        return response()->json(array('msg' => 'success'));
    }
    public function confirmordOveredit($ord_no) {
        $user = Auth::user();
        //回單修改畫面
        $order = DB::table('mod_order')
        ->where('sys_ord_no', $ord_no)
        ->where('g_key',$user->g_key)
        ->first();
        if(!isset($order)){
            $order = DB::table('mod_order')
            ->where('ord_no', $ord_no)
            ->where('g_key',$user->g_key)
            ->first();
        }
        if(!isset($order)){
            return "單號不存在";
        }
        $this->crud->hasAccessOrFail('update');

        $entry = $this->crud->getEntry($order->id);

        // get the info for that entry

        $this->data['entry'] = $entry;
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($order->id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        
        $this->data['id'] = $order->id;
        $this->data['ord_id'] = $entry->ord_id;
        $this->data['ord_no'] = $entry->ord_no;
        $this->data['sys_ord_no'] = $entry->sys_ord_no;
        $this->data['carData'] = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->c_key)->where('identity', 'c')->first();
        $this->data['skeydata'] = DB::table('sys_customers')->where('type', 'SELF')->where('g_key', $user->g_key)->where('identity', 's')->get();
        $this->data['close_status'] = "Y";
        $ownerdata = DB::table('sys_customers')->where('g_key', $user->g_key)->where('cust_no',  $entry->owner_cd)->first();
        if(isset($ownerdata) ) {
            $custfee = DB::table('mod_cust_fee')->where('g_key', $user->g_key)->where('cust_id',  $ownerdata->id)->count();
            if( isset($ownerdata)&& $custfee>0){
                $this->data['info1']= Crypt::encrypt("cust_fee_view");
                $this->data['info3']= Crypt::encrypt("ownercd='".$ownerdata->cust_no."'");
                
            }else{
                $this->data['info1']= Crypt::encrypt("cust_fee_view2");
                $this->data['info3']= Crypt::encrypt("ownercd =''");
            }
        } else {
            $this->data['info1']= Crypt::encrypt("cust_fee_view2");
            $this->data['info3']= Crypt::encrypt("ownercd =''");
        }
        $detailfeeoption = array();
        $detail =  DB::table('mod_order_detail')
        ->where('ord_id', $order->id)
        ->get();
        foreach($detail as $key => $row) {
            // $detailfeeoption[$row->id]
            for($i=1;$i<=$row->pkg_num ;$i++){
                $detailfeeoption[$row->id."-".$i] =$row->goods_no."(".$row->goods_nm.")"."-".$i;
            }
        }
        $this->data['detailfeeoption'] = $detailfeeoption;
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('order.confirmedit', $this->data);
    }
    public function ordDetailOverView() {
        $data = array(
            'crud' => (object)array(
                'colModel' => array()
            )
        );
        return view('order.orderDetailOverView', $data);
    }

    public function imgDownload() {
        //圖片下載
        try {
            $directory = 'tmp';

            $ids = request('ids');

            $sysOrdNoArray = explode(',', $ids);

            $fileData = DB::table('mod_file')
                            ->whereIn('ref_no', $sysOrdNoArray)
                            ->orderBy('ref_no', 'asc')
                            ->get();

            Storage::deleteDirectory($directory);
            Storage::makeDirectory($directory);

            $k = 0;
            $oldSysOrdNo = "";
            $test = "";
            if(count($fileData) == 0) {
                return "無檔案可下載";
            }

            foreach($fileData as $key=>$row) {
                $orderData = DB::table('mod_order')
                ->where('sys_ord_no', $row->ref_no)
                ->first();
                if(!isset($orderData->cust_ord_no)){
                    $sysOrdNo = $orderData->ord_no;
                }
                else{
                    $sysOrdNo = $orderData->cust_ord_no;
                }
                $fileName = $row->guid;
                $typeNo   = $row->type_no;

                if($key == 0) {
                    $oldSysOrdNo = $sysOrdNo;
                }

                if($oldSysOrdNo != $sysOrdNo) {
                    $k = 0;
                }

                if($typeNo == 'ERROR') {
                    Storage::copy($fileName, 'tmp/'.$sysOrdNo.'-'.$k.'-error.jpg');
                }
                else {
                    if($k==0){
                        Storage::copy($fileName, 'tmp/'.$sysOrdNo.'.jpg');
                    }else{
                        Storage::copy($fileName, 'tmp/'.$sysOrdNo.'-'.$k.'.jpg');
                    }
                }

                
                $k++;
            }

            $files = glob(storage_path('app/tmp/*'));

            $today       = new \DateTime();
            $str_date    = $today->format('Ymd');
            $zipName     = 'photo-'.$str_date;

            Storage::delete('order/'.$zipName.'.zip');
            \Zipper::make(storage_path('app/order/'.$zipName.'.zip'))->add($files)->close();

            
        }
        catch(\Exception $e) {
            return "下載發生問題";
        } 
        
        return response()->download(storage_path('app/order/'.$zipName.'.zip'));
    }


    public function sendedi() {
        //edi 轉出
        $user = Auth::user();
        $ids = request('ids');
        $now = date('YmdHis');
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {

                // $order = OrderMgmtModel::find($ids[$i]);
                $order = OrderMgmtModel::where('id', $ids[$i])
                ->where('owner_cd','007')
                ->where('g_key','SYL')
                ->whereIn('status', ['FINISHED', 'REJECT'])
                ->first();

                $content_is = "";
                $content_sh = "";
                $status     = "";
                $install    = "";
                $statusid   = "";
                if(count($order)>0){
                    $dlvdata = DB::table('mod_dlv')->where('dlv_no',$order->dlv_no)->first();
                    $sn_no = DB::table('mod_order_detail')->where('ord_no',$order->ord_no)->value('sn_no');
                    $date = date('YmdHis',strtotime($order->finish_date));
                    $deparedate = date('YmdHis',strtotime('-1 minutes',strtotime($order->finish_date)));
                    if($order->finish_date ==null){
                        $date =$now;
                        $deparedate = $now;
                    }
                    if($order->status=='FINISHED'){
                        $status  = "1";
                        $install = "1";
                    }else{
                        $status	 = '';
                        $install = '2';
                    } 
                    $content_is = $content_is.$order->ord_no.'|';
                    $content_is = $content_is.$order->cust_ord_no.'|';
                    $content_is = $content_is.$deparedate.'|';
                    $content_is = $content_is.$date.'|';
                    $content_is = $content_is.$status.'|';
                    $content_is = $content_is.$install.'|';
                    $content_is = $content_is.''.'|';
                    $content_is = $content_is.$sn_no.'|';
                    $content_is = $content_is.$dlvdata->driver_nm.'|';
                    $content_is = $content_is.$dlvdata->driver_phone.'|';
                    $content_is = $content_is.$date;
                    DB::table('sys_edi_log')->insert([
                        'sys_ord_no' => $order->sys_ord_no,
                        'style' => 'IS',
                        'status' => 'N',
                        'content' =>$content_is,
                        'g_key' =>$order->g_key,
                        'c_key' =>$order->c_key,
                        's_key' =>$order->s_key,
                        'd_key' =>$order->d_key,
                        'created_by' => $user->email,
                        'created_at' => \Carbon::now()
                    ]);
                
                    $date = date('YmdHis',strtotime($order->finish_date));
                    if($order->finish_date ==null){
                        $date =$now;
                    }
                    if($order->status=='FINISHED'){
                        $status   = "配達";
                        $statusid = "3";
                    }
                    else {
                        $status   = "客戶拒收";
                        $statusid = "5";
                    }
                    $content_sh = $content_sh.$order->ord_no.'|';
                    $content_sh = $content_sh.$order->cust_ord_no.'|';
                    $content_sh = $content_sh.'SYL-'.$order->s_key.'|';
                    $content_sh = $content_sh.$date.'|';
                    $content_sh = $content_sh.'HTY|';
                    $content_sh = $content_sh.$statusid.'|';
                    $content_sh = $content_sh.$status.'|';
                    $content_sh = $content_sh.$order->car_no.'|';
                    DB::table('sys_edi_log')->insert([
                        'sys_ord_no' => $order->sys_ord_no,
                        'style' => 'SH',
                        'status' => 'N',
                        'content' =>$content_sh,
                        'g_key' =>$order->g_key,
                        'c_key' =>$order->c_key,
                        's_key' =>$order->s_key,
                        'd_key' =>$order->d_key,
                        'created_by' => $user->email,
                        'created_at' => \Carbon::now()
                    ]);   
                    
                    $fileData = DB::table('mod_file')
                    ->where('ref_no', $order->sys_ord_no)
                    ->orderBy('ref_no', 'asc')
                    ->get();
                    if(count($fileData) > 0) {                    
                        foreach($fileData as $key=>$row) {
                            $fileName = $row->guid;
                            $typeNo   = $row->type_no;
                            DB::table('sys_edi_log')->insert([
                            'sys_ord_no' => $order->sys_ord_no,
                            'style' => 'IMG',
                            'status' => 'N',
                            'content' =>$fileName,
                            'g_key' =>$row->g_key,
                            'c_key' =>$row->c_key,
                            's_key' =>$row->s_key,
                            'd_key' =>$row->d_key,
                            'created_by' => $user->email,
                            'created_at' => \Carbon::now()
                            ]);
                        }
                    }
                }                
            }
        }
        return response()->json(array('msg' => 'success'));
    }
    public function mistakecomplete() {
        $user       = Auth::user();
        $ids = request('ids');
        $insertData = array();
        \Log::info("mistakecomplete");
        //誤刷完成
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                \Log::info($ids[$i]);
                $orderdata = DB::table('mod_order')
                ->where('id', $ids[$i])
                ->first(); 
                if($orderdata->status!="FINISHED" && $orderdata->status!="REJECT"){
                    return response()->json(['msg' => 'error', 'errorMsg' => "選取資料中有不是「配送完成」或「拒收」資料！"]);
                }
            }
            for($i=0; $i<count($ids); $i++) {
                $ordData = DB::table('mod_order')
                ->where('id', $ids[$i])
                ->first();
                $tsRecord = DB::table('mod_ts_record')->where('ref_no4', $ordData->sys_ord_no)->where('ts_type', 'E')->delete();

                if($ordData->owner_cd=="16606102"){
                    $pchome = DB::table('mod_pchome_edi')
                    ->where('sys_ord_no', $ordData->sys_ord_no)
                    ->where('STATUS', 'FINISHED')
                    ->first();
                    if(isset($pchome)){
                        if($pchome->is_send=="N"){
                            DB::table('mod_pchome_edi')
                            ->where('sys_ord_no', $ordData->sys_ord_no)
                            ->where('STATUS', 'FINISHED')
                            ->update([
                                "is_send" =>"M"
                            ]);
                        }
                    }
                }

                $errordata = DB::table('bscode')->where('cd_type',"ERRORTYPE")->where('cd', 'E001')->first();
                $pickCnt = DlvPlanModel::where('sys_ord_no', $ordData->sys_ord_no)
                ->where('dlv_type', 'P')
                ->where('status', 'FINISHED')
                ->count();
                if($pickCnt!=0){
                    DB::table('mod_order')->where('id', $ordData->id)
                    ->update([
                        'status' => 'PICKED',
                        'status_desc' => '已提貨',
                        'dlv_no' => null,
                        'dlv_type' => 'P',
                        'updated_by' =>$user->email ,
                        'updated_at' =>\Carbon::now() ,
                        'error_remark' => $errordata->cd_descp,
                        'exp_reason' => $errordata->cd_descp,
                        'updated_by' => $user->email
                    ]);
                    DB::table('mod_dlv_plan')
                    ->where('ord_id', $ordData->id)
                    ->where('dlv_no', $ordData->dlv_no)
                    ->where('dlv_type', 'D')
                    ->update([
                        'status' => 'ERROR',
                        'updated_at' =>\Carbon::now() ,
                        'error_cd' => $errordata->cd,
                        'error_descp' => $errordata->cd_descp,
                        'updated_by' => $user->email
                    ]);
                }else{
                    DB::table('mod_dlv_plan')
                    ->where('ord_id', $ordData->id)
                    ->where('dlv_no', $ordData->dlv_no)
                    ->update([
                        'status' => 'ERROR',
                        'updated_at' =>\Carbon::now() ,
                        'error_cd' => $errordata->cd,
                        'error_descp' => $errordata->cd_descp,
                        'updated_by' => $user->email
                    ]);
                    DB::table('mod_order')->where('id', $ordData->id)
                    ->update([
                        'status' => 'UNTREATED',
                        'status_desc' => '尚未安排',
                        'dlv_type' => null,
                        'dlv_no' => null,
                        'updated_by' =>$user->email ,
                        'updated_at' =>\Carbon::now() ,
                        'error_remark' => $errordata->cd_descp,
                        'exp_reason' => $errordata->cd_descp,
                        'updated_by' => $user->email
                    ]);
                }
                DB::table('mod_ts_order')
                ->where('sys_ord_no', $ordData->sys_ord_no)
                ->where("status_desc","配送完成")
                ->delete();
            }
        }
        \Log::info("mistakecomplete end");
        return response()->json(array('msg' => 'success'));
    }
    public function errorchangecar() {
        $user       = Auth::user();
        $ids = request('ids');
        $insertData = array();
        //批次換車運送
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $orderdata = DB::table('mod_order')
                ->where('id', $ids[$i])
                ->first(); 
                if($orderdata->status!="SETOFF"&&$orderdata->status!="PICKED"){
                    return response()->json(['msg' => 'error', 'errorMsg' => "選取資料中有不是「已提貨」或「出發」資料！"]);
                }
            }
            for($i=0; $i<count($ids); $i++) {
                $orderdata = DB::table('mod_order')
                ->where('id', $ids[$i])
                ->first(); 
                $ordModel = new OrderMgmtModel;
                $ordModel->insertpchomelog($orderdata->sys_ord_no,"PICKED");
                $dlvData = DB::table('mod_dlv')
                ->select('status')
                ->where('dlv_no', $orderdata->dlv_no)
                ->first();
                $count = DB::table('mod_dlv')
                ->select('status')
                ->where('dlv_no', $orderdata->dlv_no)
                ->whereNotIn('status', ["FINISHED","ERROR"])
                ->count();  
                if($count>0){

                try {

                $pickplancount = DB::table('mod_dlv_plan')
                ->where('dlv_no', $orderdata->dlv_no)
                ->where('sys_ord_no', $orderdata->sys_ord_no)
                ->where('dlv_type',"P")
                ->where('status',"!=","FINISHED")
                ->count();
                $dlvplancount = DB::table('mod_dlv_plan')
                ->where('dlv_no', $orderdata->dlv_no)
                ->where('sys_ord_no', $orderdata->sys_ord_no)
                ->where('dlv_type',"D")
                ->count();
                $dlvType  ="";

                if($orderdata->status=="PICKED"){
                    $dlvType  ="D";
                }else if($dlvData->status=="DLV" && $pickplancount!=0){
                    $dlvType  ="P";
                }else if($dlvData->status=="DLV" && $pickplancount==0){
                    $dlvType  ="D";
                }

                
                $sysOrdNo = $orderdata->sys_ord_no;
                $dlvNo = $orderdata->dlv_no;
                $errorStatus = "ERROR";
                $errordesc = "貨物--換車收/送";
                $errorType ="A001";

                $thisQuery = DB::table('bscode');
                $thisQuery->where('cd_type', "ERRORTYPE");
                $thisQuery->where('cd', $errorType);
                $thisQuery->where('g_key', $user->g_key);
                $thisQuery->where('c_key', $user->c_key);
                // $thisQuery->where('s_key', $user->s_key);
                // $thisQuery->where('d_key', $user->d_key);
                $bscode = $thisQuery->first();

                $cdDescp = "";;
                if(isset($bscode)) {
                    $cdDescp = $bscode->cd_descp;
                }

                \Log::info("history sys_ord_no");
                DB::table('mod_order')
                ->where('sys_ord_no', $sysOrdNo)
                ->update(['status' => 'PICKED', 'status_desc' => '已提貨', 'dlv_no' => null, 'error_remark' => $cdDescp, 'exp_reason' => $errorType, 'dlv_type' => 'P', 'abnormal_remark' => '']);

                $pickfincount = DB::table('mod_dlv_plan')
                ->where('dlv_no', $orderdata->dlv_no)
                ->where('sys_ord_no', $sysOrdNo)
                ->where('dlv_type',"P")
                ->whereNotIn('status', ["FINISHED","ERROR"])
                ->count();
                if($pickfincount!=0){
                    DB::table('mod_dlv_plan')
                    ->where('sys_ord_no', $sysOrdNo)
                    ->where('dlv_no', $dlvNo)
                    ->where('dlv_type',"P")
                    ->update(['status' => "FINISHED", 'updated_at' => \Carbon::now(), 'finish_date' => \Carbon::now()]);
                    try {
                        $TrackingModel = new TrackingModel();
                        $TrackingModel->sendDlvMail($sysOrdNo, "P"); //先發配送完成，直到取貨mail完成
                    }catch(\Exception $e) {
                    }
                }

                DB::table('mod_dlv_plan')
                ->where('sys_ord_no', $sysOrdNo)
                ->where('dlv_no', $dlvNo)
                ->where('dlv_type',"D")
                ->update(['status' => $errorStatus,'error_cd' => $errorType,'error_descp' => $cdDescp,'abnormal_remark' => '', 'updated_at' => \Carbon::now(), 'finish_date' =>\Carbon::now()]);

                $ordModel = new OrderMgmtModel;

                $cnt  = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->count();
                $fCnt = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->whereRaw("(status = 'FINISHED' or status = 'ERROR' or status = 'REJECT')")->count();

                if($cnt == $fCnt) {
                    DB::table('mod_dlv')
                        ->where('dlv_no', $dlvNo)
                        ->update(['status' => 'FINISHED','finished_time' => \Carbon::now()]);
                }
                }
                catch(\Exception $e) {
                \Log::info($e->getMessage());
                \Log::info($e->getLine());
                return response()->json(['msg' => 'error', 'error_log' => $e->getLine(), 'error_msg' => $e->getMessage()]);
                }
                }
            }
        }

        return response()->json(['msg' => 'success']);
    }
    public function getnewfeelookup() {
        $detailid = request('detailid');
        $ownercd = request('ownercd');

        $detailid = explode('-', $detailid);

        $detaildata = DB::table('mod_order_detail')
        ->where('id', $detailid[0] )
        ->first();

        $goodsno = $detaildata->goods_no;

        $val =  Crypt::encrypt("goods_no='".$goodsno."' and ownercd='".$ownercd."'");

        // $detailfeeoption = array();
        // $detail =  DB::table('mod_order_detail')
        // ->where('ord_id', $detaildata->ord_id)
        // ->get();
        // foreach($detail as $key => $row) {
        //     for($i=1;$i<=$row->pkg_num ;$i++){
        //         $detailfeeoption[$row->id."-".$i] =$row->goods_no."(".$row->goods_nm.")"."-".$i;
        //     }
        // }
        // $detailfeeoption;
        $detailfeeoption = array();
       return response()->json(['msg' => 'success','val'=>$val ,'detailfeeoption'=>$detailfeeoption ]);

    }
    public function getnewfee() {
        $user = Auth::user();
        try{
            $feecd = request('feecd');
            $ownercd = request('ownercd');
            $goods_no = request('goodsno');
            $amount = 0;

            $ownerdata=DB::table('sys_customers')
            ->where('cust_no', $ownercd )
            ->where('g_key',$user->g_key)
            ->first();

            $feedata=DB::table('mod_cust_fee')
            ->where('fee_cd', $feecd )
            ->where('goods_no', $goods_no )
            ->where('cust_id', $ownerdata->id )
            ->first();
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'error_log' => $e->getLine(), 'error_msg' => $e->getMessage()]);
        }
        return response()->json(['msg' => 'success','data'=>$feedata ]);
    }

    public function exportfee(Request $request){

        $ids = request('ids');
        $orddata=DB::table('mod_order')
        ->whereIn('sys_ord_no', $ids )
        ->pluck("id");

        $now = date('YmdHis');
        Excel::create("服務項目明細", function ($excel) use ($now,$orddata ) {
            $excel->sheet("服務項目明細", function ($sheet) use ($now,$orddata) {
                // 貨主名稱 訂單號碼 服務項目名稱 敘述 費用
                $sheet->row(1, array(
                    '貨主名稱','訂單號碼','服務項目名稱', '敘述', '費用'
                ));
                $sheet->setFontFamily('新細明體');
                $feedata=DB::table('mod_order_fee')
                ->whereIn('ord_id', $ids )
                ->get();
                foreach ($feedata as $key => $row2) {
                    $order = OrderMgmtModel::find($row2->ord_id);
                    $owners = DB::table('sys_customers')->where("cust_no",$order->owner_cd)->where("g_key","SYL")->first();
                    if(isset($owners)){
                        $ownernm = $owners->cname;
                    }
                    $sheet->row(($key + 2), array(
                        $order->owner_nm,$order->ord_no,$row2->fee_nm,$row2->fee_descp,$row2->amount,
                    )
                );
                }
            });	
        })->store('xls',  base_path('/public/admin/report/') );
    }
    
    public function errorselect(Request $request){
        //異常回報
        $user     = Auth::user();
        $ids       = request('ids');
        $errorType = request('cd');
        try{
            if(count($ids) > 0) {
                $errorcount = DB::table('mod_order')
                ->whereIn('id', $ids)
                ->whereNotIn("status",["SETOFF","LOADING","PICKED"])
                ->count();
                if($errorcount >0){
                    return response()->json(['msg' => 'error','errorMsg'=>"包含非貨物裝載中、出發的資料"]);
                }
                for($i=0; $i<count($ids); $i++) {
                    $orderdata = OrderMgmtModel::find($ids[$i]);
                    $ordModel = new OrderMgmtModel;
                    $errorStatus = "ERROR";
                    $errordesc = "配送發生問題";
                    $abnormalRemark = "";
                    $sysOrdNo = $orderdata->sys_ord_no;
                    $dlvNo = $orderdata->dlv_no;
                    if($errorType=="A999"){
                        $errorStatus ="REJECT";
                        $errordesc = "拒收";
                    }
                    if($errorType=="PC14"||$errorType=="PC11"){
                        $errorStatus ="FINISHED";
                        $errordesc = "配送完成";
                    }
                    $pchomestatus = $errorStatus;
                        // $errorType
                        if($errorType=="PC02"){
                            $pchomestatus="ERROR";
                        }else if($errorType=="PC03"){
                            $pchomestatus="ERROR";
                        }else if($errorType=="PC04"){
                            $pchomestatus="ERROR";
                        }else if($errorType=="PC05"){
                            $pchomestatus="ERROR";
                        }else if($errorType=="PC07"){
                            $pchomestatus="ERROR";
                        }else if($errorType=="PC10"){
                            $pchomestatus="ERROR";
                        }else if($errorType=="PC11"){
                            $pchomestatus="FINISHED";
                            $errorStatus = "FINISHED";
                        }else if($errorType=="PC12"){
                            $pchomestatus="ERROR";
                        }else if($errorType=="PC13"){
                            $pchomestatus="ERROR";
                        }else if($errorType=="PC14"){
                            $pchomestatus="FINISHED";
                            $errorStatus = "FINISHED";
                        }

                    $thisQuery = DB::table('bscode');
                    $thisQuery->where('cd_type', "ERRORTYPE");
                    $thisQuery->where('cd', $errorType);
                    $thisQuery->where('g_key', $user->g_key);
                    $thisQuery->where('c_key', $user->c_key);
                    $bscode = $thisQuery->first();
                    $cdDescp = "";
                    if(isset($bscode)) {
                        $cdDescp = $bscode->cd_descp;
                    }
                    $TrackingModel = new TrackingModel();
                    if($errorStatus=="REJECT"){
                        DB::table('mod_order')
                        ->where('id', $orderdata->id)
                        ->update([
                            'status' => $errorStatus,
                            'status_desc' => $errordesc,
                            'error_remark' => $cdDescp, 
                            'exp_reason' => $errorType, 
                        ]);
                        $TrackingModel->insertTracking('', '', '', $sysOrdNo, '', 'E', 5, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
                    }
                    if($orderdata->owner_cd=="70552531"&& $errorStatus=="REJECT"){
                        $ordModel->insertmiedilog($sysOrdNo,$errorStatus);
                    }
                    //正物流
                    if($orderdata->owner_cd=="16606102"){
                        $ordModel->insertpchomelog($sysOrdNo,$pchomestatus,$errorType);
                    }
                    //逆物流
                    if($orderdata->owner_cd=="16606102"){
                        if($errorType== "PC005" ||$errorType=="A999"||$errorType=="A093" ){
                            $ordModel->insertpchomelogNOD($sysOrdNo,"05",$errorType,$abnormalRemark);
                        }
                    }
                    if(isset($orderdata)) {
                        $tsrecord = DB::table('mod_ts_order')
                        ->where('sys_ord_no', $sysOrdNo)
                        ->where('status_desc', $orderdata->status_desc)
                        ->where('owner_cd', $orderdata->owner_cd)
                        ->count();
                        $custdata = DB::table('sys_customers')
                        ->where('cust_no', $orderdata->owner_cd)
                        ->whereIn('ftptype',["E","EM"])
                        ->count();
                        if($custdata!=0 && $errorStatus!="REJECT"){
                            DB::table('mod_ts_order')
                            ->insert([
                                "owner_cd" => $orderdata->owner_cd,
                                "owner_nm" => $orderdata->owner_nm,
                                "ord_no"   => $orderdata->ord_no,
                                "cust_ord_no"   => $orderdata->cust_ord_no, 
                                "wms_ord_no"   => $orderdata->wms_order_no,
                                "sys_ord_no" => $orderdata->sys_ord_no,
                                "ord_created_at"   => $orderdata->created_at,
                                "status_desc" => $errordesc,
                                "finish_date"   =>\Carbon::now(),
                                "exp_reason" => $errorType,
                                "error_remark" => $cdDescp,
                                "status" => $errorStatus,
                                "abnormal_remark" => $abnormalRemark,
                                "is_send"     => "N",
                                "updated_at"  =>\Carbon::now(),
                                "updated_by"  => "SYSTEM",
                                "created_at"  => \Carbon::now(),
                                "created_by"  => "SYSTEM",
                                'g_key'      => $user->g_key,
                                'c_key'      => $user->c_key,
                                's_key'      => $user->s_key,
                                'd_key'      => $user->d_key,
                            ]);
                        }
                    }
                    
                    if($errorStatus=="REJECT"){
                        $ordModel->updateOrdStatusAndFinishDate($sysOrdNo, $errorStatus);
                    }
                    $pickstatus=DB::table('mod_dlv_plan')
                    ->where('dlv_no', $dlvNo)
                    ->where('ord_id',$orderdata->id)
                    ->where("dlv_type","P")
                    ->first();
                    if(isset($pickstatus)){
                        if($pickstatus->status=="FINISHED"){
                            DB::table('mod_order')
                            ->where('id', $orderdata->id)
                            ->update([
                                'dlv_type' => "P",
                            ]);
                        }
                    }
                    DB::table('mod_dlv_plan')
                    ->where('dlv_no', $dlvNo)
                    ->where('status',"!=", "FINISHED")
                    ->where('ord_id',$orderdata->id)
                    ->update([
                        'status' => $errorStatus,
                        'error_cd' => $errorType,
                        'error_descp' => $cdDescp,
                        'abnormal_remark' => $abnormalRemark,
                        'updated_at' => \Carbon::now(),
                        'finish_date' => \Carbon::now()
                    ]);


                    $cnt  = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->count();
                    $fCnt = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->whereRaw("(status = 'FINISHED' or status = 'ERROR' or status = 'REJECT')")->count();
                    if($cnt == $fCnt) {
                        DB::table('mod_dlv')
                            ->where('dlv_no', $dlvNo)
                            ->update(['status' => 'FINISHED','finished_time' => \Carbon::now()]);
                    }
                    DB::table('mod_order')
                    ->where('id', $orderdata->id)
                    ->update([
                        'status' => $errorStatus,
                        'status_desc' => $errordesc,
                        'error_remark' => $cdDescp, 
                        'exp_reason' => $errorType, 
                    ]);
                }
            }
        }catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return response()->json(['msg' => 'error','errorMsg'=>"操作失敗", 'error_log' => $e->getLine(), 'error_msg' => $e->getMessage()]);
        } 

        return response()->json(['msg' => 'success']);
    }
    public function sendMessage(Request $request){
        //簡訊 發送
        $user     = Auth::user();
        $ids      = request('ids');
        $cd     = request('cd');
        $response = "";
        $postData =array();
        //SysId 帳號
        //SrcAddress 用來發送訊息的來源位址(20個數字)
        //DestAddress 一個以上收件人門號。(6 ~ 20 個數字，不包含前置”+”)可一次發送訊息給多個門號，門號數目上限，請與遠傳聯繫.
        //SmsBody 簡訊內容 使用Base64編碼
        $xml_array="";
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                    $basestr ="";
                    $orddata = OrderMgmtModel::find($ids[$i]);
                    $newmessage = str_replace("{sys_ord_no}",$orddata->sys_ord_no,$cd);
                    // $newmessage = str_replace("{ord_no}",$orddata->ord_no,$cd);
                    $basestr= base64_encode($newmessage);
                    $strxml = "<?xml version='1.0' encoding='UTF-8'?>". 
                        "<SmsSubmitReq>".
                        "<SysId>SUNYOUNG</SysId >". 
                        "<SrcAddress>01916800061099700223</SrcAddress>".
                        "<DestAddress>".$orddata->dlv_tel."</DestAddress>". 
                        "<SmsBody>".$basestr."</SmsBody>".
                        "<DrFlag>true</DrFlag>".
                        "<LongSmsFlag>true</LongSmsFlag >". 
                        "</SmsSubmitReq>";
                    $postdata = urlencode($strxml);
                    // $curl = curl_init();
                    // curl_setopt_array($curl, array(
                    //     CURLOPT_URL => 'http://61.20.32.60:6600/mpushapi/smssubmit?xml='.$postdata,
                    //     CURLOPT_RETURNTRANSFER => true,
                    //     CURLOPT_ENCODING => '',
                    //     CURLOPT_MAXREDIRS => 10,
                    //     CURLOPT_TIMEOUT => 0,
                    //     CURLOPT_FOLLOWLOCATION => true,
                    //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    //     CURLOPT_CUSTOMREQUEST => 'POST',
                    //     CURLOPT_HTTPHEADER => array(
                    //       'Content-Type: application/x-www-form-urlencoded'
                    //     ),
                    // ));
                    // $response  = curl_exec($curl);
                    // $xml_array = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
                    $orddata->sendmessage ="已發送";
                    $orddata->save();
                    DB::table('mod_mpush_log')->insert([
                        'sys_ord_no' => $orddata->sys_ord_no,
                        'phone' => $orddata->dlv_tel,
                        'content' => $postdata,
                        'g_key' =>$user->g_key,
                        'c_key' =>$user->c_key,
                        's_key' =>$user->s_key,
                        'd_key' =>$user->s_key,
                        'created_by' => $user->email,
                        'created_at' => \Carbon::now(),
                        'updated_at' => \Carbon::now()
                    ]);
                    // curl_close($curl);
            }
        }
        return response()->json(['msg' => 'success']);
    }
    public function sendMessage2(Request $request){
        //發送簡訊 三竹版本
        $user     = Auth::user();
        $ids      = request('ids');
        $cd     = request('cd');
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $orddata = OrderMgmtModel::find($ids[$i]);
                $cd = str_replace("{sys_ord_no}",$orddata->sys_ord_no,$cd);
                $smbody = mb_convert_encoding($cd, "BIG5", "UTF-8");
                $data = array(
                    "username" => '52491528', //三竹帳號
                    "password" => 'a52491528', //三竹密碼
                    "dstaddr" => isset($orddata->dlv_tel) ? $orddata->dlv_tel : $orddata->dlv_tel, //客戶手機
                    "DestName" => '客戶', //對客戶的稱謂 於三竹後台看的時候用的
                    "smbody" =>$smbody, //簡訊內容
                );
                $dataString = http_build_query($data);
                $url = "http://smexpress.mitake.com.tw:9600/SmSendGet.asp?$dataString";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch);
                curl_close($ch);
            }
        }


        return response()->json(['msg' => 'success']);
    }
    public function feeexport(){
        //服務項目報表匯出
        $user = Auth::user();
        $today       = new \DateTime();
        $now = date('Ymd');
        \Log::info('產出excel');
        \Log::info($user->email);

        $logid =  DB::table('export_grid_excel')
        ->insertGetId([
            'gridtable'  => '配送費用明細',
            'g_key'      => $user->g_key,
            'c_key'      => $user->c_key,
            's_key'      => $user->s_key,
            'd_key'      => $user->d_key,
            'created_by' => $user->email,
            'updated_by' => $user->email,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);

        Excel::create("配送費用明細_".$now, function($excel) {
            $excel->sheet("服務項目", function($sheet)  {
                $ids = request('ids');

                $detailfeeoption = array();
                $detail =  DB::table('mod_order_detail')
                ->select('pkg_num', 'id')
                ->whereIn('ord_id', $ids)
                ->orderBy('ord_id')
                ->get();

                foreach($detail as $key => $row) {
                    for($i=1;$i<=$row->pkg_num ;$i++){
                        array_push($detailfeeoption,$row->id."-".$i);
                    }
                }

                $headers =  DB::table('mod_order_fee')
                ->select(
                    'fee_name','fee_cd'
                )
                ->whereIn('ord_id', $ids)
                ->orderBy('ord_id')
                ->groupBy('fee_cd')
                ->pluck('fee_name')->toArray();
                // 建單日期 完成日期 貨主 訂單號碼 客戶訂單號碼 
                // 提貨 配送 
                // ... 明細
                // 運費前面加 總費用 (運費+服務項目金額加總 by 訂單) 合併儲存格
                // $headerarray =array(
                //     "建單日期","完成日期","貨主","訂單號碼","客戶訂單號碼",
                //     "提貨客戶名稱","提貨客戶聯絡人","提貨客戶電話","提貨地址",
                //     "配送客戶名稱","配送客戶聯絡人","配送客戶電話","配送地址",
                //     "商品名稱","商品編號","總費用","運費","費用備註","回刷備註","服務項目總金額"
                // );

                //2022-02-18-增加欄位
                $headerarray =array(
                    "建單日期","完成日期","站別","貨主","訂單號碼","倉庫訂單號碼","客戶訂單號碼",
                    "提貨客戶名稱","提貨客戶聯絡人","提貨客戶電話","提貨地址",
                    "配送客戶名稱","配送客戶聯絡人","配送客戶電話","配送地址",
                    "商品名稱","商品編號",'數量','數量單位','材積',
                    "總費用","運費","備註","費用備註","料品類別","回刷備註","服務項目總金額"
                );


                $sheet->row(1, array_merge($headerarray,$headers));
                $ordid = "";
                $merge = "Y";
                foreach($detailfeeoption as $key=>$row) {
                    $body = array();
                    $detailid= explode(',', $row);
                    $detaildata =  DB::table('mod_order_detail')
                    ->select('ord_id', 'goods_nm', 'goods_no', 'pkg_unit', 'cbm', 'goods_type')
                    ->where('id',  $detailid[0])
                    ->first();
                    $ord =  DB::table('mod_order')
                    ->select(
                        'cust_amt', 'amt', 'remark', 'amt_remark', 'total_amount',
                        'created_at', 'finish_date', 's_key', 'owner_nm', 'ord_no', 'wms_order_no', 'cust_ord_no',
                        'pick_cust_nm', 'pick_attn', 'pick_tel', 'pick_addr',
                        'dlv_cust_nm', 'dlv_attn', 'dlv_tel', 'dlv_addr'
                    )
                    ->where('id',  $detaildata->ord_id)
                    ->first();

                    $count =  DB::table('mod_order_detail')
                    ->select(
                        DB::raw('SUM(pkg_num) as total_count')
                    )
                    ->where('ord_id',  $detaildata->ord_id)
                    ->first();
                    $mergecount =  "N";
                    if($ordid == ""){
                        $mergecount = "N";
                        $ordid = $detaildata->ord_id;
                        $merge = "Y";
                    }else if($ordid == $detaildata->ord_id) {
                        $mergecount = "Y";
                        $merge = "N";
                    }else{
                        $mergecount ="N";
                        $ordid = $detaildata->ord_id;
                        $merge = "Y";
                    }
                    $amt = "";
                    if($ord->cust_amt!=""){
                        $amt =$ord->cust_amt;
                    }else{
                        $amt =$ord->amt;
                    };
                    $totalamt = $ord->total_amount+$amt;
                    if($mergecount=="Y"){
                        $totalamt = 0;
                        $amt = 0;
                    }
                    array_push($body,
                        $ord->created_at,$ord->finish_date,$ord->s_key ,$ord->owner_nm,$ord->ord_no, $ord->wms_order_no, $ord->cust_ord_no,
                        $ord->pick_cust_nm,$ord->pick_attn,$ord->pick_tel,$ord->pick_addr,
                        $ord->dlv_cust_nm,$ord->dlv_attn,$ord->dlv_tel,$ord->dlv_addr,
                        $detaildata->goods_nm,$detaildata->goods_no,1,$detaildata->pkg_unit,$detaildata->cbm,
                        $totalamt,$amt,$ord->remark,$ord->amt_remark,$detaildata->goods_type
                    );

                    // array_push($body,
                    //     $ord->created_at,$ord->finish_date,$ord->owner_nm,$ord->ord_no,$ord->cust_ord_no,
                    //     $ord->pick_cust_nm,$ord->pick_attn,$ord->pick_tel,$ord->pick_addr,
                    //     $ord->dlv_cust_nm,$ord->dlv_attn,$ord->dlv_tel,$ord->dlv_addr,
                    //     $detaildata->goods_no,$detaildata->goods_nm,$totalamt,$amt,$ord->remark
                    // );

                    $detailid = "";
                    $feecd =  DB::table('mod_order_fee')
                    ->select(
                        'fee_cd'
                    )
                    ->whereIn('ord_id', $ids)
                    ->orderBy('ord_id')
                    ->groupBy('fee_cd')
                    ->pluck('fee_cd')->toArray();
                    $sumfee =  DB::table('mod_order_fee')
                    ->select(DB::raw('SUM(amount) as total_amount'))
                    ->where('prod_detail_id',$row)
                    ->first();
                    $feedescp =  DB::table('mod_order_fee')
                    ->select( 
                        DB::raw(" GROUP_CONCAT(fee_descp) as fee_descp" )
                    )
                    ->where('prod_detail_id',$row)
                    ->first();
                    if(isset( $feedescp)) {
                        array_push($body,str_replace(',','、',$feedescp->fee_descp) );
                    }else{
                        array_push($body,"");
                    }
                    if($sumfee->total_amount == null || $sumfee->total_amount ==""){
                        array_push($body,0);
                    }else{
                        array_push($body,$sumfee->total_amount);
                    }
                    foreach($feecd as $feekey=>$feerow) {

                        $rowdata =  DB::table('mod_order_fee')
                        ->select('amount')
                        ->where('fee_cd', $feerow)
                        ->where('prod_detail_id',$row)
                        ->where('ord_id',  $detaildata->ord_id)
                        ->first();
                        if(isset($rowdata)){
                            if($rowdata->amount== null || $rowdata->amount==""){
                                array_push($body,0);
                            }else{
                                array_push($body,$rowdata->amount);
                            }
                        }else{
                            array_push($body,0);
                        }
                    }

                    $sheet->row($key+2, $body);
                    $sheet->setCellValueExplicit('Q'.($key+2),$body[16], \PHPExcel_Cell_DataType::TYPE_STRING);
                    
                    if($merge =="Y"){
                        $minuscount = $key + 2;
                        $sheet->mergeCells('U'.$minuscount.":".'U'.($key+1+$count->total_count));
                        $sheet->mergeCells('V'.$minuscount.":".'V'.($key+1+$count->total_count));
                    }
                    $sheet->cell('U2:U'.($key+2),function($cell){
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('V2:V'.($key+2),function($cell){
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                }
            });
            $excel->sheet("序號", function ($sheet)  {
                $ids = request('ids');
                $totalcount = 0;
                $orddata = DB::table('mod_order')
                ->select('ord_no', 'id')
                ->whereIn('id', $ids )
                ->get();
                // 貨主名稱 訂單號碼 服務項目名稱 敘述 費用
                $sheet->row(1, array(
                    '訂單號碼','商品料號','序號'
                ));
                $sheet->setFontFamily('新細明體');

                foreach ($orddata as $key => $row) {
                    $detail =  DB::table('mod_order_detail')
                    ->select('sn_no', 'goods_no')
                    ->where('ord_id', $row->id)
                    ->get();

                    foreach($detail as $key => $row2) {
                        $snarray = array("");

                        $snlist = explode(",",$row2->sn_no);

                        for($i=0;$i<count($snlist) ;$i++){
                            $sheet->row(($totalcount + 2), array($row->ord_no,$row2->goods_no,$snlist[$i])
                            );
                            $sheet->setCellValueExplicit('B'.($totalcount+2),$row2->goods_no, \PHPExcel_Cell_DataType::TYPE_STRING);
                            $totalcount ++;
                        }
                    }

                }
            });	
        })->store('xls', storage_path('app/excel'));


        DB::table('export_grid_excel')
        ->where('id', $logid)
        ->update([
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);


        return [
            "msg"=>"success", "downlink"=>"配送費用明細_".$now.".xls"
        ];;   

    }

    public function DeatailifeeGet($ord_id=null) 
    {
        $user = Auth::user();
        $orderDeta = array();
        $feedata = array();
        if($ord_id != null) {
            $feedata =DB::table('mod_order_feedetail')->where('ord_id', $ord_id)->get();
        }
        $data[] = array(
            'Rows' => $feedata,
        );
        return response()->json($data);
    }
    public function Deatailimgdel($id) 
    {
        $user = Auth::user();
        $imgdata =DB::table('mod_file')->where('id', $id)->first();
        $ref_no = $imgdata->ref_no  ;
        $fileName = $imgdata->guid  ;
        DB::table('mod_file')->where('id', $id)->delete();   
        $filecount_f= DB::table('mod_file')->where('ref_no', $ref_no)->whereIn('type_no', ['FINISH','SIGNIN'])->count();
        $filecount_e= DB::table('mod_file')->where('ref_no', $ref_no)->where('type_no', 'ERROR')->count();
        if($filecount_f==0){
            DB::table('mod_order')
            ->where('sys_ord_no', $ref_no)
            ->update(['sign_pic' => '0']);
        }
        if($filecount_e==0){
            DB::table('mod_order')
            ->where('sys_ord_no', $ref_no)
            ->update(['error_pic' => '0']);
        }
        Storage::disk('local')->delete($fileName);
        return ["msg"=>"success"];
    }

    public function DeatailimgGet($ord_id=null) 
    {
        $user = Auth::user();
        $orderDeta = array();
        $imgdata = array();
        if($ord_id != null) {
            $orderDeta=DB::table('mod_order')->where('id', $ord_id)->first();
            
            // $ogfileData = DB::table('mod_file')
            // ->select( DB::raw("(select id from syltms.mod_file b where b.guid=mod_file.guid limit 1)as id ") )
            // ->where('ref_no', $orderDeta->sys_ord_no)
            // ->where('empty_img', 'Y')
            // ->groupBy("guid")
            // ->pluck("id")->toArray();
            // dd($ogfileData);
            $imgdata =DB::table('mod_file_view')
            ->where('ref_no', $orderDeta->sys_ord_no)
            ->where('empty_img', 'Y')
            ->orderby('created_at','asc')->get();
        }
        $data[] = array(
            'Rows' => $imgdata,
        );
        return response()->json($data);
    }

    public function ordertallyGet($ord_id=null) 
    {
        $user = Auth::user();
        $orderDeta = array();
        $imgdata = array();
        if($ord_id != null) {
            $orderDeta=DB::table('mod_order_goods_check')->where('ord_id', $ord_id)->orderby('created_at','desc')->get();
        }
        $data[] = array(
            'Rows' => $orderDeta,
        );
        return response()->json($data);
    }

    public function DeatailamtGet($ord_id=null) 
    {
        $orderDetail = [];
        if($ord_id != null) {
            $this_query = DB::table('mod_order_fee');
            $this_query->where('ord_id', $ord_id);
            $orderDetail = $this_query->get();
            
            // DB::table('users')
            // ->select(DB::raw('ROW_NUMBER() OVER(ORDER BY ID DESC) AS Row, status'))
            // ->where('status', '<>', 1)
            // ->groupBy('status')
            // ->get();
        }
        
        $data[] = array(
            'Rows' => $orderDetail,
        );
        return response()->json($data);
    }

    public function Deatailamtdelall($lastid)
    {
        DB::table('mod_order_fee')
        ->where('ord_id', $lastid)
        ->delete();
        return ["msg"=>"success"];
    }
    public function Deataildelall($lastid)
    {
        DB::table('mod_order_detail')
        ->where('ord_id', $lastid)
        ->delete();
        return ["msg"=>"success"];
    }
    public function DeatailamtStore_new(Request $request)
    {
        $user = Auth::user();
        $request = json_decode($request->getContent());
        $commonFunc = new CommonModel;
        $orderPack = new OrderamtDetailModel;
        $data = array();
        DB::table('mod_order_fee')
        ->where('ord_id', $request[0]->ord_id)
        ->delete();
        for($i=0; $i<count($request); $i++) {
            $data = [
                'ord_id' => $request[0]->ord_id,
                'fee_cd' => $request[$i]->fee_cd,
                'fee_descp' => $request[$i]->fee_descp,
                'fee_name' => $request[$i]->fee_name,
                'amount' => $request[$i]->amount,
                'g_key' => $user->g_key,
                'c_key' => $user->c_key,
                's_key' => $user->s_key,
                'd_key' => $user->d_key,
                'created_by' => $user->email,
                'updated_by' => $user->email,
                'created_at' => \Carbon::now(),
                'updated_at' => \Carbon::now(),
            ];
            $orderPack->insertfee($data);
        }
        return ["msg"=>"success", "data"=>$orderPack->where('ord_id', $request[0]->ord_id)->get()];
    }
    public function DeatailStore_new(Request $request)
    {
        $user = Auth::user();
        $request = json_decode($request->getContent());
        $commonFunc = new CommonModel;
        $orderPack = new OrderDetailModel;
        $data = array();
        $ord_data = DB::table('mod_order')->where('id', $request[0]->ord_id)->first();
        DB::table('mod_order_detail')
        ->where('ord_id', $request[0]->ord_id)
        ->delete();
        for($i=0; $i<count($request); $i++) {
            DB::table('mod_order_detail')->insert([
                'ord_id' => $request[$i]->ord_id,
                'ord_no' => $ord_data->ord_no,
                'goods_no' => $request[$i]->goods_no,
                'goods_no2' => $request[$i]->goods_no2,
                'goods_nm' => $request[$i]->goods_nm,
                'pkg_num' => $request[$i]->pkg_num,
                'gw' => $request[$i]->gw,
                'gwu' => $request[$i]->gwu,
                'cbm' => $request[$i]->cbm,
                'cbmu' => $request[$i]->cbmu,
                'length' => $request[$i]->length,
                'weight' => $request[$i]->weight,
                'height' => $request[$i]->height,
                'sn_no' => $request[$i]->sn_no,
                'pkg_num' => $request[$i]->pkg_num,
                'g_key' => $user->g_key,
                'c_key' => $user->c_key,
                's_key' => $user->s_key,
                'd_key' => $user->d_key,
                'created_by' => $user->email,
                'updated_by' => $user->email,
                'created_at' => \Carbon::now(),
                'updated_at' => \Carbon::now(),
            ]);
        }
        return ["msg"=>"success", "data"=>$orderPack->where('ord_id', $request[0]->ord_id)->get()];
    }

    public function DeatailamtStore(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);        
        $commonFunc = new CommonModel;
        $request    = $commonFunc->processData($request, $this->crud->create_fields);
        if($request->fee_cd == "" || $request->fee_cd == null) {
            $request->merge(array('fee_cd' => $request->fee_name));
        }
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderPack = new OrderamtDetailModel;
            foreach($request->all() as $key=>$val) {
                $orderPack[$key] = request($key);
            }
            $orderPack->save();
            DB::table('mod_order_feedetail')
            ->insert([
                'ord_id' => $request->ord_id,
                'detail_id' => $orderPack->id,
                'fee_cd' => $request->fee_cd,
                'fee_nm' => $request->fee_name,
                'fee_type_cd' => "c",
                'fee_type_nm' => "服務項目",
                'price' => $request->amount,
                'g_key' => $user->g_key,
                'c_key' => $user->c_key,
                's_key' => $user->s_key,
                'd_key' => $user->d_key,
                'created_by' => $user->email,
                'created_at' => \Carbon::now()]
            );
            $this->updateModOrderamt($orderPack->ord_id);
        }
        $orderdata = DB::connection('mysql::write')->table('mod_order')->where('id', $orderPack->ord_id)->first();
        return ["msg"=>"success","maindata"=>$orderdata, "data"=>$orderPack->where('id', $orderPack->id)->get()];
    }

    public function DeatailamtUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);
        $commonFunc = new CommonModel;
        $request    = $commonFunc->processData($request, $this->crud->create_fields);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderPack = OrderamtDetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $orderPack[$key] = request($key);
            }
            $orderPack->save();
            $this->updateModOrderamt($orderPack->ord_id);
        }
        $detailData = DB::table('mod_order_fee')
        ->select(DB::raw('SUM(amount) as amt'))
        ->where('ord_id', $orderPack->ord_id)
        ->first();
        $orderdata = DB::connection('mysql::write')->table('mod_order')->where('id', $orderPack->ord_id)->first();
        return ["msg"=>"success","maindata"=>$orderdata, "data"=> DB::table('mod_order_fee')->where('id', $request->id)->get(), "amt"=>$detailData->amt];
    }

    private function updateModOrderamt($ord_id) {
        $detailData = DB::connection('mysql::write')->table('mod_order_fee')
                        ->select(DB::raw('SUM(amount) as amt'))
                        ->where('ord_id', $ord_id)
                        ->first();
        if(isset($detailData)) {
            $pkgUnit = null;
            OrderMgmtModel::where('id', $ord_id)
                        ->update(['total_amount' => $detailData->amt]);
        }else{
            OrderMgmtModel::where('id', $ord_id)
                        ->update(['total_amount' => 0]);
        }

        return;
    }
    public function DeatailiDel($id)
    {
        $orderPack = OrderamtDetailModel::find($id);
        $orderPack->delete();        
        $this->updateModOrderamt($orderPack->ord_id);
        $orderdata = DB::connection('mysql::write')->table('mod_order')->where('id', $orderPack->ord_id)->first();
        return ["msg"=>"success","maindata"=>$orderdata];
    }

    public function multiClose() {
        $user = Auth::user();
        $ids = request('ids');
        $OrderCloseMgmtModel = new OrderCloseMgmtModel();
        try{
            if(count($ids) > 0) {
                for($i=0; $i<count($ids); $i++) {
                    $order = OrderMgmtModel::find($ids[$i]);
                    $data = [
                        'ord_id' => $order->id,
                        'g_key' => $order->g_key,
                        'c_key' => $order->c_key,
                        's_key' => $order->s_key,
                        'd_key' => $order->d_key,
                        'ord_no' => $order->ord_no,
                        'status' => $order->status,
                        'loc_cros' => $order->loc_cros,
                        'remark' => $order->remark,
                        'dlv_cust_nm' => $order->dlv_cust_nm,
                        'dlv_cust_no' => $order->dlv_cust_no,
                        'dlv_addr' => $order->dlv_addr,
                        'dlv_attn' => $order->dlv_attn,
                        'dlv_tel' => $order->dlv_tel,
                        'dlv_tel2' => $order->dlv_tel2,
                        'dlv_city_no' => $order->dlv_city_no,
                        'dlv_city_nm' => $order->dlv_city_nm,
                        'dlv_zip' => $order->dlv_zip,
                        'dlv_area_id' => $order->dlv_area_id,
                        'dlv_area_nm' => $order->dlv_area_nm,
                        'dlv_lat' => $order->dlv_lat,
                        'dlv_lng' => $order->dlv_lng,
                        'pkg_num' => $order->pkg_num,
                        'pkg_unit' => $order->pkg_unit,
                        'etd' => $order->etd,
                        'truck_cmp_no' => $order->truck_cmp_no,
                        'truck_cmp_nm' => $order->truck_cmp_nm,
                        'truck_no' => $order->truck_no,
                        'driver' => $order->driver,
                        'exp_reason' => $order->exp_reason,
                        'exp_type' => $order->exp_type,
                        'updated_by' => $order->updated_by,
                        'created_by' => $order->created_by,
                        'created_at' => $order->created_at,
                        'updated_at' => $order->updated_at,
                        'cbm' => $order->cbm,
                        'dlv_no' => $order->dlv_no,
                        'car_no' => $order->car_no,
                        'trs_mode' => $order->trs_mode,
                        'pick_cust_no' => $order->pick_cust_no,
                        'pick_cust_nm' => $order->pick_cust_nm,
                        'pick_addr' => $order->pick_addr,
                        'pick_attn' => $order->pick_attn,
                        'pick_tel' => $order->pick_tel,
                        'pick_tel2' => $order->pick_tel2,
                        'pick_city_no' => $order->pick_city_no,
                        'pick_city_nm' => $order->pick_city_nm,
                        'pick_zip' => $order->pick_zip,
                        'pick_area_id' => $order->pick_area_id,
                        'pick_area_nm' => $order->pick_area_nm,
                        'pick_lat' => $order->pick_lat,
                        'pick_lng' => $order->pick_lng,
                        'total_cbm' => $order->total_cbm,
                        'total_cbm_unit' => $order->total_cbm_unit,
                        'dlv_email' => $order->dlv_email,
                        'pick_email' => $order->pick_email,
                        'distance' => $order->distance,
                        'car_type' => $order->car_type,
                        'dlv_type' => $order->dlv_type,
                        'amt' => $order->amt,
                        'total_gw' => $order->total_gw,
                        'cust_amt' => $order->cust_amt,
                        'amt_remark' => $order->amt_remark,
                        'ord_type_cd' => $order->ord_type_cd,
                        'ord_type_nm' => $order->ord_type_nm,
                        'wh_addr' => $order->wh_addr,
                        'owner_cd' => $order->owner_cd,
                        'owner_nm' => $order->owner_nm,
                        'sys_ord_no' => $order->sys_ord_no,
                        'error_remark' => $order->error_remark,
                        'dlv_remark' => $order->dlv_remark,
                        'pick_remark' => $order->pick_remark,
                        'finish_date' => $order->finish_date,
                        'owner_send_mail' => $order->owner_send_mail,
                        'pick_send_mail' => $order->pick_send_mail,
                        'dlv_send_mail' => $order->dlv_send_mail,
                        'sign_pic' => $order->sign_pic,
                        'error_pic' => $order->error_pic,
                        'sys_etd' => $order->sys_etd,
                        'cust_ord_no' => $order->cust_ord_no,
                        'is_urgent' => $order->is_urgent,
                        'temperate' => $order->temperate,
                        'status_desc' => $order->status_desc,
                        'temperate_desc' => $order->temperate_desc,
                        'trs_mode_desc' => $order->trs_mode_desc,
                        'car_type_desc' => $order->car_type_desc,
                        'abnormal_remark' => $order->abnormal_remark,
                        'pick_addr_info' => $order->pick_addr_info,
                        'dlv_addr_info' => $order->dlv_addr_info,
                        'collectamt' => $order->collectamt,
                        'realnum' => $order->realnum,
                        'is_ordered' => $order->is_ordered,
                        'order_tally' => $order->order_tally,
                        'wms_order_no' => $order->wms_order_no,
                        'close_by' => $user->email,
                        'close_date' => \Carbon::now(),
                        'close_status' => 'Y',
                        'sendsyl' => $order->sendsyl,
                        'imgnum' => $order->imgnum,
                    ];
                    $OrderCloseMgmtModel->insertClose($data);
                    $order->delete();
                }
            }
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        return response()->json(array('msg' => 'success'));
    }
    
    public function changeReady(Request $request){
        $user = Auth::user();
        $ids = request('ids');
        try{
			//判斷數量 尚未安排,已提貨兩種狀態的數量
			$checkstatsCount = DB::connection('mysql::write')
			->table('mod_order')
			->whereNotIn('status', ['UNTREATED', 'PICKED'])
			->whereIn('id', $ids)
			->count();
			if($checkstatsCount > 0 ) {
				$errorordNos = DB::connection('mysql::write')
				->table('mod_order')
				->whereNotIn('status', ['UNTREATED', 'PICKED'])
				->whereIn('id', $ids)
				->pluck('ord_no')
				->toArray();

				$msg = implode('，', $errorordNos) ;
				return ["msg"=>"error", "errorMsg"=>$msg.' 狀態不符合' ];
			}

			// 判斷已提貨 但還是有配送單號的車子
			$checkDlvNos = DB::connection('mysql::write')
			->table('mod_order')
			->select('ord_no', 'dlv_no')
			->where('status', 'PICKED')
			->whereIn('id', $ids)
			->get();
			foreach ($checkDlvNos as $key => $row) {
				$notEmptyDlvNo = array();
				if($row->dlv_no != null ) {
					$notEmptyDlvNo[] = $row->ord_no;
				}
				if(is_array($notEmptyDlvNo) && count($notEmptyDlvNo)>0 ) {
					$msg = implode('，', $notEmptyDlvNo) ;
					return ["msg"=>"error", "errorMsg"=>$msg.' 配送單號不為空' ];
				}
			}
            
			DB::table('mod_order')
			->where('status', 'UNTREATED')
			->whereIn('id', $ids)
			->update([
				'status'      => 'READY',
				'status_desc' => '可派車',
				'updated_at'  => \Carbon::now(),
				'updated_by'  => $user->email,
			]);

			DB::table('mod_order')
			->where('status', 'PICKED')
			->whereNull('dlv_no')
			->whereIn('id', $ids)
			->update([
				'status'      => 'READY_PICK',
				'status_desc' => '已提貨可派車',
				'updated_at'  => \Carbon::now(),
				'updated_by'  => $user->email,
			]);

        }catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }

        return response()->json(array('msg' => 'success'));
    }
}
