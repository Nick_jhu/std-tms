<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\SysLoginLogModel;
class ApiController extends Controller
{
    use  AuthenticatesUsers;

public function __construct()
{
    $this->middleware('api');
}

//调用认证接口获取授权码
protected function authenticateClient(Request $request)
{
    $credentials = $this->credentials($request);

    $data = $request->all();

    $request->request->add([
        'grant_type' => $data['grant_type'],
        'client_id' => $data['client_id'],
        'client_secret' => $data['client_secret'],
        'username' => $credentials['email'],
        'password' => $credentials['password'],
        'scope' => ''
    ]);

    $proxy = Request::create(
        'oauth/token',
        'POST'
    );

    $response = \Route::dispatch($proxy);
    $data[] = array();
    $sample_detail = array();
    if($response->status()=="200")
    {
        $sample_detail = array(
            "data"           => null,
            "token_type"     => null,
            "access_token"   => null,
            "refresh_time"   => null,
            "errorcd"        => null,
            "gps_time"       => null,
            "img_limit"      => null,
            "refresh_token"  => null,
            "app_permission" => null,
        );        
        $this_query = DB::table('users');
        $this_query->select('id', 'name', 'email', 'remember_token', 'phone', 'identity', 'online');
        $this_query->where('email', $credentials['email']);
        $sample_detail["data"] = $this_query->get();
            
        
        // $data[] = array(
        //     'Rows' => $sample_detail,
        // );

        //return response()->json($data);
        $obj = json_decode($response->content());
        $sample_detail["token_type"] = $obj->token_type;
        $sample_detail["access_token"] = $obj->access_token;
        $sample_detail["refresh_token"] = $obj->refresh_token;
        $users = DB::table('users')->select('g_key', 'c_key','s_key','d_key','id')->where('email', $credentials['email'])->first();

        $appver = DB::table('bscode')->where('cd_type','APPVERSION')->where('g_key',$users->g_key)->first();
        if($request->appversion != null){
            if($request->appversion != $appver->value1){
                return ["msg"=>"版本不相符 請更新app版本","url"=>$appver->cd_descp,"errorcd"=>"-1"];
            }
        }
        \Log::info($request->appversion);
        \Log::info('login appversion');

        $refresh_time = DB::table('bscode')->where('cd_type','REPASSTIME')->where('g_key',$users->g_key)->first();
        $gps_time = DB::table('bscode')->where('cd_type','SENDGPSTIME')->where('g_key',$users->g_key)->first();

        $permission = DB::table('permission_users')->where('user_id',$users->id)->pluck('permission_id')->toArray();
        $app= DB::table('permissions')->whereIn('id', $permission)->where('id','>','50')->pluck('name');

        $imglimit = DB::table('bscode')->where('g_key',$users->g_key)->where('cd_type', 'UPLOADIMGLIMIT')->where('cd', 'uploadimglimit')->value('value1');

        $sample_detail["errorcd"] =  "0";
        $sample_detail["app_permission"] =  $app;
        $sample_detail["refresh_time"] = $refresh_time->value1;
        $sample_detail["gps_time"] = $gps_time->value1;

        $sample_detail["img_limit"] = (int)$imglimit;
        //寫入log
        $sysLoginLog = new SysLoginLogModel;
        // $sysLoginLog->name    = $data['name'];
        $sysLoginLog->car_no    = $data['email'];
        // $sysLoginLog->phone    = $data['phone'];
        $sysLoginLog->login_time = date('Y-m-d H:i:s');
        $sysLoginLog->g_key = $users->g_key;
        $sysLoginLog->c_key = $users->c_key;
        $sysLoginLog->s_key = $users->s_key;
        $sysLoginLog->d_key = $users->d_key;        
        $sysLoginLog->save();
    }
    
    
    return ["msg"=>"success","data"=>response()->json($sample_detail)];
    //return $response;
}
//以下为重写部分
protected function authenticated(Request $request)
{
    return $this->authenticateClient($request);
}

protected function sendLoginResponse(Request $request)
{
    $this->clearLoginAttempts($request);

    return $this->authenticated($request);
}

    protected function sendFailedLoginResponse(Request $request)
    {
        $msg = $request['errors'];
        $code = $request['code'];
        return ["msg"=>"error","data"=>$msg];
    }
    public function logout()
    {
        if (\Auth::guard('admin_api')->check()) {
            $user = \Auth::guard('admin_api')->user();
            DB::table('users')->where('id', $user->id)->update(['d_token' => null, 'online' => 0]);

            \Auth::guard('admin_api')->user()->token()->delete();
        }
        

        return ['message' => 'success',  'data' => '登出成功'];
    }
}