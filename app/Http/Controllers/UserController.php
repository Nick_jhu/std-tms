<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Backpack\PermissionManager\app\Http\Controllers\UserCrudController;
class UserController extends UserCrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel(config('backpack.permissionmanager.user_model'));
        $this->crud->setEntityNameStrings(trans('backpack::permissionmanager.user'), trans('backpack::permissionmanager.users'));
        $this->crud->setRoute(config('backpack.base.route_prefix').'/user');
        $this->crud->enableAjaxTable();

        // Columns.
        $this->crud->setColumns([
            [
                'name'  => 'name',
                'label' => trans('backpack::permissionmanager.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => trans('backpack::permissionmanager.email'),
                //'type'  => 'email',
                'type'  => 'text',
            ],
            [
                'name'  => 's_key',
                'label' => trans('auth.sKey'),
                'type'  => 'text',
            ],
            [ // select_from_array
                'name' => 'identity',
                'label' => trans('auth.identity'),
                'type' => 'radio',
                'options' => ['G' => '集團', 'C' => '公司', 'S' => '站別', 'D' => '部門'],
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [ // n-n relationship (with pivot table)
               'label'     => trans('backpack::permissionmanager.roles'), // Table column heading
               'type'      => 'select_multiple',
               'name'      => 'roles', // the method that defines the relationship in your Model
               'entity'    => 'roles', // the method that defines the relationship in your Model
               'attribute' => 'name', // foreign key attribute that is shown to user
               'model'     => config('laravel-permission.models.role'), // foreign key model
            ],
            [ // n-n relationship (with pivot table)
               'label'     => trans('backpack::permissionmanager.extra_permissions'), // Table column heading
               'type'      => 'select_multiple',
               'name'      => 'permissions', // the method that defines the relationship in your Model
               'entity'    => 'permissions', // the method that defines the relationship in your Model
               'attribute' => 'show_name', // foreign key attribute that is shown to user
               'model'     => config('laravel-permission.models.permission'), // foreign key model
            ],
            [ // select_from_array
                'name' => 'account_enable',
                'label' => trans('auth.account_enable'),
                'type' => 'radio',
                'options' => ['Y' => '是', 'N' => '否'],
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
        ]);

        // Fields
        $this->crud->addFields([
            [
                'name'  => 'name',
                'label' => trans('backpack::permissionmanager.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => trans('backpack::permissionmanager.email'),
                'type'  => 'text',
            ],
            [
                'name'  => 'password',
                'label' => trans('backpack::permissionmanager.password'),
                'type'  => 'password',
            ],
            [
                'name'  => 'password_confirmation',
                'label' => trans('backpack::permissionmanager.password_confirmation'),
                'type'  => 'password',
            ],
            [ // select_from_array
                'name' => 'identity',
                'label' => "身份",
                'type' => 'select_from_array',
                'options' => ['G' => '集團', 'C' => '公司', 'S' => '站別', 'D' => '部門'],
                'allows_null' => true,
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [ // select_from_array
                'name' => 'account_enable',
                'label' => "啟用",
                'type' => 'select_from_array',
                'options' => ['Y' => '是', 'N' => '否'],
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [
                'name'  => 'g_key',
                'label' => trans('backpack::permissionmanager.gkey'),
                'type'  => 'text',
            ],
            [
                'name'  => 'c_key',
                'label' => trans('backpack::permissionmanager.ckey'),
                'type'  => 'text',
            ],
            [
                'name'  => 's_key',
                'label' => trans('backpack::permissionmanager.skey'),
                'type'  => 'text',
            ],
            [
                'name'  => 'd_key',
                'label' => trans('backpack::permissionmanager.dkey'),
                'type'  => 'text',
            ],
            [
                'name' => 'session_id',
                'type' => 'hidden',
                'value' => 'session'
            ],
            [
            // two interconnected entities
            'label'             => trans('backpack::permissionmanager.user_role_permission'),
            'field_unique_name' => 'user_role_permission',
            'type'              => 'checklist_dependency',
            'name'              => 'roles_and_permissions', // the methods that defines the relationship in your Model
            'subfields'         => [
                    'primary' => [
                        'label'            => trans('backpack::permissionmanager.roles'),
                        'name'             => 'roles', // the method that defines the relationship in your Model
                        'entity'           => 'roles', // the method that defines the relationship in your Model
                        'entity_secondary' => 'permissions', // the method that defines the relationship in your Model
                        'attribute'        => 'name', // foreign key attribute that is shown to user
                        'model'            => config('laravel-permission.models.role'), // foreign key model
                        'pivot'            => true, // on create&update, do you need to add/delete pivot table entries?]
                        'number_columns'   => 3, //can be 1,2,3,4,6
                    ],
                    'secondary' => [
                        'label'          => ucfirst(trans('backpack::permissionmanager.permission_singular')),
                        'name'           => 'permissions', // the method that defines the relationship in your Model
                        'entity'         => 'permissions', // the method that defines the relationship in your Model
                        'entity_primary' => 'roles', // the method that defines the relationship in your Model
                        'attribute'      => 'show_name', // foreign key attribute that is shown to user
                        'model'          => "Backpack\PermissionManager\app\Models\Permission", // foreign key model
                        'pivot'          => true, // on create&update, do you need to add/delete pivot table entries?]
                        'number_columns' => 3, //can be 1,2,3,4,6
                    ],
                ],
            ],
        ]);
    }

}
