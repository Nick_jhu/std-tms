<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Models\CustomerModel;
use App\Models\SampleDetailModel;
use App\Models\BaseModel;
use App\Models\CommonModel;
use App\Models\autosendModel;
use App\Models\autosenddetailModel;
use App\Http\Requests\AutoSendCrudRequest as StoreRequest;
use App\Http\Requests\AutoSendCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store as Session;

class autosendProfileCrudController extends CrudController
{
    
    public function setup() {
        $user = Auth::user();
        $this->crud->setModel("App\Models\autosendModel");
        $this->crud->setEntityNameStrings("自動派車建檔", "自動派車建檔");
        $this->crud->setRoute(config('backpack.base.route_prefix').'/autosendcar');
    
        $this->crud->setColumns(['name']);

        
        $this->crud->setCreateView('autosend.edit');
        $this->crud->setEditView('autosend.edit');
        $this->crud->setListView('autosend.index');
        $this->crud->enableAjaxTable();


        $this->crud->addField([
            'name' => 'name',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'car_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'zip_code',
            'type' => 'select2_multiple',
            'options' => DB::table('sys_area')->select('dist_cd as code', DB::raw("CONCAT(dist_cd,city_nm,dist_nm) as descp") )
                            ->where('g_key', $user->g_key)
                            ->where('c_key', $user->c_key)
                            ->get()
        ]);

        $this->crud->addField([
            'name' => 'zip_name',
            'type' => 'text',
        ]);

       
        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'updated_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);


        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 's_key',
            'type' => 'hidden'
        ]);
        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'hidden'
        ]);
    }    
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        try{
            if(Auth::user()->hasPermissionTo('Autosendcar'))
            {
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function store(StoreRequest $request)
	{
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        
        //dd($request->all());
        try {
            $response = parent::storeCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $zip  = $request->zip_code;
        $zipnm  = "";
        if($zip!=""){
            $explodezip= explode(',', $zip);
            foreach($explodezip as $row ){
                $zipdata = DB::table('sys_area')->where("g_key",$user->g_key)->where("dist_cd",$row)->first();
                if(isset($zipdata)){
                    $zipnm .= $zipdata->city_nm.$zipdata->dist_nm.",";
                }
            }
        }
        $request->merge(['zip_name' => $zipnm]);
        try {
            $response = parent::updateCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response];
    }

    public function multiDel() {
        $user = Auth::user();
        $ids = request('ids');
        
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $car = autosendModel::find($ids[$i]);
                $Detail = autosenddetailModel::where('auto_send_id',$car->id);
                $Detail->delete();
                $car->delete();
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function get($ord_id=null) {
        $orderDetail = [];
        if($ord_id != null) {
            $this_query = DB::table('mod_auto_send_detail');
            $this_query->where('auto_send_id', $ord_id);
            $orderDetail = $this_query->get();
            
        }
        $data[] = array(
            'Rows' => $orderDetail,
        );

        return response()->json($data);
    }
    public function detailStore(Request $request)
    {
        $user = Auth::user();
        $count = DB::table('mod_auto_send_detail')
        ->where("auto_send_id",$request->auto_send_id)
        ->where("car_no",$request->car_no)
        ->count();
        if($count >0){
            return ["msg"=>"error", "errormsg"=>"此車號已經存在"];
        }
        $orderDetail = new autosenddetailModel;
        foreach($request->all() as $key=>$val) {
            $orderDetail[$key] = request($key);
        }
        $orderDetail->g_key= $user->g_key;
        $orderDetail->c_key= $user->c_key;
        $orderDetail->s_key= $user->s_key;
        $orderDetail->d_key= $user->d_key;
        
        $orderDetail->created_by= $user->email;
        $orderDetail->updated_by= $user->email;
        $orderDetail->save();

        return ["msg"=>"success", "data"=>$orderDetail->where('id', $orderDetail->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $user = Auth::user();
        $now = date("Y-m-d H:i:s");
        $count = DB::table('mod_auto_send_detail')
        ->where("auto_send_id",$request->auto_send_id)
        ->where("car_no",$request->car_no)
        ->count();
        if($count >0){
            return ["msg"=>"error", "errormsg"=>"此車號已經存在"];
        }
        $orderDetail = autosenddetailModel::find($request->id);
        foreach($request->all() as $key=>$val) {
            $orderDetail[$key] = request($key);
        }
        $orderDetail->updated_by= $user->email;
        $orderDetail->updated_at= $now;
        $orderDetail->save();

        return ["msg"=>"success", "data"=>$orderDetail->where('id', $request->id)->get()];
    }

    public function detailDel($id)
    {
        $orderDetail = autosenddetailModel::find($id);
        $orderDetail->delete();
        return ["msg"=>"success"];
    }
}
