<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\UploadedFile;
use Storage;
use File;
use Response;
use Illuminate\Support\Facades\Input;
use App\Models\OrderMgmtModel;
use App\Models\OrderCloseMgmtModel;
use App\Models\OrderDetailModel;
use App\Models\OrderamtDetailModel;
use App\Models\OrderPackModel;
use App\Models\TransRecordModel;
use App\Models\OrderPackDetailModel;
use App\Models\BaseModel;
use App\Models\CommonModel;
use App\Models\TrackingModel;
use App\Models\GoogleMapModel;
use App\Models\CalculateModel;

use Illuminate\Session\Store as Session;

use App\Http\Requests\OrderMgmtCrudRequest as StoreRequest;
use App\Http\Requests\OrderMgmtCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;

class OrderMgmtcloseCrudController extends CrudController
{
    
    public function setup() {
        $this->crud->setModel("App\Models\OrderCloseMgmtModel");
        $this->crud->setEntityNameStrings(trans('modOrder.closetitleName'), trans('modOrder.closetitleName'));
        $this->crud->setRoute(config('backpack.base.route_prefix').'/OrderMgmtclose');
        $this->crud->pmsName = "modOrderclose";
        
        $this->crud->setCreateView('orderclose.edit');
        $this->crud->setEditView('orderclose.edit');
        $this->crud->setListView('orderclose.index');
        //$this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'ord_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'status',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'trs_mode_desc',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'order_tally',
            'type' => 'text'
        ]);
        // $this->crud->addField([
        //     'name' => 'trs_mode',
        //     'type' => 'lookup',
        //     'title' => '運輸類型查詢',
        //     'info1' => Crypt::encrypt('bscode'), //table
        //     'info2' => Crypt::encrypt("cd+cd_descp,cd,cd_descp,value1"), //column
        //     'info3' => Crypt::encrypt("cd_type='TM' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
        //     'info4' => "cd=trs_mode;cd_descp=trs_mode_desc" //field mapping
        // ]);
        $this->crud->addField([
            'name' => 'trs_mode',
            'type' => 'select',
            'options' => DB::table('bscode')
                            ->select('cd as code', 'cd_descp as descp', 'value2 as def')
                            ->where('cd_type', 'TM')
                            ->orderBy('value1', 'asc')
                            ->where('g_key', Auth::user()->g_key)
                            ->where('c_key', Auth::user()->c_key)
                            ->get()
        ]);

        $this->crud->addField([
            'name' => 'temperate',
            'type' => 'select',
            'options' => DB::table('bscode')
                            ->select('cd as code', 'cd_descp as descp')
                            ->where('cd_type', 'TEMPERATE')
                            ->orderBy('value1', 'asc')
                            ->where('g_key', Auth::user()->g_key)
                            ->where('c_key', Auth::user()->c_key)
                            ->get()
        ]);


        $this->crud->addField([
            'name' => 'truck_cmp_nm',
            'type' => 'lookup',
            'title' => '卡車公司查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname"), //column
            'info3' => Crypt::encrypt("status='B' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
            'info4' => "cust_no=truck_cmp_no;cname=truck_cmp_nm" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'truck_cmp_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'car_no',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'close_status',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'driver',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'exp_reason',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'exp_type',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_cust_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_cust_nm',
            'type' => 'lookup',
            'title' => '客戶查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname,contact,address,zip,city_nm,area_id,area_nm,email,phone,phone2,remark,receive_mail"), //column
            'info3' => Crypt::encrypt("status='B' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
            'info4' => "cust_no=dlv_cust_no;cname=dlv_cust_nm;address=dlv_addr;contact=dlv_attn;zip=dlv_zip;city_nm=dlv_city_nm;area_id=dlv_area_id;area_nm=dlv_area_nm;email=dlv_email;phone=dlv_tel;phone2=dlv_tel2;remark=dlv_remark;receive_mail=dlv_send_mail;area_nm=dlv_info;" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'dlv_addr',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'dlv_info',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'dlv_addr_info',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_attn',
            'type' => 'text'
        ]);
        
        $this->crud->addField([
            'name' => 'dlv_tel',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'dlv_tel2',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_email',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_city_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_city_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_zip',
            'type' => 'lookup',
            'title' => '郵地區號查詢',
            'info1' => Crypt::encrypt('sys_area'), //table
            'info2' => Crypt::encrypt("dist_cd+city_nm+dist_nm,city_nm,dist_cd,dist_nm"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "dist_cd=dlv_zip;city_nm=dlv_city_nm;dist_nm=dlv_area_nm;dist_nm=dlv_info;" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'dlv_area_id',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_area_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_lat',
            'type' => 'text'
        ]);
        

        $this->crud->addField([
            'name' => 'dlv_lng',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_cust_nm',
            'type' => 'lookup',
            'title' => '客戶查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname,contact,address,zip,city_nm,area_id,area_nm,email,phone,phone2,remark,receive_mail"), //column
            'info3' => Crypt::encrypt("status='B' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
            'info4' => "area_nm=pick_info;cust_no=pick_cust_no;cname=pick_cust_nm;address=pick_addr;contact=pick_attn;zip=pick_zip;city_nm=pick_city_nm;area_id=pick_area_id;area_nm=pick_area_nm;email=pick_email;phone=pick_tel;phone2=pick_tel2;remark=pick_remark;receive_mail=pick_send_mail" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'pick_cust_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_addr',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_attn',
            'type' => 'text'
        ]);
        
        $this->crud->addField([
            'name' => 'pick_tel',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'pick_tel2',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'pick_email',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_city_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_city_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_zip',
            'type' => 'lookup',
            'title' => '郵地區號查詢',
            'info1' => Crypt::encrypt('sys_area'), //table
            'info2' => Crypt::encrypt("dist_cd+city_nm+dist_nm,dist_cd,dist_nm,city_nm"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "city_nm=pick_info;dist_cd=pick_zip;city_nm=pick_city_nm;dist_nm=pick_area_nm;" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'pick_info',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'pick_addr_info',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_area_id',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_area_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_lat',
            'type' => 'text'
        ]);
        

        $this->crud->addField([
            'name' => 'pick_lng',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pkg_num',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pkg_unit',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 's_key',
            'type' => 'select'
        ]);

        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'remark',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_remark',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'pick_remark',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'amt',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'wms_order_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'cust_amt',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'collectamt',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'amt_remark',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'total_gw',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'total_cbm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'owner_cd',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'owner_nm',
            'type' => 'lookup',
            'title' => '貨主查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname,receive_mail"), //column
            'info3' => Crypt::encrypt("status='B' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'"), //condition
            'info4' => "cust_no=owner_cd;cname=owner_nm;receive_mail=owner_send_mail" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'sys_ord_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'distance',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'wh_addr',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'etd',
            'type' => 'date_picker'
        ]);

        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'owner_send_mail',
            'type' => 'select'
        ]);

        $this->crud->addField([
            'name' => 'pick_send_mail',
            'type' => 'select'
        ]);

        $this->crud->addField([
            'name' => 'dlv_send_mail',
            'type' => 'select'
        ]);
        $this->crud->addField([
            'name' => 'is_urgent',
            'type' => 'select'
        ]);
    }

    public function index() {
        try{
        ini_set('memory_limit', '1024M');
        $user = Auth::user();
        \Log::info("index");
        \Log::info("OrderMgmtclose");
        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }
        
        $BaseModel = new BaseModel();
        $obj = null;

        $layout = $BaseModel->getLayout(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_close'));
        $obj = json_decode($layout);

        $fieldData = $BaseModel->getColumnInfo("mod_order_close");
        if($obj != null) {
            array_push($fieldData, $obj);
        }

        $colModel = array('mod_order_close' => $fieldData);

        $this->crud->colModel = $fieldData;
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
            if(Auth::user()->hasPermissionTo('CloseOrder'))
            {
                \Log::info("before view");
                return view($this->crud->getListView(), $this->data);
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return back();
        }
        return back();
    }

    public function create()
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;

        $this->data['carData'] = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->c_key)->where('identity', 'c')->first();

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }

    public function chStation() {
        $user     = Auth::user();
        $ids      = request('ids');
        $skey     = request('cust_no');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                //$ordNo = $ids[$i];
                $orddata = OrderMgmtModel::find($ids[$i]);
                if(isset($orddata)) {
                    $orddata->s_key = $skey;
                    $orddata->updated_at = \Carbon::now();
                    $orddata->updated_by = $user->email;
                    $orddata->save();
                }   
            }
        }

        return response()->json(['msg' => 'success']);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        $entry = $this->crud->getEntry($id);

        // get the info for that entry
        $this->data['entry'] = $entry;
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['ord_id'] = $entry->ord_id;
        $this->data['id'] = $id;
        $this->data['ord_no'] = $entry->ord_no;
        $this->data['sys_ord_no'] = $entry->sys_ord_no;
        $this->data['close_status'] = $entry->close_status;
        $this->data['skeydata'] = DB::table('sys_customers')->where('g_key', $user->g_key)->where('d_key', $user->d_key)->where('c_key', $user->c_key)->where('identity', 's')->get();
        $this->data['carData'] = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->c_key)->where('identity', 'c')->first();
        try{
            if(Auth::user()->hasPermissionTo('CloseOrder'))
            {
                return view($this->crud->getEditView(), $this->data);
            }
        }
        catch(\Exception $e) {
            return back();
        }
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function get($ord_id=null) {
        $orderDetail = [];
        if($ord_id != null) {
            $this_query = DB::table('mod_order_detail');
            $this_query->where('ord_id', $ord_id);
            $orderDetail = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $orderDetail,
        );

        return response()->json($data);
    }

    public function packGet($ord_no=null) 
    {
        $orderDetail = [];
        if($ord_no != null) {
            $this_query = DB::table('mod_order_pack');
            $this_query->where('ord_no', $ord_no);
            $orderDetail = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $orderDetail,
        );

        return response()->json($data);
    }
    public function Deatailimgstore(Request $request) {

        $user = Auth::user();
        $now = date('YmdHis');
        $ordData = OrderMgmtModel::where('id', $request->ord_id)->first();
        // Storage::disk('local')->put($ordData->sys_ord_no."_".$now.".jpg", base64_decode($request->guid));
        // $path = Storage::disk('local')->putFile('',$request->guid);
        dd($request->guid);
        $path = Storage::putFileAs('', $request->guid, $ordData->sys_ord_no."_".$now.".jpg");
        DB::table('mod_file')->insert([
            'guid'        => $path,
            "ref_no"      => $ordData->sys_ord_no,
            "type_no"     => $request->type_no,
            "file_format" => "jpg",
            "empty_img"   => "Y",
            "sendsyl"     => "N",
            'g_key'       => $user->g_key,
            'c_key'       => $user->c_key,
            's_key'       => $user->s_key,
            'd_key'       => $user->d_key,
            'created_by'  => $user->email,
            'created_at'  => \Carbon::now()
        ]);
        if($request->type_no=="FINISH"){
            $ordData->sign_pic="1";
            $ordData->save();
        }else{
            $ordData->error_pic="1";
            $ordData->save();
        }
        // if($ordData->owner_cd=="70552531"){
        //     $years = substr($ordData->finish_date,0,4);
        //     $months= substr($ordData->finish_date,5,2);
        //     Storage::disk('azure')->put($years.'/'.$months.'/'.$ordData->ord_no.'.jpg', base64_decode($img));
        // }

        $imgdata =DB::table('mod_file_view')
        ->where('ref_no', $ordData->sys_ord_no)
        ->where('empty_img', 'Y')
        ->orderby('created_at','asc')->get();
        return ["msg"=>"success","path"=>$path,"data"=>$imgdata];
    }
    public function DeatailimgGet($ord_id=null) 
    {
        $user = Auth::user();
        $orderDeta = array();
        $imgdata = array();
        if($ord_id != null) {
            $orderDeta=DB::table('mod_order_close')->where('ord_id', $ord_id)->first();
            $ogfileData = DB::table('mod_file')
            ->select( DB::raw("(select id from mod_file b where b.guid=mod_file.guid limit 1)as id ") )
            ->where('ref_no', $orderDeta->sys_ord_no)
            ->where('empty_img', 'Y')
            ->groupBy("guid")
            ->pluck("id")->toArray();
;
            $imgdata =DB::table('mod_file_view')
            ->whereIn('id', $ogfileData)
            ->where('empty_img', 'Y')
            ->orderby('created_at','asc')->get();
        }
        $data[] = array(
            'Rows' => $imgdata,
        );
        return response()->json($data);
    }

    public function ordertallyGet($ord_id=null) 
    {
        $user = Auth::user();
        $orderDeta = array();
        $imgdata = array();
        if($ord_id != null) {
            $orderDeta=DB::table('mod_order_goods_check')->where('ord_id', $ord_id)->orderby('created_at','desc')->get();
        }
        $data[] = array(
            'Rows' => $orderDeta,
        );
        return response()->json($data);
    }

    public function Deatailimgdel($id) 
    {
        $user = Auth::user();
        $imgdata =DB::table('mod_file')->where('id', $id)->first();
        $ref_no = $imgdata->ref_no  ;
        DB::table('mod_file')->where('id', $id)->delete();   
        $filecount_f= DB::table('mod_file')->where('ref_no', $ref_no)->whereIn('type_no', ['FINISH','SIGNIN'])->count();
        $filecount_e= DB::table('mod_file')->where('ref_no', $ref_no)->where('type_no', 'ERROR')->count();
        if($filecount_f==0){
            DB::table('mod_order_close')
            ->where('sys_ord_no', $ref_no)
            ->update(['sign_pic' => '0']);
        }
        if($filecount_e==0){
            DB::table('mod_order_close')
            ->where('sys_ord_no', $ref_no)
            ->update(['error_pic' => '0']);
        }

        return ["msg"=>"success"];
    }
    

    public function DeatailamtGet($ord_id=null) 
    {
        $orderDetail = [];
        if($ord_id != null) {
            $this_query = DB::table('mod_order_fee');
            $this_query->where('ord_id', $ord_id);
            $orderDetail = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $orderDetail,
        );
        return response()->json($data);
    }

    public function DeatailamtStore(Request $request)
    {
        $validator = $this->detailValidator($request);        
        $commonFunc = new CommonModel;
        $request    = $commonFunc->processData($request, $this->crud->create_fields);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderPack = new OrderamtDetailModel;
            foreach($request->all() as $key=>$val) {
                $orderPack[$key] = request($key);
            }
            $orderPack->save();
        }

        return ["msg"=>"success", "data"=>$orderPack->where('id', $orderPack->id)->get()];
    }

    public function DeatailamtUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);
        $commonFunc = new CommonModel;
        $request    = $commonFunc->processData($request, $this->crud->create_fields);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderPack = OrderamtDetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $orderPack[$key] = request($key);
            }
            $orderPack->save();
        }


        return ["msg"=>"success", "data"=>$orderPack->where('id', $request->id)->get()];
    }

    public function DeatailiDel($id)
    {
        $orderPack = OrderamtDetailModel::find($id);
        $orderPack->delete();        

        return ["msg"=>"success"];
    }

    public function packDetailGet($ord_no=null) 
    {
        $packDetail = [];
        
        $packDetail = $this->getPackDetail($ord_no);

        //dd($packDetail);
        
        $data[] = array(
            'Rows' => $packDetail,
        );

        return response()->json($data);
    }


    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request    = $commonFunc->processData($request, $this->crud->create_fields);
        $today      = new \DateTime();
        $str_date    = $today->format('Ym');
        $params = array(
            "c_key" => $user->c_key,
            "date_ym" => substr($str_date, 2, strlen($str_date))
        );
        $BaseModel = new BaseModel();
        $sysOrdNo = $BaseModel->getAutoNumber("order",$params);
        $request->merge(array('sys_ord_no' => $sysOrdNo));
        if($request->ord_no == "" || $request->ord_no == null) {
            $request->merge(array('ord_no' => $sysOrdNo));
        }
        try {
            $response = parent::storeCrud($request);

            $tm = new TrackingModel();
            $tm->insertTracking('', $request->ord_no, '', $sysOrdNo, '', 'A', 1, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
            $ordData = OrderMgmtModel::where('sys_ord_no', $sysOrdNo)->first();
            if(isset($request->dlv_addr) && isset($request->pick_addr) && ($ordData->trs_mode == 'C' || $ordData->trs_mode == 'C01' || $ordData->trs_mode == 'C02')) {
                $g = new GoogleMapModel;
                $wh_addr   = (isset($ordData->wh_addr))?$ordData->wh_addr: '';
                $dlv_city  = $ordData->dlv_city_nm;
                $dlv_area  = $ordData->dlv_area_nm;
                $pick_city = $ordData->pick_city;
                $pick_area = $ordData->pick_area_nm;
                $dist = $g->getDist($pick_city.$pick_area.$ordData->pick_addr, $dlv_city.$dlv_area.$ordData->dlv_addr, $wh_addr, $ordData->trs_mode);
                OrderMgmtModel::where('id', $ordData->id)
                                ->update(['distance' => $dist]);
            }
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }

        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $dist = 0;
        $ordData = null;
        try {
            $response = parent::updateCrud($request);

            $dlv_addr  = $request->dlv_addr;
            $pick_addr = $request->pick_addr;
            $ordData = OrderMgmtModel::where('id', $request->id)->first();
            if($dlv_addr == null) {
                $city = $ordData->dlv_city_nm;
                $area = $ordData->dlv_area_nm;
                $dlv_addr = $city.$area.$ordData->dlv_addr;
            }

            if($pick_addr == null) {
                $city = $ordData->pick_city_nm;
                $area = $ordData->pick_area_nm;
                $pick_addr = $city.$area.$ordData->pick_addr;
            }
            if(isset($dlv_addr) && isset($pick_addr) && ($ordData->trs_mode == 'C' || $ordData->trs_mode == 'C01' || $ordData->trs_mode == 'C02')) {
                $g = new GoogleMapModel;

                $dist = $g->getDist($pick_addr, $dlv_addr, $ordData->wh_addr, $ordData->trs_mode);
                OrderMgmtModel::where('id', $request->id)
                                ->update(['distance' => $dist]);
            }
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }

        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "dist" => $dist];
    }

    public function destroy($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('delete');

        $sysOrdNo = DB::table('mod_order')->where('id', $id)->pluck('sys_ord_no');

        if(isset($sysOrdNo[0])) {
            $orderDetail = OrderDetailModel::where('ord_id', $id);
            //$orderPack = OrderPackModel::where('ord_no', $ord_no[0]);
            //$orderPackDetail = OrderPackDetailModel::where('ord_no', $ord_no[0]);
            $tsRecord = DB::table('mod_ts_record')->where('ref_no4', $sysOrdNo[0])->where('g_key', $user->g_key)->where('c_key', $user->c_key);

            $orderDetail->delete();
            $tsRecord->delete();
            //$orderPack->delete();
            //$orderPackDetail->delete();
        }

        return $this->crud->delete($id);
    }

    public function detailStore(Request $request)
    {
        $validator = $this->detailValidator($request);        

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderDetail = new OrderDetailModel;
            foreach($request->all() as $key=>$val) {
                $orderDetail[$key] = request($key);
            }
            //dd($orderDetail);
            $orderDetail->save();
            $this->updateModOrderGw($orderDetail->ord_id);
        }

        return ["msg"=>"success", "data"=>$orderDetail->where('id', $orderDetail->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderDetail = OrderDetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $orderDetail[$key] = request($key);
            }
            $orderDetail->save();
            $this->updateModOrderGw($orderDetail->ord_id);
        }


        return ["msg"=>"success", "data"=>$orderDetail->where('id', $request->id)->get()];
    }

    public function detailDel($id)
    {
        $orderDetail = OrderDetailModel::find($id);
        $orderDetail->delete();

        $orderPackDetail = OrderPackDetailModel::where('ord_detail_id', $id);
        $ordId = $orderDetail->ord_id;
        $orderPackDetail->delete();

        $this->updateModOrderGw($ordId);

        return ["msg"=>"success"];
    }
    public function DeatailamtDel($id)
    {
        $packData = DB::table('mod_order_pack')->where('id', $id)->first();
        
        $orderPack = OrderamtDetailModel::find($id);
        $orderPack->delete();

        if(isset($packData->pack_no)) {
            $orderPackDetail = OrderamtDetailModel::where('pack_no',$packData->pack_no);
            $orderPackDetail->delete();
        }
        

        return ["msg"=>"success"];
    }

    public function packStore(Request $request)
    {
        $validator = $this->detailValidator($request);        

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderPack = new OrderPackModel;
            foreach($request->all() as $key=>$val) {
                $orderPack[$key] = request($key);
            }
            $orderPack->save();
        }

        return ["msg"=>"success", "data"=>$orderPack->where('id', $orderPack->id)->get()];
    }

    public function packUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderPack = OrderPackModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $orderPack[$key] = request($key);
            }
            $orderPack->save();
        }


        return ["msg"=>"success", "data"=>$orderPack->where('id', $request->id)->get()];
    }

    public function packDel($id)
    {
        $packData = DB::table('mod_order_pack')->where('id', $id)->first();
        
        $orderPack = OrderPackModel::find($id);
        $orderPack->delete();

        if(isset($packData->pack_no)) {
            $orderPackDetail = OrderPackDetailModel::where('pack_no',$packData->pack_no);
            $orderPackDetail->delete();
        }
        

        return ["msg"=>"success"];
    }

    public function packDetailUpdate(Request $request)
    {
        
        $order_detail_id = $request->id;
        $pack_no = $request->pack_no;
        $ord_no = $request->ord_no;
        $num = $request->num;
        $goods_no = $request->goods_no;

        $cnt = DB::table('mod_order_pack_detail')
                        ->where('ord_no', $ord_no)
                        ->where('pack_no', $pack_no)
                        ->where('ord_detail_id', $order_detail_id)
                        ->count();
        if($cnt == 0) {
            $orderPackDetail = new OrderPackDetailModel;
            $orderPackDetail->ord_no = $ord_no;
            $orderPackDetail->pack_no = $pack_no;
            $orderPackDetail->ord_detail_id = $order_detail_id;
            $orderPackDetail->num = $num;
            $orderPackDetail->save();
        }
        else {
            $id = DB::table('mod_order_pack_detail')
                        ->where('ord_no', $ord_no)
                        ->where('pack_no', $pack_no)
                        ->where('ord_detail_id', $order_detail_id)->pluck('id');
                        
            if(isset($id[0])) {
                $orderPackDetail = OrderPackDetailModel::find($id[0]);
                $orderPackDetail->num = $num;
                $orderPackDetail->save();
            }
        }

        $packDetailNum = DB::table('mod_order_pack_detail')
                            ->where('ord_no', $ord_no)
                            ->where('ord_detail_id', $order_detail_id)->sum('num');

        $ordDetailNum = DB::table('mod_order_detail')
                ->where('ord_no', $ord_no)
                ->where('goods_no', $goods_no)->sum('pkg_num');

        return ["msg"=>"success", "data"=>$ordDetailNum - $packDetailNum];
    }

    public function getPackDetail($ord_no) {
        $packDetail = [];
        // if($ord_no != null) {
        //     $this_query = DB::table('mod_order_pack_detail');
        //     $this_query->join('mod_order_detail', 'mod_order_detail.id', '=', 'mod_order_pack_detail.ord_detail_id');
        //     $this_query->select('mod_order_detail.id', 'mod_order_detail.goods_no', 'mod_order_detail.goods_nm', DB::raw('0 AS num'), DB::raw('sum(mod_order_pack_detail.num) as last_num'));
        //     $this_query->where('mod_order_pack_detail.ord_no', $ord_no);
        //     $this_query->groupBy('mod_order_detail.id', 'mod_order_detail.goods_no', 'mod_order_detail.goods_nm');
        //     $packDetail = $this_query->get();
            
        // }

        // //dd($packDetail);

        // if(count($packDetail) == 0) {
        //     $this_query = DB::table('mod_order_detail');
        //     $this_query->select('mod_order_detail.id', 'goods_no', 'goods_nm', DB::raw('0 AS num'), DB::raw('pkg_num AS last_num'));
        //     $this_query->where('ord_no', $ord_no);
        //     $packDetail = $this_query->get();
        // }
        // else {
            
        //     foreach($packDetail as $key=>$val) {
        //         $goods_no = $val->goods_no;

        //         $this_query = DB::table('mod_order_detail');
        //         $this_query->select(DB::raw('sum(pkg_num) AS ttl_num'));
        //         $this_query->where('ord_no', $ord_no);
        //         $this_query->where('goods_no', $goods_no);
        //         $orderDetail = $this_query->get();

        //         if(count($orderDetail) > 0) {
        //             $val->last_num = $orderDetail[0]->ttl_num - $val->last_num;
        //         }
               
        //     }
        // }

        $this_query = DB::table('mod_order_detail');
        $this_query->select('mod_order_detail.id', 'goods_no', 'goods_nm', DB::raw('0 AS num'), DB::raw('(select sum(num) from mod_order_pack_detail where mod_order_pack_detail.ord_detail_id=mod_order_detail.id) AS last_num'));
        $this_query->where('ord_no', $ord_no);
        $packDetail = $this_query->get();
        
        foreach($packDetail as $key=>$val) {
            $goods_no = $val->goods_no;

            $this_query = DB::table('mod_order_detail');
            $this_query->select(DB::raw('sum(pkg_num) AS ttl_num'));
            $this_query->where('ord_no', $ord_no);
            $this_query->where('goods_no', $goods_no);
            $orderDetail = $this_query->get();

            if(count($orderDetail) > 0) {
                $val->last_num = $orderDetail[0]->ttl_num - $val->last_num;
            }
           
        }

        return $packDetail;
    }


    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }

    public function getTsData() {
        $user = Auth::user();
        $data = [];

        $bsData = DB::table('bscode')
                    ->where('cd_type','TRANSTYPE')
                    ->where('g_key', $user->g_key)
                    ->where('c_key', $user->c_key)
                    ->orderBy('cd', 'asc')
                    ->get();

        foreach($bsData as $val) {
            $cd = $val->cd;

            $tsData = DB::table('mod_trans_status')
                        ->where('ts_type', $cd)
                        ->where('g_key', $user->g_key)
                        ->where('c_key', $user->c_key)
                        ->orderBy('order', 'asc')
                        ->get();
            $data[$cd]["status"] = [];
            $data[$cd]["ts_type"] = $val->cd_descp;
            foreach($tsData as $tsVal) {
                array_push($data[$cd]["status"], $tsVal);
            }

            if(count($tsData) == 0) {
                unset($data[$cd]);
            }
        }

        return response()->json($data);
    }

    public function getOrderData($dlv_no=null) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $returnData = array();
        $returnData["ordDatas"] = DB::table('mod_dlv_plan')->where('dlv_no', $dlv_no)->where('status', '<>', 'FINISHED')->orderBy('updated_at', 'desc')->get();
        $returnData["msg"] = "success";
        
        return response()->json($returnData);
    }

    public function getOrderDetail($ord_no=null) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $returnData = array();
        $returnData["ordDetail"] = DB::table('mod_order')->where('ord_no', $ord_no)->first();
        $returnData["msg"] = "success";
        
        return response()->json($returnData);
    }

    public function orderUpload(Request $request) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');

        $imgData1 = substr($request->photo, 1+strrpos($request->photo, ','));
        Storage::disk('local')->put($request->ord_no.'.jpg', base64_decode($imgData1));

        $ord_no      = $request->ord_no;
        $ord_id      = $request->id;

        $ordData = DB::table("mod_order")->where("id", $ord_id)->first();

        DB::table('mod_order')
        ->where('id', $ord_id)
        ->update([
            'status' => "FINISHED",
            'status_desc' => "配送完成"
        ]);

        DB::table('mod_dlv_plan')
        ->where('ord_no', $ord_no)
        ->where('dlv_no', $ordData->dlv_no)
        ->update([
            'status' => "FINISHED"
        ]);
        
        
        $tsData = DB::table("mod_trans_status")->where("ts_type", "E")->orderBy("order", "desc")->first();
        $data = [
            'ts_no'      => $tsData->id,
            'ts_type'    => $tsData->ts_type,
            'sort'       => $tsData->order,
            'ts_name'    => $tsData->ts_name,
            'ts_desc'    => $tsData->ts_desc,
            'ref_no1'    => $ordData->dlv_no,
            'ref_no2'    => $ord_no,
            'ref_no3'    => $ordData->car_no,
            'ref_no4'    => null,
            'g_key'      => $ordData->g_key,
            'c_key'      => $ordData->c_key,
            's_key'      => $ordData->s_key,
            'd_key'      => $ordData->d_key,
            'created_by' => $ordData->car_no,
            'updated_by' => $ordData->car_no,
        ];

        $TransRecordModel = new TransRecordModel();
        $TransRecordModel->createRecord($data);

        if(isset($ordData->dlv_email)) {
            $TrackingModel = new TrackingModel();
            //$TrackingModel->sendTrackingMail($ord_no, $ordData->dlv_email, '貨況通知-訂單號：'.$ord_no);
        }


        return response()->json(["success" => true]);
    }


    public function showChkImg($ord_no=null, $type=null) {
        $imgData = [];
        try {
            /*
            $path = storage_path('app/' . $ord_no.'.jpg');
            if (!File::exists($path)) {
                return "尚未有圖片";
            }
            
            $file = File::get($path);
            $type = File::mimeType($path);
        
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
            */
            $user = Auth::user();
            $thisQuery = DB::table('mod_file')
                            ->where('g_key', $user->g_key)
                            ->where('c_key', $user->c_key)
                            ->where('ref_no', $ord_no);
            if($type == 'FINISH') {
                $thisQuery->whereIn('type_no', ['FINISH', 'SIGNIN']);
            }
            else {
                $thisQuery->where('type_no', $type);
            }

            $imgData = $thisQuery->get();

            foreach($imgData as $row) {
                $img = Storage::get($row->guid);
            }
        }
        catch(\Exception $e) {
            //echo $e->getMessage();
            return view('files.showImg')->with('imgData', []);
        }
    
        return view('files.showImg')->with('imgData', $imgData);
    }

    public function failOrder() {
        $ids = request('ids');
        $errorMsg = "";

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $order = OrderCloseMgmtModel::find($ids[$i]);
                if($order->status != "UNTREATED") {
                    $errorMsg = "系統訂單號：".$order->sys_ord_no."，狀態不在「尚未安排」，故無法作廢";
                    return response()->json(array('msg' => 'error', 'errorMsg' => $errorMsg));
                }
            }

            for($i=0; $i<count($ids); $i++) {
                $order = OrderCloseMgmtModel::find($ids[$i]);
                $order->status = 'FAIL';
                $order->status_desc = '作廢';
                $order->save();
            }
        }

        return response()->json(array('msg' => 'success', 'errorMsg' => $errorMsg));
    }
    public function reclose() {
        $ids = request('ids');
        $errorMsg = "";
        $user = Auth::user();
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $order = OrderCloseMgmtModel::find($ids[$i]);
                $order->close_status = 'N';
                $order->close_by = $user->email;
                $order->close_date = \Carbon::now();
                $order->save();
            }
        }

        return response()->json(array('msg' => 'success', 'errorMsg' => $errorMsg));
    }
    public function getfeevalue(Request $request) {
        $user = Auth::user();
        $id = $request->id;
        $fee_cd = $request->fee_cd;
        $order = OrderMgmtModel::find($id);
        $cust_feedata =array();
        $custata =DB::table('sys_customers')->where('g_key', $user->g_key)->where('cust_no', $order->owner_cd)->first();
        if(isset($cust_feedata)){
            $cust_feedata =DB::table('mod_cust_fee')->where('g_key', $user->g_key)->where('cust_id',$custata->id)->where('fee_cd',$fee_cd)->first();
        }
        $returnData = array();
        $returnData['cust_feedata'] = $cust_feedata;
        return response()->json($returnData);
    }

    public function close() {
        $ids = request('ids');
        $errorMsg = "";
        $user = Auth::user();
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $order = OrderCloseMgmtModel::find($ids[$i]);
                $order->close_status = 'Y';
                $order->close_by = $user->email;
                $order->close_date = \Carbon::now();
                $order->save();
            }
        }

        return response()->json(array('msg' => 'success', 'errorMsg' => $errorMsg));
    }
    public function closeOrder() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);
                $order->status = 'CLOSE';
                $order->status_desc = '作廢';
                $order->save();
            }
        }

        return response()->json(array('msg' => 'success'));
    }
    public function multiClose() {
        $user = Auth::user();
        $ids = request('ids');
        $OrderCloseMgmtModel = new OrderCloseMgmtModel();
        try{
            if(count($ids) > 0) {
                for($i=0; $i<count($ids); $i++) {
                    $order = OrderMgmtModel::find($ids[$i]);
                    $data = [
                        'ord_id' => $order->id,
                        'g_key' => $order->g_key,
                        'c_key' => $order->c_key,
                        's_key' => $order->s_key,
                        'd_key' => $order->d_key,
                        'ord_no' => $order->ord_no,
                        'status' => $order->status,
                        'loc_cros' => $order->loc_cros,
                        'remark' => $order->remark,
                        'dlv_cust_nm' => $order->dlv_cust_nm,
                        'dlv_cust_no' => $order->dlv_cust_no,
                        'dlv_addr' => $order->dlv_addr,
                        'dlv_attn' => $order->dlv_attn,
                        'dlv_tel' => $order->dlv_tel,
                        'dlv_city_no' => $order->dlv_city_no,
                        'dlv_city_nm' => $order->dlv_city_nm,
                        'dlv_zip' => $order->dlv_zip,
                        'dlv_area_id' => $order->dlv_area_id,
                        'dlv_area_nm' => $order->dlv_area_nm,
                        'dlv_lat' => $order->dlv_lat,
                        'dlv_lng' => $order->dlv_lng,
                        'pkg_num' => $order->pkg_num,
                        'pkg_unit' => $order->pkg_unit,
                        'etd' => $order->etd,
                        'truck_cmp_no' => $order->truck_cmp_no,
                        'truck_cmp_nm' => $order->truck_cmp_nm,
                        'truck_no' => $order->truck_no,
                        'driver' => $order->driver,
                        'exp_reason' => $order->exp_reason,
                        'exp_type' => $order->exp_type,
                        'updated_by' => $order->updated_by,
                        'created_by' => $order->created_by,
                        'created_at' => $order->created_at,
                        'updated_at' => $order->updated_at,
                        'cbm' => $order->cbm,
                        'dlv_no' => $order->dlv_no,
                        'car_no' => $order->car_no,
                        'trs_mode' => $order->trs_mode,
                        'pick_cust_no' => $order->pick_cust_no,
                        'pick_cust_nm' => $order->pick_cust_nm,
                        'pick_addr' => $order->pick_addr,
                        'pick_attn' => $order->pick_attn,
                        'pick_tel' => $order->pick_tel,
                        'pick_city_no' => $order->pick_city_no,
                        'pick_city_nm' => $order->pick_city_nm,
                        'pick_zip' => $order->pick_zip,
                        'pick_area_id' => $order->pick_area_id,
                        'pick_area_nm' => $order->pick_area_nm,
                        'pick_lat' => $order->pick_lat,
                        'pick_lng' => $order->pick_lng,
                        'total_cbm' => $order->total_cbm,
                        'total_cbm_unit' => $order->total_cbm_unit,
                        'dlv_email' => $order->dlv_email,
                        'pick_email' => $order->pick_email,
                        'distance' => $order->distance,
                        'car_type' => $order->car_type,
                        'dlv_type' => $order->dlv_type,
                        'amt' => $order->amt,
                        'total_gw' => $order->total_gw,
                        'cust_amt' => $order->cust_amt,
                        'amt_remark' => $order->amt_remark,
                        'ord_type_cd' => $order->ord_type_cd,
                        'ord_type_nm' => $order->ord_type_nm,
                        'wh_addr' => $order->wh_addr,
                        'owner_cd' => $order->owner_cd,
                        'owner_nm' => $order->owner_nm,
                        'sys_ord_no' => $order->sys_ord_no,
                        'error_remark' => $order->error_remark,
                        'dlv_remark' => $order->dlv_remark,
                        'pick_remark' => $order->pick_remark,
                        'finish_date' => $order->finish_date,
                        'owner_send_mail' => $order->owner_send_mail,
                        'pick_send_mail' => $order->pick_send_mail,
                        'dlv_send_mail' => $order->dlv_send_mail,
                        'sign_pic' => $order->sign_pic,
                        'error_pic' => $order->error_pic,
                        'sys_etd' => $order->sys_etd,
                        'cust_ord_no' => $order->cust_ord_no,
                        'is_urgent' => $order->is_urgent,
                        'temperate' => $order->temperate,
                        'status_desc' => $order->status_desc,
                        'temperate_desc' => $order->temperate_desc,
                        'trs_mode_desc' => $order->trs_mode_desc,
                        'car_type_desc' => $order->car_type_desc,
                        'abnormal_remark' => $order->abnormal_remark,
                        'pick_addr_info' => $order->pick_addr_info,
                        'dlv_addr_info' => $order->dlv_addr_info,
                        'collectamt' => $order->collectamt,
                        'realnum' => $order->realnum,
                        'is_ordered' => $order->is_ordered,
                        'order_tally' => $order->order_tally,
                        'wms_order_no' => $order->wms_order_no,
                        'close_status' => $order->close_status
                    ];
                    $OrderCloseMgmtModel->insertClose($data);
                    $order->delete();
                }
            }
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        return response()->json(array('msg' => 'success'));
    }
    public function multiDel() {
        $user = Auth::user();
        $ids = request('ids');
        $errorMsg = "";
        
        if(count($ids) > 0) {

            for($i=0; $i<count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);
                if($order->status != "UNTREATED") {
                    $errorMsg = "系統訂單號：".$order->sys_ord_no."，狀態不在「尚未安排」，故無法刪除";
                    return response()->json(array('msg' => 'error', 'errorMsg' => $errorMsg));
                }
                
            }
            for($i=0; $i<count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);
                $ordId = $order->id;
                $order->delete();

                $ordDetail = OrderDetailModel::where('ord_id',$ordId);
                $ordDetail->delete();

                $sysOrdNo = DB::table('mod_order')->where('id', $ordId)->pluck('sys_ord_no');

                if(isset($sysOrdNo[0])) {
                    $tsRecord = DB::table('mod_ts_record')->where('ref_no4', $sysOrdNo[0])->where('g_key', $user->g_key)->where('c_key', $user->c_key);
                    $tsRecord->delete();
                }
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function compulsiveDel() {
        $user = Auth::user();
        $ids = request('ids');

        
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);
                $ordId = $order->id;

                $DlvPlan = DB::table('mod_dlv_plan')->where('sys_ord_no', $order->sys_ord_no);
                $DlvPlan->delete();
                $order->delete();

                $ordDetail = OrderDetailModel::where('ord_id',$ordId);
                $ordDetail->delete();

                $sysOrdNo = DB::table('mod_order')->where('id', $ordId)->pluck('sys_ord_no');

                if(isset($sysOrdNo[0])) {
                    $tsRecord = DB::table('mod_ts_record')->where('ref_no4', $sysOrdNo[0])->where('g_key', $user->g_key)->where('c_key', $user->c_key);
                    $tsRecord->delete();


                }
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function reCalculateFee() {
        $ids = request('ids');
        try {
            for($i=0; $i<count($ids); $i++) {
                $ordData = DB::table('mod_order')->where('id', $ids[$i])->first();
                // if(isset($ordData) && $ordData->car_no != null) {
                //     $c = new CalculateModel();
                //     $amt = $c->calculateAmt($ids[$i]);
                //     DB::table('mod_order')
                //     ->where('id', $ids[$i])
                //     ->update([
                //         'amt' => $amt
                //     ]);
                // }
                // else {
                //     return response()->json(['msg' => 'error', 'showMsg' => '您有未派車的訂單，無法計價']);
                // }
                $c = new CalculateModel();
                $amt = $c->calculateAmt($ids[$i]);
                DB::table('mod_order')
                ->where('id', $ids[$i])
                ->update([
                    'amt' => $amt
                ]);
                
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getLine()]);
        }
        
        return response()->json(['msg' => 'success']);
    }

    public function cancelError() {
        $ids = request('ids');
        $user = Auth::user();
        try {
            if(is_array($ids)) {
                DB::table('mod_order')->whereIn('id', $ids)->where('status', 'ERROR')->update(['status' => 'UNTREATED','status_desc' => '尚未安排','dlv_no' => null,'updated_at' =>\Carbon::now() ,'updated_by' => $user->email]);
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }
        
        return response()->json(['msg' => 'success']);
    }

    private function updateModOrderGw($ord_id) {
        $detailData = DB::table('mod_order_detail')
                        ->select(DB::raw('SUM(gw) as total_gw'), DB::raw('SUM(pkg_num) as pkg_num'), DB::raw('SUM(cbm) as total_cbm'))
                        ->where('ord_id', $ord_id)
                        ->first();
        if(isset($detailData)) {
            $pkgUnit = null;
            $detailUnit = DB::table('mod_order_detail')
                        ->select('pkg_unit')
                        ->whereNotNull('pkg_unit')
                        ->where('ord_id', $ord_id)
                        ->first();
            if(isset($detailUnit)) {
                $pkgUnit = $detailUnit->pkg_unit;
            }
            OrderMgmtModel::where('id', $ord_id)
                        ->update(['total_gw' => $detailData->total_gw, 'pkg_num' => $detailData->pkg_num, 'total_cbm' => $detailData->total_cbm, 'pkg_unit' => $pkgUnit]);
        }else{
            OrderMgmtModel::where('id', $ord_id)
                        ->update(['total_gw' => null, 'pkg_num' => null, 'total_cbm' => null, 'pkg_unit' => null]);
        }

        return;
    }

    public function ordOverView() {
        $data = array(
            'crud' => (object)array(
                'colModel' => array()
            )
        );
        return view('order.index', $data);
    }

    public function ordDetailOverView() {
        $data = array(
            'crud' => (object)array(
                'colModel' => array()
            )
        );
        return view('order.orderDetailOverView', $data);
    }

    public function imgDownload() {

        try {
            $directory = 'tmp';

            $ids = request('ids');

            $sysOrdNoArray = explode(',', $ids);

            $ogfileData = DB::table('mod_file')
            ->select("guid", DB::raw("(select id from mod_file b where b.guid=mod_file.guid limit 1)as id ") )
            ->where('empty_img', 'Y')
            ->whereIn('ref_no', $sysOrdNoArray)
            ->orderBy('ref_no', 'asc')
            ->groupBy("guid")
            ->pluck("id")->toArray();

            $fileData = DB::table('mod_file')
                            ->where('empty_img', 'Y')
                            ->whereIn('id', $ogfileData)
                            ->orderBy('ref_no', 'asc')
                            ->get();

            Storage::deleteDirectory($directory);
            Storage::makeDirectory($directory);

            $k = 0;
            $oldSysOrdNo = "";
            $test = "";
            if(count($fileData) == 0) {
                return "無檔案可下載";
            }

            foreach($fileData as $key=>$row) {
                $orderData = DB::table('mod_order_close')
                ->where('sys_ord_no', $row->ref_no)
                ->first();
                if(!isset($orderData->ord_no)){
                    $sysOrdNo = $orderData->sys_ord_no;
                }
                else{
                    $sysOrdNo = $orderData->ord_no;
                }
                $fileName = $row->guid;
                $typeNo   = $row->type_no;

                if($key == 0) {
                    $oldSysOrdNo = $sysOrdNo;
                }

                if($oldSysOrdNo != $sysOrdNo) {
                    $oldSysOrdNo = $sysOrdNo;
                    $k = 0;
                }

                if($typeNo == 'ERROR') {
                    Storage::copy($fileName, 'tmp/'.$sysOrdNo.'-'.$k.'-error.jpg');
                }
                else {
                    if($k==0){
                        Storage::copy($fileName, 'tmp/'.$sysOrdNo.'.jpg');
                    }else{
                        Storage::copy($fileName, 'tmp/'.$sysOrdNo.'-'.$k.'.jpg');
                    }
                }

                
                $k++;
            }

            $files = glob(storage_path('app/tmp/*'));

            $today       = new \DateTime();
            $str_date    = $today->format('Ymd');
            $zipName     = 'photo-'.$str_date;

            Storage::delete('order/'.$zipName.'.zip');
            \Zipper::make(storage_path('app/order/'.$zipName.'.zip'))->add($files)->close();

            
        }
        catch(\Exception $e) {
            return  $e->getline();
        } 
        
        return response()->download(storage_path('app/order/'.$zipName.'.zip'));
    }
    public function sendedi() {
        $user = Auth::user();
        $ids = request('ids');
        $now = date('YmdHis');
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {

                // $order = OrderMgmtModel::find($ids[$i]);
                $order = OrderMgmtModel::where('id', $ids[$i])
                ->where('owner_cd','007')
                ->where('g_key','SYL')
                ->whereIn('status', ['FINISHED', 'REJECT'])
                ->first();

                $content_is = "";
                $content_sh = "";
                $status     = "";
                $install    = "";
                $statusid   = "";
                if(count($order)>0){
                    $dlvdata = DB::table('mod_dlv')->where('dlv_no',$order->dlv_no)->first();
                    $sn_no = DB::table('mod_order_detail')->where('ord_no',$order->ord_no)->value('sn_no');
                    $date = date('YmdHis',strtotime($order->finish_date));
                    $deparedate = date('YmdHis',strtotime('-1 minutes',strtotime($order->finish_date)));
                    if($order->finish_date ==null){
                        $date =$now;
                        $deparedate = $now;
                    }
                    if($order->status=='FINISHED'){
                        $status  = "1";
                        $install = "1";
                    }else{
                        $status	 = '';
                        $install = '2';
                    } 
                    $content_is = $content_is.$order->ord_no.'|';
                    $content_is = $content_is.$order->cust_ord_no.'|';
                    $content_is = $content_is.$deparedate.'|';
                    $content_is = $content_is.$date.'|';
                    $content_is = $content_is.$status.'|';
                    $content_is = $content_is.$install.'|';
                    $content_is = $content_is.''.'|';
                    $content_is = $content_is.$sn_no.'|';
                    $content_is = $content_is.$dlvdata->driver_nm.'|';
                    $content_is = $content_is.$dlvdata->driver_phone.'|';
                    $content_is = $content_is.$date;
                    DB::table('sys_edi_log')->insert([
                        'sys_ord_no' => $order->sys_ord_no,
                        'style' => 'IS',
                        'status' => 'N',
                        'content' =>$content_is,
                        'g_key' =>$order->g_key,
                        'c_key' =>$order->c_key,
                        's_key' =>$order->s_key,
                        'd_key' =>$order->d_key,
                        'created_by' => $user->email,
                        'created_at' => \Carbon::now()
                    ]);
                
                    $date = date('YmdHis',strtotime($order->finish_date));
                    if($order->finish_date ==null){
                        $date =$now;
                    }
                    if($order->status=='FINISHED'){
                        $status   = "配達";
                        $statusid = "3";
                    }
                    else {
                        $status   = "客戶拒收";
                        $statusid = "5";
                    }
                    $content_sh = $content_sh.$order->ord_no.'|';
                    $content_sh = $content_sh.$order->cust_ord_no.'|';
                    $content_sh = $content_sh.'SYL-'.$order->s_key.'|';
                    $content_sh = $content_sh.$date.'|';
                    $content_sh = $content_sh.'HTY|';
                    $content_sh = $content_sh.$statusid.'|';
                    $content_sh = $content_sh.$status.'|';
                    $content_sh = $content_sh.$order->car_no.'|';
                    DB::table('sys_edi_log')->insert([
                        'sys_ord_no' => $order->sys_ord_no,
                        'style' => 'SH',
                        'status' => 'N',
                        'content' =>$content_sh,
                        'g_key' =>$order->g_key,
                        'c_key' =>$order->c_key,
                        's_key' =>$order->s_key,
                        'd_key' =>$order->d_key,
                        'created_by' => $user->email,
                        'created_at' => \Carbon::now()
                    ]);   
                    
                    $fileData = DB::table('mod_file')
                    ->where('ref_no', $order->sys_ord_no)
                    ->orderBy('ref_no', 'asc')
                    ->get();
                    if(count($fileData) > 0) {                    
                        foreach($fileData as $key=>$row) {
                            $fileName = $row->guid;
                            $typeNo   = $row->type_no;
                            DB::table('sys_edi_log')->insert([
                            'sys_ord_no' => $order->sys_ord_no,
                            'style' => 'IMG',
                            'status' => 'N',
                            'content' =>$fileName,
                            'g_key' =>$row->g_key,
                            'c_key' =>$row->c_key,
                            's_key' =>$row->s_key,
                            'd_key' =>$row->d_key,
                            'created_by' => $user->email,
                            'created_at' => \Carbon::now()
                            ]);
                        }
                    }
                }                
            }
        }
        return response()->json(array('msg' => 'success'));
    }
}
