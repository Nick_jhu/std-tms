<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Models\QuotMgmtModel;
use App\Models\BaseModel;
use App\Models\CommonModel;
use Illuminate\Session\Store as Session;

use App\Http\Requests\QuotMgmtCrudRequest as StoreRequest;
use App\Http\Requests\QuotMgmtCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;

class QuotMgmtCrudController extends CrudController
{
    
    public function setup() {
        $this->crud->setModel("App\Models\QuotMgmtModel");
        $this->crud->setEntityNameStrings("報價管理", "報價管理");
        $this->crud->setRoute(config('backpack.base.route_prefix').'/QuotMgmt');
        
        $this->crud->setCreateView('quot.edit');
        $this->crud->setEditView('quot.edit');
        $this->crud->setListView('quot.index');
        //$this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'quot_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'quot_date',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'status',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'effect_from',
            'type' => 'date_picker'
        ]);

        $this->crud->addField([
            'name' => 'effect_to',
            'type' => 'date_picker'
        ]);

        $this->crud->addField([
            'name' => 'cur',
            'type' => 'lookup',
            'info1' => Crypt::encrypt('customers'), //table
            'info2' => Crypt::encrypt("id+name,id,name"), //column
            'info3' => Crypt::encrypt("status='A'"), //condition
            'info4' => "id=cust_contact;name=address", //field mapping
        ]);

        $this->crud->addField([
            'name' => 'cust_no',
            'type' => 'lookup',
            'title' => '客戶查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname,contact,address"), //column
            'info3' => Crypt::encrypt("status='B'"), //condition
            'info4' => "cust_no=cust_no;cname=cust_nm" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'cust_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'remark',
            'type' => 'text'
        ]);
    }

    public function get($ord_no=null) {
        $orderDetail = [];
        if($ord_no != null) {
            $this_query = DB::table('mod_order_detail');
            $this_query->where('ord_no', $ord_no);
            $orderDetail = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $orderDetail,
        );

        return response()->json($data);
    }


    public function store(StoreRequest $request)
	{
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        
        try {
            $response = parent::storeCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::updateCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response];
    }

    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        $ord_no = DB::table('mod_order')->where('id', $id)->pluck('ord_no');

        if(isset($ord_no[0])) {
            $orderDetail = OrderDetailModel::where('ord_no', $ord_no[0]);
            $orderPack = OrderPackModel::where('ord_no', $ord_no[0]);
            $orderPackDetail = OrderPackDetailModel::where('ord_no', $ord_no[0]);

            $orderDetail->delete();
            $orderPack->delete();
            $orderPackDetail->delete();
        }

        return $this->crud->delete($id);
    }

    public function detailStore(Request $request)
    {
        $validator = $this->detailValidator($request);        

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderDetail = new OrderDetailModel;
            foreach($request->all() as $key=>$val) {
                $orderDetail[$key] = request($key);
            }
            $orderDetail->save();
        }

        return ["msg"=>"success", "data"=>$orderDetail->where('id', $orderDetail->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $orderDetail = OrderDetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $orderDetail[$key] = request($key);
            }
            $orderDetail->save();
        }


        return ["msg"=>"success", "data"=>$orderDetail->where('id', $request->id)->get()];
    }

    public function detailDel($id)
    {
        $orderDetail = OrderDetailModel::find($id);
        $orderDetail->delete();

        $orderPackDetail = OrderPackDetailModel::where('ord_detail_id', $id);
        $orderPackDetail->delete();

        return ["msg"=>"success"];
    }


    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }
}
