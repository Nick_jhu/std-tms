<?php

namespace App\Http\Controllers;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;


class exportController implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::all();
    }
}
