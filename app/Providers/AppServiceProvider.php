<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
            Schema::defaultStringLength(191);
            DB::listen(function ($query) {
                //\Log::info($query->sql);
                //\Log::info($query->time);
                //\Log::info($query->bindings);
                //dd($query->sql);
                // $query->bindings
                // $query->time
            });

            Storage::extend('sftp', function ($app, $config) {
                return new Filesystem(new SftpAdapter($config));
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    
}
