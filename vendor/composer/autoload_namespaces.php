<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Venturecraft\\Revisionable' => array($vendorDir . '/venturecraft/revisionable/src'),
    'UpdateHelper\\' => array($vendorDir . '/kylekatarnls/update-helper/src'),
    'Parsedown' => array($vendorDir . '/erusev/parsedown'),
    'PHPExcel' => array($vendorDir . '/phpoffice/phpexcel/Classes'),
    'Mpociot\\Firebase' => array($vendorDir . '/mpociot/laravel-firebase-sync/src'),
    'Mockery' => array($vendorDir . '/mockery/mockery/library'),
    'Maatwebsite\\Excel\\' => array($vendorDir . '/maatwebsite/excel/src'),
    'LiveControl\\EloquentDataTable' => array($vendorDir . '/livecontrol/eloquent-datatable/src'),
    'Jenssegers\\Mongodb' => array($vendorDir . '/jenssegers/mongodb/src'),
    'JasperPHP' => array($vendorDir . '/cossou/jasperphp/src'),
);
