<?php

namespace LaravelFCM\Facades;

use Illuminate\Support\Facades\Facade;

class FCMD extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'fcm.sender';
    }
}
