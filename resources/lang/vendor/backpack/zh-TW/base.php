<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\Base Language Lines
    |--------------------------------------------------------------------------
    */

    'registration_closed'  => 'Registration is closed.',
    'first_page_you_see'   => 'The first page you see after login',
    'login_status'         => 'Login status',
    'logged_in'            => '登入成功!',
    'toggle_navigation'    => 'Toggle navigation',
    'administration'       => '管理',
    'user'                 => '使用者',
    'logout'               => '登出',
    'login'                => '登入',
    'register'             => '註冊',
    'name'                 => '名稱',
    'email_address'        => '電子郵件',
    'password'             => '密碼',
    'confirm_password'     => '確認密碼',
    'remember_me'          => '記住我',
    'forgot_your_password' => '忘記密碼?',
    'reset_password'       => '重設密碼',
    'send_reset_link'      => 'Send Password Reset Link',
    'click_here_to_reset'  => '點擊這裡來重設密碼',
    'unauthorized'         => 'Unauthorized.',
    'dashboard'            => 'Dashboard',
    'handcrafted_by'       => 'Handcrafted by',
    'powered_by'           => 'Powered by',
];
