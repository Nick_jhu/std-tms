<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => ':attribute 不是一個有效的URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => ':attribute 只能包含文字.',
    'alpha_dash'           => ':attribute 只能包含文字、數字、符號.',
    'alpha_num'            => ':attribute 只能包含文字、數字.',
    'array'                => ':attribute 必須為陣列.',
    'before'               => ':attribute 必須在 :date 之前.',
    'before_or_equal'      => ':attribute 必須為日期且等於或在 :date 之前.',
    'between'              => [
        'numeric' => ':attribute 必須介於 :min ~ :max.',
        'file'    => ':attribute 必須介於 :min ~ :max 千位元.',
        'string'  => ':attribute 必須介於 :min ~ :max 字元.',
        'array'   => ':attribute 必須介於 :min ~ :max 項目.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => ':attribute 必須是一個有效的Email.',
    'exists'               => '所選的 :attribute 是無效的.',
    'file'                 => ':attribute 必須是一個檔案.',
    'filled'               => ':attribute 不能為空.',
    'image'                => ':attribute 必須為圖片.',
    'in'                   => '所選的 :attribute 是無效的.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => ':attribute 必須是有效的IP位址.',
    'ipv4'                 => ':attribute 必須是有效的 IPv4 位址.',
    'ipv6'                 => ':attribute 必須是有效的 IPv6 位址.',
    'json'                 => ':attribute 必須是有效的 JSON 格式.',
    'max'                  => [
        'numeric' => ':attribute 不能大於 :max.',
        'file'    => ':attribute 不能大於 :max 千位元.',
        'string'  => ':attribute 不能大於 :max 字元.',
        'array'   => ':attribute 不能多於 :max 項目.',
    ],
    'mimes'                => ':attribute 的檔案類型必須為: :values.',
    'mimetypes'            => ':attribute 的檔案類型必須為: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => ':attribute 必須為數字.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => ':attribute 的格式是無效的.',
    'required'             => ':attribute 為必填.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => ':attribute 和 :other 必須一致.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => ':attribute 必須為字元.',
    'timezone'             => ':attribute 必須為有效的時區.',
    'unique'               => ':attribute 不能重複.',
    'uploaded'             => ':attribute 上傳失敗.',
    'url'                  => ':attribute 格式化無效.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
