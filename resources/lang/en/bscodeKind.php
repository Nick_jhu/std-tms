<?php 

 return [
    "id" => "id",
    "baseInfo" => "Information",
    "detail" => "Detail",
    "cdType" => "Type",
    "updatedBy" => "Updated User",
    "updatedAt" => "Update Time",
    "cKey" => "Company",
    "createdBy" => "Created User",
    "createdAt" => "Created Time",
    "cdDescp" => "Code",
    "titleName" => "Code List",
    "sKey" => "Station",
    "dKey" => "Dept.",
    "gKey" => "Group",
    "titleAddName" => "Basic Code"
];