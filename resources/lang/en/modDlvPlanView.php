<?php 

 return [
    "id" => "ID",
    "cKey" => "公司",
    "driverNm" => "司機姓名",
    "addr" => "地址",
    "custNm" => "客戶名稱",
    "STATUS_UNTREATED" => "尚未處理",
    "pickCustNm" => "提貨客戶",
    "updatedAt" => "最後修改時間",
    "STATUS_DLV" => "正在配送",
    "dlvType" => "派車別",
    "status" => "狀態",
    "STATUS_SEND" => "發送司機",
    "sKey" => "站別",
    "sysOrdNo" => "系統訂單號碼",
    "ordNo" => "訂單號碼",
    "ownerNm" => "貨主名稱",
    "carNo" => "車號",
    "dKey" => "部門",
    "STATUS_FINISHED" => "配送完成",
    "dlvCustNm" => "配送客戶",
    "STATUS_ERROR" => "配送異常",
    "titleName" => "配送總覽",
    "dlvNo" => "配送號",
    "gKey" => "集團",
    "etd" => "預計配送日",
    "isUrgent" => "急單"
];