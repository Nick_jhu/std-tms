<?php 

 return [
    "id" => "ID",
    "cKey" => "公司",
    "goodsNo" => "料號1",
    "goodsNo2" => "料號2",
    "goodsNm" => "料號名稱1",
    "goodsNm2" => "料號名稱2",
    "titleName" => "料號建檔",
    "titleAddName" => "料號建檔",
    "gWidth" => "寬",
    "createdBy" => "建單人員",
    "createdAt" => "建單時間",
    "updatedBy" => "最後修改人員",
    "updatedAt" => "最後修改時間",
    "cbm" => "材積",
    "cbmu" => "材積單位",
    "sKey" => "站別",
    "ownerCd" => "貨主代碼",
    "ownerNm" => "貨主名稱",
    "dKey" => "部門",
    "gw" => "重量",
    "gwu" => "重量單位",
    "gLength" => "長",
    "gKey" => "集團",
    "gHeight" => "高"
];