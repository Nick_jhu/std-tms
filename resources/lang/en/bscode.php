<?php 

 return [
    "cKey" => "Company",
    "cd" => "Code",
    "cdDescp" => "Description",
    "cdType" => "Type",
    "createdAt" => "Created Date",
    "createdBy" => "Created User",
    "dKey" => "Dept.",
    "gKey" => "Group",
    "id" => "id",
    "sKey" => "Sation",
    "updatedAt" => "Update Date",
    "updatedBy" => "Update User",
    "value1" => "value1",
    "value2" => "value2",
    "value3" => "value3",
    "titleName" => "BSCODE",
];