<?php 

 return [
    "excelImport" => "Excel Import",
    "no" => "N",
    "yes" => "Y",
    "fail" => "Invalid",
    "saveChange" => "Save",
    "save" => "Save",
    "delete" => "Delete",
    "deleteMsg" => "Success",
    "deleteSelectRow" => "Delete selected Row",
    "exportExcel" => "Export Excel",
    "cancel" => "Cancel",
    "search" => "Search",
    "add" => "New",
    "addNewRow" => "Add",
    "gridOption" => "Grid shift",
    "clear" => "Clear",
    "browse" => "Browse",
    "msg2" => "Are you Sure?",
    "edit" => "Edit",
    "copy" => "Copy",
    "msg1" => "Please select at least one record",
    "back" => "Back",
    "close" => "Close",
    "clearGrid" => "Clear"
];