<?php 

return [
    "titleName"   => "Excel匯入",
    "btnImport"   => "上傳檔案至GRID",
    "btnDel"      => "刪除",
    "btnSave"     => "保存",
    "custName"    => "客戶名稱",
    "etd"         => "到貨日期",
    "driver"      => "司機",
    "remark"      => "備註",
    "orderNo"     => "訂單號碼",
    "dlvAttn"     => "收貨人",
    "pkgNum"      => "件數",
    "totalCbm"    => "總材積",
    "dlvAddr"     => "收貨人地址",
    "dlvTel"      => "收貨人電話",
    "msg1"        => "請上傳檔案",
    "msg2"        => "匯入失敗",
    "msg3"        => "請至少選擇一筆資料",
    "msg4"        => "確定保存",
    "msg5"        => "資料將會保存至資料庫",
    "msg6"        => "保存失敗",
    "msg7"        => "保存成功"
    ];