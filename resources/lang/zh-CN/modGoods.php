<?php 

return [
    "id"           => "ID",
    "goodsNo"      => "料號1",
    "goodsNm"      => "料號名稱1",
    "goodsNo2"     => "料號2",
    "goodsNm2"     => "料號名稱2",
    "gw"           => "重量",
    "gwu"          => "重量單位", 
    "cbm"          => "材積",
    "cbmu"         => "材積單位",
    "createdBy"    => "建單人員",
    "createdAt"    => "建單時間",
    "updatedBy"    => "最後修改人員",
    "updatedAt"    => "最後修改時間",
    "sKey"         => "站別",
    "dKey"         => "部門",
    "gKey"         => "集團",
    "cKey"         => "公司",
    "titleAddName" => "料號建檔",
    "titleName"    => "料號建檔",
    "gLength"      => "長",
    "gWidth"       => "寬",
   "gHeight"     => "高"
];