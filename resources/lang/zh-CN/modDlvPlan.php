<?php 

return [
    "STATUS_UNTREATED"  => "尚未處理",
    "STATUS_SEND"  => "發送司機",
    "STATUS_DLV"  => "正在配送",
    "STATUS_ERROR"  => "配送異常",
    "STATUS_FINISHED"  => "配送完成",
];