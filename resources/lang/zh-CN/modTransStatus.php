<?php 

return [
    "id" => "id",
    "tsType" => "tsType",
    "order" => "order",
    "tsName" => "tsName",
    "tsDesc" => "tsDesc",
    "triggerCode" => "triggerCode",
    "gKey" => "gKey",
    "cKey" => "cKey",
    "sKey" => "sKey",
    "dKey" => "dKey",
    "createdAt" => "createdAt",
    "updatedAt" => "updatedAt",
    "updatedBy" => "updatedBy",
    "createdBy" => "createdBy",
    ];