<?php 

return [
    "STATUS_UNTREATED" => "尚未處理",
    "STATUS_SEND"      => "發送司機",
    "STATUS_DLV"       => "正在配送",
    "STATUS_ERROR"     => "配送異常",
    "STATUS_FINISHED"  => "配送完成",
    "id"               => "ID",
    "dlvType"          => "派車別",
    "dlvNo"            => "配送號",
    "ordNo"            => "訂單號碼",
    "sysOrdNo"         => "系統訂單號碼",
    "custNm"           => "客戶名稱",
    "status"           => "狀態",
    "addr"             => "地址",
    "etd"              => "預計配送日",
    "carNo"            => "車號",
    "driverNm"         => "司機姓名",
    "gKey"             => "集團",
    "cKey"             => "公司",
    "sKey"             => "站別",
    "dKey"             => "部門",
    "titleName"        => "配送總覽",
    "updatedAt"        => "最後修改時間"
];