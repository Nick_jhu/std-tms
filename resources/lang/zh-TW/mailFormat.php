<?php 

 return [
    "id" => "id",
    "title" => "主旨",
    "updatedBy" => "修改人",
    "updatedAt" => "修改時間",
    "cKey" => "公司",
    "createdBy" => "創建人",
    "createdAt" => "創建時間",
    "sKey" => "站別",
    "dKey" => "部門",
    "content" => "郵件格式",
    "titleName" => "郵件標題",
    "type" => "郵件類型",
    "name" => "郵件類型名稱",
    "gKey" => "集團"
];