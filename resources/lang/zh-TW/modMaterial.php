<?php 

 return [

    "id" => "id",
    "namestr" => "名稱",
    "usefulLife" => "使用年限",
    "cbm" => "材積",
    "gw" => "重量",
    "length" => "長度",
    "width" => "寬度",
    "height" => "高度",
    "cd" => "編號",
    "buy_at" => "購買日期",
    "usefullife" => "使用年限",
    "gKey"       => "集團",
    "cKey"       => "公司",
    "sKey"       => "站別",
    "dKey"       => "部門",
    "createdBy" => "建立人",
    "updatedBy" => "修改人",
    "createdAt"  => "建立時間",
    "updatedAt"  => "修改時間",
];