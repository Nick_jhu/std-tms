<?php 

 return [
    "newValue" => "1111",
    1111 => "1111",
    222 => "1111",
    "id" => "ID",
    "goodsNm" => "商品名稱",
    "goodsNo" => "商品編號1",
    "goodsNo2" => "商品編號2",
    "weight" => "寬度",
    "createdBy" => "建單人員",
    "createdAt" => "建立時間",
    "pkgNum" => "數量",
    "pkgUnit" => "數量單位",
    "updatedBy" => "最後修改人員",
    "updatedAt" => "最後更新時間",
    "cbm" => "材/體積",
    "cbmu" => "材/體積單位",
    "sn_no" => "產品序號",
    "gw" => "總重量",
    "gwu" => "總重量單位",
    "ordNo" => "訂單號碼",
    "length" => "長度",
    "height" => "高度",
    "height2" => "高度2",
    "snNo" => "產品序號"
];