<?php 

 return [
    "cKey" => "公司",
    "createdAt" => "建單時間",
    "createdBy" => "建單人員",
    "dKey" => "部門",
    "gKey" => "集團",
    "id" => "id",
    "order" => "順序",
    "sKey" => "站別",
    "triggerCode" => "觸發代碼",
    "tsDesc" => "貨況描述",
    "tsName" => "貨況名稱",
    "tsType" => "貨況類別",
    "updatedAt" => "最後修改時間",
    "updatedBy" => "最後修改人員",
    "titleName" => "貨況資料彙總",
    "titleAddName" => "貨況建檔"
];