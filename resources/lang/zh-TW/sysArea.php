<?php 

 return [
    "cKey" => "公司",
    "cityCd" => "城市代碼",
    "cityNm" => "城市名稱",
    "cntryCd" => "國家代碼",
    "cntryNm" => "國家名稱",
    "createdAt" => "創建時間",
    "createdBy" => "創建人",
    "dKey" => "部門",
    "distCd" => "區代碼",
    "distNm" => "區名稱",
    "gKey" => "集團",
    "id" => "id",
    "remark" => "備註",
    "sKey" => "站別",
    "stateCd" => "省份代碼",
    "stateNm" => "省份名稱",
    "updatedAt" => "修改時間",
    "updatedBy" => "修改人",
    "zipE" => "郵編(訖)",
    "zipF" => "郵編(起)",
    "titleName" => "區域資料彙總",
    "titleAddName" => "區域建檔"
];