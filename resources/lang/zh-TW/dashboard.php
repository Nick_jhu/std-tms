<?php 

 return [
    "action" => "Action",
    "addProducts" => "Add Products to Cart",
    "anotherAction" => "Another Action",
    "completePurchase" => "Complete Purchase",
    "customerService" => "Customer Service",
    "titleName" => "Dashboard",
    "eta" => "最後出車時間",
    "goalCompletions" => "GOAL COMPLETIONS",
    "goalCompletion" => "Goal Completion",
    "monthly" => "Monthly Recap Report",
    "onlineTruck" => "Online Truck",
    "sendInquiries" => "Send Inquiries",
    "separatedLink" => "Separated link",
    "somethingElseHere" => "Something else here",
    "speed" => "Speed",
    "totalCost" => "TOTAL COST",
    "totalProfit" => "TOTAL PROFIT",
    "totalRevenue" => "TOTAL REVENUE",
    "updated" => "Updated",
    "visitPremium" => "Visit Premium Page",
    "battery" => "battery",
    "error" => "異常訂單",
    "rate" => "訂單完成度",
    "order" => "訂單數量",
    "map" => "車輛即時位置"
];