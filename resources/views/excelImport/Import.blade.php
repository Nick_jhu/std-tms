@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
		{{ trans('excel.titleName') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">{{ trans('excel.titleName') }}</li>
	</ol>
</section>
<style>
	*,
	*:before,
	*:after {
	  box-sizing: border-box;
	}
	
	.form-container {
	  padding: 1rem;
	  margin: 2rem auto;
	  background-color: #DDDDDD;
	  border: 1px solid #DDDDDD;
	  width: 50%;
	}
	
	/* HTML5 Boilerplate accessible hidden styles */
	.promoted-input-checkbox {
	  border: 0;
	  clip: rect(0 0 0 0);
	  height: 1px;
	  margin: -1px;
	  overflow: hidden;
	  padding: 0;
	  position: absolute;
	  width: 1px;
	}
	
	.promoted-checkbox input:checked + label > svg {
	  height: 24px;
	  -webkit-animation: draw-checkbox ease-in-out 0.2s forwards;
			  animation: draw-checkbox ease-in-out 0.2s forwards;
	}
	.promoted-checkbox label:active::after {
	  background-color: #DDDDDD;
	}
	.promoted-checkbox label {
	  color: #000000;
	  line-height: 40px;
	  cursor: pointer;
	  position: relative;
	}
	.promoted-checkbox label:after {
	  content: "";
	  height: 34px;
	  width: 34px;
	  margin-right: 1rem;
	  float: left;
	  /* background-color: #DDDDDD; */
	  border: 2px solid #DDDDDD;
	  border-radius: 3px;
	  -webkit-transition: 0.15s all ease-out;
	  transition: 0.15s all ease-out;
	}
	.promoted-checkbox svg {
	  stroke: #000000;
	  stroke-width: 2px;
	  height: 0;
	  width: 40px;
	  position: absolute;
	  left: -4px;
	  top: 5px;
	  stroke-dasharray: 33;
	}
	
	@-webkit-keyframes draw-checkbox {
	  0% {
		stroke-dashoffset: 33;
	  }
	  100% {
		stroke-dashoffset: 0;
	  }
	}
	
	@keyframes draw-checkbox {
	  0% {
		stroke-dashoffset: 33;
	  }
	  100% {
		stroke-dashoffset: 0;
	  }
	}
	.sortable {
		margin: 2px;
		padding: 5px;
	}
	.sortable-container{;
		float: left;
	}
	.sortable-container span {
		font-weight: bold;
	}
	.sortable div {
		border-radius: 5px;
		padding: 5px;
		background-color: white;
		color: black;
		cursor: pointer;
		border: 1px solid transparent;
		}
		.sortable div:hover {
		border: 1px solid #356AA0;
	}   
	.sortable-item{
			border: 1px solid #356AA0;
			/* border-radius: 5px; */
			padding: 3px;
			background-color: white;
			color: black;
	}
	.sortable-item:hover {
		background-color: #356AA0;
		color: white;
	}
	.test-item {
		height:32px;
	}
	.test-item:hover {
		background-color: #356AA0;
		height:32px;
		color: white;
	}
	.menu li{
		display:inline;
		padding-right:10px;
		width:5%;
		font-size: 15px;
	}
	.header li{
		display:inline;
		font-size: 15px;
	}
	.sortdiv{ 
		width:14%; display:inline-block !important; display:inline;padding-right:10px;
		vertical-align: top;
	}
	.sortdivW{ 
		/* background-color: #e8e8e8; */
		/* border: 1px solid #356AA0; */
		width:10%; display:inline-block !important; display:inline;
		vertical-align: top;
	}
	.sortdivWC{ 
		/* background-color: #e8e8e8; */
		/* border: 1px solid #356AA0; */
		width:10.1%; display:inline-block !important; display:inline;
		vertical-align: top;
	}
	.sortdivWR{ 
		/* background-color: #e8e8e8; */
		/* border: 1px solid #356AA0; */
		width:20.5%; display:inline-block !important; display:inline;
		vertical-align: top;
	}
	.sortdivWA{ 
		/* background-color: #e8e8e8; */
		/* border: 1px solid #356AA0; */
		width:20%; display:inline-block !important; display:inline;
		vertical-align: top;
	}
	.sortdivsys{ 
		width:10%; display:inline-block !important; display:inline;padding-right:10px;
		vertical-align: top;
	}
	.sortdivzip{ 
		width:10%; display:inline-block !important; display:inline;padding-right:10px;
		vertical-align: top;
	}
	.sortdivaddr{ 
		width:20%; display:inline-block !important; display:inline;padding-right:10px;
		vertical-align: top;
	}
	.sortdivHsys{
		/* border: 1px solid; */
		background-color: #e8e8e8;
		padding-top:10px;
		width:10.25%; display:inline-block !important; display:inline; 
	}
	.sortdivHzip{
		/* border: 1px solid; */
		background-color: #e8e8e8;
		padding-top:10px;
		width:10.25%; display:inline-block !important; display:inline; 
	}
	.sortdivHaddr{ 
		/* border: 1px solid; */
		background-color: #e8e8e8;
		padding-top:10px;
		width:20.25%; display:inline-block !important; display:inline; 
	}
	.sortdivH{ 
		/* border: 1px solid; */
		background-color: #e8e8e8;
		padding-top:10px;
		width:14.25%; display:inline-block !important; display:inline; 
	}
	.ui-progressbar {
		position: relative;
	}
	.progress-label {
		position: absolute;
		left: 50%;
		top: 4px;
		font-weight: bold;
		text-shadow: 1px 1px 0 #fff;
	}
	.lmask {
		position: absolute;
		height: 100%;
		width: 100%;
		background-color: #000;
		bottom: 0;
		left: 0;
		right: 0;
		top: 0;
		z-index: 9999999;
		opacity: 0.4;
	}
	.swal2-container {
		z-index: 10000;
	}
	.lmask.fixed {
		position: fixed;
	}
	.lmask:before {
		content: '';
		background-color: rgba(0, 0, 0, 0);
		border: 5px solid rgba(0, 183, 229, 0.9);
		opacity: .9;
		border-right: 5px solid rgba(0, 0, 0, 0);
		border-left: 5px solid rgba(0, 0, 0, 0);
		border-radius: 50px;
		box-shadow: 0 0 35px #2187e7;
		width: 50px;
		height: 50px;
		-moz-animation: spinPulse 1s infinite ease-in-out;
		-webkit-animation: spinPulse 1s infinite linear;
		margin: -25px 0 0 -25px;
		position: absolute;
		top: 50%;
		left: 50%;
	}
	.lmask:after {
		content: '';
		background-color: rgba(0, 0, 0, 0);
		border: 5px solid rgba(0, 183, 229, 0.9);
		opacity: .9;
		border-left: 5px solid rgba(0, 0, 0, 0);
		border-right: 5px solid rgba(0, 0, 0, 0);
		border-radius: 50px;
		box-shadow: 0 0 15px #2187e7;
		width: 30px;
		height: 30px;
		-moz-animation: spinoffPulse 1s infinite linear;
		-webkit-animation: spinoffPulse 1s infinite linear;
		margin: -15px 0 0 -15px;
		position: absolute;
		top: 50%;
		left: 50%;
	}

	@-moz-keyframes spinPulse {
	0% {
	-moz-transform: rotate(160deg);
	opacity: 0;
	box-shadow: 0 0 1px #2187e7;
	}
	50% {
	-moz-transform: rotate(145deg);
	opacity: 1;
	}
	100% {
	-moz-transform: rotate(-320deg);
	opacity: 0;
	}
	}
	@-moz-keyframes spinoffPulse {
	0% {
	-moz-transform: rotate(0deg);
	}
	100% {
	-moz-transform: rotate(360deg);
	}
	}
	@-webkit-keyframes spinPulse {
	0% {
	-webkit-transform: rotate(160deg);
	opacity: 0;
	box-shadow: 0 0 1px #2187e7;
	}
	50% {
	-webkit-transform: rotate(145deg);
	opacity: 1;
	}
	100% {
	-webkit-transform: rotate(-320deg);
	opacity: 0;
	}
	}
	@-webkit-keyframes spinoffPulse {
	0% {
	-webkit-transform: rotate(0deg);
	}
	100% {
	-webkit-transform: rotate(360deg);
	}
	}
	
</style>
@endsection 
@section('content')
<style>
	.font-white {
		color: white
	}
	button {
		margin-right: 10px;
	}
</style>
<div class="row">
	<div class='lmask' id="cssLoading" style="display:none"></div>
	<div class="col-md-12">
		<div class="callout callout-danger" id="errorMsg" style="display:none">
			<h4>{{ trans('backpack::crud.please_fix') }}</h4>
			<ul>

			</ul>
		</div>
		<form method="POST" accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
			{{ csrf_field() }}
			<p>
				<a href="{{ url(config('backpack.base.route_prefix', 'admin').'/orderExcelSample/download') }}" class="btn btn-primary">{{ trans('excel.excelSampleDownload') }}</a>
				<input type="file" name="import_file" id="importFile" class="btn-primary font-white" />
			</p>
			<button type="submit" class="btn btn-primary" id="btnImport">{{ trans('excel.btnImport') }}</button>
			<button type="button" class="btn btn-primary" id="btnDel">{{ trans('excel.btnDel') }}</button>
			<button type="button" class="btn btn-primary" id="btnSave">{{ trans('excel.btnSave') }}</button>
			<button type="button" class="btn btn-danger" id="btnClear">{{ trans('excel.btnClear') }}</button>
			@if(Auth::user()->roles->first()->name == "Admin")
			<button type="button" class="btn btn-primary" id="btnLayout">{{ trans('excel.AdjustmentField') }}</button>
			@endif

			<div id="jqxGrid" style="margin-top: 10px"></div>
		</form>
	</div>
</div>


@endsection 
@include('backpack::template.lookup')
@section('after_scripts')

<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>
<script type="text/javascript" src="{{ asset('js') }}/bootstrap.file-input.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script type="text/javascript" src="https://aishek.github.io/jquery-animateNumber/javascripts/jquery.animateNumber.js"></script>

<script>
	$(function(){
		$('#importFile').bootstrapFileInput();
		$.jqx.theme = "bootstrap";
		
		//var dataAdapter = new $.jqx.dataAdapter(source);
		$.get(BASE_API_URL+'/admin/baseApi/getFieldsJson/mod_order', {'key': 'excelOrderGrid'}, function(data){
			var statusCode = {};
			$.grep(data[1], function(e){
				if(e.datafield == "status") {
					statusCode = e.statusCode;
					var statusSource =
					{
							datatype: "array",
							datafields: [
								{ name: 'label', type: 'string' },
								{ name: 'value', type: 'string' }
							],
							localdata: statusCode
					};

					var statusAdapter = new $.jqx.dataAdapter(statusSource, {
						autoBind: true
					});

					$.grep(data[0], function(e){
						if(e.name == "status") {
							e['values'] = { source: statusAdapter.records, value: 'value', name: 'label' }
						}
					});
				}
			});
			var g1 = {
				"name": "goods_no",
				"type": "string"
			}

			var g2 = {
				"name": "goods_no2",
				"type": "string"
			}

			var g3 = {
				"name": "goods_nm",
				"type": "string"
			}

			data[0].push(g1);
			data[0].push(g2);
			data[0].push(g3);
			var source =
			{
				datatype: "json",
				datafields: data[0],
				root:"Rows",
				pagenum: 1,					
				beforeprocessing: function (data) {
					source.totalrecords = data[0].TotalRows;
				},
				filter: function () {
				// update the grid and send a request to the server.
					$("#jqxGrid").jqxGrid('updatebounddata', 'filter');
				},
				sort: function () {
					// update the grid and send a request to the server.
					$("#jqxGrid").jqxGrid('updatebounddata', 'sort');
				},
				
				url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/tmp_mod_order') }}" + "?basecon=created_by;EQUAL;{{Auth::user()->email}}",
			};
			var dataAdapter = new $.jqx.dataAdapter(source, {
				loadComplete: function () {

				}
			});
			columns = [
				{ text: "{{ trans('excel.custName') }}", datafield: 'dlv_cust_nm', width: 250 },
				{ text: "{{ trans('excel.etd') }}", datafield: 'etd', width: 150,cellsformat: 'yyyy-MM-dd' },
				{ text: "{{ trans('excel.driver') }}", datafield: 'driver', width: 180 },
				{ text: "{{ trans('excel.remark') }}", datafield: 'remark', width: 120 },
				{ text: "{{ trans('excel.orderNo') }}", datafield: 'ord_no' },
				{ text: "{{ trans('excel.dlvAttn') }}", datafield: 'dlv_attn', width: 120 },
				{ text: "{{ trans('excel.dlvEmail') }}", datafield: 'dlv_email', width: 120 },
				{ text: "{{ trans('excel.pkgNum') }}", datafield: 'pkg_num', width: 120, cellsalign: 'right' },
				{ text: "{{ trans('excel.totalCbm') }}", datafield: 'total_cbm', width: 120, cellsalign: 'right' },
				{ text: "{{ trans('excel.totalGw') }}", datafield: 'total_gw', width: 120, cellsalign: 'right' },
				{ text: "{{ trans('excel.dlvAddr') }}", datafield: 'dlv_addr', width: 120 },
				{ text: "{{ trans('excel.dlvTel') }}", datafield: 'dlv_tel', width: 120, cellsalign: 'right' },
				{ text: "{{ trans('excel.pickAttn') }}", datafield: 'pick_attn', width: 120 },
				{ text: "{{ trans('excel.pickAddr') }}", datafield: 'pick_addr', width: 120 },
				{ text: "{{ trans('excel.pickTel') }}", datafield: 'pick_tel', width: 120,cellsalign: 'right' },
				{ text: "{{ trans('excel.distance') }}", datafield: 'distance', width: 120,cellsalign: 'right' }
			];

			var a = {
				cellsformat: "",
				datafield: "goods_no",
				dbwidth: "20",
				filtertype: "textbox",
				hidden: false,
				nullable: true,
				text: "{{trans('modOrder.goodsNo')}}",
				values: "",
				width: 120
			};

			var b = {
				cellsformat: "",
				datafield: "goods_no2",
				dbwidth: "20",
				filtertype: "textbox",
				hidden: false,
				nullable: true,
				text: "{{trans('modOrder.goodsNo2')}}",
				values: "",
				width: 120
			};

			var c = {
				cellsformat: "",
				datafield: "goods_nm",
				dbwidth: "20",
				filtertype: "textbox",
				hidden: false,
				nullable: true,
				text: "{{trans('modOrder.goodsNm')}}",
				values: "",
				width: 120
			};

			data[1].push(a);
			data[1].push(b);
			data[1].push(c);

			$("#jqxGrid").jqxGrid({
				width: '100%',
				height: $(".content-wrapper").height() - 150,
				source: dataAdapter,
				selectionmode: 'checkbox',
				columnsresize: true,
				filterable: true,
				altrows: true,
				showfilterrow: true,
				pageable: true,
				virtualmode: true,
				autoshowfiltericon: true,
				columnsreorder: true,
				columnsautoresize: true,
				clipboard: true,
				enablebrowserselection: true,
				sortable: true,
				pagesize: 9999,
				rendergridrows: function (params) {
					//alert("rendergridrows");
					return params.data;
				},
				ready: function () {
					//$('#jqxGrid').jqxGrid('autoresizecolumns');
					if(typeof data[2] !== "undefined") {
						$("#jqxGrid").jqxGrid('loadstate', data[2]);
					}
					loadState();
				},
				columns: data[1]
			});
			
		}, 'JSON');

		
			
	
		$("#myForm").submit(function () {
			if ($('#importFile').get(0).files.length === 0) {
				swal("{{ trans('excel.msg1') }}", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			var name = $('#importFile')[0].value.substring($('#importFile')[0].value.lastIndexOf("\\")+1,$('#importFile')[0].value.length);
			$.post(BASE_URL + '/ExcelImport/checkExcelname', {'name': name}, function(data){
				if(data.msg == "success") {
					$.ajax({
					url: "{{ url(config('backpack.base.route_prefix', 'admin').'/ExcelImport/importExcel') }}",
					type: 'POST',
					data: postData,
					async: false,
					beforeSend: function () {
						
					},
					error: function () {
						swal("{{ trans('excel.msg2') }}", "", "error");
						// swal("{{ trans('excel.msg2') }}", {
						// 	icon: "error",
						// });
						return false;
					},
					success: function (data) {
						//alert(data);
						if(data.msg == "error") {
							swal(data.errorLog, "", "error");
							// swal("{{ trans('excel.msg2') }}", {
							// icon: "error",
							// });
						}
						$.post(BASE_URL + '/ExcelImport/importExcelname', {'name': name}, function(data){

						});
						$('#jqxGrid').jqxGrid('clearselection');
						$('#jqxGrid').jqxGrid('updatebounddata');
						// var dataAdapter = new $.jqx.dataAdapter(data.data);
						// $("#jqxGrid").jqxGrid({ source: dataAdapter });
					},
					cache: false,
					contentType: false,
					processData: false
					});
				}
				else{
					swal(data.errorLog, "警告", "error");
				}
			});
			return false;
		});
		
		$("#btnDel").on("click",function(event){
            //event.stopPropagation()
			var rowIndexes = $('#jqxGrid').jqxGrid('getselectedrowindexes');
			if(rowIndexes.length===0){
				swal("{{ trans('excel.msg3') }}", "", "warning");
				return;
			}
			var rowIds = new Array();
			for (var i = 0; i < rowIndexes.length; i++) {
				var currentId = $('#jqxGrid').jqxGrid('getrowid', rowIndexes[i]);
				rowIds.push(currentId);
			};
			$('#jqxGrid').jqxGrid('deleterow', rowIds);
			$('#jqxGrid').jqxGrid('clearselection');

		});    
		
		$("#btnSave").on("click",function(event){
			var rows = $('#jqxGrid').jqxGrid('getrows');
			swal({
				title: "{{ trans('excel.msg4') }}?",
				text: "{{ trans('excel.msg5') }}!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: "{{ trans('excel.btnSave') }}",
				cancelButtonText: "{{ trans('excel.btnCancel') }}"
				})
				.then((result) => {
				if (result.value) {
					$("#cssLoading").show();
					swal.close();
					setTimeout(function(){
						$.ajax({
							url: "{{ url(config('backpack.base.route_prefix', 'admin') . '/ExcelImport/store') }}",
							type: "POST",				
							data: {'obj':JSON.stringify(rows)},
							async: false,
							dataType:"json",
							beforeSend: function () {
								$("#cssLoading").show();
								swal.close();
							},
							error: function (jqXHR, exception) {
								swal("{{ trans('excel.msg6') }}", "", "error");
								// swal("{{ trans('excel.msg6') }}", {
								// 	icon: "error",
								// });
								// loadingFunc.hide();
								$("#cssLoading").hide();
								return false;
							},
							success: function (data) {
								if(data.msg == "success") {
									swal("{{ trans('excel.msg7') }}", "", "success");
									// swal("{{ trans('excel.msg7') }}", {
									// icon: "success",
									// });
								}else{
									swal("{{ trans('excel.msg6') }}", data.errorLog, "error");
									// swal("{{ trans('excel.msg6') }}", {
									// icon: "error",
									// });
								}
								$('#jqxGrid').jqxGrid('updatebounddata');
								$("#cssLoading").hide();
								// loadingFunc.hide();
								return false;
							}
						});	
					}, 1000);
				
				} else {
					//swal("Your imaginary file is safe!");
				}
			});
        });

		$("#btnLayout").on("click", function(){
			$('#gridOptModal').modal('show');
		});

		$("#saveGrid").on("click", function(){
			var items = $("#jqxlistbox").jqxListBox('getItems');
			$("#jqxGrid").jqxGrid('beginupdate');

			$.each(items, function(i, item) {
				var thisIndex = $('#jqxGrid').jqxGrid('getcolumnindex', item.value)-1;
				if(thisIndex != item.index){
					//console.log(item.value+":"+thisIndex+"="+item.index);
					$('#jqxGrid').jqxGrid('setcolumnindex', item.value,  item.index);
				}
				if (item.checked) {
					$("#jqxGrid").jqxGrid('showcolumn', item.value);
				}
				else {
					$("#jqxGrid").jqxGrid('hidecolumn', item.value);
				}
			})
			
			$("#jqxGrid").jqxGrid('endupdate');
			var state = $("#jqxGrid").jqxGrid('getstate');
			state['filters']['filterscount'] = 0;
			var saveUrl = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/saveLayoutJson') }}"; 	  	  	
			var stateToSave = JSON.stringify(state);

			$.ajax({
				type: "POST",										
				url: saveUrl,		
				data: { data: stateToSave,key: "excelOrderGrid" },		 
				success: function(response) {
					if(response == "true"){
						swal("save successful", "", "success");
						$("#gridOptModal").modal('hide');
					}else{
						swal("save failded", "", "error");
					}
					
				}
			});	
		});

		$("#clearLayout").on("click", function(){
			$.ajax({
				type: "POST",										
				url: BASE_API_URL + '/admin/baseApi/clearLayout',		
				data: { key: "excelOrderGrid" },		 
				success: function(response) {
					if(response == "true"){
						location.reload();
					}else{
						swal("save failded", "", "error");
					}
					
				}
			});	
		});

		$("#btnClear").on("click", function(){
			$.ajax({
				type: "GET",										
				url: BASE_URL + '/clearTmpOrder',		
				data: {},		 
				success: function(response) {
					if(response.msg == "success"){
						location.reload();
					}					
				}
			});
		});

		function loadState() {  	
			var loadURL = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/getLayoutJson') }}"; 	  	  	
						
			$.ajax({
				type: "GET", //  OR POST WHATEVER...
				url: loadURL,
				data: { key: 'excelOrderGrid' },		 
				success: function(response) {										
					if (response != "") {	
						response = JSON.parse(response);
					}
					var listSource = [];
					state = $("#jqxGrid").jqxGrid('getstate');
					$.each(state.columns, function(i, item) {
						if(item.text != "" && item.text != "undefined"){
							listSource.push({ 
							label: item.text, 
							value: i, 
							checked: !item.hidden });
						}
					});
					$("#jqxlistbox").jqxListBox({ 
						allowDrop: true, 
						allowDrag: true,
						source: listSource,
						width: "99%",
						height: 500,
						checkboxes: true,
						filterable: true,
						searchMode: 'contains'
					});
				}
			});			  	  	  	  	
		}
		$("#jqxGrid").on('bindingcomplete', function (event) {
						var statusHeight = 50;
						var winHeight = $( window ).height() - 350 + statusHeight;
						$("#jqxGrid").jqxGrid('height', winHeight+'px');
						//$("#"+gridId).jqxGrid("hideloadelement"); 
					});
    });

</script>

<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearLayout">{{ trans('common.clear') }}</button>
			</div>
		</div>
	</div>
</div>

@endsection