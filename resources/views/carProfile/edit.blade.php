@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      車輛建檔<small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix'),'carProfile') }}">{{ trans('modCar.titleName') }}</a></li>
        <li class="active">車輛建檔</li>
      </ol>
    </section>
@endsection

@section('content')
@include('backpack::template.toolbar')
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">車輛建檔</a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="cust_no">{{ trans('modCar.custNo') }}</label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="car_no">{{ trans('modCar.carNo') }}</label>
                                        <input type="text" class="form-control" id="car_no" name="car_no" placeholder="Enter No.">
                                    </div>
                                    <!-- <div class="form-group col-md-4">
                                        <label for="dlv_type">{{ trans('modCar.dlvType') }}</label>
                                        <select type="text" class="form-control" id="dlv_type" name="dlv_type" ></select>
                                    </div> -->
                                </div>

                                <div class="row">
                                    {{--  <div class="form-group col-md-4">
                                        <label for="car_type">{{ trans('modCar.carType') }}</label>
                                        <input type="text" class="form-control" id="car_type" name="car_type"  placeholder="Enter Car Type" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="car_type_nm">{{ trans('modCar.carTypeNm') }}</label>
                                        <input type="text" class="form-control" id="car_type_nm" name="car_type_nm">
                                    </div>  --}}
                                    <div class="form-group col-md-4">
                                        <label>{{ trans('modCar.carType') }}</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a Type" style="width: 100%;" name="car_type[]">
                                        </select>
                                    </div>  
                                    <div class="form-group col-md-4">
                                        <label for="car_ton">{{ trans('modCar.carTon') }}</label>
                                        <input type="text" class="form-control" id="car_ton" name="car_ton" >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="cbm">{{ trans('modCar.cbm') }}</label>
                                        <input type="number" class="form-control" id="cbm" name="cbm" min="0.01" max="9999999999999999.99">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="cbmu">{{ trans('modCar.cbmu') }}</label>
                                        <input type="text" class="form-control" id="cbmu" name="cbmu" >
                                    </div>
                                   <div class="form-group col-md-4">
                                        <label for="load_rate">{{ trans('modCar.loadRate') }}</label>
                                        <input type="number" class="form-control" id="load_rate" name="load_rate" min="0.01" max="100.00">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="load_weight">{{ trans('modCar.loadWeight') }}</label>
                                        <input type="number" class="form-control" id="load_weight" name="load_weight" min="0.01" max="9999999999999999.99">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="driver_no">{{ trans('modCar.driverNo') }}</label>
                                        <input type="text" class="form-control" id="driver_no" name="driver_no" >
                                    </div>
                                   <div class="form-group col-md-3">
                                        <label for="driver_nm">{{ trans('modCar.driverNm') }}</label>
                                        <input type="text" class="form-control" id="driver_nm" name="driver_nm" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="phone">{{ trans('modCar.phone') }}</label>
                                        <input type="text" class="form-control" id="phone" name="phone" >
                                    </div>
                                </div>
                                <div class="row">                                  
                                    <div class="form-group col-md-4">
                                        <label for="person_no">{{ trans('modCar.personNo') }}</label>
                                        <input type="text" class="form-control" id="person_no" name="person_no" >
                                    </div>
                                   <div class="form-group col-md-4">
                                        <label for="person_nm">{{ trans('modCar.personNm') }}</label>
                                        <input type="text" class="form-control" id="person_nm" name="person_nm" >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('modCar.createdBy') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('modCar.createdAt') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('modCar.updatedBy') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('modCar.updatedAt') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->                    
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>    


@endsection

@include('backpack::template.lookup')


@section('after_scripts')


<script>
    @if(!Auth::user()->hasRole('Admin'))
    $("#iAdd").remove();
    $("#iDel").remove();
    @endif
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/carProfile') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
    @endif

    
    
    @if(isset($id))
        mainId = "{{$id}}";
        editData = '{{!! $entry !!}}';
        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        editObj = JSON.parse(editData);
        cust_no= editObj.cust_no;
        console.log(editObj);
    @endif

  
   

    $(function(){

        $( "#lookupEvent" ).on( "custContactFunc", function(event,indexs) {
            /*$.each(indexs,function(k,v){
                alert(k+v);
            });*/
            //alert(1);
        });
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/carProfile') }}";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_car') }}/" + mainId;
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/carProfile') }}";
        
        formOpt.initFieldCustomFunc = function (){
            
        };
        //setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script>    

@endsection
