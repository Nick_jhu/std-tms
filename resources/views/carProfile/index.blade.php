@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      車輛建檔<small></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">車輛建檔</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.pageId        = "modCar";
gridOpt.enabledStatus = false;
gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_car') }}";
gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_car') }}";
gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/carProfile/create') }}";
gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/carProfile') }}" + "/{id}/edit";
gridOpt.height = 800;
gridOpt.searchOpt       = true;
var enabledheader = [] ;
var filtercolumndata = [];
var btnGroup = [
  {
    btnId: "btnSearchWindow",
    btnIcon: "fa fa-search",
    btnText: "{{ trans('common.search') }}",
    btnFunc: function () {
        $('#searchWindow').jqxWindow('open');
    }
  },
  {
    btnId: "btnExportExcel",
    btnIcon: "fa fa-cloud-download",
    btnText: "{{ trans('common.exportExcel') }}",
    btnFunc: function () {
      // $("#jqxGrid").jqxGrid('exportdata', 'json', '{{ trans("sysCustomers.titleName") }}', true, null, false, BASE_API_URL+'/admin/export/data');
      var url  =BASE_URL;
      url = url.replace("admin","");
      var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
      filtercolumndata = [];
      for (var i = 0; i < filterGroups.length; i++) {
          var filterGroup = filterGroups[i];
          var filters = filterGroup.filter.getfilters();
              for (var k = 0; k < filters.length; k++) {
                 if(filters[k].condition!="CONTAINS" && filters[k].condition!="NOT_EQUAL" ){
                  var filtercolumn = {
                  'column': filterGroup.filtercolumn,
                  'value': Date.parse(filters[k].value),
                  'condition': filters[k].condition
              };
              }else{
                  var filtercolumn = {
                  'column': filterGroup.filtercolumn,
                  'value': filters[k].value,
                  'condition': filters[k].condition
              };
              }
              filtercolumndata.push(filtercolumn);
          }
      }
      var sortdata = $("#jqxGrid").jqxGrid("getsortcolumns");
      var sorttype = "";
      var sortfield = "";
      for(i=0;i<sortdata.length;i++){
          if(sortdata[i].ascending==true){
              type="asc";
          }else{
              type="desc";
          }
          if(sortfield==""){
              sortfield = sortdata[i].dataField;
          }else{
              sortfield = sortfield+";"+sortdata[i].dataField;
          }
          if(sorttype==""){
              sorttype = type;
          }else{
              sorttype = sorttype+";"+type;
          }
      }
      $.post(BASE_API_URL + '/admin/export/data', {
          'table': 'mod_car',
          'filename':'車輛建檔' ,
          'columndata': filtercolumndata,
          'header': enabledheader,
          'sorttype': sorttype,
          'sortfield': sortfield,
      }, function(data){
          if(data.msg == "success") {
              window.location.href =url+"storage/excel/"+data.downlink;
          }
      });
    }
  },
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "{{ trans('common.gridOption') }}",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  @if(Auth::user()->hasRole('Admin'))
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"{{ trans('common.add') }}",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"{{ trans('common.delete') }}",
    btnFunc:function(){
        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        var ids = new Array();
        for (var m = 0; m < rows.length; m++) {
        var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
        if(typeof row != "undefined") {
            ids.push(row.id);
        }
        }
        if(ids.length == 0) {
          swal("請至少選擇一筆資料", "", "warning");
          return;
        }
        swal({
            title: "確認視窗",
            text: "您確定要刪除嗎？",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#dc3545',
            cancelButtonColor: '#6c757d',
            confirmButtonText: "確定",
            cancelButtonText: "取消"
        }).then((result) => {
            if (result.value) {
              $.post(BASE_URL + '/carProfile/multi/del', {'ids': ids}, function(data){
                if(data.msg == "success") {
                    swal("刪除成功", "", "success");
                    $("#jqxGrid").jqxGrid('updatebounddata');
                }
                else{
                    swal("操作失敗", "", "error");
                }
              });
            
            } else {
            
            }
        });
    }
  },
  @endif
];
</script>
@endsection

@include('backpack::template.search')
