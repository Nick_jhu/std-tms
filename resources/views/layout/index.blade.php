@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      Layout管理<small></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Layout管理</li>
      </ol>
    </section>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form action="" id="dlvForm">
                    <p id="warningText" style="color:red"></p>
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="fromuser">Layout From</label>
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" id="from_user" name="from_user">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="from_user"
                                    info1="{{Crypt::encrypt('users')}}" 
                                    info2="{{Crypt::encrypt('name,email')}}" 
                                    info3="{{Crypt::encrypt('')}}"
                                    info4="email=from_user;" triggerfunc="" selectionmode="singlerow">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </div>                           
                        </div>
                        <div class="form-group col-md-2">
                            <label for="touser">Layout To</label>
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" id="to_user" name="to_user">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="to_user"
                                    info1="{{Crypt::encrypt('users')}}" 
                                    info2="{{Crypt::encrypt('email')}}" 
                                    info3="{{Crypt::encrypt('')}}"
                                    info4="email=to_user;" triggerfunc="" selectionmode="singlerow">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </div>                           
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary" id="updateBtn">搜尋</button>
                    <button type="button" class="btn btn-primary" id="confirmBtn">複製</button>
                    <button type="button" class="btn btn-primary" id="btnLayout">{{ trans('excel.AdjustmentField') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div id="jqxGrid"></div>
    </div>
</div>

{{-- <div class="modal fade" tabindex="-1" role="dialog" id="dlvPlanModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">派車主檔</h4>
			</div>
			<div class="modal-body" id="dlvPlanBody">
				<div id="dlvPlanGrid"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="confirmInsertBtn">確定插入</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div> --}}

<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearLayout">{{ trans('common.clear') }}</button>
			</div>
		</div>
	</div>
</div>

@endsection

@include('backpack::template.lookup')


@section('after_scripts')

<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>


<script>
    var is_urgent ="N";
    function getCarType(carType) {
        $.get(BASE_URL+'/getCarType', {'carType': carType}, function(data){
            var str = "";

            for(i in data) {
                str += '<option value="'+data[i].cd+'">'+data[i].cd_descp+'</option>';
            }

            $("#car_type").html(str);
        }, "JSON");
    }
    $(function(){
        $('button[btnName="from_user"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('from_user', "{{ trans('dlvCar.car') }}", callBackFunc=function(data){
                getCarType(data.car_type);
                $("#carCbm").text(data.cbm);
                $("#loadRate").text((data.load_rate * 100) + '%');
            });
            refixwidth();
        });
        $('#from_user').on('click', function(){
            var check = $('#from_user').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("dlvForm","from_user",callBackFunc=function(data){
                    getCarType(data.car_type);
                    $("#carCbm").text(data.cbm);
                    $("#loadRate").text((data.load_rate * 100) + '%');
                });
            }
        });

        $('button[btnName="to_user"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('to_user', "{{ trans('dlvCar.car') }}", callBackFunc=function(data){
            });
            refixwidth();
        });
        $('#to_user').on('click', function(){
            var check = $('#to_user').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("dlvForm","to_user",callBackFunc=function(data){
                });
            }
        });


        function refixwidth () {
            setTimeout(function(){$('#lookupGrid').jqxGrid('autoresizecolumns');},1000);
        }

        $('button[btnName="cust_nm"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('cust_nm', "{{ trans('dlvCar.carDealer') }}");
        });

        $('#cust_nm').on('click', function(){
            var check = $('#cust_nm').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("dlvForm","cust_nm");
            }
        });

        

        $.jqx.theme = "bootstrap";
        var items = new Array();
        $.get(BASE_API_URL+'/admin/baseApi/getFieldsJson/sys_layout', function(data){
            var statusCode = [
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_customers?basecon=type;EQUAL;SELF&", label: "集團建檔"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_dlv_plan_view", label: "派車作業總覽 "},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_close", label: "結案管理"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_confirm", label: "回單作業"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order", label: "訂單作業"},                
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_car", label: "車輛建檔"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_detail_view", label: "訂單明細總覽"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_total_view", label: "訂單總覽(舊)"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJsonForMongo/mod_order", label: "訂單作業"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_area", label: "區域建檔"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_total_view as mod_order_total_view", label: "訂單總覽"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_users_view", label: "客戶使用者權限"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_dlv", label: "派車作業"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_country", label: "國家建檔"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/bscode_kind", label: "基本建檔"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_goods", label: "料號建檔"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_customers?basecon=type;EQUAL;OTHER&", label: "客戶建檔"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_view", label: "訂單作業(舊)"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_mi_view", label: "小米"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_customer_view?basecon=type;EQUAL;OTHER&", label: "客戶建檔"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_pchome", label: "Pchome"},
                {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_shippen_temp_view", label: "BenQ"},
                {value: "layoutGrid", label: "Layout管理"},
                {value: "excelOrderGrid", label: "訂單匯入"},
                {value: "dlvCarGrid", label: "派車作業"},
            ]

            var statusSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: statusCode
            };

            var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                autoBind: true
            });

            
            var datafields = [
                        { name: 'id', type: 'number' },
                        { name: 'key', type: 'string', value: 'key', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
                        { name: 'created_by', type: 'string' },     
                    ];
            // var baseCondition = "{{Crypt::encrypt(' (status=\'PICKED\'AND dlv_type=\'P\') or status=\'UNTREATED\'')}}";
            var baseCondition = "{{Crypt::encrypt(' (status=\'UNTREATED\' or ( dlv_type=\'P\' and status=\'PICKED\') )')}}";
            var source =
                {
                    datatype: "json",
                    datafields: datafields,
                    root:"Rows",
                    pagenum: 0,	
                    sortcolumn: 'id',
                    sortdirection: 'desc',	
                    beforeprocessing: function (data) {
                        var ostatus = "";
                        console.log("nick");
                        for(i in data[0].Rows) {
                            if(i == 0) {
                                ostatus = data[0].Rows[i].key;
                                items.push(data[0].Rows[i].key);
                            }

                            if(ostatus != data[0].Rows[i].key) {
                                items.push(data[0].Rows[i].key);
                            }

                            ostatus = data[0].Rows[i].key;
                        }
                        source.totalrecords = data[0].TotalRows;
                    },
                    filter: function () {
                    // update the grid and send a request to the server.
                        $("#jqxGrid").jqxGrid('updatebounddata', 'filter');
                    },
                    sort: function () {
                        // update the grid and send a request to the server.
                        $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
                    },
                    //url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order') }}" + "?basecon=status;EQUAL;UNTREATED)*(dlv_type;EQUAL;P&defOrder=etd;desc",					
                    // url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order') }}" + "?dbRaw="+baseCondition,	
                    url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/sys_layout') }}"+ "?basecon=created_by;EQUAL;'2'&",				
                    pagesize: 999,
                };
            var columns = [
                { text: "id", datafield: 'id', width: 50, hidden:true, filtertype: 'number' },
                { text: "使用者名稱", datafield: 'created_by', width:200, filtertype: 'textbox' },
                { text: "Layout名稱", datafield: 'key', width:200, filtertype: 'textbox' },
            ];
            /*var dataAdapter = new $.jqx.dataAdapter(source, {
                loadComplete: function () {

                }
            });*/
            var dataAdapter = new $.jqx.dataAdapter(source, { 
                async: false, 
                loadError: function (xhr, status, error) { 
                    alert('Error loading "' + source.url + '" : ' + error); 
                },
                loadComplete: function() {
                    $("#jqxGrid").jqxGrid({height: $( window ).height() - 350});
                }
            });
            $("#jqxGrid").jqxGrid(
            {
                width: '100%',
                height: $( window ).height() - 350,//'100%',//(typeof gridOpt.height == 'undefined')?800:gridOpt.height,
                //autoheight: true,
                source: dataAdapter,
                sortable: true,
                filterable: true,
                altrows: true,
                showfilterrow: true,
                pageable: true,
                virtualmode: true,
                autoshowfiltericon: true,
                columnsreorder: true,
                columnsresize: true,
                columnsautoresize: true,
                showstatusbar: true,
                pagesize: 99999999,
                //autoloadstate: true,
                clipboard: true,
                selectionmode: 'checkbox',
                enablebrowserselection: true,
                pagesizeoptions:[50, 100, 500, 9999],
                rendergridrows: function (params) {
                    //alert("rendergridrows");
                    return params.data;
                },
                renderstatusbar: function (statusbar) {
                    var rowCount = $("#jqxGrid").jqxGrid('getrows').length;
                    statusbar.append('<div style="margin: 5px;">總筆數: ' + rowCount + '</div>');
                },
                ready: function () {
                    loadState();
                    //$('#jqxGrid').jqxGrid('autoresizecolumns');
                },
                columns: columns
            });

            $("#jqxGrid").on('bindingcomplete', function (event) {
                var statusHeight = 50;
                var winHeight = $( window ).height() - 350 + statusHeight;
                $("#jqxGrid").jqxGrid('height', winHeight+'px');
            //$("#"+gridId).jqxGrid("hideloadelement"); 
            });

            if(typeof data[2] !== "undefined") {
                $("#jqxGrid").jqxGrid('loadstate', data[2]);
            }
            
        });
        

        var chkLoadRate = function(gridCbm) {
            var carCbm = parseInt($("#cbm").val());
            var loadRate = parseFloat($("#load_rate").val());

            var carLoadable = carCbm * loadRate;
            if(carCbm > 0) {
                if(gridCbm > carCbm) {
                    return -1;
                }
                else if(gridCbm > carLoadable) {
                    return 0;
                }
            }

            return 1;
        }

        $("#jqxGrid").on("rowselect", function (event) {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            if(chkLoadRate(sum) == 0) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("");
            }
            else if(chkLoadRate(sum) == -1) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("{{ trans('dlvCar.msg1') }}");
            }
            else {
                $("#gridCbm").css('color', '#000');
                $("#warningText").text("");
            }

            var cbm          = parseFloat($("#cbm").val());
            var loadRate     = parseFloat($("#load_rate").val());
            if(cbm) {
                var gridLoadRate = sum / cbm;
                $("#gridLoadRate").text((Math.round(gridLoadRate * 100)) + "%");

                if(gridLoadRate > loadRate) {
                    $("#gridLoadRate").css('color', 'red');
                }
                else {
                    $("#gridLoadRate").css('color', '#000');
                }
            }
            

            $("#gridCbm").text(sum.toFixed(2));
        });  

        $('#jqxGrid').on('rowunselect', function (event) 
        {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            if(chkLoadRate(sum) == 0) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("");
            }
            else if(chkLoadRate(sum) == -1) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("{{ trans('dlvCar.msg1') }}");
            }
            else {
                $("#gridCbm").css('color', '#000');
                $("#warningText").text("");
            }

            var cbm          = parseFloat($("#cbm").val());
            var loadRate     = parseFloat($("#load_rate").val());
            if(cbm) {
                var gridLoadRate = sum / cbm;
                $("#gridLoadRate").text((Math.round(gridLoadRate * 100)) + "%");

                if(gridLoadRate > loadRate) {
                    $("#gridLoadRate").css('color', 'red');
                }
                else {
                    $("#gridLoadRate").css('color', '#000');
                }
            }
            
            $("#gridCbm").text(sum.toFixed(2));
        });

        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        var m = (date.getMonth()+1);
        var d = date.getDate();
        if(m < 10) {
            m = '0' + m;
        }

        if(d < 10) {
            //d = '0' + (d+1);
            d = '0' + d;
        }
        $('#dlv_date').val(date.getFullYear() + '-' + m + '-' + d);
        $('#dlv_date').datepicker({
            startDate: today,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
        $("#updateBtn").on("click", function(){
        var usermail = $("#from_user").val();
        if(usermail =="" ){
            swal("請選擇一位user", "", "warning");
            return;
        }

        var statusCode = [
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_customers?basecon=type;EQUAL;SELF&", label: "集團建檔"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_dlv_plan_view", label: "派車作業總覽 "},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_close", label: "結案管理"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_confirm", label: "回單作業"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order", label: "訂單作業"},                
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_car", label: "車輛建檔"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_detail_view", label: "訂單明細總覽"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_total_view", label: "訂單總覽(舊)"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJsonForMongo/mod_order", label: "訂單作業"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_total_view as mod_order_total_view", label: "訂單總覽"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_users_view", label: "客戶使用者權限"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_dlv", label: "派車作業"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_area", label: "區域建檔"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_country", label: "國家建檔"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/bscode_kind", label: "基本建檔"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_goods", label: "料號建檔"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_view", label: "訂單作業(舊)"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_customers?basecon=type;EQUAL;OTHER&", label: "客戶建檔"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_mi_view", label: "小米"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/sys_customer_view?basecon=type;EQUAL;OTHER&", label: "客戶建檔"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_pchome", label: "Pchome"},
            {value: "https://utms.standard-info.com/api/admin/baseApi/getGridJson/mod_shippen_temp_view", label: "BenQ"},
            {value: "layoutGrid", label: "Layout管理"},
            {value: "dlvCarGrid", label: "派車作業"},
            {value: "excelOrderGrid", label: "訂單匯入"},
        ]
        var statusSource2 =
        {
                datatype: "array",
                datafields: [
                    { name: 'label', type: 'string' },
                    { name: 'value', type: 'string' }
                ],
                localdata: statusCode
        };

        var statusAdapter2 = new $.jqx.dataAdapter(statusSource2, {
            autoBind: true
        });
        var source2 =
            {
            datatype: "json",
            datafields: [
                { name: 'id', type: 'number' },
                { name: 'key', type: 'string', value: 'key', values: { source: statusAdapter2.records, value: 'value', name: 'label' } },
                { name: 'created_by', type: 'string' },     
            ],
            root:"Rows",
            pagenum: 0,
            pagesize: 999,
            cache: false,
            url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/sys_layout') }}" + "?basecon=created_by;EQUAL;"+usermail+"&"
            };
        var dataAdapter2 = new $.jqx.dataAdapter(source2);
        console.log(dataAdapter2);
        $('#jqxGrid').jqxGrid({source: dataAdapter2});
        });
        $("#confirmBtn").on("click", function(){
            // swal("功能製作中", "", "warning");
            // return;
            copylayout();
        });





        $("#btnLayout").on("click", function(){
			$('#gridOptModal').modal('show');
		});

        $("#saveGrid").on("click", function(){
			var items = $("#jqxlistbox").jqxListBox('getItems');
			$("#jqxGrid").jqxGrid('beginupdate');

			$.each(items, function(i, item) {
				var thisIndex = $('#jqxGrid').jqxGrid('getcolumnindex', item.value)-1;
				if(thisIndex != item.index){
					//console.log(item.value+":"+thisIndex+"="+item.index);
					$('#jqxGrid').jqxGrid('setcolumnindex', item.value,  item.index);
				}
				if (item.checked) {
					$("#jqxGrid").jqxGrid('showcolumn', item.value);
				}
				else {
					$("#jqxGrid").jqxGrid('hidecolumn', item.value);
				}
			})
			
			$("#jqxGrid").jqxGrid('endupdate');
			var state = $("#jqxGrid").jqxGrid('getstate');

			var saveUrl = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/saveLayoutJson') }}"; 	  	  	
			var stateToSave = JSON.stringify(state);

			$.ajax({
				type: "POST",										
				url: saveUrl,		
				data: { data: stateToSave,key: "layoutGrid" },		 
				success: function(response) {
					if(response == "true"){
						swal("save successful", "", "success");
						$("#gridOptModal").modal('hide');
					}else{
						swal("save failded", "", "error");
					}
					
				}
			});	
		});

		$("#clearLayout").on("click", function(){
			$.ajax({
				type: "POST",										
				url: BASE_API_URL + '/admin/baseApi/clearLayout',		
				data: { key: "layoutGrid" },		 
				success: function(response) {
					if(response == "true"){
						location.reload();
					}else{
						swal("save failded", "", "error");
					}
					
				}
			});	
		});
        
        var copylayout = function() {
            console.log("nick");
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var layoutData = [];
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                console.log(row);
                layoutData.push(row.id);
            }
            if(layoutData.length == 0) {
                swal("請至少選擇一筆資料", "", "warning");
                return;
            }
            if($("#to_user").val() == "") {
                swal("請選擇存入的USER", "", "warning");
                return;
            }
            var postData = {
                copytouser  : $("#to_user").val(),
                ids         : layoutData
            }
            $.ajax({
                url: "{{ url(config('backpack.base.route_prefix', 'admin').'/layout/copylayout') }}",
                type: 'POST',
                async: false,
                data: postData,
                beforeSend: function () {
                },
                error: function (jqXHR, exception) {
                    // swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    loadingFunc.hide();
                    return;
                },
                success: function (data) {
                    console.log(data);
                    if(data.msg == "success") {
                        swal('Success', "{{ trans('modOrder.sendSuccess') }}", "success");
                        
                        $('#jqxGrid').jqxGrid('clearselection');
                        $("#jqxGrid").jqxGrid('clearfilters');
                    }
                    else {
                        // swal("test", "{{ trans('modOrder.error') }}", "error");
                        swal({
                        title: data.errormsg+'layout 已存在',
                        text: "你要覆蓋過去嗎?",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '是的',
                        cancelButtonText: "取消"
                        }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                    url: "{{ url(config('backpack.base.route_prefix', 'admin').'/layout/copylayoutconfirm') }}",
                                    type: 'POST',
                                    async: false,
                                    data: postData,
                                    beforeSend: function () {
                                    },
                                success: function (data) {
                                    if(data.msg == "success") {
                                        swal('Success', "{{ trans('modOrder.sendSuccess') }}", "success");
                                        
                                        $('#jqxGrid').jqxGrid('clearselection');
                                        $("#jqxGrid").jqxGrid('clearfilters');
                                    }
                                    else {
                                        swal("test", "{{ trans('modOrder.error') }}", "error");
                                    }
                                    loadingFunc.hide();
                                    $('#jqxGrid').jqxGrid('updatebounddata');
                                    return;
                                }
                            });
                        }
                        })
                    }
                    loadingFunc.hide();
                    $('#jqxGrid').jqxGrid('updatebounddata');
                    return;
                }
            });
        }

        function loadState() {  	
			var loadURL = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/getLayoutJson') }}"; 	  	  	
			$.ajax({
				type: "GET", //  OR POST WHATEVER...
				url: loadURL,
				data: { key: 'layoutGrid' },		 
				success: function(response) {										
					if (response != "") {	
						response = JSON.parse(response);
					}
					var listSource = [];
					state = $("#jqxGrid").jqxGrid('getstate');
					$.each(state.columns, function(i, item) {
						if(item.text != "" && item.text != "undefined"){
							listSource.push({ 
							label: item.text, 
							value: i, 
							checked: !item.hidden });
						}
					});
					$("#jqxlistbox").jqxListBox({ 
						allowDrop: true, 
						allowDrag: true,
						source: listSource,
						width: "99%",
						height: 500,
						checkboxes: true,
						filterable: true,
						searchMode: 'contains'
					});
				}
			});			  	  	  	  	
		}
    });
</script>    
@endsection
