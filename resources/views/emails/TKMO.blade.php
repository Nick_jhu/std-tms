<p></p>
<p>{{$trackingData['custNm']}} 您好，</p>
<p>您的貨物(訂單號：{{$trackingData['ordNo']}} )已送達客戶指定地點。</p>
<p>貨物明細：</p>
@foreach($trackingData['ordDetail'] as $row)<p>{{$row-&gt;goods_no}} {{$row-&gt;goods_nm}} * {{$row-&gt;pkg_num}} &nbsp; &nbsp; {{$row-&gt;pkg_unit}} @endforeach</p>
<p>謝謝您</p>
<p><br></p>
<p><br></p>
<p>若您有倉儲物流服務的需求，歡迎隨時<span style="background-color: rgb(255, 255, 255);">聯繫我們。</span></p>
<p></p>
<hr>
<p style="text-align: right; ">Email delivery powered by <a href="https://www.standard-info.com/" target="_blank">使丹達資訊</a><br></p>