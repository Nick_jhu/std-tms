@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      貨況建檔<small></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">貨況建檔</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.enabledStatus = false;
gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_trans_status') }}";
gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_trans_status') }}";
gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modTransStatus/create') }}";
gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modTransStatus') }}" + "/{id}/edit";
gridOpt.height = 800;

var btnGroup = [
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"{{ trans('common.add') }}",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"{{ trans('common.delete') }}",
    btnFunc:function(){
      alert("Delete");
    }
  }
];
</script>
@endsection

@include('backpack::template.search')
