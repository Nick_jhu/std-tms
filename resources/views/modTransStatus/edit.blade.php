@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      貨況建檔<small></small>
      </h1>
      <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'),'modTransStatus') }}">{{ trans('modTransStatus.titleName') }}</a></li>
            <li class="active">貨況建檔</li>
      </ol>
    </section>
@endsection

@section('content')
@include('backpack::template.toolbar')
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">貨況建檔</a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="ts_type">{{ trans('modTransStatus.tsType') }}</label>
                                        <input type="text" class="form-control" id="ts_type" name="ts_type" placeholder="Enter Category" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="order">{{ trans('modTransStatus.order') }}</label>
                                        <input type="text" class="form-control" id="order" name="order" placeholder="Enter Order.">
                                    </div>                                   
                                </div>                                
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="ts_name">{{ trans('modTransStatus.tsName') }}</label>
                                        <input type="text" class="form-control" id="ts_name" name="ts_name" placeholder="Enter Name" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="ts_desc">{{ trans('modTransStatus.tsDesc') }}</label>
                                        <input type="text" class="form-control" id="ts_desc" name="ts_desc" placeholder="Enter Desc.">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="trigger_code">{{ trans('modTransStatus.triggerCode') }}</label>
                                        <input type="text" class="form-control" id="trigger_code" name="trigger_code" placeholder="Enter Code.">
                                    </div>                                    
                                </div>                                                    
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('modTransStatus.createdBy') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('modTransStatus.createdAt') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('modTransStatus.updatedBy') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('modTransStatus.updatedAt') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->                    
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>    


@endsection

@include('backpack::template.lookup')


@section('after_scripts')


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modTransStatus') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
    @endif

    
    
    @if(isset($id))
        mainId = "{{$id}}";
        editData = '{{!! $entry !!}}';
        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        editObj = JSON.parse(editData);
        cust_no= editObj.cust_no;
        console.log(editObj);
    @endif

  
   

    $(function(){

        $( "#lookupEvent" ).on( "custContactFunc", function(event,indexs) {
            /*$.each(indexs,function(k,v){
                alert(k+v);
            });*/
            alert(1);
        });
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modTransStatus') }}";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_trans_status') }}";
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modTransStatus') }}";
        
        formOpt.initFieldCustomFunc = function (){
            
        };                
    })
</script>    

@endsection
