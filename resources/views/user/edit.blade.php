@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      使用者管理<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix'),'custuser') }}">{{ trans('sysCustomers.titleName') }}</a></li>
		<li class="active">{{ trans('sysCustomers.titleAddName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('backpack::template.toolbar')
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">        
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <!-- <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">{{ trans('sysCustomers.baseInfo') }}</a></li>                     -->
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                            
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <input type="hidden" class="form-control" id="session_id" name="session_id">
                                        <label for="name">名稱</label>
                                        <input type="text" class="form-control" id="name" name="name" required="required">
                                    </div> 
                                    <div class="form-group col-md-3">
                                        <label for="email">電子郵件</label>
                                        <input type="text" class="form-control" id="email" name="email" required="required">
                                    </div>       
                                    <div class="form-group col-md-3">
                                        <label for="password">密碼</label>
                                        <input type="password" class="form-control" id="password" name="password">
                                    </div>     
                                    <div class="form-group col-md-3">
                                        <label for="password_confirmation">確認密碼</label>
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                    </div>                    
                                </div>
                                <div class="row"> 
                                    <div class="form-group col-md-3">
                                        <label for="g_key">集團</label>
                                        <input type="text" class="form-control" id="g_key" name="g_key" required="required" switch="off">
                                    </div>  
                                    <div class="form-group col-md-3">
                                        <label for="c_key">公司</label>
                                        <input type="text" class="form-control" id="c_key" name="c_key" required="required">
                                    </div>    
                                    <div class="form-group col-md-3">
                                        <label for="s_key">站別</label>
                                        <input type="text" class="form-control" id="s_key" name="s_key" required="required">
                                    </div>   
                                    <div class="form-group col-md-3">
                                        <label for="d_key">部門</label>
                                        <input type="text" class="form-control" id="d_key" name="d_key" required="required">
                                    </div>          
                                </div>  
                                <div class="row">   
                                    <div class="form-group col-md-3">
                                        <label for="status">啟用</label>
                                        <select class="form-control" id="account_enable" name="account_enable" no-clear="Y">
                                            <option value="N">否</option>
                                            <option value="Y">是</option>
                                        </select>
                                    </div>      
                                    <div class="form-group col-md-3">
                                            <label for="status">貨主</label>
                                            <select class="form-control select2" id="owner_cd" multiple="multiple" data-placeholder="Select a Owner" style="width: 100%;" name="owner_cd[]">
                                            </select>
                                        </div>   
                                </div> 
                                <div class="row"> 
                                    @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif             
                                </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>   

@endsection


@include('backpack::template.lookup')


@section('after_scripts')

<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/custuser') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
    @endif


    @if(isset($id))
        mainId = "{{$id}}";
        editData = '{{!! json_encode($entry["original"]) !!}}';

        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        //editObj = JSON.parse(editData);
        //cust_no= editObj.cust_no;
        //console.log(editObj);
    @endif


    $(function(){
        $("#iAdd").hide();
        $("#iCopy").hide();
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/custuser') }}";
        //formOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_customers') }}";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/users') }}/" + mainId;
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/custuser') }}";
        
        formOpt.initFieldCustomFunc = function (){
            $("select[name='state[]']").select2({tags: true});
        };

        formOpt.addFunc = function() {
            $("#status").val("B");
            $("#identity").prop("disabled", false);
            $("#identity").trigger("change");
        }
        formOpt.copyFunc = function() {
            $("#status").val("B");
            $("#identity").prop("disabled", false);
            $("#identity").trigger("change");
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);      


        // $("#identity").on("change", function(){
        //     var key = $(this).val();

        //     if(key == "C") {
        //         $("#group").show();
        //         $("#cmp").hide();
        //         $("#stn").hide();
        //     }
        //     else if(key == "S") {
        //         $("#group").show();
        //         $("#cmp").show();
        //         $("#stn").hide();
        //     }
        //     else if(key == "D") {
        //         $("#group").show();
        //         $("#cmp").show();
        //         $("#stn").show();
        //     }
        //     else {
        //         $("#group").hide();
        //         $("#cmp").hide();
        //         $("#stn").hide();
        //     }

        //     $.get(BASE_URL+'/gcsd/get', {}, function(data){
        //         if(data.gData.length > 0) {
        //             var opt = "";
        //             for(i in data.gData) {
        //                 opt += '<option value="'+data.gData[i].cust_no+'">'+data.gData[i].cname+'</option>'; 
        //             }
        //             $("#g_key").html(opt);
        //         }

        //         if(data.cData.length > 0) {
        //             var opt = "";
        //             for(i in data.cData) {
        //                 opt += '<option value="'+data.cData[i].cust_no+'">'+data.cData[i].cname+'</option>'; 
        //             }
        //             $("#c_key").html(opt);
        //         }

        //         if(data.sData.length > 0) {
        //             var opt = "";
        //             for(i in data.sData) {
        //                 opt += '<option value="'+data.sData[i].cust_no+'">'+data.sData[i].cname+'</option>'; 
        //             }
        //             $("#s_key").html(opt);
        //         }
        //     });
        // });
    })
</script>    

@endsection
