@extends('backpack::layout')
@section('header')
<section class="content-header">
	<h1>
		<!-- {{ trans('sysSite.titleName') }} -->
        使用者管理
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">{{ trans('sysSite.titleName') }}</li>
	</ol>
</section>
@endsection 
@section('before_scripts')


<script>
    var gridOpt = {};
    var filtervalue = "";
    var filterfield = "";
    var filtertype  = "";
    var enabledheader = [] ;
    var filtercolumndata = [];
    var headerdata = [];
    gridOpt.pageId        = "modUsersView";
    gridOpt.enabledStatus = false;
    gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_users_view') }}";
    gridOpt.dataUrl   = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_users_view') }}";
    gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/custuser/create') }}";
    gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/custuser') }}" + "/{id}/edit";
	gridOpt.searchOpt     = true;
    var btnGroup = [
        // {
        //     btnId: "btnSearchWindow",
        //     btnIcon: "fa fa-search",
        //     btnText: "{{ trans('common.search') }}",
        //     btnFunc: function () {
        //         $('#searchWindow').jqxWindow('open');
        //     }
        // },
        // {
        //     btnId: "btnExportExcel",
        //     btnIcon: "fa fa-cloud-download",
        //     btnText: "{{ trans('common.exportExcel') }}",
        //     btnFunc: function () {
        //         $("#jqxGrid").jqxGrid('exportdata', 'xls', '{{ trans("sysSite.titleName") }}');
        //     }
        // },
        // {
        //     btnId: "btnOpenGridOpt",
        //     btnIcon: "fa fa-table",
        //     btnText: "{{ trans('common.gridOption') }}",
        //     btnFunc: function () {
        //         $('#gridOptModal').modal('show');
        //     }
        // },
        // {
        //     btnId:"btnAdd",
        //     btnIcon:"fa fa-edit",
        //     btnText:"{{ trans('common.add') }}",
        //     btnFunc:function(){
        //         location.href= gridOpt.createUrl;
        //     }
        // }, 
		{
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        @can('NormalPermissions')
        {
            btnId:"btnDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"{{ trans('common.delete') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    ids.push(row.id);
                }
                }
                if(ids.length == 0) {
                swal("請至少選擇一筆資料", "", "warning");
                }
                $.post(BASE_URL + '/custuser/multi/del', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("刪除成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $("#jqxGrid").jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });
            }
        }
        @endcan
    ];

</script>
@endsection 
@include('backpack::template.search')