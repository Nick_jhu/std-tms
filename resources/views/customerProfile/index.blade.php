@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>   
        客戶建檔
      <small></small>
      </h1>
      <ol class="breadcrumb">
      <li class="active">客戶建檔</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.pageId        = "sysCustomer";
gridOpt.enabledStatus = false;
gridOpt.fieldsUrl     = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_customer_view') }}";
gridOpt.dataUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/sys_customer_view') }}" + "?basecon=type;EQUAL;OTHER&";
gridOpt.createUrl     = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile/create') }}";
gridOpt.editUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile') }}" + "/{id}/edit";
gridOpt.searchOpt     = true;
var enabledheader = [] ;
var filtercolumndata = [];
var btnGroup = [
  {
    btnId: "btnSearchWindow",
    btnIcon: "fa fa-search",
    btnText: "{{ trans('common.search') }}",
    btnFunc: function () {
        $('#searchWindow').jqxWindow('open');
    }
  },
  {
    btnId: "btnExportExcel",
    btnIcon: "fa fa-cloud-download",
    btnText: "{{ trans('common.exportExcel') }}",
    btnFunc: function () {
      var url  =BASE_URL;
      url = url.replace("admin","");
      var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
      filtercolumndata = [];
      for (var i = 0; i < filterGroups.length; i++) {
          var filterGroup = filterGroups[i];
          var filters = filterGroup.filter.getfilters();
              for (var k = 0; k < filters.length; k++) {
                 if(filters[k].condition!="CONTAINS" && filters[k].condition!="NOT_EQUAL" ){
                  var filtercolumn = {
                  'column': filterGroup.filtercolumn,
                  'value': Date.parse(filters[k].value),
                  'condition': filters[k].condition
              };
              }else{
                  var filtercolumn = {
                  'column': filterGroup.filtercolumn,
                  'value': filters[k].value,
                  'condition': filters[k].condition
              };
              }
              filtercolumndata.push(filtercolumn);
          }
      }
      var sortdata = $("#jqxGrid").jqxGrid("getsortcolumns");
      var sorttype = "";
      var sortfield = "";
      for(i=0;i<sortdata.length;i++){
          if(sortdata[i].ascending==true){
              type="asc";
          }else{
              type="desc";
          }
          if(sortfield==""){
              sortfield = sortdata[i].dataField;
          }else{
              sortfield = sortfield+";"+sortdata[i].dataField;
          }
          if(sorttype==""){
              sorttype = type;
          }else{
              sorttype = sorttype+";"+type;
          }
      }
      $.post(BASE_API_URL + '/admin/export/data', {
          'table': 'sys_customer_view',
          'filename':'客戶建檔' ,
          'columndata': filtercolumndata,
          'header': enabledheader,
          'sorttype': sorttype,
          'sortfield': sortfield,
      }, function(data){
          if(data.msg == "success") {
              window.location.href =url+"storage/excel/"+data.downlink;
          }
      });
    }
  },
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "{{ trans('common.gridOption') }}",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"{{ trans('common.add') }}",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"{{ trans('common.delete') }}",
    btnFunc:function(){
      var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
      var ids = new Array();
      for (var m = 0; m < rows.length; m++) {
        var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
        if(typeof row != "undefined") {
          ids.push(row.id);
        }
      }
      if(ids.length == 0) {
        swal("請至少選擇一筆資料", "", "warning");
        return;
      }

      swal({
        title: "確認視窗",
        text: "您確定要刪除嗎？",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#dc3545',
        cancelButtonColor: '#6c757d',
        confirmButtonText: "確定",
        cancelButtonText: "取消"
      }).then((result) => {
          if (result.value) {
            $.post(BASE_URL + '/custProfile/multi/del', {'ids': ids}, function(data){
              if(data.msg == "success") {
                swal("刪除成功", "", "success");
                $("#jqxGrid").jqxGrid('updatebounddata');
              }
              else{
                swal("操作失敗", "", "error");
              }
            });
          
          } else {
          
          }
      });
      
    }
  },
  @can('custftp')
  {
    btnId:"btncustexport",
    btnIcon:"fa fa-cloud-download",
    btnText:"轉出",
    btnFunc:function(){
      var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
      var ids = new Array();
      for (var m = 0; m < rows.length; m++) {
        var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
        if(typeof row != "undefined") {
          ids.push(row.id);
        }
      }

      if(ids.length == 0) {
        swal("請至少選擇一筆資料", "", "warning");
        return;
      }

      swal({
        title: "確認視窗",
        text: "您確定要轉出嗎？",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#dc3545',
        cancelButtonColor: '#6c757d',
        confirmButtonText: "確定",
        cancelButtonText: "取消"
      }).then((result) => {
          if (result.value) {
            $("#overlay").fadeIn();
            $(".cssload-wraper").show();
            $.post(BASE_URL + '/custProfile/multi/export', {'ids': ids}, function(data){
              if(data.msg == "success") {
                swal("成功", "", "success");
                $("#jqxGrid").jqxGrid('updatebounddata');
                $("#overlay").fadeOut();
                $(".cssload-wraper").hide();
              }
              else{
                swal("操作失敗", "", "error");
                $("#overlay").fadeOut();
                $(".cssload-wraper").hide();
              }
            });
          } else {
          
          }
      });
      
    }
  }
  @endcan
];


</script>
@endsection

@include('backpack::template.search')
