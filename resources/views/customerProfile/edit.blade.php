@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      客戶建檔<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix'),'customerProfile') }}">{{ trans('sysCustomers.titleName') }}</a></li>
		<li class="active">客戶建檔</li>
      </ol>
    </section>
    <form method="POST" accept-charset="UTF-8" id="myfileForm" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="file" id="excelFile" name="import_file" style="display:none;"/>
        <button type="submit" class="btn btn-primary" id="btnImport" style="display:none;">{{ trans('excel.btnImport') }}</button>
    </form>
@endsection

@section('content')
@include('backpack::template.toolbar')
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">        
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">{{ trans('sysCustomers.baseInfo') }}</a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cust_no">{{ trans('sysCustomers.custNo') }}</label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cname">{{ trans('sysCustomers.cname') }}</label>
                                        <input type="text" class="form-control" id="cname" name="cname">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="ename">{{ trans('sysCustomers.ename') }}</label>
                                        <input type="text" class="form-control" id="ename" name="ename">
                                    </div>    
                                    <div class="form-group col-md-3">
                                        <label for="tax_id">{{ trans('sysCustomers.taxId') }}</label>
                                        <input type="text" class="form-control" id="tax_id" name="tax_id">
                                    </div>                                  
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="email">{{ trans('sysCustomers.email') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input type="eamil" class="form-control" id="email" name="email" >
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cmp_abbr">{{ trans('sysCustomers.cmpAbbr') }}</label>
                                        <input type="text" class="form-control" id="cmp_abbr" name="cmp_abbr" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('sysCustomers.status') }}</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="A">{{ trans('sysCustomers.STATUS_A') }}</option>
                                            <option value="B">{{ trans('sysCustomers.STATUS_B') }}</option>
                                        </select>
                                    </div>     
                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('sysCustomers.receiveMail') }}</label>
                                        <select class="form-control" id="receive_mail" name="receive_mail">
                                            <option value="N">否</option>
                                            <option value="Y">是</option>
                                        </select>
                                    </div>                                                                   
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="contact">{{ trans('sysCustomers.contact') }}</label>
                                        <input type="text" class="form-control" id="contact" name="contact">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="phone">{{ trans('sysCustomers.phone') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" class="form-control" id="phone" name="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="phone">{{ trans('sysCustomers.phone') }}2</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" class="form-control" id="phone2" name="phone2" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="fax">{{ trans('sysCustomers.fax') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-fax"></i>
                                            </div>
                                            <input type="text" class="form-control" id="fax" name="fax">
                                        </div>
                                    </div>                                                                       
                                </div>                               
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="zip">{{ trans('sysCustomers.zip') }}</label>
                                        <input type="text" class="form-control" id="zip" name="zip">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="city_nm">{{ trans('sysCustomers.cityNm') }}{{ trans('sysCustomers.areaNm') }}</label>
                                        <input type="text" class="form-control" id="custer_info" name="custer_info">
                                        <input type="hidden" class="form-control" id="city_nm" name="city_nm">
                                        <input type="hidden" class="form-control" id="area_nm" name="area_nm">
                                    </div>
                                    <!-- <div class="form-group col-md-3">
                                        <label for="area_nm">{{ trans('sysCustomers.areaNm') }}</label>
                                        <input type="hidden" class="form-control" id="area_id" name="area_id">
                                        <input type="text" class="form-control" id="area_nm" name="area_nm">
                                    </div> -->
                                    <div class="form-group col-md-3">
                                        <label for="def">{{ trans('sysCustomers.def') }}</label>
                                        <select type="text" class="form-control" id="def" name="def">
                                            <option value="0">否</option>
                                            <option value="1">是</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>{{ trans('sysCustomers.custType') }}</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a Type" style="width: 100%;" name="cust_type[]">
                                        </select>
                                    </div>
                                <!-- <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="email_type">{{ trans('sysCustomers.emailType') }}</label>
                                        <select type="text" class="form-control" id="email_type" name="email_type">
                                            <option value="B">提+配</option>
                                            <option value="P">提貨</option>
                                            <option value="D">配送</option>
                                        </select>
                                    </div> -->
                                    <!-- <div class="form-group col-md-3">
                                    <label for="industry">{{ trans('sysCustomers.industry') }}</label>
                                        <input type="text" class="form-control" id="industry" name="industry">
                                    </div>
                                    <div class="form-group col-md-3">
                                    <label for="zip_code">{{ trans('sysCustomers.zipCode') }}</label>
                                        <input type="text" class="form-control" id="zip_code" name="zip_code">
                                    </div>
                                    <div class="form-group col-md-3">
                                    <label for="website">{{ trans('sysCustomers.website') }}</label>
                                        <input type="text" class="form-control" id="website" name="website">
                                    </div> -->
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-9">
                                        <label for="address">{{ trans('sysCustomers.address') }}</label>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="email_type">{{ trans('sysCustomers.emailType') }}</label>
                                        <select type="text" class="form-control" id="email_type" name="email_type">
                                            <option value="B">提+配</option>
                                            <option value="P">提貨</option>
                                            <option value="D">配送</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="en_address">{{ trans('sysCustomers.enAddress') }}</label>
                                        <input type="text" class="form-control" id="en_address" name="en_address" placeholder="Enter Address">
                                    </div>
                                </div> -->
                                @can('custftp')
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>FTP網址</label>
                                            <input type="text" class="form-control" id="ftppath" name="ftppath">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>FTP PORT</label>
                                            <input type="text" class="form-control" id="ftpport" name="ftpport">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>FTP帳號</label>
                                            <input type="text" class="form-control" id="ftpuser" name="ftpuser">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>FTP密碼</label>
                                            <input type="text" class="form-control" id="ftppassword" name="ftppassword">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>FTP檔案位置</label>
                                            <input type="text" class="form-control" id="ftpforder" name="ftpforder">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="ftptype">發送方式</label>
                                        <select type="text" class="form-control" id="ftptype" name="ftptype">
                                            <option value="E">EXCEL</option>
                                            <option value="M">MAIL</option>
                                            <option value="EM">EXCEL+MAIL</option>
                                            <option value="N">不發送</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="ftpcat">FTP/SFTP</label>
                                        <select type="text" class="form-control" id="ftpcat" name="ftpcat">
                                            <option value="SFTP">SFTP</option>
                                            <option value="FTP">FTP</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>貨況發送mail</label>
                                        <input type="text" class="form-control" id="sendmail" name="sendmail">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ trans('sysCustomers.remark') }}</label>
                                            <textarea class="form-control" rows="3" id="remark" name="remark" placeholder="Enter ..."></textarea>
                                        </div>
                                    </div>
                                </div>
                                @endcan

                                {{--  <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('sysCustomers.createdBy') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('sysCustomers.createdAt') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('sysCustomers.updatedBy') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('sysCustomers.updatedAt') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>  --}}
                                                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </form>

            <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
                <ul class="nav nav-tabs">
                    @can('custfee')
                    <li class="active"><a href="#tab_6" data-toggle="tab" aria-expanded="false">其他服務項目</a></li>
                    @endcan
                    @can('custerrorreport')
                    <li class=""><a href="#tab_8" data-toggle="tab" aria-expanded="false">異常回報</a></li>
                    @endcan
                    {{-- <li class=""><a href="#tab_9" data-toggle="tab" aria-expanded="false">偏遠地區設定</a></li> --}}
                </ul>
                <div class="tab-content">
                    @can('custfee')
                    <div class="tab-pane active" id="tab_6">
                        <div class="box box-primary" id="subBox" style="display:none">
                            <div class="box-header with-border">
                                <h3 class="box-title">其他服務項目</h3>
                            </div>
                            <form method="POST" accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">

                                        <div class="form-group col-md-2">
											<label for="goodsno">商品料號</label>
											<div class="input-group input-group-sm">
												<input type="text" class="form-control" id="goodsno" name="goodsno">
												<span class="input-group-btn">
													<button type="button" class="btn btn-default btn-flat lookup" btnname="goodsno"
														info1="{{Crypt::encrypt('mod_goods')}}" 
														info2="{{Crypt::encrypt('goods_no+goods_nm,goods_no,goods_nm,gw,gwu,cbm,cbmu,g_length,g_height,g_width,goods_no2,price')}}" 
														@if(isset($ownerinfo3)) 
                                                        info3="{{$ownerinfo3}}"
                                                        @endif
														info4="goods_no=goodsno;" triggerfunc="" selectionmode="singlerow">
														<i class="fa fa-search" aria-hidden="true"></i>
													</button>
												</span>
											</div>
										</div>

                                        <div class="form-group col-md-2">
                                            <label for="goods_no">其他服務項目名稱</label>
                                            {{--  <input type="text" class="form-control input-sm" name="goods_no" grid="true" >  --}}
                                            <div class="input-group input-group-sm">
                                                {{-- <input type="hidden" class="form-control" name="fee_cd" id="fee_cd"> --}}
                                                <input type="text" class="form-control" id="fee_name" name="fee_name">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="goods_no"
                                                        info1="{{Crypt::encrypt('bscode')}}" 
                                                        info2="{{Crypt::encrypt('cd+cd_descp,cd,cd_descp')}}" 
                                                        info3="{{Crypt::encrypt('cd_type=\'EXTRAFEE\'')}}"
                                                        info4="cd_descp=fee_name;cd=fee_cd;" triggerfunc="" selectionmode="singlerow">                                                        
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="fee_amount">費用金額</label>
                                            <input type="number" class="form-control input-sm" name="fee_amount" grid="true" >
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="fee_cd">費用代碼</label>
                                            <input type="text" class="form-control input-sm" name="fee_cd" id="fee_cd" grid="true" >
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
    
                                <div class="box-footer">
                                    @if(isset($id))
                                        <input type="hidden" class="form-control input-sm noClear"  name="cust_id" value="{{$id}}" grid="true">
                                    @endif
                                    <input type="hidden" class="form-control input-sm" name="id" grid="true">
                                    <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                                </div>
                            </form>
                        </div>
    
                        <div id="jqxGrid"></div>
                    </div>
                    @endcan
                    <div class="tab-pane" id="tab_9">
						<div class="box box-primary" id="subBox3" style="display:none">
							<div class="box-header with-border">
								<h3 class="box-title">偏遠地區設定</h3>
							</div>
							<form method="POST" accept-charset="UTF-8" id="subForm3" enctype="multipart/form-data">
								<!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<div class="form-group col-md-2">
											<label for="fee_name">{{ trans('modOrder.feeName') }}</label>
											<div class="input-group input-group-sm">
												<input type="hidden" class="form-control" name="fee_cd" id="fee_cd">
												<input type="text" class="form-control" id="fee_name" name="fee_name">
												<span class="input-group-btn">
													<button type="button" class="btn btn-default btn-flat lookup" btnname="fee_name"
														info1="{{Crypt::encrypt('sys_area')}}" 
														info2="{{Crypt::encrypt('dist_cd+dist_nm,dist_cd,dist_nm')}}" 
														info3="{{Crypt::encrypt('g_key=\'Auth::user()->g_key\'')}}"
														info4="cd_descp=fee_name;cd=fee_cd;" triggerfunc="" selectionmode="selectionmode">
														<i class="fa fa-search" aria-hidden="true"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="form-group col-md-2">
											<label for="fee_descp">{{ trans('modOrder.feeDescp') }}</label>
											<input type="text" class="form-control input-sm" name="fee_descp" grid="fee_descp" >
										</div>
										<div class="form-group col-md-2">
											<label for="amount">{{ trans('modOrder.amount') }}</label>
											<input type="number" class="form-control input-sm" id="amount" name="amount" grid="true" >
										</div>
									</div>
								</div>
								<!-- /.box-body -->

								<div class="box-footer">
									<input type="hidden" class="form-control input-sm" name="id" grid="true">
									<button type="button" class="btn btn-sm btn-primary" id="Save3">{{ trans('common.save') }}</button>
									<button type="button" class="btn btn-sm btn-danger" id="Cancel3">{{ trans('common.cancel') }}</button>
								</div>
							</form>
						</div>
						<div id="jqxGrid2"></div>
					</div>
                    @can('custerrorreport')
					<div class="tab-pane" id="tab_8">
						<div class="box box-primary" id="subBox8" style="display:none">
							<div class="box-header with-border">
								<h3 class="box-title">異常回報</h3>
							</div>
							<form method="POST" accept-charset="UTF-8" id="subForm8" enctype="multipart/form-data">
								<!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<div class="form-group col-md-3">
                                            {{-- <input type="text" class="form-control input-sm" name="cd" grid="true" > --}}
                                            <label for="cd">代碼</label>
											{{--  <input type="text" class="form-control input-sm" name="goods_no" grid="true" >  --}}
											<div class="input-group input-group-sm">
												<input type="text" class="form-control" id="cd" name="cd">
												<span class="input-group-btn">
													<button type="button" class="btn btn-default btn-flat lookup" btnname="cd"
														info1="{{Crypt::encrypt('bscode')}}" 
														info2="{{Crypt::encrypt('cd,cd_descp')}}" 
														info3="{{Crypt::encrypt('cd_type=\'ERRORTYPE\'')}}"
														info4="goods_no=goods_no;goods_nm=goods_nm;" triggerfunc="" selectionmode="singlerow">
														<i class="fa fa-search" aria-hidden="true"></i>
													</button>
												</span>
											</div>

										</div>
										<div class="form-group col-md-3">
											<label for="cd_descp">名稱</label>
											<input type="text" class="form-control input-sm" id="cd_descp" name="cd_descp" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="sorted">順序</label>
											<input type="text" class="form-control input-sm" name="sorted" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="remark">備註</label>
											<input type="text" class="form-control input-sm" name="remark" grid="true" >
										</div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
											<label for="remark">訂單狀態</label>
                                            <select class="form-control" id="order_status" name="order_status">
                                                <option value="ERROR">配送發生問題</option>
                                                <option value="FINISHED">配送完成</option>
                                                <option value="REJECT">拒收</option>
                                            </select>
										</div>
                                    </div>
								</div>
								<!-- /.box-body -->

								<div class="box-footer">
									<input type="hidden" class="form-control input-sm" name="id" grid="true">
									<button type="button" class="btn btn-sm btn-primary" id="errorSave">{{ trans('common.save') }}</button>
									<button type="button" class="btn btn-sm btn-danger" id="errorCancel">{{ trans('common.cancel') }}</button>
								</div>
							</form>
						</div>
						<div id="errorGrid"></div>
					</div>
                    @endcan
                </div>
                <!-- /.tab-content -->
            </div>

        </div>
</div>   

@endsection


@include('backpack::template.lookup')


@section('after_scripts')

<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
    @endif


    @if(isset($id))
        mainId = "{{$id}}";
        editData = '{{!! $entry !!}}';

        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
        //editObj = JSON.parse(objJson);
        //cust_no= editObj.cust_no;
    @endif


    $(function(){


        $('#subBox8 input[name="cd"]').on('click', function(){
            var check = $('#subBox8 input[name="cd"]').data('ui-autocomplete') != undefined;
            var orcd = $("#cd").val();
            var orcd_descp = $("#cd").val();
            if(check == false) {
                initAutocomplete("subForm8","cd",callBackFunc=function(data){
                    $("#cd").val(data.cd);
                    $("#cd_descp").val(data.cd_descp);
                    if(data.cd==""||data.cd==null ){
                        $("#cd").val(orcd);
                        $("#cd_descp").val(orcd_descp);
                    }
                },"cd");
            }
        });
        $('#subBox input[name="goodsno"]').on('click', function(){
            var check = $('#subBox8 input[name="goodsno"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("subForm","goodsno",callBackFunc=function(data){
                    $("#goodsno").val(data.cd);
                    // $("#cd_descp").val(data.cd_descp);
                },"goodsno");
            }
        });

        $('#subBox8 button[btnName="cd"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('cd', "異常類型", callBackFunc=function(data){
                $("#cd").val(data.cd);
                $("#cd_descp").val(data.cd_descp);
            });
        });
        
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile') }}";
        //formOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_customers') }}";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/sys_customers') }}/" + mainId;
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile') }}";
        
        formOpt.initFieldCustomFunc();
		$.get( formOpt.fieldsUrl , function( data ) {
            $("select[name='state[]']").select2({tags: true});
            $('#custer_info').val(data['city_nm']+data['area_nm']) ;
            if($('#custer_info').val()==0)
            $('#custer_info').val("");
        });
        formOpt.saveErrorFunc = function(data) {
            var msg =data.errorLog;
            swal("警告", msg, "warning");
        }
        formOpt.addFunc = function() {
            $("#status").val("B");
        }

        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);   
        $('#subBox button[btnName="goods_no"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('goods_no', "代收款建檔", callBackFunc=function(data){
            });
        });

        $('#subBox button[btnName="goodsno"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('goodsno', "料號建檔", callBackFunc=function(data){
            });
        });
        var btnGroup = [
            @can('custfee')
				{
					btnId: "btnUploadExcel",
					btnIcon: "fa fa-cloud-upload",
					btnText: "服務項目匯入",
					btnFunc: function () {
                        $("#excelFile").click();
					}
				}
            @endcan
        ];

        initBtn(btnGroup);
        @if(isset($id))
        $.ajax({
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {

					var col = [
						[
							{name: "id", type: "number"},
							{name: "fee_cd", type: "string"},
							{name: "fee_name", type: "string"},
							{name: "fee_amount", type: "string"},
                            {name: "goodsno", type: "string"},
						],
						[
							{text: "id", datafield: "id", width: 100, hidden: true},
                            {text: "商品料號", datafield: "goodsno", width: 300},
                            {text: "費用代碼", datafield: "fee_cd", width: 300},
							{text: "服務項目名稱", datafield: "fee_name", width: 130},
							{text: "金額", datafield: "fee_amount", width: 300},
						]
					];
					
					var opt = {};
					opt.gridId = "jqxGrid";
					opt.fieldData = col;
					opt.formId = "subForm";
					opt.saveId = "Save";
					opt.cancelId = "Cancel";
					opt.showBoxId = "subBox";
					opt.height = 300;
					opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/customerProfile/get') }}" + '/' + mainId;
                    opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile') }}" + "/store";
					opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile') }}" + "/update";
					opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile') }}" + "/delete/";
					opt.commonBtn = true;
                    opt.showtoolbar = true;
					opt.defaultKey = {
						'cust_id': mainId
					};
					opt.beforeSave = function (row) {
                        if(row.fee_cd == "" || row.fee_cd == null) {
							swal("警告", "費用代碼不可為空", "warning");
							return false;
						}                        
                        if(row.fee_name == "" || row.fee_name == null) {
							swal("警告", "服務項目名稱不可為空", "warning");
							return false;
						}
						return true;
					}

					opt.afterSave = function (data) {
                        if(data.msg=="error"){
                            swal("警告", data.errorLog, "warning");
                        }
					}

					opt.beforeCancel = function() {
					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
            });   
            
            $.ajax({
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {

					var col = [
						[
							{name: "id", type: "number"},
							{name: "cd", type: "string"},
                            {name: "cd_descp", type: "string"},
                            {name: "sorted", type: "string"},
                            {name: "remark", type: "string"},
                            {name: "order_status", type: "string"},
						],
						[
							{text: "id", datafield: "id", width: 100, hidden: true},
							{text: "代碼", datafield: "cd", width: 130},
                            {text: "名稱", datafield: "cd_descp", width: 130},
                            {text: "順序", datafield: "sorted", width: 130},
                            {text: "備註", datafield: "remark", width: 130},
                            {text: "訂單狀態", datafield: "order_status", width: 130},
						]
					];
					
					var opt = {};
					opt.gridId = "errorGrid";
					opt.fieldData = col;
					opt.formId = "subForm8";
					opt.saveId = "errorSave";
					opt.cancelId = "errorCancel";
					opt.showBoxId = "subBox8";
					opt.height = 300;
					opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/customererrorProfile/get') }}" + '/' + mainId;
                    opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customererrorProfile') }}" + "/store";
					opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customererrorProfile') }}" + "/update";
					opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customererrorProfile') }}" + "/delete/";
					opt.commonBtn = true;
                    opt.showtoolbar = true;
					opt.defaultKey = {
						'cust_id': mainId
					};
					opt.beforeSave = function (row) {
					}

					opt.afterSave = function (data) {
                        if(data.msg=="error"){
                            swal("警告", data.errorLog, "warning");
                        }
					}

					opt.beforeCancel = function() {
					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
            }); 


            $.ajax({
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {

					var col = [
						[
							{name: "id", type: "number"},
							{name: "longway_no", type: "string"},
							{name: "longway_price", type: "string"},
						],
						[
							{text: "id", datafield: "id", width: 100, hidden: true},
							{text: "偏遠地區代碼", datafield: "longway_no", width: 300, hidden: true},
							{text: "偏遠地區費用", datafield: "longway_price", width: 130}
						]
					];
					
					var opt = {};
					opt.gridId = "jqxGrid2";
					opt.fieldData = col;
					opt.formId = "subForm3";
					opt.saveId = "Save3";
					opt.cancelId = "Cancel3";
					opt.showBoxId = "subBox3";
					opt.height = 300;
					opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/longway/get') }}" + '/' + mainId;
                    opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/longway') }}" + "/store";
					opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/longway') }}" + "/update";
					opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/longway') }}" + "/delete/";
					opt.commonBtn = true;
                    opt.showtoolbar = true;
					opt.defaultKey = {
						'cust_id': mainId
					};
					opt.beforeSave = function (row) {
					}

					opt.afterSave = function (data) {
                        if(data.msg=="error"){
                            swal("警告", data.errorLog, "warning");
                        }
					}

					opt.beforeCancel = function() {
					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			}); 
    @endif
    $(function(){
            $("#myfileForm").submit(function () {

                var postData = new FormData($(this)[0]);
                console.log(mainId);
                $.ajax({
                    url: BASE_URL + "/customerFee/importExcel/"+mainId,
                    type: 'POST',
                    data: postData,
                    async: false,
                    beforeSend: function () {
                        
                    },
                    error: function () {
                        swal("excel 匯入發生錯誤", "", "error");
                        $('#myfileForm')[0].reset();
                        return false;
                    },
                    success: function (data) {
                        if(data.msg == "error") {
                            swal(data.errorMsg, "", "error");
                            $('#myfileForm')[0].reset();
                            return;
                        }
                        
                        swal("匯入成功", "", "success");
                        $('#myfileForm')[0].reset();
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        // $("#jqxGrid").jqxGrid('clearselection');
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
                return false;
            });
            
            $("#excelFile").on("change", function(){
                $("#myfileForm").submit();
            });
        })
    })
</script>    

@endsection
