@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      區域建檔<small></small>
      </h1>
      <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'),'sysArea') }}">區域建檔</a></li>
            <li class="active">{{ trans('sysArea.titleAddName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('backpack::template.toolbar')
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">{{ trans('sysArea.titleAddName') }}</a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="cntry_cd">{{ trans('sysArea.cntryCd') }}</label>
                                        <input type="text" class="form-control" id="cntry_cd" name="cntry_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="cntry_nm">{{ trans('sysArea.cntryNm') }}</label>
                                        <input type="text" class="form-control" id="cntry_nm" name="cntry_nm" placeholder="Enter No.">
                                    </div>                                   
                                </div>                                
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="state_cd">{{ trans('sysArea.stateCd') }}</label>
                                        <input type="text" class="form-control" id="state_cd" name="state_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="state_nm">{{ trans('sysArea.stateNm') }}</label>
                                        <input type="text" class="form-control" id="state_nm" name="state_nm" placeholder="Enter No.">
                                    </div>                                   
                                </div>       
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="city_cd">{{ trans('sysArea.cityCd') }}</label>
                                        <input type="text" class="form-control" id="city_cd" name="city_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="city_nm">{{ trans('sysArea.cityNm') }}</label>
                                        <input type="text" class="form-control" id="city_nm" name="city_nm" placeholder="Enter No.">
                                    </div>                                   
                                </div>       
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="dist_cd">{{ trans('sysArea.distCd') }}</label>
                                        <input type="text" class="form-control" id="dist_cd" name="dist_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="dist_nm">{{ trans('sysArea.distNm') }}</label>
                                        <input type="text" class="form-control" id="dist_nm" name="dist_nm" placeholder="Enter No.">
                                    </div>                                   
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="zip_f">{{ trans('sysArea.zipF') }})</label>
                                        <input type="text" class="form-control" id="zip_f" name="zip_f" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="zip_e">{{ trans('sysArea.zipE') }}</label>
                                        <input type="text" class="form-control" id="zip_e" name="zip_e" placeholder="Enter No.">
                                    </div>                                   
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ trans('sysArea.remark') }}</label>
                                            <textarea class="form-control" rows="3" name="remark" placeholder="Enter ..."></textarea>
                                        </div>
                                    </div>
                                </div>              
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('sysArea.createdBy') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('sysArea.createdAt') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('sysArea.updatedBy') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('sysArea.updatedAt') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->                    
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>    


@endsection

@include('backpack::template.lookup')


@section('after_scripts')


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysArea') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
    @endif

    
    
    @if(isset($id))
        mainId = "{{$id}}";
        editData = '{{!! $entry !!}}';
        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        editObj = JSON.parse(editData);
        cust_no= editObj.cust_no;
        console.log(editObj);
    @endif

  
   

    $(function(){

        $( "#lookupEvent" ).on( "custContactFunc", function(event,indexs) {
            /*$.each(indexs,function(k,v){
                alert(k+v);
            });*/
            alert(1);
        });
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysArea') }}";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/sys_area') }}/" + mainId;
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysArea') }}";
        
        formOpt.initFieldCustomFunc = function (){
            
        };                
    })
</script>    

@endsection
