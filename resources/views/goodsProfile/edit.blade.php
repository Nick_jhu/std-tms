@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      商品建檔<small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix'),'carProfile') }}">{{ trans('modGoods.titleName') }}</a></li>
        <li class="active">商品建檔</li>
      </ol>
    </section>
@endsection

@section('content')
@include('backpack::template.toolbar')
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">商品建檔</a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
										<label for="owner_cd">{{ trans('modOrder.ownerCd') }}</label>
										<input type="text" class="form-control" id="owner_cd" name="owner_cd">
									</div>
                                    <div class="form-group col-md-3">
										<label for="owner_nm">{{ trans('modOrder.ownerNm') }}</label>
										<input type="text" class="form-control" id="owner_nm" name="owner_nm">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="price">費用</label>
                                        <input type="number" class="form-control" id="price" name="price">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="pkg_unit">數量單位</label>
                                        <input type="text" class="form-control" id="pkg_unit" name="pkg_unit">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="goods_no">{{ trans('modGoods.goodsNo') }}</label>
                                        <input type="text" class="form-control" id="goods_no" name="goods_no" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="goods_nm">{{ trans('modGoods.goodsNm') }}</label>
                                        <input type="text" class="form-control" id="goods_nm" name="goods_nm">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="goods_no2">{{ trans('modGoods.goodsNo2') }}</label>
                                        <input type="text" class="form-control" id="goods_no2" name="goods_no2">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="goods_nm2">{{ trans('modGoods.goodsNm2') }}</label>
                                        <input type="text" class="form-control" id="goods_nm2" name="goods_nm2">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="gw">{{ trans('modGoods.gw') }}</label>
                                        <input type="text" class="form-control" id="gw" name="gw" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="gwu">{{ trans('modGoods.gwu') }}</label>
                                        <input type="text" class="form-control" id="gwu" name="gwu">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cbm">{{ trans('modGoods.cbm') }}</label>
                                        <input type="text" class="form-control" id="cbm" name="cbm">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cbmu">{{ trans('modGoods.cbmu') }}</label>
                                        <input type="text" class="form-control" id="cbmu" name="cbmu">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="g_length">{{ trans('modGoods.gLength') }}</label>
                                        <input type="text" class="form-control" id="g_length" name="g_length" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="g_width">{{ trans('modGoods.gWidth') }}</label>
                                        <input type="text" class="form-control" id="g_width" name="g_width">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="g_height">{{ trans('modGoods.gHeight') }}</label>
                                        <input type="text" class="form-control" id="g_height" name="g_height">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="goods_type_nm">費用類別</label>
                                        <input type="hidden" class="form-control" id="goods_type_cd" name="goods_type_cd">
										<input type="text" class="form-control" id="goods_type_nm" name="goods_type_nm">
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="sn_flag">是否刷序號</label>
                                        <select class="form-control" id="sn_flag" name="sn_flag" >
                                            <option value="Y">是</option>
                                            <option value="N">否</option>
                                        </select>
                                    </div>
                                </div>

                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->                    
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>    


@endsection

@include('backpack::template.lookup')


@section('after_scripts')


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/goodsProfile') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
    @endif

    
    
    @if(isset($id))
        mainId = "{{$id}}";
        //editData = '{{!! $entry !!}}';
        //editData = editData.substring(1);
		//editData = editData.substring(0, editData.length - 1);
		//var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
		//editObj  = JSON.parse(objJson);
        //cust_no= editObj.cust_no;
        console.log(editObj);
    @endif

  
   

    $(function(){
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/goodsProfile') }}";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_goods') }}/" + mainId;
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/goodsProfile') }}";
        
        formOpt.initFieldCustomFunc = function (){
            
        };
        formOpt.addFunc = function() {
            $("#sn_flag").val("Y");
        }
        //setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script>    

@endsection
