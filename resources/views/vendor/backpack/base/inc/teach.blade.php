<div class="teach-btn">
  <button type="button" id="teachBtn"><i class="fa fa-question" aria-hidden="true"></i></button>
</div>
<style>
  .teach-btn {
    position: fixed;
    right: 15px;
    bottom: 15px;
    z-index: 10000;
  }
  .teach-btn button {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 48px;
    height: 48px;
    font-size: 20px;
    border-radius: 50%;
    background: #3c8dbc;
    color: #fff;
    box-shadow: 0 0.1875rem 0.1875rem 0 rgba(0, 0, 0, 0.1);
    border: 0px solid transparent;
    outline: 0px solid transparent;
  }
</style>
<script>
  // console.log(location.href);
  window.onload = function(){
    let routArr = location.href.split('/') || [];
    let routArrLength = routArr.length;
    // console.log(routArr);
    let teachBtn = document.getElementById('teachBtn');
    teachBtn.addEventListener('click', function() {
      swal({
            title: "教學",
            text: `以下為${routArr[4]}頁教學`,
            showCancelButton: true,
            confirmButtonColor: '#dc3545',
            cancelButtonColor: '#6c757d',
            confirmButtonText: "確定",
            cancelButtonText: "取消"
        }).then((result) => {

      });
    });
  };
</script>
