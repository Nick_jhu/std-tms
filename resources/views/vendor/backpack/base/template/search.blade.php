 @section('content')
 <style>
    #searchWindow .row{
        margin-bottom: 10px;
    }
 </style>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary" id="statusDiv">
			<div class="box-header with-border">
				<h3 class="box-title">Status</h3>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body no-padding">
				{{--
				<ul class="nav nav-pills nav-stacked" id="statusList">
				</ul> --}}
                <p></p>
				<div style="width:100%;" id="statusList">
                    
                </div>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		{{--
		<div class="box box-primary" id="statusDiv">
			<div class="box-header with-border">
				<h3 class="box-title">Status</h3>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked" id="statusList">
				</ul>
			</div>
			<!-- /.box-body -->
		</div> --}} {{--
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Grid Option</h3>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
						<button type="button" class="btn btn-box-tool" id="saveGrid">
							<i class="fa fa-floppy-o" aria-hidden="true"></i>
						</button>
				</div>
			</div>
			<div class="box-body no-padding">
				<div id="jqxlistbox"></div>
			</div>
			<!-- /.box-body -->
		</div> --}}
		<!-- /. box -->
	</div>
	<div class="col-md-12">
		<div class="box box-primary">
			<!-- /.box-header -->
			<div class="box-body">
				<div class="button-group">
					<div class="row" id="btnArea">

					</div>
					<div id="jqxGrid"></div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div id="searchWindow" style="overflow-x:hidden; display:none">
    <div>搜尋</div>
    <div id="searchBody">
        <div id="searchContent">

        </div>
        <div class="row" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;" id="searchFooter">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="請輸入名稱" name="searchName">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch">儲存</button>
                <button class="btn btn-sm btn-warning" name="setDefault">預設</button>
                <button class="btn btn-sm btn-danger" name="delSearch">刪除</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn">搜尋</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd">新增</button>
            </div>
        </div>
    </div>
    
</div>
<!-- /.modal -->
@endsection @section('after_scripts')
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js?v={{Config::get('app.version')}}"></script>
<script>
$(function(){
    var theme = 'bootstrap';
    $.jqx.theme = theme;
    $('#q-edit').on('click', function(){
        
    });

    if(gridOpt.enabledStatus===false){
        $("#statusDiv").remove();
    }
    
    gridOpt.gridId = "jqxGrid";
    gridOpt.getState = function(){
        loadState();
    };
    //gridOpt.getState = function(){};

    var fieldData = null;
    var fieldObj = null;

    @if(count($crud->colModel) > 0)
		fieldData = '{{!! json_encode($crud->colModel) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
        initGrid(gridOpt.gridId,fieldObj, gridOpt.dataUrl+"/"+gridOpt.enabledStatus,gridOpt);
    @else
    $.get( gridOpt.fieldsUrl, function( fieldData ) {
        console.log(fieldData);
        //alert(thisUrl);
        fieldObj = fieldData;
        initGrid(gridOpt.gridId,fieldData, gridOpt.dataUrl+"/"+gridOpt.enabledStatus,gridOpt);
        /*$("#"+gridOpt.gridId).on("bindingcomplete", 
        function (event) {
            alert("bindingcomplete");
            loadState();
        }); */           
        

        // Create a jqxMenu
        //$("#statusList").jqxMenu();
        //$("#statusList").css('visibility', 'visible');
       
    });

    @endif

    var rowdoubleclick = true;

    if(typeof gridOpt.rowdoubleclick !== "undefined") {
        rowdoubleclick = gridOpt.rowdoubleclick;
    }

    if(rowdoubleclick === true) {
        $('#'+gridOpt.gridId).on('rowdoubleclick', function (event) 
        { 
            var args = event.args;
            // row's bound index.
            var boundIndex = args.rowindex;
            // row's visible index.
            var visibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick; 
            // original event.
            var ev = args.originalEvent;

            var datarow = $("#"+gridOpt.gridId).jqxGrid('getrowdata', boundIndex);
            console.log(datarow);

            var editUrl = gridOpt.editUrl.replace("{id}", datarow.id);
            //location.href = editUrl;
            window.open(editUrl);
        });
    }
    

    $("#saveGrid").on("click", function(){
        var items = $("#jqxlistbox").jqxListBox('getItems');
        //$('#jqxGrid').jqxGrid('clear');
        $("#"+gridOpt.gridId).jqxGrid('beginupdate');

        $.each(items, function(i, item) {
            var thisIndex = $('#'+gridOpt.gridId).jqxGrid('getcolumnindex', item.value)-1;
            if(thisIndex != item.index){
                //console.log(item.value+":"+thisIndex+"="+item.index);
                $('#'+gridOpt.gridId).jqxGrid('setcolumnindex', item.value,  item.index);
            }
            if (item.checked) {
                $("#"+gridOpt.gridId).jqxGrid('showcolumn', item.value);
            }
            else {
                $("#"+gridOpt.gridId).jqxGrid('hidecolumn', item.value);
            }
        })
        
        $("#"+gridOpt.gridId).jqxGrid('endupdate');
        state = $("#"+gridOpt.gridId).jqxGrid('getstate');
        state['filters']['filterscount'] = 0;
        var saveUrl = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/saveLayoutJson') }}"; 	  	  	
        var stateToSave = JSON.stringify(state);

        $.ajax({
            type: "POST",										
            url: saveUrl,		
            data: { data: stateToSave,key: gridOpt.dataUrl },		 
            success: function(response) {
                if(response == "true"){
                    alert("save successful");
                    $('#gridOptModal').modal('hide');
                }else{
                    alert("save failded");
                }
                
            }
        });	
    });

    $("#clearGrid").on("click", function(){
        $.ajax({
            type: "POST",										
            url: "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/clearLayout') }}",		
            data: { key: gridOpt.dataUrl },		 
            success: function(response) {
                if(response == "true"){
                    alert("clear successful");
                    location.reload();
                    $('#gridOptModal').modal('hide');
                }else{
                    //alert("save failded");
                }
                
            }
        });	
    });

    function loadState() {  	
        var loadURL = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/getLayoutJson') }}"; 	  	  	
        $.ajax({
            type: "GET", //  OR POST WHATEVER...
            url: loadURL,
            data: { key: gridOpt.dataUrl },		 
            success: function(response) {										
                if (response != "") {	
                    response = JSON.parse(response);
                    $("#"+gridOpt.gridId).jqxGrid('loadstate', response);
                }
                
                var listSource = [];
        
                state = $("#"+gridOpt.gridId).jqxGrid('getstate');

                $.get(BASE_URL+"/searchList/get/" + gridOpt.pageId, {}, function(data){
                    if(data.msg == "success") {
                        var opt = "<option value=''>請選擇</option>";
            
                        for(i in data.data) {
                            if(data.data[i]["layout_default"] == "Y") {
                                opt += "<option value='"+data.data[i]["id"]+"' selected>"+data.data[i]["title"]+"</option>";
                                $("input[name='searchName']").val(data.data[i]["title"]);
                            }
                            else {
                                opt += "<option value='"+data.data[i]["id"]+"'>"+data.data[i]["title"]+"</option>";
                            }
                        }
            
                        $("select[name='selSearchName']").html(opt);
            
                        $.get(BASE_URL+"/searchHtml/get/" + $("select[name='selSearchName']").val(), {}, function(data){
                            if(data.msg == "success") {
                                if(data.data != null) {
                                    $("#searchContent").html(data.data["data"]);
                                }
                                else {
                                    var seasrchTpl = getSearchTpl(state);
                                    initSearchWindow(seasrchTpl);
                                }

                                var offset = $(".content-wrapper").offset();
                                $('#searchWindow').jqxWindow({
                                    position: { x: offset.left + 50, y: offset.top + 50} ,
                                    showCollapseButton: true, maxHeight: 400, maxWidth: 700, minHeight: 200, minWidth: 200, height: 300, width: 700, autoOpen: false,
                                    initContent: function () {
                                        $('#searchWindow').jqxWindow('focus');
                                    }
                                });


                                $("button[name='winSearchAdd']").on("click", function(){
                                    var seasrchTpl = getSearchTpl(state);
                                    $("#searchContent").append(searchTpl);           
                                });

                                $(document).on("click", "button[name='winBtnRemove']", function(){ 
                                    $( this ).parents(".row").remove();
                                });

                                $(document).on("change", "select[name='winField[]']", function(){ 
                                    $('option:selected', this).siblings().removeAttr('selected');
                                    $('option:selected', this).attr('selected', 'selected');
                                    var val  = $(this).val();
                                    var info = searchObj(val, fieldObj[0]);
                                    var str  = [
                                        {val: 'CONTAINS', label: '包含'},
                                        {val: 'EQUAL', label: '等於'},
                                        {val: 'NOT_EQUAL', label: '不等於'},
                                        {val: 'IN', label: 'IN'},
                                        {val: 'NULL', label: 'NULL'},
                                        {val: 'NOT_NULL', label: 'NOT NULL'},
                                        {val: 'LESS_THAN', label: '小於'},
                                        {val: 'LESS_THAN_OR_EQUAL', label: '小於等於'},
                                        {val: 'GREATER_THAN', label: '大於'},
                                        {val: 'GREATER_THAN_OR_EQUAL', label: '大於等於'},
                                    ];

                                    var num  = [
                                        {val: 'EQUAL', label: '等於'},
                                        {val: 'NOT_EQUAL', label: '不等於'},
                                        {val: 'IN', label: 'IN'},
                                        {val: 'LESS_THAN', label: '小於'},
                                        {val: 'LESS_THAN_OR_EQUAL', label: '小於等於'},
                                        {val: 'GREATER_THAN', label: '大於'},
                                        {val: 'GREATER_THAN_OR_EQUAL', label: '大於等於'},
                                        {val: 'NULL', label: 'NULL'},
                                        {val: 'NOT_NULL', label: 'NOT NULL'},
                                    ];

                                    var opt = "";
                                    if(info.type == "string") {
                                        for(i in str) {
                                            opt += '<option value="'+str[i].val+'">'+str[i].label+'</option>';
                                        }
                                    }
                                    else {
                                        for(i in num) {
                                            opt += '<option value="'+num[i].val+'">'+num[i].label+'</option>';
                                        }
                                    }
                                    $($(this).parent().siblings()[0]).find("select").html(opt);
                                });

                                $(document).on("change", "select[name='winOp[]']", function(){
                                    $('option:selected', this).siblings().removeAttr('selected');
                                    $('option:selected', this).attr('selected', 'selected');
                                });

                                $(document).on("change", "input[name='winContent[]']", function(){
                                    $(this).attr('value', $(this).val());
                                });

                                $("button[name='winSearchBtn']").on("click", function(){
                                    var winField   = [];
                                    var winContent = [];
                                    var winOp      = [];
                                    $("select[name='winField[]']").each(function(){
                                        winField.push($(this).val());
                                    });
                                    $("input[name='winContent[]']").each(function(){
                                        winContent.push($(this).val());
                                    });
                                    $("select[name='winOp[]']").each(function(){
                                        winOp.push($(this).val());
                                    });
                                    
                                    addfilter(winField, winContent, winOp);
                                });
                            }
                        });
                        
                    }
                });

                $.each(state.columns, function(i, item) {
                    if(item.text != "" && item.text != "undefined"){
                        listSource.push({ 
                        label: item.text, 
                        value: i, 
                        checked: !item.hidden });
                        if(!item.hidden){
                            var headerdata = {
                            'filed_text':item.text,
                                'filed_name':i,
                                'show':item.hidden
                            };    
                            enabledheader.push(headerdata);
                        }
                    }
                });

                $("#jqxlistbox").jqxListBox({ 
                    allowDrop: true, 
                    allowDrag: true,
                    source: listSource,
                    width: "99%",
                    height: 500,
                    checkboxes: true,
                    filterable: true,
                    searchMode: 'contains'
                });
            }
        });			  	  	  	  	
    }	

    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });

    $("select[name='selSearchName']").on("change", function(){
        var text = $('option:selected', this).text();
        if($(this).val() == '') {
            text = "";
        }
        $("input[name='searchName']").val(text);

        $.get(BASE_URL+"/searchHtml/get/" + $(this).val(), {}, function(data){
            //console.log(data.data["data"]);
            $("#searchContent").html(data.data["data"]);
        });
    })


    function initSearchWindow(searchTpl) {
        $("#searchContent").append(searchTpl);
    }

    function getSearchTpl(state) {
        var fields = [];
        $.each(state.columns, function(i, item) {
            if(item.hidden == false && item.text != "") {
                var a = {
                    label: item.text,
                    value: i
                }
                fields.push(a);
            }
        });

        var fieldStr = "";
        for(i in fields) {
            fieldStr += '<option value="'+fields[i].value+'">'+fields[i].label+'</option>';
        }

        searchTpl = '<div class="row">\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winField[]">'+fieldStr+'</select>\
                            </div>\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winOp[]">\
                                <option value="CONTAINS">包含</option>\
                                <option value="EQUAL">等於</option>\
                                <option value="NOT_EQUAL">不等於</option>\
                                <option value="IN">IN</option>\
                                <option value="NULL">NULL</option>\
                                <option value="NOT_NULL">NOT NULL</option>\
                                <option value="LESS_THAN">小於</option>\
                                <option value="LESS_THAN_OR_EQUAL">小於等於</option>\
                                <option value="GREATER_THAN">大於</option>\
                                <option value="GREATER_THAN_OR_EQUAL">大於等於</option>\
                                </select>\
                            </div>\
                            <div class="col-xs-4">\
                                <input type="text" class="form-control input-sm" name="winContent[]">\
                            </div>\
                            <div class="col-xs-2">\
                                <button class="btn btn-sm btn-info btn-danger" name="winBtnRemove">-</button>\
                            </div>\
                        </div>';
        return searchTpl;
    }

    var addfilter = function (datafield, filterval, filtercondition) {
        $("#jqxGrid").jqxGrid('clearfilters');
        var old_filed ="";
        for(i in datafield) {
            var filtergroup = new $.jqx.filter();
            if(old_filed != datafield[i]){
                old_filed = datafield[i];

                var filter_or_operator = 0;
                //var filtervalue = filterval;
                var filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
                filtergroup.addfilter(filter_or_operator, filter);

                // add the filters.
                $("#jqxGrid").jqxGrid('addfilter', datafield[i], filtergroup);
            }else{
                var filter3 = filtergroup.createfilter('datefilter',  filterval[i-1], filtercondition[i-1]);
                filtergroup.addfilter(0, filter3);
                $("#jqxGrid").jqxGrid('addfilter',  datafield[i], filtergroup);

                var filter2 = filtergroup.createfilter('datefilter',  filterval[i], filtercondition[i]);
                filtergroup.addfilter(0, filter2);
                $("#jqxGrid").jqxGrid('addfilter',  datafield[i], filtergroup);


            }
        }
        // apply the filters.
        $("#jqxGrid").jqxGrid('applyfilters');
    }

    

    function searchObj(nameKey, myArray){
        for (var i=0; i < myArray.length; i++) {
            if (myArray[i].name === nameKey) {
                return myArray[i];
            }
        }
    }

    $('#searchWindow').on('open', function (event) { 
        if($("body").width() < 400) {
            var offset = $("body").offset();
            $('#searchWindow').jqxWindow({ height: 500, width: $("body").width(), position: { x: 0, y: offset.top + 150} , });
        }
        else {

        }
        
    });

    $("button[name='saveSearch']").on("click", function(){
        var htmlData = $("#searchContent").html();
        var postData = {
            "key" : gridOpt.pageId,
            "data": htmlData,
            "title": $("input[name='searchName']").val(),
            "id": ($("select[name='selSearchName'] option:selected").text() == $("input[name='searchName']").val())?$("select[name='selSearchName']").val():null
        };
        $.ajax({
            url: BASE_URL + "/saveSearchLayout",
            data: postData,
            dataType: "JSON",
            method: "POST",
            success: function(result){
                if(result.msg == "success") {
                    swal("儲存成功", "", "success");
                    $("select[name='selSearchName'] option").removeAttr("selected");
                    $("select[name='selSearchName']").append("<option value='"+result.id+"' selected>"+$("input[name='searchName']").val()+"</option>")
                }
                else {
                    swal("操作失敗", "", "error");
                }
            },
            error: function() {
                swal("請輸入查詢條件名稱", "", "error");
            }
        });
    });

    $("button[name='setDefault']").on("click", function(){
        $.ajax({
            url: BASE_URL + "/setSearchDefault/" + gridOpt.pageId + "/" + $("select[name='selSearchName']").val(),
            dataType: "JSON",
            method: "PUT",
            success: function(result){
                if(result.msg == "success") {
                    swal("儲存成功", "", "success");
                }
                else {
                    swal("操作失敗", "", "error");
                }
            },
            error: function() {
                swal("網路似乎出了問題，請稍後再試", "", "error");
            }
        });
    });

    $("button[name='delSearch']").on("click", function(){
        $.ajax({
            url: BASE_URL + "/delSearchLayout/" + $("select[name='selSearchName']").val(),
            dataType: "JSON",
            method: "DELETE",
            success: function(result){
                if(result.msg == "success") {
                    swal("刪除成功", "", "success");
                    $("select[name='selSearchName'] option:selected").remove();
                    $("input[name='searchName']").val("");
                }
                else {
                    if(typeof data.errorMsg !== "undefined") {
                        swal("操作失敗", data.errorMsg, "error");
                    }
                    else {
                        swal("操作失敗", "", "error");
                    }
                    
                }
            },
            error: function() {
                swal("網路似乎出了問題，請稍後再試", "", "error");
            }
        });
    });
});

</script>

@endsection