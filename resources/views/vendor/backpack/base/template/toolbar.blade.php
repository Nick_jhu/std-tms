{{--  <div class="col-md-2">
    <div class="box box-default">
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked" id="btnArea">
                <li>
                    <a class="MenuButton" id="iAdd"><i class="fa fa-plus"></i> {{ trans('common.add') }}</a>
                </li>
                <li>
                    <a class="MenuButton" id="iEdit"><i class="fa fa-pencil"></i> {{ trans('common.edit') }}</a>
                </li>
                <li>
                    <a class="MenuButton" id="iDel"><i class="fa fa-trash-o"></i> {{ trans('common.delete') }}</a>
                </li>
                <li>
                    <a class="MenuButton" id="iSave"><i class="fa fa-floppy-o"></i> {{ trans('common.save') }}</a>
                </li>
                <li>
                    <a class="MenuButton" id="iCancel"><i class="fa fa-ban"></i> {{ trans('common.cancel') }}</a>
                </li>
                <li>
                    <a class="MenuButton" id="iCopy"><i class="fa fa-files-o"></i> {{ trans('common.copy') }}</a>
                </li>
            </ul>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /. box -->
</div>  --}}

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-body" id="tool-bar">
                <a class="btn btn-app" id="iBack" style="margin-right: 35px !important;" href="{{ url($crud->route) }}"><i class="fa fa-arrow-left"></i> Back</a>
                {{--  @can($crud->pmsName.'.add')  --}}
                <a class="btn btn-app" id="iAdd"><i class="fa fa-plus"></i> {{ trans('common.add') }}</a>
                {{--  @endcan
                @can($crud->pmsName.'.edit')  --}}
                <a class="btn btn-app" id="iEdit"><i class="fa fa-pencil"></i> {{ trans('common.edit') }}</a>
                {{--  @endcan
                @can($crud->pmsName.'.delete')  --}}
                <a class="btn btn-app" id="iDel"><i class="fa fa-trash-o"></i> {{ trans('common.delete') }}</a>
                {{--  @endcan  --}}
                <a class="btn btn-app" id="iSave"><i class="fa fa-floppy-o"></i> {{ trans('common.save') }}</a>
                <a class="btn btn-app" id="iCancel"><i class="fa fa-ban"></i> {{ trans('common.cancel') }}</a>
                <a class="btn btn-app" id="iCopy"><i class="fa fa-files-o"></i> {{ trans('common.copy') }}</a>
            </div>
        </div>
    </div>
</div>

@section('toolbar_scripts')
<script type="text/javascript" src="{{ asset('js/core') }}/edit-var.js"></script>
<script>

$(function(){
            formOpt.initFieldCustomFunc();
            
            //初始化欄位
            $.get( formOpt.fieldsUrl , function( data ) {
                if(mainId == "") {
                    //initField(data);
                    menuBtnFunc.init();
                    menuBtnFunc.disabled(['iEdit', 'iDel', 'iCopy']);
                    formOpt.addFunc();
                    setField.init(formOpt.formId,formOpt.fieldObj, true);
                    $("#iAdd").click();
                }
                else {
                    //initField(data);
                    setField.init(formOpt.formId,formOpt.fieldObj, true);

                    //若是編輯進來的，要把資料塞入Form中
                    if(data !== null) {
                        setFormData(formOpt.formId,data);
                        formOpt.setFormDataFunc();
                        menuBtnFunc.init();
                    }
                }

                if(typeof formOpt.afterInit === "function") {
                    formOpt.afterInit();
                }
            });
    
            $('#iAdd').on('click', function(){
                setField.disabled(formOpt.formId, ['created_by', 'created_at', 'updated_by', 'updated_at']);
                $('input').not('[switch="off"]').prop('disabled', false);
                $('select').not('[switch="off"]').prop('disabled', false);
                $('textarea').not('[switch="off"]').prop('disabled', false);
                $(':input:not(:checkbox, :radio)').not('[no-clear="Y"]').val('');
                $('input[name="_method"]').val('POST');
                SAVE_URL = formOpt.saveUrl;
                if(typeof OLD_DATA != "undefined") {
                    OLD_DATA = [];
                }
                menuBtnFunc.addFunc();
                
                formOpt.addFunc();
            });
    
            $('#iEdit').on('click', function(){
                setField.disabled(formOpt.formId, ['created_by', 'created_at', 'updated_by', 'updated_at']);
                $('input').not('[grid="true"]').not('[switch="off"]').prop('disabled', false);
                $('select').not('[grid="true"]').not('[switch="off"]').prop('disabled', false);
                $('textarea').not('[switch="off"]').prop('disabled', false);
                
                SAVE_URL = formOpt.saveUrl + '/' + mainId;
                menuBtnFunc.editFunc();
                
                formOpt.editFunc();
            });
    
            $('#iCopy').on('click', function(){
                setField.disabled(formOpt.formId, ['created_by', 'created_at', 'updated_by', 'updated_at']);
                $('input').not('[switch="off"]').prop('disabled', false);
                $('select').not('[switch="off"]').prop('disabled', false);
                $('textarea').not('[switch="off"]').prop('disabled', false);
                $('input[name="_method"]').val('POST');
                menuBtnFunc.addFunc();
                if(typeof OLD_DATA != "undefined") {
                    OLD_DATA = [];
                }else {
                    OLD_DATA = [];
                }
                
                formOpt.copyFunc();
            });
    
            $('#iCancel').on('click', function(){
                location.reload();
                formOpt.cancelFunc();
            });
            
            $('#iSave').on('click', function(){
                //var changeData = getChangeData(formOpt.formId, new FormData($('#'+formOpt.formId)[0]));
                if(formOpt.beforesaveFunc()){
                    var changeData = getChangeData(formOpt.formId, $('#'+formOpt.formId).serializeArray());
                    console.log($.param(changeData));
                    var method = (typeof $("#"+formOpt.formId + " input[name='_method']").val() !== "undefined")? $("#"+formOpt.formId + " input[name='_method']").val():"POST";
                    $.ajax({
                        url: SAVE_URL,
                        //type: 'POST',
                        type: method,
                        //data: changeData,
                        data: changeData,
                        async: false,
                        dataType: 'JSON',
                        beforeSend: function () {
                            loadingFunc.show();
                        },
                        error: function (jqXHR, exception) {
                            try{
                                var msgObj = JSON.parse(jqXHR.responseText);
                                $('#errorMsg').find('ul').html('');
                                $.each(msgObj, function(i, v){
                                    $('#errorMsg').find('ul').append('<li>' + v + '</li>');
                                    $('#errorMsg').show();
                                });
                                
                            }
                            catch(err){
                                new PNotify(PNotifyOpt.saveError);
                            }
                            formOpt.saveErrorFunc();
                            loadingFunc.hide();
                            return false;
                        },
                        success: function (data) {
                            if(data.msg == "success") {
                                $('#errorMsg').hide();
                                new PNotify(PNotifyOpt.saveSuccess);
        
                                if(typeof data.lastId != "undefined") {
                                    location.href = formOpt.editUrl  + "/" + data.lastId + "/edit";
                                }
        
                                setField.disabledAll(formOpt.formId,fieldObj, true);
                                menuBtnFunc.init();
                                formOpt.saveSuccessFunc(data);
                                
                            }else{
                                formOpt.saveErrorFunc(data);
                            }
                            loadingFunc.hide();
                            return false;
                        },
                        cache: false,
                        //contentType: false,
                        //processData: false
                    });
                }
            });
    
            $('#iDel').on('click', function(e){
                e.preventDefault();
                var delete_button = $(this);
                var delete_url = SAVE_URL + '/' + mainId;

                if (formOpt.beforeDelFunc() && confirm("{{ trans('common.msg2') }}?") == true) {
                    $.ajax({
                        url: delete_url,
                        type: 'DELETE',
                        success: function(result) {
                            // Show an alert with the result
                            new PNotify(PNotifyOpt.delSuccess);
                            $('#iAdd').click();
                            menuBtnFunc.addFunc();
                            formOpt.addFunc();
                            location.href = formOpt.editUrl + '/create';
                        },
                        error: function(result) {
                            // Show an alert with the result
                            new PNotify(PNotifyOpt.delError);
                        }
                    });
                } else {
                    //new PNotify(PNotifyOpt.delError);
                }
    
            });
        });


        function initBtn(btnGroup){
            $.each(btnGroup, function(i, item) {
                var btnHtml = '<a class="btn btn-app" id="{btnId}"><i class="fa {btnIcon}"></i>{btnText}</a>';

                btnHtml = btnHtml.replace("{btnId}",item.btnId);
                btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
                btnHtml = btnHtml.replace("{btnText}",item.btnText);
                $("#tool-bar").append(btnHtml);
                $("#"+item.btnId).on("click",function(){
                    item.btnFunc();
                });
            })
        }
</script>
@endsection