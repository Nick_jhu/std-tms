@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	{{ trans('sysRefFee.titleNameDelivery') }}<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">{{ trans('sysRefFee.titleNameDelivery') }}</li>
	</ol>
</section>
@endsection 
@section('content')
<style>
	.font-white { color: white}
</style>
		<div class="row">
			<div class="col-md-12">
				<div class="callout callout-danger" id="errorMsg" style="display:none">
					<h4>{{ trans('backpack::crud.please_fix') }}</h4>
					<ul>

					</ul>
				</div>		
				<form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">

							{{ csrf_field() }}
							<input type="file" name="import_file" id="importFile" class="btn-primary font-white"/>
							<button type="submit" class="btn btn-primary" id="btnImport">{{ trans('excel.btnImport') }}</button>						
							<a href="{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/download') }}" class="btn btn-primary">{{ trans('sysRefFee.downloadExample') }}</a>
				</form>
			</div>
		</div>
		<div class="box box-primary" id="subBox" style="display:none">
			
			<form method="POST" accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="form-group col-md-4">
							<input type="hidden" id="trans_type_cd" name="trans_type_cd">
							<input type="hidden" id="type" name="type">
							<label for="car_type_nm">{{ trans('sysRefFee.transTypeNm') }}</label>
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="trans_type_nm" name="trans_type_nm">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-flat lookup" btnname="trans_type_nm"
									info1="{{Crypt::encrypt('bscode')}}" 
									info2="{{Crypt::encrypt('cd+cd_descp,cd,cd_descp')}}" 
									info3="{{Crypt::encrypt('cd_type=\'DELIVERYTYPE\'')}}"
									info4="cd=trans_type_cd;cd_descp=trans_type_nm" triggerfunc="" selectionmode="singlerow">
									<i class="fa fa-search" aria-hidden="true"></i>
								</button>
							</span>
						</div>
						</div>
						<div class="form-group col-md-4">
							<label for="fee_op">{{ trans('sysRefFee.feeOp') }}</label>
							<input type="text" class="form-control input-sm" name="fee_op" grid="true" >
						</div>
						<div class="form-group col-md-4">
							<label for="fee_weight">{{ trans('sysRefFee.feeWeight') }}</label>
							<input type="text" class="form-control input-sm" name="fee_weight" grid="true" >
						</div>
					</div>
					<div class="row">											
						<div class="form-group col-md-4">
							<label for="fee">{{ trans('sysRefFee.fee') }}</label>
							<input type="text" class="form-control input-sm" name="fee" grid="true" >
						</div>
						<div class="form-group col-md-8">
							<label for="remark">{{ trans('sysRefFee.remark') }}</label>
							<input type="text" class="form-control input-sm" name="remark" grid="true" >
						</div>																					
					</div>																		
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<input type="hidden" class="form-control input-sm" name="id" grid="true">
					<button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
					<button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
				</div>
			</form>
		</div>

		<div id="jqxGrid"></div>


 @endsection 
 @include('backpack::template.lookup') 
 @section('after_scripts')
 <script type="text/javascript" src="{{ asset('js') }}/bootstrap.file-input.js"></script>
	<script>
		var mainId = "";

		$(function () {			
			$('#importFile').bootstrapFileInput();
			$('button[btnName="trans_type_nm"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('trans_type_nm', "{{ trans('sysRefFee.transTypeNm') }}", callBackFunc=function(data){
                
           		});
        	});
			$('#trans_type_nm').on('click', function(){
				var check = $('#trans_type_nm').data('ui-autocomplete') != undefined;
				if(check == false) {
					initAutocomplete("subForm","trans_type_nm",callBackFunc=function(data){
						
					});
				}
			});

			
			mainId = "mainId"
			$.ajax({
				url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee') }}"+ "?basecon=type;EQUAL;D",					
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {
					console.log(fieldData);
					var custFieldData = [];
					custFieldData[0] = [
				{name: 'id', type: 'integer'},
				{name: 'trans_type_nm', type: 'string'},
				{name: 'fee_op', type: 'string'},
				{name: 'fee_weight', type: 'float'},
				{name: 'fee', type: 'float'},			
				{name: 'remark', type: 'string'},
			];
			custFieldData[1] = [
				{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false},
				{text:"{{ trans('sysRefFee.transTypeNm') }}", datafield: 'trans_type_nm', filtertype:"textbox", width:220, editable: false},
				{text:"{{ trans('sysRefFee.feeOp') }}", datafield: 'fee_op', filtertype:"textbox", width:100, editable: false},
				{text:"{{ trans('sysRefFee.feeWeight') }}", datafield: 'fee_weight', filtertype:"number", width:100, editable: false},
				{text:"{{ trans('sysRefFee.fee') }}", datafield: 'fee', filtertype:"number", width:100},				
				{text:"{{ trans('sysRefFee.remark') }}", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
					];
					var opt = {};
					opt.gridId = "jqxGrid";
					opt.fieldData = custFieldData;
					opt.formId = "subForm";
					opt.saveId = "Save";
					opt.cancelId = "Cancel";
					opt.showBoxId = "subBox";
					opt.height = 300;
					opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/get') }}";
					opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery') }}" + "/store";
					opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery') }}" + "/update";
					opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery') }}" + "/delete/";
					opt.commonBtn = true;
					opt.showtoolbar = true;
					opt.defaultKey = {
						
					};
					opt.beforeSave = function (formData) {
						// $addBtn.jqxButton({disabled: false});
						// $delBtn.jqxButton({disabled: false});
						
					}
	
					opt.afterSave = function (data) {

					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			});

			$("#myForm").submit(function () {
				if ($('#importFile').get(0).files.length === 0) {
    				swal("{{ trans('sysRefFee.msg1') }}", "", "warning");
					return false;
				}
				var postData = new FormData($(this)[0]);
				$.ajax({
                url: "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/importExcel') }}",
                type: 'POST',
                data: postData,
                async: false,
                beforeSend: function () {
                    
                },
                error: function () {
					swal("{{ trans('sysRefFee.msg2') }}", "", "error");
					// swal("{{ trans('excel.msg2') }}", {
					// 	icon: "error",
					// });
					return false;
                },
                success: function (data) {
				   //alert(data);
				   if(data.msg == "error") {
						swal("{{ trans('sysRefFee.msg2') }}", "", "error");
						// swal("{{ trans('excel.msg2') }}", {
						// icon: "error",
						// });
					}else{
						swal("{{ trans('sysRefFee.msg3') }}", "", "success");
					}
				$('#jqxGrid').jqxGrid('updatebounddata');
				// var dataAdapter = new $.jqx.dataAdapter(data.data);
                // $("#jqxGrid").jqxGrid({ source: dataAdapter });
				
                },
                cache: false,
                contentType: false,
                processData: false
            	});
				return false;
			});
		})
	</script>
@endsection
