@extends('backpack::layout') 
@section('header')
<section class="content-header">

</section>
@endsection 
@section('content')
<style>
	.font-white { color: white}
</style>
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<!-- <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">{{ trans('sysRefFee.titleName') }}</a></li>                     -->
			<!-- <li><a href="#tab_2" data-toggle="tab" aria-expanded="false">{{ trans('sysRefFee.titleNameNonCar') }}</a></li>     
			<li><a href="#tab_4" data-toggle="tab" aria-expanded="false">{{ trans('sysRefFee.titleNameNonCarNoDiscount') }}</a></li>                                   
			<li><a href="#tab_3" data-toggle="tab" aria-expanded="false">{{ trans('sysRefFee.titleNameDelivery') }}</a></li>                     -->
			<li><a href="#tab_6" data-toggle="tab" aria-expanded="true">計件費用設定</a></li>
			<li><a href="#tab_7" data-toggle="tab" aria-expanded="false">材積費用設定</a></li>
			<li><a href="#tab_8" data-toggle="tab" aria-expanded="false">計重費用設定</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<!-- <div class="row">
					<div class="col-md-12">	
						<form method="POST"  accept-charset="UTF-8" id="myForm1" enctype="multipart/form-data">
								{{ csrf_field() }}
								<input type="file" name="import_file" id="importFile1" class="btn-primary font-white"/>
								<button type="submit" class="btn btn-primary" id="btnImport1">{{ trans('excel.btnImport') }}</button>						
								<a href="{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeCar/download') }}" class="btn btn-primary">{{ trans('sysRefFee.downloadExample') }}</a>
						</form>
					</div>
				</div> -->
				<div class="box box-primary" id="subBox1" style="display:none">
					<form method="POST" accept-charset="UTF-8" id="subForm1" enctype="multipart/form-data">
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-4">
									<input type="hidden" name="car_type_cd">
									<input type="hidden" name="type">
									<label for="car_type_nm">{{ trans('sysRefFee.carTypeNm') }}</label>
								<div class="input-group input-group-sm">
									<input type="text" class="form-control" id="car_type_nm" name="car_type_nm">
									<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-flat lookup" btnname="car_type_nm"
											info1="{{Crypt::encrypt('bscode')}}" 
											info2="{{Crypt::encrypt('cd+cd_descp,cd,cd_descp')}}" 
											info3="{{Crypt::encrypt('cd_type=\'CARTYPE\'')}}"
											info4="cd=car_type_cd;cd_descp=car_type_nm" triggerfunc="" selectionmode="singlerow">
											<i class="fa fa-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
								</div>
								<div class="form-group col-md-4">
									<label for="distance">{{ trans('sysRefFee.distance') }}</label>
									<input type="text" class="form-control input-sm" name="distance" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="regular_fee">{{ trans('sysRefFee.regularFee') }}</label>
									<input type="text" class="form-control input-sm" name="regular_fee" grid="true" >
								</div>											
							</div>
							<div class="row">
								<div class="form-group col-md-4">
									<label for="fee_from">{{ trans('sysRefFee.feeFrom') }}</label>
									<input type="text" class="form-control input-sm" name="fee_from" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee_to">{{ trans('sysRefFee.feeTo') }}</label>
									<input type="text" class="form-control input-sm" name="fee_to" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee">{{ trans('sysRefFee.fee') }}</label>
									<input type="text" class="form-control input-sm" name="fee" grid="true" >
								</div>											
							</div>
							<div class="row">
								<div class="form-group col-md-8">
									<label for="remark">{{ trans('sysRefFee.remark') }}</label>
									<input type="text" class="form-control input-sm" name="remark" grid="true" >
								</div>																					
							</div>											
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="hidden" class="form-control input-sm" name="id" grid="true">
							<button type="button" class="btn btn-sm btn-primary" id="Save1">{{ trans('common.save') }}</button>
							<button type="button" class="btn btn-sm btn-danger" id="Cancel1">{{ trans('common.cancel') }}</button>
						</div>
					</form>
				</div>
				<div id="jqxGrid1"></div>
			</div>

			<div class="tab-pane" id="tab_2">
				<div class="row">
					<div class="col-md-12">
						<div class="callout callout-danger" id="errorMsg" style="display:none">
							<h4>{{ trans('backpack::crud.please_fix') }}</h4>
							<ul>

							</ul>
						</div>
						<form method="POST" accept-charset="UTF-8" id="myForm2" enctype="multipart/form-data">
							{{ csrf_field() }}
							<input type="file" name="import_file" id="importFile2" class="btn-primary font-white" />
							<button type="submit" class="btn btn-primary" id="btnImport2">{{ trans('excel.btnImport') }}</button>
							<a href="{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeNonCar/download') }}" class="btn btn-primary">{{ trans('sysRefFee.downloadExample') }}</a>
						</form>
					</div>
				</div>

				<div class="box box-primary" id="subBox2" style="display:none">
					<form method="POST" accept-charset="UTF-8" id="subForm2" enctype="multipart/form-data">
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-4">
									<input type="hidden" id="car_type_cd2" name="car_type_cd">
									<input type="hidden" id="type2" name="type" value="N">
									<label for="car_type_nm">{{ trans('sysRefFee.carTypeNm') }}</label>
									<div class="input-group input-group-sm">
										<input type="text" class="form-control" id="car_type_nm2" name="car_type_nm">
										<span class="input-group-btn">
											<button type="button" class="btn btn-default btn-flat lookup" btnname="car_type_nm2"
												info1="{{Crypt::encrypt('bscode')}}" 
												info2="{{Crypt::encrypt('cd+cd_descp,cd,cd_descp')}}" 
												info3="{{Crypt::encrypt('cd_type=\'CARTYPE\'')}}"
												info4="cd=car_type_cd2;cd_descp=car_type_nm2" triggerfunc="" selectionmode="singlerow">
												<i class="fa fa-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="fee_unit">{{ trans('sysRefFee.feeUnit') }}</label>
									<input type="text" class="form-control input-sm" name="fee_unit" grid="true">
								</div>
								
							</div>
							<div class="row">
								<div class="form-group col-md-4">
									<label for="fee_from">{{ trans('sysRefFee.feeFrom') }}</label>
									<input type="text" class="form-control input-sm" name="fee_from" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee_to">{{ trans('sysRefFee.feeTo') }}</label>
									<input type="text" class="form-control input-sm" name="fee_to" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee">{{ trans('sysRefFee.fee') }}</label>
									<input type="text" class="form-control input-sm" name="fee" grid="true">
								</div>
							</div>
							<div class="row">
								{{--  <div class="form-group col-md-4">
									<label for="second_fee">{{ trans('sysRefFee.secondFee') }}</label>
									<input type="text" class="form-control input-sm" name="second_fee" grid="true">
								</div>  --}}
								<div class="form-group col-md-8">
									<label for="remark">{{ trans('sysRefFee.remark') }}</label>
									<input type="text" class="form-control input-sm" name="remark" grid="true">
								</div>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="hidden" class="form-control input-sm" name="id" grid="true">
							<button type="button" class="btn btn-sm btn-primary" id="Save2">{{ trans('common.save') }}</button>
							<button type="button" class="btn btn-sm btn-danger" id="Cancel2">{{ trans('common.cancel') }}</button>
						</div>
					</form>
				</div>

				<div id="jqxGrid2"></div>
			</div>

			<div class="tab-pane" id="tab_3">
				<div class="row">
					<div class="col-md-12">
						<form method="POST"  accept-charset="UTF-8" id="myForm3" enctype="multipart/form-data">
							{{ csrf_field() }}
							<input type="file" name="import_file" id="importFile3" class="btn-primary font-white"/>
							<button type="submit" class="btn btn-primary" id="btnImport3">{{ trans('excel.btnImport') }}</button>						
							<a href="{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/download') }}" class="btn btn-primary">{{ trans('sysRefFee.downloadExample') }}</a>
						</form>
					</div>
				</div>
				<div class="box box-primary" id="subBox3" style="display:none">
					<form method="POST" accept-charset="UTF-8" id="subForm3" enctype="multipart/form-data">
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-3">
									<!-- <input type="hidden" name="cust_cd">
									<input type="hidden" name="type"> -->
									<!-- <label for="cust_nm">{{ trans('sysRefFee.custNm') }}</label>
									<div class="input-group input-group-sm">
										<input type="text" class="form-control" id="cust_nm" name="cust_nm">
										<span class="input-group-btn">
											<button type="button" class="btn btn-default btn-flat lookup" btnname="cust_nm"
												info1="{{Crypt::encrypt('sys_customers')}}" 
												info2="{{Crypt::encrypt('cust_no+cname,cust_no,cname')}}" 
												info3="{{Crypt::encrypt('type=\'OTHER\'')}}"
												info4="cust_no=cust_cd;cname=cust_nm" triggerfunc="" selectionmode="singlerow">
												<i class="fa fa-search" aria-hidden="true"></i>
											</button>
										</span>
									</div> -->
								</div>
								<!-- <div class="form-group col-md-4">
									<label for="fee_op">{{ trans('sysRefFee.feeOp') }}</label>
									<input type="text" class="form-control input-sm" name="fee_op" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee_weight">{{ trans('sysRefFee.feeWeight') }}</label>
									<input type="text" class="form-control input-sm" name="fee_weight" grid="true" >
								</div> -->
								<div class="form-group col-md-3">
									<label for="fee_from">{{ trans('sysRefFee.feeFrom') }}</label>
									<input type="text" class="form-control input-sm" name="fee_from" grid="true" >
								</div>
								<div class="form-group col-md-3">
									<label for="fee_to">{{ trans('sysRefFee.feeTo') }}</label>
									<input type="text" class="form-control input-sm" name="fee_to" grid="true" >
								</div>
								<div class="form-group col-md-3">
									<label for="fee">{{ trans('sysRefFee.fee') }}</label>
									<input type="text" class="form-control input-sm" name="fee" grid="true">
								</div>
							</div>
							<div class="row">											
								<!-- <div class="form-group col-md-4">
									<label for="fee">{{ trans('sysRefFee.fee') }}</label>
									<input type="text" class="form-control input-sm" name="fee" grid="true" >
								</div> -->
								<div class="form-group col-md-3">
									<label for="regular_fee">{{ trans('sysRefFee.regularFee') }}</label>
									<input type="text" class="form-control input-sm" name="regular_fee" grid="true" >
								</div>
								<div class="form-group col-md-9">
									<label for="remark">{{ trans('sysRefFee.remark') }}</label>
									<input type="text" class="form-control input-sm" name="remark" grid="true" >
								</div>																					
							</div>																		
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="hidden" class="form-control input-sm" name="id" grid="true">
							<button type="button" class="btn btn-sm btn-primary" id="Save3">{{ trans('common.save') }}</button>
							<button type="button" class="btn btn-sm btn-danger" id="Cancel3">{{ trans('common.cancel') }}</button>
						</div>
					</form>
				</div>

				<div id="jqxGrid3"></div>
			</div>

			<div class="tab-pane" id="tab_6">
				<div class="box box-primary" id="subBox6" style="display:none">
					<form method="POST" accept-charset="UTF-8" id="subForm6" enctype="multipart/form-data">
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-3">
									<input type="hidden" id="cust_cd" name="cust_cd">
									<input type="hidden" id="type" name="type">
									<label for="cust_nm">{{ trans('sysRefFee.custNm') }}</label>
									<div class="input-group input-group-sm">
										<input type="text" class="form-control" id="cust_nm" name="cust_nm">
										<span class="input-group-btn">
											<button type="button" class="btn btn-default btn-flat lookup" btnname="cust_nm"
												info1="{{Crypt::encrypt('sys_customers')}}" 
												info2="{{Crypt::encrypt('cust_no+cname,cust_no,cname')}}" 
												info3="{{Crypt::encrypt('type=\'OTHER\'')}}"
												info4="cust_no=cust_cd;cname=cust_nm" triggerfunc="" selectionmode="singlerow">
												<i class="fa fa-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group col-md-3">
									<label for="start_date">有效期間起</label>
									<input type="text" class="form-control input-sm" id="start_date2" name="start_date">
								</div> 
								<div class="form-group col-md-3">
									<label for="end_date">有效期間訖</label>
									<input type="text" class="form-control input-sm" id="end_date2" name="end_date">
								</div> 
								<div class="form-group col-md-3">
									<label for="regular_fee">{{ trans('sysRefFee.regularFee') }}</label>
									<input type="text" class="form-control input-sm" name="regular_fee" grid="true" >
								</div>
							</div>
							<div class="row">
							<div class="form-group col-md-3">
									<label for="fee_from">{{ trans('sysRefFee.feeFrom') }}</label>
									<input type="text" class="form-control input-sm" name="fee_from" grid="true" >
								</div>
								<div class="form-group col-md-3">
									<label for="fee_to">{{ trans('sysRefFee.feeTo') }}</label>
									<input type="text" class="form-control input-sm" name="fee_to" grid="true" >
								</div>
								<div class="form-group col-md-3">
									<label for="fee">{{ trans('sysRefFee.fee') }}</label>
									<input type="text" class="form-control input-sm" name="fee" grid="true">
								</div>
								<div class="form-group col-md-3">
								<label for="trs_mode">{{ trans('modOrder.temperate') }}</label>
									<select class="form-control" id="trs_mode" name="temperate" no-clear="Y">
										@foreach($viewData['TEMPERATEData'] as $row)
											<option value="{{$row->code}}">{{$row->descp}}</option>
										@endforeach
									</select>                                      
								</div>
							</div>
							<div class="row">											
								<div class="form-group col-md-6">
									<label for="remark">{{ trans('sysRefFee.remark') }}</label>
									<input type="text" class="form-control input-sm" name="remark" grid="true" >
								</div>																					
							</div>																		
						</div>

						<div class="box-footer">
							<input type="hidden" class="form-control input-sm" name="id" grid="true">
							<button type="button" class="btn btn-sm btn-primary" id="Save6">{{ trans('common.save') }}</button>
							<button type="button" class="btn btn-sm btn-danger" id="Cancel6">{{ trans('common.cancel') }}</button>
						</div>
					</form>
				</div>

				<div id="jqxGrid6"></div>
			</div>

			<div class="tab-pane" id="tab_7">
				<div class="box box-primary" id="subBox7" style="display:none">
					<form method="POST" accept-charset="UTF-8" id="subForm7" enctype="multipart/form-data">
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-3">
									<input type="hidden" id="cust_cd2" name="cust_cd">
									<input type="hidden" id="type2" name="type">
									<label for="cust_nm2">{{ trans('sysRefFee.custNm') }}</label>
									<div class="input-group input-group-sm">
										<input type="text" class="form-control" id="cust_nm2" name="cust_nm">
										<span class="input-group-btn">
											<button type="button" class="btn btn-default btn-flat lookup" btnname="cust_nm2"
												info1="{{Crypt::encrypt('sys_customers')}}" 
												info2="{{Crypt::encrypt('cust_no+cname,cust_no,cname')}}" 
												info3="{{Crypt::encrypt('type=\'OTHER\'')}}"
												info4="cust_no=cust_cd2;cname=cust_nm2" triggerfunc="" selectionmode="singlerow">
												<i class="fa fa-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group col-md-3">
									<label for="start_date">有效期間起</label>
									<input type="text" class="form-control input-sm" id="start_date" name="start_date">
								</div> 
								<div class="form-group col-md-3">
									<label for="end_date">有效期間訖</label>
									<input type="text" class="form-control input-sm" id="end_date" name="end_date">
								</div> 
								<div class="form-group col-md-3">
									<label for="regular_fee">{{ trans('sysRefFee.regularFee') }}</label>
									<input type="text" class="form-control input-sm" name="regular_fee" grid="true" >
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-3">
									<label for="fee_from">{{ trans('sysRefFee.cbmfeeFrom') }}</label>
									<input type="text" class="form-control input-sm" name="fee_from" grid="true" >
								</div>
								<div class="form-group col-md-3">
									<label for="fee_to">{{ trans('sysRefFee.cbmfeeTo') }}</label>
									<input type="text" class="form-control input-sm" name="fee_to" grid="true" >
								</div>
								<div class="form-group col-md-3">
									<label for="fee">{{ trans('sysRefFee.fee') }}</label>
									<input type="text" class="form-control input-sm" name="fee" grid="true">
								</div>
								<div class="form-group col-md-3">
								<label for="trs_mode">{{ trans('modOrder.temperate') }}</label>
									<select class="form-control" id="trs_mode" name="temperate" no-clear="Y">
										@foreach($viewData['TEMPERATEData'] as $row)
											<option value="{{$row->code}}">{{$row->descp}}</option>
										@endforeach
									</select>                                      
								</div>
							</div>
							<div class="row">											
								<div class="form-group col-md-3">
									<label for="overcbm">{{ trans('sysRefFee.overcbm') }}</label>
									<input type="text" class="form-control input-sm" name="overcbm" grid="true" >
								</div>	
								<div class="form-group col-md-3">
									<label for="overamt">{{ trans('sysRefFee.overamt') }}</label>
									<input type="text" class="form-control input-sm" name="overamt" grid="true" >
								</div>
								<div class="form-group col-md-6">
									<label for="remark">{{ trans('sysRefFee.remark') }}</label>
									<input type="text" class="form-control input-sm" name="remark" grid="true" >
								</div>																					
							</div>																		
						</div>

						<div class="box-footer">
							<input type="hidden" class="form-control input-sm" name="id" grid="true">
							<button type="button" class="btn btn-sm btn-primary" id="Save7">{{ trans('common.save') }}</button>
							<button type="button" class="btn btn-sm btn-danger" id="Cancel7">{{ trans('common.cancel') }}</button>
						</div>
					</form>
				</div>

				<div id="jqxGrid7"></div>
			</div>

			<div class="tab-pane" id="tab_8">
				<div class="box box-primary" id="subBox8" style="display:none">
					<form method="POST" accept-charset="UTF-8" id="subForm8" enctype="multipart/form-data">
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-3">
									<input type="hidden" id="cust_cd3" name="cust_cd">
									<input type="hidden" id="type3" name="type">
									<label for="cust_nm3">{{ trans('sysRefFee.custNm') }}</label>
									<div class="input-group input-group-sm">
										<input type="text" class="form-control" id="cust_nm3" name="cust_nm">
										<span class="input-group-btn">
											<button type="button" class="btn btn-default btn-flat lookup" btnname="cust_nm3"
												info1="{{Crypt::encrypt('sys_customers')}}" 
												info2="{{Crypt::encrypt('cust_no+cname,cust_no,cname')}}" 
												info3="{{Crypt::encrypt('type=\'OTHER\'')}}"
												info4="cust_no=cust_cd3;cname=cust_nm3" triggerfunc="" selectionmode="singlerow">
												<i class="fa fa-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group col-md-3">
									<label for="start_date">有效期間起</label>
									<input type="text" class="form-control input-sm" id="start_dateGW" name="start_date">
								</div> 
								<div class="form-group col-md-3">
									<label for="end_date">有效期間訖</label>
									<input type="text" class="form-control input-sm" id="end_dateGW" name="end_date">
								</div> 
								<div class="form-group col-md-3">
									<label for="regular_fee">{{ trans('sysRefFee.regularFee') }}</label>
									<input type="text" class="form-control input-sm" name="regular_fee" grid="true" >
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-3">
									<label for="fee_from">FROM</label>
									<input type="text" class="form-control input-sm" name="fee_from" grid="true" >
								</div>
								<div class="form-group col-md-3">
									<label for="fee_to">TO</label>
									<input type="text" class="form-control input-sm" name="fee_to" grid="true" >
								</div>
								<div class="form-group col-md-3">
									<label for="fee">費用</label>
									<input type="text" class="form-control input-sm" name="fee" grid="true">
								</div>
								<div class="form-group col-md-3">
								<label for="trs_mode">溫層</label>
									<select class="form-control" id="trs_mode" name="temperate" no-clear="Y">
										@foreach($viewData['TEMPERATEData'] as $row)
											<option value="{{$row->code}}">{{$row->descp}}</option>
										@endforeach
									</select>                                      
								</div>
							</div>
							<div class="row">											
								<div class="form-group col-md-3">
									<label for="overcbm">每超過重量大小</label>
									<input type="text" class="form-control input-sm" name="overcbm" grid="true" >
								</div>	
								<div class="form-group col-md-3">
									<label for="overamt">每超過重量大小金額</label>
									<input type="text" class="form-control input-sm" name="overamt" grid="true" >
								</div>
								<div class="form-group col-md-6">
									<label for="remark">{{ trans('sysRefFee.remark') }}</label>
									<input type="text" class="form-control input-sm" name="remark" grid="true" >
								</div>																					
							</div>																		
						</div>

						<div class="box-footer">
							<input type="hidden" class="form-control input-sm" name="id" grid="true">
							<button type="button" class="btn btn-sm btn-primary" id="Save8">{{ trans('common.save') }}</button>
							<button type="button" class="btn btn-sm btn-danger" id="Cancel8">{{ trans('common.cancel') }}</button>
						</div>
					</form>
				</div>

				<div id="jqxGrid8"></div>
			</div>
			<div class="tab-pane" id="tab_4">

				<div class="box box-primary" id="subBox4" style="display:none">
					<form method="POST" accept-charset="UTF-8" id="subForm4" enctype="multipart/form-data">
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-4">
									<input type="hidden" id="car_type_cd4" name="car_type_cd">
									<input type="hidden" id="type2" name="type" value="N">
									<label for="car_type_nm">{{ trans('sysRefFee.temperate') }}</label>
									<div class="input-group input-group-sm">
										<input type="text" class="form-control" id="car_type_nm4" name="car_type_nm">
										<span class="input-group-btn">
											<button type="button" class="btn btn-default btn-flat lookup" btnname="car_type_nm4"
												info1="{{Crypt::encrypt('bscode')}}" 
												info2="{{Crypt::encrypt('cd+cd_descp,cd,cd_descp')}}" 
												info3="{{Crypt::encrypt('cd_type=\'TEMPERATE\'')}}"
												info4="cd=car_type_cd4;cd_descp=car_type_nm4" triggerfunc="" selectionmode="singlerow">
												<i class="fa fa-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="fee_unit">{{ trans('sysRefFee.feeUnit') }}</label>
									<input type="text" class="form-control input-sm" name="fee_unit" grid="true">
								</div>
								
							</div>
							<div class="row">
								<div class="form-group col-md-4">
									<label for="fee_from">{{ trans('sysRefFee.feeFrom') }}</label>
									<input type="text" class="form-control input-sm" name="fee_from" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee_to">{{ trans('sysRefFee.feeTo') }}</label>
									<input type="text" class="form-control input-sm" name="fee_to" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee">{{ trans('sysRefFee.fee') }}</label>
									<input type="text" class="form-control input-sm" name="fee" grid="true">
								</div>
							</div>
							<div class="row">
								{{--  <div class="form-group col-md-4">
									<label for="second_fee">{{ trans('sysRefFee.secondFee') }}</label>
									<input type="text" class="form-control input-sm" name="second_fee" grid="true">
								</div>  --}}
								<div class="form-group col-md-8">
									<label for="remark">{{ trans('sysRefFee.remark') }}</label>
									<input type="text" class="form-control input-sm" name="remark" grid="true">
								</div>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="hidden" class="form-control input-sm" name="id" grid="true">
							<button type="button" class="btn btn-sm btn-primary" id="Save4">{{ trans('common.save') }}</button>
							<button type="button" class="btn btn-sm btn-danger" id="Cancel4">{{ trans('common.cancel') }}</button>
						</div>
					</form>
				</div>

				<div id="jqxGrid4"></div>
			</div>
		</div>
	</div>
	


@endsection 
@include('backpack::template.lookup') 
@section('after_scripts')
<script type="text/javascript" src="{{ asset('js') }}/bootstrap.file-input.js"></script>
<script>
	var mainId = "";

	$(function () {			
		// car();
		// nonCar();
		// express();
		// nonCarNoDiscount();
		Piece();
		Cbm();
		Gw();
		$('#start_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
		$('#end_date').datepicker({
			autoclose: true,
			format: 'yyyy-mm-dd'
		});

		$('#start_dateGW').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
		$('#end_dateGW').datepicker({
			autoclose: true,
			format: 'yyyy-mm-dd'
		});
		

		$('#start_date2').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
		$('#end_date2').datepicker({
			autoclose: true,
			format: 'yyyy-mm-dd'
		});
		$("[href='#tab_6']").click();
	});

	function car() {
		$('#importFile1').bootstrapFileInput();
		$('#subBox1 button[btnName="car_type_nm"]').on('click', function(){
			$('#lookupModal').modal('show');
			initLookup('car_type_nm', "{{ trans('sysRefFee.carTypeNm') }}", callBackFunc=function(data){
				$("#subBox1 input[name='car_type_nm'").val(data.cd_descp);
				$("#subBox1 input[name='car_type_cd'").val(data.cd);
			});
		});
		$('#subBox1 input[name="car_type_nm"]').on('click', function(){
			var check = $('#subBox1 input[name="car_type_nm"]').data('ui-autocomplete') != undefined;
			if(check == false) {
				initAutocomplete("subForm1","car_type_nm",callBackFunc=function(data){
					$("#subBox1 input[name='car_type_cd'").val(data.cd);
				});
			}
		});
		mainId = "mainId"
		$.ajax({
			url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee') }}"+ "?basecon=type;EQUAL;C",					
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				var custFieldData = [];
				custFieldData[0] = [
					{name: 'id', type: 'integer'},
					{name: 'car_type_nm', type: 'string'},
					{name: 'car_type_cd', type: 'string'},
					{name: 'distance', type: 'float'},
					{name: 'regular_fee', type: 'float'},
					{name: 'fee_from', type: 'float'},
					{name: 'fee_to', type: 'float'},
					{name: 'fee', type: 'float'},
					{name: 'remark', type: 'string'},
				];
				custFieldData[1] = [
					{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false, hidden: true},
					{text:"{{ trans('sysRefFee.carTypeCd') }}", datafield: 'car_type_cd', filtertype:"textbox", hidden:true, width:100, editable: false},
					{text:"{{ trans('sysRefFee.carTypeNm') }}", datafield: 'car_type_nm', filtertype:"textbox", width:100, editable: false},
					{text:"{{ trans('sysRefFee.distance') }}", datafield: 'distance', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.regularFee') }}", datafield: 'regular_fee', filtertype:"number", width:100},
					{text:"{{ trans('sysRefFee.feeFrom') }}", datafield: 'fee_from', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.feeTo') }}", datafield: 'fee_to', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.fee') }}", datafield: 'fee', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.remark') }}", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
				];
				var opt = {};
				opt.gridId = "jqxGrid1";
				opt.fieldData = custFieldData;
				opt.formId = "subForm1";
				opt.saveId = "Save1";
				opt.cancelId = "Cancel1";
				opt.showBoxId = "subBox1";
				opt.height = $( window ).height() - 250;
				opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeCar/get') }}";
				opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeCar') }}" + "/store";
				opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeCar') }}" + "/update";
				opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeCar') }}" + "/delete/";
				opt.commonBtn = true;
				opt.showtoolbar = true;
				opt.defaultKey = {
					
				};
				opt.beforeSave = function (formData) {
					// $addBtn.jqxButton({disabled: false});
					// $delBtn.jqxButton({disabled: false});
					
				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});

		$("#myForm1").submit(function () {
			if ($('#importFile1').get(0).files.length === 0) {
				swal("{{ trans('sysRefFee.msg1') }}", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			$.ajax({
				url: "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeCar/importExcel') }}",
				type: 'POST',
				data: postData,
				async: false,
				beforeSend: function () {
					
				},
				error: function () {
					swal("{{ trans('sysRefFee.msg2') }}", "", "error");
					// swal("{{ trans('excel.msg2') }}", {
					// 	icon: "error",
					// });
					return false;
				},
				success: function (data) {
					//alert(data);
					if(data.msg == "error") {
						swal("{{ trans('sysRefFee.msg2') }}", "", "error");
						// swal("{{ trans('excel.msg2') }}", {
						// icon: "error",
						// });
					}else{
						swal("{{ trans('sysRefFee.msg3') }}", "", "success");
					}
					$('#jqxGrid1').jqxGrid('updatebounddata');
					// var dataAdapter = new $.jqx.dataAdapter(data.data);
					// $("#jqxGrid").jqxGrid({ source: dataAdapter });
				
				},
				cache: false,
				contentType: false,
				processData: false
			});
			return false;
		});
	}

	function Piece() {
		$('#importFile3').bootstrapFileInput();
		$('button[btnName="cust_nm"]').on('click', function(){
			$('#lookupModal').modal('show');
			initLookup('cust_nm', "{{ trans('sysRefFee.custNm') }}", callBackFunc=function(data){
				
			});
		});

		$('#cust_nm').on('click', function(){
			var check = $('#cust_nm').data('ui-autocomplete') != undefined;
			if(check == false) {
				initAutocomplete("subForm3","cust_nm",callBackFunc=function(data){
					
				});
			}
		});

		
		mainId = "mainId"
		$.ajax({
			url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee') }}",					
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				var custFieldData = [];
				custFieldData[0] = [
					{name: 'id', type: 'integer'},
					{name: 'cust_cd', type: 'string'},
					{name: 'cust_nm', type: 'string'},
					{name: 'fee_op', type: 'string'},
					{name: 'fee_weight', type: 'float'},
					{name: 'fee_from', type: 'float'},
					{name: 'fee_to', type: 'float'},
					{name: 'fee', type: 'float'},	
					{name: 'regular_fee', type: 'float'},
					{name: 'remark', type: 'string'},
					{name: 'temperate', type: 'string'},
					{name: 'start_date', type: 'string', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
					{name: 'end_date', type: 'string', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
				];
				custFieldData[1] = [
					{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false, hidden:true},
					{text:"cust_cd", datafield: 'cust_cd', filtertype:"textbox", width:100, hidden:true},
					{text:"{{ trans('sysRefFee.custNm') }}", datafield: 'cust_nm', filtertype:"textbox", width:220, editable: false},
					//{text:"{{ trans('sysRefFee.feeOp') }}", datafield: 'fee_op', filtertype:"textbox", width:100, editable: false},
					//{text:"{{ trans('sysRefFee.feeWeight') }}", datafield: 'fee_weight', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.feeFrom') }}", datafield: 'fee_from', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.feeTo') }}", datafield: 'fee_to', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.fee') }}", datafield: 'fee', filtertype:"number", width:100},				
					{text:"{{ trans('sysRefFee.regularFee') }}", datafield: 'regular_fee', filtertype:"number", width:100},				
					{text:"{{ trans('sysRefFee.remark') }}", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
					{text:"{{ trans('sysRefFee.temperate') }}", datafield: 'temperate', filtertype:"textbox", width:200, editable: false},
					{text:"{{ trans('sysRefFee.start_date')}}", datafield: 'start_date', width: 200, cellsformat: 'yyyy-MM-dd',filtertype: 'date'},
					{text:"{{ trans('sysRefFee.end_date')}}", datafield: 'end_date', width: 200, cellsformat: 'yyyy-MM-dd',filtertype: 'date'},
				];
				var opt = {};
				opt.gridId = "jqxGrid6";
				opt.fieldData = custFieldData;
				opt.formId = "subForm6";
				opt.saveId = "Save6";
				opt.cancelId = "Cancel6";
				opt.showBoxId = "subBox6";
				opt.height = $( window ).height() - 250;
				opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeePiece/get') }}";
				opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeePiece') }}" + "/store";
				opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeePiece') }}" + "/update";
				opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeePiece') }}" + "/delete/";
				opt.commonBtn = true;
				opt.showtoolbar = true;
				opt.defaultKey = {
					
				};
				opt.beforeSave = function (formData) {
					// $addBtn.jqxButton({disabled: false});
					// $delBtn.jqxButton({disabled: false});
					// console.log($("#end_date").val());
				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});
		$("#myForm6").submit(function () {
			if ($('#importFile6').get(0).files.length === 0) {
				swal("{{ trans('sysRefFee.msg1') }}", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			$.ajax({
			url: "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/importExcel') }}",
			type: 'POST',
			data: postData,
			async: false,
			beforeSend: function () {
				
			},
			error: function () {
				swal("{{ trans('sysRefFee.msg2') }}", "", "error");
				// swal("{{ trans('excel.msg2') }}", {
				// 	icon: "error",
				// });
				return false;
			},
			success: function (data) {
				//alert(data);
				if(data.msg == "error") {
					swal("{{ trans('sysRefFee.msg2') }}", "", "error");
					// swal("{{ trans('excel.msg2') }}", {
					// icon: "error",
					// });
				}else{
					swal("{{ trans('sysRefFee.msg3') }}", "", "success");
				}
			$('#jqxGrid').jqxGrid('updatebounddata');
			// var dataAdapter = new $.jqx.dataAdapter(data.data);
			// $("#jqxGrid").jqxGrid({ source: dataAdapter });
			
			},
			cache: false,
			contentType: false,
			processData: false
			});
			return false;
		});
	}

	function Cbm() {
		$('button[btnName="cust_nm2"]').on('click', function(){
			$('#lookupModal').modal('show');
			initLookup('cust_nm2', "{{ trans('sysRefFee.custNm') }}", callBackFunc=function(data){
				
			});
		});
		mainId = "mainId"
		$.ajax({
			url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee') }}",					
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				var custFieldData = [];
				custFieldData[0] = [
					{name: 'id', type: 'integer'},
					{name: 'cust_cd', type: 'string'},
					{name: 'cust_nm', type: 'string'},
					{name: 'fee_op', type: 'string'},
					{name: 'fee_weight', type: 'float'},
					{name: 'fee_from', type: 'float'},
					{name: 'fee_to', type: 'float'},
					{name: 'fee', type: 'float'},	
					{name: 'regular_fee', type: 'float'},
					{name: 'remark', type: 'string'},
					{name: 'temperate', type: 'string'},
					{name: 'overcbm', type: 'integer'},
					{name: 'overamt', type: 'integer'},
					{name: 'start_date', type: 'string', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
					{name: 'end_date', type: 'string', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
				];
				custFieldData[1] = [
					{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false, hidden:true},
					{text:"cust_cd", datafield: 'cust_cd', filtertype:"textbox", width:100, hidden:true},
					{text:"{{ trans('sysRefFee.custNm') }}", datafield: 'cust_nm', filtertype:"textbox", width:220, editable: false},
					{text:"{{ trans('sysRefFee.overcbm') }}", datafield: 'overcbm', filtertype:"textbox", width:100, editable: false},
					{text:"{{ trans('sysRefFee.overamt') }}", datafield: 'overamt', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.cbmfeeFrom') }}", datafield: 'fee_from', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.cbmfeeTo') }}", datafield: 'fee_to', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.cbmfee') }}", datafield: 'fee', filtertype:"number", width:100},				
					{text:"{{ trans('sysRefFee.regularFee') }}", datafield: 'regular_fee', filtertype:"number", width:100},				
					{text:"{{ trans('sysRefFee.remark') }}", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
					{text:"{{ trans('sysRefFee.temperate') }}", datafield: 'temperate', filtertype:"textbox", width:200, editable: false},
					{text:"{{ trans('sysRefFee.start_date')}}", datafield: 'start_date', width: 200, cellsformat: 'yyyy-MM-dd',filtertype: 'date'},
					{text:"{{ trans('sysRefFee.end_date')}}", datafield: 'end_date', width: 200, cellsformat: 'yyyy-MM-dd',filtertype: 'date'},
				];
				var opt = {};
				opt.gridId = "jqxGrid7";
				opt.fieldData = custFieldData;
				opt.formId = "subForm7";
				opt.saveId = "Save7";
				opt.cancelId = "Cancel7";
				opt.showBoxId = "subBox7";
				opt.height = $( window ).height() - 250;
				opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeCbm/get') }}";
				opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeCbm') }}" + "/store";
				opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeCbm') }}" + "/update";
				opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeCbm') }}" + "/delete/";
				opt.commonBtn = true;
				opt.showtoolbar = true;
				opt.defaultKey = {
					
				};
				opt.beforeSave = function (formData) {
					// $addBtn.jqxButton({disabled: false});
					// $delBtn.jqxButton({disabled: false});
					// console.log($("#end_date").val());
				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});
		$("#myForm7").submit(function () {
			if ($('#importFile7').get(0).files.length === 0) {
				swal("{{ trans('sysRefFee.msg1') }}", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			$.ajax({
			url: "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/importExcel') }}",
			type: 'POST',
			data: postData,
			async: false,
			beforeSend: function () {
				
			},
			error: function () {
				swal("{{ trans('sysRefFee.msg2') }}", "", "error");
				// swal("{{ trans('excel.msg2') }}", {
				// 	icon: "error",
				// });
				return false;
			},
			success: function (data) {
				//alert(data);
				if(data.msg == "error") {
					swal("{{ trans('sysRefFee.msg2') }}", "", "error");
					// swal("{{ trans('excel.msg2') }}", {
					// icon: "error",
					// });
				}else{
					swal("{{ trans('sysRefFee.msg3') }}", "", "success");
				}
			$('#jqxGrid').jqxGrid('updatebounddata');
			// var dataAdapter = new $.jqx.dataAdapter(data.data);
			// $("#jqxGrid").jqxGrid({ source: dataAdapter });
			
			},
			cache: false,
			contentType: false,
			processData: false
			});
			return false;
		});
	}

	function Gw() {
		$('button[btnName="cust_nm3"]').on('click', function(){
			$('#lookupModal').modal('show');
			initLookup('cust_nm3', "{{ trans('sysRefFee.custNm') }}", callBackFunc=function(data){
				
			});
		});
		mainId = "mainId"
		$.ajax({
			url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee') }}",					
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				var custFieldData = [];
				custFieldData[0] = [
					{name: 'id', type: 'integer'},
					{name: 'cust_cd', type: 'string'},
					{name: 'cust_nm', type: 'string'},
					{name: 'fee_op', type: 'string'},
					{name: 'fee_weight', type: 'float'},
					{name: 'fee_from', type: 'float'},
					{name: 'fee_to', type: 'float'},
					{name: 'fee', type: 'float'},	
					{name: 'regular_fee', type: 'float'},
					{name: 'remark', type: 'string'},
					{name: 'temperate', type: 'string'},
					{name: 'overcbm', type: 'integer'},
					{name: 'overamt', type: 'integer'},
					{name: 'start_date', type: 'string', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
					{name: 'end_date', type: 'string', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
				];
				custFieldData[1] = [
					{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false, hidden:true},
					{text:"cust_cd", datafield: 'cust_cd', filtertype:"textbox", width:100, hidden:true},
					{text:"{{ trans('sysRefFee.custNm') }}", datafield: 'cust_nm', filtertype:"textbox", width:220, editable: false},
					{text:"{{ trans('sysRefFee.overcbm') }}", datafield: 'overcbm', filtertype:"textbox", width:100, editable: false},
					{text:"{{ trans('sysRefFee.overamt') }}", datafield: 'overamt', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.cbmfeeFrom') }}", datafield: 'fee_from', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.cbmfeeTo') }}", datafield: 'fee_to', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.cbmfee') }}", datafield: 'fee', filtertype:"number", width:100},				
					{text:"{{ trans('sysRefFee.regularFee') }}", datafield: 'regular_fee', filtertype:"number", width:100},				
					{text:"{{ trans('sysRefFee.remark') }}", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
					{text:"{{ trans('sysRefFee.temperate') }}", datafield: 'temperate', filtertype:"textbox", width:200, editable: false},
					{text:"{{ trans('sysRefFee.start_date')}}", datafield: 'start_date', width: 200, cellsformat: 'yyyy-MM-dd',filtertype: 'date'},
					{text:"{{ trans('sysRefFee.end_date')}}", datafield: 'end_date', width: 200, cellsformat: 'yyyy-MM-dd',filtertype: 'date'},
				];
				var opt = {};
				opt.gridId = "jqxGrid8";
				opt.fieldData = custFieldData;
				opt.formId = "subForm8";
				opt.saveId = "Save8";
				opt.cancelId = "Cancel8";
				opt.showBoxId = "subBox8";
				opt.height = $( window ).height() - 250;
				opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeGw/get') }}";
				opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeGw') }}" + "/store";
				opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeGw') }}" + "/update";
				opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeGw') }}" + "/delete/";
				opt.commonBtn = true;
				opt.showtoolbar = true;
				opt.defaultKey = {
					
				};
				opt.beforeSave = function (formData) {
					// $addBtn.jqxButton({disabled: false});
					// $delBtn.jqxButton({disabled: false});
					// console.log($("#end_date").val());
				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});
		$("#myForm8").submit(function () {
			if ($('#importFile8').get(0).files.length === 0) {
				swal("{{ trans('sysRefFee.msg1') }}", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			$.ajax({
			url: "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/importExcel') }}",
			type: 'POST',
			data: postData,
			async: false,
			beforeSend: function () {
				
			},
			error: function () {
				swal("{{ trans('sysRefFee.msg2') }}", "", "error");
				// swal("{{ trans('excel.msg2') }}", {
				// 	icon: "error",
				// });
				return false;
			},
			success: function (data) {
				//alert(data);
				if(data.msg == "error") {
					swal("{{ trans('sysRefFee.msg2') }}", "", "error");
					// swal("{{ trans('excel.msg2') }}", {
					// icon: "error",
					// });
				}else{
					swal("{{ trans('sysRefFee.msg3') }}", "", "success");
				}
			$('#jqxGrid').jqxGrid('updatebounddata');
			// var dataAdapter = new $.jqx.dataAdapter(data.data);
			// $("#jqxGrid").jqxGrid({ source: dataAdapter });
			
			},
			cache: false,
			contentType: false,
			processData: false
			});
			return false;
		});
	}
	function express() {
		$('#importFile3').bootstrapFileInput();
		$('button[btnName="cust_nm"]').on('click', function(){
			$('#lookupModal').modal('show');
			initLookup('cust_nm', "{{ trans('sysRefFee.custNm') }}", callBackFunc=function(data){
				
			});
		});

		$('#cust_nm').on('click', function(){
			var check = $('#cust_nm').data('ui-autocomplete') != undefined;
			if(check == false) {
				initAutocomplete("subForm3","cust_nm",callBackFunc=function(data){
					
				});
			}
		});

		
		mainId = "mainId"
		$.ajax({
			url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee') }}"+ "?basecon=type;EQUAL;D",					
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				var custFieldData = [];
				custFieldData[0] = [
					{name: 'id', type: 'integer'},
					{name: 'cust_cd', type: 'string'},
					{name: 'cust_nm', type: 'string'},
					//{name: 'fee_op', type: 'string'},
					//{name: 'fee_weight', type: 'float'},
					{name: 'fee_from', type: 'float'},
					{name: 'fee_to', type: 'float'},
					{name: 'fee', type: 'float'},	
					{name: 'regular_fee', type: 'float'},
					{name: 'remark', type: 'string'},
				];
				custFieldData[1] = [
					{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false, hidden:true},
					{text:"cust_cd", datafield: 'cust_cd', filtertype:"textbox", width:100, hidden:true},
					{text:"{{ trans('sysRefFee.custNm') }}", datafield: 'cust_nm', filtertype:"textbox", width:220, editable: false},
					//{text:"{{ trans('sysRefFee.feeOp') }}", datafield: 'fee_op', filtertype:"textbox", width:100, editable: false},
					//{text:"{{ trans('sysRefFee.feeWeight') }}", datafield: 'fee_weight', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.feeFrom') }}", datafield: 'fee_from', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.feeTo') }}", datafield: 'fee_to', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.fee') }}", datafield: 'fee', filtertype:"number", width:100},				
					{text:"{{ trans('sysRefFee.regularFee') }}", datafield: 'regular_fee', filtertype:"number", width:100},				
					{text:"{{ trans('sysRefFee.remark') }}", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
				];
				var opt = {};
				opt.gridId = "jqxGrid3";
				opt.fieldData = custFieldData;
				opt.formId = "subForm3";
				opt.saveId = "Save3";
				opt.cancelId = "Cancel3";
				opt.showBoxId = "subBox3";
				opt.height = $( window ).height() - 250;
				opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/get') }}";
				opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery') }}" + "/store";
				opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery') }}" + "/update";
				opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery') }}" + "/delete/";
				opt.commonBtn = true;
				opt.showtoolbar = true;
				opt.defaultKey = {
					
				};
				opt.beforeSave = function (formData) {
					// $addBtn.jqxButton({disabled: false});
					// $delBtn.jqxButton({disabled: false});
					
				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});

		$("#myForm3").submit(function () {
			if ($('#importFile3').get(0).files.length === 0) {
				swal("{{ trans('sysRefFee.msg1') }}", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			$.ajax({
			url: "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/importExcel') }}",
			type: 'POST',
			data: postData,
			async: false,
			beforeSend: function () {
				
			},
			error: function () {
				swal("{{ trans('sysRefFee.msg2') }}", "", "error");
				// swal("{{ trans('excel.msg2') }}", {
				// 	icon: "error",
				// });
				return false;
			},
			success: function (data) {
				//alert(data);
				if(data.msg == "error") {
					swal("{{ trans('sysRefFee.msg2') }}", "", "error");
					// swal("{{ trans('excel.msg2') }}", {
					// icon: "error",
					// });
				}else{
					swal("{{ trans('sysRefFee.msg3') }}", "", "success");
				}
			$('#jqxGrid').jqxGrid('updatebounddata');
			// var dataAdapter = new $.jqx.dataAdapter(data.data);
			// $("#jqxGrid").jqxGrid({ source: dataAdapter });
			
			},
			cache: false,
			contentType: false,
			processData: false
			});
			return false;
		});
	}

	function nonCar() {
		$('#importFile2').bootstrapFileInput();
		$('#subBox2 button[btnName="car_type_nm2"]').on('click', function(){
			$('#lookupModal').modal('show');
			initLookup('car_type_nm2', "{{ trans('sysRefFee.carTypeNm') }}", callBackFunc=function(data){
				$("#subBox2 input[name='car_type_nm'").val(data.cd_descp);
				$("#subBox2 input[name='car_type_cd'").val(data.cd);
			});
		});
		$('#subBox2 input[name="car_type_nm"]').on('click', function(){
			var check = $('#subBox2 input[name="car_type_nm"]').data('ui-autocomplete') != undefined;
			if(check == false) {
				initAutocomplete("subForm2","car_type_nm2",callBackFunc=function(data){
					$("#subBox2 input[name='car_type_cd'").val(data.cd);
				},"car_type_nm");
			}
		});

		$('#subBox4 button[btnName="car_type_nm4"]').on('click', function(){
			$('#lookupModal').modal('show');
			initLookup('car_type_nm4', "{{ trans('sysRefFee.carTypeNm') }}", callBackFunc=function(data){
				$("#subBox4 input[name='car_type_nm'").val(data.cd_descp);
				$("#subBox4 input[name='car_type_cd'").val(data.cd);
			});
		});

		$('#subBox4 input[name="car_type_nm"]').on('click', function(){
			var check = $('#subBox4 input[name="car_type_nm"]').data('ui-autocomplete') != undefined;
			if(check == false) {
				initAutocomplete("subForm4","car_type_nm4",callBackFunc=function(data){
					$("#subBox4 input[name='car_type_cd'").val(data.cd);
				},"car_type_nm");
			}
		});

		
		mainId = "mainId"
		$.ajax({
			url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee') }}"+ "?basecon=type;EQUAL;N",					
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				var custFieldData = [];
				custFieldData[0] = [
					{name: 'id', type: 'integer'},
					{name: 'car_type_nm', type: 'string'},
					{name: 'fee_unit', type: 'string'},
					{name: 'fee_from', type: 'float'},
					{name: 'fee_to', type: 'float'},
					{name: 'fee', type: 'float'},
					//{name: 'second_fee', type: 'float'},			
					{name: 'remark', type: 'string'},
				];
				custFieldData[1] = [
					{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false, hidden: true},
					{text:"{{ trans('sysRefFee.carTypeNm') }}", datafield: 'car_type_nm', filtertype:"textbox", width:220, editable: false},
					{text:"{{ trans('sysRefFee.feeUnit') }}", datafield: 'fee_unit', filtertype:"textbox", width:100, editable: false},
					{text:"{{ trans('sysRefFee.feeFrom') }}", datafield: 'fee_from', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.feeTo') }}", datafield: 'fee_to', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.fee') }}", datafield: 'fee', filtertype:"number", width:100, editable: false},
					//{text:"{{ trans('sysRefFee.secondFee') }}", datafield: 'second_fee', filtertype:"number", width:100},				
					{text:"{{ trans('sysRefFee.remark') }}", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
				];
				var opt = {};
				opt.gridId = "jqxGrid2";
				opt.fieldData = custFieldData;
				opt.formId = "subForm2";
				opt.saveId = "Save2";
				opt.cancelId = "Cancel2";
				opt.showBoxId = "subBox2";
				opt.height = $( window ).height() - 250;
				opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeNonCar/get') }}";
				opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeNonCar') }}" + "/store";
				opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeNonCar') }}" + "/update";
				opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeNonCar') }}" + "/delete/";
				opt.commonBtn = true;
				opt.showtoolbar = true;
				opt.defaultKey = {
					
				};
				opt.beforeSave = function (formData) {
					// $addBtn.jqxButton({disabled: false});
					// $delBtn.jqxButton({disabled: false});
					
				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});

		$("#myForm2").submit(function () {
			if ($('#importFile2').get(0).files.length === 0) {
				swal("{{ trans('sysRefFee.msg1') }}", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			$.ajax({
			url: "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeNonCar/importExcel') }}",
			type: 'POST',
			data: postData,
			async: false,
			beforeSend: function () {
				
			},
			error: function () {
				swal("{{ trans('sysRefFee.msg2') }}", "", "error");
				// swal("{{ trans('excel.msg2') }}", {
				// 	icon: "error",
				// });
				return false;
			},
			success: function (data) {
				//alert(data);
				if(data.msg == "error") {
					swal("{{ trans('sysRefFee.msg2') }}", "", "error");
					// swal("{{ trans('excel.msg2') }}", {
					// icon: "error",
					// });
				}else{
					swal("{{ trans('sysRefFee.msg3') }}", "", "success");
				}
			$('#jqxGrid2').jqxGrid('updatebounddata');
			// var dataAdapter = new $.jqx.dataAdapter(data.data);
			// $("#jqxGrid").jqxGrid({ source: dataAdapter });
			
			},
			cache: false,
			contentType: false,
			processData: false
			});
			return false;
		});
	}

	function nonCarNoDiscount() {
		
		mainId = "mainId"
		$.ajax({
			url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee') }}"+ "?basecon=type;EQUAL;N02",					
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				var custFieldData = [];
				custFieldData[0] = [
					{name: 'id', type: 'integer'},
					{name: 'car_type_nm', type: 'string'},
					{name: 'fee_unit', type: 'string'},
					{name: 'fee_from', type: 'float'},
					{name: 'fee_to', type: 'float'},
					{name: 'fee', type: 'float'},
					//{name: 'second_fee', type: 'float'},			
					{name: 'remark', type: 'string'},
				];
				custFieldData[1] = [
					{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false, hidden: true},
					{text:"{{ trans('sysRefFee.temperate') }}", datafield: 'car_type_nm', filtertype:"textbox", width:220, editable: false},
					{text:"{{ trans('sysRefFee.feeUnit') }}", datafield: 'fee_unit', filtertype:"textbox", width:100, editable: false},
					{text:"{{ trans('sysRefFee.feeFrom') }}", datafield: 'fee_from', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.feeTo') }}", datafield: 'fee_to', filtertype:"number", width:100, editable: false},
					{text:"{{ trans('sysRefFee.fee') }}", datafield: 'fee', filtertype:"number", width:100, editable: false},
					//{text:"{{ trans('sysRefFee.secondFee') }}", datafield: 'second_fee', filtertype:"number", width:100},				
					{text:"{{ trans('sysRefFee.remark') }}", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
				];
				var opt = {};
				opt.gridId = "jqxGrid4";
				opt.fieldData = custFieldData;
				opt.formId = "subForm4";
				opt.saveId = "Save4";
				opt.cancelId = "Cancel4";
				opt.showBoxId = "subBox4";
				opt.height = $( window ).height() - 250;
				opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeNonCarNoDiscount/get') }}";
				opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeNonCarNoDiscount') }}" + "/store";
				opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeNonCarNoDiscount') }}" + "/update";
				opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeNonCarNoDiscount') }}" + "/delete/";
				opt.commonBtn = true;
				opt.showtoolbar = true;
				opt.defaultKey = {
					
				};
				opt.beforeSave = function (formData) {
					// $addBtn.jqxButton({disabled: false});
					// $delBtn.jqxButton({disabled: false});
					
				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});

		$("#myForm2").submit(function () {
			if ($('#importFile2').get(0).files.length === 0) {
				swal("{{ trans('sysRefFee.msg1') }}", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			$.ajax({
			url: "{{ url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeNonCar/importExcel') }}",
			type: 'POST',
			data: postData,
			async: false,
			beforeSend: function () {
				
			},
			error: function () {
				swal("{{ trans('sysRefFee.msg2') }}", "", "error");
				// swal("{{ trans('excel.msg2') }}", {
				// 	icon: "error",
				// });
				return false;
			},
			success: function (data) {
				//alert(data);
				if(data.msg == "error") {
					swal("{{ trans('sysRefFee.msg2') }}", "", "error");
					// swal("{{ trans('excel.msg2') }}", {
					// icon: "error",
					// });
				}else{
					swal("{{ trans('sysRefFee.msg3') }}", "", "success");
				}
			$('#jqxGrid2').jqxGrid('updatebounddata');
			// var dataAdapter = new $.jqx.dataAdapter(data.data);
			// $("#jqxGrid").jqxGrid({ source: dataAdapter });
			
			},
			cache: false,
			contentType: false,
			processData: false
			});
			return false;
		});
	}
</script>
@endsection
