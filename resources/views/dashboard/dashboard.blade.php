@extends('backpack::layout')

@section('header')
<section class="content-header">
    <h1 style='display: inline;'>
        {{ trans('dashboard.titleName') }}<small></small>
    </h1>
    <button type="button" style="float: right;" id="updateimgsize">即時更新</button>
    <h1 style='float:right; display: inline;'>
        圖片已使用空間<span id="foldersize">{{$viewData['foldersize']}}</span>MB&nbsp;&nbsp;
    </h1>
  {{-- <ol class="breadcrumb" style='float:right;'>
        <li class="active">{{ trans('dashboard.titleName') }}</li>
  </ol> --}}
</section>
@endsection

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/plugins/select2/select2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/dist/css/skins/_all-skins.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/plugins/pace/pace.min.css">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/backpack/pnotify/pnotify.custom.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/backpack/backpack.base.css">

<link rel="stylesheet" href="https://core.standard-info.com/css/custom.css?v=20190305-002">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="https://core.standard-info.com/vendor/jquery/jquery-ui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.css">
    <style>
      #map_canvas {
          height: 500px;
          width: 100%
      }
      
      #map_canvas img {
          max-width: none;
      }
      
      #map_canvas div {
          -webkit-transform: translate3d(0, 0, 0);
      }

      .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
        .circlegreen {
            height: 15px;
            width: 15px;
            background-color: #00FF00;
            border-radius: 50%;
            display: inline-block;
        }
        .circlegred {
            height: 15px;
            width: 15px;
            background-color: #FF0000;
            border-radius: 50%;
            display: inline-block;
        }
</style>
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="{{ asset('vendor/ejs') }}/ejs.min.js"></script>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-list-alt" style="margin-top: 17px"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">{{ trans('dashboard.order') }}</span>
            <span class="info-box-number"><span id="ord">{{$viewData['ordSum']}}</span><small></small></span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-exclamation-triangle" style="margin-top: 17px"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">{{ trans('dashboard.error') }}</span>
            <span class="info-box-number" id="errorNum">{{$viewData['ordErrorSum']}}</span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-percent" style="margin-top: 17px"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">{{ trans('dashboard.rate') }}</span>
            <span class="info-box-number"><span id="ord1">{{$viewData['ordCompleteNum']}}</span>%</span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-clock-o" style="margin-top: 17px"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">{{ trans('dashboard.eta') }}</span>
            <span class="info-box-number">{{$viewData['lastTime']}}</span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('dashboard.map') }}</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div id="map_canvas" style="position: relative; overflow: hidden;"></div>
            </div>
        </div>
    </div>


</div>

{{--  <div class="row">
    <div class="col-md-12">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('dashboard.onlineTruck') }}</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body" id="transports">
            </div>
        </div>
    </div>
</div>  --}}
@can('gpscar')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title" style="display:inline;">司機在線狀況</h3> <button type="button"  style="float: right;" id="updatedriver">即時更新 </button>  <button type="button" style="float: right;" id="sendmsg">傳送訊息 </button>
        </div>
        <div id="jqxGrid" class="col-md-12">
        <table id="tab0" style="display:inline;width:80%;"> 
            <tr id="tra" style='width:15%;'>
            </tr>
        </table >
        <table id="tab1" style="display:inline;width:80%;"> 
            <tr id="tr0" style='width:15%;'>
            </tr>
        </table >
        <table id="tab2" style="display:inline;width:50%"> 
            <tr id="trx" style='width:15%;'>
            </tr>
        </table >
        <table id="tab3" style="display:inline;width:50%"> 
            <tr id="try" style='width:15%;'>
            </tr>
        </table >
        <table id="tab4" style="display:inline;width:50%"> 
            <tr style='width:15%;'>
            </tr>
        </table >
        <table id="tab5" style="display:inline;width:50%"> 
            <tr style='width:15%;'>
            </tr>
        </table >
        <table id="tab6" style="display:inline;width:50%"> 
        <tr style='width:15%;'>
            </tr>
        </table >
        <table id="tab7" style="display:inline;width:50%"> 
        <tr style='width:15%;'>
            </tr>
        </table >
        <table id="tab8" style="display:inline;width:50%"> 
        <tr style='width:15%;'>
            </tr>
        </table >
        </div>
            <!-- ./box-body -->
        <div class="box-footer">
            
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
    </div>
</div>
@endcan
<div class="modal fade"  role="dialog" id="dlvPlanModal">
	<div class="modal-dialog modal-lg" role="document" style="width:500px">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">訊息通知</h4>
			</div>
			<div class="modal-body" id="dlvPlanBody" style="height: 200px;width:100%">
                <div class="row">
                    <div class="form-group col-md-12">
                        <select class="form-control select2" style="width: 100%; text-align:center" id='carno' name="state">
                                @foreach($viewData['cardata'] as $row)
                                    <option value="{{$row->email}}">{{$row->email}}</option>
                                @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="goods_no2">內容</label>
                        <textarea class="form-control" id='msgcontent' rows="3" name="remark"></textarea>
                    </div>
                </div>
			</div>
			<div class="modal-footer">
                <button type="button" class="btn btn-primary" id="confirmInsertBtn">確定發送</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<script id="transports-template" type="x-ejs">
<% transports.forEach(function(transport) { %>
<div class="mdl-card" style="background:rgb(250, <%= Math.round(transport.power * 2.5 ) %>, <%= Math.round(transport.power * 2.5 ) %>)">
  <div class="mdl-card__title">
    <h3 class="mdl-card__title-text"><%= transport.id %></h3>
  </div>
  <div class="mdl-card__actions">
  <img src="<%= transport.map %>">
  <ul>
    <!--<li><label>Location:</label><%= transport.lat %>,<%= transport.lng %></li>-->
    <li><label>{{ trans('dashboard.battery') }}:</label><%= transport.power %>%</li>
    <li><label>{{ trans('dashboard.speed') }}:</label><%= transport.speed %>km/h</li>
    <li><label>{{ trans('dashboard.updated') }}:</label><%= transport.time %></li>
  </ul>
  </div>
</div>
<% }) %>
</script>


<div class="row">
    @can('dashboard_salesByMonth')
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">每月營業額</h3>
            </div>
            <!-- /.box-header -->
            <form action="" class="form-horizontal" id="salesByMothForm">
            <div class="box-body" style="height:452px;">
                    <div class="form-group">
                        <label for="year" class="col-sm-2 control-label">年</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="year" id="salesByYear">
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022"selected>2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                                <option value="2029">2029</option>
                                <option value="2030">2030</option>
                            </select>
                        </div>
                    </div>
                <canvas id="myChart" style="height:350px; width:100%"></canvas>
                <p style="font-size: 16px; font-weight: 800;">累計金額：<span id="byMonthTotal">0</span></p>
            </div>
            </form>
            <!-- ./box-body -->
            <div class="box-footer">
                
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>
    @endcan
    <!-- /.col -->
    @can('dashboard_salesByDay')
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">每日營業額</h3>
            </div>
            <!-- /.box-header -->
            <form action="" class="form-horizontal" id="salesByDayForm">
            <div class="box-body" style="height:452px;">
                    <div class="form-group">
                        <label for="year" class="col-sm-2 control-label">年</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="year">
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022"selected>2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                                <option value="2029">2029</option>
                                <option value="2030">2030</option>
                            </select>
                        </div>

                        <label for="year" class="col-sm-2 control-label">月</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="month">
                                <option value="1">一月</option>
                                <option value="2">二月</option>
                                <option value="3">三月</option>
                                <option value="4">四月</option>
                                <option value="5">五月</option>
                                <option value="6">六月</option>
                                <option value="7">七月</option>
                                <option value="8">八月</option>
                                <option value="9">九月</option>
                                <option value="10">十月</option>
                                <option value="11">十一月</option>
                                <option value="12">十二月</option>
                            </select>
                        </div>
                    </div>  
           
                <canvas id="chartByDay" style="height:350px; width:100%"></canvas>
                <p style="font-size: 16px; font-weight: 800;">累計金額：<span id="byDayTotal">0</span></p>
            </div>
            </form>
            <!-- ./box-body -->
            <div class="box-footer">
                
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>
    @endcan
    <!-- /.col -->
</div>

<div class="row">
    @can('dashboard_ordersByMonth')
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">每月訂單量</h3>
            </div>
            <!-- /.box-header -->
            <form action="" class="form-horizontal" id="orderByMothForm">
            <div class="box-body" style="height:452px;">
                    <div class="form-group">
                        <label for="year" class="col-sm-2 control-label">年</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="year" id="orderByYear">
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022"selected>2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                                <option value="2029">2029</option>
                                <option value="2030">2030</option>
                            </select>
                        </div>
                    </div>
                <canvas id="orderMonthChart" style="height:350px; width:100%"></canvas>
                <p style="font-size: 16px; font-weight: 800;">累計訂單數：<span id="orderbyMonthTotal">0</span></p>
            </div>
            </form>
            <!-- ./box-body -->
            <div class="box-footer">
                
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>
    @endcan
    <!-- /.col -->
    @can('dashboard_ordersByDay')
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">每日訂單量</h3>
            </div>
            <!-- /.box-header -->
            <form action="" class="form-horizontal" id="orderByDayForm">
            <div class="box-body" style="height:452px;">
                    <div class="form-group">
                        <label for="year" class="col-sm-2 control-label">年</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="year">
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022"selected>2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                                <option value="2029">2029</option>
                                <option value="2030">2030</option>
                            </select>
                        </div>

                        <label for="year" class="col-sm-2 control-label">月</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="month">
                                <option value="1">一月</option>
                                <option value="2">二月</option>
                                <option value="3">三月</option>
                                <option value="4">四月</option>
                                <option value="5">五月</option>
                                <option value="6">六月</option>
                                <option value="7">七月</option>
                                <option value="8">八月</option>
                                <option value="9">九月</option>
                                <option value="10">十月</option>
                                <option value="11">十一月</option>
                                <option value="12">十二月</option>
                            </select>
                        </div>
                    </div>  
           
                <canvas id="orderDayChart" style="height:350px; width:100%"></canvas>
                <p style="font-size: 16px; font-weight: 800;">累計訂單數：<span id="orderbyDayTotal">0</span></p>
            </div>
            </form>
            <!-- ./box-body -->
            <div class="box-footer">
                
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>
    @endcan
    <!-- /.col -->
</div>

<style>
    .chat-room{
        position: fixed;
        bottom: 0px;
        right: 0px;
    }
</style>

{{--  <div class="row">
    <div class="col-md-3 chat-room">
        <div id="app">
            <div class="box box-primary direct-chat direct-chat-primary" style="margin:0">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('dashboard.customerService') }}</h3>

                    <div class="box-tools pull-right">
                    <span data-toggle="tooltip" title="" class="badge bg-light-blue" data-original-title="3 New Messages">3</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-widget="chat-pane-toggle" data-original-title="Contacts">
                        <i class="fa fa-comments"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="">
                    <!-- 註解：使用template來當迴圈容器，或是判斷用的容器，當條件達成時產出template內容 -->
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages" id="js-roomBody">
                        <template v-for="item in messages">
                            <!-- other people -->
                            <template v-if="item.userName != userName">
                                <!-- Message. Default to the left -->
                                <div class="direct-chat-msg">
                                    <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left">@{{item.userName}}</span>
                                    <span class="direct-chat-timestamp pull-right">@{{item.timeStamp}}</span>
                                    </div>
                                    <!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="http://fakeimg.pl/40x40/" alt="Message User Image"><!-- /.direct-chat-img -->
                                    <div v-if="item.type == 'text'" class="messageBox__message">
                                    <div class="direct-chat-text">
                                        @{{item.message}}
                                    </div>
                                    </div>
                                    <div v-if="item.type == 'image'" class="messageBox__image"><img :src="item.message"></div>
                                    <!-- /.direct-chat-text -->
                                </div>
                                <!-- /.direct-chat-msg -->
                            </template>
                            <!-- 區塊：self -->
                            <template v-if="item.userName == userName">
                                <!-- Message to the right -->
                                <div class="direct-chat-msg right">
                                    <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-right">@{{item.userName}}</span>
                                    <span class="direct-chat-timestamp pull-left">@{{item.timeStamp}}</span>
                                    </div>
                                    <!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" alt="Message User Image"><!-- /.direct-chat-img -->
                                    <div v-if="item.type == 'text'" class="messageBox__message">
                                    <div class="direct-chat-text">
                                        @{{item.message}}
                                    </div>
                                    </div>
                                    <div v-if="item.type == 'image'" class="messageBox__image"><img :src="item.message"></div>
                                    <!-- /.direct-chat-text -->
                                </div>
                                <!-- /.direct-chat-msg -->
                            </template>
                        </template>
                        <!-- 區塊：上傳進度條 -->
                        <div v-show="upload" class="messageBox messageBox--self">
                        <div class="messageBox__progress">
                            <div id="js-progressBar" class="messageBox__progress--state"></div>
                            <div class="messageBox__progress--number">@{{progress}}</div>
                        </div>
                        </div>
                    </div>
                    <!--/.direct-chat-messages-->

                    <!-- Contacts are loaded here -->
                    <div class="direct-chat-contacts">
                    <ul class="contacts-list">
                        <li>
                        <a href="#">
                            <img class="contacts-list-img" src="http://fakeimg.pl/128x128/" alt="User Image">

                            <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                    Count Dracula
                                    <small class="contacts-list-date pull-right">2/28/2015</small>
                                </span>
                            <span class="contacts-list-msg">How have you been? I was...</span>
                            </div>
                            <!-- /.contacts-list-info -->
                        </a>
                        </li>
                        <!-- End Contact Item -->
                    </ul>
                    <!-- /.contatcts-list -->
                    </div>
                    <!-- /.direct-chat-pane -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer" style="">

                    <div class="input-group">
                        <input type="text" name="message" placeholder="Type Message ..." id="js-message" class="form-control" @keydown.enter="sendMessage($event)">
                            
                            <span class="input-group-btn">
                            <span class="btn btn-primary" id="fileUpload">
                                +<input type="file" id="myInput" accept="image/*" style="display:none"   @change="sendImage($event)">
                            </span>
                            <button type="button" class="btn btn-primary btn-flat" @click="sendMessage()">Send</button>
                            </span>
                    </div>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
        
    </div>
</div>  --}}


@endsection

@section('after_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ79gLT1qaLfNJDf98OJr8cT5FZ4MJ4_w&libraries=geometry,places&ext=.js"></script>
<script type="text/javascript" src="https://aishek.github.io/jquery-animateNumber/javascripts/jquery.animateNumber.js"></script>
<script>
    var byDayChart = null;
    var byYearChart = null;
    var orderbyDayChart = null;
    var orderbyMonthChart = null;
    var cardata = "<option value=''> </option>";
    var area = 0;
    $(function(){
        $.get("{{url('admin/getDriver')}}", {},function(data){
            var i = 0;
            for (k = 1; k <= data.test.length; k++) {
                cardata +="<option value='"+data.test[i]["email"]+"'>"+data.test[i]["email"]+"</option>";
                area = Math.ceil(k/7);
                if(data.test[i]["status"]=="Y"){
                    $("#tab"+area.toString()).append("<tr id='tax"+i+"' style='height:40px;'><td style='font-size:18px;'> <span class='circlegreen'> </span>  <a href='#'  onclick='searchCar()'>"+data.test[i]["email"]+"</a>&nbsp &nbsp </td> <td style='font-size:18px'> <span>"+data.test[i]["gps_time"]+" </span></td> </tr>");
                }else{
                    $("#tab"+area.toString()).append("<tr id='tax"+i+"' style='height:40px;'><td style='font-size:18px;'> <span class='circlegred'> </span>  <a href='#'  onclick='searchCar()'>"+data.test[i]["email"]+"</a>&nbsp &nbsp </td> <td style='font-size:18px'> <span>"+data.test[i]["gps_time"]+" </span></td> </tr>");
                }
                i++;
            }
        });
        //doPollForOrd();
        //doPollForError();
        //doPollForOrd1();
        function doPollForOrd(){
            var num = Math.floor((Math.random() * (99-80+1)) + 80);
            $('#ord').animateNumber({ number: num });
            setTimeout(doPollForOrd,5000);
        }

        function doPollForError(){
            var num = Math.floor((Math.random() * (5-1+1)) +1);
            $('#errorNum').animateNumber({ number: num });
            setTimeout(doPollForError,10000);
        }

        function doPollForOrd1(){
            var num = Math.floor((Math.random() * (99-90+1)) + 90);
            $('#ord1').animateNumber({ number: num });
            setTimeout(doPollForOrd1,5000);
        }
    });

    @can('dashboard_salesByMonth')
    $(function(){
        var d = new Date();
        var n = d.getFullYear();
        //$("#salesByYear").val(n);
        getAmtReportByMonth(n);

        $("#salesByYear").on("change", function(){
            var year = $(this).val();
            getAmtReportByMonth(year);
        });
        function getAmtReportByMonth(year) {
            if(byYearChart != null) {
                byYearChart.destroy();
            }
            var ctx = document.getElementById("myChart");
            $.get("{{url('admin/getAmtReportByMonth')}}", {year: year}, function(data){
                byYearChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: data.label,
                        datasets: [{
                            label: '營業額',
                            data: data.data,
                            backgroundColor: 'rgba(54, 162, 235, 0.2)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1
                        }]
                    },
                    options: {
                        "hover": {
                            "animationDuration": 0
                        },
                        "animation": {
                            "duration": 1,
                            "onComplete": function () {
                                var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'bottom';

                                this.data.datasets.forEach(function (dataset, i) {
                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                    meta.data.forEach(function (bar, index) {
                                        var data = addCommas(parseInt(dataset.data[index]));      
                                                            
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                    });
                                });
                            }
                        },
                        legend: {
                            "display": false
                        },
                        tooltips: {
                            "enabled": false
                        }
                    }
                });

                $("#byMonthTotal").text(addCommas(data.total));
            }, "JSON");
        }
    });
    @endcan

    @can('dashboard_salesByDay')
    $(function(){
        var d = new Date();
        var y = d.getFullYear();
        var n = d.getMonth();
        $("#salesByDayForm select[name='month']").val(n+1);
        getAmtReportByDay(y, n + 1);

        $("#salesByDayForm select[name='month']").on("change", function(){
            var month = $(this).val();
            var year = $("#salesByDayForm select[name='year']").val();
            getAmtReportByDay(year, month);
        });

        $("#salesByDayForm select[name='year']").on("change", function(){
            var year = $(this).val();
            var month = $("#salesByDayForm select[name='month']").val();
            getAmtReportByDay(year, month);
        });


        function getAmtReportByDay(year, month) {
            if(byDayChart != null) {
                byDayChart.destroy();
            }
            var ctx = document.getElementById("chartByDay");
            $.get("{{url('admin/getAmtReportByDay')}}", {year: year, month: month}, function(data){
                byDayChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: data.label,
                        datasets: [{
                            label: '營業額',
                            data: data.data,
                            backgroundColor: 'rgba(54, 162, 235, 0.2)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1
                        }]
                    },
                    options: {
                        "hover": {
                            "animationDuration": 0
                        },
                        // "animation": {
                        //     "duration": 1,
                        //     "onComplete": function () {
                        //         var chartInstance = this.chart,
                        //         ctx = chartInstance.ctx;

                        //         ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        //         ctx.textAlign = 'center';
                        //         ctx.textBaseline = 'bottom';

                        //         this.data.datasets.forEach(function (dataset, i) {
                        //             var meta = chartInstance.controller.getDatasetMeta(i);
                        //             meta.data.forEach(function (bar, index) {
                        //                 var data = addCommas(parseInt(dataset.data[index]));      
                                                            
                        //                 ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        //             });
                        //         });
                        //     }
                        // },
                        legend: {
                            "display": false
                        },
                        tooltips: {
                            "enabled": true,
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || 'Other';
                                    var label = data.labels[tooltipItem.index];
                                    return datasetLabel + ': ' + addCommas(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]));
                                }
                            }
                        }
                    }
                });

                $("#byDayTotal").text(addCommas(data.total));
            }, "JSON");
        }
        
    });
    @endcan

    @can('dashboard_ordersByMonth')
    $(function(){
        var d = new Date();
        var n = d.getFullYear();
        //$("#salesByYear").val(n);
        ordersByMonth(n);

        $("#orderByYear").on("change", function(){
            var year = $(this).val();
            ordersByMonth(year);
        });
        
        function ordersByMonth(year) {
            if(orderbyMonthChart != null) {
                orderbyMonthChart.destroy();
            }
            var ctx = document.getElementById("orderMonthChart");
            $.get("{{url('admin/getOrderReportByMonth')}}", {year: year}, function(data){
                orderbyMonthChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: data.label,
                        datasets: [{
                            label: '營業額',
                            data: data.data,
                            backgroundColor: 'rgba(75, 192, 192, 0.2)',
                            borderColor: 'rgba(75, 192, 192, 1)',
                            borderWidth: 1
                        }]
                    },
                    options: {
                        "hover": {
                            "animationDuration": 0
                        },
                        "animation": {
                            "duration": 1,
                            "onComplete": function () {
                                var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'bottom';

                                this.data.datasets.forEach(function (dataset, i) {
                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                    meta.data.forEach(function (bar, index) {
                                        var data = addCommas(parseInt(dataset.data[index]));      
                                                            
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                    });
                                });
                            }
                        },
                        legend: {
                            "display": false
                        },
                        tooltips: {
                            "enabled": false
                        }
                    }
                });

                $("#orderbyMonthTotal").text(addCommas(data.total));
            }, "JSON");
        }
    });
    @endcan

    @can('dashboard_ordersByDay')
    $(function(){
        var d = new Date();
        var y = d.getFullYear();
        var n = d.getMonth();
        $("#orderByDayForm select[name='month']").val(n+1);
        getOrderReportByDay(y, n + 1);

        $("#orderByDayForm select[name='month']").on("change", function(){
            var month = $(this).val();
            var year = $("#orderByDayForm select[name='year']").val();
            getOrderReportByDay(year, month);
        });

        $("#orderByDayForm select[name='year']").on("change", function(){
            var year = $(this).val();
            var month = $("#orderByDayForm select[name='month']").val();
            getOrderReportByDay(year, month);
        });


        function getOrderReportByDay(year, month) {
            if(orderbyDayChart != null) {
                orderbyDayChart.destroy();
            }
            var ctx = document.getElementById("orderDayChart");
            $.get("{{url('admin/getOrderReportByDay')}}", {year: year, month: month}, function(data){
                orderbyDayChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: data.label,
                        datasets: [{
                            label: '訂單量',
                            data: data.data,
                            backgroundColor: 'rgba(75, 192, 192, 0.2)',
                            borderColor: 'rgba(75, 192, 192, 1)',
                            borderWidth: 1
                        }]
                    },
                    options: {
                        "hover": {
                            "animationDuration": 0
                        },
                        "animation": {
                            "duration": 1,
                            "onComplete": function () {
                                var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'bottom';

                                this.data.datasets.forEach(function (dataset, i) {
                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                    meta.data.forEach(function (bar, index) {
                                        var data = addCommas(parseInt(dataset.data[index]));      
                                                            
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                    });
                                });
                            }
                        },
                        legend: {
                            "display": false
                        },
                        tooltips: {
                            "enabled": false
                        }
                    }
                });

                $("#orderbyDayTotal").text(addCommas(data.total));
            }, "JSON");
        }
        
    });
    @endcan

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
</script>
<script src="https://unpkg.com/vue"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.0/firebase.js"></script>
<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>
<SCRIPT type="text/javascript">
    $(document).ready(function() {
        $("#carno").select2();
    });
</SCRIPT>
<script>
    var markers = [];
    var infowindows = [];
    $(function() {
        $("#fileUpload").on("click",function(event){
            //event.stopPropagation()
            $('#myInput')[0].click();
        });    
        
        $("#confirmInsertBtn").on("click",function(event){
            var title = document.getElementById('carno').value;
            var content = document.getElementById('msgcontent').value;
            $.post(BASE_URL + "/sendormsg/", {'title': title,'content': content}, function(data){
                    console.log(data);
                    if(data == "SUCCESS") {
                        swal("操作成功", "", "success");
                    }
                    else{
                        swal(data.msg, data.Message, "error");
                    }
                    });
            $("#dlvPlanModal").modal('hide');
            $("#msgcontent").val("");
        })

        $("#sendmsg").on("click",function(event){
            $("#dlvPlanModal").modal('show');  
        })
        $("#updateimgsize").on("click",function(event){
            $.post(BASE_URL + "/updatefordrsize/", function(data){
                    console.log(data);
                    $("#foldersize").text(data);
            });
        })
        $("#updatedriver").on("click",function(event){
            for (x = 0; x <= 7; x++) {
                $("#tab"+x.toString()).html("");
                $("#tab"+x.toString()).append("<tbody>");
            }
            $.get("{{url('admin/getDriver')}}", {},function(data){
            var i = 0;
            for (k = 1; k <= data.test.length; k++) {
                area = Math.ceil(k/7);
                if(data.test[i]["status"]=="Y"){
                    $("#tab"+area.toString()).append("<tr id='tax"+i+"' style='height:40px;'><td style='font-size:18px;'> <span class='circlegreen'> </span>  <a href='#'  onclick='searchCar()'>"+data.test[i]["email"]+"</a>&nbsp &nbsp </td> <td style='font-size:18px'> <span>"+data.test[i]["gps_time"]+" </span></td> </tr>");
                }else{
                    $("#tab"+area.toString()).append("<tr id='tax"+i+"' style='height:40px;'><td style='font-size:18px;'> <span class='circlegred'> </span>  <a href='#'  onclick='searchCar()'>"+data.test[i]["email"]+"</a>&nbsp &nbsp </td> <td style='font-size:18px'> <span>"+data.test[i]["gps_time"]+" </span></td> </tr>");
                }
                i++;
            }
            for (y = 0; y <= 7; y++) {
                $("#tab"+y.toString()).append("</tbody>");
            }
            })
        });
        @can('gpscar')
        var myVar=setInterval(function(){myTimer()},300000);
        function myTimer(){
            console.log("nick");
            for (a = 0; a <= 7; a++) {
                $("#tab"+a.toString()).html("");
                $("#tab"+a.toString()).append("<tbody>");
            }
            $.get("{{url('admin/getDriver')}}", {},function(data){
            var i = 0;
            for (k = 1; k <= data.test.length; k++) {
                area = Math.ceil(k/7);
                if(data.test[i]["status"]=="Y"){
                    $("#tab"+area.toString()).append("<tr id='tax"+i+"' style='height:40px;'><td style='font-size:18px;'> <span class='circlegreen'> </span>  <a href='#'  onclick='searchCar()'>"+data.test[i]["email"]+"</a>&nbsp &nbsp </td> <td style='font-size:18px'> <span>"+data.test[i]["gps_time"]+" </span></td> </tr>");
                }else{
                    $("#tab"+area.toString()).append("<tr id='tax"+i+"' style='height:40px;'><td style='font-size:18px;'> <span class='circlegred'> </span>  <a href='#'  onclick='searchCar()'>"+data.test[i]["email"]+"</a>&nbsp &nbsp </td> <td style='font-size:18px'> <span>"+data.test[i]["gps_time"]+" </span></td> </tr>");
                }
                i++;
            }
            for (b = 0; b <= 7; b++) {
                $("#tab"+b.toString()).append("</tbody>");
            }
            })
        }
        @endcan
    });
    {{--  var thisUser = '{{ Auth::user()->name }}';

    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyDVfLzY9RXWp2WBPjWP9NPzcrTlcbjJrko",
        authDomain: "test-bcfaa.firebaseapp.com",
        databaseURL: "https://test-bcfaa.firebaseio.com",
        projectId: "test-bcfaa",
        storageBucket: "test-bcfaa.appspot.com",
        messagingSenderId: "451958662330"
    };
    if (firebase.apps.length == 0) {
        firebase.initializeApp(config);
        // msgRef = firebase中的資料表/messages/，若沒有的會自動建立
        const msgRef = firebase.database().ref('/messages/');
        const storageRef = firebase.storage().ref('/images/');
        var app2 = new Vue({
            el: '#app',
            // 資料位置，於html中可用{{}}渲染出來
            data() {
                return {
                    userNameSet: false, // 姓名輸入框
                    userName: thisUser, // 名稱
                    messages: [], // 訊息內容
                    upload: false, // 上傳進度框
                    progress: '' // 上傳進度%數
                }
            },
            // 這個頁面的functions
            methods: {
            /** 彈出設定視窗 */
            setName() {
                const vm = this;
                vm.userNameSet = true;
            },
            /** 儲存設定名稱 */
            saveName() {
                // vue的mtthod中this是指export中這整塊的資料
                const vm = this;
                const userName = thisUser;
                if (userName.trim() == '') { return; }
                // 這裡的vm.userName(this.userName)就是data()裡面的userName
                vm.userName = userName;
                vm.userNameSet = false;
            },
            /** 取得時間 */
            getTime() {
                const now = new Date();
                const hours = now.getHours();
                const minutes = now.getMinutes();
                return `${(hours >= 12) ? "下午" : "上午"} ${hours}:${(minutes < 10) ? '0' + minutes : minutes}`;
            },
            /** 傳送訊息 */
            sendMessage(e) {
                const vm = this;
                const userName = thisUser;
                let message = document.querySelector('#js-message');
                // 如果是按住shift則不傳送訊息(多行輸入)
                if (e.shiftKey) {
                return false;
                }
                // 如果輸入是空則不傳送訊息
                if (message.value.length <= 1 && message.value.trim() == '') {
                // 避免enter產生的空白換行
                e.preventDefault();
                return false;
                }
                // 對firebase的db做push，db只能接受json物件格式，若要用陣列要先轉字串來存
                msgRef.push({
                userName: thisUser,
                type: 'text',
                message: message.value,
                // 取得時間，這裡的vm.getTime()就是method中的getTime
                timeStamp: vm.getTime()
                })
                // 清空輸入欄位並避免enter產生的空白換行
                message.value = '';
                e.preventDefault();
            },
            /** 傳送圖片 */
            sendImage(e) {
                const vm = this;
                const userName = thisUser;
                // 取得上傳檔案的資料
                const file = e.target.files[0];
                const fileName = Math.floor(Date.now() / 1000) + `_${file.name}`;
                const metadata = {
                contentType: 'image/*'
                };
                // 取得HTML進度條元素
                let progressBar = document.querySelector('#js-progressBar');
                // 上傳資訊設定
                const uploadTask = storageRef.child(fileName).put(file, metadata);
                // 上傳狀態處理
                uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                /* 上傳進度 */
                function(snapshot) {
                    let progress = Math.floor((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    if (progress < 100) {
                    // 開啟進度條
                    vm.upload = true;
                    vm.progress = `${progress}%`;
                    progressBar.setAttribute('style', `width:${progress}%`);
                    }
                },
                /* 錯誤處理 */
                function(error) {
                    msgRef.child('bug/').push({
                    userName: thisUser,
                    type: 'image',
                    message: error.code,
                    timeStamp: vm.getTime()
                    })
                },
                /* 上傳結束處理 */
                function() {
                    var downloadURL = uploadTask.snapshot.downloadURL;
                    msgRef.push({
                    userName: thisUser,
                    type: 'image',
                    message: downloadURL,
                    timeStamp: vm.getTime()
                    })
                    // 關閉進度條
                    vm.upload = false;
                });
            },
            /** 顯示更多 */
            readMore(e) {
                // 把內容高度限制取消
                e.target.previousElementSibling.setAttribute('style', 'max-height: 100%;')
                // 隱藏"顯示更多"按紐
                e.target.setAttribute('style', 'display: none;');
            }
            },
            // mounted是vue的生命週期之一，代表模板已編譯完成，已經取值準備渲染元件了
            mounted() {
            const vm = this;
            msgRef.on('value', function(snapshot) {
                const val = snapshot.val();
                vm.messages = val;
            })
            },
            // update是vue的生命週期之一，在元件渲染完成後執行
            updated() {
            // 判斷內容高度超過300就隱藏起來，把"顯示更多"按紐打開
            const messages = document.querySelectorAll('.messageBox__message');
            messages.forEach((message) => {
                if (message.offsetHeight > 300) {
                message.querySelector('.messageBox__readMore').setAttribute('style', 'display: block');
                }
            })
            // 當畫面渲染完成，把聊天視窗滾到最底部(讀取最新消息)
            const roomBody = document.querySelector('#js-roomBody');
            roomBody.scrollTop = roomBody.scrollHeight;
            }
        })
    }  --}}

    var map;
    var directionDisplay;
    var directionsService;
    var stepDisplay;
    var markerArray = [];
    var position;
    var marker = null;
    var polyline = null;
    var poly2 = null;
    var speed = 0.000005,
        wait = 1;
    var infowindow = null;
    var timerHandle = null;

    // Create a map and center it on Manhattan.
    var myOptions = {
        center:new google.maps.LatLng(25.070245, 121.2775058),
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP,

    };
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    infowindow = new google.maps.InfoWindow({
        size: new google.maps.Size(150, 50)
    });
    // Instantiate a directions service.
    directionsService = new google.maps.DirectionsService();

    

    

    var car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";
    var icon = {
        path: car,
        scale: .7,
        strokeColor: 'white',
        strokeWeight: .10,
        fillOpacity: 1,
        fillColor: '#404040',
        offset: '5%',
        rotation: 90,
        anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
    };

    var infowindow = new google.maps.InfoWindow({
        content: '<table class="table table-bordered"><tr><td colspan="2">Car Info.</td></tr><tr><td>車況</td><td>良好</td></tr><tr><tr><td>latitude</td><td>25.041591</td></tr><tr><td>longitude</td><td>121.538248</td></tr></table>'
    });
    // console.log(infowindow);
    
    function searchCar() {
        var car_no = event.target.innerText;
        // clearMarkers();
        $.get("{{ url(config('backpack.base.route_prefix','admin').'/car/get') }}/" + car_no, {}, function(data){
            map.setCenter({
                            lat: data.lat,
                            lng: data.lng
                    });
            // var marker = new google.maps.Marker({
            // position: new google.maps.LatLng(data.lat,data.lng),
            // map: map,
            // icon: "https://cdn4.iconfinder.com/data/icons/shopping-colorful-flat-long-shadow/136/Shopping_icons-31-24-48.png"
            // });
            // markers.push(marker);
            // moveMarker(data, marker);
        });
    };

    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
        markers = [];
    }
    $(function(){
        
        $('body').tooltip('disable');

        // marker = new google.maps.Marker({
        //     position: new google.maps.LatLng(25.041591, 121.538248),
        //     map: map,
        //     icon: icon
        // });

        // marker.addListener('click', function() {
        //     infowindow.open(map, marker);
        // });

        // var icon = {
        //     path: car,
        //     scale: .7,
        //     strokeColor: 'white',
        //     strokeWeight: .10,
        //     fillOpacity: 1,
        //     fillColor: '#404040',
        //     offset: '5%',
        //     rotation: 270,
        //     anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
        // };

        // var marker1 = new google.maps.Marker({
        //     position: new google.maps.LatLng(25.048162, 121.549448),
        //     map: map,
        //     icon: icon
        // });


        // var icon = {
        //     path: car,
        //     scale: .7,
        //     strokeColor: 'white',
        //     strokeWeight: .10,
        //     fillOpacity: 1,
        //     fillColor: '#404040',
        //     offset: '5%',
        //     anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
        // };

        // var marker2 = new google.maps.Marker({
        //     position: new google.maps.LatLng(25.050339, 121.543912),
        //     map: map,
        //     icon: icon
        // });
    });  
</script>
<script>
    /* websocket test */
    initWebsocket();
    function initWebsocket () {
        let wsURL = 'wss://dev-wms.target-ai.com:9599/websocket/getConnect/groupId_StdTmsGpsDataByWebsocket';
        let ws = new WebSocket(wsURL); // 建立連線
        ws.onopen = function(e) {
            // websocketonopen(e);
            console.log('ws 連線成功~~');
        };
        ws.error = function(error) {
            // websocketonerror(error);
            console.error('ws 連線失敗', error);
        };
        ws.onmessage = function(e) {
            websocketonmessage(e);
        };
        ws.onclose = function(e) {
            setTimeout(function() {
                initWebsocket();
            }, 1000);
            if (e.wasClean) {
                websocketclose(e);
            } else {
                console.log(e.code);
            }
        };
    }
    // function websocketonopen (e) {
    //     console.log('ws 連線成功~~');
    // }
    // function websocketonerror (error) {
    //     console.error('ws 連線失敗', error);
    // }
    function websocketonmessage (e) {
        // 後端通知前端，前端取得資料
        // console.log(e);
        let _data = e.data;
        console.log('ws 取得資料', _data);
        if (_data.indexOf('sessionid') === -1) {
            let obj = (JSON.parse('[' + _data.split('=').join('":"').split(', ').join('","').split('{').join('{"').split('"[').join('[').split('}]}').join('"}]}') + ']'))[0];
            let key = (Object.keys(obj))[0];
            obj.id = key;
            // console.log(markers);
            // console.log(obj[key]);
            let markersLength = markers.length;
            for (let i = 0; i < markersLength; i++) {
                // console.log(markers[i].getTitle())
                if (markers[i].getTitle() === key) {
                    updatetMarker(obj[key][0], markers[i], i);
                }
            }
        }
    }
    function websocketsend (data) {
        // 前端丟資料
        console.log('send data', data);
    }
    function websocketclose () {
        console.log('ws 關閉連線');
    }
</script>
<script src="{{ asset('js') }}/main.js?v={{Config::get('app.version')}}"></script>

@endsection