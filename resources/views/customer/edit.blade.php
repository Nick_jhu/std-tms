@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        Customers<small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/log') }}">{{ trans('backpack::logmanager.log_manager') }}</a></li>
        <li class="active">{{ trans('backpack::logmanager.existing_logs') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('backpack::template.toolbar')
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Basic Information</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Details</a></li>
                    <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Remark</a></li>
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="cust_no">Customer No.</label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no" placeholder="Enter No." >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="name">Customer Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="email">Email</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input type="eamil" class="form-control" id="email" name="email" >
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="cust_contact">Contact Person</label>
                                        <input type="text" class="form-control" id="cust_contact" name="cust_contact"  placeholder="Enter Contact" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="gender">Gender</label>
                                        <div class="radio">
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" value="m">male
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" value="f">female
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="phone">Phone</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" class="form-control" id="phone" name="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="cust_date">Date</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker" name="cust_date">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="city">Select</label>
                                        <select class="form-control" id="city" name="city">
                                            <option value="taipei">Taipei</option>
                                            <option value="taichung">Taichung</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="spec_service">Checkbox</label>
                                        <div class="checkbox">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="spec_service[]" value="1">checkbox1
                                            </label>
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="spec_service[]" value="2">checkbox2
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class='row'>
                                    <div class="form-group col-md-8">
                                        <label>Multiple</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a State" style="width: 100%;" name="state[]">

                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="exampleInputFile">File input</label>
                                        <input type="file" id="exampleInputFile" >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="address">Address</label>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address">
                                    </div>
                                </div>

                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="box box-primary" id="subBox" style="display:none">
                            <div class="box-header with-border">
                            <h3 class="box-title">Quick Example</h3>
                            </div>
                            <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="name">Customer Name</label>
                                        <input type="text" class="form-control input-sm" name="name" grid="true" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="phone">phone</label>
                                        <input type="text" class="form-control input-sm" name="phone" grid="true" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="address">Address</label>
                                        <input type="text" class="form-control input-sm" name="address" grid="true" >
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                                <button type="button" class="btn btn-sm btn-primary" id="Save">Save</button>
                                <button type="button" class="btn btn-sm btn-danger" id="Cancel">Cancel</button>
                            </div>
                            </button>
                        </div>
                        
                        <div id="jqxGrid"></div>
                    </div>
                    <!-- /.tab-pane -->
                    
                    <div class="tab-pane" id="tab_3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Textarea</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>

    {{--  <div id="popupWindow" style="display:none">
        <div>Edit</div>
        <div style="overflow: hidden;">
            <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="name">Customer Name</label>
                    <input type="text" class="form-control input-sm" name="name">
                </div>
                <div class="form-group col-md-4">
                    <label for="phone">phone</label>
                    <input type="text" class="form-control input-sm" name="phone">
                </div>
                <div class="form-group col-md-4">
                    <label for="address">Address</label>
                    <input type="text" class="form-control input-sm" name="address">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4"><input style="margin-right: 5px;" type="button" id="Save" value="Save" /><input id="Cancel" type="button" value="Cancel" /></div>
                <div class="col-md-4"></div>
            </div>
            <input type="hidden" class="form-control input-sm" name="id">
            </form>
        </div>
        
    </div>  --}}



@endsection

@include('backpack::template.lookup')


@section('after_scripts')

<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customer') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
    @endif

    
    
    @if(isset($id))
        mainId = "{{$id}}";
        editData = '{{!! $entry !!}}';
        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        editObj = JSON.parse(editData);
        cust_no= editObj.cust_no;
        console.log(editObj);
    @endif

  
   

    $(function(){

        $( "#lookupEvent" ).on( "custContactFunc", function(event,indexs) {
            /*$.each(indexs,function(k,v){
                alert(k+v);
            });*/
            alert(1);
        });
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customer') }}/";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/customers') }}";
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customer') }}";
        
        formOpt.initFieldCustomFunc = function (){
            
        };


        

        var btnGroup = [
        {
            btnId:"btnAdd",
            btnIcon:"fa fa-edit",
            btnText:"AddBtn",
            btnFunc:function(){
                location.href= gridOpt.createUrl;
            }
        }, 
        {
            btnId:"btnDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"Delete",
            btnFunc:function(){
                alert("Delete");
            }
        }, 
        {
            btnId:"btnProducts",
            btnIcon:"fa fa-barcode",
            btnText:"Products",
            btnFunc:function(){
                alert("Products");
            }
        }
        ];
        
        initBtn(btnGroup);

        $.get( "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sample_detail') }}", function( fieldData ) {
            console.log(fieldData);
            var opt = {};
            opt.gridId = "jqxGrid";
            opt.fieldData = fieldData;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/sample_detail') }}" + '/' + cust_no;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sample_detail') }}" + "/store";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sample_detail') }}" + "/update";
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sample_detail') }}" + "/delete/";
            opt.defaultKey = {'cust_no': cust_no};
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                
            }

            genDetailGrid(opt);
        });
    })
</script>    

@endsection
