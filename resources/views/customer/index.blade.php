@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        Customers<small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/log') }}">{{ trans('backpack::logmanager.log_manager') }}</a></li>
        <li class="active">{{ trans('backpack::logmanager.existing_logs') }}</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.enabledStatus = false;
gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/customers') }}";
gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/customers') }}";
gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customer/create') }}";
gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customer') }}" + "/{id}/edit";
gridOpt.height = 800;

var btnGroup = [
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "{{ trans('common.gridOption') }}",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText: "{{trans('backpack::customer.do_add')}}" ,
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"Delete",
    btnFunc:function(){
      alert("Delete");
    }
  }, 
  {
    btnId:"btnProducts",
    btnIcon:"fa fa-barcode",
    btnText:"Products",
    btnFunc:function(){
      alert("Products");
    }
  }
];
</script>
@endsection

@include('backpack::template.search')
