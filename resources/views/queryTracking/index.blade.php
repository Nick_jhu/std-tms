<!doctype html>

<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>貨物追蹤-UTMS</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue_grey-blue.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/kybarg/mdl-selectfield/mdl-menu-implementation/mdl-selectfield.min.css">
    <link rel="stylesheet" href="{{url('css/materialize-stepper.min.css')}}">
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script defer src="https://cdn.rawgit.com/kybarg/mdl-selectfield/mdl-menu-implementation/mdl-selectfield.min.js"></script>
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!-- Materializecss compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <!-- jQueryValidation Plugin -->
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>
    <!--Import Materialize-Stepper JavaScript (after the jquery.validate.js and materialize.js) -->
	
    <script src="{{url('js/materialize-stepper.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert2@7.1.2/dist/sweetalert2.all.js"></script>
<script src="https://rawgit.com/GoogleChrome/dialog-polyfill/0.4.5/dialog-polyfill.js"></script>
<link rel="stylesheet" href="https://rawgit.com/GoogleChrome/dialog-polyfill/0.4.5/dialog-polyfill.css">
    <style>
        #view-source {
            position: fixed;
            display: block;
            right: 0;
            bottom: 0;
            margin-right: 40px;
            margin-bottom: 40px;
            z-index: 900;
        }

        .demo-ribbon {
            width: 100%;
            height: 80vh;
            background-color: #3F51B5;
            flex-shrink: 0;
            background: url('https://utms.standard-info.com/trackingview.jpg?auto=format&fit=crop&w=1435&q=80') center / cover;
        }

        .demo-main {
            /* margin-top: -70vh; */
            flex-shrink: 0;
            background: url('https://utms.standard-info.com/trackingview.jpg?auto=format&fit=crop&w=1435&q=80') center / cover;
        }

        .demo-header .mdl-layout__header-row {
            padding-left: 40px;
        }

        .demo-container {
            max-width: 1600px;
            width: calc(100% - 16px);
            padding-top: 15vh;
            /* margin: 25vh auto; */
        }

        .demo-content {
            border-radius: 2px;
            padding: 10px 56px;
            margin-bottom: 80px;
            background-color: hsla(0, 0%, 100%, .8) !important;
            padding-bottom: 30px;
            position: relative;
        }

        .demo-layout.is-small-screen .demo-content {
            padding: 10px 28px;
        }

        .demo-content h3 {
            font-weight: 1000;
        }

        .demo-footer {
            padding-left: 40px;
        }

        .demo-footer .mdl-mini-footer--link-list a {
            font-size: 13px;
        }

        .demo-container label {
            color: rgba(0, 0, 0, 0.66);
        }

        .mdl-textfield {
            width: 100%;
        }

        #backBtn,
        #searchBtn {
            position: absolute;
            right: 20px;
            bottom: -25px;
        }

        .demo-content textarea {
            resize: none;
        }

        ul.stepper.horizontal {
            min-height: 200px;
        }

        @media only screen and (min-width: 993px) {
            ul.stepper.horizontal .step-content {
                padding: 20px 20px;
            }

            ul.stepper.horizontal .step-title {
                min-width: 160px;
            }
            .mdl-dialog {
                width: 50%;
            }
        }

        @media only screen and (orientation: portrait) {
            .mdl-dialog {
                width: 90%;
            }
            .demo-header .mdl-layout__header-row {
                padding-left: 0px;
            }
        }

        .mdl-dialog table {
            object-fit: cover;
        }
    </style>
</head>

<body>
    <div class="demo-layout mdl-layout mdl-layout--fixed-header mdl-js-layout mdl-color--grey-100">
        <header class="demo-header mdl-layout__header mdl-layout__header--scroll mdl-color--grey-100 mdl-color-text--grey-800" style="position:fixed">
            <div class="mdl-layout__header-row">
                <img src="https://utms.standard-info.com/queckingitle.png" height="64px">
                {{-- <div class="mdl-layout-spacer"></div> --}}
            </div>
        </header>
        <!-- <div class="demo-ribbon"></div> -->
        <main class="demo-main mdl-layout__content">
            <div class="demo-container mdl-grid">
                <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
                <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col" id="searchArea">
                    <h3>{{ trans('tracking.tracking') }}<small style="font-size:14px;color:red;padding-left:5px;">(※官網&特力訂單請直接輸入訂單編號，momo訂單則輸入前14碼，多張訂單請用半形逗號隔開)</small></h3>
                    <div class="mdl-textfield mdl-js-textfield">
                        <textarea class="mdl-textfield__input" type="text" rows="5" cols="30" id="ordNoArea"></textarea>
                        <label class="mdl-textfield__label" for="sample5">{{ trans('tracking.pleaseInputOrdno') }}...</label>
                    </div>
                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-js-ripple-effect" id="searchBtn">
                        <i class="material-icons">search</i>
                    </button>
                </div>
                <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col" id="resultArea"
                    style="display:none">
                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-js-ripple-effect" id="backBtn">
                        <i class="material-icons">keyboard_backspace</i>
                    </button>
                    <h3>{{ trans('tracking.result') }}</h3>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <select class="mdl-textfield__input" id="selOrdNo" name="selOrdNo">
                            <option value="1">1</option>
                        </select>
                        <label class="mdl-textfield__label" for="selOrdNo">{{ trans('tracking.chooseOrder') }}</label>
                    </div>
                    <!-- <div class="mdl-stepper-horizontal-alternative">
                        <div class="mdl-stepper-step active-step step-done">
                            <div class="mdl-stepper-circle">
                                <span>1</span>
                            </div>
                            <div class="mdl-stepper-title">訂單處理中</div>
                            <div class="mdl-stepper-bar-left"></div>
                            <div class="mdl-stepper-bar-right"></div>
                        </div>
                        <div class="mdl-stepper-step active-step step-done">
                            <div class="mdl-stepper-circle">
                                <span>2</span>
                            </div>
                            <div class="mdl-stepper-title">待出貨</div>
                            <div class="mdl-stepper-optional">Optional</div>
                            <div class="mdl-stepper-bar-left"></div>
                            <div class="mdl-stepper-bar-right"></div>
                        </div>
                        <div class="mdl-stepper-step active-step step-done">
                            <div class="mdl-stepper-circle">
                                <span>3</span>
                            </div>
                            <div class="mdl-stepper-title">運送中</div>
                            <div class="mdl-stepper-optional">Optional</div>
                            <div class="mdl-stepper-bar-left"></div>
                            <div class="mdl-stepper-bar-right"></div>
                        </div>
                        <div class="mdl-stepper-step">
                            <div class="mdl-stepper-circle">
                                <span>4</span>
                            </div>
                            <div class="mdl-stepper-title">到達指定地</div>
                            <div class="mdl-stepper-optional">Optional</div>
                            <div class="mdl-stepper-bar-left"></div>
                            <div class="mdl-stepper-bar-right"></div>
                        </div>
                    </div> -->
                    <ul class="stepper horizontal" id="trackingArea">

                    </ul>
                    <p style="text-align:center">
                        <button id="show-dialog" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" sysOrdNo="">查看明細</button>
                    </p>

                </div>
            </div>
        </main>
        {{-- <p style="text-align:center">Copyright © 2017
            <a href="https://standard-info.com" target="_blank">使丹達資訊股份有限公司</a> 保留一切權利。</p> --}}
    </div>

    <dialog class="mdl-dialog" id="detailModal">
        <h4 class="mdl-dialog__title">訂單明細</h4>
        <div class="mdl-dialog__content">
            <table class="mdl-data-table mdl-js-data-table  mdl-shadow--2dp">
                <thead>
                    <tr>
                        <th>商品名稱</th>
                        <th>數量</th>
                        <th>重量</th>
                        <th>材積</th>
                    </tr>
                </thead>
                <tbody id="detailBody">
                    
                </tbody>
            </table>
        </div>
        <div class="mdl-dialog__actions">
            <button type="button" class="mdl-button close">關閉</button>
        </div>
    </dialog>
</body>
<script>
    $(function () {
        var _token = "{{$c}}";
        $("#searchBtn").click(function () {
            var opt = "";
            var ordNos = $("#ordNoArea").val();
            var ordNo = [];

            //ordNos = ordNos.trim();

            ordNo = ordNos.split(",");
            if (ordNos == "" || ordNo.length == 0) {
                swal(
                    '{{ trans('tracking.prompt') }}',
                    '{{ trans('tracking.pleaseInputOrdno') }}',
                    'warning'
                );
                return;
            }

            $("#searchArea").removeClass("animated fadeInLeft");
            $("#searchArea").addClass("animated fadeOutLeft").hide();

            $("#resultArea").removeClass("animated fadeOutRight");
            $("#resultArea").addClass("animated fadeInRight").show();

            var opt = "";
            for (i in ordNo) {
                if (i == 0) {
                    opt += "<option value='" + ordNo[i] + "' selected>" + ordNo[i] + "</option>";
                } else {
                    opt += "<option value='" + ordNo[i] + "'>" + ordNo[i] + "</option>";
                }

            }

            $("#selOrdNo").html(opt);
            $(".mdl-selectfield").click();
            getTracking(ordNo[0]);
        });

        $("#backBtn").click(function () {
            $("#searchArea").removeClass("animated fadeOutLeft");
            $("#searchArea").addClass("animated fadeInLeft").show();

            $("#resultArea").removeClass("animated fadeInRight");
            $("#resultArea").addClass("animated fadeOutRight").hide();
        });

        var dialog = document.querySelector('dialog');
       //  var showDialogButton = document.querySelector('#show-dialog');
        if (!dialog.showModal) {
            dialogPolyfill.registerDialog(dialog);
        }
        $("#show-dialog").click(function () {
            var sysOrdNo = $(this).attr("sysOrdNo");
            $.get('{{url("/")}}/api/out/ordDetail/get/'+sysOrdNo+'/D', {}, function(data){
                if(data.msg == "success") {
                    var str = "";
                    for(i in data.detail) {
                        var gw = parseFloat(((data.detail[i].gw != null) ? data.detail[i].gw : 0)).toFixed(2);
                        var cbm = parseFloat(((data.detail[i].cbm != null) ? data.detail[i].cbm : 0)).toFixed(2);
                        str += "<tr>\
                                <td>"+((data.detail[i].goods_nm != null)?data.detail[i].goods_nm:'')+"</td>\
                                <td>"+data.detail[i].pkg_num+"</td>\
                                <td>"+gw+"</td>\
                                <td>"+cbm+"</td>\
                            </tr>";
                    }

                    $("#detailBody").html(str);
                }
            });
            dialog.showModal();
            
        });
        dialog.querySelector('.close').addEventListener('click', function () {
            dialog.close();
        });

        $("#selOrdNo").on("change", function () {
            var ordNo = $(this).val();
            getTracking(ordNo);
        });

        $(window).resize(function () {
            var winWidth = $(window).width();

            if (winWidth < 1533) {
                $("#trackingArea").removeClass("horizontal");
            } else {
                if ($("#trackingArea").hasClass("horizontal") == false) {
                    $("#trackingArea").addClass("horizontal");
                }
            }
        });


        function getTracking(ordNo) {
            var winWidth = $(window).width();

            if (winWidth < 1533) {
                $("#trackingArea").removeClass("horizontal");
            } else {
                if ($("#trackingArea").hasClass("horizontal") == false) {
                    $("#trackingArea").addClass("horizontal");
                }
            }
            $("#trackingArea").html('');
            $.getJSON("{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/tracking/get') }}" + "/" + ordNo + "?c=" + _token,
                function (data) {
                    if (data.msg == "success") {
                        //$('.stepper').activateStepper();
                        var trackingData = data.trackingData;
                        var str = '';
                        var refNo4 = '';
                        if (trackingData.length != 　0) {
                            for (i in trackingData) {
                                var extitle = "";
                                if(trackingData[i].title=="到達指定地點"&&trackingData[i].ordstatus=="REJECT"){
                                    extitle="<span style='color:red;'>(拒收)</span>";
                                }
                                var $li = $('<li class="step ' + trackingData[i].status +
                                    '">\
                                        <div data-step-label="' +
                                    ((trackingData[i].date != null) ? trackingData[i].date : '') +
                                    '" class="step-title waves-effect">' + trackingData[i].title +extitle+
                                    '</div>\
                                        <div class="step-content">\
                                            <p>' +
                                    trackingData[i].descp +
                                    '</p>\
                                        </div>\
                                    </li>'
                                );
                                $li.click(function () {
                                    var $thisLi = $(this);
                                    var hasDone = $(this).hasClass('done');
                                    setTimeout(function () {
                                        $(".step-content").hide();
                                        if (hasDone == true) {
                                            $thisLi.removeClass('active');
                                            $thisLi.addClass('done');
                                            $thisLi.find(".step-content").show();
                                        } else {
                                            $thisLi.removeClass('active');
                                        }
                                    }, 10);
                                });
                                $("#trackingArea").append($li);
                                $("#show-dialog").attr('sysOrdNo', data.sysOrdNo);
                                swal.close();
                            }


                        } else {
                            swal(
                                '提示',
                                '查無您的單號',
                                'warning'
                            );
                        }


                    }

                }, "JSON");
        }
    });
</script>

</html>
