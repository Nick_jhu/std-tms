@extends('backpack::layout')
@section('header')
<section class="content-header">
    <h1>
        {{ trans('modOrder.titleName') }}new
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">{{ trans('modOrder.titleName') }}new</li>
    </ol>
</section>
<div id="searchWindow" style="overflow-x:hidden; display:none">
    <div>搜尋</div>
    <div id="searchBody">
        <div id="searchContent">

        </div>
        <div class="row" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;" id="searchFooter">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="請輸入名稱" name="searchName">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch">儲存</button>
                <button class="btn btn-sm btn-warning" name="setDefault">預設</button>
                <button class="btn btn-sm btn-danger" name="delSearch">刪除</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn">搜尋</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd">新增</button>
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('before_scripts')

<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js?v={{Config::get('app.version')}}"></script>
<script>
    var gridOpt = {};
    gridOpt.pageId          = "modOrder";
    gridOpt.enabledStatus   = true;
    gridOpt.fieldsUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_total_view') }}";
    gridOpt.dataUrl         = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_total_view') }}";
    gridOpt.fieldsUrl       = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmttest/create') }}";
    gridOpt.editUrl         = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmttest') }}" + "/{id}/edit";
    gridOpt.height          = 800;
    gridOpt.selectionmode   = "checkbox";
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick  = false;
    gridOpt.searchOpt       = true;

    var btnGroup = [
        {
            btnId: "btnSearchWindow",
            btnIcon: "fa fa-search",
            btnText: "{{ trans('common.search') }}",
            btnFunc: function () {
                $('#searchWindow').jqxWindow('open');
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
            //$("#jqxGrid").jqxGrid('exportdata', 'xls', '{{ trans("modOrder.titleName") }}');
            $("#jqxGrid").jqxGrid('exportdata', 'json', '訂單作業', true, null, false, BASE_API_URL+'/admin/export/data');
            }
        },
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
    ];

    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });
    // var loadURL = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/getLayoutJson') }}";					
    //     $.ajax({
    //         type: "GET", //  OR POST WHATEVER...
    //         url: loadURL,
    //         data: { key: 'excelOrderGrid' },		 
    //         success: function(response) {										
    //             if (response != "") {	
    //                 response = JSON.parse(response);
    //             }
    //             var listSource = [];
    //             state = $("#jqxGrid").jqxGrid('getstate');
    //             $.each(state.columns, function(i, item) {
    //                 if(item.text != "" && item.text != "undefined"){
    //                     listSource.push({ 
    //                     label: item.text, 
    //                     value: i, 
    //                     checked: !item.hidden });
    //                 }
    //             });
    //             $("#jqxlistbox").jqxListBox({ 
    //                 allowDrop: true, 
    //                 allowDrag: true,
    //                 source: listSource,
    //                 width: "99%",
    //                 height: 500,
    //                 checkboxes: true,
    //                 filterable: true,
    //                 searchMode: 'contains'
    //             });
    //         }
    //     });	
</script>
@endsection

@section('content')
<div id="jqxLoader">
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="button-group">
                    <div class="row" id="btnArea"></div>
                    <div id="jqxGrid"></div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<div class="row">
	<div class="col-md-2">
		{{--
		<div class="box box-primary" id="statusDiv">
			<div class="box-header with-border">
				<h3 class="box-title">Status</h3>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked" id="statusList">
				</ul>
			</div>
			<!-- /.box-body -->
		</div> --}} {{--
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Grid Option</h3>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
						<button type="button" class="btn btn-box-tool" id="saveGrid">
							<i class="fa fa-floppy-o" aria-hidden="true"></i>
						</button>
				</div>
			</div>
			<div class="box-body no-padding">
				<div id="jqxlistbox"></div>
			</div>
			<!-- /.box-body -->
		</div> --}}
		<!-- /. box -->
	</div>
	<div class="col-md-12">
		<div class="box box-primary">
			<!-- /.box-header -->
			<div class="box-body">
				<div class="button-group">
					<div class="row" id="btnArea">

					</div>
					<div id="jqxGrid"></div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="skeyModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">站別</h4>
			</div>
			<div class="modal-body" id="skeyBody">
				<div id="skeyGrid"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-danger" id="chStation">確定修改</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
@endsection

@section('after_scripts')
<script>
    var theme = 'bootstrap';
    $.jqx.theme = theme;
    gridOpt.gridId = "jqxGrid";
    loadState();
    gridOpt.getState = function(){
        loadState();
    };
    var fieldData = null;
    var fieldObj = null;
    var source = {
        datatype: "json",
        datafields: [
            {
                "name": "id",
                "type": "number"
            },
            {
                "name": "g_key",
                "type": "string"
            },
            {
                "name": "c_key",
                "type": "string"
            },
            {
                "name": "s_key",
                "type": "string"
            },
            {
                "name": "d_key",
                "type": "string"
            },
            {
                "name": "ord_no",
                "type": "string"
            },
            {
                "name": "status",
                "type": "string"
            },
            {
                "name": "status_desc",
                "type": "string"
            },
            {
                "name": "remark",
                "type": "string"
            },
            {
                "name": "dlv_cust_nm",
                "type": "string"
            },
            {
                "name": "dlv_addr",
                "type": "string"
            },
            {
                "name": "dlv_attn",
                "type": "string"
            },
            {
                "name": "dlv_tel",
                "type": "string"
            },
            {
                "name": "dlv_city_nm",
                "type": "string"
            },
            {
                "name": "dlv_zip",
                "type": "string"
            },

            {
                "name": "dlv_area_nm",
                "type": "string"
            },
            {
                "name": "pkg_num",
                "type": "number"
            },
            {
                "name": "pkg_unit",
                "type": "string"
            },
            {
                "name": "etd",
                "type": "range"
            },
            {
                "name": "truck_cmp_nm",
                "type": "string"
            },
            {
                "name": "driver",
                "type": "string"
            },
            {
                "name": "exp_reason",
                "type": "string"
            },
            {
                "name": "error_remark",
                "type": "string"
            },
            {
                "name": "updated_by",
                "type": "string"
            },
            {
                "name": "created_by",
                "type": "string"
            },
            {
                "name": "created_at",
                "type": "range"
            },
            {
                "name": "updated_at",
                "type": "range"
            },
            {
                "name": "confirm_dt",
                "type": "range"
            },
            {
                "name": "close_date",
                "type": "range"
            },
            {
                "name": "dlv_no",
                "type": "string"
            },
            {
                "name": "close_by",
                "type": "string"
            },
            {
                "name": "close_status",
                "type": "string"
            },
            {
                "name": "car_no",
                "type": "string"
            },
            {
                "name": "trs_mode",
                "type": "string"
            },
            {
                "name": "trs_mode_desc",
                "type": "string"
            },
            {
                "name": "pick_cust_nm",
                "type": "string"
            },
            {
                "name": "pick_addr",
                "type": "string"
            },
            {
                "name": "pick_attn",
                "type": "string"
            },
            {
                "name": "pick_tel",
                "type": "string"
            },
            {
                "name": "pick_city_nm",
                "type": "string"
            },
            {
                "name": "pick_zip",
                "type": "string"
            },
            {
                "name": "pick_area_nm",
                "type": "string"
            },
            {
                "name": "total_cbm",
                "type": "number"
            },
            {
                "name": "dlv_email",
                "type": "string"
            },
            {
                "name": "pick_email",
                "type": "string"
            },
            {
                "name": "distance",
                "type": "number"
            },
            {
                "name": "car_type",
                "type": "string"
            },
            {
                "name": "car_type_desc",
                "type": "string"
            },
            {
                "name": "amt",
                "type": "number"
            },
            {
                "name": "total_gw",
                "type": "number"
            },
            {
                "name": "cust_amt",
                "type": "number"
            },
            {
                "name": "amt_remark",
                "type": "string"
            },
            {
                "name": "owner_nm",
                "type": "string"
            },
            {
                "name": "sys_ord_no",
                "type": "string"
            },
            {
                "name": "dlv_remark",
                "type": "string"
            },
            {
                "name": "pick_remark",
                "type": "string"
            },
            {
                "name": "finish_date",
                "type": "range"
            },
            {
                "name": "cust_ord_no",
                "type": "string"
            },
            {
                "name": "wms_order_no",
                "type": "string"
            },
            {
                "name": "is_urgent",
                "type": "string"
            },
            {
                "name": "is_confirm",
                "type": "string"
            },
            {
                "name": "confirm_by",
                "type": "string"
            },
            {
                "name": "collectamt",
                "type": "number"
            },
            {
                "name": "total_amount",
                "type": "number"
            },
            {
                "name": "temperate",
                "type": "string"
            },
            {
                "name": "dlv_addr_info",
                "type": "string"
            },
            {
                "name": "pick_addr_info",
                "type": "string"
            },
            {
                "name": "pick_addr_info",
                "type": "string"
            },
            {
                "name": "sign_pic",
                "type": "number"
            },
            {
                "name": "error_pic",
                "type": "number"
            },
            {
                "name": "abnormal_remark",
                "type": "string"
            },
            {
                "name": "temperate_desc",
                "type": "string"
            }
        ],
        root: "Rows",
        pagenum: 0,
        beforeprocessing: function (data) {
            source.totalrecords = data[0].TotalRows;
            if(data[0].StatusCount.length > 0) {
                data[0].StatusCount.push({'count': data[0].TotalRows, 'status': 'ALL','statustext': 'ALL'});
            }

            if($("#statusList").find('a').length == 0) {
                
                $("#statusList").html("");
                $.each(data[0].StatusCount, function(i, item) {
                    $("#statusList").append(' <a class="statusBtn btn btn-app"><span class="badge bg-purple"> '+item.count+'</span><i class="fa fa-bullhorn"></i><span class="text" id="'+item.status+'">'+item.statustext+'</span></a>');
                });
    
                $('.statusBtn').off().on('click', function(){
                    $('#statusList').find('a').removeClass('active');
                    $(this).addClass('active');
    
                    var statusGroup = new $.jqx.filter();
                    var filter_or_operator = 1;
                    var filtervalue = $(this).find('span.text').attr("id");
                    if(filtervalue == "ALL") {
                        $("#jqxGrid").jqxGrid('clearfilters');
                    }
                    else {
                        var filtercondition = 'contains';
                        var statusFilter1 = statusGroup.createfilter('stringfilter', filtervalue, filtercondition);
                        statusGroup.addfilter(filter_or_operator, statusFilter1);
                        $("#jqxGrid").jqxGrid('addfilter', 'status', statusGroup);
                        $("#jqxGrid").jqxGrid('applyfilters');
                    }
                    
                });
            }
        },
        filter: function () {
            // update the grid and send a request to the server.
            $("#jqxGrid").jqxGrid('updatebounddata', 'filter');
        },
        sort: function () {
            // update the grid and send a request to the server.
            $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
        },
        cache: false,
        pagesize: 50,
        url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_total_view/true') }}"
    }
    var dataAdapter = new $.jqx.dataAdapter(source, {
        async: false, 
        loadError: function (xhr, status, error) { 
            alert('Error loading "' + source.url + '" : ' + error); 
        },
        loadComplete: function() {
            $.get(gridOpt.getStatusCount, {}, function(res){
                res[0].StatusCount.push({'count': res[0].TotalRows, 'status': 'ALL','statustext': 'ALL'});
                genStatus(res, 'jqxGrid');
            }, 'JSON');
        }
    });
    function genStatus(data, gridId) {
    $("#statusList").html("");
    $.each(data[0].StatusCount, function(i, item) {
        $("#statusList").append(' <a class="statusBtn btn btn-app"><span class="badge bg-purple"> '+item.count+'</span><i class="fa fa-bullhorn"></i><span class="text" id="'+item.status+'">'+item.statustext+'</span></a>');
    });

    $('.statusBtn').off().on('click', function(){
        $('#statusList').find('a').removeClass('active');
        $(this).addClass('active');

        var statusGroup = new $.jqx.filter();
        var filter_or_operator = 1;
        var filtervalue = $(this).find('span.text').attr("id");
        if(filtervalue == "ALL") {
            $("#" + gridId).jqxGrid('clearfilters');
        }
        else {
            var filtercondition = 'contains';
            var statusFilter1 = statusGroup.createfilter('stringfilter', filtervalue, filtercondition);
            statusGroup.addfilter(filter_or_operator, statusFilter1);
            $("#" + gridId).jqxGrid('addfilter', 'status', statusGroup);
            $("#" + gridId).jqxGrid('applyfilters');
        }
        
    });
}
    var h = 350;

    if(gridOpt.enabledStatus == false) {
        h = 250;
    }
    var winHeigt = $( window ).height() - h;
    if(typeof gridOpt.who !== "undefined" && gridOpt.who == "lookup") {
        winHeigt = 500;
    }
    $("#jqxGrid").jqxGrid(
        {
            width: '100%',
            height: winHeigt,
            source: dataAdapter,
            sortable: true,
            filterable: true,
            altrows: true,
            showfilterrow: true,
            pageable: true,
            virtualmode: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            columnsresize: true,
            columnsautoresize: true,
            clipboard: true,
            selectionmode: 'checkbox',
            enablebrowserselection: true,
            pagesizeoptions: [50, 100, 500, 9999],
            rendergridrows: function (params) {
                //alert("rendergridrows");
                return params.data;
            },
            ready: function () {
                $("#jqxLoader").jqxLoader({ width: 100, height: 60, imagePosition: 'top' });
                $("#jqxLoader").jqxLoader('open');
                //$('#' + gridId).jqxGrid('autoresizecolumns');
                // loadState();
            },
            updatefilterconditions: function (type, defaultconditions) {
                var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                switch (type) {
                    case 'stringfilter':
                        return stringcomparisonoperators;
                    case 'numericfilter':
                        return numericcomparisonoperators;
                    case 'datefilter':
                        return datecomparisonoperators;
                    case 'booleanfilter':
                        return booleancomparisonoperators;
                }
            },
            columns: [
                {
                    "text": "{{ trans('modOrder.etd') }}",
                    "datafield": "etd",
                    "width": 100,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "yyyy-MM-dd",
                    "filtertype": "range",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.trsMode') }}",
                    "datafield": "trs_mode",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "trsMode": [
                        {
                            "label": "\u5c08\u8eca(C)",
                            "value": "C"
                        },
                        {
                            "label": "\u975e\u5c08\u8eca(N)",
                            "value": "N"
                        },
                        {
                            "label": "\u5c08\u8eca-\u9577\u7a0b(C02)",
                            "value": "C02"
                        },
                        {
                            "label": "\u5c08\u8eca-\u77ed\u7a0b(C01)",
                            "value": "C01"
                        },
                        {
                            "label": "\u975e\u5c08\u8eca-\u91cd\u91cf(N01)",
                            "value": "N01"
                        },
                        {
                            "label": "\u5c1a\u672a\u8a08\u50f9",
                            "value": "NON"
                        },
                        {
                            "label": "\u975e\u5c08\u8eca-\u7121\u6298\u6263(N02)",
                            "value": "N02"
                        }
                    ],
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.trsModedesc') }}",
                    "datafield": "trs_mode_desc",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.temperate') }}",
                    "datafield": "temperate",
                    "width": 300,
                    "dbwidth": "100",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.temperateDesc') }}",
                    "datafield": "temperate_desc",
                    "width": 300,
                    "dbwidth": "100",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.sysOrdNo') }}",
                    "datafield": "sys_ord_no",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.ordNo') }}",
                    "datafield": "ord_no",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.custOrdNo') }}",
                    "datafield": "cust_ord_no",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.wmsorderno') }}",
                    "datafield": "wms_order_no",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.status') }}",
                    "datafield": "status",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": {
                        "source": "statusGridObj",
                        "value": "val",
                        "name": "key"
                    },
                    "statusCode": [
                        {
                            "label": "\u5c1a\u672a\u5b89\u6392",
                            "value": "UNTREATED"
                        },
                        {
                            "label": "\u5df2\u6d3e\u55ae\u672a\u51fa\u767c",
                            "value": "SEND"
                        },
                        {
                            "label": "\u8ca8\u7269\u88dd\u8f09\u4e2d",
                            "value": "LOADING"
                        },
                        {
                            "label": "\u51fa\u767c",
                            "value": "SETOFF"
                        },
                        {
                            "label": "\u6b63\u5e38\u914d\u9001",
                            "value": "NORMAL"
                        },
                        {
                            "label": "\u5df2\u63d0\u8ca8",
                            "value": "PICKED"
                        },
                        {
                            "label": "\u914d\u9001\u767c\u751f\u554f\u984c",
                            "value": "ERROR"
                        },
                        {
                            "label": "\u914d\u9001\u5ef6\u9072",
                            "value": "DELAY"
                        },
                        {
                            "label": "\u914d\u9001\u5b8c\u6210",
                            "value": "FINISHED"
                        },
                        {
                            "label": "\u4f5c\u5ee2",
                            "value": "FAIL"
                        },
                        {
                            "label": "\u95dc\u9589",
                            "value": "CLOSE"
                        },
                        {
                            "label": "modOrder.STATUS_REJECT",
                            "value": "REJECT"
                        }
                    ],
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.statusDesc') }}",
                    "datafield": "status_desc",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.isUrgent') }}",
                    "datafield": "is_urgent",
                    "width": 100,
                    "dbwidth": "1",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.ownerNm') }}",
                    "datafield": "owner_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.remark') }}",
                    "datafield": "remark",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickCustNm') }}",
                    "datafield": "pick_cust_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickZip') }}",
                    "datafield": "pick_zip",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickCityNm') }}",
                    "datafield": "pick_city_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickAreaNm') }}",
                    "datafield": "pick_area_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickAddr') }}",
                    "datafield": "pick_addr",
                    "width": 600,
                    "dbwidth": "200",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickAddrInfo') }}",
                    "datafield": "pick_addr_info",
                    "width": 600,
                    "dbwidth": "200",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickAttn') }}",
                    "datafield": "pick_attn",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text":  "{{ trans('modOrder.pickRemark') }}",
                    "datafield": "pick_remark",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickTel') }}",
                    "datafield": "pick_tel",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickEmail') }}",
                    "datafield": "pick_email",
                    "width": 300,
                    "dbwidth": "100",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvCustNm') }}",
                    "datafield": "dlv_cust_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvZip') }}",
                    "datafield": "dlv_zip",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvCityNm') }}",
                    "datafield": "dlv_city_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvAreaNm') }}",
                    "datafield": "dlv_area_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvAddr') }}",
                    "datafield": "dlv_addr",
                    "width": 600,
                    "dbwidth": "200",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvAddrInfo') }}",
                    "datafield": "dlv_addr_info",
                    "width": 600,
                    "dbwidth": "200",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvRemark') }}",
                    "datafield": "dlv_remark",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvAttn') }}",
                    "datafield": "dlv_attn",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvTel') }}",
                    "datafield": "dlv_tel",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvEmail') }}",
                    "datafield": "dlv_email",
                    "width": 300,
                    "dbwidth": "100",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pkgNum') }}",
                    "datafield": "pkg_num",
                    "width": 100,
                    "dbwidth": "11",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pkgUnit') }}",
                    "datafield": "pkg_unit",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.totalCbm') }}",
                    "datafield": "total_cbm",
                    "width": 309,
                    "dbwidth": "103",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.totalGw') }}",
                    "datafield": "total_gw",
                    "width": 309,
                    "dbwidth": "103",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.driver') }}",
                    "datafield": "driver",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.carNo') }}",
                    "datafield": "car_no",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.distance') }}",
                    "datafield": "distance",
                    "width": 366,
                    "dbwidth": "122",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.collectamt') }}",
                    "datafield": "collectamt",
                    "width": 100,
                    "dbwidth": "182",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.total_amount') }}",
                    "datafield": "total_amount",
                    "width": 100,
                    "dbwidth": "182",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.amt') }}",
                    "datafield": "amt",
                    "width": 546,
                    "dbwidth": "182",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.custAmt') }}",
                    "datafield": "cust_amt",
                    "width": 546,
                    "dbwidth": "182",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.amtRemark') }}",
                    "datafield": "amt_remark",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvNo') }}",
                    "datafield": "dlv_no",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.expReason') }}",
                    "datafield": "error_remark",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.abnormalRemark') }}",
                    "datafield": "abnormal_remark",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.finishDate') }}",
                    "datafield": "finish_date",
                    "width": 100,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "yyyy-MM-dd HH:mm:ss",
                    "filtertype": "range",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.signPic') }}",
                    "datafield": "sign_pic",
                    "width": 546,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999,
                    "cellsrenderer":function (row, columnfield, value, defaulthtml, columnproperties) {
                    var sysOrdNo = dataAdapter.records[row].sys_ord_no;
                    var signPic = dataAdapter.records[row].sign_pic;
                    var btn = "";

                    if(signPic > 0) {
                        btn = `<button class="btn btn-link" onclick="openSignPic('{sysOrdNo}')">簽收照片連結</button>`;
                        btn = btn.replace("{sysOrdNo}", sysOrdNo);
                    }
                    
                    return btn;
                }
                },
                {
                    "text": "{{ trans('modOrder.errorPic') }}",
                    "datafield": "error_pic",
                    "width": 546,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999,
                    "cellsrenderer":function (row, columnfield, value, defaulthtml, columnproperties) {
                    var sysOrdNo = dataAdapter.records[row].sys_ord_no;
                    var errorPic = dataAdapter.records[row].error_pic;
                    var btn = "";

                    if(errorPic > 0) {
                        btn = `<button class="btn btn-link" onclick="openErrorPic('{sysOrdNo}')">異常照片連結</button>`;
                        btn = btn.replace("{sysOrdNo}", sysOrdNo);
                    }
                    
                    return btn;
                }
                },
                {
                    "text": "{{ trans('modOrder.isconfirm') }}",
                    "datafield": "is_confirm",
                    "width": 100,
                    "dbwidth": "1",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.confirmby') }}",
                    "datafield": "confirm_by",
                    "width": 100,
                    "dbwidth": "1",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.confirm_dt') }}",
                    "datafield": "confirm_dt",
                    "width": 100,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "yyyy-MM-dd HH:mm:ss",
                    "filtertype": "range",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },

                {
                    "text": "{{ trans('modOrder.close_status') }}",
                    "datafield": "close_status",
                    "width": 100,
                    "dbwidth": "1",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.close_by') }}",
                    "datafield": "close_by",
                    "width": 100,
                    "dbwidth": "1",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.close_date') }}",
                    "datafield": "close_date",
                    "width": 100,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "yyyy-MM-dd HH:mm:ss",
                    "filtertype": "range",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },

                {
                    "text": "{{ trans('modOrder.createdBy') }}",
                    "datafield": "created_by",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.createdAt') }}",
                    "datafield": "created_at",
                    "width": 100,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "yyyy-MM-dd HH:mm:ss",
                    "filtertype": "range",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.updatedBy') }}",
                    "datafield": "updated_by",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.updatedAt') }}",
                    "datafield": "updated_at",
                    "width": 100,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "yyyy-MM-dd HH:mm:ss",
                    "filtertype": "range",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },

                {
                    "text": "{{ trans('modOrder.truckCmpNm') }}",
                    "datafield": "truck_cmp_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "id",
                    "datafield": "id",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.gKey') }}",
                    "datafield": "g_key",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.cKey') }}",
                    "datafield": "c_key",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.sKey') }}",
                    "datafield": "s_key",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dKey') }}",
                    "datafield": "d_key",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
            ]
        });
    gridOpt.gridId = "jqxGrid";
    $("#jqxGrid").on('bindingcomplete', function (event) {
        $('#jqxLoader').jqxLoader('close');
        var statusHeight = 0;
        if(typeof gridOpt.enabledStatus !== "undefined") {
            if(gridOpt.enabledStatus == false) {
                statusHeight = 50;
            }
        }
        var winHeight = $( window ).height() - 350 + statusHeight;
        $("#jqxGrid").jqxGrid('height', winHeight+'px');
        //$("#"+gridId).jqxGrid("hideloadelement"); 
    });
    // gridOpt.fieldsUrl = 'http://coresys.test:8080/api/admin/baseApi/getGridJson/mod_order/true?filterscount=0&groupscount=0&pagenum=0&pagesize=50&recordstartindex=0&recordendindex=50&_=1558592094698';
    // $.get( gridOpt.fieldsUrl, function( fieldData ) {
    //     console.log(fieldData);

    // });
    function loadState() {  	
        var loadURL = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/getLayoutJson') }}"; 	  	  	
        $.ajax({
            type: "GET", //  OR POST WHATEVER...
            url: loadURL,
            data: { key: gridOpt.dataUrl },		 
            success: function(response) {										
                if (response != "") {	
                    response = JSON.parse(response);
                    $("#"+gridOpt.gridId).jqxGrid('loadstate', response);
                }
                
                var listSource = [];
        
                state = $("#"+gridOpt.gridId).jqxGrid('getstate');
                console.log(state);
                $('#jqxLoader').jqxLoader('close');
                $.get(BASE_URL+"/searchList/get/" + gridOpt.pageId, {}, function(data){
                    if(data.msg == "success") {
                        var opt = "<option value=''>請選擇</option>";
            
                        for(i in data.data) {
                            if(data.data[i]["layout_default"] == "Y") {
                                opt += "<option value='"+data.data[i]["id"]+"' selected>"+data.data[i]["title"]+"</option>";
                                $("input[name='searchName']").val(data.data[i]["title"]);
                            }
                            else {
                                opt += "<option value='"+data.data[i]["id"]+"'>"+data.data[i]["title"]+"</option>";
                            }
                        }
            
                        $("select[name='selSearchName']").html(opt);
            
                        $.get(BASE_URL+"/searchHtml/get/" + $("select[name='selSearchName']").val(), {}, function(data){
                            if(data.msg == "success") {
                                if(data.data != null) {
                                    $("#searchContent").html(data.data["data"]);
                                }
                                else {
                                    var seasrchTpl = getSearchTpl(state);
                                    initSearchWindow(seasrchTpl);
                                }

                                var offset = $(".content-wrapper").offset();
                                $('#searchWindow').jqxWindow({
                                    position: { x: offset.left + 50, y: offset.top + 50} ,
                                    showCollapseButton: true, maxHeight: 400, maxWidth: 700, minHeight: 200, minWidth: 200, height: 300, width: 700, autoOpen: false,
                                    initContent: function () {
                                        $('#searchWindow').jqxWindow('focus');
                                    }
                                });


                                $("button[name='winSearchAdd']").on("click", function(){
                                    var seasrchTpl = getSearchTpl(state);
                                    $("#searchContent").append(searchTpl);           
                                });

                                $(document).on("click", "button[name='winBtnRemove']", function(){ 
                                    $( this ).parents(".row").remove();
                                });

                                $(document).on("change", "select[name='winField[]']", function(){ 
                                    $('option:selected', this).siblings().removeAttr('selected');
                                    $('option:selected', this).attr('selected', 'selected');
                                    var val  = $(this).val();
                                    var info = searchObj(val, fieldObj[0]);
                                    var str  = [
                                        {val: 'CONTAINS', label: '包含'},
                                        {val: 'EQUAL', label: '等於'},
                                        {val: 'NOT_EQUAL', label: '不等於'},
                                        {val: 'NULL', label: 'NULL'},
                                        {val: 'NOT_NULL', label: 'NOT NULL'},
                                    ];

                                    var num  = [
                                        {val: 'EQUAL', label: '等於'},
                                        {val: 'NOT_EQUAL', label: '不等於'},
                                        {val: 'LESS_THAN', label: '小於'},
                                        {val: 'LESS_THAN_OR_EQUAL', label: '小於等於'},
                                        {val: 'GREATER_THAN', label: '大於'},
                                        {val: 'GREATER_THAN_OR_EQUAL', label: '大於等於'},
                                        {val: 'NULL', label: 'NULL'},
                                        {val: 'NOT_NULL', label: 'NOT NULL'},
                                    ];

                                    var opt = "";
                                    if(info.type == "string") {
                                        for(i in str) {
                                            opt += '<option value="'+str[i].val+'">'+str[i].label+'</option>';
                                        }
                                    }
                                    else {
                                        for(i in num) {
                                            opt += '<option value="'+num[i].val+'">'+num[i].label+'</option>';
                                        }
                                    }
                                    $($(this).parent().siblings()[0]).find("select").html(opt);
                                });

                                $(document).on("change", "select[name='winOp[]']", function(){
                                    $('option:selected', this).siblings().removeAttr('selected');
                                    $('option:selected', this).attr('selected', 'selected');
                                });

                                $(document).on("change", "input[name='winContent[]']", function(){
                                    $(this).attr('value', $(this).val());
                                });

                                $("button[name='winSearchBtn']").on("click", function(){
                                    var winField   = [];
                                    var winContent = [];
                                    var winOp      = [];
                                    $("select[name='winField[]']").each(function(){
                                        winField.push($(this).val());
                                    });
                                    $("input[name='winContent[]']").each(function(){
                                        winContent.push($(this).val());
                                    });
                                    $("select[name='winOp[]']").each(function(){
                                        winOp.push($(this).val());
                                    });
                                    
                                    addfilter(winField, winContent, winOp);
                                });
                            }
                        });
                        
                    }
                });

                $.each(state.columns, function(i, item) {
                    if(item.text != "" && item.text != "undefined"){
                        listSource.push({ 
                        label: item.text, 
                        value: i, 
                        checked: !item.hidden });
                    }
                });

                $("#jqxlistbox").jqxListBox({ 
                    allowDrop: true, 
                    allowDrag: true,
                    source: listSource,
                    width: "99%",
                    height: 500,
                    checkboxes: true,
                    filterable: true,
                    searchMode: 'contains'
                });
            }
        });			  	  	  	  	
    }
    $("#saveGrid").on("click", function(){
        var items = $("#jqxlistbox").jqxListBox('getItems');
        //$('#jqxGrid').jqxGrid('clear');
        $("#"+gridOpt.gridId).jqxGrid('beginupdate');

        $.each(items, function(i, item) {
            console.log($('#'+gridOpt.gridId).jqxGrid('getcolumnindex', item.value));
            var thisIndex = $('#'+gridOpt.gridId).jqxGrid('getcolumnindex', item.value)-1;
            if(thisIndex != item.index){
                //console.log(item.value+":"+thisIndex+"="+item.index);
                $('#'+gridOpt.gridId).jqxGrid('setcolumnindex', item.value,  item.index);
            }
            if (item.checked) {
                $("#"+gridOpt.gridId).jqxGrid('showcolumn', item.value);
            }
            else {
                $("#"+gridOpt.gridId).jqxGrid('hidecolumn', item.value);
            }
        })
        
        $("#"+gridOpt.gridId).jqxGrid('endupdate');
        state = $("#"+gridOpt.gridId).jqxGrid('getstate');
        state['filters']['filterscount'] = 0;
        var saveUrl = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/saveLayoutJson') }}"; 	  	  	
        var stateToSave = JSON.stringify(state);

        $.ajax({
            type: "POST",										
            url: saveUrl,		
            data: { data: stateToSave,key: gridOpt.dataUrl },		 
            success: function(response) {
                if(response == "true"){
                    alert("save successful");
                    $('#gridOptModal').modal('hide');
                }else{
                    alert("save failded");
                }
                
            }
        });	
    });

    $("#clearGrid").on("click", function(){
        $.ajax({
            type: "POST",										
            url: "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/clearLayout') }}",		
            data: { key: gridOpt.dataUrl },		 
            success: function(response) {
                if(response == "true"){
                    alert("clear successful");
                    location.reload();
                    $('#gridOptModal').modal('hide');
                }else{
                    //alert("save failded");
                }
                
            }
        });	
    });

    $("#chStation").on("click", function(){
        var idx = $('#skeyGrid').jqxGrid('getselectedrowindexes');
        if(idx.length ==  0) {
            swal("請選擇一筆資料", "", "warning");
            return;
        }
        console.log();
        var ids = [];
        var dlvTypes = [];
        var cust_no = "";
        for(i in idx) {
            var rowData = $('#skeyGrid').jqxGrid('getrowdata', idx[i]);
            cust_no = rowData.cust_no;
        }

        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        var id = new Array();
        for (var m = 0; m < rows.length; m++) {
            var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
            if(typeof row != "undefined") {
                id.push(row.id);
            }
        }

        $.post(BASE_URL + "/OrderMgmt/chStation", {cust_no: cust_no, ids: id}, function(data){
            $("#skeyModal").modal('hide');
            if(data.msg == "success") {
                swal("完成", "", "success");
                $('#skeyGrid').jqxGrid('updatebounddata');
                $('#jqxGrid').jqxGrid('updatebounddata');
                $("#jqxGrid").jqxGrid('clearselection');
            }
        });
    
    });
    $("select[name='selSearchName']").on("change", function(){
        var text = $('option:selected', this).text();
        if($(this).val() == '') {
            text = "";
        }
        $("input[name='searchName']").val(text);

        $.get(BASE_URL+"/searchHtml/get/" + $(this).val(), {}, function(data){
            //console.log(data.data["data"]);
            $("#searchContent").html(data.data["data"]);
        });
    })
    function initSearchWindow(searchTpl) {
        $("#searchContent").append(searchTpl);
    }
    function getSearchTpl(state) {
        var fields = [];
        $.each(state.columns, function(i, item) {
            if(item.hidden == false && item.text != "") {
                var a = {
                    label: item.text,
                    value: i
                }
                fields.push(a);
            }
        });

        var fieldStr = "";
        for(i in fields) {
            fieldStr += '<option value="'+fields[i].value+'">'+fields[i].label+'</option>';
        }

        searchTpl = '<div class="row">\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winField[]">'+fieldStr+'</select>\
                            </div>\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winOp[]">\
                                <option value="CONTAINS">包含</option>\
                                <option value="EQUAL">等於</option>\
                                <option value="NULL">NULL</option>\
                                <option value="NOT_NULL">NOT NULL</option>\
                                </select>\
                            </div>\
                            <div class="col-xs-4">\
                                <input type="text" class="form-control input-sm" name="winContent[]">\
                            </div>\
                            <div class="col-xs-2">\
                                <button class="btn btn-sm btn-info btn-danger" name="winBtnRemove">-</button>\
                            </div>\
                        </div>';
        return searchTpl;
    }

    var addfilter = function (datafield, filterval, filtercondition) {
        $("#jqxGrid").jqxGrid('clearfilters');
        
        for(i in datafield) {
            var filtergroup = new $.jqx.filter();
            var filter_or_operator = 1;
            //var filtervalue = filterval;
            var filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
            filtergroup.addfilter(filter_or_operator, filter);

            // add the filters.
            $("#jqxGrid").jqxGrid('addfilter', datafield[i], filtergroup);
        }
        // apply the filters.
        $("#jqxGrid").jqxGrid('applyfilters');
    }

    

    function searchObj(nameKey, myArray){
        for (var i=0; i < myArray.length; i++) {
            if (myArray[i].name === nameKey) {
                return myArray[i];
            }
        }
    }

    $('#searchWindow').on('open', function (event) { 
        if($("body").width() < 400) {
            var offset = $("body").offset();
            $('#searchWindow').jqxWindow({ height: 500, width: $("body").width(), position: { x: 0, y: offset.top + 150} , });
        }
        else {

        }
        
    });

    $("button[name='saveSearch']").on("click", function(){
        var htmlData = $("#searchContent").html();
        var postData = {
            "key" : gridOpt.pageId,
            "data": htmlData,
            "title": $("input[name='searchName']").val(),
            "id": ($("select[name='selSearchName'] option:selected").text() == $("input[name='searchName']").val())?$("select[name='selSearchName']").val():null
        };
        $.ajax({
            url: BASE_URL + "/saveSearchLayout",
            data: postData,
            dataType: "JSON",
            method: "POST",
            success: function(result){
                if(result.msg == "success") {
                    swal("儲存成功", "", "success");
                    $("select[name='selSearchName'] option").removeAttr("selected");
                    $("select[name='selSearchName']").append("<option value='"+result.id+"' selected>"+$("input[name='searchName']").val()+"</option>")
                }
                else {
                    swal("操作失敗", "", "error");
                }
            },
            error: function() {
                swal("請輸入查詢條件名稱", "", "error");
            }
        });
    });
</script>
<script>
Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};
</script>
@endsection