<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>
        Standard Global System
    </title>

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/fonts/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="assets/login/css/main.css">

    <script type="text/javascript" src="https://api.map.baidu.com/api?v=3.0&ak=0lPULNZ5PmrFVg76kFuRjezF"></script>
    <script type="text/javascript" src="https://unpkg.com/inmap/dist/inmap.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/inmap@2.0.2/dist/inmap.min.js"></script>
    <!--===============================================================================================-->

    <style>
        #overlay {
            position: absolute; /* Sit on top of the page content */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0; 
            left: 0;
            background-color: rgba(0,0,0,0.3); /* Black background with opacity */
            z-index: 99; /* Specify a stack order in case you're using a different order for other elements */
        }
    </style>
</head>

<body style="overflow: hidden">
    <form action="" id="Login_Form">
        <div class="limiter">
            <div class="container-login100">
                <div class="login100-more">
                    <div id="overlay"></div>
                    <div id="allmap" style="width:100%; height: 1200px"> </div>
                </div>

                <div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">

                    <span class="login100-form-title p-b-59" style="text-align:center">
                        <img src="https://www.standard-info.com/website/wp-content/uploads/2017/06/Standard_logo_20161207-v2%E5%8E%BB%E8%83%8C.png" width="200" />
                    </span>

                    <div class="form-group">
                        <label for="exampleInputEmail1" style="color: #fff">Sign in</label>
                        <input type="text" class="form-control" placeholder="account">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="password">
                    </div>

                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn" style="margin: 0 auto;">
                            <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn">
                                Log in
                            </button>
                        </div>
                    </div>

                    <p style="text-align:center; color: #fff; font-size: 12px;">
                        <br />
                        忘記密碼，請聯絡資訊課。<br />

                        Version 2019.05.16-001
                    </p>
                </div>
            </div>
        </div>
    </form>

    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="assets/js/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->

    <script>
        let data = [
            { "from": { "name": "Taipei", "coordinates": [121.469106, 25.101793] }, "to": { "name": "Xiamen", "coordinates": [118.085173, 24.480737] }, "count": 1 },
            { "from": { "name": "Taipei", "coordinates": [121.469106, 25.101793] }, "to": { "name": "Qingdao", "coordinates": [120.379122, 36.064902] }, "count": 2 },
            { "from": { "name": "Taipei", "coordinates": [121.469106, 25.101793] }, "to": { "name": "Hong Kong", "coordinates": [114.087411, 22.413796] }, "count": 3 },
            { "from": { "name": "Taipei", "coordinates": [121.469106, 25.101793] }, "to": { "name": "Shanghai", "coordinates": [121.480237, 31.236305] }, "count": 4 },
            { "from": { "name": "Taipei", "coordinates": [121.469106, 25.101793] }, "to": { "name": "Wuhan", "coordinates": [114.311408, 30.591013] }, "count": 5 },
            { "from": { "name": "Taipei", "coordinates": [121.469106, 25.101793] }, "to": { "name": "Chongqing", "coordinates": [106.910942, 29.429721] }, "count": 6 }]
        var inmap = new inMap.Map({
            id: 'allmap',
            skin: "Blueness",
            center: [121.469106, 25.101793],
            zoom: {
                value: 6,
                show: false,
                max: 18,
                min: 5
            },
        });
        var overlay = new inMap.MoveLineOverlay({
            style: {
                point: { //散点配置
                    tooltip: {
                        show: true,
                        formatter: "{name}"
                    },
                    style: {
                        normal: {
                            backgroundColor: 'rgba(200, 200, 50, 1)',
                            borderWidth: 1,
                            borderColor: "rgba(255,255,255,1)",
                            size: 6,
                            label: {
                                show: true,
                                color: 'rgba(255,255,255,1)'

                            },
                        },
                        mouseOver: {
                            backgroundColor: 'rgba(200, 200, 200, 1)',
                            borderColor: "rgba(255,255,255,1)",
                            borderWidth: 4,
                        },
                        selected: {
                            backgroundColor: 'rgba(184,0,0,1)',
                            borderColor: "rgba(255,255,255,1)"
                        },
                    },
                    event: {
                        onMouseClick: function (item) { }
                    }
                },
                line: { //线的配置
                    style: {
                        normal: {
                            borderColor: 'rgba(200, 200, 50, 1)',
                            borderWidth: 1,
                            // shadowColor: 'rgba(255, 250, 50, 1)',
                            // shadowBlur: 20,
                            lineOrCurive: "curve"
                        }
                    }
                },
                lineAnimation: {
                    style: {
                        size: 2,
                        //移动点颜色
                        fillColor: '#fff',
                        //移动点阴影颜色
                        shadowColor: '#fff',
                        //移动点阴影大小
                        shadowBlur: 10,
                        lineOrCurve: 'curve',
                    }

                },
            },
            data: data,

        });
        inmap.add(overlay);

        $(document).ready(function(){
            if($(window).width() < 520)
            {
                $("#allmap").hide();
            }

            $( window ).resize(function() {
                if($(window).width() < 520)
                {
                    $("#allmap").hide();
                }
            });
        });
    </script>
</body>

</html>