@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
		{{ trans('modOrderView.titleName') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">{{ trans('modOrderView.titleName') }}</li>
	</ol>
</section>

<div class="modal fade" id="ts-modal" tabindex="-1" role="dialog" aria-labelledby="lookupTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="lookupTitle">貨況選擇</h5>
      </div>
      <div class="modal-body" id="ts-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  <input type="hidden" id="lookupEvent" name="lookupEvent" />
</div>

@endsection 
@section('before_scripts')s
<script>
 var winHeight = $( window ).height() - 350 + 50;
    var gridOpt = {};
    gridOpt.pageId        = "modOrderView";
    gridOpt.enabledStatus = true;
    gridOpt.fieldsUrl     = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_view') }}";
    gridOpt.dataUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_view') }}";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key = " + gridOpt.dataUrl;
    gridOpt.createUrl     = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt/create') }}";
    gridOpt.editUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt') }}" + "/{id}/edit";
    gridOpt.height        = winHeight;
    gridOpt.searchOpt     = true;
    gridOpt.selectionmode = "checkbox";
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick = true;
    gridOpt.getStatusCount = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getStatusCount/mod_order_view') }}"

    var btnGroup = [
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
            //$("#jqxGrid").jqxGrid('exportdata', 'xls', '{{ trans("modOrder.titleName") }}');
            $("#jqxGrid").jqxGrid('exportdata', 'json', '訂單作業', true, null, false, BASE_API_URL+'/admin/export/data');
            }
        },
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        @can('OrdControl')
        {
            btnId: "btnAdd",
            btnIcon: "fa fa-edit",
            btnText: "{{ trans('common.add') }}",
            btnFunc: function () {
            location.href = gridOpt.createUrl;
            }
        },
        {
            btnId:"btnDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"{{ trans('common.delete') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                swal({
                    title: "確認視窗",
                    text: "您確定要刪除嗎？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: "確定",
                    cancelButtonText: "取消"
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + '/order/multi/del', {'ids': ids}, function(data){
                            if(data.msg == "success") {
                                swal("刪除成功", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                            }
                            else{
                                swal("操作失敗", data.errorMsg, "error");
                            }
                        });
                    
                    } else {
                    
                    }
                });
                
            }
        },
        @endcan
        @can('sendTrackingBtn')
        {
            btnId: "btnSendStatus",
            btnIcon: "fa fa-newspaper-o",
            btnText: "{{ trans('modOrder.sendCargo') }}",
            btnFunc: function () {
            var rowid = $("#jqxGrid").jqxGrid('getselectedrowindex');
            if(rowid == -1) {
                swal("{{ trans('modOrder.sendCargoWarning') }}", "", "warning");
                return;
            }


            $.get("{{ url(config('backpack.base.route_prefix', 'admin') . '/order/getTsData') }}", {}, function(data){
                var ttl = '';
                $.each(data, function(i, v){
                var p = '', ul = '', li = '';
                p  += '<h4>'+data[i]["ts_type"]+'</h4>';
                ul += '<ul class="list-inline">';
                
                $.each(data[i]["status"], function(k, vv) {
                    li += '<li><button type="button" class="btn bg-purple btn-flat margin tsBtn" tsid="'+data[i]["status"][k]["id"]+'">'+data[i]["status"][k]["ts_name"]+'</button></li>';
                });
                ul += li;
                ul += '</ul>';

                ttl += p + ul;
                });

                $("#ts-body").html(ttl);

                $(".tsBtn").unbind( "click" ).on("click", function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var selectedRecords = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        selectedRecords.push({tsid: $(this).attr("tsid"), sys_ord_no: row.sys_ord_no});
                    }
                }

                $.ajax({
                    url: "{{ url(config('backpack.base.route_prefix', 'admin').'/trackingApi/insertTracking') }}",
                    type: 'POST',
                    async: false,
                    data: {data: JSON.stringify(selectedRecords)},
                    beforeSend: function () {
                    loadingFunc.show();
                    },
                    error: function (jqXHR, exception) {
                    swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    loadingFunc.hide();
                    return;
                    },
                    success: function (data) {
                    if(data.msg == "success") {
                        swal('Success', "{{ trans('modOrder.sendSuccess') }}", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                    }
                    else {
                        swal('Oops...', "{{ trans('modOrder.error') }}", "error");

                    }
                    loadingFunc.hide();
                    return;
                    }
                });
                });

                $("#ts-modal").modal("show");
            }, 'JSON');
            }
        },
        @endcan
        @can('OrdControl')
        {
            btnId:"btnFail",
            btnIcon:"fa fa-ban",
            btnText:"{{ trans('common.fail') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                    ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                }

                swal({
                        title: "確認視窗",
                        text: "您確定要作廢嗎？",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#dc3545',
                        cancelButtonColor: '#6c757d',
                        confirmButtonText: "確定",
                        cancelButtonText: "取消"
                    }).then((result) => {
                        if (result.value) {
                            $.post(BASE_URL + '/order/fail', {'ids': ids}, function(data){
                                if(data.msg == "success") {
                                swal("作廢完成", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                                }
                                else{
                                swal("操作失敗", data.errorMsg, "error");
                                }
                            });
                        
                        } else {
                        
                        }
                });
            }
        },
        @endcan
        @can('ShowBtnReport')
        {
            btnId:"btnReport1",
            btnIcon:"fa fa-file",
            btnText:"{{ trans('common.btnReport1') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                var id_unc01 = new Array();
                var id_c01 = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        console.log(row.owner_cd);
                        ids.push(row.id);
                        if(row.owner_cd!='C01'){
                            id_unc01.push(row.id);
                        }
                        else{
                            id_c01.push(row.id);
                        }
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                if(id_unc01.length>0){
                    var now = new Date();
                    var date_now = now.format("yyyyMMddhhmmss");
                    console.log(date_now);
                    window.location.href =('http://172.105.207.193:8855/WebReport/ReportServer?reportlet=out.cpt&format=pdf&__filename__='+'出倉報表'+date_now+'&id=' + id_unc01.join(','));
                    console.log("report1");
                    // win.location.reload();
                }
                if(id_c01.length>0){
                    var now = new Date();
                    var date_now = now.format("yyyyMMddhhmmss");
                    window.location.href =('http://172.105.207.193:8855/WebReport/ReportServer?reportlet=out_sp.cpt&format=pdf&__filename__='+'出倉報表(有德)'+date_now+'&id=' + id_c01.join(','));
                    console.log("report2");
                    // win2.location.reload();
                };
            }
        },
        {
            btnId:"btnReport2",
            btnIcon:"fa fa-file",
            btnText:"{{ trans('common.btnReport2') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                var now = new Date();
                var date_now = now.format("yyyyMMddhhmmss");
                window.location.href = ('http://172.105.207.193:8855/WebReport/ReportServer?reportlet=dlv.cpt&format=pdf&__filename__='+'配送單'+date_now+'&id=' + ids.join(','));
                // win1.location.reload();
                
            }
        },
        @endcan
        /*{
            btnId:"btnClose",
            btnIcon:"fa fa-window-close-o",
            btnText:"{{ trans('common.close') }}",
            btnFunc:function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                ids.push(row.id);
                }
            }
            if(ids.length == 0) {
                swal("請至少選擇一筆資料", "", "warning");
                return;
            }
            $.post(BASE_URL + '/order/close', {'ids': ids}, function(data){
                if(data.msg == "success") {
                swal("完成", "", "success");
                $("#jqxGrid").jqxGrid('updatebounddata');
                }
                else{
                swal("操作失敗", "", "error");
                }
            });
            }
        },*/
        @can('ReCal')
        {
            btnId:"btnReCal",
            btnIcon:"fa fa-usd",
            btnText:"{{ trans('modOrder.btnReCal') }}",
            btnFunc:function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                ids.push(row.id);
                }
            }
            if(ids.length == 0) {
                swal("請至少選擇一筆資料", "", "warning");
                return;
            }
            $.post(BASE_URL + '/order/reCalculateFee', {'ids': ids}, function(data){
                if(data.msg == "success") {
                    swal("計算完成", "", "success");
                    $("#jqxGrid").jqxGrid('updatebounddata');
                }
                else{
                    var msg = (typeof data.showMsg == 'undefined')?data.showMsg:'';
                    swal("操作失敗", msg, "error");
                }
            });
            }
        },
        @endcan
        @can('OrdControl')
        {
            btnId: "btnCancelError",
            btnIcon: "fa fa-exclamation-circle",
            btnText: "{{ trans('modOrder.btnRevertUnNormal') }}",
            btnFunc: function () {
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                var statuss = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                    ids.push(row.id);
                    }
                    if(row.status == "拒收") {
                    statuss.push(row.id);
                    }
                }
                if(statuss.length > 0) {
                    swal("拒收不可解除異常", "", "warning");
                    return;
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                $.post(BASE_URL + '/order/cancelError', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("完成", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                    }
                    else{
                        var msg = (typeof data.showMsg == 'undefined')?data.showMsg:'';
                        swal("操作失敗", msg, "error");
                    }
                });
            }
        },
        @endcan
        @can('OrderMgmt_ImgDownload')
        {
            btnId: "btnDownLoadImg",
            btnIcon: "fa fa-file-image-o",
            btnText: "{{ trans('modOrder.btnDownloadPicture') }}",
            btnFunc: function () {
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.sys_ord_no);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                var strSysOrdNo = ids.join();

                window.open(BASE_URL + "/img/get?ids=" + strSysOrdNo);
            }
        },
        @endcan
        @can('OrderMgmt_CompulsiveDelete')
        {
            btnId:"btnCompulsiveDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"{{ trans('modOrder.btnDelete') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                swal({
                    title: "確認視窗",
                    text: "您確定要刪除嗎？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: "確定",
                    cancelButtonText: "取消"
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + '/order/compulsiveDel/del', {'ids': ids}, function(data){
                            if(data.msg == "success") {
                                swal("刪除成功", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                            }
                            else{
                                swal("操作失敗", data.errorMsg, "error");
                            }
                        });
                    
                    } else {
                    
                    }
                });
                
            }
        },
        @endcan
        @can('OrderMgmt_sendedi')
        {
            btnId:"btnSendEDI",
            btnIcon: "fa fa-cloud-upload",
            btnText:"{{ trans('modOrder.btnEDI') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("{{ trans('modOrder.pleaseChooseOneData') }}", "", "warning");
                    return;
                }

                $.post(BASE_URL + '/order/sendedi', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("{{ trans('modOrder.success') }}", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                    }
                    else{
                        swal("{{ trans('modOrder.error') }}", data.errorMsg, "error");
                    }
                });
                
            }
        }
        @endcan
    ];
</script>
@endsection
<script>
        var picsignsysno = 0;
    var picerrsysno = 0;
    var sysnonow = 0;
    $('#updategridsign').on('click', function(imgtype){
        console.log("success close window");
        document.getElementById(picsignsysno+"sign").style.background ="white";
    });  
    $('#updategriderror').on('click', function(imgtype){
        document.getElementById(picerrsysno+"error").style.background ="white";
        console.log("success close window");
    }); 
Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};
</script>

@include('backpack::template.search')

