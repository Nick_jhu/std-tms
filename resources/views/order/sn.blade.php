@extends('backpack::layout')
@section('header')
<section class="content-header">
    <h1>
        序號綁定
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">序號綁定</li>
    </ol>
</section>
@endsection
@section('before_scripts')
<style>   
table {
    width: 100%;
    display:block;
}
thead {
    display: inline-block;
    width: 100%;
    height: 20px;
}
tbody{
    width: 100%;
    display: table;
}
</style>
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js?v={{Config::get('app.version')}}"></script>
<script>
    var detailid= new Array();

    var gridOpt = {};
    gridOpt.pageId          = "modOrder";
    gridOpt.enabledStatus   = true;
    gridOpt.fieldsUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_confirm') }}";
    gridOpt.dataUrl         = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_confirm') }}";
    gridOpt.fieldsUrl       = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmttest/create') }}";
    gridOpt.editUrl         = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmttest') }}" + "/{id}/edit";
    gridOpt.height          = 800;
    gridOpt.selectionmode   = "checkbox";
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick  = true;
    gridOpt.searchOpt       = true;
    gridOpt.getStatusCount  = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getStatusCount/mod_order') }}"

    var btnGroup = [

    ];

    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });
</script>
@endsection

@section('content')
<div id="jqxLoader">
</div>


<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
                <div class="form-group col-md-2">
                    <label for="ord_no">訂單號</label>
                    <form method="post" action="test.php" onsubmit="return opennew();">
                    <input id="ord_no" type="text" name="ord_no" placeholder="請輸入訂單號">
                    <input type="submit" style="display:none" value="送出">
                    </form>
                </div>
                {{-- <div class="form-group col-md-2">
                    <label for="ord_no"></label>
                    <button type="button" class="btn btn-primary" id="btnSave">{{ trans('excel.btnSave') }}</button>
                </div> --}}
                <input type="button" style="display:none" id="updategrid" >
			</div>
			<div class="box-body no-padding">
                <p></p>

			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>
{{-- <div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="button-group">
                    <div class="row" id="btnArea"></div>
                    <div id="jqxGrid"></div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div> --}}
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
                <div id="order_model">
                    <!-- /.box-header -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="skeyModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">站別</h4>
			</div>
			<div class="modal-body" id="skeyBody">
				<div id="skeyGrid"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-danger" id="chStation">確定修改</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
@endsection
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/plugins/select2/select2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/dist/css/skins/_all-skins.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/plugins/pace/pace.min.css">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/backpack/pnotify/pnotify.custom.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/backpack/backpack.base.css">

<link rel="stylesheet" href="https://core.standard-info.com/css/custom.css?v=20190305-002">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="https://core.standard-info.com/vendor/jquery/jquery-ui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.css">
@section('after_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="{{ asset('vendor/ejs') }}/ejs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ79gLT1qaLfNJDf98OJr8cT5FZ4MJ4_w&libraries=geometry,places&ext=.js"></script>
<script type="text/javascript" src="https://aishek.github.io/jquery-animateNumber/javascripts/jquery.animateNumber.js"></script>
<script>
    var theme = 'bootstrap';
    $.jqx.theme = theme;

    var h = 350;

    if(gridOpt.enabledStatus == false) {
        h = 250;
    }
    var winHeigt = $( window ).height() - h;
    if(typeof gridOpt.who !== "undefined" && gridOpt.who == "lookup") {
        winHeigt = 500;
    }


    $("#saveGrid").on("click", function(){
        var items = $("#jqxlistbox").jqxListBox('getItems');
        //$('#jqxGrid').jqxGrid('clear');
        $("#"+gridOpt.gridId).jqxGrid('beginupdate');

        $.each(items, function(i, item) {
            console.log($('#'+gridOpt.gridId).jqxGrid('getcolumnindex', item.value));
            var thisIndex = $('#'+gridOpt.gridId).jqxGrid('getcolumnindex', item.value)-1;
            if(thisIndex != item.index){
                //console.log(item.value+":"+thisIndex+"="+item.index);
                $('#'+gridOpt.gridId).jqxGrid('setcolumnindex', item.value,  item.index);
            }
            if (item.checked) {
                $("#"+gridOpt.gridId).jqxGrid('showcolumn', item.value);
            }
            else {
                $("#"+gridOpt.gridId).jqxGrid('hidecolumn', item.value);
            }
        })
        
        $("#"+gridOpt.gridId).jqxGrid('endupdate');
        state = $("#"+gridOpt.gridId).jqxGrid('getstate');

        var saveUrl = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/saveLayoutJson') }}"; 	  	  	
        var stateToSave = JSON.stringify(state);

        $.ajax({
            type: "POST",										
            url: saveUrl,		
            data: { data: stateToSave,key: gridOpt.dataUrl },		 
            success: function(response) {
                if(response == "true"){
                    alert("save successful");
                    $('#gridOptModal').modal('hide');
                }else{
                    alert("save failded");
                }
                
            }
        });	
    });

    $("#clearGrid").on("click", function(){
        $.ajax({
            type: "POST",										
            url: "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/clearLayout') }}",		
            data: { key: gridOpt.dataUrl },		 
            success: function(response) {
                if(response == "true"){
                    alert("clear successful");
                    location.reload();
                    $('#gridOptModal').modal('hide');
                }else{
                    //alert("save failded");
                }
                
            }
        });	
    });
    

    function getSearchTpl(state) {
        var fields = [];
        $.each(state.columns, function(i, item) {
            if(item.hidden == false && item.text != "") {
                var a = {
                    label: item.text,
                    value: i
                }
                fields.push(a);
            }
        });
    }
    $('#Savesn').on('click', function(){
        console.log("savetest");

    });
</script>
<script>
    $(document).ready(function() {
    // 在這撰寫javascript程式碼
        console.log("ready");
        $('#ord_no').focus();
    });
    $('#updategrid').on('click', function(){
        console.log("test");
        swal.close();
        $('#ord_no').focus();
        $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
    });
    function savesnno() {
        console.log(detailid);
        var detailsnno = new Array();
        var snno = "";
        $.each(detailid, function(i, item) {
            snno = $('#'+item).val();
            detailsnno.push(snno);
        });
        $.post(BASE_URL + '/order/snnostore/', {'ids': detailid,'nos': detailsnno}, function(data){
            if(data.msg == "success") {
                swal("成功", "", "success");
                $("#order_model").html('');
                $("#ord_no").val("");
                $('#ord_no').focus();
            }
            else{
                swal("操作失敗", data.errorMsg, "error");
            }
        });
    };
    function opennew() {  
        var ord_no = $("#ord_no").val();
        var ordHtml = "";
        var resulrHtml = "";
        var isfirst = "Y";
        var ordHtml="";
        var firstid="";
        var owner="";
        var nextid ="";
        detailid= new Array();
        $("#order_model").html('');
        // $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
        $.post(BASE_URL + '/order/checkforsn/', {'ord_no': ord_no}, function(data){
            if(data.msg == "success") {
                console.log(data);
                owner = data.owner;
                ordHtml="<div class='row'><div class='form-group col-md-2'>\
                    <label for='owner'>"+owner+"</label>\
                </div></div>";
                $.each(data.data, function(i, item) {
                    if(firstid==""){
                        firstid = item.id;
                    }
                    if(data.data.length==(i+1)){
                        nextid =i;
                    }else{
                        nextid = data.data[i+1]['id'];
                    }
                    console.log(item);
                    // 料號 名稱 數量 
                    // <div class='form-group col-md-3'> 
                    //     <label for='sn_no'>{{ trans('modOrderDetail.snNo') }}</label>
                    //     <input type='text' class='form-control input-sm'>
                    // </div>

                    // <form id="snform"  action="sendsn" method="post" onsubmit="return sub();"> 
                    //     {{ csrf_field() }}
                    // </form> box-body
                    detailid.push(item.id);
                    if(item.sn_no==null){
                        item.sn_no = "";    
                    }
                    if(item.sn_no==null){
                        item.sn_no = "";    
                    }
                    if(item.goods_nm==null){
                        item.goods_nm = "";    
                    }
                    if(item.pkg_num==null){
                        item.pkg_num = 0;    
                    }
                    
                    // $( "li.third-item" ).next()
                    ordHtml = ordHtml+
                        "<form id='formId' method='POST' accept-charset='UTF-8' enctype='multipart/form-data'>\
                        <div class='box-body detail'> <div class='col-md-6'><label for='sn_no'>\
                        {{ trans('modOrderDetail.snNo') }}</label>\
                        <input type='text' onkeydown='detailchange(event,"+nextid+")' class='sn-input form-control input-sm' id='"+
                        item.id+ "'value='"+ item.sn_no+"'>"
                        +"</div>"+
                        "<div class='col-md-2'><label for='goodsNm'>\
                        {{ trans('modOrderDetail.goodsNm') }}</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.goods_nm+"'>"
                        +"</div>"+
                        "<div class='col-md-2'><label for='class'>\
                        {{ trans('modOrderDetail.pkgNum') }}</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.pkg_num+"'>"
                        +"</div>"+
                        "<div class='col-md-2'><label for='class'>\
                        {{ trans('modOrderDetail.goodsNo') }}</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.goods_no+"'>"
                        +"</div></div></form>"
                        ;
                });
                ordHtml = ordHtml+"<div class='box-footer'>\
                    <button type='button' class='btn btn-sm btn-primary' id='Savesn' onclick='savesnno()'>儲存</button>\
                </div>";
                $("#order_model").append(ordHtml);
            }else{
                swal("查無此單號!", '查無此單號', "error");
            }
            // $("#ord_no").val("");
            $('#'+firstid).focus();
        });
        return false;
    }  
Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};
function detailchange(event,id) {
    console.log(event);
    if( event.keyCode == 13 ) {
        $('#'+id).focus();
    }
}

</script>
@endsection