@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
		訂單明細總覽(結案)
        <strong style="color:red;">(至少一個查詢條件後才可顯示資料)</strong>
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">訂單明細總覽(結案)</li>
	</ol>
</section>

@endsection 
@section('before_scripts')
<script>
    var gridOpt = {};
    gridOpt.pageId        = "modOrderDetailcloseView";
    gridOpt.enabledStatus = false;
    gridOpt.fieldsUrl     = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_detail_close_view') }}";
    gridOpt.dataUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_detail_close_view') }}";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key = " + gridOpt.dataUrl;
    gridOpt.height        = 800;
    gridOpt.searchOpt     = true;
    gridOpt.selectionmode   = "checkbox";
    // gridOpt.selectionmode = "multiplecellsadvanced";
    gridOpt.enablebrowserselection = false;
    gridOpt.rowdoubleclick = false;
    var enabledheader = [] ;
    var filtercolumndata = [];
    var btnGroup = [
        {
            btnId: "btnSearschWindow",
            btnIcon: "fa fa-search",
            btnText: "{{ trans('common.search') }}",
            btnFunc: function () {
                $('#searchWindow').jqxWindow('open');
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
                var url  =BASE_URL;
                url = url.replace("admin","");
                var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
                filtercolumndata = [];
                for (var i = 0; i < filterGroups.length; i++) {
                    var filterGroup = filterGroups[i];
                    var filters = filterGroup.filter.getfilters();
                        for (var k = 0; k < filters.length; k++) {
                           if(filters[k].condition!="CONTAINS" && filters[k].condition!="NOT_EQUAL" ){
                            var filtercolumn = {
                            'column': filterGroup.filtercolumn,
                            'value': Date.parse(filters[k].value),
                            'condition': filters[k].condition
                        };
                        }else{
                            var filtercolumn = {
                            'column': filterGroup.filtercolumn,
                            'value': filters[k].value,
                            'condition': filters[k].condition
                        };
                        }
                        filtercolumndata.push(filtercolumn);
                    }
                }
                var sortdata = $("#jqxGrid").jqxGrid("getsortcolumns");
                var sorttype = "";
                var sortfield = "";
                for(i=0;i<sortdata.length;i++){
                    if(sortdata[i].ascending==true){
                        type="asc";
                    }else{
                        type="desc";
                    }
                    if(sortfield==""){
                        sortfield = sortdata[i].dataField;
                    }else{
                        sortfield = sortfield+";"+sortdata[i].dataField;
                    }
                    if(sorttype==""){
                        sorttype = type;
                    }else{
                        sorttype = sorttype+";"+type;
                    }
                }

                $.post(BASE_API_URL + '/admin/export/data', {
                    'table': 'mod_order_detail_close_view',
                    'filename':'訂單明細總覽(結案)' ,
                    'columndata': filtercolumndata,
                    'header': enabledheader,
                    'sorttype': sorttype,
                    'sortfield': sortfield,
                }, function(data){
                    if(data.msg == "success") {
                        window.location.href =url+"storage/excel/"+data.downlink;
                    }
                });
            }
        },
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
    ];
</script>
@endsection


@include('backpack::template.search')

