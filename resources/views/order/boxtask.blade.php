@extends('backpack::layout')
@section('header')
<section class="content-header">
    <h1>
        工作站裝箱作業
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">工作站裝箱作業</li>
    </ol>
</section>
@endsection
@section('before_scripts')
<style>   
table {
    width: 100%;
    display:block;
}
thead {
    display: inline-block;
    width: 100%;
    height: 20px;
}
tbody{
    width: 100%;
    display: table;
}
input[type="text"]
{
    font-size:30px;
}
.input-sm{
    height: 40px;
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
}
</style>
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js?v={{Config::get('app.version')}}"></script>
<script>
    var detailid= new Array();

    var gridOpt = {};
    gridOpt.pageId          = "modOrder";
    gridOpt.enabledStatus   = true;
    gridOpt.fieldsUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_confirm') }}";
    gridOpt.dataUrl         = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_confirm') }}";
    gridOpt.fieldsUrl       = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmttest/create') }}";
    gridOpt.editUrl         = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmttest') }}" + "/{id}/edit";
    gridOpt.height          = 800;
    gridOpt.selectionmode   = "checkbox";
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick  = true;
    gridOpt.searchOpt       = true;
    gridOpt.getStatusCount  = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getStatusCount/mod_order') }}"

    var btnGroup = [
    ];

    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });
</script>
@endsection

@section('content')
<div id="jqxLoader">
</div>


<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
                <input type="hidden" id="sysordno" type="text" name="sysordno">
                <div class="form-group col-md-3">
                    <label  style="font-size: 30" for="ord_no">訂單號</label>
                    <form method="post" action="test.php" onsubmit="return opennew();">
                    <input id="ord_no" type="text" name="ord_no" placeholder="請輸入訂單號">
                    <input type="submit" style="display:none" value="送出">
                    </form>
                </div>
                <div class="form-group col-md-3">
                    <label style="font-size: 30" for="ord_no">商品料號</label>
                    <form method="post" onsubmit="return opensndetail();">
                    <input id="goodsinput" type="text" name="goodsinput" placeholder="請輸入商品編號1">
                    {{-- <input type="submit" style="display:none" value="送出"> --}}
                    </form>
                </div>

                <div class="form-group col-md-6">
                    <br>
                    {{-- <label for="ord_no"  id="reportarea" style="display:none">列印報表</label> --}}
                    <button type = "button" class = "btn btn-primary" id = "report"        onclick="printreport('A')" style = "display:none;font-size: 50; margin-left:30px;">面單</button>
                    <button type = "button" class = "btn btn-primary" id = "bigreport"     onclick="printreport('B')" style = "display:none;font-size: 50; margin-left:80">大面單</button>
                    <button type = "button" class = "btn btn-primary" id = "etmailreport"  onclick="printreport('C')" style = "display:none;font-size: 50; margin-left:80">東森</button>
                    <button type = "button" class = "btn btn-primary" id = "senaoreport"   onclick="printreport('D')" style = "display:none;font-size: 50; margin-left:80">神腦</button>
                    <button type = "button" class = "btn btn-primary" id = "weblinkreport" onclick="printreport('E')" style = "display:none;font-size: 50; margin-left:80">展碁</button>
                    <button type = "button" class = "btn btn-primary" id = "clearbotask"   onclick="clearbotask()" style = "display:none;font-size: 20; margin-left:80">  清除<br/>裝箱資料</button>
                    <button type = "hidden" class = "btn btn-primary" id = "reporttest"    onclick="printreporttest('A')" style = "display:none;font-size: 50; margin-left:30px;">面單</button>
                </div>

                <input type="button" style="display:none" id="updategrid" >
			</div>
			<div class="box-body no-padding">
                <p></p>

			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>
{{-- <div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="button-group">
                    <div class="row" id="btnArea"></div>
                    <div id="jqxGrid"></div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div> --}}
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
                <div id="order_model">
                    <!-- /.box-header -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="skeyModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">站別</h4>
			</div>
			<div class="modal-body" id="skeyBody">
				<div id="skeyGrid"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-danger" id="chStation">確定修改</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
@endsection
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/plugins/select2/select2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/dist/css/skins/_all-skins.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/plugins/pace/pace.min.css">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/backpack/pnotify/pnotify.custom.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/backpack/backpack.base.css">

<link rel="stylesheet" href="https://core.standard-info.com/css/custom.css?v=20190305-002">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="https://core.standard-info.com/vendor/jquery/jquery-ui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.css">
@section('after_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="{{ asset('vendor/ejs') }}/ejs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ79gLT1qaLfNJDf98OJr8cT5FZ4MJ4_w&libraries=geometry,places&ext=.js"></script>
<script type="text/javascript" src="https://aishek.github.io/jquery-animateNumber/javascripts/jquery.animateNumber.js"></script>
<script>
    var theme = 'bootstrap';
    $.jqx.theme = theme;

    var h = 350;

    if(gridOpt.enabledStatus == false) {
        h = 250;
    }
    var winHeigt = $( window ).height() - h;
    if(typeof gridOpt.who !== "undefined" && gridOpt.who == "lookup") {
        winHeigt = 500;
    }


    $("#saveGrid").on("click", function(){
        var items = $("#jqxlistbox").jqxListBox('getItems');
        //$('#jqxGrid').jqxGrid('clear');
        $("#"+gridOpt.gridId).jqxGrid('beginupdate');

        $.each(items, function(i, item) {
            console.log($('#'+gridOpt.gridId).jqxGrid('getcolumnindex', item.value));
            var thisIndex = $('#'+gridOpt.gridId).jqxGrid('getcolumnindex', item.value)-1;
            if(thisIndex != item.index){
                //console.log(item.value+":"+thisIndex+"="+item.index);
                $('#'+gridOpt.gridId).jqxGrid('setcolumnindex', item.value,  item.index);
            }
            if (item.checked) {
                $("#"+gridOpt.gridId).jqxGrid('showcolumn', item.value);
            }
            else {
                $("#"+gridOpt.gridId).jqxGrid('hidecolumn', item.value);
            }
        })
        
        $("#"+gridOpt.gridId).jqxGrid('endupdate');
        state = $("#"+gridOpt.gridId).jqxGrid('getstate');

        var saveUrl = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/saveLayoutJson') }}"; 	  	  	
        var stateToSave = JSON.stringify(state);

        $.ajax({
            type: "POST",										
            url: saveUrl,		
            data: { data: stateToSave,key: gridOpt.dataUrl },		 
            success: function(response) {
                if(response == "true"){
                    alert("save successful");
                    $('#gridOptModal').modal('hide');
                }else{
                    alert("save failded");
                }
                
            }
        });	
    });

    $("#clearGrid").on("click", function(){
        $.ajax({
            type: "POST",										
            url: "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/clearLayout') }}",		
            data: { key: gridOpt.dataUrl },		 
            success: function(response) {
                if(response == "true"){
                    alert("clear successful");
                    location.reload();
                    $('#gridOptModal').modal('hide');
                }else{
                    //alert("save failded");
                }
                
            }
        });	
    });
    

    function getSearchTpl(state) {
        var fields = [];
        $.each(state.columns, function(i, item) {
            if(item.hidden == false && item.text != "") {
                var a = {
                    label: item.text,
                    value: i
                }
                fields.push(a);
            }
        });
    }
    $('#Savesn').on('click', function(){
        console.log("savetest");

    });
</script>
<script>

    var orddetailid  = '';
    var goodsno      = '';
    var goodsnm      = '';
    var pkgnum       = '';
    var boxno        = '';
    var snno         = '';
    var mainid       = '';
    var printflag    = 'Y';
    var continueflag = 'N';
    var ischange     = 0;
    var totalnum     = 0;
    var detailids    = new Array();
    $(document).ready(function() {
    // 在這撰寫javascript程式碼
        console.log("ready");
        $('#ord_no').focus();
    });
    $('#updategrid').on('click', function(){
        console.log("test");
        swal.close();
        $('#ord_no').focus();
        $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
    });

    function savesnno() {
        console.log(detailid);
        var detailsnno = new Array();
        var snno = "";
        $.each(detailid, function(i, item) {
            snno = $('#'+item).val();
            detailsnno.push(snno);
        });
        $.post(BASE_URL + '/order/snnostore/', {'ids': detailid,'nos': detailsnno}, function(data){
            if(data.msg == "success") {
                swal("成功", "", "success");
                $("#order_model").html('');
                $("#ord_no").val("");
                $('#ord_no').focus();
            }
            else{
                swal("操作失敗", data.errorMsg, "error");
            }
        });
    };
    function opennew() {  
        var ord_no = $("#ord_no").val();
        var ordHtml = "";
        var resulrHtml = "";
        var isfirst = "Y";
        var ordHtml="";
        var firstid="";
        var owner="";
        var nextid ="";
        printflag = "Y";
        detailid= new Array();
        $("#order_model").html('');
        // $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
        $.post(BASE_URL + '/order/checkforsn/', {'ord_no': ord_no}, function(data){
            if(data.msg == "success") {
                console.log(data);
                owner     = data.owner;
                ownercode = data.ownercode;
                mainid    = data.mainid;
                // 2074503	東森得易購股份有限公司	展碁國際股份有限公司
                // 2096600	 	306663-展碁國際
                // 2201890	 	(11927)展碁國際
                document.getElementById("report").style.display        = "none";
                document.getElementById("bigreport").style.display     = "none";
                document.getElementById("etmailreport").style.display  = "none";
                document.getElementById("senaoreport").style.display   = "none";
                document.getElementById("weblinkreport").style.display = "none";
                if (ownercode == '2074503') {
                    document.getElementById("etmailreport").style.display = "inline";
                } else if(ownercode == '2096600') {
                    document.getElementById("senaoreport").style.display = "inline";
                } else if(ownercode == '2201890') {
                    document.getElementById("weblinkreport").style.display = "inline";
                } else {
                    document.getElementById("report").style.display = "inline";
                    document.getElementById("bigreport").style.display = "inline";
                }
                if(data.totalboxnum == null) {
                    data.totalboxnum = 1;
                }

                ordHtml="<div class='row'>\
                    <div class=' col-md-3'>\
                        <label style='font-size: 30' for='owner'>"+owner+"</label>\
                    </div>\
                    <div class=' col-md-3'>\
                        <label style='font-size: 30' for='boxnum'>"+"總箱數:</label>\
                        <input style='font-size:20;height:40' id='totalboxnum' type='number' min =1 value ="+data.totalboxnum+">\
                    </div>\
                </div>";
                $.each(data.data, function(i, item) {
                    if(firstid==""){
                        firstid = item.id;
                    }
                    if(data.data.length==(i+1)){
                        nextid =i;
                    }else{
                        nextid = data.data[i+1]['id'];
                    }
                    detailids[item.id] =  item.pkg_num - item.sn_count;
                    detailid.push(item.id);
                    $display = 'style="display:none"';
                    if(item.sn_flag=="Y"){
                        $display = "style=display:inline";
                    } else {
                        $display = "style=display:none";
                    }
                    if(item.sn_no==null){
                        item.sn_no = "";    
                    }
                    if(item.goods_nm==null){
                        item.goods_nm = "";    
                    }
                    if(item.pkg_num==null){
                        item.pkg_num = 0;    
                    }
                    if(item.swipe_count==null){
                        item.swipe_count = 0;
                    }
                    if(item.box_no==null){
                        item.box_no = 1;
                    }
                    if(item.sn_count==null){
                        item.sn_count = 0;
                    }
                    // 商品編號1 商品名稱 數量 已刷的數量 箱號 序號數量 
                    if(item.pkg_num != item.swipe_count) {
                        printflag ="N" ;
                    } else {
                        printflag ="Y" ;
                    }
                    if (item.sn_flag == "Y" ) {
                        ordHtml = ordHtml+
                        "<div class='row'><div class='col-md-2'><label style='font-size: 30' for='goodsNo'>\
                        {{ trans('modOrderDetail.goodsNo') }}</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.goods_no+"'>"
                        +"</div>"+

                        "<div class='col-md-2'><label style='font-size: 30' for='pkgNum'>\
                        商品名稱</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.goods_nm+"'>"
                        +"</div>"+

                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                        數量</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.pkg_num+"'>"
                        +"</div>"+

                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                        已裝數量</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.swipe_count+"'>"
                        +"</div>"+

                        "<div class='col-md-2'"+$display+"><label style='font-size: 30' for='class'>\
                        序號數量</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.sn_count+"'>"
                        +"</div></div></div>"
                        ;
                    } else {
                        ordHtml = ordHtml+
                        "<div class='row'><div class='col-md-2'><label style='font-size: 30' for='goodsNo'>\
                        {{ trans('modOrderDetail.goodsNo') }}</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.goods_no+"'>"
                        +"</div>"+

                        "<div class='col-md-2'><label style='font-size: 30' for='pkgNum'>\
                        商品名稱</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.goods_nm+"'>"
                        +"</div>"+

                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                        數量</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.pkg_num+"'>"
                        +"</div>"+

                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                        已裝數量</label>\
                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                        "value='"+ item.swipe_count+"'>"
                        +"</div></div></div>";
                    }
                });
                $("#order_model").append(ordHtml);

            }else{
                swal("查無此單號!", '查無此單號', "error");
            }
             $("#sysordno").val(data.sysordno);

            document.getElementById("clearbotask").style.display = "inline";

             
             $("#goodsinput").focus();
        });
        return false;
    } 
    function opensndetail() {  
        var sysordno   = $("#sysordno").val();
        var goodsno    = $("#goodsinput").val();
        if(goodsno==""){
            return false;
        }
        var ordHtml      = "";
        var resulrHtml   = "";
        var firstid      = "";
        var owner        = "";
        var nextid       = "";

        if( goodsno == "printsylreport@A" ) {
            $("#goodsinput").val('');
            printreport('A');
            return false;
        } else if(goodsno == "printsylreport@B") {
            $("#goodsinput").val('');
            printreport('B');
            return false;
        } else if(goodsno == "printsylreport@C") {
            $("#goodsinput").val('');
            printreport('C');
            return false;
        } else if(goodsno == "printsylreport@D") {
            $("#goodsinput").val('');
            printreport('D');
            return false;
        } else if(goodsno == "printsylreport@E") {
            $("#goodsinput").val('');
            printreport('E');
            return false;
        }

        // $("#order_model").html('');
        // $("#jqxGrid").jqxGrid('updatebounddata', 'sort');;
        // return false;
        $.post(BASE_URL + '/order/checkasnrask/', {'sysordno': sysordno, 'goodsno': goodsno}, function(data){
            if(data.msg == "success") {
                $("#goodsinput").val(goodsno); 
                console.log(data);

                // 料號 商品名稱 數量 已刷數量(可編輯) 箱號(預設1)  
                if(data.proddata == null ) {
                    swal("料號不存在", '此商品不存在料號建檔', "error");
                    return false;
                }
                if( data.data.swipe_count == null ) {
                    data.data.swipe_count = 0;
                }

                if( data.data.box_no == null ) {
                    data.data.box_no = 1;
                }
                totalnum    = data.data.pkg_num;
                orddetailid = data.data.id;
                goodsno     = goodsno;
                goodsnm     = data.data.goods_nm;
                pkgnum      = data.data.pkg_num;
                boxno       = data.data.box_no;
                snno        = snno;

                var remainingCount = detailids[data.data.id];

                remainnum = data.remainnum;

                if(remainnum == 0 ) {
                    $("#goodsinput").val('');
                    swal("已刷滿", '此商品數量已達標', "error");
                    $("#goodsinput").focus();
                    return false;
                }

                if(ischange > 0 ) {
                    data.data.box_no = data.data.box_no +1;
                    ischange = 0;
                }
                if(data.proddata.sn_flag == 'Y') {
                    if(remainingCount > 0) {
                        ordHtml = 
                            "<div class='col-md-4'>\
                                <label style='font-size: 30' for='class'>料號</label>\
                                <input type='text' class='form-control input-sm' name='goodsno' readonly='readonly' value='"+goodsno+"'>"
                            +"</div>"+
                            "<div class='col-md-4'>\
                                <label style='font-size: 30' for='class' >商品名稱</label>\
                                <input type='text' class='form-control input-sm' name='goodsnm'  readonly='readonly' value='"+data.data.goods_nm+"'>"
                            +"</div><p>"+

                            "<div class='col-md-4' style='height: 100;'></div>"+

                            "<p><div class='col-md-4'>\
                                <label style='font-size: 30' for='class'>未裝箱數量</label>\
                                <input type='number' style='font-size: 30' class='form-control input-sm' name='pkgnum'  readonly='readonly' value='"+remainnum+"'>"
                            +"</div>"+

                            "<div class='col-md-4'>\
                                <label style='font-size: 30' for='class'>裝箱數量</label>\
                                <input type='number' style='font-size: 30' class='form-control input-sm' id ='swipecount' readonly='readonly' onkeydown='swipeenter(event)' name='swipecount' value='1''>"
                            +"</div>"+

                            "<div class='col-md-4'>\
                                <label style='font-size: 30' for='class'>箱號</label>\
                                <input type='number' style='font-size: 30' class='form-control input-sm' name='boxno' id='boxnonow' readonly='readonly' value='"+data.data.box_no+"'>"
                            +"</div>"+

                            
                            "<p> <div class='col-md-8'>\
                                <label style='font-size: 40' for='class'>序號</label>\
                                <input id='snnofocus' style='font-size: 70;height:70px' onkeydown='snenter(event)' type='text' class='form-control input-sm' value='"+''+"'>"+
                                "<input id='hiddencolumn' onkeydown='hiddenenter(event)' type='test' class='form-control input-sm' style='opacity: 0'>"
                            +"</div><p>"+
                            "<p> <div class='col-md-11'>"+
                                '<input type="button" style="font-size: 30;margin-right:50" class="btn btn-primary" onclick=onBtnClicked("confirm","Y") value="確認">'+
                                '<input type="button" style="font-size: 30" class="btn btn-danger" onclick=cancelswal() value="取消">'+
                            "</div>"+
                            "<div class='col-md-1'>"+
                                '<input type="button" style="font-size: 30" class="btn btn-danger" onclick=onBtnClicked("changebox") value="換箱">'+
                            "</div><p>"
                        ;
                    } else {
                        ordHtml = 
                            "<div class='col-md-4'>\
                                <label style='font-size: 30' for='class'>料號</label>\
                                <input type='text' class='form-control input-sm' name='goodsno' readonly='readonly' value='"+goodsno+"'>"
                            +"</div>"+
                            "<div class='col-md-4'>\
                                <label style='font-size: 30' for='class' >商品名稱</label>\
                                <input type='text' class='form-control input-sm' name='goodsnm'  readonly='readonly' value='"+data.data.goods_nm+"'>"
                            +"</div><p>"+

                            "<div class='col-md-4' style='height: 100;'></div>"+

                            "<p><div class='col-md-4'>\
                                <label style='font-size: 30' for='class'>未裝箱數量</label>\
                                <input type='number' style='font-size: 30' class='form-control input-sm' name='pkgnum'  readonly='readonly' value='"+remainnum+"'>"
                            +"</div>"+

                            "<div class='col-md-4'>\
                                <label style='font-size: 30' for='class'>裝箱數量</label>\
                                <input type='number' style='font-size: 30' class='form-control input-sm' id ='swipecount' readonly='readonly' onkeydown='swipeenter(event)' name='swipecount' value='1''>"
                            +"</div>"+

                            "<div class='col-md-4'>\
                                <label style='font-size: 30' for='class'>箱號</label>\
                                <input type='number' style='font-size: 30' class='form-control input-sm' name='boxno' id='boxnonow' readonly='readonly' value='"+data.data.box_no+"'>"
                            +"</div>"+
                            "<div class='col-md-6'>\
                                <input id='hiddencolumn' onkeydown='hiddenenter(event)' type='test' class='form-control input-sm' style='opacity: 0'>"
                            +"</div>"+
                            "<p> <div class='col-md-11'>"+
                                '<input type="button" style="font-size: 30;margin-right:50" class="btn btn-primary" onclick=onBtnClicked("confirm","Y") value="確認">'+
                                '<input type="button" style="font-size: 30" class="btn btn-danger" onclick=cancelswal() value="取消">'+
                            "</div>"+
                            "<div class='col-md-1'>"+
                                '<input type="button" style="font-size: 30" class="btn btn-danger" onclick=onBtnClicked("changebox") value="換箱">'+
                            "</div><p>"
                        ;
                    }

                    swal({
                        title            : '裝箱明細',
                        icon             : 'info',
                        html             : ordHtml,
                        width            : '80%',
                        showCloseButton  : true,
                        showCancelButton : false,
                        focusConfirm     : false,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        closeOnClickOutside: false
                    });
                    if(remainingCount > 0) {
                        $("#snnofocus").focus();
                    } else {
                        $("#hiddencolumn").focus();
                    }
                    
                } else {
                    ordHtml = 
                    "<p> <div class='col-md-4'>\
                        <label style='font-size: 30' for='class'>料號</label>\
                        <input type='text' class='form-control input-sm' name='goodsno' readonly='readonly' value='"+goodsno+"'>"
                    +"</div>"+

                    "<div class='col-md-4'>\
                        <label style='font-size: 30' for='class' >商品名稱</label>\
                        <input type='text' class='form-control input-sm' name='goodsnm'  readonly='readonly' value='"+data.data.goods_nm+"'>"
                    +"</div><p>"+

                    "<div class='col-md-4' style='height: 100;'></div>"+

                    "<p><div class='col-md-4'>\
                        <label style='font-size: 30' for='class'>未裝箱數量</label>\
                        <input type='number' style='font-size: 30' class='form-control input-sm' name='pkgnum'  readonly='readonly' value='"+remainnum+"'>"
                    +"</div>"+

                    "<div class='col-md-4'>\
                        <label style='font-size: 30' for='class'>裝箱數量</label>\
                        <input type='number' style='font-size: 30' class='form-control input-sm' id ='swipecount' onkeydown='swipeenter(event)' name='swipecount' value='1''>"
                    +"</div>"+

                    "<div class='col-md-4'>\
                        <label style='font-size: 30' for='class'>箱號</label>\
                        <input type='number' style='font-size: 30' class='form-control input-sm' name='boxno' id='boxnonow' readonly='readonly' value='"+data.data.box_no+"'>"
                    +"</div><p>"+

                    "<p> <div class='col-md-6'>\
                        <input id='hiddencolumn' onkeydown='hiddenenter(event)' type='test' class='form-control input-sm' style='opacity: 0'>"
                    +"</div><p>"+
                    "<p> <div class='col-md-11'>"+
                        '<input type="button" style="font-size: 30;margin-right:50" class="btn btn-primary" onclick=onBtnClicked("confirm","N") value="確認">'+
                        '<input type="button" style="font-size: 30" class="btn btn-danger" onclick=cancelswal() value="取消">'+
                    "</div>"+
                    "<div class='col-md-1'>"+
                        '<input type="button" style="font-size: 30" class="btn btn-danger" onclick=onBtnClicked("changebox") value="換箱">'+
                    "</div><p>";

                    swal({
                        title            : '裝箱明細',
                        icon             : 'info',
                        html             : ordHtml,
                        width            : '80%',
                        showCloseButton  : true,
                        showCancelButton : false,
                        focusConfirm     : false,
                        allowOutsideClick: false,
                        allowEscapeKey   : false,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        closeOnClickOutside: false
                    })
                    $("#hiddencolumn").focus();
                }
            }else{
                swal("查無此料號!", '查無此料號', "error");
                $("#goodsinput").val('');
                $("#goodsinput").focus();
            }
            // $("#ord_no").val("");
            // $("#goodsinput").focus();
        });
        return false;
    } 
      
Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};

$(".detail").keydown(function(event){
    console.log("132");
    if( event.which == 13 ) {
        $( "div.detail" ).next().focus();
    }
});
function detailchange(event) {
    console.log(event);

    // if( event.keyCode == 13 ) {
    //     $('#'+id).focus();
    // }
}
function snenter(event) {
    if( event.which == 13 ) {
        $('#hiddencolumn').focus();
    }
}

function cancelswal() {
    
    swal.close();
    $('#goodsinput').focus();
    $('#goodsinput').val('');
}
function onBtnClicked(type,snflag) {
    var firstid    = "";
    var detail =  new Array();
    if(type == 'confirm') {
        var snno  = $("#snnofocus").val();
        boxno = $("#boxnonow").val();

        if((snno == "" || snno == null) && continueflag=='N'   && snno !=undefined && detail[item.id] > 0) {
            alert('序號為空');
            $('#hiddencolumn').val('');
            $('#hiddencolumn').focus();
            return;
        }
        detailids[orddetailid] --;
        continueflag = 'N';
        $.ajax({
                url: BASE_URL +'/saveboxdetail/',
                type: 'POST',
                data:  { 
                    'sysordno'   : $("#sysordno").val(),
                    'orddetailid': orddetailid,
                    'goodsno'    : $("#goodsinput").val(),
                    'goodsnm'    : goodsnm,
                    'pkgnum'     : pkgnum,
                    'swipecount' : $("#swipecount").val(),
                    'boxno'      : boxno,
                    'snno'       : $("#snnofocus").val()
                },
                async: true,
                beforeSend: function () {
                },
                error: function (jqXHR, exception) {
                    
                },
                success: function (data) {
                    if(data.msg == "success" ) {
                        var ordHtml      = "";
                        $("#order_model").html('');

                        $.post(BASE_URL + '/order/checkforsn/', {'ord_no': $("#sysordno").val()}, function(data){
                            if(data.msg == "success") {
                                console.log(data);
                                owner     = data.owner;
                                ownercode = data.ownercode;
                                mainid    = data.mainid;

                                document.getElementById("etmailreport").style.display = "none";
                                document.getElementById("senaoreport").style.display = "none";
                                document.getElementById("weblinkreport").style.display = "none";
                                if (ownercode == '2074503') {
                                    document.getElementById("etmailreport").style.display = "inline";
                                } else if(ownercode == '2096600') {
                                    document.getElementById("senaoreport").style.display = "inline";
                                } else if(ownercode == '2201890') {
                                    document.getElementById("weblinkreport").style.display = "inline";
                                } else {
                                    document.getElementById("report").style.display = "inline";
                                    document.getElementById("bigreport").style.display = "inline";
                                }

                                if(data.totalboxnum == null) {
                                    data.totalboxnum = 1;
                                }
                                if(ischange >0 ) {
                                    data.totalboxnum = data.totalboxnum +1;
                                }

                                ordHtml="<div class='row'>\
                                    <div class=' col-md-3'>\
                                        <label style='font-size: 30' for='owner'>"+owner+"</label>\
                                    </div>\
                                    <div class=' col-md-3'>\
                                        <label style='font-size: 30' for='boxnum'>"+"總箱數:</label>\
                                        <input style='font-size:20;height:40' id='totalboxnum' type='number' min =1 value ="+data.totalboxnum+">\
                                    </div>\
                                    </div>";
                                $.each(data.data, function(i, item) {
                                    if(firstid==""){
                                        firstid = item.id;
                                    }
                                    if(data.data.length==(i+1)){
                                        nextid =i;
                                    }else{
                                        nextid = data.data[i+1]['id'];
                                    }
                                    
                                    detailid.push(item.id);
                                    $display = 'style="display:inline"';
                                    if(item.sn_flag=="Y"){
                                        $display = 'style="display:inline"';
                                    } else {
                                        $display = 'style="display:none"';
                                    }
                                    if(item.sn_no==null){
                                        item.sn_no = "";    
                                    }
                                    if(item.goods_nm==null){
                                        item.goods_nm = "";    
                                    }
                                    if(item.pkg_num==null){
                                        item.pkg_num = 0;    
                                    }
                                    if(item.swipe_count==null){
                                        item.swipe_count = 0;
                                    }
                                    if(item.box_no==null){
                                        item.box_no = 1;
                                    }
                                    if(item.sn_count==null){
                                        item.sn_count = 0;
                                    }
                                    // 商品編號1 商品名稱 數量 已刷的數量 箱號 序號數量 
                                    // $( "li.third-item" ).next()
                                    if(item.pkg_num != item.swipe_count) {
                                        printflag ="N" ;
                                    } else {
                                        printflag ="Y" ;
                                    }
                                    if (item.sn_flag == "Y" ) {
                                        ordHtml = ordHtml+
                                        "<div class='row'><div class='col-md-2'><label style='font-size: 30' for='goodsNo'>\
                                        {{ trans('modOrderDetail.goodsNo') }}</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.goods_no+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='pkgNum'>\
                                        商品名稱</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.goods_nm+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                                        數量</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.pkg_num+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                                        已裝數量</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.swipe_count+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'"+$display+"><label style='font-size: 30' for='class'>\
                                        序號數量</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.sn_count+"'>"
                                        +"</div></div></div>"
                                        ;
                                    } else {
                                        ordHtml = ordHtml+
                                        "<div class='row'><div class='col-md-2'><label style='font-size: 30' for='goodsNo'>\
                                        {{ trans('modOrderDetail.goodsNo') }}</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.goods_no+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='pkgNum'>\
                                        商品名稱</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.goods_nm+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                                        數量</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.pkg_num+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                                        已裝數量</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.swipe_count+"'>"
                                        +"</div></div></div>";
                                    }
                                });
                                $("#order_model").append(ordHtml);
                            } else {
                                swal("查無此單號!", '查無此單號', "error");
                            }

                            // $("#ord_no").val("");
                            $("#sysordno").val(data.sysordno);
                            document.getElementById("clearbotask").style.display = "inline";
                            $("#goodsinput").focus();
                        });


                        swal.close();
                        $("#goodsinput").val('');
                        return false;   
                    } else {
                        alert(data.msg);
                        $('#hiddencolumn').val('');
                        $("#snnofocus").val(''),
                        $("#snnofocus").focus();
                        return false;
                    }
                }


        });
    } else {
            // $('#boxnonow').val(parseInt($('#boxnonow').val())+1);
            document.getElementById('boxnonow').disabled = true;// 變更欄位為禁用
            $('#hiddencolumn').val('');
            $('#hiddencolumn').focus();
            ischange ++;
            
            if(snflag == "Y") {
                if(document.getElementById('boxnonow').disabled == true) {
                return false;
            }

        }
    }
    return false;
}

function clearbotask() {
    id = mainid;
    ischange = 0;
    $.post(BASE_URL + '/clearbotask', {
        'id'   : id,
    }, function(data) {
        if(data.msg =='success') {
            opennew();
            // swal("完成", "", "success");
            $("#goodsinput").val('');
            $("#goodsinput").focus();
            return ; 
        }
    });

}

function printreporttest(type) {
    var now         = new Date();

    reporturl   = '';
    id          = [mainid];
    $reporttype = '';

    var totalboxnum = parseInt($("#totalboxnum").val());

    if($("#totalboxnum").val() == "" || $("#totalboxnum").val() =='0') {
        swal("總箱數有誤", '請確認總箱數', "error");
        return ;
    }
    if(totalboxnum <= 0 ) {
        swal("總箱數有誤", '請確認總箱數', "error");
        return ;
    }

    if(type == 'A' ) {
        reporturl = 'autosylreportlistnew';
    } else if( type == 'B') {
        reporturl = 'autosylreportlistbag';
    } else {
        reporturl = 'sylnewreportByowner';
    } 

    if( printflag == "N") {
        swal({
            title: '數量不符',
            text: "已刷數量與實際數量不符 確定要列印嗎?",
            icon: 'warning',
            confirmButtonText:"確認",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: '取消'
            }).then((result) => {
            if (result.value) {

                // swal("測試報表列印", "", "warning");
                $.post(BASE_URL + '/order/'+reporturl+'/downtest', {
                    'totalboxnum': totalboxnum,
                    'ids'        : id,
                    'order1'     : '',
                    'order2'     : '',
                    'order3'     : '',
                    'printflag'  : 'Y'
                }, function(data) {
                    if(data.filename =='error' || data.msg.length > 0) {
                        swal("報表異常請重新列印", "", "warning");
                        return ; 
                    }
                });

            } else {
                return ;
            }
        })
    } else {

        $.post(BASE_URL + '/order/'+reporturl+'/downtest', {
            'totalboxnum': totalboxnum,
            'ids'        : id,
            'order1'     : '',
            'order2'     : '',
            'order3'     : '',
            'printflag'  : 'Y'
        }, function(data) {
            if(data.filename =='error' || data.msg.length > 0) {
                swal("報表異常請重新列印", "", "warning");
                return ; 
            }
        });

    }
}
function printreport(type) {

    var now         = new Date();

    reporturl   = '';
    id          = [mainid];
    $reporttype = '';

    var totalboxnum = parseInt($("#totalboxnum").val());

    if($("#totalboxnum").val() == "" || $("#totalboxnum").val() =='0') {
        swal("總箱數有誤", '請確認總箱數', "error");
        return ;
    }
    if(totalboxnum <= 0 ) {
        swal("總箱數有誤", '請確認總箱數', "error");
        return ;
    }

    if(type == 'A' ) {
        reporturl = 'autosylreportlistnew';
    } else if( type == 'B') {
        reporturl = 'autosylreportlistbag';
    } else {
        reporturl = 'sylnewreportByowner';
    } 

    if( printflag == "N") {
        swal({
            title: '數量不符',
            text: "已刷數量與實際數量不符 確定要列印嗎?",
            icon: 'warning',
            confirmButtonText:"確認",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: '取消'
            }).then((result) => {
            if (result.value) {

                // swal("測試報表列印", "", "warning");
                $.post(BASE_URL + '/order/'+reporturl+'/down', {
                    'totalboxnum': totalboxnum,
                    'ids'        : id,
                    'order1'     : '',
                    'order2'     : '',
                    'order3'     : '',
                    'printflag'  : 'Y'
                }, function(data) {
                    if(data.filename =='error' || data.msg.length > 0) {
                        swal("報表異常請重新列印", "", "warning");
                        return ; 
                    }
                });

            } else {
                return ;
            }
        })
    } else {

        $.post(BASE_URL + '/order/'+reporturl+'/down', {
            'totalboxnum': totalboxnum,
            'ids'        : id,
            'order1'     : '',
            'order2'     : '',
            'order3'     : '',
            'printflag'  : 'Y'
        }, function(data) {
            if(data.filename =='error' || data.msg.length > 0) {
                swal("報表異常請重新列印", "", "warning");
                return ; 
            }
        });

    }
}



function swipeenter(event) {
    if( event.which == 13) {
        $('#hiddencolumn').focus();
    }
}

function hiddenenter(event) {
    var firstid    = "";
    var nextid     = "";
    var ordHtml    = "";
    var resulrHtml = "";
    detailid = new Array();
    printflag ="Y";
    console.log('hiddenenter');
    console.log(event.target.value);
    if( event.which == 13 && event.target.value =="00000") {
        console.log(event);
        var snno  = $("#snnofocus").val();
        if((snno == "" || snno == null) && continueflag=='N'   && snno !=undefined) {
            alert('序號為空');
            $('#hiddencolumn').val('');
            $('#hiddencolumn').focus();
            return ;
        }
        continueflag = 'N';
        boxno = $("#boxnonow").val();
        $.ajax({
                url: BASE_URL +'/saveboxdetail/',
                type: 'POST',
                    data:  { 
                        'sysordno'   : $("#sysordno").val(),
                        'orddetailid': orddetailid,
                        'goodsno'    : $("#goodsinput").val(),
                        'goodsnm'    : goodsnm,
                        'pkgnum'     : pkgnum,
                        'swipecount' : $("#swipecount").val(),
                        'boxno'      : boxno,
                        'snno'       : snno
                },
                async: false,
                beforeSend: function () {
                },
                error: function (jqXHR, exception) {
                },
                success: function (data) {
                    if(data.msg == "success" ) {
                        swal.close();
                        detailids[orddetailid]--;
                        $("#goodsinput").val('');

                        $("#order_model").html('');
                        var ordHtml    = "";
                        $.post(BASE_URL + '/order/checkforsn/', {'ord_no': $("#sysordno").val()}, function(data){
                            if(data.msg == "success") {
                                console.log(data);
                                owner     = data.owner;
                                ownercode = data.ownercode;
                                mainid    = data.mainid;

                                document.getElementById("etmailreport").style.display = "none";
                                document.getElementById("senaoreport").style.display = "none";
                                document.getElementById("weblinkreport").style.display = "none";
                                if (ownercode == '2074503') {
                                    document.getElementById("etmailreport").style.display = "inline";
                                } else if(ownercode == '2096600') {
                                    document.getElementById("senaoreport").style.display = "inline";
                                } else if(ownercode == '2201890') {
                                    document.getElementById("weblinkreport").style.display = "inline";
                                } else {
                                    document.getElementById("report").style.display = "inline";
                                    document.getElementById("bigreport").style.display = "inline";
                                }

                                if(data.totalboxnum == null) {
                                    data.totalboxnum = 1;
                                }
                                if(ischange >0 ) {
                                    data.totalboxnum = data.totalboxnum +1;
                                }

                                ordHtml="<div class='row'>\
                                    <div class=' col-md-3'>\
                                        <label style='font-size: 30' for='owner'>"+owner+"</label>\
                                    </div>\
                                    <div class=' col-md-3'>\
                                        <label style='font-size: 30' for='boxnum'>"+"總箱數:</label>\
                                        <input style='font-size:20;height:40' id='totalboxnum' type='number' min =1 value ="+data.totalboxnum+">\
                                    </div>\
                                    </div>";
                                $.each(data.data, function(i, item) {
                                    if(firstid==""){
                                        firstid = item.id;
                                    }
                                    if(data.data.length==(i+1)){
                                        nextid =i;
                                    }else{
                                        nextid = data.data[i+1]['id'];
                                    }
                                    
                                    detailid.push(item.id);
                                    $display = 'style="display:inline"';
                                    if(item.sn_flag=="Y"){
                                        $display = 'style="display:inline"';
                                    } else {
                                        $display = 'style="display:none"';
                                    }
                                    if(item.sn_no==null){
                                        item.sn_no = "";    
                                    }
                                    if(item.goods_nm==null){
                                        item.goods_nm = "";    
                                    }
                                    if(item.pkg_num==null){
                                        item.pkg_num = 0;    
                                    }
                                    if(item.swipe_count==null){
                                        item.swipe_count = 0;
                                    }
                                    if(item.box_no==null){
                                        item.box_no = 1;
                                    }
                                    if(item.sn_count==null){
                                        item.sn_count = 0;
                                    }
                                    // 商品編號1 商品名稱 數量 已刷的數量 箱號 序號數量 
                                    // $( "li.third-item" ).next()
                                    if(item.pkg_num != item.swipe_count) {
                                        printflag ="N" ;
                                    } else {
                                        printflag ="Y" ;
                                    }
                                    if (item.sn_flag == "Y" ) {
                                        ordHtml = ordHtml+
                                        "<div class='row'><div class='col-md-2'><label style='font-size: 30' for='goodsNo'>\
                                        {{ trans('modOrderDetail.goodsNo') }}</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.goods_no+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='pkgNum'>\
                                        商品名稱</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.goods_nm+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                                        數量</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.pkg_num+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                                        已裝數量</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.swipe_count+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'"+$display+"><label style='font-size: 30' for='class'>\
                                        序號數量</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.sn_count+"'>"
                                        +"</div></div></div>"
                                        ;
                                    } else {
                                        ordHtml = ordHtml+
                                        "<div class='row'><div class='col-md-2'><label style='font-size: 30' for='goodsNo'>\
                                        {{ trans('modOrderDetail.goodsNo') }}</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.goods_no+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='pkgNum'>\
                                        商品名稱</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.goods_nm+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                                        數量</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.pkg_num+"'>"
                                        +"</div>"+

                                        "<div class='col-md-2'><label style='font-size: 30' for='swipeCount'>\
                                        已裝數量</label>\
                                        <input type='text' readonly='readonly' class='form-control input-sm'"+
                                        "value='"+ item.swipe_count+"'>"
                                        +"</div></div></div>";
                                    }
                                });
                                $("#order_model").append(ordHtml);
                            } else {
                                swal("查無此單號!", '查無此單號', "error");
                            }

                            // $("#ord_no").val("");
                            $("#sysordno").val(data.sysordno);
                            document.getElementById("clearbotask").style.display = "inline";
                            $("#goodsinput").focus();
                        });
                    } else {
                        alert(data.msg);
                        $('#hiddencolumn').val('');
                        $("#snnofocus").val(''),
                        $("#snnofocus").focus();
                    }


                    return;
                }
        });
    } else if ( event.which == 13 && event.target.value == "99999") {
        // $('#boxnonow').val(parseInt($('#boxnonow').val())+1);
        document.getElementById('boxnonow').disabled=true;// 變更欄位為禁用
        $('#hiddencolumn').val('');
        $('#hiddencolumn').focus();
        ischange ++;
    } else if ( event.which == 13 && event.target.value != "99999") {
        $('#hiddencolumn').val('');
        $('#hiddencolumn').focus();
    }
}

</script>
@endsection