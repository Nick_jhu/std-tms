@extends('backpack::layout')
@section('header')
<section class="content-header">
    <h1>
        {{ trans('modOrder.titleNamenew') }}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">{{ trans('modOrder.titleNamenew') }}</li>
    </ol>
</section>
<div id="searchWindow" style="overflow-x:hidden; display:none">
    <div>搜尋</div>
    <div id="searchBody">
        <div id="searchContent">

        </div>
        <div class="row" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;" id="searchFooter">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="請輸入名稱" name="searchName">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch">儲存</button>
                <button class="btn btn-sm btn-warning" name="setDefault">預設</button>
                <button class="btn btn-sm btn-danger" name="delSearch">刪除</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn">搜尋</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd">新增</button>
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('before_scripts')

<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js?v={{Config::get('app.version')}}"></script>
<script>
    var gridOpt = {};
    var filtervalue = "";
    var filterfield = "";
    var filtertype  = "";
    var enabledheader = [] ;
    var filtercolumndata = [];
    gridOpt.pageId          = "modOrder";
    gridOpt.enabledStatus   = true;
    gridOpt.fieldsUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order') }}";
    gridOpt.dataUrl         = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order') }}";
    gridOpt.fieldsUrl       = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmttest/create') }}";
    gridOpt.editUrl         = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmttest') }}" + "/{id}/edit";
    gridOpt.height          = 800;
    gridOpt.selectionmode   = "checkbox";
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick  = true;
    gridOpt.searchOpt       = true;
    gridOpt.getStatusCount  = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getStatusCount/mod_order') }}"
    var btnGroup = [
        {
            btnId: "btnSearchWindow",
            btnIcon: "fa fa-search",
            btnText: "{{ trans('common.search') }}",
            btnFunc: function () {
                $('#searchWindow').jqxWindow('open');
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
                var url  =BASE_URL;
                url = url.replace("admin","");
                var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
                filtercolumndata = [];
                for (var i = 0; i < filterGroups.length; i++) {
                    var filterGroup = filterGroups[i];
                    var filters = filterGroup.filter.getfilters();
                        for (var k = 0; k < filters.length; k++) {
                            if(filters[k].condition!="CONTAINS" && filters[k].condition!="NOT_EQUAL" ){
                            var filtercolumn = {
                            'column': filterGroup.filtercolumn,
                            'value': Date.parse(filters[k].value),
                            'condition': filters[k].condition
                        };
                        }else{
                            var filtercolumn = {
                            'column': filterGroup.filtercolumn,
                            'value': filters[k].value,
                            'condition': filters[k].condition
                        };
                        }
                        filtercolumndata.push(filtercolumn);
                    }
                }
                $.post(BASE_API_URL + '/admin/export/data', {
                    'table': 'mod_order',
                    'filename':'訂單作業' ,
                    'columndata': filtercolumndata,
                    'header': enabledheader,
                }, function(data){

                    if(data.msg == "success") {
                        window.location.href =url+"storage/excel/"+data.downlink;
                    }

                });
            }
        },
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        @can('OrdControl')
        {
            btnId: "btnAdd",
            btnIcon: "fa fa-edit",
            btnText: "{{ trans('common.add') }}",
            btnFunc: function () {
            location.href = gridOpt.createUrl;
            }
        },
        @endcan
        @can('OrdDel')
        {
            btnId:"btnDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"{{ trans('common.delete') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                swal({
                    title: "確認視窗",
                    text: "您確定要刪除嗎？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: "確定",
                    cancelButtonText: "取消"
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + '/order/multi/del', {'ids': ids}, function(data){
                            if(data.msg == "success") {
                                swal("刪除成功", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                                $("#jqxGrid").jqxGrid('clearselection');
                            }
                            else{
                                swal("操作失敗", data.errorMsg, "error");
                            }
                        });
                    
                    } else {
                    
                    }
                });
                
            }
        },
        @endcan
        @can('sendTrackingBtn')
        {
            btnId: "btnSendStatus",
            btnIcon: "fa fa-newspaper-o",
            btnText: "{{ trans('modOrder.sendCargo') }}",
            btnFunc: function () {
            var rowid = $("#jqxGrid").jqxGrid('getselectedrowindex');
            if(rowid == -1) {
                swal("{{ trans('modOrder.sendCargoWarning') }}", "", "warning");
                return;
            }


            $.get("{{ url(config('backpack.base.route_prefix', 'admin') . '/order/getTsData') }}", {}, function(data){
                var ttl = '';
                $.each(data, function(i, v){
                var p = '', ul = '', li = '';
                p  += '<h4>'+data[i]["ts_type"]+'</h4>';
                ul += '<ul class="list-inline">';
                
                $.each(data[i]["status"], function(k, vv) {
                    li += '<li><button type="button" class="btn bg-purple btn-flat margin tsBtn" tsid="'+data[i]["status"][k]["id"]+'">'+data[i]["status"][k]["ts_name"]+'</button></li>';
                });
                ul += li;
                ul += '</ul>';

                ttl += p + ul;
                });

                $("#ts-body").html(ttl);

                $(".tsBtn").unbind( "click" ).on("click", function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var selectedRecords = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        selectedRecords.push({tsid: $(this).attr("tsid"), sys_ord_no: row.sys_ord_no});
                    }
                }

                $.ajax({
                    url: "{{ url(config('backpack.base.route_prefix', 'admin').'/trackingApi/insertTracking') }}",
                    type: 'POST',
                    async: false,
                    data: {data: JSON.stringify(selectedRecords)},
                    beforeSend: function () {
                    loadingFunc.show();
                    },
                    error: function (jqXHR, exception) {
                    swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    loadingFunc.hide();
                    return;
                    },
                    success: function (data) {
                    if(data.msg == "success") {
                        swal('Success', "{{ trans('modOrder.sendSuccess') }}", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $("#jqxGrid").jqxGrid('clearselection');
                    }
                    else {
                        swal('Oops...', "{{ trans('modOrder.error') }}", "error");

                    }
                    loadingFunc.hide();
                    return;
                    }
                });
                });

                $("#ts-modal").modal("show");
            }, 'JSON');
            }
        },
        @endcan
        @can('OrdControl')
        {
            btnId:"btnFail",
            btnIcon:"fa fa-ban",
            btnText:"{{ trans('common.fail') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                    ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                swal({
                        title: "確認視窗",
                        text: "您確定要作廢嗎？",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#dc3545',
                        cancelButtonColor: '#6c757d',
                        confirmButtonText: "確定",
                        cancelButtonText: "取消"
                    }).then((result) => {
                        if (result.value) {
                            $.post(BASE_URL + '/order/fail', {'ids': ids}, function(data){
                                if(data.msg == "success") {
                                swal("作廢完成", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                                $("#jqxGrid").jqxGrid('clearselection');
                                }
                                else{
                                swal("操作失敗", data.errorMsg, "error");
                                }
                            });
                        
                        } else {
                        
                        }
                });
            }
        },
        @endcan
        @can('ShowBtnReportsylnew')
        {
            btnId:"btnReport9",
            btnIcon:"fa fa-file",
            btnText:"面單",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                (async function getFormValues () {
                const {value: formValues} = await swal({
                title: '請選擇排序',
                // 貨主 訂單號碼 站所 配送客戶 配送聯絡人 配送地址
                html:
                    "<span>排序1<span>"+
                    '<select class="form-control" id="swal-input1" >' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>'+
                    "<br>"+
                    "<span>排序2<span>"+
                    '<select class="form-control" id="swal-input2" >' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>'+
                    "<br>"+
                    "<span>排序3<span>"+
                    '<select class="form-control" id="swal-input3">' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>',
                focusConfirm: false,
                focusConfirm: false,
                showCancelButton: true,
                cancelButtonText:  '取消',
                confirmButtonText: '確定',
                preConfirm: () => {
                    return [
                    document.getElementById('swal-input1').value,
                    document.getElementById('swal-input2').value,
                    document.getElementById('swal-input3').value
                    ]
                }
                })
                if (formValues) {
                    var order1 = document.getElementById('swal-input1').value;
                    var order2 = document.getElementById('swal-input2').value;
                    var order3 = document.getElementById('swal-input3').value;
                    var now = new Date();
                    $.post(BASE_URL + '/order/sylreportlistnew/down', {'ids': ids,'order1': order1,'order2': order2,'order3': order3}, function(data){
                        if(data.filename =='error' || data.msg.length > 0){
                            swal("報表異常請重新列印", "", "warning");
                            return ; 
                        }
                        var date_now = now.format("yyyyMMddhhmmss");
                        var link = document.createElement('a');
                            link.id = 'downurl6';
                            link.href = BASE_URL+"report/"+data.filename+".pdf";
                            link.download = "面單"+date_now+".pdf";
                            console.log(link);
                            document.body.appendChild(link);
                            document.getElementById('downurl6').click();
                            document.getElementById('downurl6').remove();
                    });
                    $('#jqxGrid').jqxGrid('clearselection');
                    }
                })()

            }
        },
        @endcan
        {
            btnId:"btnReport9p",
            btnIcon:"fa fa-file",
            btnText:"面單鎖個資",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                (async function getFormValues () {
                const {value: formValues} = await swal({
                title: '請選擇排序',
                // 貨主 訂單號碼 站所 配送客戶 配送聯絡人 配送地址
                html:
                    "<span>排序1<span>"+
                    '<select class="form-control" id="swal-input1" >' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>'+
                    "<br>"+
                    "<span>排序2<span>"+
                    '<select class="form-control" id="swal-input2" >' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>'+
                    "<br>"+
                    "<span>排序3<span>"+
                    '<select class="form-control" id="swal-input3">' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>',
                focusConfirm: false,
                focusConfirm: false,
                showCancelButton: true,
                cancelButtonText:  '取消',
                confirmButtonText: '確定',
                preConfirm: () => {
                    return [
                    document.getElementById('swal-input1').value,
                    document.getElementById('swal-input2').value,
                    document.getElementById('swal-input3').value
                    ]
                }
                })
                if (formValues) {
                    var order1 = document.getElementById('swal-input1').value;
                    var order2 = document.getElementById('swal-input2').value;
                    var order3 = document.getElementById('swal-input3').value;
                    var now = new Date();
                    $.post(BASE_URL + '/order/sylreportprivate/down', {'ids': ids,'order1': order1,'order2': order2,'order3': order3}, function(data){
                        if(data.filename =='error' || data.msg.length > 0){
                            swal("報表異常請重新列印", "", "warning");
                            return ; 
                        }
                        var date_now = now.format("yyyyMMddhhmmss");
                        var link = document.createElement('a');
                            link.id = 'downurl6';
                            link.href = BASE_URL+"report/"+data.filename+".pdf";
                            link.download = "面單"+date_now+".pdf";
                            console.log(link);
                            document.body.appendChild(link);
                            document.getElementById('downurl6').click();
                            document.getElementById('downurl6').remove();
                    });
                    $('#jqxGrid').jqxGrid('clearselection');
                    }
                })()

            }
        },
        @can('ShowBtnReportsylbig')
        {
            btnId:"btnReport9b",
            btnIcon:"fa fa-file",
            btnText:"大面單",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                (async function getFormValues () {
                const {value: formValues} = await swal({
                title: '請選擇排序',
                // 貨主 訂單號碼 站所 配送客戶 配送聯絡人 配送地址
                html:
                    "<span>排序1<span>"+
                    '<select class="form-control" id="swal-input4" >' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>'+
                    "<br>"+
                    "<span>排序2<span>"+
                    '<select class="form-control" id="swal-input5" >' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>'+
                    "<br>"+
                    "<span>排序3<span>"+
                    '<select class="form-control" id="swal-input6">' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>',
                focusConfirm: false,
                focusConfirm: false,
                showCancelButton: true,
                cancelButtonText:  '取消',
                confirmButtonText: '確定',
                preConfirm: () => {
                    return [
                    document.getElementById('swal-input4').value,
                    document.getElementById('swal-input5').value,
                    document.getElementById('swal-input6').value
                    ]
                }
                })
                if (formValues) {
                    var order1 = document.getElementById('swal-input4').value;
                    var order2 = document.getElementById('swal-input5').value;
                    var order3 = document.getElementById('swal-input6').value;
                    var now = new Date();
                    $.post(BASE_URL + '/order/sylreportlistbag/down', {'ids': ids,'order1': order1,'order2': order2,'order3': order3}, function(data){
                        if(data.filename =='error' || data.msg.length > 0){
                            swal("報表異常請重新列印", "", "warning");
                            return ; 
                        }
                        var date_now = now.format("yyyyMMddhhmmss");
                        var link = document.createElement('a');
                            link.id = 'downurlbag';
                            link.href = BASE_URL+"report/"+data.filename+".pdf";
                            link.download = "大面單"+date_now+".pdf";
                            console.log(link);
                            document.body.appendChild(link);
                            document.getElementById('downurlbag').click();
                            document.getElementById('downurlbag').remove();
                    });
                    $('#jqxGrid').jqxGrid('clearselection');
                    }
                })()
            }
        },
        @endcan
        @can('ShowBtnReportsylnew')
        {
            btnId:"btnReportx",
            btnIcon:"fa fa-file",
            btnText:"提配單",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                (async function getFormValues () {
                const {value: formValues} = await swal({
                title: '請選擇排序',
                // 貨主 訂單號碼 站所 配送客戶 配送聯絡人 配送地址
                html:
                    "<span>排序1<span>"+
                    '<select class="form-control" id="swal-input1" >' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>'+
                    "<br>"+
                    "<span>排序2<span>"+
                    '<select class="form-control" id="swal-input2" >' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>'+
                    "<br>"+
                    "<span>排序3<span>"+
                    '<select class="form-control" id="swal-input3">' +
                        "<option value=''>"+"無"+"</option>"+
                        "<option value='owner_cd'>"+"貨主"+"</option>"+
                        "<option value='ord_no'>"+"訂單號"+"</option>"+
                        "<option value='s_key'>"+"站所"+"</option>"+
                        "<option value='dlv_cust_no'>"+"配送客戶"+"</option>"+
                        "<option value='dlv_attn'>"+"配送聯絡人"+"</option>"+
                        "<option value='dlv_addr'>"+"配送地址"+"</option>"+
                    '</select>',
                focusConfirm: false,
                focusConfirm: false,
                showCancelButton: true,
                cancelButtonText:  '取消',
                confirmButtonText: '確定',
                preConfirm: () => {
                    return [
                    document.getElementById('swal-input1').value,
                    document.getElementById('swal-input2').value,
                    document.getElementById('swal-input3').value
                    ]
                }
                })

                if (formValues) {
                    var order1 = document.getElementById('swal-input1').value;
                    var order2 = document.getElementById('swal-input2').value;
                    var order3 = document.getElementById('swal-input3').value;
                    var now = new Date();
                    $.post(BASE_URL + '/order/sylreportnew/down', {'ids': ids,'order1': order1,'order2': order2,'order3': order3}, function(data){
                        if(data.filename =='error' || data.msg.length > 0){
                            swal("報表異常請重新列印", "", "warning");
                            return ; 
                        }

                        var date_now = now.format("yyyyMMddhhmmss");
                        var link = document.createElement('a');
                            link.id = 'downurlx';
                            link.href = BASE_URL+"report/"+data.filename+".pdf";
                            link.download = "提配單"+date_now+".pdf";
                            console.log(link);
                            document.body.appendChild(link);
                            document.getElementById('downurlx').click();
                            document.getElementById('downurlx').remove();
                            window.open(link.href);
                    });
                    $('#jqxGrid').jqxGrid('clearselection');
                    }
                })()
                // win1.location.reload();
            }
        },
        @endcan
        @can('ShowBtnReport')
        {
            btnId:"btnReport1",
            btnIcon:"fa fa-file",
            btnText:"{{ trans('common.btnReport1') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                var id_unc01 = new Array();
                var id_c01 = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        console.log(row.owner_nm);
                        ids.push(row.id);
                        if(row.owner_nm=='有德機器'||row.owner_nm=='有德機器股份有限公司'){
                            id_c01.push(row.id);
                        }
                        else{
                            id_unc01.push(row.id);
                        }
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                if(id_unc01.length>0){
                    var now = new Date();
                    var date_now = now.format("yyyyMMddhhmmss");
                    console.log(date_now);
                    window.location.href =('http://172.105.207.193:8855/WebReport/ReportServer?reportlet=out.cpt&format=pdf&__filename__='+'出倉報表'+date_now+'&id=' + id_unc01.join(','));
                    console.log("report1");
                    // win.location.reload();
                }
                if(id_c01.length>0){
                    var now = new Date();
                    var date_now = now.format("yyyyMMddhhmmss");
                    window.location.href =('http://172.105.207.193:8855/WebReport/ReportServer?reportlet=out_sp.cpt&format=pdf&__filename__='+'出倉報表(有德)'+date_now+'&id=' + id_c01.join(','));
                    console.log("report2");
                    // win2.location.reload();
                };
                
            }
        },
        {
            btnId:"btnReport2",
            btnIcon:"fa fa-file",
            btnText:"{{ trans('common.btnReport2') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                var now = new Date();
                var date_now = now.format("yyyyMMddhhmmss");
                window.location.href = ('http://172.105.207.193:8855/WebReport/ReportServer?reportlet=dlv.cpt&format=pdf&__filename__='+'配送單'+date_now+'&id=' + ids.join(','));
                // win1.location.reload();
                
            }
        },
        @endcan
        /*{
            btnId:"btnClose",
            btnIcon:"fa fa-window-close-o",
            btnText:"{{ trans('common.close') }}",
            btnFunc:function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                ids.push(row.id);
                }
            }
            if(ids.length == 0) {
                swal("請至少選擇一筆資料", "", "warning");
                return;
            }
            $.post(BASE_URL + '/order/close', {'ids': ids}, function(data){
                if(data.msg == "success") {
                swal("完成", "", "success");
                $("#jqxGrid").jqxGrid('updatebounddata');
                }
                else{
                swal("操作失敗", "", "error");
                }
            });
            }
        },*/
        @can('ReCal')
        {
            btnId:"btnReCal",
            btnIcon:"fa fa-usd",
            btnText:"{{ trans('modOrder.btnReCal') }}",
            btnFunc:function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                ids.push(row.id);
                }
            }
            if(ids.length == 0) {
                swal("請至少選擇一筆資料", "", "warning");
                return;
            }
            $.post(BASE_URL + '/order/reCalculateFee', {'ids': ids}, function(data){
                if(data.msg == "success") {
                    swal("計算完成", "", "success");
                    $("#jqxGrid").jqxGrid('updatebounddata');
                    $("#jqxGrid").jqxGrid('clearselection');
                }
                else{
                    var msg = (typeof data.showMsg == 'undefined')?data.showMsg:'';
                    swal("操作失敗", msg, "error");
                }
            });
            }
        },
        @endcan
        @can('CancelError')
        {
            btnId: "btnCancelError",
            btnIcon: "fa fa-exclamation-circle",
            btnText: "{{ trans('modOrder.btnRevertUnNormal') }}",
            btnFunc: function () {
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                var statuss = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                    ids.push(row.id);
                    }
                    if(row.status != "ERROR") {
                    statuss.push(row.id);
                    }
                }
                if(statuss.length > 0) {
                    swal("配送發生問題才可以解除異常", "", "warning");
                    return;
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                $.post(BASE_URL + '/order/cancelError', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("完成", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $("#jqxGrid").jqxGrid('clearselection');
                    }
                    else{
                        var msg = (typeof data.showMsg == 'undefined')?data.showMsg:'';
                        swal("操作失敗", msg, "error");
                    }
                });
            }
        },
        @endcan
        @can('OrderMgmt_ImgDownload')
        {
            btnId: "btnDownLoadImg",
            btnIcon: "fa fa-file-image-o",
            btnText: "{{ trans('modOrder.btnDownloadPicture') }}",
            btnFunc: function () {
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.sys_ord_no);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                var strSysOrdNo = ids.join();

                window.open(BASE_URL + "/img/get?ids=" + strSysOrdNo);
            }
        },
        @endcan
        @can('OrderMgmt_CompulsiveDelete')
        {
            btnId:"btnCompulsiveDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"{{ trans('modOrder.btnDelete') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                swal({
                    title: "確認視窗",
                    text: "您確定要刪除嗎？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: "確定",
                    cancelButtonText: "取消"
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + '/order/compulsiveDel/del', {'ids': ids}, function(data){
                            if(data.msg == "success") {
                                swal("刪除成功", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                                $("#jqxGrid").jqxGrid('clearselection');
                            }
                            else{
                                swal("操作失敗", data.errorMsg, "error");
                            }
                        });
                    
                    } else {
                    
                    }
                });
                
            }
        },
        @endcan
        @can('OrderMgmt_sendedi')
        {
            btnId:"btnSendEDI",
            btnIcon: "fa fa-cloud-upload",
            btnText:"{{ trans('modOrder.btnEDI') }}",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                $.post(BASE_URL + '/order/sendedi', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("{{ trans('modOrder.sendSuccess') }}", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $("#jqxGrid").jqxGrid('clearselection');
                    }
                    else{
                        swal("{{ trans('modOrder.error') }}", data.errorMsg, "error");
                    }
                });
                
            }
        },
        @endcan
        @can('Chstation')
        {
            btnId:"btnChstation",
            btnIcon: "fa fa-cloud-upload",
            btnText:"轉站",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                var statusCode = [
                    {value: "UNTREATED", label: "{{ trans('modDlvPlan.STATUS_UNTREATED') }}"},
                    {value: "SEND", label: "{{ trans('modDlvPlan.STATUS_SEND') }}"},
                    {value: "DISPATCHED", label: "已派單"},
                    {value: "DLV", label: "{{ trans('modDlvPlan.STATUS_DLV') }}"},
                    {value: "FINISHED", label: "{{ trans('modDlvPlan.STATUS_FINISHED') }}"},
                    {value: "ERROR", label: "{{ trans('modDlvPlan.STATUS_ERROR') }}"},
                ];

                var statusSource =
                {
                        datatype: "array",
                        datafields: [
                            { name: 'label', type: 'string' },
                            { name: 'value', type: 'string' }
                        ],
                        localdata: statusCode
                };
                var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'cust_no', type: 'string' },
                        { name: 'cmp_abbr', type: 'string' }                              
                    ],
                    root:"Rows",
                    pagenum: 1,					
                    beforeprocessing: function (data) {
                    },
                    pagesize: 200,
                    cache: false,
                    url: BASE_URL + '/TranPlanMgmt/getStantiondata',
                    // sortcolumn: 'ord_no',
                    // sortdirection: 'asc'
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $('#skeyGrid').jqxGrid('destroy');
                $("#skeyBody").html('<div id="skeyGrid"></div>');
                $("#skeyGrid").jqxGrid(
                {
                    width: '100%',
                    //height: '100%',
                    autoheight: true,
                    source: dataAdapter,
                    sortable: true,
                    filterable: true,
                    altrows: true,
                    selectionmode: 'singlerow',
                    showfilterrow: true,
                    columnsresize: true,
                    ready: function () {
                        $('#skeyGrid').jqxGrid('autoresizecolumns');
                    },
                    columns: [
                        { text: '站別', datafield: 'cust_no', width: 300 },
                        { text: '名稱', datafield: 'cmp_abbr', width: 300 },
                    ]
                });
                $("#skeyModal").modal('show');
            }
        },
        @endcan
        @can('CloseOrder')
        {
            btnId:"btnclose",
            btnIcon: "fa fa-check-square-o",
            btnText:"結案",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        if(row.status !="FINISHED" && row.status !="ERROR" && row.status !="REJECT" && row.status !="FAIL"){
                            swal("只有「配送完成」「 配送發生問題」 「拒收」 「作廢」訂單 才可進行結案!", "", "warning");
                            return;
                        }else{
                            ids.push(row.id);
                        }
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                swal({
                    title: "確認視窗",
                    text: "您確定要結案嗎？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: "確定",
                    cancelButtonText: "取消"
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + '/order/multi/close', {'ids': ids}, function(data){
                            if(data.msg == "success") {
                                swal("操作成功", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                                $("#jqxGrid").jqxGrid('clearselection');
                            }
                            else{
                                swal("操作失敗", data.errorMsg, "error");
                            }
                        });
                    
                    } else {
                    
                    }
                });
                
            }
        },
        @endcan
		@can('sendbenqxml')
        {
            btnId:"btnexportxml",
            btnIcon:"fa fa-table",
            btnText:"發送BENQxml",
            btnFunc:function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            var notwo = "";
            for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        if(row.owner_cd == "019"){
                            ids.push(row.id);
                        }else{
                            swal("只有benq資料可選擇", "", "warning");
                            return;
                        }
                    }
                }
            if(ids.length == 0) {
                swal("請至少選擇一筆資料", "", "warning");
                return;
            }
            if(ids.length > 1) {
                swal("請最多選擇一筆資料", "", "warning");
                return;
            }
            $.post(BASE_URL + '/exportxml', {'ids': ids}, function(data){
                if(data.msg == "success") {
                // swal("操作成功", "", "success");
                var result="";
                var result =  data.xmlformat;
                swal({
                        title: '是否要發送XML',
                        input: 'textarea',
                        inputValue : result,
                        inputAttributes: {
                            autocapitalize: 'off',
                            input: 'textarea',
                        },
                        showCancelButton: true,
                        cancelButtonText:  '取消',
                        confirmButtonText: '確定',
                        showLoaderOnConfirm: true,
                        preConfirm: (amt) => {
                            
                            $.post(BASE_URL + '/suresendxml', {'ids': ids}, function(data){
                                swal("操作成功", "", "success");
                            });

                        }}) 
                }else{
                    swal("並無相關api資料", "", "error");
                }
            });

            }
        },
        @endcan
		@can('errorchange')
        {
            btnId:"btnerrorchange",
            btnIcon:"fa fa-newspaper-o",
            btnText:"批次換車運送",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                swal({
                    title: "確認視窗",
                    text: "您確定要換車嗎？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: "確定",
                    cancelButtonText: "取消"
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + '/order/error/changecar', {'ids': ids}, function(data){
                            if(data.msg == "success") {
                                swal("操作成功", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                                $("#jqxGrid").jqxGrid('clearselection');
                            }
                            else{
                                swal("操作失敗", data.errorMsg, "error");
                            }
                        });
                    
                    } else {
                    
                    }
                });
                
            }
        },
        @endcan
		@can('Mistakecomplete')
        {
            btnId: "btnMistakecomplete",
            btnIcon: "fa fa-exclamation-circle",
            btnText: "解除誤刷完成",
            btnFunc: function () {
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                var statuss = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                    ids.push(row.id);
                    }
                    if(row.status != "ERROR") {
                    statuss.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                $.post(BASE_URL + '/order/mistakecomplete', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("完成", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $("#jqxGrid").jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", data.errorMsg, "error");
                    }
                });
            }
        },
        @endcan
        @can('mpushMessage')
        {
            btnId:"btnMessage",
            btnIcon: "fa fa-cloud-upload",
            btnText:"發送簡訊",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                var o_price = 0;
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                (async function getFormValues () {
                    
                // <select class="form-control" id="swal-input1" name="status">
                //     <option value="UNTREATED">{{ trans('modOrder.STATUS_UNTREATED') }}</option>
                // </select>
                var optinon = "";
                var first = "";
                @foreach($viewData['msgdata'] as $row)
                    if(first==""){
                        first = "{{$row->value1}}";
                    }
                   optinon =optinon+"<option value='{{$row->value1}}'>"+"{{$row->cd_descp}}"+"</option>"
                @endforeach
                const {value: formValues} = await swal({
                title: '簡訊',
                html:
                    '<select class="form-control" id="swal-input1" name="status" onchange="myFunctionchange()">' +
                        optinon+
                    '</select>'+
                    '<textarea id="swal-input2" class="swal2-input" placeholder="請輸入內容" maxlength="60" style="height:200px;">'+first+"</textarea>"+
                    "<span style='color:red;'>字數限制60個字<span>",
                focusConfirm: false,
                focusConfirm: false,
                showCancelButton: true,
                cancelButtonText:  '取消',
                confirmButtonText: '發送',
                preConfirm: () => {
                    return [
                    document.getElementById('swal-input1').value,
                    document.getElementById('swal-input2').value
                    ]
                }
                })
                if (formValues) {
                    var title = document.getElementById('swal-input1').value;
                    var content = document.getElementById('swal-input2').value;
                    $.post(BASE_URL + "/OrderMgmt/sendMessage", {cd: content, ids: ids}, function(data){
                        if(data.msg == "success") {
                            swal("完成", "", "success");
                            $('#jqxGrid').jqxGrid('updatebounddata');
                            $("#jqxGrid").jqxGrid('clearselection');
                        }
                    });
                }
                })()

            }
        },
        @endcan
        @can('exportfeeexcel')
        {
            btnId:"btnexportfeeexcel",
            btnIcon:"fa fa-cloud-download",
            btnText:"服務項目匯出",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                var url  =BASE_URL;
                url = url.replace("admin","");
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                    ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                }

                $.post(BASE_URL + '/fee/export', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        window.location.href =url+"storage/excel/"+data.downlink;
                    }
                });
            }
        },
        @endcan

        @can('CHANGEREADY')
        {
            btnId:"btnchangeready",
            btnIcon: "fa fa-check-square-o",
            btnText:"可派車",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                var url  =BASE_URL;
                url = url.replace("admin","");
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                    ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                }

                $.post(BASE_URL + '/OrderMgmt/changeReady', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $("#jqxGrid").jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", data.errorMsg, "error");
                    }
                });
            }
        },
        @endcan
    ];

    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });
    // var loadURL = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/getLayoutJson') }}";					
    //     $.ajax({
    //         type: "GET", //  OR POST WHATEVER...
    //         url: loadURL,
    //         data: { key: 'excelOrderGrid' },		 
    //         success: function(response) {										
    //             if (response != "") {	
    //                 response = JSON.parse(response);
    //             }
    //             var listSource = [];
    //             state = $("#jqxGrid").jqxGrid('getstate');
    //             $.each(state.columns, function(i, item) {
    //                 if(item.text != "" && item.text != "undefined"){
    //                     listSource.push({ 
    //                     label: item.text, 
    //                     value: i, 
    //                     checked: !item.hidden });
    //                 }
    //             });
    //             $("#jqxlistbox").jqxListBox({ 
    //                 allowDrop: true, 
    //                 allowDrag: true,
    //                 source: listSource,
    //                 width: "99%",
    //                 height: 500,
    //                 checkboxes: true,
    //                 filterable: true,
    //                 searchMode: 'contains'
    //             });
    //         }
    //     });	
</script>
@endsection

@section('content')
<div id="jqxLoader">
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary" id="statusDiv">
			<div class="box-header with-border">
				勾選筆數：<span id="selectcount"></span>&nbsp;&nbsp;&nbsp; <h3 class="box-title">Status </h3><strong style="color:red;">(狀態「配送完成」及「ALL」的按鈕需在下方輸入至少一個查詢條件後才可顯示資料)</strong>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body no-padding">
                <p></p>
				<div style="width:100%;" id="statusList">
                    
                </div>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="button-group">
                    <div class="row" id="btnArea"></div>
                    <div id="jqxGrid"></div>
                    <input type="button" style="display:none" id="updategridsign" >
                    <input type="button" style="display:none" id="updategriderror" >
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<div class="row">
	<div class="col-md-2">
		{{--
		<div class="box box-primary" id="statusDiv">
			<div class="box-header with-border">
				<h3 class="box-title">Status</h3>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked" id="statusList">
				</ul>
			</div>
			<!-- /.box-body -->
		</div> --}} {{--
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Grid Option</h3>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
						<button type="button" class="btn btn-box-tool" id="saveGrid">
							<i class="fa fa-floppy-o" aria-hidden="true"></i>
						</button>
				</div>
			</div>
			<div class="box-body no-padding">
				<div id="jqxlistbox"></div>
			</div>
			<!-- /.box-body -->
		</div> --}}
		<!-- /. box -->
	</div>
	<div class="col-md-12">
		<div class="box box-primary">
			<!-- /.box-header -->
			<div class="box-body">
				<div class="button-group">
					<div class="row" id="btnArea">

					</div>
					<div id="jqxGrid"></div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="skeyModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">站別</h4>
			</div>
			<div class="modal-body" id="skeyBody">
				<div id="skeyGrid"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-danger" id="chStation">確定修改</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
@endsection

@section('after_scripts')
<script>
    var theme = 'bootstrap';
    $.jqx.theme = theme;
    gridOpt.gridId = "jqxGrid";
    loadState();
    gridOpt.getState = function(){
        loadState();
    };
    var fieldData = null;
    var fieldObj = null;
    var source = {
        datatype: "json",
        datafields: [
            {
                "name": "id",
                "type": "number"
            },
            {
                "name": "g_key",
                "type": "string"
            },
            {
                "name": "c_key",
                "type": "string"
            },
            {
                "name": "s_key",
                "type": "string"
            },
            {
                "name": "d_key",
                "type": "string"
            },
            {
                "name": "ord_no",
                "type": "string"
            },
            {
                "name": "status",
                "type": "string"
            },
            {
                "name": "status_desc",
                "type": "string"
            },
            {
                "name": "remark",
                "type": "string"
            },
            {
                "name": "dlv_cust_nm",
                "type": "string"
            },
            {
                "name": "dlv_addr",
                "type": "string"
            },
            {
                "name": "dlv_attn",
                "type": "string"
            },
            {
                "name": "dlv_tel",
                "type": "string"
            },
            {
                "name": "dlv_tel2",
                "type": "string"
            },
            {
                "name": "dlv_city_nm",
                "type": "string"
            },
            {
                "name": "dlv_zip",
                "type": "string"
            },

            {
                "name": "dlv_area_nm",
                "type": "string"
            },
            {
                "name": "pkg_num",
                "type": "number"
            },
            {
                "name": "pkg_unit",
                "type": "string"
            },
            {
                "name": "etd",
                "type": "range"
            },
            {
                "name": "truck_cmp_nm",
                "type": "string"
            },
            {
                "name": "driver",
                "type": "string"
            },
            {
                "name": "exp_reason",
                "type": "string"
            },
            {
                "name": "error_remark",
                "type": "string"
            },
            {
                "name": "updated_by",
                "type": "string"
            },
            {
                "name": "created_by",
                "type": "string"
            },
            {
                "name": "created_at",
                "type": "range"
            },
            {
                "name": "updated_at",
                "type": "range"
            },
            {
                "name": "dlv_no",
                "type": "string"
            },
            {
                "name": "car_no",
                "type": "string"
            },
            {
                "name": "trs_mode",
                "type": "string"
            },
            {
                "name": "trs_mode_desc",
                "type": "string"
            },
            {
                "name": "pick_cust_nm",
                "type": "string"
            },
            {
                "name": "pick_addr",
                "type": "string"
            },
            {
                "name": "pick_attn",
                "type": "string"
            },
            {
                "name": "pick_tel",
                "type": "string"
            },
            {
                "name": "pick_tel2",
                "type": "string"
            },
            {
                "name": "pick_city_nm",
                "type": "string"
            },
            {
                "name": "pick_zip",
                "type": "string"
            },
            {
                "name": "pick_area_nm",
                "type": "string"
            },
            {
                "name": "total_cbm",
                "type": "number"
            },
            {
                "name": "dlv_email",
                "type": "string"
            },
            {
                "name": "pick_email",
                "type": "string"
            },
            {
                "name": "distance",
                "type": "number"
            },
            {
                "name": "car_type",
                "type": "string"
            },
            {
                "name": "car_type_desc",
                "type": "string"
            },
            {
                "name": "amt",
                "type": "number"
            },
            {
                "name": "total_gw",
                "type": "number"
            },
            {
                "name": "cust_amt",
                "type": "number"
            },
            {
                "name": "amt_remark",
                "type": "string"
            },
            {
                "name": "owner_nm",
                "type": "string"
            },
            {
                "name": "is_inbound",
                "type": "string"
            },
            {
                "name": "owner_cd",
                "type": "string"
            },
            {
                "name": "sys_ord_no",
                "type": "string"
            },
            {
                "name": "dlv_remark",
                "type": "string"
            },
            {
                "name": "pick_remark",
                "type": "string"
            },
            {
                "name": "finish_date",
                "type": "range"
            },
            {
                "name": "cust_ord_no",
                "type": "string"
            },
            {
                "name": "wms_order_no",
                "type": "string"
            },
            {
                "name": "is_urgent",
                "type": "string"
            },
            {
                "name": "is_confirm",
                "type": "string"
            },
            {
                "name": "confirm_by",
                "type": "string"
            },
            {
                "name": "confirm_dt",
                "type": "range"
            },
            {
                "name": "collectamt",
                "type": "number"
            },
            {
                "name": "total_amount",
                "type": "number"
            },
            {
                "name": "temperate",
                "type": "string"
            },
            {
                "name": "dlv_addr_info",
                "type": "string"
            },
            {
                "name": "pick_addr_info",
                "type": "string"
            },
            {
                "name": "pick_addr_info",
                "type": "string"
            },
            {
                "name": "sign_pic",
                "type": "number"
            },
            {
                "name": "error_pic",
                "type": "number"
            },
            {
                "name": "sendmessage",
                "type": "string"
            },
            {
                "name": "abnormal_remark",
                "type": "string"
            },
            {
                "name": "temperate_desc",
                "type": "string"
            }
        ],
        root: "Rows",
        pagenum: 0,
        beforeprocessing: function (data) {
            source.totalrecords = data[0].TotalRows;
            if(data[0].StatusCount.length > 0) {
                data[0].StatusCount.push({'count': data[0].TotalRows, 'status': 'ALL','statustext': 'ALL'});
            }

            if($("#statusList").find('a').length == 0) {
                
                $("#statusList").html("");
                $.each(data[0].StatusCount, function(i, item) {
                    $("#statusList").append(' <a class="statusBtn btn btn-app"><span class="badge bg-purple"> '+item.count+'</span><i class="fa fa-bullhorn"></i><span class="text" id="'+item.status+'">'+item.statustext+'</span></a>');
                });
    
                $('.statusBtn').off().on('click', function(){
                    $('#statusList').find('a').removeClass('active');
                    $(this).addClass('active');
    
                    var statusGroup = new $.jqx.filter();
                    var filter_or_operator = 1;
                    var filtervalue = $(this).find('span.text').attr("id");
                    if(filtervalue == "ALL") {
                        $("#jqxGrid").jqxGrid('clearfilters');
                    }
                    else {
                        var filtercondition = 'contains';
                        var statusFilter1 = statusGroup.createfilter('stringfilter', filtervalue, filtercondition);
                        statusGroup.addfilter(filter_or_operator, statusFilter1);
                        $("#jqxGrid").jqxGrid('addfilter', 'status', statusGroup);
                        $("#jqxGrid").jqxGrid('applyfilters');
                    }
                    
                });
            }
        },
        filter: function () {
            // update the grid and send a request to the server.
            $("#jqxGrid").jqxGrid('updatebounddata', 'filter');
        },
        sort: function () {
            // update the grid and send a request to the server.
            $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
        },
        cache: false,
        pagesize: 50,
        url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order/true') }}"
    }
    var dataAdapter = new $.jqx.dataAdapter(source, {
        async: false, 
        loadError: function (xhr, status, error) { 
            alert('Error loading "' + source.url + '" : ' + error); 
        },
        loadComplete: function() {
            $.get(gridOpt.getStatusCount, {}, function(res){
                console.log(res);
                res[0].StatusCount.push({'count': res[0].TotalRows, 'status': 'ALL','statustext': 'ALL'});
                genStatus(res, 'jqxGrid');
            }, 'JSON');
        }
    });
    function genStatus(data, gridId) {
    $("#statusList").html("");
    console.log(data);
    $.each(data[0].StatusCount, function(i, item) {
        $("#statusList").append(' <a class="statusBtn btn btn-app"><span class="badge bg-purple"> '+item.count+'</span><i class="fa fa-bullhorn"></i><span class="text" id="'+item.status+'">'+item.statustext+'</span></a>');
    });

    $('.statusBtn').off().on('click', function(){
        $('#statusList').find('a').removeClass('active');
        $(this).addClass('active');

        var statusGroup = new $.jqx.filter();
        var filter_or_operator = 1;
        var filtervalue = $(this).find('span.text').attr("id");
        if(filtervalue == "ALL") {
            $("#" + gridId).jqxGrid('clearfilters');
        }
        else {
            var filtercondition = 'contains';
            var statusFilter1 = statusGroup.createfilter('stringfilter', filtervalue, filtercondition);
            statusGroup.addfilter(filter_or_operator, statusFilter1);
            $("#" + gridId).jqxGrid('addfilter', 'status', statusGroup);
            $("#" + gridId).jqxGrid('applyfilters');
        }
        
    });
}
    var h = 350;

    if(gridOpt.enabledStatus == false) {
        h = 250;
    }
    var winHeigt = $( window ).height() - h;
    if(typeof gridOpt.who !== "undefined" && gridOpt.who == "lookup") {
        winHeigt = 500;
    }
    $("#jqxGrid").jqxGrid(
        {
            width: '100%',
            height: winHeigt,
            source: dataAdapter,
            sortable: true,
            filterable: true,
            altrows: true,
            showfilterrow: true,
            pageable: true,
            virtualmode: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            columnsresize: true,
            columnsautoresize: true,
            clipboard: true,
            selectionmode: 'checkbox',
            enablebrowserselection: true,
            pagesizeoptions: [50, 100, 500, 2000],
            rendergridrows: function (params) {
                //alert("rendergridrows");
                return params.data;
            },
            ready: function () {
                $("#jqxLoader").jqxLoader({ width: 100, height: 60, imagePosition: 'top' });
                $("#jqxLoader").jqxLoader('open');
                //$('#' + gridId).jqxGrid('autoresizecolumns');
                // loadState();
            },
            updatefilterconditions: function (type, defaultconditions) {
                var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                switch (type) {
                    case 'stringfilter':
                        return stringcomparisonoperators;
                    case 'numericfilter':
                        return numericcomparisonoperators;
                    case 'datefilter':
                        return datecomparisonoperators;
                    case 'booleanfilter':
                        return booleancomparisonoperators;
                }
            },
            columns: [
                {
                    "text": "入庫",
                    "datafield": "is_inbound",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.etd') }}",
                    "datafield": "etd",
                    "width": 100,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "yyyy-MM-dd",
                    "filtertype": "range",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.trsMode') }}",
                    "datafield": "trs_mode",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "trsMode": [
                        {
                            "label": "\u5c08\u8eca(C)",
                            "value": "C"
                        },
                        {
                            "label": "\u975e\u5c08\u8eca(N)",
                            "value": "N"
                        },
                        {
                            "label": "\u5c08\u8eca-\u9577\u7a0b(C02)",
                            "value": "C02"
                        },
                        {
                            "label": "\u5c08\u8eca-\u77ed\u7a0b(C01)",
                            "value": "C01"
                        },
                        {
                            "label": "\u975e\u5c08\u8eca-\u91cd\u91cf(N01)",
                            "value": "N01"
                        },
                        {
                            "label": "\u5c1a\u672a\u8a08\u50f9",
                            "value": "NON"
                        },
                        {
                            "label": "\u975e\u5c08\u8eca-\u7121\u6298\u6263(N02)",
                            "value": "N02"
                        }
                    ],
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.trsModedesc') }}",
                    "datafield": "trs_mode_desc",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.temperate') }}",
                    "datafield": "temperate",
                    "width": 300,
                    "dbwidth": "100",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.temperateDesc') }}",
                    "datafield": "temperate_desc",
                    "width": 300,
                    "dbwidth": "100",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.sysOrdNo') }}",
                    "datafield": "sys_ord_no",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.ordNo') }}",
                    "datafield": "ord_no",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.custOrdNo') }}",
                    "datafield": "cust_ord_no",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.wmsorderno') }}",
                    "datafield": "wms_order_no",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.status') }}",
                    "datafield": "status",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": {
                        "source": "statusGridObj",
                        "value": "val",
                        "name": "key"
                    },
                    "statusCode": [
                        {
                            "label": "\u5c1a\u672a\u5b89\u6392",
                            "value": "UNTREATED"
                        },
                        {
                            "label": "\u5df2\u6d3e\u55ae\u672a\u51fa\u767c",
                            "value": "SEND"
                        },
                        {
                            "label": "\u8ca8\u7269\u88dd\u8f09\u4e2d",
                            "value": "LOADING"
                        },
                        {
                            "label": "\u51fa\u767c",
                            "value": "SETOFF"
                        },
                        {
                            "label": "\u51fa\u767c",
                            "value": "SETOFF"
                        },
                        {
                            "label": "\u6b63\u5e38\u914d\u9001",
                            "value": "NORMAL"
                        },
                        {
                            "label": "\u5df2\u63d0\u8ca8",
                            "value": "PICKED"
                        },
                        {
                            "label": "\u914d\u9001\u767c\u751f\u554f\u984c",
                            "value": "ERROR"
                        },
                        {
                            "label": "\u914d\u9001\u5ef6\u9072",
                            "value": "DELAY"
                        },
                        {
                            "label": "\u914d\u9001\u5b8c\u6210",
                            "value": "FINISHED"
                        },
                        {
                            "label": "\u4f5c\u5ee2",
                            "value": "FAIL"
                        },
                        {
                            "label": "\u95dc\u9589",
                            "value": "CLOSE"
                        },
                        {
                            "label": "modOrder.STATUS_REJECT",
                            "value": "REJECT"
                        }
                    ],
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.statusDesc') }}",
                    "datafield": "status_desc",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.isUrgent') }}",
                    "datafield": "is_urgent",
                    "width": 100,
                    "dbwidth": "1",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.ownerNm') }}",
                    "datafield": "owner_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.ownerCd') }}",
                    "datafield": "owner_cd",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.remark') }}",
                    "datafield": "remark",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickCustNm') }}",
                    "datafield": "pick_cust_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickZip') }}",
                    "datafield": "pick_zip",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickCityNm') }}",
                    "datafield": "pick_city_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickAreaNm') }}",
                    "datafield": "pick_area_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickAddr') }}",
                    "datafield": "pick_addr",
                    "width": 600,
                    "dbwidth": "200",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickAddrInfo') }}",
                    "datafield": "pick_addr_info",
                    "width": 600,
                    "dbwidth": "200",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickAttn') }}",
                    "datafield": "pick_attn",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text":  "{{ trans('modOrder.pickRemark') }}",
                    "datafield": "pick_remark",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickTel') }}",
                    "datafield": "pick_tel",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickTel2') }}",
                    "datafield": "pick_tel2",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pickEmail') }}",
                    "datafield": "pick_email",
                    "width": 300,
                    "dbwidth": "100",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvCustNm') }}",
                    "datafield": "dlv_cust_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvZip') }}",
                    "datafield": "dlv_zip",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvCityNm') }}",
                    "datafield": "dlv_city_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvAreaNm') }}",
                    "datafield": "dlv_area_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvAddr') }}",
                    "datafield": "dlv_addr",
                    "width": 600,
                    "dbwidth": "200",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvAddrInfo') }}",
                    "datafield": "dlv_addr_info",
                    "width": 600,
                    "dbwidth": "200",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvRemark') }}",
                    "datafield": "dlv_remark",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvAttn') }}",
                    "datafield": "dlv_attn",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvTel') }}",
                    "datafield": "dlv_tel",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvTel2') }}",
                    "datafield": "dlv_tel2",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvEmail') }}",
                    "datafield": "dlv_email",
                    "width": 300,
                    "dbwidth": "100",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pkgNum') }}",
                    "datafield": "pkg_num",
                    "width": 100,
                    "dbwidth": "11",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.pkgUnit') }}",
                    "datafield": "pkg_unit",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.totalCbm') }}",
                    "datafield": "total_cbm",
                    "width": 309,
                    "dbwidth": "103",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.totalGw') }}",
                    "datafield": "total_gw",
                    "width": 309,
                    "dbwidth": "103",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.driver') }}",
                    "datafield": "driver",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.carNo') }}",
                    "datafield": "car_no",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.distance') }}",
                    "datafield": "distance",
                    "width": 366,
                    "dbwidth": "122",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.collectamt') }}",
                    "datafield": "collectamt",
                    "width": 100,
                    "dbwidth": "182",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.total_amount') }}",
                    "datafield": "total_amount",
                    "width": 100,
                    "dbwidth": "182",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.amt') }}",
                    "datafield": "amt",
                    "width": 546,
                    "dbwidth": "182",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.custAmt') }}",
                    "datafield": "cust_amt",
                    "width": 546,
                    "dbwidth": "182",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.amtRemark') }}",
                    "datafield": "amt_remark",
                    "width": 300,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dlvNo') }}",
                    "datafield": "dlv_no",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.expReason') }}",
                    "datafield": "error_remark",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.abnormalRemark') }}",
                    "datafield": "abnormal_remark",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                
                {
                    "text": "{{ trans('modOrder.finishDate') }}",
                    "datafield": "finish_date",
                    "width": 100,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "yyyy-MM-dd HH:mm:ss",
                    "filtertype": "range",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.signPic') }}",
                    "datafield": "sign_pic",
                    "width": 546,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999,
                    "cellsrenderer":function (row, columnfield, value, defaulthtml, columnproperties) {
                    var sysOrdNo = dataAdapter.records[row].sys_ord_no;
                    var signPic = dataAdapter.records[row].sign_pic;
                    var btn = "";

                    if(signPic > 0) {
                        btn = `<button class="btn btn-link"  name="signbtn" onclick="openSignPic('{sysOrdNo}')"  id='{idno}sign'>簽收照片連結</button>`;
                        btn = btn.replace("{sysOrdNo}", sysOrdNo);
                        btn = btn.replace("{idno}", sysOrdNo);
                    }
                    
                    return btn;
                }
                },
                {
                    "text": "{{ trans('modOrder.errorPic') }}",
                    "datafield": "error_pic",
                    "width": 546,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999,
                    "cellsrenderer":function (row, columnfield, value, defaulthtml, columnproperties) {
                    var sysOrdNo = dataAdapter.records[row].sys_ord_no;
                    var errorPic = dataAdapter.records[row].error_pic;
                    var btn = "";

                    if(errorPic > 0) {
                        btn = `<button class="btn btn-link" name="errorbtn"  onclick="openErrorPic('{sysOrdNo}')"  id='{idno}error'>異常照片連結</button>`;
                        btn = btn.replace("{sysOrdNo}", sysOrdNo);
                        btn = btn.replace("{idno}", sysOrdNo);
                    }
                    
                    return btn;
                }
                },
                {
                    "text": "{{ trans('modOrder.isconfirm') }}",
                    "datafield": "is_confirm",
                    "width": 100,
                    "dbwidth": "1",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.confirmby') }}",
                    "datafield": "confirm_by",
                    "width": 100,
                    "dbwidth": "1",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.confirmdt') }}",
                    "datafield": "confirm_dt",
                    "width": 100,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "yyyy-MM-dd HH:mm:ss",
                    "filtertype": "range",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.createdBy') }}",
                    "datafield": "created_by",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.createdAt') }}",
                    "datafield": "created_at",
                    "width": 100,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "yyyy-MM-dd HH:mm:ss",
                    "filtertype": "range",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.updatedBy') }}",
                    "datafield": "updated_by",
                    "width": 100,
                    "dbwidth": "30",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.updatedAt') }}",
                    "datafield": "updated_at",
                    "width": 100,
                    "dbwidth": 100,
                    "nullable": true,
                    "cellsformat": "yyyy-MM-dd HH:mm:ss",
                    "filtertype": "range",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },

                {
                    "text": "{{ trans('modOrder.truckCmpNm') }}",
                    "datafield": "truck_cmp_nm",
                    "width": 210,
                    "dbwidth": "70",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "發送簡訊",
                    "datafield": "sendmessage",
                    "width": 100,
                    "dbwidth": "20",
                    "nullable": true,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "right",
                    "values": "",
                    "sendmessage": [
                        {
                            "label": "尚未發送簡訊",
                            "value": "0"
                        }
                    ],
                    "filterdelay": 99999999
                },
                {
                    "text": "id",
                    "datafield": "id",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "number",
                    "cellsalign": "right",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.gKey') }}",
                    "datafield": "g_key",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.cKey') }}",
                    "datafield": "c_key",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.sKey') }}",
                    "datafield": "s_key",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
                {
                    "text": "{{ trans('modOrder.dKey') }}",
                    "datafield": "d_key",
                    "width": 100,
                    "dbwidth": "10",
                    "nullable": false,
                    "cellsformat": "",
                    "filtertype": "textbox",
                    "cellsalign": "left",
                    "values": "",
                    "filterdelay": 99999999
                },
            ]
        });
    gridOpt.gridId = "jqxGrid";
    $("#jqxGrid").on('bindingcomplete', function (event) {
        var statusHeight = 0;
        $('#jqxLoader').jqxLoader('close');
        if(typeof gridOpt.enabledStatus !== "undefined") {
            if(gridOpt.enabledStatus == false) {
                statusHeight = 50;
            }
        }
        var winHeight = $( window ).height() - 350 + statusHeight;
        $("#jqxGrid").jqxGrid('height', winHeight+'px');
        //$("#"+gridId).jqxGrid("hideloadelement"); 
    });
    // gridOpt.fieldsUrl = 'http://coresys.test:8080/api/admin/baseApi/getGridJson/mod_order/true?filterscount=0&groupscount=0&pagenum=0&pagesize=50&recordstartindex=0&recordendindex=50&_=1558592094698';
    // $.get( gridOpt.fieldsUrl, function( fieldData ) {
    //     console.log(fieldData);

    // });
    $("#jqxGrid").on("rowselect", function (event) {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            $("#selectcount").text(rows.length);
    });
    $('#jqxGrid').on('rowunselect', function (event){
        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            $("#selectcount").text(rows.length);
    });
    function loadState() {  	
        var loadURL = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/getLayoutJson') }}"; 	  	  	
        $.ajax({
            type: "GET", //  OR POST WHATEVER...
            url: loadURL,
            data: { key: gridOpt.dataUrl },		 
            success: function(response) {										
                if (response != "") {	
                    response = JSON.parse(response);
                    $("#"+gridOpt.gridId).jqxGrid('loadstate', response);
                }
                
                var listSource = [];
        
                state = $("#"+gridOpt.gridId).jqxGrid('getstate');
                console.log(state);
                $('#jqxLoader').jqxLoader('close');
                $.get(BASE_URL+"/searchList/get/" + gridOpt.pageId, {}, function(data){
                    if(data.msg == "success") {
                        var opt = "<option value=''>請選擇</option>";
            
                        for(i in data.data) {
                            if(data.data[i]["layout_default"] == "Y") {
                                opt += "<option value='"+data.data[i]["id"]+"' selected>"+data.data[i]["title"]+"</option>";
                                $("input[name='searchName']").val(data.data[i]["title"]);
                            }
                            else {
                                opt += "<option value='"+data.data[i]["id"]+"'>"+data.data[i]["title"]+"</option>";
                            }
                        }
            
                        $("select[name='selSearchName']").html(opt);
            
                        $.get(BASE_URL+"/searchHtml/get/" + $("select[name='selSearchName']").val(), {}, function(data){
                            if(data.msg == "success") {
                                if(data.data != null) {
                                    $("#searchContent").html(data.data["data"]);
                                }
                                else {
                                    var seasrchTpl = getSearchTpl(state);
                                    initSearchWindow(seasrchTpl);
                                }

                                var offset = $(".content-wrapper").offset();
                                $('#searchWindow').jqxWindow({
                                    position: { x: offset.left + 50, y: offset.top + 50} ,
                                    showCollapseButton: true, maxHeight: 400, maxWidth: 700, minHeight: 200, minWidth: 200, height: 300, width: 700, autoOpen: false,
                                    initContent: function () {
                                        $('#searchWindow').jqxWindow('focus');
                                    }
                                });


                                $("button[name='winSearchAdd']").on("click", function(){
                                    var seasrchTpl = getSearchTpl(state);
                                    $("#searchContent").append(searchTpl);           
                                });

                                $(document).on("click", "button[name='winBtnRemove']", function(){ 
                                    $( this ).parents(".row").remove();
                                });

                                $(document).on("change", "select[name='winField[]']", function(){ 
                                    $('option:selected', this).siblings().removeAttr('selected');
                                    $('option:selected', this).attr('selected', 'selected');
                                    var val  = $(this).val();
                                    var info = searchObj(val, fieldObj[0]);
                                    var str  = [
                                        {val: 'CONTAINS', label: '包含'},
                                        {val: 'EQUAL', label: '等於'},
                                        {val: 'NOT_EQUAL', label: '不等於'},
                                        {val: 'IN', label: 'IN'},
                                        {val: 'NULL', label: 'NULL'},
                                        {val: 'NOT_NULL', label: 'NOT NULL'},
                                    ];

                                    var num  = [
                                        {val: 'EQUAL', label: '等於'},
                                        {val: 'NOT_EQUAL', label: '不等於'},
                                        {val: 'IN', label: 'IN'},
                                        {val: 'LESS_THAN', label: '小於'},
                                        {val: 'LESS_THAN_OR_EQUAL', label: '小於等於'},
                                        {val: 'GREATER_THAN', label: '大於'},
                                        {val: 'GREATER_THAN_OR_EQUAL', label: '大於等於'},
                                        {val: 'NULL', label: 'NULL'},
                                        {val: 'NOT_NULL', label: 'NOT NULL'},
                                    ];

                                    var opt = "";
                                    if(info.type == "string") {
                                        for(i in str) {
                                            opt += '<option value="'+str[i].val+'">'+str[i].label+'</option>';
                                        }
                                    }
                                    else {
                                        for(i in num) {
                                            opt += '<option value="'+num[i].val+'">'+num[i].label+'</option>';
                                        }
                                    }
                                    $($(this).parent().siblings()[0]).find("select").html(opt);
                                });

                                $(document).on("change", "select[name='winOp[]']", function(){
                                    $('option:selected', this).siblings().removeAttr('selected');
                                    $('option:selected', this).attr('selected', 'selected');
                                });

                                $(document).on("change", "input[name='winContent[]']", function(){
                                    $(this).attr('value', $(this).val());
                                });

                                $("button[name='winSearchBtn']").on("click", function(){
                                    var winField   = [];
                                    var winContent = [];
                                    var winOp      = [];
                                    $("select[name='winField[]']").each(function(){
                                        winField.push($(this).val());
                                    });
                                    $("input[name='winContent[]']").each(function(){
                                        winContent.push($(this).val());
                                    });
                                    $("select[name='winOp[]']").each(function(){
                                        winOp.push($(this).val());
                                    });
                                    
                                    addfilter(winField, winContent, winOp);
                                });
                            }
                        });
                        
                    }
                });

                $.each(state.columns, function(i, item) {
                    if(item.text != "" && item.text != "undefined"){
                        listSource.push({ 
                        label: item.text, 
                        value: i, 
                        checked: !item.hidden });
                        if(!item.hidden){
                            var headerdata = {
                            'filed_text':item.text,
                                'filed_name':i,
                                'show':item.hidden
                            };    
                            enabledheader.push(headerdata);
                        }
                    }
                });

                $("#jqxlistbox").jqxListBox({ 
                    allowDrop: true, 
                    allowDrag: true,
                    source: listSource,
                    width: "99%",
                    height: 500,
                    checkboxes: true,
                    filterable: true,
                    searchMode: 'contains'
                });
            }
        });			  	  	  	  	
    }
    $("#saveGrid").on("click", function(){
        var items = $("#jqxlistbox").jqxListBox('getItems');
        //$('#jqxGrid').jqxGrid('clear');
        $("#"+gridOpt.gridId).jqxGrid('beginupdate');

        $.each(items, function(i, item) {
            console.log($('#'+gridOpt.gridId).jqxGrid('getcolumnindex', item.value));
            var thisIndex = $('#'+gridOpt.gridId).jqxGrid('getcolumnindex', item.value)-1;
            if(thisIndex != item.index){
                //console.log(item.value+":"+thisIndex+"="+item.index);
                $('#'+gridOpt.gridId).jqxGrid('setcolumnindex', item.value,  item.index);
            }
            if (item.checked) {
                $("#"+gridOpt.gridId).jqxGrid('showcolumn', item.value);
            }
            else {
                $("#"+gridOpt.gridId).jqxGrid('hidecolumn', item.value);
            }
        })
        
        $("#"+gridOpt.gridId).jqxGrid('endupdate');
        state = $("#"+gridOpt.gridId).jqxGrid('getstate');
        state['filters']['filterscount'] = 0;
        var saveUrl = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/saveLayoutJson') }}"; 	  	  	
        var stateToSave = JSON.stringify(state);

        $.ajax({
            type: "POST",										
            url: saveUrl,		
            data: { data: stateToSave,key: gridOpt.dataUrl },		 
            success: function(response) {
                if(response == "true"){
                    alert("save successful");
                    $('#gridOptModal').modal('hide');
                }else{
                    alert("save failded");
                }
                
            }
        });	
    });

    $("#clearGrid").on("click", function(){
        $.ajax({
            type: "POST",										
            url: "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/clearLayout') }}",		
            data: { key: gridOpt.dataUrl },		 
            success: function(response) {
                if(response == "true"){
                    alert("clear successful");
                    location.reload();
                    $('#gridOptModal').modal('hide');
                }else{
                    //alert("save failded");
                }
                
            }
        });	
    });
    function myFunctionchange() {
        var x = document.getElementById("swal-input1").value;
        // console.log(x);
        $("#swal-input2").text(x);
        // document.getElementById("demo").innerHTML = "You selected: " + x;
    }
    $("#chStation").on("click", function(){
        var idx = $('#skeyGrid').jqxGrid('getselectedrowindexes');
        if(idx.length ==  0) {
            swal("請選擇一筆資料", "", "warning");
            return;
        }
        console.log();
        var ids = [];
        var dlvTypes = [];
        var cust_no = "";
        for(i in idx) {
            var rowData = $('#skeyGrid').jqxGrid('getrowdata', idx[i]);
            cust_no = rowData.cust_no;
        }

        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        var id = new Array();
        for (var m = 0; m < rows.length; m++) {
            var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
            if(typeof row != "undefined") {
                id.push(row.id);
            }
        }

        $.post(BASE_URL + "/OrderMgmt/chStation", {cust_no: cust_no, ids: id}, function(data){
            $("#skeyModal").modal('hide');
            if(data.msg == "success") {
                swal("完成", "", "success");
                $('#skeyGrid').jqxGrid('updatebounddata');
                $('#jqxGrid').jqxGrid('updatebounddata');
                $("#jqxGrid").jqxGrid('clearselection');
            }
        });
    
    });
    $("select[name='selSearchName']").on("change", function(){
        var text = $('option:selected', this).text();
        if($(this).val() == '') {
            text = "";
        }
        $("input[name='searchName']").val(text);

        $.get(BASE_URL+"/searchHtml/get/" + $(this).val(), {}, function(data){
            //console.log(data.data["data"]);
            $("#searchContent").html(data.data["data"]);
        });
    })
    function initSearchWindow(searchTpl) {
        $("#searchContent").append(searchTpl);
    }
    function getSearchTpl(state) {
        var fields = [];
        $.each(state.columns, function(i, item) {
            if(item.hidden == false && item.text != "") {
                var a = {
                    label: item.text,
                    value: i
                }
                fields.push(a);
            }
        });

        var fieldStr = "";
        for(i in fields) {
            fieldStr += '<option value="'+fields[i].value+'">'+fields[i].label+'</option>';
        }

        searchTpl = '<div class="row">\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winField[]">'+fieldStr+'</select>\
                            </div>\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winOp[]">\
                                <option value="CONTAINS">包含</option>\
                                <option value="EQUAL">等於</option>\
                                <option value="NOT_EQUAL">不等於</option>\
                                <option value="IN">IN</option>\
                                <option value="NULL">NULL</option>\
                                <option value="NOT_NULL">NOT NULL</option>\
                                <option value="LESS_THAN">小於</option>\
                                <option value="LESS_THAN_OR_EQUAL">小於等於</option>\
                                <option value="GREATER_THAN">大於</option>\
                                <option value="GREATER_THAN_OR_EQUAL">大於等於</option>\
                                </select>\
                            </div>\
                            <div class="col-xs-4">\
                                <input type="text" class="form-control input-sm" name="winContent[]">\
                            </div>\
                            <div class="col-xs-2">\
                                <button class="btn btn-sm btn-info btn-danger" name="winBtnRemove">-</button>\
                            </div>\
                        </div>';
        return searchTpl;
    }

    $("#jqxGrid").on("rowdoubleclick", function(event){
        // var row = $("#jqxGrid").jqxGrid('getrowdata', rows['event.args.rowindex']);
        var args = event.args;
            // row's bound index.
            var boundIndex = args.rowindex;
            // row's visible index.
            var visibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick; 
            // original event.
            var ev = args.originalEvent;

            var datarow = $("#"+gridOpt.gridId).jqxGrid('getrowdata', boundIndex);
            console.log(datarow);

            var editUrl = gridOpt.editUrl.replace("{id}", datarow.id);
            //location.href = editUrl;
            window.open(editUrl);
    })

    var addfilter = function (datafield, filterval, filtercondition) {
        $("#jqxGrid").jqxGrid('clearfilters');
        var old_filed ="";
        for(i in datafield) {
            var filtergroup = new $.jqx.filter();
            if(old_filed != datafield[i]){
                old_filed = datafield[i];

                var filter_or_operator = 1;
                //var filtervalue = filterval;
                var filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
                if(datafield[i]=="updated_at" || datafield[i]=="created_at"|| datafield[i]=="confirm_dt"|| 
                datafield[i]=="finish_date" ||datafield[i]=="etd"){
                    filter = filtergroup.createfilter('datefilter', filterval[i], filtercondition[i]);
                    filtergroup.addfilter(filter_or_operator, filter);
                }else{
                    filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
                    filtergroup.addfilter(filter_or_operator, filter);
                }

                // add the filters.
                $("#jqxGrid").jqxGrid('addfilter', datafield[i], filtergroup);
            }else{


                var filter3 = filtergroup.createfilter('datefilter',  filterval[i-1], filtercondition[i-1]);
                filtergroup.addfilter(0, filter3);
                $("#jqxGrid").jqxGrid('addfilter',  datafield[i], filtergroup);

                var filter2 = filtergroup.createfilter('datefilter',  filterval[i], filtercondition[i]);
                filtergroup.addfilter(0, filter2);
                $("#jqxGrid").jqxGrid('addfilter',  datafield[i], filtergroup);


            }
        }
        // apply the filters.
        $("#jqxGrid").jqxGrid('applyfilters');
    }

    

    function searchObj(nameKey, myArray){
        for (var i=0; i < myArray.length; i++) {
            if (myArray[i].name === nameKey) {
                return myArray[i];
            }
        }
    }

    $('#searchWindow').on('open', function (event) { 
        if($("body").width() < 400) {
            var offset = $("body").offset();
            $('#searchWindow').jqxWindow({ height: 500, width: $("body").width(), position: { x: 0, y: offset.top + 150} , });
        }
        else {

        }
        
    });

    $("button[name='setDefault']").on("click", function(){
        $.ajax({
            url: BASE_URL + "/setSearchDefault/" + gridOpt.pageId + "/" + $("select[name='selSearchName']").val(),
            dataType: "JSON",
            method: "PUT",
            success: function(result){
                if(result.msg == "success") {
                    swal("儲存成功", "", "success");
                }
                else {
                    swal("操作失敗", "", "error");
                }
            },
            error: function() {
                swal("網路似乎出了問題，請稍後再試", "", "error");
            }
        });
    });

    $("button[name='delSearch']").on("click", function(){
        $.ajax({
            url: BASE_URL + "/delSearchLayout/" + $("select[name='selSearchName']").val(),
            dataType: "JSON",
            method: "DELETE",
            success: function(result){
                if(result.msg == "success") {
                    swal("刪除成功", "", "success");
                    $("select[name='selSearchName'] option:selected").remove();
                    $("input[name='searchName']").val("");
                }
                else {
                    if(typeof data.errorMsg !== "undefined") {
                        swal("操作失敗", data.errorMsg, "error");
                    }
                    else {
                        swal("操作失敗", "", "error");
                    }
                    
                }
            },
            error: function() {
                swal("網路似乎出了問題，請稍後再試", "", "error");
            }
        });
    });

    $("button[name='saveSearch']").on("click", function(){
        var htmlData = $("#searchContent").html();
        var postData = {
            "key" : gridOpt.pageId,
            "data": htmlData,
            "title": $("input[name='searchName']").val(),
            "id": ($("select[name='selSearchName'] option:selected").text() == $("input[name='searchName']").val())?$("select[name='selSearchName']").val():null
        };
        $.ajax({
            url: BASE_URL + "/saveSearchLayout",
            data: postData,
            dataType: "JSON",
            method: "POST",
            success: function(result){
                if(result.msg == "success") {
                    swal("儲存成功", "", "success");
                    $("select[name='selSearchName'] option").removeAttr("selected");
                    $("select[name='selSearchName']").append("<option value='"+result.id+"' selected>"+$("input[name='searchName']").val()+"</option>")
                }
                else {
                    swal("操作失敗", "", "error");
                }
            },
            error: function() {
                swal("請輸入查詢條件名稱", "", "error");
            }
        });
    });
</script>
<script>
    var picsignsysno = 0;
    var picerrsysno = 0;
    var sysnonow = 0;
    $('#updategridsign').on('click', function(imgtype){
        console.log("success close window");
        document.getElementById(picsignsysno+"sign").style.background ="white";
    });  
    $('#updategriderror').on('click', function(imgtype){
        document.getElementById(picerrsysno+"error").style.background ="white";
        console.log("success close window");
    });  
Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};
</script>
@endsection