@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	{{ trans('modOrder.orderInfo') }}<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url(config('backpack.base.route_prefix'),'OrderMgmt') }}">{{ trans('modOrder.titleName') }}</a></li>
		<li class="active">{{ trans('modOrder.titleAddName') }}</li>
	</ol>
</section>
<script>
	var truck_cmp_no = "";
	var truck_cmp_no = "";
	var whAddr = "";

	@if(isset($carData))
		truck_cmp_no = "{{$carData->cust_no}}";
		truck_cmp_nm = "{{$carData->cname}}";
		whAddr       = "{{$carData->city_nm.$carData->area_nm.$carData->address}}";
	@endif
</script>
@endsection 
@section('content')
	@include('backpack::template.toolbar')
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4>{{ trans('backpack::crud.please_fix') }}</h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">{{ trans('modOrder.basicInfo') }}</a></li>
						<li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.pickupInfo') }}</a></li>
						<li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.deliveryInfo') }}</a></li>
						<li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.amtInfo') }}</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">

							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-3">
										<label for="sys_ord_no">{{ trans('modOrder.sysOrdNo') }}</label>
										<input type="text" class="form-control" id="sys_ord_no" name="sys_ord_no" switch="off">
									</div>
									<div class="form-group col-md-3">
										<label for="ord_no">{{ trans('modOrder.ordNo') }}</label>
										<input type="text" class="form-control" id="ord_no" name="ord_no">
									</div>
									<div class="form-group col-md-3">
										<label for="etd">{{ trans('modOrder.etd') }}</label>
										<input type="text" class="form-control" id="etd" name="etd">
									</div>
									<div class="form-group col-md-3">
                                        <label for="status">{{ trans('modOrder.is_urgent') }}</label>
                                        <select class="form-control" id="is_urgent" name="is_urgent" no-clear="Y">
                                            <option value="N">{{ trans('common.no') }}</option>
                                            <option value="Y">{{ trans('common.yes') }}</option>
                                        </select>
                                    </div>  
								</div>
								<div class="row">
									<div class="form-group col-md-3">
										<label for="status">{{ trans('modOrder.status') }}</label>
										<select class="form-control" id="status" name="status" switch="off">
                                            <option value="UNTREATED">{{ trans('modOrder.STATUS_UNTREATED') }}</option>
                                            <option value="SEND">{{ trans('modOrder.STATUS_SEND') }}</option>
                                            <option value="LOADING">{{ trans('modOrder.STATUS_LOADING') }}</option>
                                            <option value="SETOFF">{{ trans('modOrder.STATUS_SETOFF') }}</option>
                                            <option value="NORMAL">{{ trans('modOrder.STATUS_NORMAL') }}</option>
                                            <option value="PICKED">{{ trans('modOrder.STATUS_PICKED') }}</option>
                                            <option value="ERROR">{{ trans('modOrder.STATUS_ERROR') }}</option>
                                            <option value="DELAY">{{ trans('modOrder.STATUS_DELAY') }}</option>
                                            <option value="FINISHED">{{ trans('modOrder.STATUS_FINISHED') }}</option>
                                            <option value="CLOSE">{{ trans('modOrder.STATUS_CLOSE') }}</option>
                                        </select>
									</div>
									<div class="form-group col-md-3">
										<label for="trs_mode">{{ trans('modOrder.trsMode') }}</label>
										<select class="form-control" id="trs_mode" name="trs_mode" no-clear="Y">
                                            
                                        </select>
									</div>
									<div class="form-group col-md-3">
										<label for="trs_mode">{{ trans('modOrder.temperate') }}</label>
										<select class="form-control" id="trs_mode" name="temperate" no-clear="Y">
                                            
                                        </select>
									</div>
								</div>

								<div class="row">
									<div class="form-group col-md-3">
										<label for="truck_cmp_no">{{ trans('modOrder.truckCmpNo') }}</label>
										<input type="text" class="form-control" id="truck_cmp_no" name="truck_cmp_no">
									</div>
									<div class="form-group col-md-3">
										<label for="truck_cmp_nm">{{ trans('modOrder.truckCmpNm') }}</label>
										<input type="text" class="form-control" id="truck_cmp_nm" name="truck_cmp_nm">
									</div>
									<div class="form-group col-md-6">
										<label for="wh_addr">{{ trans('modOrder.whAddr') }}</label>
										<input type="text" class="form-control" id="wh_addr" name="wh_addr">
									</div>
								</div>

								<div class="row">
									<div class="form-group col-md-3">
										<label for="owner_cd">{{ trans('modOrder.ownerCd') }}</label>
										<input type="text" class="form-control" id="owner_cd" name="owner_cd">
									</div>
									<div class="form-group col-md-3">
										<label for="owner_nm">{{ trans('modOrder.ownerNm') }}</label>
										<input type="text" class="form-control" id="owner_nm" name="owner_nm">
									</div>
									<div class="form-group col-md-3">
                                        <label for="status">{{ trans('modOrder.ownerSendMail') }}</label>
                                        <select class="form-control" id="owner_send_mail" name="owner_send_mail">
                                            <option value="N">{{ trans('common.no') }}</option>
                                            <option value="Y">{{ trans('common.yes') }}</option>
                                        </select>
                                    </div>  
								</div>

								<div class="row">
									<div class="form-group col-md-12">
										<label for="remark">{{ trans('modOrder.remark') }}</label>
										<textarea class="form-control" rows="3" name="remark"></textarea>
									</div>
								</div>


								@if(isset($id))
								<input type="hidden" name="id" value="{{$id}}" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								@endif

							</div>
						</div>

						<div class="tab-pane" id="tab_2">
							<div class="row">
								<div class="form-group col-md-4">
									<label for="dlv_cust_no">{{ trans('modOrder.dlvCustNo') }}</label>
									<input type="text" class="form-control" id="dlv_cust_no" name="dlv_cust_no">
								</div>
								<div class="form-group col-md-4">
									<label for="dlv_cust_nm">{{ trans('modOrder.dlvCustNm') }}</label>
									<input type="text" class="form-control" id="dlv_cust_nm" name="dlv_cust_nm">
								</div>
								<div class="form-group col-md-4">
									<label for="dlv_attn">{{ trans('modOrder.dlvAttn') }}</label>
									<input type="text" class="form-control" id="dlv_attn" name="dlv_attn">
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-md-4">
									<label for="dlv_tel">{{ trans('modOrder.dlvTel') }}</label>
									<input type="text" class="form-control" id="dlv_tel" name="dlv_tel">
								</div>
								<div class="form-group col-md-4">
									<label for="dlv_email">{{ trans('modOrder.dlvEmail') }}</label>
									<input type="text" class="form-control" id="dlv_email" name="dlv_email">
								</div>
								<div class="form-group col-md-3">
									<label for="status">{{ trans('modOrder.dlvSendMail') }}</label>
									<select class="form-control" id="dlv_send_mail" name="dlv_send_mail">
										<option value="N">{{ trans('common.no') }}</option>
										<option value="Y">{{ trans('common.yes') }}</option>
									</select>
								</div>  
							</div>

							<div class="row">
							    <div class="form-group col-md-4">
									<label for="dlv_zip">{{ trans('modOrder.dlvZip') }}</label>
									<input type="text" class="form-control" id="dlv_zip" name="dlv_zip">
								</div>
								<div class="form-group col-md-4">
									<label for="dlv_city_nm">{{ trans('modOrder.dlvCityNm') }}</label>
									<input type="text" class="form-control" id="dlv_city_nm" name="dlv_city_nm">
								</div>
								<div class="form-group col-md-4">
									<label for="dlv_area_nm">{{ trans('modOrder.dlvAreaNm') }}</label>
									<input type="hidden" class="form-control" id="dlv_area_id" name="dlv_area_id">
									<input type="text" class="form-control" id="dlv_area_nm" name="dlv_area_nm">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-12">
									<label for="dlv_addr">{{ trans('modOrder.dlvAddr') }}</label>
									<input type="text" class="form-control" id="dlv_addr" name="dlv_addr">
									<input type="hidden" class="form-control" id="dlv_lat" name="dlv_lat">
									<input type="hidden" class="form-control" id="dlv_lng" name="dlv_lng">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-12">
									<label for="dlv_remark">{{ trans('modOrder.dlvRemark') }}</label>
									<textarea class="form-control" rows="3" name="dlv_remark" id="dlv_remark"></textarea>
								</div>
							</div>
						</div>

						<div class="tab-pane" id="tab_3">
							<div class="row">
								<div class="form-group col-md-4">
									<input type="text" class="form-control" id="pick_cust_no" style="display:none" name="pick_cust_no">
									<label for="pick_cust_nm">{{ trans('modOrder.pickCustNm') }}</label>
									<input type="text" class="form-control" id="pick_cust_nm" name="pick_cust_nm">
								</div>
								<div class="form-group col-md-4">
									<label for="pick_attn">{{ trans('modOrder.pickAttn') }}</label>
									<input type="text" class="form-control" id="pick_attn" name="pick_attn">
								</div>
								<div class="form-group col-md-4">
									<label for="pick_tel">{{ trans('modOrder.pickTel') }}</label>
									<input type="text" class="form-control" id="pick_tel" name="pick_tel">
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-md-4">
									<label for="pick_email">{{ trans('modOrder.pickEmail') }}</label>
									<input type="text" class="form-control" id="pick_email" name="pick_email">
								</div>
								<div class="form-group col-md-3">
									<label for="status">{{ trans('modOrder.pickSendMail') }}</label>
									<select class="form-control" id="pick_send_mail" name="pick_send_mail">
										<option value="N">{{ trans('common.no') }}</option>
										<option value="Y">{{ trans('common.yes') }}</option>
									</select>
								</div>  
							</div>

							<div class="row">
							    <div class="form-group col-md-4">
									<label for="pick_zip">{{ trans('modOrder.pickZip') }}</label>
									<input type="text" class="form-control" id="pick_zip" name="pick_zip">
								</div>
								<div class="form-group col-md-4">
									<label for="pick_city_nm">{{ trans('modOrder.pickCityNm') }}</label>
									<input type="text" class="form-control" id="pick_city_nm" name="pick_city_nm">
								</div>
								<div class="form-group col-md-4">
									<label for="pick_area_nm">{{ trans('modOrder.pickAreaNm') }}</label>
									<input type="hidden" class="form-control" id="pick_area_id" name="pick_area_id">
									<input type="text" class="form-control" id="pick_area_nm" name="pick_area_nm">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-12">
									<label for="pick_addr">{{ trans('modOrder.pickAddr') }}</label>
									<input type="text" class="form-control" id="pick_addr" name="pick_addr">
									<input type="hidden" class="form-control" id="pick_lat" name="pick_lat">
									<input type="hidden" class="form-control" id="pick_lng" name="pick_lng">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-12">
									<label for="pick_remark">{{ trans('modOrder.pickRemark') }}</label>
									<textarea class="form-control" rows="3" name="pick_remark" id="pick_remark"></textarea>
								</div>
							</div>
						</div>

						<div id="tab_5" class="tab-pane">
							<div class="row">
								<div class="form-group col-md-4">
									<label for="amt">{{ trans('modOrder.amt') }}</label>
									<input type="text" class="form-control" id="amt" name="amt" switch="off">
								</div>
								<div class="form-group col-md-4">
									<label for="cust_amt">{{ trans('modOrder.custAmt') }}</label>
									<input type="text" class="form-control" id="cust_amt" name="cust_amt">
								</div>
								<div class="form-group col-md-4">
									<label for="distance">{{ trans('modOrder.distance') }}</label>
									<input type="text" class="form-control" id="distance" name="distance" switch="off">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-4">
									<label for="pkg_num">{{ trans('modOrder.pkgNum') }}</label>
									<input type="text" class="form-control" id="pkg_num" name="pkg_num" switch="off">
								</div>
								<div class="form-group col-md-4">
									<label for="total_gw">{{ trans('modOrder.totalGw') }}</label>
									<input type="text" class="form-control" id="total_gw" name="total_gw" switch="off">
								</div>
								<div class="form-group col-md-4">
									<label for="total_cbm">{{ trans('modOrder.totalCbm') }}</label>
									<input type="text" class="form-control" id="total_cbm" name="total_cbm" switch="off">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-12">
									<label for="amt_remark">{{ trans('modOrder.amtRemark') }}</label>
									<input type="text" class="form-control" id="amt_remark" name="amt_remark">
								</div>
							</div>
						</div>

					</div>

				</div>
			</form>

			<div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab_6" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.orderDetail') }}</a></li>
					{{--  <li><a href="#tab_4" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.loadingPlan') }}</a></li>  --}}
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_6">
						<div class="box box-primary" id="subBox" style="display:none">
							<div class="box-header with-border">
								<h3 class="box-title">{{ trans('modOrder.orderDetail') }}</h3>
							</div>
							<form method="POST" accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
								<!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<div class="form-group col-md-2">
											<label for="goods_no">{{ trans('modOrderDetail.goodsNo') }}</label>
											{{--  <input type="text" class="form-control input-sm" name="goods_no" grid="true" >  --}}
											<div class="input-group input-group-sm">
												<input type="text" class="form-control" id="goods_no" name="goods_no">
												<span class="input-group-btn">
													<button type="button" class="btn btn-default btn-flat lookup" btnname="goods_no"
														info1="{{Crypt::encrypt('mod_goods')}}" 
														info2="{{Crypt::encrypt('goods_no+goods_nm,goods_no,goods_nm,gw,gwu,cbm,cbmu,`g_length`,`g_height`,`g_width`,`goods_no2`')}}" 
														info3="{{Crypt::encrypt('')}}"
														info4="goods_no=goods_no;goods_no=goods_nm,gw=gw,gwu=guw,cbm=cbm,cbmu=cbmu,length=g_length,height=g_height,weight=g_width,goods_no2=goods_no2" triggerfunc="" selectionmode="singlerow">
														<i class="fa fa-search" aria-hidden="true"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="form-group col-md-2">
												<label for="goods_no2">{{ trans('modOrderDetail.goodsNo2') }}</label>
												<input type="text" class="form-control input-sm" name="goods_no2" grid="true" required="required">
											</div>
										<div class="form-group col-md-2">
											<label for="goods_nm">{{ trans('modOrderDetail.goodsNm') }}</label>
											<input type="text" class="form-control input-sm" name="goods_nm" grid="true" required="required">
										</div>
										<div class="form-group col-md-2">
											<label for="pkg_num">{{ trans('modOrderDetail.pkgNum') }}</label>
											<input type="text" class="form-control input-sm" name="pkg_num" grid="true" >
										</div>
										<div class="form-group col-md-2">
											<label for="pkg_unit">{{ trans('modOrderDetail.pkgUnit') }}</label>
                                            <input type="text" class="form-control input-sm" name="pkg_unit" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="gw">{{ trans('modOrderDetail.gw') }}(單位重：<span id="subGw"></span>)</label>
											<input type="text" class="form-control input-sm" name="gw" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="gwu">{{ trans('modOrderDetail.gwu') }}</label>
											<input type="text" class="form-control input-sm" name="gwu" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbm">{{ trans('modOrderDetail.cbm') }}</label>
											<input type="text" class="form-control input-sm" name="cbm" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbmu">{{ trans('modOrderDetail.cbmu') }}</label>
											<input type="text" class="form-control input-sm" name="cbmu" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="length">{{ trans('modOrderDetail.length') }}</label>
											<input type="text" class="form-control input-sm" name="length" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="weight">{{ trans('modOrderDetail.weight') }}</label>
											<input type="text" class="form-control input-sm" name="weight" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="height">{{ trans('modOrderDetail.height') }}</label>
											<input type="text" class="form-control input-sm" name="height" grid="true" >
										</div>
										<div class="form-group col-md-3"> 
											<label for="sn_no">{{ trans('modOrderDetail.snNo') }}</label>
											<input type="text" class="form-control input-sm" name="sn_no" grid="true" >
										</div>
									</div>
								</div>
								<!-- /.box-body -->

								<div class="box-footer">
									@if(isset($id))
										<input type="hidden" class="form-control input-sm noClear"  name="ord_id" value="{{$id}}" grid="true">
									@endif
									<input type="hidden" class="form-control input-sm" name="id" grid="true">
									<button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
									<button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
								</div>
							</form>
						</div>

						<div id="jqxGrid"></div>
					</div>

					<div class="tab-pane" id="tab_4">
						<div class="box box-primary" id="sub1Box" style="display:none">
							<div class="box-header with-border">
								<h3 class="box-title">{{ trans('order.loadingPlan') }}</h3>
							</div>
							<form method="POST" accept-charset="UTF-8" id="sub1Form" enctype="multipart/form-data">
								<!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<div class="form-group col-md-3">
											<label for="goods_tnm">{{ trans('modOrderPack.goodsTnm') }}</label>
											<input type="text" class="form-control input-sm" name="goods_tnm" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="pack_no">{{ trans('modOrderPack.packNo') }}</label>
											<input type="text" class="form-control input-sm" name="pack_no" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbm">{{ trans('modOrderPack.cbm') }}</label>
											<input type="text" class="form-control input-sm" name="cbm" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbmu">{{ trans('modOrderPack.cbmu') }}</label>
											<input type="text" class="form-control input-sm" name="cbmu" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="cntr_no">{{ trans('modOrderPack.cntrNo') }}</label>
											<input type="text" class="form-control input-sm" name="cntr_no" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cntr_max_weight">{{ trans('modOrderPack.cntrMaxWeight') }}</label>
											<input type="text" class="form-control input-sm" name="cntr_max_weight" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="length">{{ trans('modOrderPack.length') }}</label>
											<input type="text" class="form-control input-sm" name="length" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="weight">{{ trans('modOrderPack.weight') }}</label>
											<input type="text" class="form-control input-sm" name="weight" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="height">{{ trans('modOrderPack.height') }}</label>
											<input type="text" class="form-control input-sm" name="height" grid="true" >
										</div>
									</div>
								</div>
								<!-- /.box-body -->

								<div class="box-footer">
									<input type="hidden" class="form-control input-sm" name="id" grid="true">
									<button type="button" class="btn btn-sm btn-primary" id="packSave">{{ trans('common.save') }}</button>
									<button type="button" class="btn btn-sm btn-danger" id="packCancel">{{ trans('common.cancel') }}</button>
								</div>
							</form>
						</div>
						<div id="packGrid"></div>
					</div>
				</div>
				<!-- /.tab-content -->
			</div>
		</div>	
	</div>

	<!-- Modal -->
	<div class="modal fade" id="packModal" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">{{ trans('modOrderPackDetail.packingContent') }}</h5>
		</div>
		<div class="modal-body">
			<div id="packDetailGrid"></div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('common.close') }}</button>
		</div>
		</div>
	</div>
	</div>


 @endsection 
 @include('backpack::template.lookup') 
 @section('after_scripts')
	<script>
		var mainId = "";
		var ord_no = "";
		var sysOrdNo = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt') }}";

		var fieldData = null;
		var fieldObj = null;

		@if(isset($crud -> create_fields))
		fieldData = '{{!! json_encode($crud->create_fields) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		//console.log(fieldObj);
		@endif



		@if(isset($id))

		mainId   = "{{$id}}";
		// editData = '{{!! $entry !!}}';
		// editData = editData.substring(1);
		// editData = editData.substring(0, editData.length - 1);
		// var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
		// editObj  = JSON.parse(objJson);
		// ord_no   = editObj.ord_no;
		// sysOrdNo = editObj.sys_ord_no;

		ord_no = "{{trim(preg_replace('/\s+/', ' ', $ord_no))}}";
		sysOrdNo = "{{$sys_ord_no}}";

		//console.log(editObj);
		@endif




		$(function () {
			$('#subBox button[btnName="goods_no"]').on('click', function(){
				$('#lookupModal').modal('show');
				initLookup('goods_no', "料號建檔", callBackFunc=function(data){
					$("#subBox input[name='goods_nm']").val(data.goods_nm);
					$("#subBox input[name='goods_no']").val(data.goods_no);
					$("#subBox input[name='goods_no2']").val(data.goods_no2);
					$("#subBox input[name='goods_nm2']").val(data.goods_nm2);
					$("#subBox input[name='gwu']").val(data.gwu);
					$("#subBox input[name='cbm']").val(data.cbm);
					$("#subBox input[name='cbmu']").val(data.cbmu);
					$("#subBox input[name='length']").val(data['g_length']);
					$("#subBox input[name='weight']").val(data['g_width']);
					$("#subBox input[name='height']").val(data['g_height']);
					$("#subBox input[name='gw']").attr("gw", data.gw);
					var pkgNum = parseInt($("#subBox input[name='pkg_num']").val()) || 0;
					if(pkgNum > 0) {
						$("#subBox input[name='gw']").val((data.gw * pkgNum).toFixed(2));
					}
					$("#subGw").text(data.gw);
				});
			});

			$('#subBox input[name="goods_no"]').on('click', function(){
				var check = $('#subBox input[name="goods_no"]').data('ui-autocomplete') != undefined;
				if(check == false) {
					initAutocomplete("subForm","goods_no",callBackFunc=function(data){
						$("#subBox input[name='goods_nm']").val(data.goods_nm);
						$("#subBox input[name='goods_no2']").val(data.goods_no2);
						$("#subBox input[name='goods_nm2']").val(data.goods_nm2);
						$("#subBox input[name='gwu']").val(data.gwu);
						$("#subBox input[name='cbm']").val(data.cbm);
						$("#subBox input[name='cbmu']").val(data.cbmu);
						$("#subBox input[name='length']").val(data['g_length']);
						$("#subBox input[name='weight']").val(data['g_width']);
						$("#subBox input[name='height']").val(data['g_height']);
						var pkgNum = parseInt($("#subBox input[name='pkg_num']").val()) || 0;
						$("#subBox input[name='gw']").attr("gw", data.gw);
						if(pkgNum > 0) {
							$("#subBox input[name='gw']").val((data.gw * pkgNum).toFixed(2));
						}
						$("#subGw").text(data.gw);
						
					},"goods_no");
				}
			});

			$("#subBox input[name='pkg_num']").on("change", function(){
				var gw = parseFloat($("#subBox input[name='gw']").attr("gw")) || 0;
				var pkgNum = parseInt($("#subBox input[name='pkg_num']").val()) || 0;

				$("#subBox input[name='gw']").val((gw * pkgNum).toFixed(2));
			});


			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt') }}";
			formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_order') }}/" + mainId;
			formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt') }}";
			formOpt.afterInit = function() {
				@cannot('OrdControl')
				menuBtnFunc.disabled(['iSave', 'iCancel','iAdd', 'iEdit', 'iCopy', 'iDel']);
				@endcan
				if(editObj != null) {
					if(editObj.status == "CLOSE") {
						menuBtnFunc.disabled(['iSave', 'iCancel','iAdd', 'iEdit', 'iCopy', 'iDel']);
					}
				}
			}

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.beforeDelFunc = function() {
				var status = $("#status").val();

				if(status != "UNTREATED") {
					alert('狀態不在「尚未安排」，故無法刪除');
					return false;
				}

				return true;
			}

			formOpt.afterDel = function() {
				$('#jqxGrid').jqxGrid('clear');
				$('#packGrid').jqxGrid('clear');
				$('#packDetailGrid').jqxGrid('clear');
			}

			formOpt.addFunc = function() {
				$('#jqxGrid').jqxGrid('clear');
				$('#packGrid').jqxGrid('clear');
				$("#subPanel").hide();

				$("#truck_cmp_no").val(truck_cmp_no);
				$("#truck_cmp_nm").val(truck_cmp_nm);
				$("#wh_addr").val(whAddr);
				$("#sys_ord_no").val('');
				$("#ord_no").val('');

				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();

				if(dd<10) {
					dd = '0'+dd
				} 

				if(mm<10) {
					mm = '0'+mm
				} 

				today = yyyy+'-'+mm+'-'+dd;
				$("#etd").val(today);
				$("#status").val("UNTREATED");
				//$("#trs_mode").val("NON");
			}

			formOpt.editFunc = function() {
				$("#subPanel").show();
				$("#jqxGrid").jqxGrid({'showtoolbar': false});
				$("#packGrid").jqxGrid({'showtoolbar': false});
			}

			formOpt.copyFunc = function() {
				$("#subPanel").hide();
				$("#sys_ord_no").val('');
				$("#ord_no").val('');
				$("#status").val("UNTREATED");
				$("#amt").val('');
				$("#cust_amt").val('');
				$("#distance").val('');
				$("#pkg_num").val('');
				$("#total_gw").val('');
				$("#total_cbm").val('');
				$("#amt_remark").val('');
			}

			formOpt.saveSuccessFunc = function(data) {
				$("#jqxGrid").jqxGrid({'showtoolbar': true});
				$("#packGrid").jqxGrid({'showtoolbar': true});

				if(typeof data.dist !== "undefined" && data.dist > 0) {
					$("#distance").val(data.dist);
				}
			}

			var btnGroup = [
				{
					btnId: "btnShowImg",
					btnIcon: "fa fa-picture-o",
					btnText: "簽收照片",
					btnFunc: function () {
						window.open("{{ url(config('backpack.base.route_prefix', 'admin').'/show') }}" + "/" + $("#sys_ord_no").val() + '/FINISH', '簽收照片', config='height=500,width=600');
					}
				},
				{
					btnId: "btnShowErrorImg",
					btnIcon: "fa fa-picture-o",
					btnText: "異常照片",
					btnFunc: function () {
						window.open("{{ url(config('backpack.base.route_prefix', 'admin').'/show') }}" + "/" + $("#sys_ord_no").val() + '/ERROR', '異常照片', config='height=500,width=600');
					}
				}
			];

			initBtn(btnGroup);

			$.ajax({
				url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_detail') }}",
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {

					var col = [
						[
							{name: "id", type: "number"},
							{name: "ord_no", type: "string"},
							{name: "goods_no", type: "string"},
							{name: "goods_no2", type: "string"},
							{name: "goods_nm", type: "string"},
							{name: "pkg_num", type: "number"},
							{name: "pkg_unit", type: "string"},
							{name: "gw", type: "number"},
							{name: "gwu", type: "string"},
							{name: "cbm", type: "number"},
							{name: "cbmu", type: "string"},
							{name: "length", type: "number"},
							{name: "weight", type: "number"},
							{name: "height", type: "number"},
							{name: "sn_no", type: "string"},
							{name: "ord_id", type: "number"},
						],
						[
							{text: "{{ trans('modOrderDetail.id') }}", datafield: "id", width: 100, hidden: true},
							{text: "{{ trans('modOrderDetail.ordNo') }}", datafield: "ord_no", width: 130, nullable: false,hidden:true},
							{text: "{{ trans('modOrderDetail.goodsNo') }}", datafield: "goods_no", width: 150, nullable: false},
							{text: "{{ trans('modOrderDetail.goodsNo2') }}", datafield: "goods_no2", width: 150, nullable: false},
							{text: "{{ trans('modOrderDetail.goodsNm') }}", datafield: "goods_nm", width: 150, nullable: false},
							{text: "{{ trans('modOrderDetail.pkgNum') }}", datafield: "pkg_num", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.pkgUnit') }}", datafield: "pkg_unit", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.gw') }}", datafield: "gw", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.gwu') }}", datafield: "gwu", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.cbm') }}", datafield: "cbm", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.cbmu') }}", datafield: "cbmu", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.length') }}", datafield: "length", width: 80, nullable: false},
							{text: "{{ trans('modOrderDetail.weight') }}", datafield: "weight", width: 80, nullable: false},
							{text: "{{ trans('modOrderDetail.height') }}", datafield: "height", width: 80, nullable: false},
							{text: "{{ trans('modOrderDetail.snNo') }}", datafield: "sn_no", width: 80, nullable: false},
							{text: "{{ trans('modOrderDetail.ordId') }}", datafield: "ord_id", width: 80, hidden: true},
						]
					];
					
					var opt = {};
					opt.gridId = "jqxGrid";
					opt.fieldData = col;
					opt.formId = "subForm";
					opt.saveId = "Save";
					opt.cancelId = "Cancel";
					opt.showBoxId = "subBox";
					opt.height = 300;
					opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/orderDetail/get') }}" + '/' + mainId;
					opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderDetail') }}" + "/store";
					opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderDetail') }}" + "/update";
					opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderDetail') }}" + "/delete/";
					opt.commonBtn = true;
					@can('OrdControl')
						opt.showtoolbar = true;
					@endcan
					opt.defaultKey = {
						'ord_no': ord_no
					};
					opt.beforeSave = function (row) {
						if(row.pkg_num == "" || row.pkg_num == null) {
							swal("警告", "數量不能為空", "warning");
							return false;
						}

						if(row.goods_nm == "" || row.goods_nm == null) {
							swal("警告", "商品名稱不能為空", "warning");
							return false;
						}
						return true;
					}

					opt.afterSave = function (data) {
						var sumPkgNum = $("#jqxGrid").jqxGrid('getcolumnaggregateddata', 'pkg_num', ['sum']);
						var sumGw = $("#jqxGrid").jqxGrid('getcolumnaggregateddata', 'gw', ['sum']);
						var sumCbm = $("#jqxGrid").jqxGrid('getcolumnaggregateddata', 'cbm', ['sum']);
						$("#pkg_num").val(sumPkgNum.sum);
						$("#total_gw").val(sumGw.sum);
						$("#total_cbm").val(sumCbm.sum);
						$("#subGw").text("");
					}

					opt.beforeCancel = function() {
						$("#subGw").text("");
					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			});

			$.ajax({
				url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_pack') }}",
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {
					$.each(fieldData[1], function(i, v){
						if(fieldData[1][i]['text'] == 'cbm') {
							fieldData[1][i]['aggregates'] = ['sum'];
							console.log(fieldData[1][i]['aggregates']);
						}
					});

					var opt = {};
					opt.gridId = "packGrid";
					opt.fieldData = fieldData;
					opt.formId = "sub1Form";
					opt.saveId = "packSave";
					opt.cancelId = "packCancel";
					opt.showBoxId = "sub1Box";
					opt.height = 300;
					opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/orderPack/get') }}" + '/' + ord_no;
					opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderPack') }}" + "/store";
					opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderPack') }}" + "/update";
					opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderPack') }}" + "/delete/";
					opt.showaggregates = true;
					opt.commonBtn = true;
					opt.inlineEdit = false;
					opt.showtoolbar = true;
					opt.custBtn = [
						{
							title: "{{ trans('modOrderPackDetail.packingContent') }}",
							func: function() {
								var selectedrowindex = $("#packGrid").jqxGrid('getselectedrowindex');
								if(selectedrowindex == -1) {
									swal("{{ trans('modOrder.msg1') }}", "", "warning");
									return;
								}
								$('#packDetailGrid').jqxGrid('updatebounddata');
								$('#packModal').modal('show');
							}
						}
					];
					opt.defaultKey = {
						'ord_no': ord_no
					};
					opt.beforeSave = function (formData) {

					}

					opt.afterSave = function (data) {

					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			});


			var opt = {};
			var custFieldData = [];
			custFieldData[0] = [
				{name: 'id', type: 'integer'},
				{name: 'goods_no', type: 'string'},
				{name: 'goods_nm', type: 'string'},
				{name: 'num', type: 'integer'},
				{name: 'last_num', type: 'integer'},
			];
			custFieldData[1] = [
				{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false},
				{text:"{{ trans('modOrderDetail.goodsNo') }}", datafield: 'goods_no', filtertype:"textbox", width:100, editable: false},
				{text:"{{ trans('modOrderDetail.goodsNm') }}", datafield: 'goods_nm', filtertype:"textbox", width:100, editable: false},
				{text:"{{ trans('modOrderPackDetail.num') }}", datafield: 'num', filtertype:"number", width:100},
				{text:"{{ trans('modOrder.lastNum') }}", datafield: 'last_num', filtertype:"number", width:100, editable: false}
			];
			opt.gridId = "packDetailGrid";
			opt.fieldData = custFieldData;
			opt.inlineEdit = true;
			opt.height = 300;
			opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/orderPackDetail/get') }}" + '/' + ord_no;
			opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderPackDetail') }}" + "/update";
			opt.defaultKey = {
				'ord_no': ord_no
			};
			opt.commonBtn = false;
			opt.beforeSave = function (rowData) {
				var returnMsg = [];
				returnMsg[0] = true;
				returnMsg[1] = rowData;
				var num = rowData.num;
				var last_num = rowData.last_num;
				if(num == 0) {
					returnMsg[0] = false;
					return returnMsg;
				}
				if(num > last_num) {
					swal("{{ trans('modOrder.msg2') }}", "", "warning");
					returnMsg[0] = false;
					return returnMsg;
				}

				var selectedrowindex = $("#packGrid").jqxGrid('getselectedrowindex');
				var packData = $("#packGrid").jqxGrid('getrowdata', selectedrowindex);
				rowData = {pack_no: packData.pack_no, ord_no: packData.ord_no, ord_detail_id: rowData.ord_detail_id, num: rowData.num, id: rowData.id, goods_no: rowData.goods_no};
				returnMsg[1] = rowData;
				return returnMsg;
			}

			opt.afterSave = function (data, rowid, newdata) {
				
				newdata.last_num = data.data;
				return newdata;
			}

			genDetailGrid(opt);
		});
	</script>
@endsection
