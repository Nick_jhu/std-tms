@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	{{ trans('modOrder.orderInfo') }}<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url(config('backpack.base.route_prefix'),'OrderMgmt1') }}">{{ trans('modOrder.titleName') }}</a></li>
		<li class="active">{{ trans('modOrder.titleAddName') }}</li>
	</ol>
</section>
<script>
	var truck_cmp_no = "";
	var truck_cmp_no = "";
	var whAddr = "";

	@if(isset($carData))
		truck_cmp_no = "{{$carData->cust_no}}";
		truck_cmp_nm = "{{$carData->cname}}";
		whAddr       = "{{$carData->city_nm.$carData->area_nm.$carData->address}}";
	@endif
</script>
@endsection 
@section('content')
	@include('backpack::template.toolbar')
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4>{{ trans('backpack::crud.please_fix') }}</h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.basicInfo') }}</a></li>
						<li class=""><a href="#tab_7" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.pickdlvinfo') }}</a></li>
						<!-- <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.pickupInfo') }}</a></li>
						<li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.deliveryInfo') }}</a></li> -->
						<li class="active"><a href="#tab_5" data-toggle="tab" aria-expanded="true">{{ trans('modOrder.amtInfo') }}</a></li>
					</ul>
					<div class="tab-content">
						<div id="tab_1" class="tab-pane">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-3">
										<label for="sys_ord_no">{{ trans('modOrder.sysOrdNo') }}</label>
										<input type="text" class="form-control" id="sys_ord_no" name="sys_ord_no" switch="off">
									</div>
									<div class="form-group col-md-3">
										<label for="ord_no">{{ trans('modOrder.ordNo') }}</label>
										@if($close_status=="Y")
										<input type="text" class="form-control" id="ord_no" name="ord_no" switch="off">
										@endif
										@if($close_status!="Y")
										<input type="text" class="form-control" id="ord_no" name="ord_no">
										@endif
									</div>
									<div class="form-group col-md-3">
										<label for="cust_ord_no">{{ trans('modOrder.custOrdNo') }}</label>
										<input type="text" class="form-control" id="cust_ord_no" name="cust_ord_no" switch="off">
									</div>
									<div class="form-group col-md-3">
										<label for="wms_order_no">{{ trans('modOrder.wmsOrderNo') }}</label>
										<input type="text" class="form-control" id="wms_order_no" name="wms_order_no" switch="off">
									</div>
									<!-- <div class="form-group col-md-3">
                                        <label for="status">{{ trans('modOrder.wmsorderno') }}</label>
										<input type="text" class="form-control" id="wms_order_no" name="wms_order_no">
                                    </div>   -->

								</div>
								<div class="row">
									<div class="form-group col-md-3">
										<label for="status">{{ trans('modOrder.status') }}</label>
										<select class="form-control" id="status" name="status" switch="off">
                                            <option value="UNTREATED">{{ trans('modOrder.STATUS_UNTREATED') }}</option>
                                            <option value="SEND">{{ trans('modOrder.STATUS_SEND') }}</option>
                                            <option value="LOADING">{{ trans('modOrder.STATUS_LOADING') }}</option>
                                            <option value="SETOFF">{{ trans('modOrder.STATUS_SETOFF') }}</option>
                                            <option value="NORMAL">{{ trans('modOrder.STATUS_NORMAL') }}</option>
                                            <option value="PICKED">{{ trans('modOrder.STATUS_PICKED') }}</option>
                                            <option value="ERROR">{{ trans('modOrder.STATUS_ERROR') }}</option>
                                            <option value="DELAY">{{ trans('modOrder.STATUS_DELAY') }}</option>
                                            <option value="FINISHED">{{ trans('modOrder.STATUS_FINISHED') }}</option>
											<option value="CLOSE">{{ trans('modOrder.STATUS_CLOSE') }}</option>
											<option value="REJECT">{{ trans('modOrder.STATUS_REJECT') }}</option>
                                        </select>
									</div>
									<div class="form-group col-md-3">
										<label for="etd">{{ trans('modOrder.etd') }}</label>
										@if($close_status=="Y")
										<input type="text" class="form-control" id="etd" name="etd" switch="off">
										@endif
										@if($close_status!="Y")
										<input type="text" class="form-control" id="etd" name="etd">
										@endif
									</div>
									<div class="form-group col-md-3">
										<label for="trs_mode">{{ trans('modOrder.trsMode') }}</label>
										@if($close_status=="Y")
										<select class="form-control" id="trs_mode" name="trs_mode" no-clear="Y"switch="off">
												
										</select>
										@endif
										
										@if($close_status!="Y")
										<select class="form-control" id="trs_mode" name="trs_mode" no-clear="Y">
												
										</select>
										@endif
									</div>
									<div class="form-group col-md-3">
									<label for="temperate">{{ trans('modOrder.temperate') }}</label>
									@if($close_status=="Y")
									<select class="form-control" id="temperate" name="temperate" no-clear="Y"switch="off">
                                            
									</select>
									@endif
									@if($close_status!="Y")
									<select class="form-control" id="temperate" name="temperate" no-clear="Y">
                                            
									</select>
									@endif                                   
									</div>
									{{-- <div class="form-group col-md-3">
										<label for="s_key">{{ trans('modOrder.sKey') }}</label>
										@if($close_status=="Y")
										<select class="form-control" id="s_key" name="s_key" no-clear="Y"switch="off">	
											@foreach($skeydata as $row)
												<option value="{{$row->cust_no}}">{{$row->cmp_abbr}}</option>
											@endforeach									
										</select>  
										@endif
										@if($close_status!="Y")
										<select class="form-control" id="s_key" name="s_key" no-clear="Y">	
											@foreach($skeydata as $row)
												<option value="{{$row->cust_no}}">{{$row->cmp_abbr}}</option>
											@endforeach									
										</select>
										@endif                                     
									</div> --}}
								</div>

								<div class="row">
									<!-- <div class="form-group col-md-3">
										<label for="truck_cmp_no">{{ trans('modOrder.truckCmpNo') }}</label>
										<input type="text" class="form-control" id="truck_cmp_no" name="truck_cmp_no">
									</div> -->
									<div class="form-group col-md-3">
										<input type="hidden" class="form-control" id="truck_cmp_no" name="truck_cmp_no">
										<label for="truck_cmp_nm">{{ trans('modOrder.truckCmpNm') }}</label>
										@if($close_status=="Y")
										<input type="text" class="form-control" id="truck_cmp_nm" name="truck_cmp_nm"switch="off">									
										</select>  
										@endif
										@if($close_status!="Y")
										<input type="text" class="form-control" id="truck_cmp_nm" name="truck_cmp_nm">
										@endif 
									</div>
									<div class="form-group col-md-6">
										<label for="wh_addr">{{ trans('modOrder.whAddr') }}</label>
										@if($close_status=="Y")
										<input type="text" class="form-control" id="wh_addr" name="wh_addr"switch="off">									
										</select>  
										@endif
										@if($close_status!="Y")
										<input type="text" class="form-control" id="wh_addr" name="wh_addr">
										@endif 
									</div>
								</div>

								<div class="row">
									<!-- <div class="form-group col-md-3">
										<label for="owner_cd">{{ trans('modOrder.ownerCd') }}</label>
										<input type="text" class="form-control" id="owner_cd" name="owner_cd">
									</div> -->
									<div class="form-group col-md-3">
										<input type="hidden" class="form-control" id="owner_cd" name="owner_cd">
										<label for="owner_nm">{{ trans('modOrder.ownerNm') }}</label>
										@if($close_status=="Y")
										<input type="text" class="form-control" id="owner_nm" name="owner_nm"switch="off">									
										@endif
										@if($close_status!="Y")
										<input type="text" class="form-control" id="owner_nm" name="owner_nm">
										@endif
									</div>
									<div class="form-group col-md-3">
										<label for="status">{{ trans('modOrder.ownerSendMail') }}</label>
										@if($close_status=="Y")
                                        <select class="form-control" id="owner_send_mail" name="owner_send_mail"switch="off">
                                            <option value="N">{{ trans('common.no') }}</option>
                                            <option value="Y">{{ trans('common.yes') }}</option>
                                        </select>								
										@endif
										@if($close_status!="Y")
                                        <select class="form-control" id="owner_send_mail" name="owner_send_mail">
                                            <option value="N">{{ trans('common.no') }}</option>
                                            <option value="Y">{{ trans('common.yes') }}</option>
                                        </select>
										@endif
									</div>
									<div class="form-group col-md-3">
                                        <label for="status">{{ trans('modOrder.is_urgent') }}</label>
                                        <select class="form-control" id="is_urgent" name="is_urgent" no-clear="Y" switch="off">
                                            <option value="N">{{ trans('common.no') }}</option>
                                            <option value="Y">{{ trans('common.yes') }}</option>
                                        </select>
									</div>
									<div class="form-group col-md-3">
										<label for="s_key">{{ trans('modOrder.sKey') }}</label>
											<select class="form-control" id="s_key" name="s_key" no-clear="Y" switch="off">	
												@foreach($skeydata as $row)
													<option value="{{$row->cust_no}}">{{$row->cmp_abbr}}</option>
												@endforeach									
											</select>                                      
									</div>
								</div>
								{{-- <div class="row">
									<div class="form-group col-md-12">
										<label for="order_tally">{{ trans('modOrder.order_tally') }}</label>
										@if($close_status=="Y")
										<textarea class="form-control" rows="1" name="order_tally"switch="off"></textarea>								
										@endif
										@if($close_status!="Y")
										<textarea class="form-control" rows="1" name="order_tally"></textarea>
										@endif
									</div>
								</div> --}}
								<div class="row">
									<div class="form-group col-md-12">
										<label for="remark">{{ trans('modOrder.remark') }}</label>
										@if($close_status=="Y")
										<textarea class="form-control" rows="3" name="remark"></textarea>								
										@endif
										@if($close_status!="Y")
										<textarea class="form-control" rows="3" name="remark"></textarea>
										@endif
									</div>
								</div>


								@if(isset($id))
								<input type="hidden" name="id" value="{{$id}}" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								@endif

							</div>
						</div>
						<div id="tab_5" class="tab-pane active">
							<div class="row">
								<div class="form-group col-md-4">
									<label for="amt">{{ trans('modOrder.amt') }}</label>
									<input type="text" class="form-control" id="amt" name="amt" switch="off">
								</div>
								<div class="form-group col-md-4">
									<label for="cust_amt">{{ trans('modOrder.custAmt') }}</label>
									<input type="text" class="form-control" id="cust_amt" name="cust_amt">
								</div>
								<div class="form-group col-md-4">
									<label for="distance">{{ trans('modOrder.distance') }}</label>
									<input type="text" class="form-control" id="distance" name="distance" switch="off">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-4">
									<label for="pkg_num">{{ trans('modOrder.pkgNum') }}</label>
									<input type="text" class="form-control" id="pkg_num" name="pkg_num" switch="off">
								</div>
								<div class="form-group col-md-4">
									<label for="total_gw">{{ trans('modOrder.totalGw') }}</label>
									<input type="text" class="form-control" id="total_gw" name="total_gw" switch="off">
								</div>
								<div class="form-group col-md-4">
									<label for="total_cbm">{{ trans('modOrder.totalCbm') }}</label>
									<input type="text" class="form-control" id="total_cbm" name="total_cbm" switch="off">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-4">
									<label for="cust_amt">{{ trans('modOrder.collectamt') }}</label>
									@if($close_status=="Y")
									<input type="text" class="form-control" id="collectamt" name="collectamt"switch="off">
									@endif
									@if($close_status!="Y")
									<input type="text" class="form-control" id="collectamt" name="collectamt">
									@endif
								</div>
								<div class="form-group col-md-4">
									<label for="cust_amt">{{ trans('modOrder.total_amount') }}</label>
									<input type="text" class="form-control" id="total_amount" name="total_amount" readonly='true'>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-12">
									<label for="amt_remark">{{ trans('modOrder.amtRemark') }}</label>
									<input type="text" class="form-control" id="amt_remark" name="amt_remark">
								</div>
							</div>
						</div>
						<div id="tab_7" class="tab-pane">
							<div class="row">
								<div class="col-md-12">
									<div class="box box-primary" id="statusDiv">
										<div class="box-header with-border">
										<h3 class="box-title">{{ trans('modOrder.pickupInfo') }}</h3>
											<div class="box-tools">
												<button id="testbtn1" type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-minus"></i>
												</button>
											</div>
										</div>
										<div class='box-body no-padding'>
											<div class="row">
												<div class="form-group col-md-3">
													<input type="text" class="form-control" id="pick_cust_no" style="display:none" name="pick_cust_no">
													<label for="pick_cust_nm">{{ trans('modOrder.pickCustNm') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="pick_cust_nm" name="pick_cust_nm"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="pick_cust_nm" name="pick_cust_nm">
													@endif
												</div>
												<div class="form-group col-md-3">
													<label for="pick_attn">{{ trans('modOrder.pickAttn') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="pick_attn" name="pick_attn"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="pick_attn" name="pick_attn">
													@endif
												</div>
												<div class="form-group col-md-3">
													<label for="pick_tel">{{ trans('modOrder.pickTel') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="pick_tel" name="pick_tel"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="pick_tel" name="pick_tel">
													@endif
												</div>
												<div class="form-group col-md-3">
													<label for="pick_tel">{{ trans('modOrder.pickTel2') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="pick_tel2" name="pick_tel2"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="pick_tel2" name="pick_tel2">
													@endif
												</div>
											</div>
										
											<div class="row">
												<div class="form-group col-md-3">
													<label for="pick_zip">{{ trans('modOrder.pickZip') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="pick_zip" name="pick_zip"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="pick_zip" name="pick_zip">
													@endif
												</div>
												<div class="form-group col-md-3">
													<label for="pick_city_nm">{{ trans('modOrder.pickCityAreaNm') }}</label>
													<input type="text" class="form-control" id="pick_info" name="pick_info" switch="off">
													<input type="hidden" class="form-control" id="pick_area_id" name="pick_area_id">
													<input type="hidden" class="form-control" id="pick_city_nm" name="pick_city_nm">
													<input type="hidden" class="form-control" id="pick_area_nm" name="pick_area_nm">
													<input type="hidden" class="form-control" id="pick_addr_info" name="pick_addr_info">
												</div>
												<div class="form-group col-md-6">
													<label for="pick_addr">{{ trans('modOrder.pickAddr') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="pick_addr" name="pick_addr"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="pick_addr" name="pick_addr">
													@endif
													<input type="hidden" class="form-control" id="pick_lat" name="pick_lat">
													<input type="hidden" class="form-control" id="pick_lng" name="pick_lng">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6">
													<label for="pick_remark">{{ trans('modOrder.pickRemark') }}</label>
													@if($close_status=="Y")
													<textarea class="form-control" rows="3" name="pick_remark" id="pick_remark"switch="off"></textarea>
													@endif
													@if($close_status!="Y")
													<textarea class="form-control" rows="3" name="pick_remark" id="pick_remark"></textarea>
													@endif
												</div>
												<div class="form-group col-md-3">
													<label for="pick_email">{{ trans('modOrder.pickEmail') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="pick_email" name="pick_email"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="pick_email" name="pick_email">
													@endif
												</div>
												<div class="form-group col-md-3">
													<label for="status">{{ trans('modOrder.pickSendMail') }}</label>
													@if($close_status=="Y")
													<select class="form-control" id="pick_send_mail2" name="pick_send_mail"switch="off">
														<option value="N">{{ trans('common.no') }}</option>
														<option value="Y">{{ trans('common.yes') }}</option>
													</select>
													@endif
													@if($close_status!="Y")
													<select class="form-control" id="pick_send_mail2" name="pick_send_mail">
														<option value="N">{{ trans('common.no') }}</option>
														<option value="Y">{{ trans('common.yes') }}</option>
													</select>
													@endif
												</div>  
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="box box-primary" id="statusDiv">
										<div class="box-header with-border">
										<h3 class="box-title">{{ trans('modOrder.deliveryInfo') }}</h3>
											<div class="box-tools">
												<button id="testbtn1" type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-minus"></i>
												</button>
											</div>
										</div>
										<div class='box-body no-padding'>
											<div class="row">
												<div class="form-group col-md-3">
													<input type="text" class="form-control" id="dlv_cust_no" style="display:none" name="dlv_cust_no">
													<label for="dlv_cust_nm">{{ trans('modOrder.dlvCustNm') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="dlv_cust_nm" name="dlv_cust_nm"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="dlv_cust_nm" name="dlv_cust_nm">
													@endif
												</div>
												<div class="form-group col-md-3">
													<label for="dlv_attn">{{ trans('modOrder.dlvAttn') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="dlv_attn" name="dlv_attn"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="dlv_attn" name="dlv_attn">
													@endif
												</div>
												<div class="form-group col-md-3">
													<label for="dlv_tel">{{ trans('modOrder.dlvTel') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="dlv_tel" name="dlv_tel"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="dlv_tel" name="dlv_tel"switch="off">
													@endif
												</div>
												<div class="form-group col-md-3">
													<label for="dlv_tel">{{ trans('modOrder.dlvTel2') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="dlv_tel2" name="dlv_tel2"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="dlv_tel2" name="dlv_tel2"switch="off">
													@endif
												</div>
											</div>
										
											<div class="row">
												<div class="form-group col-md-3">
													<label for="dlv_zip">{{ trans('modOrder.dlvZip') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="dlv_zip" name="dlv_zip"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="dlv_zip" name="dlv_zip">
													@endif
												</div>
												<div class="form-group col-md-3">
													<label for="dlv_city_nm">{{ trans('modOrder.dlvCityAreaNm') }}</label>
													<input type="text" class="form-control" id="dlv_info" name="dlv_info"switch="off">
													<input type="hidden" class="form-control" id="dlv_area_id" name="dlv_area_id">
													<input type="hidden" class="form-control" id="dlv_city_nm" name="dlv_city_nm">
													<input type="hidden" class="form-control" id="dlv_area_nm" name="dlv_area_nm">
													<input type="hidden" class="form-control" id="dlv_addr_info" name="dlv_addr_info">
												</div>
												<!-- <div class="form-group col-md-3">
													<input type="text" class="form-control" id="dlv_city_nm" name="dlv_city_nm">
												</div> -->
												<div class="form-group col-md-6">
													<label for="dlv_addr">{{ trans('modOrder.dlvAddr') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="dlv_addr" name="dlv_addr"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="dlv_addr" name="dlv_addr">
													@endif
													<input type="hidden" class="form-control" id="dlv_lat" name="dlv_lat">
													<input type="hidden" class="form-control" id="dlv_lng" name="dlv_lng">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6">
													<label for="dlv_remark">{{ trans('modOrder.dlvRemark') }}</label>
													<textarea class="form-control" rows="3" name="dlv_remark" id="dlv_remark" switch="off"></textarea>
												</div>
												<div class="form-group col-md-3">
													<label for="dlv_email">{{ trans('modOrder.dlvEmail') }}</label>
													@if($close_status=="Y")
													<input type="text" class="form-control" id="dlv_email" name="dlv_email"switch="off">
													@endif
													@if($close_status!="Y")
													<input type="text" class="form-control" id="dlv_email" name="dlv_email">
													@endif
												</div>
												<div class="form-group col-md-3">
													<label for="status">{{ trans('modOrder.dlvSendMail') }}</label>
													@if($close_status=="Y")
													<select class="form-control" id="dlv_send_mail2" name="dlv_send_mail"switch="off">
														<option value="N">{{ trans('common.no') }}</option>
														<option value="Y">{{ trans('common.yes') }}</option>
													</select>
													@endif
													@if($close_status!="Y")
													<select class="form-control" id="dlv_send_mail2" name="dlv_send_mail">
														<option value="N">{{ trans('common.no') }}</option>
														<option value="Y">{{ trans('common.yes') }}</option>
													</select>
													@endif
												</div>  
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>

			<div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
				<ul class="nav nav-tabs">
					<li class=""><a href="#tab_6" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.orderDetail') }}</a></li>
					<li class="active"><a href="#tab_9" data-toggle="tab" aria-expanded="true">{{ trans('modOrder.amtDetail') }}</a></li>
					<li class=""><a href="#tab_8" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.imgInfo') }}</a></li>
					<li class=""><a href="#tab_10" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.ordInfo') }}</a></li>
					{{--  <li><a href="#tab_4" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.loadingPlan') }}</a></li>  --}}
				</ul>
				<div class="tab-content">
					<div class="tab-pane" id="tab_6">
						@if($close_status!="Y")
						<div class="box box-primary" id="subBox" style="display:none">
							<div class="box-header with-border">
								<h3 class="box-title">{{ trans('modOrder.orderDetail') }}</h3>
							</div>
							<form method="POST" accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
								<!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<div class="form-group col-md-2">
											<label for="goods_no">{{ trans('modOrderDetail.goodsNo') }}</label>
											{{--  <input type="text" class="form-control input-sm" name="goods_no" grid="true" >  --}}
											<div class="input-group input-group-sm">
												<input type="text" class="form-control" id="goods_no" name="goods_no">
												<span class="input-group-btn">
													<button type="button" class="btn btn-default btn-flat lookup" btnname="goods_no"
														info1="{{Crypt::encrypt('mod_goods')}}" 
														info2="{{Crypt::encrypt('goods_no+goods_nm,goods_no,goods_nm,gw,gwu,cbm,cbmu,g_length,g_height,g_width,goods_no2')}}" 
														info3="{{Crypt::encrypt('')}}"
														info4="goods_no=goods_no;goods_nm=goods_nm;gw=gw;gwu=guw;cbm=cbm;cbmu=cbmu;g_length=g_length;g_height=g_height;g_width=g_width;goods_no2=goods_no2;" triggerfunc="" selectionmode="singlerow">
														<i class="fa fa-search" aria-hidden="true"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="form-group col-md-2">
												<label for="goods_no2">{{ trans('modOrderDetail.goodsNo2') }}</label>
												<input type="text" class="form-control input-sm" id="goods_no2" name="goods_no2" grid="true" required="required">
											</div>
										<div class="form-group col-md-2">
											<label for="goods_nm">{{ trans('modOrderDetail.goodsNm') }}</label>
											<input type="text" class="form-control input-sm"  id="goods_nm" name="goods_nm" grid="true" required="required">
										</div>
										<div class="form-group col-md-2">
											<label for="pkg_num">{{ trans('modOrderDetail.pkgNum') }}</label>
											<input type="text" class="form-control input-sm"  name="pkg_num" grid="true" >
										</div>
										<div class="form-group col-md-2">
											<label for="pkg_unit">{{ trans('modOrderDetail.pkgUnit') }}</label>
                                            <input type="text" class="form-control input-sm" name="pkg_unit" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="gw">{{ trans('modOrderDetail.gw') }}(單位重：<span id="subGw"></span>)</label>
											<input type="text" class="form-control input-sm" name="gw" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="gwu">{{ trans('modOrderDetail.gwu') }}</label>
											<input type="text" class="form-control input-sm"  id="gwu"  name="gwu" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbm">{{ trans('modOrderDetail.cbm') }}</label>
											<input type="text" class="form-control input-sm" id="cbm" name="cbm" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbmu">{{ trans('modOrderDetail.cbmu') }}</label>
											<input type="text" class="form-control input-sm" id="cbmu" name="cbmu" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="length">{{ trans('modOrderDetail.length') }}</label>
											<input type="text" class="form-control input-sm" id="g_length" name="length" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="weight">{{ trans('modOrderDetail.weight') }}</label>
											<input type="text" class="form-control input-sm" id="g_width" name="weight" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="height">{{ trans('modOrderDetail.height') }}</label>
											<input type="text" class="form-control input-sm" id="g_height" name="height" grid="true" >
										</div>
										<div class="form-group col-md-3"> 
											<label for="sn_no">{{ trans('modOrderDetail.snNo') }}</label>
											<input type="text" class="form-control input-sm" name="sn_no" grid="true" >
										</div>
									</div>
								</div>
								<!-- /.box-body -->

								<div class="box-footer">
									@if(isset($id))
										<input type="hidden" class="form-control input-sm noClear"  name="ord_id" value="{{$id}}" grid="true">
									@endif
									<input type="hidden" class="form-control input-sm" name="id" grid="true">
									<button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
									<button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
								</div>
							</form>
						</div>
						@endif
						<div id="jqxGrid"></div>
					</div>

					<div class="tab-pane" id="tab_4">
						<div class="box box-primary" id="sub1Box" style="display:none">
							<div class="box-header with-border">
								<h3 class="box-title">{{ trans('order.loadingPlan') }}</h3>
							</div>
							<form method="POST" accept-charset="UTF-8" id="sub1Form" enctype="multipart/form-data">
								<!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<div class="form-group col-md-3">
											<label for="goods_tnm">{{ trans('modOrderPack.goodsTnm') }}</label>
											<input type="text" class="form-control input-sm" name="goods_tnm" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="pack_no">{{ trans('modOrderPack.packNo') }}</label>
											<input type="text" class="form-control input-sm" name="pack_no" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbm">{{ trans('modOrderPack.cbm') }}</label>
											<input type="text" class="form-control input-sm" name="cbm" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbmu">{{ trans('modOrderPack.cbmu') }}</label>
											<input type="text" class="form-control input-sm" name="cbmu" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="cntr_no">{{ trans('modOrderPack.cntrNo') }}</label>
											<input type="text" class="form-control input-sm" name="cntr_no" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cntr_max_weight">{{ trans('modOrderPack.cntrMaxWeight') }}</label>
											<input type="text" class="form-control input-sm" name="cntr_max_weight" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="length">{{ trans('modOrderPack.length') }}</label>
											<input type="text" class="form-control input-sm" name="length" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="weight">{{ trans('modOrderPack.weight') }}</label>
											<input type="text" class="form-control input-sm" name="weight" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="height">{{ trans('modOrderPack.height') }}</label>
											<input type="text" class="form-control input-sm" name="height" grid="true" >
										</div>
									</div>
								</div>
								<!-- /.box-body -->

								<div class="box-footer">
									<input type="hidden" class="form-control input-sm" name="id" grid="true">
									<button type="button" class="btn btn-sm btn-primary" id="packSave">{{ trans('common.save') }}</button>
									<button type="button" class="btn btn-sm btn-danger" id="packCancel">{{ trans('common.cancel') }}</button>
								</div>
							</form>
						</div>
						<div id="packGrid"></div>
					</div>

					<div class="tab-pane active" id="tab_9">
						<div class="box box-primary" id="subBox3">
							<div class="box-header with-border">
								<h3 class="box-title">{{ trans('modOrder.amtDetail') }}</h3>
							</div>
							<form method="POST" accept-charset="UTF-8" id="subForm3" enctype="multipart/form-data">
								<!-- /.box-header -->
								<div class="box-body">
									<div class="row">

										<div class="form-group col-md-3">
											<label for="prod_detail_id">商品</label>
											<input type="hidden" class="form-control input-sm" name="fee_goods_nm" id="fee_goods_nm" >
											<select class="form-control" id="prod_detail_id" name="prod_detail_id" onchange="detailidchange()">
												@foreach($detailfeeoption as $detailid=> $row)
													<option value="{{$detailid}}">{{$row}}</option>
												@endforeach
											</select>
										</div>

										<div class="form-group col-md-2">
											<label for="fee_name">{{ trans('modOrder.feeName') }}</label>
											<div class="input-group input-group-sm">
												<input type="hidden" class="form-control" name="fee_cd" id="fee_cd">
												<input type="text" class="form-control" id="fee_name" name="fee_name">
												<span class="input-group-btn">
													<button type="button" class="btn btn-default btn-flat lookup" btnname="fee_name"
													    info1="{{$info1}}" 
														info2="{{Crypt::encrypt('cd+cd_descp,cd,cd_descp,value1')}}" 
														info3="{{$info3}}"
														info4="cd_descp=fee_name;cd=fee_cd;value1=amount;" triggerfunc="" selectionmode="singlerow">
														<i class="fa fa-search" aria-hidden="true"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="form-group col-md-2">
											<label for="amount">金額</label>
											<input type="number" class="form-control input-sm" id="amount" name="amount" grid="true" >
										</div>
										<div class="form-group col-md-2">
											<label for="fee_descp">{{ trans('modOrder.feeDescp') }}</label>
											<input type="text" class="form-control input-sm" id="fee_descp" name="fee_descp" grid="fee_descp" >
										</div>

									</div>
								</div>
								<!-- /.box-body -->

								<div class="box-footer">
									<input type="hidden" class="form-control input-sm" name="id" grid="true">
									<button type="button" class="btn btn-sm btn-primary" id="Save3">{{ trans('common.save') }}</button>
									<button type="button" class="btn btn-sm btn-danger" id="Cancel3">{{ trans('common.cancel') }}</button>
								</div>
							</form>
						</div>
						<div id="amtGrid"></div>
					</div>

					<div class="tab-pane" id="tab_8">
						<div class="box box-primary" id="subBox8" style="display:none">
							<form method="POST" accept-charset="UTF-8" id="subForm8" enctype="multipart/form-data">
								<!-- /.box-header -->
								<div class="box-body">
									<div class="row">
									</div>
								</div>
								<!-- /.box-body -->
							</form>
						</div>
						<div id="imgGrid"></div>
					</div>
					<div class="tab-pane" id="tab_10">
						<div id="tallyGrid"></div>
					</div>
				</div>
				<!-- /.tab-content -->
			</div>
		</div>	
	</div>

	<!-- Modal -->
	<div class="modal fade" id="packModal" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">{{ trans('modOrderPackDetail.packingContent') }}</h5>
		</div>
		<div class="modal-body">
			<div id="packDetailGrid"></div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('common.close') }}</button>
		</div>
		</div>
	</div>
	</div>


 @endsection 
 @include('backpack::template.lookup') 
 @section('after_scripts')
	<script>
		var mainId = "";
		var realId = "";
		var ord_no = "";1
		var sysOrdNo = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmttest') }}";

		var fieldData = null;
		var fieldObj = null;

		@if(isset($crud -> create_fields))
		fieldData = '{{!! json_encode($crud->create_fields) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		//console.log(fieldObj);
		@endif



		@if(isset($id))
		realId 	= "{{$id}}";
		mainId   = "{{$id}}";
		// editData = '{{!! $entry !!}}';
		// editData = editData.substring(1);
		// editData = editData.substring(0, editData.length - 1);
		// var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
		// editObj  = JSON.parse(objJson);
		// ord_no   = editObj.ord_no;
		// sysOrdNo = editObj.sys_ord_no;
		console.log(mainId);
		ord_no = "{{trim(preg_replace('/\s+/', ' ', $ord_no))}}";
		sysOrdNo = "{{$sys_ord_no}}";
		close_status = "{{$close_status}}";
		//console.log(editObj);
		@endif




		$(function () {
			var imagerenderer = function (row, datafield, value) {
                if(value != "") {
					console.log(BASE_URL.replace('/admin',''));
                    return '<img style="margin-left: 15px;" height="60" width="50" src="'+BASE_URL.replace('/admin','')+'/storage/' + value.replace('public/', '') + '"/>';
                }

                return "";
            }
			$('#subBox button[btnName="goods_no"]').on('click', function(){
				$('#lookupModal').modal('show');
				initLookup('goods_no', "料號建檔", callBackFunc=function(data){
					$("#subBox input[name='goods_nm']").val(data.goods_nm);
					$("#subBox input[name='goods_no']").val(data.goods_no);
					$("#subBox input[name='goods_no2']").val(data.goods_no2);
					$("#subBox input[name='goods_nm2']").val(data.goods_nm2);
					$("#subBox input[name='gwu']").val(data.gwu);
					$("#subBox input[name='cbm']").val(data.cbm);
					$("#subBox input[name='cbmu']").val(data.cbmu);
					$("#subBox input[name='length']").val(data['g_length']);
					$("#subBox input[name='weight']").val(data['g_width']);
					$("#subBox input[name='height']").val(data['g_height']);
					$("#subBox input[name='gw']").attr("gw", data.gw);
					var pkgNum = parseInt($("#subBox input[name='pkg_num']").val()) || 0;
					if(pkgNum > 0) {
						$("#subBox input[name='gw']").val((data.gw * pkgNum).toFixed(2));
					}
					$("#subGw").text(data.gw);
				});
			});

			$('#subBox3 button[btnName="fee_name"]').on('click', function(){
				$('#lookupModal').modal('show');
				var selted = document.getElementById("prod_detail_id");
				var goodsval = selted.options[selted.selectedIndex].value;
				var owner_cd =$('#owner_cd').val();
				$.get(BASE_URL+'/getnewfeelookup', {'detailid': goodsval,'ownercd': owner_cd}, function(data){
					if(data.msg=='success'){
						$('button[btnName="fee_name"]').attr('info3', data.val);
					}
				})
				initLookup('fee_name', "代收款建檔", callBackFunc=function(data){
					$("#amount").val(data.value1);
					var code_cd = data.cd;
					var owner_cd =$('#owner_cd').val();
					var selted = document.getElementById("prod_detail_id");
					var goodsval = selted.options[selted.selectedIndex].value;
					$.get(BASE_URL+'/getnewfee', {'feecd': code_cd,'ownercd': owner_cd ,'goods_no': owner_cd}, function(data){
						if(data.data!=null){
							$("#subBox3 input[name='amount']").val(data.data.fee_amount);
						}
					});
				});
			});

			$('#subBox3 input[name="fee_name"]').on('click', function(){
				var selted = document.getElementById("prod_detail_id");
				var goodsval = selted.options[selted.selectedIndex].value;
				var owner_cd =$('#owner_cd').val();
				$.get(BASE_URL+'/getnewfeelookup', {'detailid': goodsval,'ownercd': owner_cd}, function(data){
					if(data.msg=='success'){
						$('button[btnName="fee_name"]').attr('info3', data.val);
					}
				})
				var check = $('#subBox input[name="fee_name"]').data('ui-autocomplete') != undefined;
				if(check == false) {
					initAutocomplete("subForm3","fee_name",callBackFunc=function(data){
						$("#fee_name").val(data.cd_descp);
						$("#fee_cd").val(data.cd);
					},"fee_name");
				}
			});

			$("#subBox input[name='pkg_num']").on("change", function(){
				var gw = parseFloat($("#subBox input[name='gw']").attr("gw")) || 0;
				var pkgNum = parseInt($("#subBox input[name='pkg_num']").val()) || 0;

				$("#subBox input[name='gw']").val((gw * pkgNum).toFixed(2));
			});


			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmttest') }}";
			formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_order') }}/" + mainId;
			formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmttest') }}";
			formOpt.afterInit = function() {
				menuBtnFunc.disabled(['iCancel','iAdd', 'iCopy', 'iDel']);
			}

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.beforeDelFunc = function() {
				var status = $("#status").val();

				if(status != "UNTREATED") {
					alert('狀態不在「尚未安排」，故無法刪除');
					return false;
				}

				return true;
			}

			formOpt.afterDel = function() {
				$('#jqxGrid').jqxGrid('clear');
				$('#packGrid').jqxGrid('clear');
				$('#packDetailGrid').jqxGrid('clear');
			}

			formOpt.addFunc = function() {
				$('#jqxGrid').jqxGrid('clear');
				$('#packGrid').jqxGrid('clear');
				$("#subPanel").hide();

				$("#truck_cmp_no").val(truck_cmp_no);
				$("#truck_cmp_nm").val(truck_cmp_nm);
				$("#wh_addr").val(whAddr);
				$("#sys_ord_no").val('');
				$("#ord_no").val('');

				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();

				if(dd<10) {
					dd = '0'+dd
				} 

				if(mm<10) {
					mm = '0'+mm
				} 

				today = yyyy+'-'+mm+'-'+dd;
				$("#etd").val(today);
				$("#status").val("UNTREATED");
				//$("#trs_mode").val("NON");
			}

			formOpt.editFunc = function() {
				$("#subPanel").show();
				$("#jqxGrid").jqxGrid({'showtoolbar': false});
				$("#packGrid").jqxGrid({'showtoolbar': false});
			}

			formOpt.copyFunc = function() {
				$("#subPanel").hide();
				$("#sys_ord_no").val('');
				$("#ord_no").val('');
				$("#status").val("UNTREATED");
				$("#amt").val('');
				$("#cust_amt").val('');
				$("#distance").val('');
				$("#pkg_num").val('');
				$("#total_gw").val('');
				$("#total_cbm").val('');
				$("#amt_remark").val('');
			}

			formOpt.saveSuccessFunc = function(data) {
				$("#jqxGrid").jqxGrid({'showtoolbar': true});
				$("#packGrid").jqxGrid({'showtoolbar': true});

				if(typeof data.dist !== "undefined" && data.dist > 0) {
					$("#distance").val(data.dist);
				}
			}

			var btnGroup = [
				{
					btnId: "btnConfirm",
					btnIcon: "fa fa-check-square-o",
					btnText: "回單確認",
					btnFunc:function(){
						swal({
							title: "確認視窗",
							text: "您確定要回單嗎？",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: '#dc3545',
							cancelButtonColor: '#6c757d',
							confirmButtonText: "確定",
							cancelButtonText: "取消"
						}).then((result) => {
							if (result.value) {
								$.post(BASE_URL + '/order/confirm', {'ids': mainId}, function(data){
									if(data.msg == "success") {
										swal("回單成功", "", "success");
										window.opener.document.getElementById("updategrid").click();
										window.close();
									}
									else{
										swal("操作失敗", data.errorMsg, "error");
									}
								});
							
							} else {
							
							}
						});
						
					}
				},
			];

			initBtn(btnGroup);

			$.ajax({
				url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_detail') }}",
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {

					var col = [
						[
							{name: "id", type: "number"},
							{name: "ord_no", type: "string"},
							{name: "goods_no", type: "string"},
							{name: "goods_no2", type: "string"},
							{name: "goods_nm", type: "string"},
							{name: "pkg_num", type: "number"},
							{name: "pkg_unit", type: "string"},
							{name: "gw", type: "number"},
							{name: "gwu", type: "string"},
							{name: "cbm", type: "number"},
							{name: "cbmu", type: "string"},
							{name: "length", type: "number"},
							{name: "weight", type: "number"},
							{name: "height", type: "number"},
							{name: "sn_no", type: "string"},
							{name: "ord_id", type: "number"},
						],
						[
							{text: "{{ trans('modOrderDetail.id') }}", datafield: "id", width: 100, hidden: true},
							{text: "{{ trans('modOrderDetail.ordNo') }}", datafield: "ord_no", width: 130, nullable: false,hidden:true},
							{text: "{{ trans('modOrderDetail.goodsNo') }}", datafield: "goods_no", width: 150, nullable: false},
							{text: "{{ trans('modOrderDetail.goodsNo2') }}", datafield: "goods_no2", width: 150, nullable: false},
							{text: "{{ trans('modOrderDetail.goodsNm') }}", datafield: "goods_nm", width: 150, nullable: false},
							{text: "{{ trans('modOrderDetail.pkgNum') }}", datafield: "pkg_num", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.pkgUnit') }}", datafield: "pkg_unit", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.gw') }}", datafield: "gw", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.gwu') }}", datafield: "gwu", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.cbm') }}", datafield: "cbm", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.cbmu') }}", datafield: "cbmu", width: 100, nullable: false},
							{text: "{{ trans('modOrderDetail.length') }}", datafield: "length", width: 80, nullable: false},
							{text: "{{ trans('modOrderDetail.weight') }}", datafield: "weight", width: 80, nullable: false},
							{text: "{{ trans('modOrderDetail.height') }}", datafield: "height", width: 80, nullable: false},
							{text: "{{ trans('modOrderDetail.snNo') }}", datafield: "sn_no", width: 80, nullable: false},
							{text: "{{ trans('modOrderDetail.ordId') }}", datafield: "ord_id", width: 80, hidden: true},
						]
					];
					
					var opt = {};
					opt.gridId = "jqxGrid";
					opt.fieldData = col;
					opt.formId = "subForm";
					opt.saveId = "Save";
					opt.cancelId = "Cancel";
					opt.showBoxId = "subBox";
					opt.height = 300;
					opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/orderDetail/get') }}" + '/' + realId;
					opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderDetail') }}" + "/store";
					opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderDetail') }}" + "/update";
					opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderDetail') }}" + "/delete/";
					opt.commonBtn = true;
					if(close_status=="N")
						opt.showtoolbar = true;
					opt.defaultKey = {
						'ord_no': ord_no
					};
					opt.beforeSave = function (row) {
						if(row.pkg_num == "" || row.pkg_num == null) {
							swal("警告", "數量不能為空", "warning");
							return false;
						}

						if(row.goods_nm == "" || row.goods_nm == null) {
							swal("警告", "商品名稱不能為空", "warning");
							return false;
						}
						return true;
					}

					opt.afterSave = function (data) {
						console.log(data);
						var sumPkgNum = $("#jqxGrid").jqxGrid('getcolumnaggregateddata', 'pkg_num', ['sum']);
						var sumGw = $("#jqxGrid").jqxGrid('getcolumnaggregateddata', 'gw', ['sum']);
						var sumCbm = $("#jqxGrid").jqxGrid('getcolumnaggregateddata', 'cbm', ['sum']);
						$("#pkg_num").val(sumPkgNum.sum);
						$("#total_gw").val(sumGw.sum);
						$("#total_cbm").val(sumCbm.sum);
						$("#subGw").text("");
					}

					opt.beforeCancel = function() {
						$("#subGw").text("");
					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			});

			$.ajax({
				url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_fee') }}",
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {

					var col = [
						[
							{name: "id", type: "number"},
							{name: "fee_cd", type: "string"},
							{name: "fee_descp", type: "string"},
							{name: "fee_name", type: "string"},
							{name: "amount", type: "number"},
							{name: "prod_detail_id", type: "string"},
							{name: "fee_goods_nm", type: "string"},
						],
						[
							{text: "id", datafield: "id", width: 100, hidden: true},
							{text: "費用代碼", datafield: "fee_cd", width: 300, hidden: true},
							{text: "服務項目名稱", datafield: "fee_name", width: 130},
							{text: "金額", datafield: "amount", width: 130},
							{text: "敘述", datafield: "fee_descp", width: 300},
							{text: "商品id", datafield: "prod_detail_id", width: 130, hidden: true},
							{text: "對應商品", datafield: "fee_goods_nm", width: 130},
						]
					];
					
					var opt = {};
					opt.gridId = "amtGrid";
					opt.fieldData = col;
					opt.formId = "subForm3";
					opt.saveId = "Save3";
					opt.cancelId = "Cancel3";
					opt.showBoxId = "subBox3";
					opt.height = 300;
					opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/orderdetailamt/get') }}" + '/' + realId ;
					opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderdetailamt') }}" + "/store";
					opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/orderdetailamt') }}" + "/update/";
					opt.delUrl  =   "{{ url(config('backpack.base.route_prefix', 'admin').'/orderdetailamt') }}" + "/delete/";
					opt.defaultKey = {'ord_id': realId};
					opt.commonBtn = true;
					opt.showtoolbar = true;
					opt.beforeSave = function (row) {
						var sel = document.getElementById("prod_detail_id");
						var text= sel.options[sel.selectedIndex].text;
						$("#fee_goods_nm").val(text);
					}
					opt.afterSave = function (data) {
						var sumtotal_amount =data.amt;
						$('#total_amount').val(data.maindata.total_amount);
						
					}
					opt.beforeCancel = function() {
					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			});
			

			$.ajax({
				url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_file') }}",
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {

					var col = [
						[
							{name: "id", type: "number"},
							{name: "guid", type: "string"},
							{name: "type_no", type: "string"},
							{name: "created_at", type: "string"}
						],
						[
							{text: "id", datafield: "id", width: 100, hidden: true },
							{text: "圖片", datafield: "guid", width: 150, cellsrenderer: imagerenderer},
							{text: "敘述", datafield: "type_no", width: 300},
							{text: "上傳時間", datafield: "created_at", width: 300}
						]
					];
					
					var opt = {};
					opt.gridId = "imgGrid";
					opt.fieldData = col;
					opt.formId = "subForm8";
					opt.saveId = "Save8";
					opt.cancelId = "Cancel8";
					opt.showBoxId = "subBox8";
					opt.height = 300;
					opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/orderdetailimg/get') }}" + '/' + mainId ;
					opt.delUrl  =   "{{ url(config('backpack.base.route_prefix', 'admin').'/orderdetailimg') }}" + "/delete/";
					opt.defaultKey = {'ord_id': realId};
					opt.commonBtn = true;
					@can('ImgControl')
					if(close_status=="N"){
					opt.showtoolbar = true;
					}
					@endcan

					opt.beforeSave = function (row) {
					}
					opt.afterSave = function (data) {
					}
					opt.beforeCancel = function() {
					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			});
			$.ajax({
				url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_file') }}",
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {

					var col = [
						[
							{name: "id", type: "number"},
							{name: "created_at", type: "string"},
							{name: "order_tally", type: "string"},
							{name: "ord_num", type: "number"},
							{name: "real_num", type: "number"},
							{name: "created_by", type: "string"}
						],
						[
							{text: "id", datafield: "id", width: 100, hidden: true },
							{text: "點貨日期", datafield: "created_at", width: 150},
							{text: "是否符合", datafield: "order_tally", width: 100},
							{text: "訂單數量", datafield: "ord_num", width: 100},
							{text: "實際數量", datafield: "real_num", width: 100},
							{text: "點貨人", datafield: "created_by", width: 100},
						]
					];
					
					var opt = {};
					opt.gridId = "tallyGrid";
					opt.fieldData = col;
					opt.formId = "subForm10";
					opt.saveId = "Save10";
					opt.cancelId = "Cancel10";
					opt.showBoxId = "subBox10";
					opt.height = 300;
					opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/ordertally/get') }}" + '/' + realId ;
					opt.defaultKey = {'ord_id': realId};
					opt.commonBtn = true;

					opt.beforeSave = function (row) {
					}
					opt.afterSave = function (data) {
					}
					opt.beforeCancel = function() {
					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			});
		});
	</script>
	<script type="text/javascript">
		$("#fee_name").on('keydown', function (e) {
			var feecd = $("#fee_name").val();
			if (e.key === "Enter") {
				if(feecd!=""){
					$.post(BASE_URL + '/confirmordfee/', {'feecd': feecd,'ord_id': mainId}, function(data){
					if(data.msg == "success") {
						$("#amtGrid").jqxGrid('updatebounddata', 'sort');
						var sumtotal_amount =data.amt;
						$("#total_amount").val(sumtotal_amount);
					}else{
						swal("不存在的服務項目!", data.errorMsg, "error");
					}
					$("#fee_name").val("");
					$("#fee_cd").val("");
				});
				}
				else{
					
				}
			}
		});
	    $("#imgGrid").on("rowdoubleclick", function(event){
			// alert("test");
			var selectedrowindex = $("#imgGrid").jqxGrid('getselectedrowindex');
			var Ttest =  $("#imgGrid").jqxGrid('getrowdata', selectedrowindex);
			var boundindex =  Ttest.boundindex;
            console.log(boundindex);
  			var url = BASE_URL.replace('/admin','')+'/storage/' + Ttest.guid.replace('public/', '');
            window.open(url);
    })
	$(function(){
		formOpt.initFieldCustomFunc();
		$.get( formOpt.fieldsUrl , function( data ) {
                if(typeof formOpt.afterInit === "function") {
                    formOpt.afterInit();

					$('#fee_name').focus();
					var selted = document.getElementById("prod_detail_id");
					var goodsval = selted.options[selted.selectedIndex].value;
					var owner_cd = data['owner_cd'];
					$.get(BASE_URL+'/getnewfeelookup', {'detailid': goodsval,'ownercd': owner_cd}, function(data){
						if(data.msg=='success'){
							$('button[btnName="fee_name"]').attr('info3', data.val);
						}
					})

					$('#pick_info').val(data['pick_city_nm']+data['pick_area_nm']) ;
					$('#dlv_info').val(data['dlv_city_nm']+data['dlv_area_nm']) ;
					if($('#pick_info').val()==0)
						$('#pick_info').val("");
					if($('#dlv_info').val()==0)
						$('#dlv_info').val("");
                }
            });
	});
	function detailidchange() {
		var selted = document.getElementById("prod_detail_id");
		var goodsval = selted.options[selted.selectedIndex].value;
		//清空
			// $('#fee_cd').val('');
			// $('#fee_name').val('');
			// $('#amount').val(0);
		//
		var owner_cd =$('#owner_cd').val();
		$.get(BASE_URL+'/getnewfeelookup', {'detailid': goodsval,'ownercd': owner_cd}, function(data){
			if(data.msg=='success'){
				$('button[btnName="fee_name"]').attr('info3', data.val);
			}
		})
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {

	});
</script>
@endsection
