@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      集團建檔<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix'),'customerProfile') }}">{{ trans('sysCustomers.titleName') }}</a></li>
		<li class="active">集團建檔</li>
      </ol>
    </section>
@endsection

@section('content')
@include('backpack::template.toolbar')
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">        
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">{{ trans('sysCustomers.baseInfo') }}</a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cust_no">{{ trans('sysCustomers.custNo') }}</label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cname">{{ trans('sysCustomers.cname') }}</label>
                                        <input type="text" class="form-control" id="cname" name="cname">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="ename">{{ trans('sysCustomers.ename') }}</label>
                                        <input type="text" class="form-control" id="ename" name="ename">
                                    </div>      
                                    <div class="form-group col-md-3">
                                        <label for="email">{{ trans('sysCustomers.email') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input type="eamil" class="form-control" id="email" name="email" >
                                        </div>
                                    </div>                              
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cmp_abbr">{{ trans('sysCustomers.cmpAbbr') }}</label>
                                        <input type="text" class="form-control" id="cmp_abbr" name="cmp_abbr">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="contact">{{ trans('sysCustomers.contact') }}</label>
                                        <input type="text" class="form-control" id="contact" name="contact" =>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('sysCustomers.status') }}</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="A">{{ trans('sysCustomers.STATUS_A') }}</option>
                                            <option value="B">{{ trans('sysCustomers.STATUS_B') }}</option>
                                        </select>
                                    </div>                                                                       
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="identity">{{ trans('sysCustomers.identity') }}</label>
                                        <select class="form-control" id="identity" name="identity" @if(isset($id)) switch="off" @endif >
                                            <option value="G">集團</option>
                                            <option value="C">公司</option>
                                            <option value="S">站別</option>
                                            <option value="D">部門</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3" id="group" style="display:none">
                                        <label for="g_key">集團</label>
                                        <select class="form-control" id="g_key" name="g_key"></select>
                                    </div>
                                    <div class="form-group col-md-3" id="cmp" style="display:none">
                                        <label for="c_key">公司</label>
                                        <select class="form-control" id="c_key" name="c_key"></select>
                                    </div>
                                    <div class="form-group col-md-3" id="stn" style="display:none">
                                        <label for="d_key">站別</label>
                                        <select class="form-control" id="d_key" name="d_key"></select>
                                    </div>
                                    
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="phone">{{ trans('sysCustomers.phone') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" class="form-control" id="phone" name="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="fax">{{ trans('sysCustomers.fax') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-fax"></i>
                                            </div>
                                            <input type="text" class="form-control" id="fax" name="fax">
                                        </div>
                                    </div>                                                               
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="zip">{{ trans('sysCustomers.zip') }}</label>
                                        <input type="text" class="form-control" id="zip" name="zip">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="city_nm">{{ trans('sysCustomers.cityNm') }}</label>
                                        <input type="text" class="form-control" id="city_nm" name="city_nm">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="area_nm">{{ trans('sysCustomers.areaNm') }}</label>
                                        <input type="hidden" class="form-control" id="area_id" name="area_id">
                                        <input type="text" class="form-control" id="area_nm" name="area_nm">
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="address">{{ trans('sysCustomers.address') }}</label>
                                        <input type="text" class="form-control" id="address" name="address">
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ trans('sysCustomers.remark') }}</label>
                                            <textarea class="form-control" rows="3" name="remark"></textarea>
                                        </div>
                                    </div>
                                </div>

                                {{--  <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('sysCustomers.createdBy') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('sysCustomers.createdAt') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('sysCustomers.updatedBy') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('sysCustomers.updatedAt') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>  --}}
                                                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>   

@endsection


@include('backpack::template.lookup')


@section('after_scripts')

<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/companyProfile') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
    @endif


    @if(isset($id))
        mainId = "{{$id}}";
        editData = '{{!! json_encode($entry["original"]) !!}}';

        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        //editObj = JSON.parse(editData);
        //cust_no= editObj.cust_no;
        //console.log(editObj);
    @endif


    $(function(){
        
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/companyProfile') }}";
        //formOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_customers') }}";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/sys_customers') }}/" + mainId;
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/companyProfile') }}";
        
        formOpt.initFieldCustomFunc = function (){
            $("select[name='state[]']").select2({tags: true});
        };

        formOpt.addFunc = function() {
            $("#status").val("B");
            $("#identity").prop("disabled", false);
            $("#identity").trigger("change");
        }
        formOpt.copyFunc = function() {
            $("#status").val("B");
            $("#identity").prop("disabled", false);
            $("#identity").trigger("change");
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);      


        $("#identity").on("change", function(){
            var key = $(this).val();

            if(key == "C") {
                $("#group").show();
                $("#cmp").hide();
                $("#stn").hide();
            }
            else if(key == "S") {
                $("#group").show();
                $("#cmp").show();
                $("#stn").hide();
            }
            else if(key == "D") {
                $("#group").show();
                $("#cmp").show();
                $("#stn").show();
            }
            else {
                $("#group").hide();
                $("#cmp").hide();
                $("#stn").hide();
            }

            $.get(BASE_URL+'/gcsd/get', {}, function(data){
                if(data.gData.length > 0) {
                    var opt = "";
                    for(i in data.gData) {
                        opt += '<option value="'+data.gData[i].cust_no+'">'+data.gData[i].cname+'</option>'; 
                    }
                    $("#g_key").html(opt);
                }

                if(data.cData.length > 0) {
                    var opt = "";
                    for(i in data.cData) {
                        opt += '<option value="'+data.cData[i].cust_no+'">'+data.cData[i].cname+'</option>'; 
                    }
                    $("#c_key").html(opt);
                }

                if(data.sData.length > 0) {
                    var opt = "";
                    for(i in data.sData) {
                        opt += '<option value="'+data.sData[i].cust_no+'">'+data.sData[i].cname+'</option>'; 
                    }
                    $("#s_key").html(opt);
                }
            });
        });
    })
</script>    

@endsection
