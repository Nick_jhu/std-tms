@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      {{ trans('dlvCar.deliveryWork') }}<small></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">{{ trans('dlvCar.deliveryWork') }}</li>
      </ol>
    </section>
    <style>
        *,
        *:before,
        *:after {
          box-sizing: border-box;
        }
        
        .form-container {
          padding: 1rem;
          margin: 2rem auto;
          background-color: #DDDDDD;
          border: 1px solid #DDDDDD;
          width: 50%;
        }
        
        /* HTML5 Boilerplate accessible hidden styles */
        .promoted-input-checkbox {
          border: 0;
          clip: rect(0 0 0 0);
          height: 1px;
          margin: -1px;
          overflow: hidden;
          padding: 0;
          position: absolute;
          width: 1px;
        }
        
        .promoted-checkbox input:checked + label > svg {
          height: 24px;
          -webkit-animation: draw-checkbox ease-in-out 0.2s forwards;
                  animation: draw-checkbox ease-in-out 0.2s forwards;
        }
        .promoted-checkbox label:active::after {
          background-color: #DDDDDD;
        }
        .promoted-checkbox label {
          color: #000000;
          line-height: 40px;
          cursor: pointer;
          position: relative;
        }
        .promoted-checkbox .check-label-text {
            white-space: nowrap;
            margin-left: 40px;
        }
        .promoted-checkbox label:after {
          display: block;
          position: absolute;
          top: 50%;
          transform: translateY(-50%);
          content: "";
          height: 34px;
          width: 34px;
          margin-right: 1rem;
          /* background-color: #DDDDDD; */
          border: 2px solid #DDDDDD;
          border-radius: 3px;
          -webkit-transition: 0.15s all ease-out;
          transition: 0.15s all ease-out;
        }
        .promoted-checkbox svg {
          stroke: #000000;
          stroke-width: 2px;
          height: 0;
          width: 40px;
          position: absolute;
          left: -4px;
          top: 5px;
          stroke-dasharray: 33;
        }
        
        @-webkit-keyframes draw-checkbox {
          0% {
            stroke-dashoffset: 33;
          }
          100% {
            stroke-dashoffset: 0;
          }
        }
        
        @keyframes draw-checkbox {
          0% {
            stroke-dashoffset: 33;
          }
          100% {
            stroke-dashoffset: 0;
          }
        }
        
        
        
    </style>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form action="" id="dlvForm" style="margin-block-end:0px">
                    <p id="warningText" style="color:red"></p>
                    <p>勾選筆數：<span id="selectcount"></span> {{ trans('dlvCar.carCapacity') }}：<span id="carCbm">0</span>&nbsp;&nbsp;&nbsp;{{ trans('dlvCar.selected') }}：<span id="gridCbm">0</span>  {{ trans('dlvCar.loadingRate') }}：<span id="loadRate">0%</span>&nbsp;&nbsp;&nbsp;{{ trans('dlvCar.selectedRate') }}：<span id="gridLoadRate">0%</span></p>
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="dlv_date">{{ trans('dlvCar.sendCarDate') }}</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="dlv_date" name="dlv_date">
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="cust_nm">{{ trans('dlvCar.carDealer') }}</label>
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" id="cust_nm" name="cust_nm" @if(isset($viewData)) value="{{$viewData['cname']}}" @endif>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="cust_nm"
                                    info1="{{Crypt::encrypt('sys_customers_item')}}" 
                                    info2="{{Crypt::encrypt('cust_no+cname,cust_no,cname')}}" 
                                    info3="{{Crypt::encrypt('cust_type=\'CT003\'')}}"
                                    info4="cust_no=cust_no;cname=cust_nm" triggerfunc="" selectionmode="singlerow">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </div>
                            <input type="hidden" class="form-control" id="cust_no" name="cust_no" @if(isset($viewData)) value="{{$viewData['cust_no']}}" @endif>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="car_no">{{ trans('dlvCar.carNo') }}</label>
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" id="car_no" name="car_no">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="car_no"
                                    info1="{{Crypt::encrypt('mod_car')}}" 
                                    info2="{{Crypt::encrypt('car_no,driver_nm,phone,car_no,car_type,cbm,load_rate')}}" 
                                    info3="{{Crypt::encrypt('')}}"
                                    info4="car_no=car_no;driver_nm=driver_nm;cbm=cbm;phone=driver_phone;load_rate=load_rate;" triggerfunc="" selectionmode="singlerow">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </div>
                            
                            <input type="hidden" class="form-control" id="load_rate" name="load_rate">
                            <input type="hidden" class="form-control" id="cbm" name="cbm">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="car_type">{{ trans('dlvCar.carType') }}</label>
                            {{--  <input type="text" class="form-control" id="car_type_nm" disabled="disabled">
                            <input type="hidden" class="form-control" id="car_type">  --}}
                            <select class="form-control" name="car_type" id="car_type"></select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="driver_nm">{{ trans('dlvCar.driver') }}</label>
                            <input type="text" class="form-control" id="driver_nm">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="driver_phone">{{ trans('dlvCar.phone') }}</label>
                            <input type="text" class="form-control" id="driver_phone" name="driver_phone">
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="form-group col-md-2">
                            <label for="ord_no">訂單號碼</label>
                            <input type="text" class="form-control" id="ord_no" name="ord_no">
                        </div>
                    </div> --}}
                    <div class="form-group col-md-4">
                    <button type="button" class="btn btn-primary" id="confirmBtn">{{ trans('dlvCar.confirmSendCar') }}</button>
                    <button type="button" class="btn btn-danger" id="urgentBtn">{{ trans('dlvCar.urgent') }}</button>
                    <button type="button" class="btn btn-warning" id="insertBtn">{{ trans('dlvCar.insertOrd') }}</button>
                    @can('dlvCarFieldAd')
                    <button type="button" class="btn btn-primary" id="btnLayout">{{ trans('excel.AdjustmentField') }}</button>
                    @endcan
                    <button type="button" class="btn btn-danger" id="emptyBtn">{{ trans('dlvCar.clear') }}</button>
                    @can('AutoDelivery')
                    {{-- <button type="button" class="btn btn-warning" id="autosend">{{ trans('dlvCar.AutoDelivery') }}</button> --}}
                    @endcan 
                    <button type="button" class="btn btn-danger" id="delrowbtn">移除</button>
                    @can('CHANGEREADY')
                    <button type="button" class="btn btn-primary" id="readybackbtn">取消可派車</button>
                    @endcan 
                    </div>
                </form>
                <form style="display:inline;" method="post" onsubmit="return addgridrow();">
                    <div class="form-group col-md-2">
                        {{-- <span for="ord_no" >刷單號碼</span> --}}
                        <input class="form-control" type="text" size="50" id="orderno" name="orderno" placeholder="請輸入刷單號碼">
                        </div>
                        <div class="form-group col-md-2">
                            <input class="form-control" type="text" size="50" id="noorder" name="noorder" placeholder="請輸入刷單號碼(新)">
                        </div>
                    <div class="form-group col-md-2">
                        <svg xmlns="http://www.w3.org/2000/svg" style="display: none" style="float:right">
                            <symbol id="checkmark" viewBox="0 0 24 24">
                              <path stroke-linecap="round" stroke-miterlimit="10" fill="none" d="M22.9 3.7l-15.2 16.6-6.6-7.1">
                              </path>
                            </symbol>
                          </svg>
                          <div class="promoted-checkbox" style="display:inline;margin-right:10px;">
                            <input id="sendtoapp" style="float:right" name="sendtoapp" type="checkbox" class="promoted-input-checkbox" @if($viewData['checkData']!="") checked="{{$viewData['checkData']}}" @endif/>
                            <label for="sendtoapp">
                              <svg>
                                <use xlink:href="#checkmark" /></svg>
                                <span class="check-label-text " style="font-size:20px;"> {{ trans('dlvCar.SendDirectly') }}</span>
                            </label>
                        </div>
                    </div>
                 </form>
            </div>
        </div>
    </div>
</div>
<div class="row" id="mainForm">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li name="prodDetail" class="active">
                <a onclick="tab_1()" href="#tab_1" data-toggle="tab" aria-expanded="false">模式1</a>
            </li>
            <li name="prodGift">
                <a onclick="tab_2()" href="#tab_2" data-toggle="tab" aria-expanded="false">模式2</a>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div id="jqxGrid" style="display:block"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div id="jqxGrid1" style="display:none"></div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="dlvPlanModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">派車主檔</h4>
			</div>
			<div class="modal-body" id="dlvPlanBody">
				<div id="dlvPlanGrid"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="confirmInsertBtn">確定插入</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearLayout">{{ trans('common.clear') }}</button>
			</div>
		</div>
	</div>
</div>

@endsection

@include('backpack::template.lookup')


@section('after_scripts')

<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>


<script>
    var is_urgent ="N";
    var focus ="1";
    var gridheight = 0;
    var grid1height = 0;
    var dlvlimit = 0;
    @if(isset($viewData['dlvlimit']))
        dlvlimit = parseInt("{{$viewData['dlvlimit']}}"); 
    @endif
    function getCarType(carType) {
        $.get(BASE_URL+'/getCarType', {'carType': carType}, function(data){
            var str = "";

            for(i in data) {
                str += '<option value="'+data[i].cd+'">'+data[i].cd_descp+'</option>';
            }

            $("#car_type").html(str);
        }, "JSON");
    }
    $(function(){
        $('button[btnName="car_no"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('car_no', "{{ trans('dlvCar.car') }}", callBackFunc=function(data){
                console.log(data);
                getCarType(data.car_type);
                $("#carCbm").text(data.cbm);
                $("#loadRate").text((data.load_rate * 100) + '%');
            });
            refixwidth();
        });
        $('#car_no').on('click', function(){
            var check = $('#car_no').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("dlvForm","car_no",callBackFunc=function(data){
                    getCarType(data.car_type);
                    $("#carCbm").text(data.cbm);
                    $("#loadRate").text((data.load_rate * 100) + '%');
                });
            }
        });
        function refixwidth () {
            setTimeout(function(){$('#lookupGrid').jqxGrid('autoresizecolumns');},1000);
        }

        $('button[btnName="cust_nm"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('cust_nm', "{{ trans('dlvCar.carDealer') }}");
        });

        $('#cust_nm').on('click', function(){
            var check = $('#cust_nm').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("dlvForm","cust_nm");
            }
        });

        

        $.jqx.theme = "bootstrap";
        var items = new Array();
        $.get(BASE_API_URL+'/admin/baseApi/getFieldsJson/mod_order', {'key': 'dlvCarGrid'}, function(data){
            var statusCode = [
                {value: "UNTREATED", label: "{{ trans('modOrder.STATUS_UNTREATED') }}"},
                {value: "SEND", label: "{{ trans('modOrder.STATUS_SEND') }}"},
                {value: "DLV", label: "{{ trans('modOrder.STATUS_DLV') }}"},
                {value: "PICKED", label: "{{ trans('modOrder.STATUS_PICKED') }}"},
                {value: "SETOFF", label: "{{ trans('modOrder.STATUS_SETOFF') }}"}
            ]

            var statusSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: statusCode
            };

            var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                autoBind: true
            });

            
            var datafields = [
                        { name: 'id', type: 'number' },
                        { name: 'status', type: 'string', value: 'status', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
                        { name: 'etd', type: 'date', cellsformat: 'yyyy-MM-dd' },
                        { name: 'created_at', type: 'string', cellsformat: 'yyyy-MM-dd' },
                        { name: 'sys_ord_no', type: 'string' },
                        { name: 'ord_no', type: 'string' },
                        { name: 'truck_cmp_no', type: 'string' },
                        { name: 'truck_cmp_nm', type: 'string' },
                        { name: 'pick_cust_no', type: 'string' },
                        { name: 'pick_cust_nm', type: 'string' },
                        { name: 'dlv_cust_no', type: 'string' },
                        { name: 'dlv_cust_nm', type: 'string' },
                        { name: 'remark', type: 'string' },
                        { name: 'dlv_no', type: 'string' },
                        { name: 'pick_remark', type: 'string' },
                        { name: 'dlv_remark', type: 'string' },
                        { name: 'is_inbound', type: 'string' },
						{ name: 'status_desc', type: 'string' },
                        { name: 'owner_nm', type: 'string' },
                        { name: 'total_cbm', type: 'float' },
                        { name: 'pkg_num', type: 'float' },
                        { name: 'dlv_attn', type: 'string' },
                        { name: 'pick_city_nm', type: 'string' },
                        { name: 'pick_area_nm', type: 'string' },
                        { name: 'pick_addr', type: 'string' },
                        { name: 'dlv_city_nm', type: 'string' },
                        { name: 'dlv_area_nm', type: 'string' },
                        { name: 'dlv_addr', type: 'string' },
                        { name: 'dlv_tel', type: 'string' },
                        { name: 'is_urgent', type: 'string' },   
                        { name: 's_key', type: 'string' },
                    ];
            // var baseCondition = "{{Crypt::encrypt(' (status=\'PICKED\'AND dlv_type=\'P\') or status=\'UNTREATED\'')}}";
            var baseCondition = "{{$viewData['basecondition']}}"
            // var baseCondition = "{{Crypt::encrypt(' ( (status=\'READY\' or status=\'READY_PICK\') and dlv_no is null )')}}";
            // var baseCondition = "{{Crypt::encrypt(' ( ".baseconditionstatus." and dlv_no is null )')}}";
            var source =
                {
                    datatype: "json",
                    datafields: datafields,
                    root:"Rows",
                    pagenum: 0,	
                    sortcolumn: 'etd',
                    sortdirection: 'desc',	
                    beforeprocessing: function (data) {
                        var ostatus = "";
                        for(i in data[0].Rows) {
                            if(i == 0) {
                                ostatus = data[0].Rows[i].status;
                                items.push(data[0].Rows[i].status);
                            }

                            if(ostatus != data[0].Rows[i].status) {
                                items.push(data[0].Rows[i].status);
                            }

                            ostatus = data[0].Rows[i].status;
                        }
                        source.totalrecords = data[0].TotalRows;
                    },
                    filter: function () {
                    // update the grid and send a request to the server.
                        $("#jqxGrid").jqxGrid('updatebounddata', 'filter');
                        $("#jqxGrid").jqxGrid('clearselection');
                    },
                    sort: function () {
                        // update the grid and send a request to the server.
                        $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
                        $("#jqxGrid").jqxGrid('clearselection');
                    },
                    //url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order') }}" + "?basecon=status;EQUAL;UNTREATED)*(dlv_type;EQUAL;P&defOrder=etd;desc",					
                    url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order') }}" + "?dbRaw="+baseCondition,
                    pagesize: 50,
                };
            console.log(items);
            var columns = [
                { text: "id", datafield: 'id', width: 50, hidden:true, filtertype: 'number',"filterdelay": 99999999 },
                { text: "入庫", datafield: 'is_inbound', width: '120',"filterdelay": 99999999},
				{ text: "{{ trans('modOrder.status') }}", datafield: 'status_desc', width: '120',"filterdelay": 99999999},
                { text: "{{ trans('modOrder.etd') }}", datafield: 'etd', width: 100,cellsformat: 'yyyy-MM-dd',filtertype: 'date' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.sysOrdNo') }}", datafield: 'sys_ord_no', width:120, filtertype: 'textbox',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.ordNo') }}", datafield: 'ord_no', width:120, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.truckCmpNo') }}", datafield: 'truck_cmp_no', width: 120, filtertype:'textbox',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.truckCmpNm') }}", datafield: 'truck_cmp_nm', width: 120, filtertype:'textbox',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.pickCustNo') }}", datafield: 'pick_cust_no', width: 120, filtertype:'textbox',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.pickCustNm') }}", datafield: 'pick_cust_nm', width:120, filtertype:'textbox',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.dlvCustNo') }}", datafield: 'dlv_cust_no', width: 120, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.dlvCustNm') }}", datafield: 'dlv_cust_nm', width:120, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.remark') }}", datafield: 'remark', width: 300, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.pickRemark') }}", datafield: 'pick_remark', width: 300, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.dlvRemark') }}", datafield: 'dlv_remark', width: 300, filtertype:'textbox',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.ownerNm') }}", datafield: 'owner_nm', width:120, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.totalCbm') }}", datafield: 'total_cbm', width: 70, cellsalign: 'right', filtertype: 'number',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.pkgNum') }}", datafield: 'pkg_num', width: 70, cellsalign: 'right', filtertype: 'number' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.dlvAttn') }}", datafield: 'dlv_attn', width: 100, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.pickCityNm') }}", datafield: 'pick_city_nm', width: 100, filtertype:'textbox',"filterdelay": 99999999},
                { text: "{{ trans('modOrder.pickAreaNm') }}", datafield: 'pick_area_nm', width: 100, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.pickAddr') }}", datafield: 'pick_addr', width: 300, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.dlvCityNm') }}", datafield: 'dlv_city_nm', width: 100, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.dlvAreaNm') }}", datafield: 'dlv_area_nm', width: 100, filtertype:'textbox',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.dlvAddr') }}", datafield: 'dlv_addr', width: 300, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.dlvTel') }}", datafield: 'dlv_tel', width: 120, filtertype:'textbox',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.isUrgent') }}", datafield: 'is_urgent', width: 120, filtertype:'textbox' ,"filterdelay": 99999999},
                { text: "{{ trans('modOrder.sKey') }}", datafield: 's_key', width: 120, filtertype:'textbox',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.createdAt') }}", datafield: 'created_at', width: 100,cellsformat: 'yyyy-MM-dd',filtertype: 'textbox' ,"filterdelay": 99999999},
            ];
            var dataAdapter = new $.jqx.dataAdapter(source, { 
                async: false, 
                loadError: function (xhr, status, error) { 
                    alert('Error loading "' + source.url + '" : ' + error); 
                },
                loadComplete: function() {
                    if(gridheight==0){
                        gridheight = $( window ).height() - 350;
                        $("#jqxGrid").jqxGrid({height: $( window ).height() - 350});
                    }
                    
                }
            });
            $("#jqxGrid").jqxGrid(
            {
                width: '100%',
                height: gridheight,//'100%',//(typeof gridOpt.height == 'undefined')?800:gridOpt.height,
                //autoheight: true,
                source: dataAdapter,
                sortable: true,
                filterable: true,
                altrows: true,
                showfilterrow: true,
                pageable: true,
                virtualmode: true,
                autoshowfiltericon: true,
                columnsreorder: true,
                columnsresize: true,
                columnsautoresize: true,
                showstatusbar: true,
                pagesize: 99999999,
                //autoloadstate: true,
                clipboard: true,
                selectionmode: 'checkbox',
                enablebrowserselection: true,
                pagesizeoptions:[50, 100, 500, 2000],
                rendergridrows: function (params) {
                    //alert("rendergridrows");
                    return params.data;
                },
                renderstatusbar: function (statusbar) {
                    var rowCount = $("#jqxGrid").jqxGrid('getrows').length;
                },
                ready: function () {
                    loadState();
                },
                columns: columns
            });

            $("#jqxGrid").on('bindingcomplete', function (event) {
                if(gridheight==0){
                    gridheight = $( window ).height() - 350;
                    $("#jqxGrid").jqxGrid('height', winHeight+'px');
                }
            //$("#"+gridId).jqxGrid("hideloadelement");f 
            });

            if(typeof data[2] !== "undefined") {
                setTimeout(function(){ $("#jqxGrid1").jqxGrid('loadstate', data[2]); },2000);
                $("#jqxGrid") .jqxGrid('loadstate', data[2]);
                $("#jqxGrid").jqxGrid('clearfilters');
            }
            
        });
        $.get(BASE_API_URL+'/admin/baseApi/getFieldsJson/mod_order', {'key': 'dlvCarGrid'}, function(data){
            var statusCode = [
                {value: "UNTREATED", label: "{{ trans('modOrder.STATUS_UNTREATED') }}"},
                {value: "SEND", label: "{{ trans('modOrder.STATUS_SEND') }}"},
                {value: "DLV", label: "{{ trans('modOrder.STATUS_DLV') }}"},
                {value: "PICKED", label: "{{ trans('modOrder.STATUS_PICKED') }}"},
                {value: "SETOFF", label: "{{ trans('modOrder.STATUS_SETOFF') }}"}
            ]

            var statusSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: statusCode
            };

            var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                autoBind: true
            });

            
            var datafields = [
                        { name: 'id', type: 'number' },
                        { name: 'status', type: 'string', value: 'status', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
                        { name: 'etd', type: 'date', cellsformat: 'yyyy-MM-dd' },
                        { name: 'created_at', type: 'date', cellsformat: 'yyyy-MM-dd' },
                        { name: 'is_inbound', type: 'string' },
                        { name: 'sys_ord_no', type: 'string' },
                        { name: 'ord_no', type: 'string' },
                        { name: 'truck_cmp_no', type: 'string' },
                        { name: 'truck_cmp_nm', type: 'string' },
                        { name: 'pick_cust_no', type: 'string' },
                        { name: 'pick_cust_nm', type: 'string' },
                        { name: 'dlv_cust_no', type: 'string' },
                        { name: 'dlv_cust_nm', type: 'string' },
                        { name: 'remark', type: 'string' },
                        { name: 'pick_remark', type: 'string' },
                        { name: 'dlv_remark', type: 'string' },
                        //{ name: 'owner_cd', type: 'string' },
                        { name: 'owner_nm', type: 'string' },
                        { name: 'total_cbm', type: 'float' },
                        { name: 'pkg_num', type: 'float' },
                        { name: 'dlv_attn', type: 'string' },
                        { name: 'pick_city_nm', type: 'string' },
                        { name: 'pick_area_nm', type: 'string' },
                        { name: 'pick_addr', type: 'string' },
                        { name: 'dlv_city_nm', type: 'string' },
                        { name: 'dlv_area_nm', type: 'string' },
                        { name: 'dlv_addr', type: 'string' },
                        { name: 'dlv_tel', type: 'string' },
                        { name: 'is_urgent', type: 'string' },   
                        { name: 's_key', type: 'string' },         
                    ];
            // var baseCondition = "{{Crypt::encrypt(' (status=\'PICKED\'AND dlv_type=\'P\') or status=\'UNTREATED\'')}}";
            var baseCondition = "{{Crypt::encrypt(' ( (status=\'READY\' or status=\'READY_PICK\') and dlv_no is null )')}}";

            var source =
                {
                    // localdata: data,
                    datatype: "json",
                    datafields: datafields,
                    // root:"Rows",
                    // pagenum: 0,	
                    // sortcolumn: 'etd',
                    // sortdirection: 'desc',	
                    beforeprocessing: function (data) {
                        var ostatus = "";
                        for(i in data[0].Rows) {
                            if(i == 0) {
                                ostatus = data[0].Rows[i].status;
                                items.push(data[0].Rows[i].status);
                            }

                            if(ostatus != data[0].Rows[i].status) {
                                items.push(data[0].Rows[i].status);
                            }

                            ostatus = data[0].Rows[i].status;
                        }
                        source.totalrecords = data[0].TotalRows;
                    },
                    addrow: function (rowid, rowdata, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful 
                        //and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    deleterow: function (rowid, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful 
                        //and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    updaterow	: function(rowid, rowdata, commit) {
                        commit(true);
                    },
                    // url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_dlv_temp') }}",
                    pagesize: 9999,
                };
            console.log(items);
            console.log(data[2]);
            // var columns = [data[2]];
            var columns = [
                { text: "id", datafield: 'id', width: 50, hidden:true, filtertype: 'number' },
                { text: "{{ trans('modOrder.status') }}", datafield: 'status', width: 120, filtertype: 'textbox' },
                { text: "{{ trans('modOrder.etd') }}", datafield: 'etd', width: 100,cellsformat: 'yyyy-MM-dd',filtertype: 'date' },
                { text: "入庫", datafield: 'is_inbound', width:120, filtertype: 'textbox' },
                { text: "{{ trans('modOrder.sysOrdNo') }}", datafield: 'sys_ord_no', width:120, filtertype: 'textbox' },
                { text: "{{ trans('modOrder.ordNo') }}", datafield: 'ord_no', width:120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.truckCmpNo') }}", datafield: 'truck_cmp_no', width: 120, filtertype:'textbox',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.truckCmpNm') }}", datafield: 'truck_cmp_nm', width: 120, filtertype:'textbox',"filterdelay": 99999999 },
                { text: "{{ trans('modOrder.pickCustNo') }}", datafield: 'pick_cust_no', width: 120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.pickCustNm') }}", datafield: 'pick_cust_nm', width:120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvCustNo') }}", datafield: 'dlv_cust_no', width: 120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvCustNm') }}", datafield: 'dlv_cust_nm', width:120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.remark') }}", datafield: 'remark', width: 300, filtertype:'textbox' },
                { text: "{{ trans('modOrder.pickRemark') }}", datafield: 'pick_remark', width: 300, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvRemark') }}", datafield: 'dlv_remark', width: 300, filtertype:'textbox' },
                { text: "{{ trans('modOrder.ownerNm') }}", datafield: 'owner_nm', width:120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.totalCbm') }}", datafield: 'total_cbm', width: 70, cellsalign: 'right', filtertype: 'number' },
                { text: "{{ trans('modOrder.pkgNum') }}", datafield: 'pkg_num', width: 70, cellsalign: 'right', filtertype: 'number' },
                { text: "{{ trans('modOrder.dlvAttn') }}", datafield: 'dlv_attn', width: 100, filtertype:'textbox' },
                { text: "{{ trans('modOrder.pickCityNm') }}", datafield: 'pick_city_nm', width: 100, filtertype:'textbox' },
                { text: "{{ trans('modOrder.pickAreaNm') }}", datafield: 'pick_area_nm', width: 100, filtertype:'textbox' },
                { text: "{{ trans('modOrder.pickAddr') }}", datafield: 'pick_addr', width: 300, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvCityNm') }}", datafield: 'dlv_city_nm', width: 100, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvAreaNm') }}", datafield: 'dlv_area_nm', width: 100, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvAddr') }}", datafield: 'dlv_addr', width: 300, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvTel') }}", datafield: 'dlv_tel', width: 120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.isUrgent') }}", datafield: 'is_urgent', width: 120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.sKey') }}", datafield: 's_key', width: 120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.createdAt') }}", datafield: 'created_at', width: 100,cellsformat: 'yyyy-MM-dd',filtertype: 'textbox' ,"filterdelay": 99999999},
            ];
            // var dataAdapter2 = new $.jqx.dataAdapter(source);
            var dataAdapter2 = new $.jqx.dataAdapter(source, { 

                loadError: function (xhr, status, error) { 
                    console.log("test");
                    alert('Error loading "' + source.url + '" : ' + error); 
                },
                loadComplete: function() {
                    if(gridheight==0){
                        gridheight = $( window ).height() - 450;
                        $("#jqxGrid1").jqxGrid({height: $( window ).height() - 450});
                    }
                }
            });
            $("#jqxGrid1").jqxGrid(
            {
                width: '100%',
                height: gridheight+200,//'100%',//(typeof gridOpt.height == 'undefined')?800:gridOpt.height,
                //autoheight: true,
                source: dataAdapter2,
                sortable: true,
                // filterable: true,
                altrows: true,
                // showfilterrow: true,
                pageable: true,
                virtualmode: false,
                // autoshowfiltericon: true,
                columnsreorder: true,
                columnsresize: true,
                columnsautoresize: true,
                showstatusbar: true,
                pagesize: 99999999,
                //autoloadstate: true,
                clipboard: true,
                selectionmode: 'checkbox',
                enablebrowserselection: true,
                pagesizeoptions:[50, 100, 500, 9999],
                rendergridrows: function (params) {
                    //alert("rendergridrows");
                    return params.data;
                },
                renderstatusbar: function (statusbar) {
                    var rowCount = $("#jqxGrid1").jqxGrid('getrows').length;
                },
                ready: function () {
                    // loadState();
                },
                columns: columns
            });

            $("#jqxGrid1").on('bindingcomplete', function (event) {
                console.log("jqxGrid1 bindingcomplete");
                var statusHeight = 100;
                var winHeight = $( window ).height() - 450 + statusHeight;
                if(gridheight==0){
                    gridheight = $( window ).height() - 450;
                    $("#jqxGrid1").jqxGrid('height', winHeight+'px');
                }
            //$("#"+gridId).jqxGrid("hideloadelement"); 
            });

            if(typeof data[2] !== "undefined") {
                // $("#jqxGrid1").jqxGrid('loadstate', data[2]);
            }
        });
        

        var chkLoadRate = function(gridCbm) {
            var carCbm = parseInt($("#cbm").val());
            var loadRate = parseFloat($("#load_rate").val());

            var carLoadable = carCbm * loadRate;
            if(carCbm > 0) {
                if(gridCbm > carCbm) {
                    return -1;
                }
                else if(gridCbm > carLoadable) {
                    return 0;
                }
            }

            return 1;
        }
        $("#jqxGrid1").on("rowselect", function (event) {
            console.log("jqxGrid1 rowselect");
        });  
        $("#jqxGrid").on("rowselect", function (event) {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            $("#selectcount").text(rows.length);
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                }
            }

            if(chkLoadRate(sum) == 0) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("");
            }
            else if(chkLoadRate(sum) == -1) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("{{ trans('dlvCar.msg1') }}");
            }
            else {
                $("#gridCbm").css('color', '#000');
                $("#warningText").text("");
            }

            var cbm          = parseFloat($("#cbm").val());
            var loadRate     = parseFloat($("#load_rate").val());
            if(cbm) {
                var gridLoadRate = sum / cbm;
                $("#gridLoadRate").text((Math.round(gridLoadRate * 100)) + "%");

                if(gridLoadRate > loadRate) {
                    $("#gridLoadRate").css('color', 'red');
                }
                else {
                    $("#gridLoadRate").css('color', '#000');
                }
            }
            

            $("#gridCbm").text(sum.toFixed(2));
        });  

        $('#jqxGrid').on('rowunselect', function (event) 
        {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }
            $("#selectcount").text(rows.length);
            if(chkLoadRate(sum) == 0) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("");
            }
            else if(chkLoadRate(sum) == -1) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("{{ trans('dlvCar.msg1') }}");
            }
            else {
                $("#gridCbm").css('color', '#000');
                $("#warningText").text("");
            }

            var cbm          = parseFloat($("#cbm").val());
            var loadRate     = parseFloat($("#load_rate").val());
            if(cbm) {
                var gridLoadRate = sum / cbm;
                $("#gridLoadRate").text((Math.round(gridLoadRate * 100)) + "%");

                if(gridLoadRate > loadRate) {
                    $("#gridLoadRate").css('color', 'red');
                }
                else {
                    $("#gridLoadRate").css('color', '#000');
                }
            }
            
            $("#gridCbm").text(sum.toFixed(2));
        });

        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        var m = (date.getMonth()+1);
        var d = date.getDate();
        if(m < 10) {
            m = '0' + m;
        }

        if(d < 10) {
            //d = '0' + (d+1);
            d = '0' + d;
        }
        $('#dlv_date').val(date.getFullYear() + '-' + m + '-' + d);
        $('#dlv_date').datepicker({
            startDate: today,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $("#confirmBtn").on("click", function(){
            // if(focus=="1"){
                var gridCbm =  getGridCbm();
                if(focus=="1"){
                    var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                }else{
                    var rows = $("#jqxGrid1").jqxGrid('getrows');
                }
                var selectcount = rows.length;
                if(parseInt(selectcount)>dlvlimit){
                    swal('警告', "派車上限不可超過"+dlvlimit+"筆", "warning");
                    return;
                }
                if(chkLoadRate(gridCbm) == 0 || chkLoadRate(gridCbm) == -1) {
                    swal({
                        text: "{{ trans('dlvCar.msg2') }}",
                        title: "{{ trans('dlvCar.msg3') }}",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: "{{ trans('dlvCar.cancel') }}",
                        confirmButtonText: "{{ trans('dlvCar.yes') }}"
                        }).then(function (result) {
                        if (result.value) {
                            sendDlvCar();
                        }
                    })
                }
                else {
                    sendDlvCar();
                }
            // }else{
            //     sendDlvCar();
            // }
        });

        $("#autosend").on("click", function(){
            var postData = {
                car_no      : $("#car_no").val(),
                driver_nm   : $("#driver_nm").val(),
                driver_phone: $("#driver_phone").val(),
                dlv_date    : $("#dlv_date").val(),
                car_type    : $("#car_type").val(),
                sendToApp   : $('#sendtoapp').prop('checked')
            }
            $.ajax({
                url: "{{ url(config('backpack.base.route_prefix', 'admin').'/TranPlanMgmt/autosend') }}",
                type: 'POST',
                async: false,
                data: postData,
                beforeSend: function () {
                    loadingFunc.show();
                },
                error: function (jqXHR, exception) {
                    swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    loadingFunc.hide();
                    return;
                },
                success: function (data) {
                    if(data.msg == "success") {
                        swal('Success', "{{ trans('modOrder.sendSuccess') }}", "success");
                        
                        $('#jqxGrid').jqxGrid('clearselection');
                        // $("#jqxGrid").jqxGrid('clearfilters');
                    }
                    else {
                        swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    }
                    loadingFunc.hide();
                    $('#jqxGrid').jqxGrid('updatebounddata');
                    return;
                }
            });
        });

        var sendDlvCar = function() {
            if(focus=="1"){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            }else{
                var rows = $("#jqxGrid1").jqxGrid('getrows');
            }
            $("#selectcount").text(rows.length);
            var selectedRecords = new Array();
            var dlvData = [];
            for (var m = 0; m < rows.length; m++) {
                if(focus=="1"){
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                }else{
                    var row = rows[m];
                }
                console.log(row);
                if((row.dlv_addr == "" || row.dlv_addr == null) && (row.pick_addr == "" || row.pick_addr == null)) {
                    swal('警告', "訂單號："+row.sys_ord_no+"，提貨地址、配送地址不能為空", "warning");
                    return;
                }

                if(((row.dlv_cust_nm != "" && row.dlv_cust_nm != null) || (row.dlv_cust_no != "" && row.dlv_cust_no != null)) && (row.dlv_addr == "" || row.dlv_addr == null)) {
                    swal('警告', "訂單號："+row.sys_ord_no+"，配送地址不能為空", "warning");
                    return;
                }

                if(((row.pick_cust_nm != "" && row.pick_cust_nm != null) || (row.pick_cust_no != "" && row.pick_cust_no != null)) && (row.pick_addr == "" || row.pick_addr == null)) {
                    swal('警告', "訂單號："+row.sys_ord_no+"，提貨地址不能為空", "warning");
                    return;
                }

                dlvData.push(row.id);
            }

            var postData = {
                car_no      : $("#car_no").val(),
                driver_nm   : $("#driver_nm").val(),
                driver_phone: $("#driver_phone").val(),
                dlv_date    : $("#dlv_date").val(),
                car_type    : $("#car_type").val(),
                sendToApp   : $('#sendtoapp').prop('checked'),
                ord_ids     : dlvData
            }
            if(postData.car_no == "" || postData.car_no == null) {
                swal('警告', "請輸入車號", "warning");
                return;
            }

            $.ajax({
                url: "{{ url(config('backpack.base.route_prefix', 'admin').'/TranPlanMgmt/sendDlvCar') }}",
                type: 'POST',
                async: false,
                data: postData,
                beforeSend: function () {
                    loadingFunc.show();
                },
                error: function (jqXHR, exception) {
                    swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    loadingFunc.hide();
                    return;
                },
                success: function (data) {
                    if(data.msg == "success") {
                        swal('Success', "{{ trans('modOrder.sendSuccess') }}", "success");
                        
                        $('#jqxGrid').jqxGrid('clearselection');
                        // $("#jqxGrid").jqxGrid('clearfilters');
                    }
                    else {
                        swal(data.errorLog, "{{ trans('modOrder.error') }}", "error");
                    }
                    loadingFunc.hide();
                    $('#jqxGrid').jqxGrid('updatebounddata');
                    return;
                }
            });
            $('#jqxGrid1').jqxGrid('clear');
            $('#jqxGrid').jqxGrid('updatebounddata');
        }

        var getGridCbm = function() {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            return sum;
        }

        $("#emptyBtn").on("click", function(){
            document.getElementById('dlvForm').reset();
            $("#carCbm").text(0);
            $("#loadRate").text(0);
        })
        $("#delrowbtn").on("click", function(){
            var rows = $("#jqxGrid1").jqxGrid('selectedrowindexes');
            console.log(rows);
            var ids = new Array();
            for(var m = 0; m < rows.length; m++) {
                var id = $("#jqxGrid1").jqxGrid('getrowid', rows[m]);
                ids.push(id);
            }
            for(var n = 0; n < ids.length; n++) {
                $("#jqxGrid1").jqxGrid('deleterow', ids[n]);
            }
            $('#jqxGrid1').jqxGrid('clearselection');
        })
        $("#readybackbtn").on("click", function(){
            var ids = new Array();
            if(focus == "1" ){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                for(var m = 0; m < rows.length; m++) {
                    var data = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    ids.push(data.id);
                }
            } else {
                var rows = $("#jqxGrid1").jqxGrid('selectedrowindexes');
                var rowdetail = new Array();
                for(var m = 0; m < rows.length; m++) {
                    var data = $("#jqxGrid1").jqxGrid('getrowdata', rows[m]);
                    ids.push(data.id);
                    var id = $("#jqxGrid1").jqxGrid('getrowid', rows[m]);
                    rowdetail.push(id);
                }

                for(var n = 0; n < rowdetail.length; n++) {
                    $("#jqxGrid1").jqxGrid('deleterow', rowdetail[n]);
                }
                $('#jqxGrid1').jqxGrid('clearselection');
            }

            
            var postData = {
                ids: ids
            }
            $.ajax({
                url: "{{ url(config('backpack.base.route_prefix', 'admin').'/TranPlanMgmt/readyback') }}",
                type: 'POST',
                async: false,
                data: postData,
                beforeSend: function () {
                    loadingFunc.show();
                },
                error: function (jqXHR, exception) {
                    swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    loadingFunc.hide();
                    return;
                },
                success: function (data) {
                    if(data.msg == "success") {
                        swal('Success', "{{ trans('modOrder.sendSuccess') }}", "success");
                        
                        $('#jqxGrid').jqxGrid('clearselection');
                        // $("#jqxGrid").jqxGrid('clearfilters');
                    }
                    else {
                        swal(data.errorLog, "{{ trans('modOrder.error') }}", "error");
                    }
                    loadingFunc.hide();
                    $('#jqxGrid').jqxGrid('updatebounddata');
                    return;
                }
            });

        })

        $("#car_no").on("change", function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            if(!chkLoadRate(sum)) {
                $("#gridCbm").css('color', 'red');
            }
            else {
                $("#gridCbm").css('color', '#000');
            }

            var cbm          = parseFloat($("#cbm").val());
            var loadRate     = parseFloat($("#load_rate").val());
            var gridLoadRate = sum / cbm;
            $("#gridLoadRate").text((Math.round(gridLoadRate * 100)) + "%");

            if(gridLoadRate > loadRate) {
                $("#gridLoadRate").css('color', 'red');
            }
            else {
                $("#gridLoadRate").css('color', '#000');
            }
        });

        $("#urgentBtn").on("click", function(){
            if(focus=="1"){
                var idx = $('#jqxGrid').jqxGrid('getselectedrowindex');
                if(idx == -1) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }
            }else{
                var idx = $('#jqxGrid1').jqxGrid('getselectedrowindex');
                if(idx == -1) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }
            }
            showview("Y");        
        });
        

        $("#insertBtn").on("click", function(){
            if(focus=="1"){
                var idx = $('#jqxGrid').jqxGrid('getselectedrowindex');
                if(idx == -1) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }
            }else{
                var idx = $('#jqxGrid1').jqxGrid('getselectedrowindex');
                if(idx == -1) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }
            }
            showview("N")
        });

        $("#confirmInsertBtn").on("click", function(){
            var idx = $('#dlvPlanGrid').jqxGrid('getselectedrowindex');
            if(idx == -1) {
                swal("請選擇您要插入的派車單號", "", "warning");
                return;
            }
            var rowData = $('#dlvPlanGrid').jqxGrid('getrowdata', idx);
            var rows = "";
            if(focus=="1"){
                rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            }else{
                rows = $("#jqxGrid1").jqxGrid('selectedrowindexes');
            }
            var selectedRecords = new Array();
            var dlvData = [];
            if( (rowData.dlv_num+rows.length) >dlvlimit ){
                swal('警告', "派車上限不可超過"+dlvlimit+"筆", "warning");
                return;
            }
            for (var m = 0; m < rows.length; m++) {
                if(focus=="1"){
                    row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                }else{
                    row = $("#jqxGrid1").jqxGrid('getrowdata', rows[m]);
                }
                // var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if((row.dlv_addr == "" || row.dlv_addr == null) && (row.pick_addr == "" || row.pick_addr == null)) {
                    swal('警告', "訂單號："+row.sys_ord_no+"，提貨地址、配送地址不能為空", "warning");
                    return;
                }
                dlvData.push(row.id);
            }

            var postData = {
                dlv_no      : rowData.dlv_no,
                ord_ids     : dlvData,
                is_urgent   : is_urgent
            }
            $.ajax({
                url: "{{ url(config('backpack.base.route_prefix', 'admin').'/TranPlanMgmt/insertToDlv') }}",
                type: 'POST',
                async: false,
                data: postData,
                beforeSend: function () {
                    loadingFunc.show();
                },
                error: function (jqXHR, exception) {
                    swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    loadingFunc.hide();
                    return;
                },
                success: function (data) {
                    if(data.msg == "success") {
                        swal('Success', "{{ trans('modOrder.sendSuccess') }}", "success");
                        $('#jqxGrid1').jqxGrid('clear');
                        $('#jqxGrid').jqxGrid('clearselection');
                        $("#dlvPlanModal").modal('hide');
                    }
                    else {
                        swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    }
                    loadingFunc.hide();
                    $('#jqxGrid').jqxGrid('updatebounddata');
                    return;
                }
            });
        });

        $("#btnLayout").on("click", function(){
			$('#gridOptModal').modal('show');
		});

        $("#saveGrid").on("click", function(){
			var items = $("#jqxlistbox").jqxListBox('getItems');
			$("#jqxGrid").jqxGrid('beginupdate');

			$.each(items, function(i, item) {
				var thisIndex = $('#jqxGrid').jqxGrid('getcolumnindex', item.value)-1;
				if(thisIndex != item.index){
					//console.log(item.value+":"+thisIndex+"="+item.index);
					$('#jqxGrid').jqxGrid('setcolumnindex', item.value,  item.index);
				}
				if (item.checked) {
					$("#jqxGrid").jqxGrid('showcolumn', item.value);
				}
				else {
					$("#jqxGrid").jqxGrid('hidecolumn', item.value);
				}
			})
			
			$("#jqxGrid").jqxGrid('endupdate');
			var state = $("#jqxGrid").jqxGrid('getstate');

			var saveUrl = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/saveLayoutJson') }}"; 	  	  	
			var stateToSave = JSON.stringify(state);

			$.ajax({
				type: "POST",										
				url: saveUrl,		
				data: { data: stateToSave,key: "dlvCarGrid" },		 
				success: function(response) {
					if(response == "true"){
						swal("save successful", "", "success");
						$("#gridOptModal").modal('hide');
					}else{
						swal("save failded", "", "error");
					}
					
				}
			});	
		});

		$("#clearLayout").on("click", function(){
			$.ajax({
				type: "POST",										
				url: BASE_API_URL + '/admin/baseApi/clearLayout',		
				data: { key: "dlvCarGrid" },		 
				success: function(response) {
					if(response == "true"){
						location.reload();
					}else{
						swal("save failded", "", "error");
					}
					
				}
			});	
		});
        
        function showview($urgent) {
            is_urgent = $urgent;
            var idx = $('#dlvPlanGrid').jqxGrid('getselectedrowindex');
            var rowData= "";
            if(focus=="1"){
                rowData = $('#jqxGrid').jqxGrid('getrowdata', idx);
            }else{
                rowData = $('#jqxGrid1').jqxGrid('getrowdata', idx);
            }
            var statusCode = [
                {value: "UNTREATED", label: "{{ trans('modDlv.STATUS_UNTREATED') }}"},
                {value: "SEND", label: "{{ trans('modDlv.STATUS_SEND') }}"},
                {value: "DLV", label: "{{ trans('modDlv.STATUS_DLV') }}"},
                {value: "FINISHED", label: "{{ trans('modDlv.STATUS_FINISHED') }}"}
            ]

            var statusSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: statusCode
            };

            var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                autoBind: true
            });
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'id', type: 'number' },
                    { name: 'created_at', type: 'range', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
                    { name: 'dlv_no', type: 'string' },
                    { name: 'status', type: 'string', value: 'status', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
                    { name: 'car_no', type: 'string' },
                    { name: 'car_type', type: 'string' },
                    { name: 'driver_nm', type: 'string' },
                    { name: 'dlv_num', type: 'string' },
                ],
                root:"Rows",
                pagenum: 0,		
                filter: function () {
                    // update the grid and send a request to the server.
                    $("#dlvPlanGrid").jqxGrid('updatebounddata', 'filter');
                },
                sort: function () {
                    // update the grid and send a request to the server.
                    $("#dlvPlanGrid").jqxGrid('updatebounddata', 'sort');
                },			
                beforeprocessing: function (data) {
                        var ostatus = "";
                        for(i in data[0].Rows) {
                            if(i == 0) {
                                ostatus = data[0].Rows[i].status;
                                items.push(data[0].Rows[i].status);
                            }

                            if(ostatus != data[0].Rows[i].status) {
                                items.push(data[0].Rows[i].status);
                            }

                            ostatus = data[0].Rows[i].status;
                        }
                        source.totalrecords = data[0].TotalRows;
                    },
                pagesize: 10,
                cache: false,
                url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_dlv') }}" + "?basecon=status;NOT_EQUAL;FINISHED",
                sortcolumn: 'ord_no',
                sortdirection: 'asc'
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $('#dlvPlanGrid').jqxGrid('destroy');
            $("#dlvPlanBody").html('<div id="dlvPlanGrid"></div>');
            $("#dlvPlanGrid").jqxGrid(
            {
                width: '100%',
                //height: '100%',
                autoheight: true,
                source: dataAdapter,
                sortable: true,
                filterable: true,
                altrows: true,
                virtualmode: true,
                pagesize: 99999999,
                pageable: true,
                autoshowfiltericon: true,
                columnsreorder: true,
                columnsautoresize: true,
                showstatusbar: true,
                //autoloadstate: true,
                clipboard: true,
                pagesizeoptions:[50, 100, 500],
                selectionmode: 'singlerow',
                showfilterrow: true,
                columnsresize: true,
                rendergridrows: function (params) {
                    //alert("rendergridrows");
                    return params.data;
                },
                renderstatusbar: function (statusbar) {
                    var rowCount = $("#dlvPlanGrid").jqxGrid('getrows').length;
                },
                ready: function () {
                    loadState();
                    //$('#jqxGrid').jqxGrid('autoresizecolumns');
                },
                updatefilterconditions: function (type, defaultconditions) {
                    var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                    var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                    switch (type) {
                        case 'stringfilter':
                            return stringcomparisonoperators;
                        case 'numericfilter':
                            return numericcomparisonoperators;
                        case 'datefilter':
                            return datecomparisonoperators;
                        case 'booleanfilter':
                            return booleancomparisonoperators;
                    }
                },
                columns: [
                    { text: 'id', datafield: 'id', width: 50, hidden: true },
                    { text: '{{trans("modDlv.createdAt")}}', datafield: 'created_at', width: 150, cellsformat: 'yyyy-MM-dd hh:mm:ss',filtertype: 'date' },
                    { text: '{{trans("modDlv.dlvNo")}}', datafield: 'dlv_no', width: 150, hidden: false },
                    { text: '{{trans("modDlv.status")}}', datafield: 'status', width: 110},
                    { text: '{{trans("modDlv.carNo")}}', datafield: 'car_no', width: 150 },
                    { text: '{{trans("modDlv.carType")}}', datafield: 'car_type', width: 150 },
                    { text: '{{trans("modDlv.driverNm")}}', datafield: 'driver_nm', width: 150 },
                    { text: '{{trans("modDlv.dlvNum")}}', datafield: 'dlv_num', width: 150 }
                ]
            });
            $("#dlvPlanModal").modal('show');
        }
        $("#dlvPlanGrid").on('bindingcomplete', function (event) {
        var statusHeight = 0;
        //$("#"+gridId).jqxGrid("hideloadelement"); 
        });
        function loadState() {  	
			var loadURL = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/getLayoutJson') }}"; 	  	  	
			$.ajax({
				type: "GET", //  OR POST WHATEVER...
				url: loadURL,
				data: { key: 'dlvCarGrid' },		 
				success: function(response) {										
					if (response != "") {	
						response = JSON.parse(response);
					}
					var listSource = [];
                    state = $("#jqxGrid").jqxGrid('getstate');
                    console.log(gridheight);
                    $("#jqxGrid").jqxGrid({height:gridheight});
                    setTimeout(function(){ $("#jqxGrid1") .jqxGrid('loadstate', state );},1000);
					$.each(state.columns, function(i, item) {
						if(item.text != "" && item.text != "undefined"){
							listSource.push({ 
							label: item.text, 
							value: i, 
							checked: !item.hidden });
						}
                    });
					$("#jqxlistbox").jqxListBox({ 
						allowDrop: true, 
						allowDrag: true,
						source: listSource,
						width: "99%",
						height: 500,
						checkboxes: true,
						filterable: true,
						searchMode: 'contains'
					});
				}
			});			  	  	  	  	
		}
    });
    $(document).ready(function() {
    // 在這撰寫javascript程式碼
        $("[href='#tab_1']").click();   
        $('#orderno').focus();
        state = $("#jqxGrid").jqxGrid('getstate');
        setTimeout(function(){ $("#jqxGrid1") .jqxGrid('loadstate', state );},1000);
    });
    function tab_1() {
        focus = "1";
        document.getElementById('jqxGrid').style.display = 'block';
        document.getElementById('jqxGrid1').style.display = 'none';
        state = $("#jqxGrid").jqxGrid('getstate');
        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        if(rows!=undefined){
            $("#selectcount").text(rows.length);
        }
        setTimeout(function(){ $("#jqxGrid1") .jqxGrid('loadstate', state );},1500);
    }
    function tab_2() {
        focus = "2";
        document.getElementById('jqxGrid').style.display = 'none';
        document.getElementById('jqxGrid1').style.display = 'block';
        state = $("#jqxGrid").jqxGrid('getstate');
        $("#selectcount").text('');
        setTimeout(function(){ $("#jqxGrid1") .jqxGrid('loadstate', state );},1500);
    }
    $("#orderno").keypress(function(e){

    code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13)
        {
            //targetForm是表單的ID
            addgridrow();
        }
    });
    $("#noorder").keypress(function(e){

    code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13)
        {
            //targetForm是表單的ID
            addgridrow();
        }
    });
    function addgridrow() {  
    var ordno1 =  $('#noorder').val();
    var ordno2 =  $('#orderno').val();
    var ordno  =  "";
    if(ordno1.length>0){
        ordno =ordno1;
    }else if(ordno2.length>0) {
        ordno =ordno2;
    }
    $.ajax({
            url:BASE_URL + '/dlvorder/check/',
            type:"GET",
            data:{ ord_no: ordno },
            success:function(data){
                if(data.msg == "success") {
                    $("[href='#tab_2']").click();   
                    console.log("success");
                    var exist= "N";
                    var allrows = $("#jqxGrid1").jqxGrid('getrows');
                    for (var m = 0; m < allrows.length; m++) {
                        if(allrows[m].ord_no==data.data.ord_no){
                            exist ="Y";
                        }
                    }
                    if(exist=="N"){
                        var row = {};
                        row["id"]=data.data.id;
                        var status="";
                        if(data.data.status=="UNTREATED"){
                            status="{{ trans('modOrder.STATUS_UNTREATED') }}";
                        }
                        if(data.data.status=="SEND"){
                            status="{{ trans('modOrder.STATUS_SEND') }}";
                        }
                        if(data.data.status=="DLV"){
                            status="{{ trans('modOrder.STATUS_DLV') }}";
                        }
                        if(data.data.status=="PICKED"){
                            status="{{ trans('modOrder.STATUS_PICKED') }}";
                        }
                        if(data.data.status=="SETOFF"){
                            status="{{ trans('modOrder.STATUS_SETOFF') }}";
                        }
                        row["status"]=status;          
                        row["etd"]=data.data.etd;
                        row["is_inbound"]=data.data.is_inbound;
                        row["sys_ord_no"]=data.data.sys_ord_no;
                        row["ord_no"]=data.data.ord_no;
                        row["pick_cust_no"]=data.data.pick_cust_no;
                        row["pick_cust_nm"]=data.data.pick_cust_nm;
                        row["dlv_cust_no"]=data.data.dlv_cust_no;
                        row["dlv_cust_nm"]=data.data.dlv_cust_nm;
                        row["remark"]=data.data.remark;
                        row["pick_remark"]=data.data.pick_remark;
                        row["dlv_remark"]=data.data.dlv_remark;
                        row["owner_nm"]=data.data.owner_nm;
                        row["total_cbm"]=data.data.total_cbm;
                        row["pkg_num"]=data.data.pkg_num;
                        row["pick_city_nm"]=data.data.pick_city_nm;
                        row["pick_area_nm"]=data.data.pick_area_nm;
                        row["pick_addr"]=data.data.pick_addr;
                        row["dlv_city_nm"]=data.data.dlv_city_nm;
                        row["dlv_area_nm"]=data.data.dlv_area_nm;
                        row["dlv_addr"]=data.data.dlv_addr;
                        row["dlv_tel"]=data.data.dlv_tel;
                        row["is_urgent"]=data.data.is_urgent;
                        row["s_key"]=data.data.s_key;
                        row["created_at"]=data.data.created_at;
                        focusc= '2';
                        try {
                        // 需要測試的語句
                            var commit = $("#jqxGrid1").jqxGrid('addrow', null,row);
                        }
                        catch (e) {
                        }
                    }
                    $('#noorder').val('');
                    $('#orderno').val('');
                }
                else{
                    swal('警告', "訂單號："+ordno+"，不存在", "warning");
                    $('#noorder').val('');
                    $('#orderno').val('');
                }

            },
            error:function(err){
                // uxAlert("網路連線失敗,稍後重試",err);
            }
        });

    return false;
    }
</script>    

@endsection
