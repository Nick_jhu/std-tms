@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      {{ trans('dlvCar.deliveryWork') }}<small></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">{{ trans('dlvCar.deliveryWork') }}</li>
      </ol>
    </section>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form action="" id="dlvForm">
                    <p id="warningText" style="color:red"></p>
                    <p>{{ trans('dlvCar.carCapacity') }}：<span id="carCbm">0</span>&nbsp;&nbsp;&nbsp;{{ trans('dlvCar.selected') }}：<span id="gridCbm">0</span>  {{ trans('dlvCar.loadingRate') }}：<span id="loadRate">0%</span>&nbsp;&nbsp;&nbsp;{{ trans('dlvCar.selectedRate') }}：<span id="gridLoadRate">0%</span></p>
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="dlv_date">{{ trans('dlvCar.sendCarDate') }}</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="dlv_date" name="dlv_date">
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="cust_nm">{{ trans('dlvCar.carDealer') }}</label>
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" id="cust_nm" name="cust_nm" @if(isset($viewData)) value="{{$viewData['cname']}}" @endif>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="cust_nm"
                                    info1="{{Crypt::encrypt('sys_customers_item')}}" 
                                    info2="{{Crypt::encrypt('cust_no+cname,cust_no,cname')}}" 
                                    info3="{{Crypt::encrypt('cust_type=\'CT003\'')}}"
                                    info4="cust_no=cust_no;cname=cust_nm" triggerfunc="" selectionmode="singlerow">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </div>
                            <input type="hidden" class="form-control" id="cust_no" name="cust_no" @if(isset($viewData)) value="{{$viewData['cust_no']}}" @endif>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="car_no">{{ trans('dlvCar.carNo') }}</label>
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" id="car_no" name="car_no">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="car_no"
                                    info1="{{Crypt::encrypt('mod_car')}}" 
                                    info2="{{Crypt::encrypt('car_no,driver_nm,phone,car_no,car_type,cbm,load_rate')}}" 
                                    info3="{{Crypt::encrypt('')}}"
                                    info4="car_no=car_no;driver_nm=driver_nm;cbm=cbm;phone=driver_phone;load_rate=load_rate;" triggerfunc="" selectionmode="singlerow">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </div>
                            
                            <input type="hidden" class="form-control" id="load_rate" name="load_rate">
                            <input type="hidden" class="form-control" id="cbm" name="cbm">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="car_type">{{ trans('dlvCar.carType') }}</label>
                            {{--  <input type="text" class="form-control" id="car_type_nm" disabled="disabled">
                            <input type="hidden" class="form-control" id="car_type">  --}}
                            <select class="form-control" name="car_type" id="car_type"></select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="driver_nm">{{ trans('dlvCar.driver') }}</label>
                            <input type="text" class="form-control" id="driver_nm">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="driver_phone">{{ trans('dlvCar.phone') }}</label>
                            <input type="text" class="form-control" id="driver_phone" name="driver_phone">
                        </div>
                    </div>
                    
                    <button type="button" class="btn btn-primary" id="confirmBtn">{{ trans('dlvCar.confirmSendCar') }}</button>
                    <button type="button" class="btn btn-danger" id="urgentBtn">{{ trans('dlvCar.urgent') }}</button>
                    <button type="button" class="btn btn-warning" id="insertBtn">{{ trans('dlvCar.insertOrd') }}</button>
                    @can('dlvCarFieldAd')
                    <button type="button" class="btn btn-primary" id="btnLayout">{{ trans('excel.AdjustmentField') }}</button>
                    @endcan
                    <button type="button" class="btn btn-danger" id="emptyBtn">{{ trans('dlvCar.clear') }}</button>
                    @can('AutoDelivery')
                    <button type="button" class="btn btn-warning" id="autosend">{{ trans('dlvCar.AutoDelivery') }}</button>
                    @endcan
                    <label for="sendtoapp"style="float:right">{{ trans('dlvCar.SendDirectly') }}</label>
                    <input type="checkbox"style="float:right" id="sendtoapp" name="sendtoapp" @if($viewData['checkData']!="") checked="{{$viewData['checkData']}}" @endif >
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div id="jqxGrid"></div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="dlvPlanModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">派車主檔</h4>
			</div>
			<div class="modal-body" id="dlvPlanBody">
				<div id="dlvPlanGrid"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="confirmInsertBtn">確定插入</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearLayout">{{ trans('common.clear') }}</button>
			</div>
		</div>
	</div>
</div>

@endsection

@include('backpack::template.lookup')


@section('after_scripts')

<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>


<script>
    var is_urgent ="N";
    function getCarType(carType) {
        $.get(BASE_URL+'/getCarType', {'carType': carType}, function(data){
            var str = "";

            for(i in data) {
                str += '<option value="'+data[i].cd+'">'+data[i].cd_descp+'</option>';
            }

            $("#car_type").html(str);
        }, "JSON");
    }
    $(function(){
        $('button[btnName="car_no"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('car_no', "{{ trans('dlvCar.car') }}", callBackFunc=function(data){
                console.log(data);
                getCarType(data.car_type);
                $("#carCbm").text(data.cbm);
                $("#loadRate").text((data.load_rate * 100) + '%');
            });
            refixwidth();
        });
        $('#car_no').on('click', function(){
            var check = $('#car_no').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("dlvForm","car_no",callBackFunc=function(data){
                    getCarType(data.car_type);
                    $("#carCbm").text(data.cbm);
                    $("#loadRate").text((data.load_rate * 100) + '%');
                });
            }
        });
        function refixwidth () {
            setTimeout(function(){$('#lookupGrid').jqxGrid('autoresizecolumns');},1000);
        }

        $('button[btnName="cust_nm"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('cust_nm', "{{ trans('dlvCar.carDealer') }}");
        });

        $('#cust_nm').on('click', function(){
            var check = $('#cust_nm').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("dlvForm","cust_nm");
            }
        });

        

        $.jqx.theme = "bootstrap";
        var items = new Array();
        $.get(BASE_API_URL+'/admin/baseApi/getFieldsJson/mod_order', {'key': 'dlvCarGrid'}, function(data){
            var statusCode = [
                {value: "UNTREATED", label: "{{ trans('modOrder.STATUS_UNTREATED') }}"},
                {value: "SEND", label: "{{ trans('modOrder.STATUS_SEND') }}"},
                {value: "DLV", label: "{{ trans('modOrder.STATUS_DLV') }}"},
                {value: "PICKED", label: "{{ trans('modOrder.STATUS_PICKED') }}"},
                {value: "SETOFF", label: "{{ trans('modOrder.STATUS_SETOFF') }}"}
            ]

            var statusSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: statusCode
            };

            var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                autoBind: true
            });

            console.log(statusAdapter);
            
            var datafields = [
                        { name: 'id', type: 'number' },
                        { name: 'status', type: 'string', value: 'status', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
                        { name: 'etd', type: 'date', cellsformat: 'yyyy-MM-dd' },
                        { name: 'sys_ord_no', type: 'string' },
                        { name: 'ord_no', type: 'string' },
                        { name: 'pick_cust_no', type: 'string' },
                        { name: 'pick_cust_nm', type: 'string' },
                        { name: 'dlv_cust_no', type: 'string' },
                        { name: 'dlv_cust_nm', type: 'string' },
                        { name: 'remark', type: 'string' },
                        { name: 'pick_remark', type: 'string' },
                        { name: 'dlv_remark', type: 'string' },
                        //{ name: 'owner_cd', type: 'string' },
                        { name: 'owner_nm', type: 'string' },
                        { name: 'total_cbm', type: 'float' },
                        { name: 'pkg_num', type: 'float' },
                        { name: 'dlv_attn', type: 'string' },
                        { name: 'pick_city_nm', type: 'string' },
                        { name: 'pick_area_nm', type: 'string' },
                        { name: 'pick_addr', type: 'string' },
                        { name: 'dlv_city_nm', type: 'string' },
                        { name: 'dlv_area_nm', type: 'string' },
                        { name: 'dlv_addr', type: 'string' },
                        { name: 'dlv_tel', type: 'string' },
                        { name: 'is_urgent', type: 'string' },   
                        { name: 's_key', type: 'string' },         
                    ];
            // var baseCondition = "{{Crypt::encrypt(' (status=\'PICKED\'AND dlv_type=\'P\') or status=\'UNTREATED\'')}}";
            var baseCondition = "{{Crypt::encrypt(' (status=\'UNTREATED\' or ( dlv_type=\'P\' and status=\'PICKED\') )')}}";
            var source =
                {
                    datatype: "json",
                    datafields: datafields,
                    root:"Rows",
                    pagenum: 0,	
                    sortcolumn: 'etd',
                    sortdirection: 'desc',	
                    beforeprocessing: function (data) {
                        var ostatus = "";
                        for(i in data[0].Rows) {
                            if(i == 0) {
                                ostatus = data[0].Rows[i].status;
                                items.push(data[0].Rows[i].status);
                            }

                            if(ostatus != data[0].Rows[i].status) {
                                items.push(data[0].Rows[i].status);
                            }

                            ostatus = data[0].Rows[i].status;
                        }
                        source.totalrecords = data[0].TotalRows;
                    },
                    filter: function () {
                    // update the grid and send a request to the server.
                        $("#jqxGrid").jqxGrid('updatebounddata', 'filter');
                        $("#jqxGrid").jqxGrid('clearselection');
                    },
                    sort: function () {
                        // update the grid and send a request to the server.
                        $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
                        $("#jqxGrid").jqxGrid('clearselection');
                    },
                    //url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order') }}" + "?basecon=status;EQUAL;UNTREATED)*(dlv_type;EQUAL;P&defOrder=etd;desc",					
                    url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order') }}" + "?dbRaw="+baseCondition,					
                    pagesize: 50,
                };
            console.log(items);
            var columns = [
                { text: "id", datafield: 'id', width: 50, hidden:true, filtertype: 'number' },
                { text: "{{ trans('modOrder.status') }}", datafield: 'status', width: '120', filterable:false },
                { text: "{{ trans('modOrder.etd') }}", datafield: 'etd', width: 100,cellsformat: 'yyyy-MM-dd',filtertype: 'date' },
                { text: "{{ trans('modOrder.sysOrdNo') }}", datafield: 'sys_ord_no', width:120, filtertype: 'textbox' },
                { text: "{{ trans('modOrder.ordNo') }}", datafield: 'ord_no', width:120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.pickCustNo') }}", datafield: 'pick_cust_no', width: 120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.pickCustNm') }}", datafield: 'pick_cust_nm', width:120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvCustNo') }}", datafield: 'dlv_cust_no', width: 120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvCustNm') }}", datafield: 'dlv_cust_nm', width:120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.remark') }}", datafield: 'remark', width: 300, filtertype:'textbox' },
                { text: "{{ trans('modOrder.pickRemark') }}", datafield: 'pick_remark', width: 300, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvRemark') }}", datafield: 'dlv_remark', width: 300, filtertype:'textbox' },
                //{ text: "{{ trans('modOrder.ownerCd') }}", datafield: 'owner_cd', width:120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.ownerNm') }}", datafield: 'owner_nm', width:120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.totalCbm') }}", datafield: 'total_cbm', width: 70, cellsalign: 'right', filtertype: 'number' },
                { text: "{{ trans('modOrder.pkgNum') }}", datafield: 'pkg_num', width: 70, cellsalign: 'right', filtertype: 'number' },
                { text: "{{ trans('modOrder.dlvAttn') }}", datafield: 'dlv_attn', width: 100, filtertype:'textbox' },
                { text: "{{ trans('modOrder.pickCityNm') }}", datafield: 'pick_city_nm', width: 100, filtertype:'textbox' },
                { text: "{{ trans('modOrder.pickAreaNm') }}", datafield: 'pick_area_nm', width: 100, filtertype:'textbox' },
                { text: "{{ trans('modOrder.pickAddr') }}", datafield: 'pick_addr', width: 300, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvCityNm') }}", datafield: 'dlv_city_nm', width: 100, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvAreaNm') }}", datafield: 'dlv_area_nm', width: 100, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvAddr') }}", datafield: 'dlv_addr', width: 300, filtertype:'textbox' },
                { text: "{{ trans('modOrder.dlvTel') }}", datafield: 'dlv_tel', width: 120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.isUrgent') }}", datafield: 'is_urgent', width: 120, filtertype:'textbox' },
                { text: "{{ trans('modOrder.sKey') }}", datafield: 's_key', width: 120, filtertype:'textbox' },
            ];
            /*var dataAdapter = new $.jqx.dataAdapter(source, {
                loadComplete: function () {

                }
            });*/
            var dataAdapter = new $.jqx.dataAdapter(source, { 
                async: false, 
                loadError: function (xhr, status, error) { 
                    alert('Error loading "' + source.url + '" : ' + error); 
                },
                loadComplete: function() {
                    $("#jqxGrid").jqxGrid({height: $( window ).height() - 350});
                }
            });
            $("#jqxGrid").jqxGrid(
            {
                width: '100%',
                height: $( window ).height() - 350,//'100%',//(typeof gridOpt.height == 'undefined')?800:gridOpt.height,
                //autoheight: true,
                source: dataAdapter,
                sortable: true,
                filterable: true,
                altrows: true,
                showfilterrow: true,
                pageable: true,
                virtualmode: true,
                autoshowfiltericon: true,
                columnsreorder: true,
                columnsresize: true,
                columnsautoresize: true,
                showstatusbar: true,
                pagesize: 99999999,
                //autoloadstate: true,
                clipboard: true,
                selectionmode: 'checkbox',
                enablebrowserselection: true,
                pagesizeoptions:[50, 100, 500, 9999],
                rendergridrows: function (params) {
                    //alert("rendergridrows");
                    return params.data;
                },
                renderstatusbar: function (statusbar) {
                    var rowCount = $("#jqxGrid").jqxGrid('getrows').length;
                    statusbar.append('<div style="margin: 5px;">總筆數: ' + rowCount + '</div>');
                },
                ready: function () {
                    loadState();
                    //$('#jqxGrid').jqxGrid('autoresizecolumns');
                },
                columns: columns
            });

            $("#jqxGrid").on('bindingcomplete', function (event) {
                var statusHeight = 50;
                var winHeight = $( window ).height() - 350 + statusHeight;
                $("#jqxGrid").jqxGrid('height', winHeight+'px');
            //$("#"+gridId).jqxGrid("hideloadelement"); 
            });

            if(typeof data[2] !== "undefined") {
                $("#jqxGrid").jqxGrid('loadstate', data[2]);
            }
            
        });
        

        var chkLoadRate = function(gridCbm) {
            var carCbm = parseInt($("#cbm").val());
            var loadRate = parseFloat($("#load_rate").val());

            var carLoadable = carCbm * loadRate;
            if(carCbm > 0) {
                if(gridCbm > carCbm) {
                    return -1;
                }
                else if(gridCbm > carLoadable) {
                    return 0;
                }
            }

            return 1;
        }

        $("#jqxGrid").on("rowselect", function (event) {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            if(chkLoadRate(sum) == 0) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("");
            }
            else if(chkLoadRate(sum) == -1) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("{{ trans('dlvCar.msg1') }}");
            }
            else {
                $("#gridCbm").css('color', '#000');
                $("#warningText").text("");
            }

            var cbm          = parseFloat($("#cbm").val());
            var loadRate     = parseFloat($("#load_rate").val());
            if(cbm) {
                var gridLoadRate = sum / cbm;
                $("#gridLoadRate").text((Math.round(gridLoadRate * 100)) + "%");

                if(gridLoadRate > loadRate) {
                    $("#gridLoadRate").css('color', 'red');
                }
                else {
                    $("#gridLoadRate").css('color', '#000');
                }
            }
            

            $("#gridCbm").text(sum.toFixed(2));
        });  

        $('#jqxGrid').on('rowunselect', function (event) 
        {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            if(chkLoadRate(sum) == 0) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("");
            }
            else if(chkLoadRate(sum) == -1) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("{{ trans('dlvCar.msg1') }}");
            }
            else {
                $("#gridCbm").css('color', '#000');
                $("#warningText").text("");
            }

            var cbm          = parseFloat($("#cbm").val());
            var loadRate     = parseFloat($("#load_rate").val());
            if(cbm) {
                var gridLoadRate = sum / cbm;
                $("#gridLoadRate").text((Math.round(gridLoadRate * 100)) + "%");

                if(gridLoadRate > loadRate) {
                    $("#gridLoadRate").css('color', 'red');
                }
                else {
                    $("#gridLoadRate").css('color', '#000');
                }
            }
            
            $("#gridCbm").text(sum.toFixed(2));
        });

        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        var m = (date.getMonth()+1);
        var d = date.getDate();
        if(m < 10) {
            m = '0' + m;
        }

        if(d < 10) {
            //d = '0' + (d+1);
            d = '0' + d;
        }
        $('#dlv_date').val(date.getFullYear() + '-' + m + '-' + d);
        $('#dlv_date').datepicker({
            startDate: today,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $("#confirmBtn").on("click", function(){
            var gridCbm = getGridCbm();
            if(chkLoadRate(gridCbm) == 0 || chkLoadRate(gridCbm) == -1) {
                swal({
                    text: "{{ trans('dlvCar.msg2') }}",
                    title: "{{ trans('dlvCar.msg3') }}",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: "{{ trans('dlvCar.cancel') }}",
                    confirmButtonText: "{{ trans('dlvCar.yes') }}"
                    }).then(function (result) {
                    if (result.value) {
                        sendDlvCar();
                    }
                })
            }
            else {
                sendDlvCar();
            }
        });

        $("#autosend").on("click", function(){
            var postData = {
                car_no      : $("#car_no").val(),
                driver_nm   : $("#driver_nm").val(),
                driver_phone: $("#driver_phone").val(),
                dlv_date    : $("#dlv_date").val(),
                car_type    : $("#car_type").val(),
                sendToApp   : $('#sendtoapp').prop('checked')
            }
            $.ajax({
                url: "{{ url(config('backpack.base.route_prefix', 'admin').'/TranPlanMgmt/autosend') }}",
                type: 'POST',
                async: false,
                data: postData,
                beforeSend: function () {
                    loadingFunc.show();
                },
                error: function (jqXHR, exception) {
                    swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    loadingFunc.hide();
                    return;
                },
                success: function (data) {
                    if(data.msg == "success") {
                        swal('Success', "{{ trans('modOrder.sendSuccess') }}", "success");
                        
                        $('#jqxGrid').jqxGrid('clearselection');
                        $("#jqxGrid").jqxGrid('clearfilters');
                    }
                    else {
                        swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    }
                    loadingFunc.hide();
                    $('#jqxGrid').jqxGrid('updatebounddata');
                    return;
                }
            });
        });

        var sendDlvCar = function() {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var dlvData = [];
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                console.log(row);
                if((row.dlv_addr == "" || row.dlv_addr == null) && (row.pick_addr == "" || row.pick_addr == null)) {
                    swal('警告', "訂單號："+row.sys_ord_no+"，提貨地址、配送地址不能為空", "warning");
                    return;
                }

                if(((row.dlv_cust_nm != "" && row.dlv_cust_nm != null) || (row.dlv_cust_no != "" && row.dlv_cust_no != null)) && (row.dlv_addr == "" || row.dlv_addr == null)) {
                    swal('警告', "訂單號："+row.sys_ord_no+"，配送地址不能為空", "warning");
                    return;
                }

                if(((row.pick_cust_nm != "" && row.pick_cust_nm != null) || (row.pick_cust_no != "" && row.pick_cust_no != null)) && (row.pick_addr == "" || row.pick_addr == null)) {
                    swal('警告', "訂單號："+row.sys_ord_no+"，提貨地址不能為空", "warning");
                    return;
                }

                dlvData.push(row.id);
            }

            var postData = {
                car_no      : $("#car_no").val(),
                driver_nm   : $("#driver_nm").val(),
                driver_phone: $("#driver_phone").val(),
                dlv_date    : $("#dlv_date").val(),
                car_type    : $("#car_type").val(),
                sendToApp   : $('#sendtoapp').prop('checked'),
                ord_ids     : dlvData
            }
            if(postData.car_no == "" || postData.car_no == null) {
                swal('警告', "請輸入車號", "warning");
                return;
            }

            $.ajax({
                url: "{{ url(config('backpack.base.route_prefix', 'admin').'/TranPlanMgmt/sendDlvCar') }}",
                type: 'POST',
                async: false,
                data: postData,
                beforeSend: function () {
                    loadingFunc.show();
                },
                error: function (jqXHR, exception) {
                    swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    loadingFunc.hide();
                    return;
                },
                success: function (data) {
                    if(data.msg == "success") {
                        swal('Success', "{{ trans('modOrder.sendSuccess') }}", "success");
                        
                        $('#jqxGrid').jqxGrid('clearselection');
                        $("#jqxGrid").jqxGrid('clearfilters');
                    }
                    else {
                        swal(data.errorLog, "{{ trans('modOrder.error') }}", "error");
                    }
                    loadingFunc.hide();
                    $('#jqxGrid').jqxGrid('updatebounddata');
                    return;
                }
            });
        }

        var getGridCbm = function() {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            return sum;
        }

        $("#emptyBtn").on("click", function(){
            document.getElementById('dlvForm').reset();
            $("#carCbm").text(0);
            $("#loadRate").text(0);
        })

        $("#car_no").on("change", function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            if(!chkLoadRate(sum)) {
                $("#gridCbm").css('color', 'red');
            }
            else {
                $("#gridCbm").css('color', '#000');
            }

            var cbm          = parseFloat($("#cbm").val());
            var loadRate     = parseFloat($("#load_rate").val());
            var gridLoadRate = sum / cbm;
            $("#gridLoadRate").text((Math.round(gridLoadRate * 100)) + "%");

            if(gridLoadRate > loadRate) {
                $("#gridLoadRate").css('color', 'red');
            }
            else {
                $("#gridLoadRate").css('color', '#000');
            }
        });

        $("#urgentBtn").on("click", function(){
            var idx = $('#jqxGrid').jqxGrid('getselectedrowindex');
            if(idx == -1) {
                swal("請選擇一筆資料", "", "warning");
                return;
            }
            showview("Y");        
        });
        

        $("#insertBtn").on("click", function(){
            var idx = $('#jqxGrid').jqxGrid('getselectedrowindex');
            if(idx == -1) {
                swal("請選擇一筆資料", "", "warning");
                return;
            }
            showview("N")
        });

        $("#confirmInsertBtn").on("click", function(){
            var idx = $('#dlvPlanGrid').jqxGrid('getselectedrowindex');
            if(idx == -1) {
                swal("請選擇您要插入的派車單號", "", "warning");
                return;
            }
            var rowData = $('#dlvPlanGrid').jqxGrid('getrowdata', idx);

            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var dlvData = [];
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if((row.dlv_addr == "" || row.dlv_addr == null) && (row.pick_addr == "" || row.pick_addr == null)) {
                    swal('警告', "訂單號："+row.sys_ord_no+"，提貨地址、配送地址不能為空", "warning");
                    return;
                }
                dlvData.push(row.id);
            }

            var postData = {
                dlv_no      : rowData.dlv_no,
                ord_ids     : dlvData,
                is_urgent   : is_urgent
            }
            $.ajax({
                url: "{{ url(config('backpack.base.route_prefix', 'admin').'/TranPlanMgmt/insertToDlv') }}",
                type: 'POST',
                async: false,
                data: postData,
                beforeSend: function () {
                    loadingFunc.show();
                },
                error: function (jqXHR, exception) {
                    swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    loadingFunc.hide();
                    return;
                },
                success: function (data) {
                    if(data.msg == "success") {
                        swal('Success', "{{ trans('modOrder.sendSuccess') }}", "success");
                        
                        $('#jqxGrid').jqxGrid('clearselection');
                        $("#dlvPlanModal").modal('hide');
                    }
                    else {
                        swal('Oops...', "{{ trans('modOrder.error') }}", "error");
                    }
                    loadingFunc.hide();
                    $('#jqxGrid').jqxGrid('updatebounddata');
                    return;
                }
            });
        });

        $("#btnLayout").on("click", function(){
			$('#gridOptModal').modal('show');
		});

        $("#saveGrid").on("click", function(){
			var items = $("#jqxlistbox").jqxListBox('getItems');
			$("#jqxGrid").jqxGrid('beginupdate');

			$.each(items, function(i, item) {
				var thisIndex = $('#jqxGrid').jqxGrid('getcolumnindex', item.value)-1;
				if(thisIndex != item.index){
					//console.log(item.value+":"+thisIndex+"="+item.index);
					$('#jqxGrid').jqxGrid('setcolumnindex', item.value,  item.index);
				}
				if (item.checked) {
					$("#jqxGrid").jqxGrid('showcolumn', item.value);
				}
				else {
					$("#jqxGrid").jqxGrid('hidecolumn', item.value);
				}
			})
			
			$("#jqxGrid").jqxGrid('endupdate');
			var state = $("#jqxGrid").jqxGrid('getstate');

			var saveUrl = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/saveLayoutJson') }}"; 	  	  	
			var stateToSave = JSON.stringify(state);

			$.ajax({
				type: "POST",										
				url: saveUrl,		
				data: { data: stateToSave,key: "dlvCarGrid" },		 
				success: function(response) {
					if(response == "true"){
						swal("save successful", "", "success");
						$("#gridOptModal").modal('hide');
					}else{
						swal("save failded", "", "error");
					}
					
				}
			});	
		});

		$("#clearLayout").on("click", function(){
			$.ajax({
				type: "POST",										
				url: BASE_API_URL + '/admin/baseApi/clearLayout',		
				data: { key: "excelOrderGrid" },		 
				success: function(response) {
					if(response == "true"){
						location.reload();
					}else{
						swal("save failded", "", "error");
					}
					
				}
			});	
		});
        function showview($urgent) {
            is_urgent = $urgent;
            var idx = $('#dlvPlanGrid').jqxGrid('getselectedrowindex');
            var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx);
            var statusCode = [
                {value: "UNTREATED", label: "{{ trans('modDlv.STATUS_UNTREATED') }}"},
                {value: "SEND", label: "{{ trans('modDlv.STATUS_SEND') }}"},
                {value: "DLV", label: "{{ trans('modDlv.STATUS_DLV') }}"},
                {value: "FINISHED", label: "{{ trans('modDlv.STATUS_FINISHED') }}"}
            ]

            var statusSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: statusCode
            };

            var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                autoBind: true
            });
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'id', type: 'number' },
                    { name: 'created_at', type: 'range', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
                    { name: 'dlv_no', type: 'string' },
                    { name: 'status', type: 'string', value: 'status', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
                    { name: 'car_no', type: 'string' },
                    { name: 'car_type', type: 'string' },
                    { name: 'driver_nm', type: 'string' }     
                ],
                root:"Rows",
                pagenum: 0,		
                filter: function () {
                    // update the grid and send a request to the server.
                    $("#dlvPlanGrid").jqxGrid('updatebounddata', 'filter');
                },
                sort: function () {
                    // update the grid and send a request to the server.
                    $("#dlvPlanGrid").jqxGrid('updatebounddata', 'sort');
                },			
                beforeprocessing: function (data) {
                        var ostatus = "";
                        for(i in data[0].Rows) {
                            if(i == 0) {
                                ostatus = data[0].Rows[i].status;
                                items.push(data[0].Rows[i].status);
                            }

                            if(ostatus != data[0].Rows[i].status) {
                                items.push(data[0].Rows[i].status);
                            }

                            ostatus = data[0].Rows[i].status;
                        }
                        source.totalrecords = data[0].TotalRows;
                    },
                pagesize: 10,
                cache: false,
                url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_dlv') }}" + "?basecon=status;NOT_EQUAL;FINISHED",
                sortcolumn: 'ord_no',
                sortdirection: 'asc'
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $('#dlvPlanGrid').jqxGrid('destroy');
            $("#dlvPlanBody").html('<div id="dlvPlanGrid"></div>');
            $("#dlvPlanGrid").jqxGrid(
            {
                width: '100%',
                //height: '100%',
                autoheight: true,
                source: dataAdapter,
                sortable: true,
                filterable: true,
                altrows: true,
                virtualmode: true,
                pagesize: 99999999,
                pageable: true,
                autoshowfiltericon: true,
                columnsreorder: true,
                columnsautoresize: true,
                showstatusbar: true,
                //autoloadstate: true,
                clipboard: true,
                pagesizeoptions:[50, 100, 500],
                selectionmode: 'singlerow',
                showfilterrow: true,
                columnsresize: true,
                rendergridrows: function (params) {
                    //alert("rendergridrows");
                    return params.data;
                },
                renderstatusbar: function (statusbar) {
                    var rowCount = $("#dlvPlanGrid").jqxGrid('getrows').length;
                },
                ready: function () {
                    loadState();
                    //$('#jqxGrid').jqxGrid('autoresizecolumns');
                },
                updatefilterconditions: function (type, defaultconditions) {
                    var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                    var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                    switch (type) {
                        case 'stringfilter':
                            return stringcomparisonoperators;
                        case 'numericfilter':
                            return numericcomparisonoperators;
                        case 'datefilter':
                            return datecomparisonoperators;
                        case 'booleanfilter':
                            return booleancomparisonoperators;
                    }
                },
                columns: [
                    { text: 'id', datafield: 'id', width: 50, hidden: true },
                    { text: '{{trans("modDlv.createdAt")}}', datafield: 'created_at', width: 150, cellsformat: 'yyyy-MM-dd hh:mm:ss',filtertype: 'date' },
                    { text: '{{trans("modDlv.dlvNo")}}', datafield: 'dlv_no', width: 150, hidden: false },
                    { text: '{{trans("modDlv.status")}}', datafield: 'status', width: 110},
                    { text: '{{trans("modDlv.carNo")}}', datafield: 'car_no', width: 150 },
                    { text: '{{trans("modDlv.carType")}}', datafield: 'car_type', width: 150 },
                    { text: '{{trans("modDlv.driverNm")}}', datafield: 'driver_nm', width: 150 }
                ]
            });
            $("#dlvPlanModal").modal('show');
        }
        $("#dlvPlanGrid").on('bindingcomplete', function (event) {
        var statusHeight = 0;
        //$("#"+gridId).jqxGrid("hideloadelement"); 
        });
        function loadState() {  	
			var loadURL = "{{ url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/getLayoutJson') }}"; 	  	  	
			$.ajax({
				type: "GET", //  OR POST WHATEVER...
				url: loadURL,
				data: { key: 'dlvCarGrid' },		 
				success: function(response) {										
					if (response != "") {	
						response = JSON.parse(response);
					}
					var listSource = [];
					state = $("#jqxGrid").jqxGrid('getstate');
					$.each(state.columns, function(i, item) {
						if(item.text != "" && item.text != "undefined"){
							listSource.push({ 
							label: item.text, 
							value: i, 
							checked: !item.hidden });
						}
					});
					$("#jqxlistbox").jqxListBox({ 
						allowDrop: true, 
						allowDrag: true,
						source: listSource,
						width: "99%",
						height: 500,
						checkboxes: true,
						filterable: true,
						searchMode: 'contains'
					});
				}
			});			  	  	  	  	
		}
    });
</script>    

@endsection
