@extends('backpack::layout') 
@section('header')
<style>
    .sortable-item{
            border: 1px solid #356AA0;
            /* border-radius: 5px; */
            padding: 3px;
            background-color: white;
            color: black;
    }
    .sortable-item:hover {
          background-color: #356AA0;
          color: white;
    }

    .menu li{
        display:inline;
        padding-right:10px;
        width:5%;
        font-size: 15px;
    }
    .header li{
        display:inline;
        font-size: 15px;
    }
    .sortdiv{ 
        width:14%; display:inline-block !important; display:inline;padding-right:10px;
        vertical-align: top;
    }
    .sortdivsys{ 
        width:10%; display:inline-block !important; display:inline;padding-right:10px;
        vertical-align: top;
    }
    .sortdivzip{ 
        width:10%; display:inline-block !important; display:inline;padding-right:10px;
        vertical-align: top;
    }
    .sortdivaddr{ 
        width:20%; display:inline-block !important; display:inline;padding-right:10px;
        vertical-align: top;
    }
    .sortdivHsys{
        /* border: 1px solid; */
        background-color: #e8e8e8;
        padding-top:10px;
        width:10.25%; display:inline-block !important; display:inline; 
    }
    .sortdivHzip{
        /* border: 1px solid; */
        background-color: #e8e8e8;
        padding-top:10px;
        width:10.25%; display:inline-block !important; display:inline; 
    }
    .sortdivHaddr{ 
        /* border: 1px solid; */
        background-color: #e8e8e8;
        padding-top:10px;
        width:20.25%; display:inline-block !important; display:inline; 
    }
    .sortdivH{ 
        /* border: 1px solid; */
        background-color: #e8e8e8;
        padding-top:10px;
        width:14.25%; display:inline-block !important; display:inline; 
    }
.lmask {
position: absolute;
height: 100%;
width: 100%;
background-color: #000;
bottom: 0;
left: 0;
right: 0;
top: 0;
z-index: 9999;
opacity: 0.4;
}
.lmask.fixed {
position: fixed;
}
.lmask:before {
content: '';
background-color: rgba(0, 0, 0, 0);
border: 5px solid rgba(0, 183, 229, 0.9);
opacity: .9;
border-right: 5px solid rgba(0, 0, 0, 0);
border-left: 5px solid rgba(0, 0, 0, 0);
border-radius: 50px;
box-shadow: 0 0 35px #2187e7;
width: 50px;
height: 50px;
-moz-animation: spinPulse 1s infinite ease-in-out;
-webkit-animation: spinPulse 1s infinite linear;
margin: -25px 0 0 -25px;
position: absolute;
top: 50%;
left: 50%;
}
.lmask:after {
content: '';
background-color: rgba(0, 0, 0, 0);
border: 5px solid rgba(0, 183, 229, 0.9);
opacity: .9;
border-left: 5px solid rgba(0, 0, 0, 0);
border-right: 5px solid rgba(0, 0, 0, 0);
border-radius: 50px;
box-shadow: 0 0 15px #2187e7;
width: 30px;
height: 30px;
-moz-animation: spinoffPulse 1s infinite linear;
-webkit-animation: spinoffPulse 1s infinite linear;
margin: -15px 0 0 -15px;
position: absolute;
top: 50%;
left: 50%;
}

@-moz-keyframes spinPulse {
0% {
-moz-transform: rotate(160deg);
opacity: 0;
box-shadow: 0 0 1px #2187e7;
}
50% {
-moz-transform: rotate(145deg);
opacity: 1;
}
100% {
-moz-transform: rotate(-320deg);
opacity: 0;
}
}
@-moz-keyframes spinoffPulse {
0% {
-moz-transform: rotate(0deg);
}
100% {
-moz-transform: rotate(360deg);
}
}
@-webkit-keyframes spinPulse {
0% {
-webkit-transform: rotate(160deg);
opacity: 0;
box-shadow: 0 0 1px #2187e7;
}
50% {
-webkit-transform: rotate(145deg);
opacity: 1;
}
100% {
-webkit-transform: rotate(-320deg);
opacity: 0;
}
}
@-webkit-keyframes spinoffPulse {
0% {
-webkit-transform: rotate(0deg);
}
100% {
-webkit-transform: rotate(360deg);
}
}
  </style>
<section class="content-header">
	<h1>
		{{ trans('modDlv.titleName') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">{{ trans('modDlv.titleName') }}</li>
	</ol>
</section>
<div class='lmask' id="cssLoading" style="display:none"></div>
<div class="modal fade" tabindex="-1" role="dialog" id="dlvPlanModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">派車明細</h4>
			</div>
			<div class="modal-body" id="dlvPlanBody">
				<div id="dlvPlanGrid"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-danger" id="allRemove">確定移除</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="dlvPlanModal1">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
                <h4 class="modal-title">派車明細-出發時間：<span id="dTime"></span></h4>
                <h4 class="modal-title">出發位址：台北市大安區新生南路一段163號4樓</h4>
			</div>
			<div class="modal-body" id="dlvPlanBody1">
				<div id="dlvPlanGrid1"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-danger" id="calEta">計算</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection 
@section('before_scripts')
<script>
    var gridOpt = {};
    gridOpt.pageId        = "modDlv";
    gridOpt.enabledStatus = true;
    gridOpt.fieldsUrl     = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_dlv') }}";
    gridOpt.dataUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_dlv') }}";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key = " + gridOpt.dataUrl;
    gridOpt.createUrl     = "{{ url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar/create') }}";
    gridOpt.editUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar') }}" + "/{id}/edit";
    gridOpt.height        = 800;
    gridOpt.searchOpt     = true;
    var enabledheader = [] ;
    var filtercolumndata = [];
    var btnGroup = [
        {
            btnId: "btnSearchWindow",
            btnIcon: "fa fa-search",
            btnText: "{{ trans('common.search') }}",
            btnFunc: function () {
                $('#searchWindow').jqxWindow('open');
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
            var url  =BASE_URL;
            url = url.replace("admin","");
            var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
            filtercolumndata = [];
            for (var i = 0; i < filterGroups.length; i++) {
                var filterGroup = filterGroups[i];
                var filters = filterGroup.filter.getfilters();
                    for (var k = 0; k < filters.length; k++) {
                        if(filters[k].condition!="CONTAINS"){
                        var filtercolumn = {
                        'column': filterGroup.filtercolumn,
                        'value': Date.parse(filters[k].value),
                        'condition': filters[k].condition
                    };
                    }else{
                        var filtercolumn = {
                        'column': filterGroup.filtercolumn,
                        'value': filters[k].value,
                        'condition': filters[k].condition
                    };
                    }
                    filtercolumndata.push(filtercolumn);
                }
            }
            $.post(BASE_API_URL + '/admin/export/data', {
                'table': 'mod_dlv',
                'filename':'派車總覽' ,
                'columndata': filtercolumndata,
                'header': enabledheader,
            }, function(data){
                if(data.msg == "success") {
                    window.location.href =url+"storage/excel/"+data.downlink;
                }
            });
        }
        },
        @can('TransportationPlan_Grid')
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        @endcan
        {
            btnId:"btnSendApp",
            btnIcon:"fa fa-mobile",
            btnText:"{{ trans('modDlv.sendApp') }}",
            btnFunc:function(){
                var idx = $('#jqxGrid').jqxGrid('getselectedrowindexes');
                if(idx.length ==  0) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }

                var ids = [];
                for(i in idx) {
                    var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx[i]);
                    ids.push(rowData.id);
                }
                $.post(BASE_URL + "/TranPlanMgmt/sendToApp", {ids: ids}, function(data){
                    if(data.msg == "success") {
                        swal("{{ trans('modDlv.msg1') }}", "{{ trans('modDlv.msg2') }}", "success");
                        $('#jqxGrid').jqxGrid('updatebounddata');
                    }
                    else {
                        swal(data.error_log, "", "error");
                    }
                });
            }
        },
        @can('TransportationPlan_RoutingPlan')
        {
            btnId:"btnAutoRoute",
            btnIcon:"fa fa-road",
            btnText:"{{ trans('modDlv.routingPlan') }}",
            btnFunc:function(){
                swal("{{ trans('modDlv.msg3') }}", "", "success");
            }
        },
        @endcan
        {
            btnId:"btnCancelCar",
            btnIcon:"fa fa-ban",
            btnText:"{{ trans('modDlv.cancelCar') }}",
            btnFunc:function(){
                var idx = $('#jqxGrid').jqxGrid('getselectedrowindex');
                if(idx == -1) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }
                var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx);
                var dlvTypeCode = [
                    {value: "P", label: "提貨"},
                    {value: "D", label: "配送"},
                ]

                var dlvSource =
                {
                        datatype: "array",
                        datafields: [
                            { name: 'label', type: 'string' },
                            { name: 'value', type: 'string' }
                        ],
                        localdata: dlvTypeCode
                };

                var dlvAdapter = new $.jqx.dataAdapter(dlvSource, {
                    autoBind: true
                });

                var statusCode = [
                    {value: "UNTREATED", label: "{{ trans('modDlvPlan.STATUS_UNTREATED') }}"},
                    {value: "SEND", label: "{{ trans('modDlvPlan.STATUS_SEND') }}"},
                    {value: "DLV", label: "{{ trans('modDlvPlan.STATUS_DLV') }}"},
                    {value: "FINISHED", label: "{{ trans('modDlvPlan.STATUS_FINISHED') }}"},
                    {value: "ERROR", label: "{{ trans('modDlvPlan.STATUS_ERROR') }}"},
                ];

                var statusSource =
                {
                        datatype: "array",
                        datafields: [
                            { name: 'label', type: 'string' },
                            { name: 'value', type: 'string' }
                        ],
                        localdata: statusCode
                };

                var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                    autoBind: true
                });

                var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'id', type: 'number' },
                        { name: 'dlv_type', type: 'string', values: { source: dlvAdapter.records, value: 'value', name: 'label' } },
                        { name: 'dlv_no', type: 'string' },
                        { name: 'ord_id', type: 'number' },
                        { name: 'owner_nm', type: 'string' },
                        { name: 'status', type: 'string', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
                        { name: 'etd', type: 'date', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
                        { name: 'ord_no', type: 'string' },
                        { name: 'sys_ord_no', type: 'string' },
                        { name: 'cust_nm', type: 'string' },
                        //{ name: 'pick_cust_nm', type: 'string' },
                        //{ name: 'dlv_cust_nm', type: 'string' },
                        { name: 'addr', type: 'string' }               
                    ],
                    root:"Rows",
                    pagenum: 1,					
                    beforeprocessing: function (data) {
                    },
                    pagesize: 200,
                    cache: false,
                    url: BASE_URL + '/TranPlanMgmt/getDlvPlan/' + rowData.dlv_no,
                    sortcolumn: 'ord_no',
                    sortdirection: 'asc'
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $('#dlvPlanGrid').jqxGrid('destroy');
                $("#dlvPlanBody").html('<div id="dlvPlanGrid"></div>');
                $("#dlvPlanGrid").jqxGrid(
                {
                    width: '100%',
                    //height: '100%',
                    autoheight: true,
                    source: dataAdapter,
                    sortable: true,
                    filterable: true,
                    altrows: true,
                    selectionmode: 'checkbox',
                    showfilterrow: true,
                    columnsresize: true,
                    ready: function () {
                        $('#dlvPlanGrid').jqxGrid('autoresizecolumns');
                    },
                    columns: [
                        { text: 'id', datafield: 'id', width: 50, hidden: true },
                        { text: '{{trans("modDlv.dlvType")}}', datafield: 'dlv_type', width: 100, hidden: false },
                        { text: 'dlvNo', datafield: 'dlv_no', width: 50, hidden: true },
                        { text: 'ord_id', datafield: 'ord_id', width: 50, hidden: true },
                        { text: '{{trans("modDlv.status")}}', datafield: 'status', width: 110},
                        { text: '{{trans("modOrder.etd")}}', datafield: 'etd', width: 120, cellsformat: 'yyyy-MM-dd',filtertype: 'date' },
                        { text: '{{trans("modDlvPlanView.ownerNm")}}', datafield: 'owner_nm', width: 110},
                        { text: '{{trans("modOrder.ordNo")}}', datafield: 'ord_no', width: 120 },
                        { text: '{{trans("modOrder.sysOrdNo")}}', datafield: 'sys_ord_no', width: 120 },
                        { text: '{{trans("modDlvPlanView.custNm")}}', datafield: 'cust_nm', width: 120 },
                        //{ text: '{{trans("modOrder.pickCustNm")}}', datafield: 'pick_cust_nm', width: 100 },
                        //{ text: '{{trans("modOrder.dlvCustNm")}}', datafield: 'dlv_cust_nm', width: 101 },
                        { text: '{{trans("modOrder.dlvAddr")}}', datafield: 'addr', width: 300 }
                    ]
                });
                $("#dlvPlanModal").modal('show');
            }
        },
        {
            btnId:"btnExportDetail",
            btnIcon:"fa fa-arrow-circle-down",
            btnText:"匯出明細",
            btnFunc:function(){
                var idx = $('#jqxGrid').jqxGrid('getselectedrowindexes');
                if(idx.length ==  0) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }

                var ids = '';
                for(i in idx) {
                    var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx[i]);
                    ids += rowData.dlv_no + ';';
                }
                ids = ids.substring(0, ids.length - 1);
                location.href = BASE_URL + '/export/detail?dlvNo=' + ids;
            }
        },
        @if(Auth::user()->hasRole('Admin'))
        {
            btnId:"btnCalEta",
            btnIcon:"fa fa-clock-o",
            btnText:"預計到貨",
            btnFunc:function(){
                var idx = $('#jqxGrid').jqxGrid('getselectedrowindex');
                if(idx == -1) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }
                var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx);
                var dlvTypeCode = [
                    {value: "P", label: "提貨"},
                    {value: "D", label: "配送"},
                ]

                var dlvSource =
                {
                        datatype: "array",
                        datafields: [
                            { name: 'label', type: 'string' },
                            { name: 'value', type: 'string' }
                        ],
                        localdata: dlvTypeCode
                };

                var dlvAdapter = new $.jqx.dataAdapter(dlvSource, {
                    autoBind: true
                });

                var statusCode = [
                    {value: "UNTREATED", label: "{{ trans('modDlvPlan.STATUS_UNTREATED') }}"},
                    {value: "SEND", label: "{{ trans('modDlvPlan.STATUS_SEND') }}"},
                    {value: "DLV", label: "{{ trans('modDlvPlan.STATUS_DLV') }}"},
                    {value: "FINISHED", label: "{{ trans('modDlvPlan.STATUS_FINISHED') }}"},
                    {value: "ERROR", label: "{{ trans('modDlvPlan.STATUS_ERROR') }}"},
                ];

                var statusSource =
                {
                        datatype: "array",
                        datafields: [
                            { name: 'label', type: 'string' },
                            { name: 'value', type: 'string' }
                        ],
                        localdata: statusCode
                };

                var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                    autoBind: true
                });

                var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'id', type: 'number' },
                        { name: 'dlv_type', type: 'string', values: { source: dlvAdapter.records, value: 'value', name: 'label' } },
                        { name: 'dlv_no', type: 'string' },
                        { name: 'ord_id', type: 'number' },
                        { name: 'owner_nm', type: 'string' },
                        { name: 'status', type: 'string', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
                        { name: 'etd', type: 'date', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
                        { name: 'sys_etd', type: 'date', cellsformat: 'yyyy-MM-dd HH:mm:ss', filtertype: 'date' },
                        { name: 'ord_no', type: 'string' },
                        { name: 'sys_ord_no', type: 'string' },
                        { name: 'cust_nm', type: 'string' },
                        //{ name: 'pick_cust_nm', type: 'string' },
                        //{ name: 'dlv_cust_nm', type: 'string' },
                        { name: 'addr', type: 'string' }               
                    ],
                    root:"Rows",
                    pagenum: 1,					
                    beforeprocessing: function (data) {
                    },
                    pagesize: 200,
                    cache: false,
                    url: BASE_URL + '/TranPlanMgmt/getDlvPlan/' + rowData.dlv_no,
                    sortcolumn: 'ord_no',
                    sortdirection: 'asc'
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $('#dlvPlanGrid1').jqxGrid('destroy');
                $("#dlvPlanBody1").html('<div id="dlvPlanGrid1"></div>');
                $("#dlvPlanGrid1").jqxGrid(
                {
                    width: '100%',
                    height: '500px',
                    //autoheight: true,
                    source: dataAdapter,
                    sortable: true,
                    filterable: true,
                    altrows: true,
                    showfilterrow: false,
                    columnsresize: true,
                    selectionmode: 'singlerow',
                    enablebrowserselection: true,
                    ready: function () {
                        $('#dlvPlanGrid1').jqxGrid('autoresizecolumns');
                    },
                    columns: [
                        { text: 'id', datafield: 'id', width: 50, hidden: true },
                        { text: '{{trans("modDlv.dlvType")}}', datafield: 'dlv_type', width: 100, hidden: false },
                        { text: 'dlvNo', datafield: 'dlv_no', width: 50, hidden: true },
                        { text: 'ord_id', datafield: 'ord_id', width: 50, hidden: true },
                        { text: '{{trans("modDlv.status")}}', datafield: 'status', width: 110, hidden:true},
                        { text: '{{trans("modOrder.etd")}}', datafield: 'etd', width: 120, cellsformat: 'yyyy-MM-dd',filtertype: 'date', hidden:true },
                        { text: '{{trans("modOrder.etd")}}', datafield: 'sys_etd', width: 180, cellsformat: 'yyyy-MM-dd HH:mm:ss',filtertype: 'date' },
                        { text: '{{trans("modDlvPlanView.ownerNm")}}', datafield: 'owner_nm', width: 110},
                        { text: '{{trans("modOrder.ordNo")}}', datafield: 'ord_no', width: 120, hidden: true },
                        { text: '{{trans("modOrder.sysOrdNo")}}', datafield: 'sys_ord_no', width: 120, hidden:true },
                        { text: '{{trans("modDlvPlanView.custNm")}}', datafield: 'cust_nm', width: 120 },
                        //{ text: '{{trans("modOrder.pickCustNm")}}', datafield: 'pick_cust_nm', width: 100 },
                        //{ text: '{{trans("modOrder.dlvCustNm")}}', datafield: 'dlv_cust_nm', width: 101 },
                        { text: '{{trans("modOrder.dlvAddr")}}', datafield: 'addr', width: 300 }
                    ]
                });
                $("#dlvPlanModal1").modal('show');
            }
        },      
        @endif
        @can('TransportationPlan_CompulsiveDelete')
        {
            btnId:"btnCompulsiveDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"強制刪除",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                var dlvNo = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    console.log(row);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                        dlvNo.push(row.dlv_no);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                swal({
                    title: "確認視窗",
                    text: "您確定要刪除嗎？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: "確定",
                    cancelButtonText: "取消"
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + "/TranPlanMgmt/delDlvPlan", {'ids': ids,'dlvNo': dlvNo}, function(data){
                            if(data.msg == "success") {
                                swal("刪除成功", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                            }
                            else{
                                swal("操作失敗", data.errorMsg, "error");
                            }
                        });
                    
                    } else {
                    
                    }
                });
                
            }
        },
        @endcan
        @can('errorchange')
        {
            btnId:"btnerrorchange",
            btnIcon:"fa fa-newspaper-o",
            btnText:"換車運送",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                if(ids.length > 1) {
                    swal("請至多選擇一筆資料", "", "warning");
                    return;
                }
                swal({
                    title: "確認視窗",
                    text: "您確定要換車嗎？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: "確定",
                    cancelButtonText: "取消"
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + '/TranPlanMgmt/error/changecar', {'ids': ids}, function(data){
                            if(data.msg == "success") {
                                swal("操作成功", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                                $("#jqxGrid").jqxGrid('clearselection');
                                $("#cssLoading").hide();
                            }
                            else{
                                swal("操作失敗", data.errorMsg, "error");
                                $("#cssLoading").hide();
                            }
                        });
                    
                    } else {
                    
                    }
                    $("#cssLoading").show();
                    // swal("資料處理中", "", "success");
                });
            }
        },    
        @endcan
    ];
    $("#allRemove").on("click", function(){
        var idx = $('#dlvPlanGrid').jqxGrid('getselectedrowindexes');
        if(idx.length ==  0) {
            swal("請選擇一筆資料", "", "warning");
            return;
        }

        var ids = [];
        var dlvTypes = [];
        var dlvNo = "";
        for(i in idx) {
            var rowData = $('#dlvPlanGrid').jqxGrid('getrowdata', idx[i]);
            ids.push(rowData.id);
            dlvNo = rowData.dlv_no;
        }

        $.post(BASE_URL + "/TranPlanMgmt/rmDlvPlan", {dlvNo: dlvNo, ids: ids}, function(data){
            if(data.msg == "success") {
                swal("移除完成", "", "success");
                $("#dlvPlanModal").modal('hide');
                $('#dlvPlanGrid').jqxGrid('updatebounddata');
                $('#jqxGrid').jqxGrid('updatebounddata');
                $("#jqxGrid").jqxGrid('clearselection');
            }
            else if(data.errorLog == "已完成訂單不能取消派車"){
                swal("已完成訂單不能取消派車", "", "error");
            }
            else {
                swal("操作發生問題", "", "error");
            }
        });
    
    });

    $("#calEta").on("click", function(){
        var idx = $('#jqxGrid').jqxGrid('getselectedrowindex');
        if(idx == -1) {
            swal("請選擇一筆資料", "", "warning");
            return;
        }
        var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx);

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();

        var yyyy = today.getFullYear();
        if (dd < 10) {
        dd = '0' + dd;
        } 
        if (mm < 10) {
        mm = '0' + mm;
        } 
        var today = yyyy + '-' + mm + '-' + dd + ' ' + h + ':' + m + ':' + s;

        $("#dTime").text(today);

        $.get(BASE_URL + "/TranPlanMgmt/getDlvEtaPlan/"+rowData.dlv_no, {}, function(data){
            if(data.msg == "success") {
                swal("計算完成", "", "success");
                $('#dlvPlanGrid1').jqxGrid('updatebounddata');
            }
            else {
                swal("操作發生問題", "", "error");
            }
        });

    });
</script>
@endsection


@include('backpack::template.search')

