@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	{{ trans('modDlv.dlvPlan') }}<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url(config('backpack.base.route_prefix'),'TranPlanMgmt/SendCar') }}">{{ trans('modDlv.titleName') }}</a></li>
		<li class="active">{{ trans('modDlv.dlvPlan') }}</li>
	</ol>
</section>
@endsection 
@section('content')
	@include('backpack::template.toolbar')
	<div class="row">
		
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4>{{ trans('backpack::crud.please_fix') }}</h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">{{ trans('modDlv.sendCarInfo') }}</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">

							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-4">
										<label for="dlv_no">{{ trans('modDlv.dlvNo') }}</label>
										<input type="text" class="form-control" id="dlv_no" name="dlv_no" switch="off">
									</div>
									<div class="form-group col-md-4">
										<label for="status">{{ trans('modDlv.status') }}</label>
										<select class="form-control" id="status" name="status">
                                            <option value="UNTREATED">{{ trans('modDlv.STATUS_UNTREATED') }}</option>
                                            <option value="SEND">{{ trans('modDlv.STATUS_SEND') }}</option>
                                            <option value="DLV">{{ trans('modDlv.STATUS_DLV') }}</option>
                                            <option value="ERROR">{{ trans('modDlv.STATUS_ERROR') }}</option>
                                            <option value="FINISHED">{{ trans('modDlv.STATUS_FINISHED') }}</option>
                                        </select>
									</div>
									<div class="form-group col-md-4">
										<label for="dlv_date">{{ trans('modDlv.dlvDate') }}</label>
										<input type="text" class="form-control" id="dlv_date" name="dlv_date">
									</div>
								</div>

                                <div class="row">
                                    <div class="form-group col-md-4">
										<label for="cust_no">{{ trans('modDlv.custNo') }}</label>
										<input type="text" class="form-control" id="cust_no" name="cust_no">
									</div>
                                    <div class="form-group col-md-4">
										<label for="car_no">{{ trans('modDlv.carNo') }}</label>
										<input type="text" class="form-control" id="car_no" name="car_no">
									</div>
                                    <div class="form-group col-md-4">
										<label for="car_type_nm">{{ trans('modDlv.carType') }}</label>
										<select class="form-control" id="car_type" name="car_type">
                                        </select>
									</div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
										<label for="driver_nm">{{ trans('modDlv.driverNm') }}</label>
										<input type="text" class="form-control" id="driver_nm" name="driver_nm">
									</div>
                                    <div class="form-group col-md-4">
										<label for="driver_phone">{{ trans('modDlv.driverPhone') }}</label>
										<input type="text" class="form-control" id="driver_phone" name="driver_phone">
									</div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
										<label for="load_rate">{{ trans('modDlv.loadRate') }}</label>
										<input type="text" class="form-control" id="load_rate" name="load_rate">
									</div>
                                    <div class="form-group col-md-4">
										<label for="load_tweight">{{ trans('modDlv.loadTweight') }}</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="load_tweight" name="load_tweight">
                                            <span class="input-group-addon">KGS</span>
                                        </div>
									</div>
                                    <div class="form-group col-md-4">
										<label for="load_tcbm">{{ trans('modDlv.loadTcbm') }}</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="load_tcbm" name="load_tcbm">
                                            <input type="hidden" class="form-control" id="load_tcbmu" name="load_tcbmu">
                                            <span class="input-group-addon">M3</span>
                                        </div>
										
									</div>
                                </div>


								@if(isset($id))
								<input type="hidden" name="id" value="{{$id}}" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								@endif

							</div>
						</div>

					</div>
					<!-- /.tab-content -->
				</div>
			</form>
			<div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
					<li class="active"><a href="#tab_5" data-toggle="tab" aria-expanded="false">{{ trans('modDlv.dlvDetail') }}</a></li>
                    {{--  <li><a href="#tab_3" data-toggle="tab" aria-expanded="false">{{ trans('modDlv.loadingPlan') }}</a></li>
                    <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">{{ trans('modDlv.loadingDetails') }}</a></li>  --}}
                </ul>
                <div class="tab-content">
					<div class="tab-pane active" id="tab_5">
						<div id="dlvDetailGrid"></div>
					</div>
                    <div class="tab-pane" id="tab_3">
						<button type="button" class="btn btn-primary btn-sm" id="ordLookup" btnName="ordLookup" info1="{{Crypt::encrypt('mod_order')}}" info2="{{Crypt::encrypt('ord_no,ord_no,truck_cmp_no,truck_cmp_nm')}}" info3="{{Crypt::encrypt('')}}" info4="" selectionmode="checkbox" triggerfunc="ordCallBackFunc">{{ trans('modDlv.chooseOrder') }}</button>
						<button type="button" class="btn btn-primary btn-sm" id="handBtn">{{ trans('modDlv.manualFilling') }}</button>
						<button type="button" class="btn btn-primary btn-sm" id="autoBtn">{{ trans('modDlv.autoFilling') }}</button>
                        <div id="packGrid"></div>
                    </div>
					<div class="tab-pane" id="tab_4">
                        <div id="carLoadGrid"></div>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
		</div>	
	</div>




@endsection 
@include('backpack::template.lookup') 
@section('after_scripts')
	<script>
	function getCarType(carType) {
        $.get(BASE_URL+'/getCarType', {'carType': carType}, function(data){
            var str = "";

            for(i in data) {
                str += '<option value="'+data[i].cd+'">'+data[i].cd_descp+'</option>';
            }

            $("#car_type").html(str);
        }, "JSON");
	}
	
	function carNoCallBack(data) {
		getCarType(data.car_type);
	}
		var mainId = "";
		var dlv_no = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar') }}";

		var fieldData = null;
		var fieldObj = null;

		@if(isset($crud -> create_fields))
		fieldData = '{{!! json_encode($crud->create_fields) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		@endif



		@if(isset($id))
		mainId = "{{$id}}";
		editData = '{{!! $entry !!}}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
		editObj = JSON.parse(editData);
		dlv_no = editObj.dlv_no;
		@endif




		$(function () {

			$( "#lookupEvent" ).on( "ordCallBackFunc", function(event,indexs) {
				console.log(indexs);
				var ordArray = [];
				$.each(indexs,function(k,v){
					var data = $('#lookupGrid').jqxGrid('getrowdata', indexs[k]);
					var ordNo = data.ord_no;

					ordArray.push(ordNo);
				});

				$.ajax({
					url: "{{ url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar') }}" + '/getPack',
					type: 'POST',
					async: false,
					cache: false,
					data: {ordArray: ordArray},
					beforeSend: function () {

					},
					error: function (jqXHR, exception) {

					},
					success: function (data) {

						var source = {
										datatype: "json",
										localdata: data.Rows
									};
						var dataAdapter = new $.jqx.dataAdapter(source);
						$('#packGrid').jqxGrid({source: dataAdapter});

						$("#packGrid").jqxGrid('selectallrows');
					}
					
				});
			});

			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar') }}";
			formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_dlv') }}/" + mainId;
			formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar') }}";

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterInit = function() {
				@cannot('TransPlanControl')
				menuBtnFunc.disabled(['iSave', 'iCancel','iAdd', 'iEdit', 'iCopy', 'iDel']);
				@endcan
			}

			formOpt.afterDel = function() {
				$('#ordGrid').jqxGrid('clear');
			}

			formOpt.editFunc = function() {
				var status = $("#status").val();

				if(status != "UNTREATED") {
					$("#car_no").prop("disabled", true);
					$("#dlv_date").prop("disabled", true);
					$("#cust_no").prop("disabled", true);
					$("#car_type").prop("disabled", true);
					$("button[btnname='cust_no']").prop("disabled", true);
					$("button[btnname='car_no']").prop("disabled", true);
				}
			}

			var btnGroup = [];

			initBtn(btnGroup);

			$('#ordLookup').on('click', function(){
				$('#lookupModal').modal('show');
				initLookup('ordLookup', "{{ trans('modDlv.ordQuery') }}");
			});

			
		});


        setField.disabled("mainForm", ["load_rate", "load_tweight", "load_tcbm", "status"]);
	

        //#region 裝填結果
		// $.ajax({
		// 	url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_car_load') }}",
		// 	type: 'GET',
		// 	async: false,
		// 	beforeSend: function () {
		// 	},
		// 	error: function (jqXHR, exception) {

		// 	},
		// 	success: function (fieldData) {
		// 		console.log(fieldData);
		// 		var col = [
		// 			[
		// 				{name: "id", type: "number"},
		// 				{name: "dlv_no", type: "string"},
		// 				{name: "ord_no", type: "string"},
		// 				{name: "pack_no", type: "string"},
		// 			],
		// 			[
		// 				{text: "{{ trans('modCarLoad.id') }}", datafield: "id", width: 100, dbwidth: "10", nullable: false},
		// 				{text: "{{ trans('modCarLoad.dlvNo') }}", datafield: "dlv_no", width: 150, dbwidth: "10", nullable: false},
		// 				{text: "{{ trans('modCarLoad.ordNo') }}", datafield: "ord_no", width: 150, dbwidth: "10", nullable: false},
		// 				{text: "{{ trans('modCarLoad.packNo') }}", datafield: "pack_no", width: 150, dbwidth: "10", nullable: false},
		// 			]
		// 		];
		// 		var opt = {};
		// 		opt.gridId = "carLoadGrid";
		// 		opt.fieldData = col;
		// 		opt.formId = "sub2Form";
		// 		opt.saveId = "carLoadSave";
		// 		opt.cancelId = "carLoadCancel";
		// 		opt.showBoxId = "sub2Box";
		// 		opt.height = 300;
		// 		opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/carLoad/get') }}" + '/' + dlv_no;
		// 		opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderPack') }}" + "/store";
		// 		opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderPack') }}" + "/update";
		// 		opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/orderPack') }}" + "/delete/";
		// 		opt.showaggregates = false;
		// 		opt.commonBtn = false;
		// 		opt.inlineEdit = false;
		// 		opt.showtoolbar = false;
		// 		opt.defaultKey = {};
		// 		opt.beforeSave = function (formData) {

		// 		}

		// 		opt.afterSave = function (data) {

		// 		}

		// 		genDetailGrid(opt);
		// 	},
		// 	cache: false,
		// 	contentType: false,
		// 	processData: false
		// });
		//#endregion

		//#region 裝填結果
		var dlvTypeCode = [
			{value: "P", label: "提貨"},
			{value: "D", label: "配送"},
		]

		var dlvSource =
		{
				datatype: "array",
				datafields: [
					{ name: 'label', type: 'string' },
					{ name: 'value', type: 'string' }
				],
				localdata: dlvTypeCode
		};

		var dlvAdapter = new $.jqx.dataAdapter(dlvSource, {
			autoBind: true
		});

		//派車明細
		// $.ajax({
		// 	url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_dlv_plan') }}",
		// 	type: 'GET',
		// 	async: false,
		// 	beforeSend: function () {
		// 	},
		// 	error: function (jqXHR, exception) {

		// 	},
		// 	success: function (fieldData) {
				
		// 	},
		// 	cache: false,
		// 	contentType: false,
		// 	processData: false
		// });

		var statusCode = [
			{value: "UNTREATED", label: "{{ trans('modDlvPlan.STATUS_UNTREATED') }}"},
			{value: "SEND", label: "{{ trans('modDlvPlan.STATUS_SEND') }}"},
			{value: "DLV", label: "{{ trans('modDlvPlan.STATUS_DLV') }}"},
			{value: "FINISHED", label: "{{ trans('modDlvPlan.STATUS_FINISHED') }}"},
			{value: "ERROR", label: "{{ trans('modDlvPlan.STATUS_ERROR') }}"},
			{value: "REJECT", label: "{{ trans('modDlvPlan.STATUS_REJECT') }}"},
		];

		var statusSource =
		{
				datatype: "array",
				datafields: [
					{ name: 'label', type: 'string' },
					{ name: 'value', type: 'string' }
				],
				localdata: statusCode
		};

		var statusAdapter = new $.jqx.dataAdapter(statusSource, {
			autoBind: true
		});

		var col = [
			[
				{ name: 'id', type: 'number' },
				{ name: 'dlv_type', type: 'string', values: { source: dlvAdapter.records, value: 'value', name: 'label' } },
				{ name: 'status', type: 'string', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
				{ name: 'dlv_no', type: 'string' },
				{ name: 'ord_id', type: 'number' },
				//{ name: 'status', type: 'string', value: 'status' },
				{ name: 'etd', type: 'date', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
				{ name: 'sys_ord_no', type: 'string' },
				{ name: 'ord_no', type: 'string' },
				//{ name: 'pick_cust_nm', type: 'string' },
				//{ name: 'dlv_cust_nm', type: 'string' },
				{ name: 'cust_nm', type: 'string' },
				{ name: 'addr', type: 'string' },
				{ name: 'is_urgent', type: 'string' },
				{ name: 'finish_date', type: 'date',cellsformat: 'yyyy-MM-dd HH:mm:ss', filtertype: 'date' }               
			],
			[
				{ text: '{{trans("modOrder.etd")}}', datafield: 'etd', width: 120, cellsformat: 'yyyy-MM-dd',filtertype: 'date' },
				{ text: 'id', datafield: 'id', width: 50, hidden: true },
				{ text: '{{trans("modDlv.dlvType")}}', datafield: 'dlv_type', width: 100, hidden: false },
				{ text: '{{trans("modDlv.status")}}', datafield: 'status', width: 100, hidden: false },
				{ text: 'dlvNo', datafield: 'dlv_no', width: 50, hidden: true },
				{ text: 'ord_id', datafield: 'ord_id', width: 50, hidden: true },
				//{ text: '{{trans("modDlv.status")}}', datafield: 'status', width: 110},
				{ text: '{{trans("modOrder.sysOrdNo")}}', datafield: 'sys_ord_no', width: 120 },
				{ text: '{{trans("modOrder.ordNo")}}', datafield: 'ord_no', width: 120 },
				//{ text: '{{trans("modOrder.pickCustNm")}}', datafield: 'pick_cust_nm', width: 150 },
				//{ text: '{{trans("modOrder.dlvCustNm")}}', datafield: 'dlv_cust_nm', width: 150 },
				{ text: '{{trans("modDlvPlan.custNm")}}', datafield: 'cust_nm', width: 150 },
				{ text: '{{trans("modOrder.dlvAddr")}}', datafield: 'addr', width: 300 },
				{ text: '{{trans("modOrder.isUrgent")}}', datafield: 'is_urgent', width: 300 },
				{ text: '{{trans("modOrder.finish_date")}}', datafield: 'finish_date', width: 200, cellsformat: 'yyyy-MM-dd HH:mm:ss',filtertype: 'date'}
			]
		];
		var opt = {};
		opt.gridId                 = "dlvDetailGrid";
		opt.fieldData              = col;
		opt.formId                 = "sub2Form";
		opt.saveId                 = "dlvDetailSave";
		opt.cancelId               = "dlvDetailCancel";
		opt.showBoxId              = "sub3Box";
		opt.height                 = 300;
		opt.getUrl                 = BASE_URL + '/TranPlanMgmt/getDlvPlan/' +  dlv_no;
		opt.addUrl                 = "";
		opt.updateUrl              = "";
		opt.delUrl                 = "";
		opt.showaggregates         = false;
		opt.commonBtn              = false;
		opt.inlineEdit             = false;
		opt.showtoolbar            = false;
		opt.selectionmode          = 'multiplecellsadvanced';
		opt.enablebrowserselection = false;
		opt.defaultKey = {};
		opt.beforeSave = function (formData) {

		}

		opt.afterSave = function (data) {

		}

		genDetailGrid(opt);
		//#endregion


		$("#handBtn").on("click", function(){
			var selIndex = $("#packGrid").jqxGrid('getselectedrowindexes');
			var rowData = [];
			for(var i=0; i<selIndex.length; i++) {
				var data = $("#packGrid").jqxGrid('getrowdata', selIndex[i]);
				data["dlv_no"] = dlv_no;
				rowData.push(data);
			}

			var carNo = $("#car_no").val();

			if(carNo == "") {
				notifyMsg("{{ trans('modDlv.msg5') }}", "{{ trans('modDlv.msg6') }}", 'warning');
				return;
			}

			$.ajax({
				url: "{{ url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar') }}" + '/handPickup',
				type: 'POST',
				async: false,
				cache: false,
				data: {data: JSON.stringify(rowData)},
				beforeSend: function () {

				},
				error: function (jqXHR, exception) {

				},
				success: function (data) {
					if(data.msg == "success") {
						notifyMsg("{{ trans('modDlv.msg7') }}", "{{ trans('modDlv.msg8') }}", 'success');
					}
					else {
						notifyMsg("{{ trans('modDlv.msg5') }}", "{{ trans('modDlv.msg9') }}", 'warning');
					}
					$('#carLoadGrid').jqxGrid('updatebounddata');
				}
				
			});
		});

		$("#autoBtn").on("click", function(){
			var rowData = $("#packGrid").jqxGrid('getrows');

			var carNo = $("#car_no").val();

			if(carNo == "") {
				//notifyMsg('警告訊息', '請先輸入車號', 'warning');
				//return;
			}

			$.ajax({
				url: "{{ url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar') }}" + '/autoPickup',
				type: 'POST',
				async: false,
				cache: false,
				data: {data: JSON.stringify(rowData), dlv_no: dlv_no},
				beforeSend: function () {

				},
				error: function (jqXHR, exception) {

				},
				success: function (data) {
					if(data.msg == "success") {
						notifyMsg("{{ trans('modDlv.msg7') }}", "{{ trans('modDlv.msg8') }}", 'success');

						var source = {
										datatype: "json",
										localdata: data.data.Rows
									};
						var dataAdapter = new $.jqx.dataAdapter(source);
						//$('#packGrid').jqxGrid('clear');
						$('#packGrid').jqxGrid({source: dataAdapter});
					}
					else {
						notifyMsg("{{ trans('modDlv.msg5') }}", "{{ trans('modDlv.msg9') }}", 'warning');
					}
					$('#carLoadGrid').jqxGrid('updatebounddata');
				}
				
			});
		});
	</script>
@endsection
