@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      {{ trans('tracking.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="{{ url(config('backpack.base.route_prefix'),'tracing') }}">{{ trans('tracking.titleName') }}</a></li>
      </ol>
    </section>
@endsection
    <style>
      #map_canvas {
          height: 300px;
          width: 100%
      }
      
      #map_canvas img {
          max-width: none;
      }
      
      #map_canvas div {
          -webkit-transform: translate3d(0, 0, 0);
      }
    </style>
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
<div class="row">
    <div class="col-sm-5">
        <div class="box box-warning direct-chat direct-chat-warning" id="searchBox">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('tracking.inputOrdNo') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form role="form" id="searchForm">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="ord_no">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="ord_no">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="ord_no">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="ord_no">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="ord_no">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="col-sm-6 col-sm-offset-3" style="text-align:center">
                    <button type="button" class="btn btn-default" id="searchBtn">{{ trans('common.search') }}</button>
                    <button type="button" class="btn btn-danger" id="clearBtn">{{ trans('common.clear') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-7" id="timeLineBox">
        <div class="box box-warning direct-chat direct-chat-warning" >
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('tracking.ordNo') }}：</h3>
                <select class="input-sm" name="" id="ord_no">
                </select>
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="timeLineBody" style="padding:5px">
                
            </div>
            {{--  <div class="box-footer">
                <div class="col-sm-6 col-sm-offset-3" style="text-align:center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="BackBtn">{{ trans('common.back') }}</button>
                </div>
            </div>  --}}
        </div>
    </div>
</div>



@endsection

@section('after_scripts')
<script src="{{ asset('vendor/adminlte/plugins/chartjs') }}/Chart.js"></script>
<script>
    $(function(){

        $("#searchBtn").click(function(){
            /*
            $("#searchBox").removeClass('animated slideInLeft');
            $("#timeLineBox").removeClass('animated slideOutRight');
            */
            var opt = "";
            var objForm = $("#searchForm").serializeArray();
            var ord_no = [];

            $.each(objForm, function(i, v){
                if(objForm[i]["value"] != "") {
                    opt += "<option value='"+objForm[i]["value"]+"'>"+objForm[i]["value"]+"</option>";
                    ord_no.push(objForm[i]["value"]);
                }
                
            });

            $("#ord_no").html(opt);
            getTracking(ord_no[0]);

            /*
            $("#searchBox").addClass('animated slideOutLeft');
            $("#searchBox").hide();
            $("#timeLineBox").show();
            $("#timeLineBox").addClass('animated slideInRight');
            */
        });

        $("#BackBtn").click(function(){
            /*
            $("#searchBox").removeClass('animated slideOutLeft');
            $("#timeLineBox").removeClass('animated slideInRight');

            $("#timeLineBox").addClass('animated slideOutRight');
            $("#timeLineBox").hide();
            $("#searchBox").show();
            $("#searchBox").addClass('animated slideInLeft');
            */
            
        });

        $("#ord_no").on("change", function(){
            var ord_no = $(this).val();
            getTracking(ord_no);
        });

        $("#clearBtn").on("click", function(){
            $("input[name='ord_no']").val('');
            $("#timeLineBody").html('');
            $("#ord_no").html('');
        });


        function getTracking(ord_no) {
            $.getJSON("{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/trackingApi/get') }}" + "/"+ ord_no, function( data ) {
                if(data.msg == "success") {
                    var trackingData = data.trackingData;
                    if(trackingData.length !=　0) {
                        var timeLine = "";
                        var $ul = $('<ul class="timeline"></ul>');
                        var icon = "";
                        $.each(trackingData, function(i, v){
                            var detailData = trackingData[i];
                            for(var j=0; j<detailData.length; j++) {
                                if(j == 0) {
                                    var date = detailData[j]["date"].substring(0, 10);
                                    var timeStr = '<li class="time-label">\
                                                        <span class="bg-red">\
                                                            '+date+'\
                                                        </span>\
                                                    </li>';
                                    $ul.append(timeStr);
                                }
                                if(detailData[j]["status"] == "E") {
                                    icon = "fa-flag bg-green";
                                }
                                else {
                                    icon = "fa-clock-o bg-aqua";
                                }
                                var time = detailData[j]["date"].substring(10, detailData[j]["date"].length);
                                var str = '<li>\
                                    <i class="fa '+icon+'"></i>\
                                    <div class="timeline-item">\
                                        <span class="time"><i class="fa fa-clock-o"></i> '+time+'</span>\
                                        <h3 class="timeline-header"><a href="#" timeline-widget="collapse">'+detailData[j]["title"]+'</a></h3>\
                                        <div class="timeline-body">\
                                            '+detailData[j]["descp"]+'\
                                        </div>\
                                    </div>\
                                </li>';
                                
                                $ul.append(str);
                            }
                        });
                        var end = '<li> <i class="fa fa-flag bg-gray"></i></li>';
                        $ul.append(end);

                        $("#timeLineBody").html('');
                        $("#timeLineBody").append($ul);
                    }
                    else {
                        $("#timeLineBody").html('');
                        swal("{{ trans('tracking.msg1') }}", "{{ trans('tracking.msg2') }}", "warning");
                    }

                    
                }
                
            });
        }
    });
</script>
@endsection