@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
		基本健檔
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ url(config('backpack.base.route_prefix'),'bscodeKind') }}">{{ trans('bscodeKind.titleName') }}</a>
		</li>
		<li class="active">基本健檔</li>
	</ol>
</section>
@endsection 
@section('content') 
@include('backpack::template.toolbar')
<div class="row">

	<div class="col-md-12">
		<div class="callout callout-danger" id="errorMsg" style="display:none">
			<h4>{{ trans('backpack::crud.please_fix') }}</h4>
			<ul>

			</ul>
		</div>
		<form method="POST" accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
			<input type="hidden" name="g_key" class="form-control">
			<input type="hidden" name="c_key" class="form-control">
			<input type="hidden" name="s_key" class="form-control">
			<input type="hidden" name="d_key" class="form-control">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#tab_1" data-toggle="tab" aria-expanded="true">{{ trans('bscodeKind.baseInfo') }}</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1">
						<form role="form">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-4">
										<label for="cd_type">{{ trans('bscodeKind.cdType') }}</label>
										<input type="text" class="form-control" id="cd_type" name="cd_type" placeholder="Enter No.">
									</div>
									<div class="form-group col-md-4">
										<label for="cd_descp">{{ trans('bscodeKind.cdDescp') }}</label>
										<input type="text" class="form-control" id="cd_descp" name="cd_descp" placeholder="Enter Description">
                                    </div>
                                    @if(Auth::user()->hasRole('Admin'))
                                    <div class="form-group col-md-4">
                                        <label for="adminonly">顯示</label>
                                        <select class="form-control" id="adminonly" name="adminonly">
                                            <option value="Y">是</option>
                                            <option value="N">否</option>
                                        </select>
                                    </div>
                                    @endif
								</div>

								<div class="row">
									<div class="form-group col-md-3">
										<label for="created_by">{{ trans('bscodeKind.createdBy') }}</label>
										<input type="text" class="form-control" id="created_by" name="created_by">
									</div>
									<div class="form-group col-md-3">
										<label for="created_at">{{ trans('bscodeKind.createdAt') }}</label>
										<input type="text" class="form-control" id="created_at" name="created_at">
									</div>
									<div class="form-group col-md-3">
										<label for="updated_by">{{ trans('bscodeKind.updatedBy') }}</label>
										<input type="text" class="form-control" id="updated_by" name="updated_by">
									</div>
									<div class="form-group col-md-3">
										<label for="updated_at">{{ trans('bscodeKind.updatedAt') }}</label>
										<input type="text" class="form-control" id="updated_at" name="updated_at">
									</div>
								</div>


								@if(isset($id))
								<input type="hidden" name="id" value="{{$id}}" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> @endif

							</div>
						</form>
					</div>
					<!-- /.tab-pane -->

				</div>
				<!-- /.tab-content -->
			</div>
        </form>
	</div>
	
</div>

<div class="row">

	<div class="col-md-12">
        <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_2" data-toggle="tab" aria-expanded="false">{{ trans('bscodeKind.detail') }}</a>
                </li>
            </ul>
            <div class="tab-content">
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="tab_2">
                    <div class="box box-primary" id="subBox" style="display:none">
                        <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('bscode.titleName') }}</h3>
                        </div>
                        <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="cd">{{ trans('bscode.cd') }}</label>
                                    <input type="text" class="form-control input-sm" name="cd" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cd_descp">{{ trans('bscode.cdDescp') }}</label>
                                    <input type="text" class="form-control input-sm" name="cd_descp" grid="true" >
                                </div>                                    
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="value1">{{ trans('bscode.value1') }}</label>
                                    <input type="text" class="form-control input-sm" name="value1" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="value2">{{ trans('bscode.value2') }}</label>
                                    <input type="text" class="form-control input-sm" name="value2" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="value3">{{ trans('bscode.value3') }}</label>
                                    <input type="text" class="form-control input-sm" name="value3" grid="true" >
                                </div>                                      
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                            <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                            <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                        </div>
                        </button>
                    </div>
                    
                    <div id="jqxGrid"></div>
                </div>
                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
	</div>
	
</div>


@endsection 
@include('backpack::template.lookup') 
@section('after_scripts')

<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>


<script>
	var mainId = "";
    var cd_type = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
    @endif

    
    
    @if(isset($id))
        mainId = "{{$id}}";
        editData = '{{!! $entry !!}}';
        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        editObj = JSON.parse(editData);
        cd_type= editObj.cd_type;
        console.log(editObj);
    @endif

    
   

    $(function(){

        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind') }}";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/bscode_kind') }}/" + mainId;
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind') }}";
        formOpt.addFunc = function(){
            $('#jqxGrid').jqxGrid('clear');
            $("#subPanel").hide();
        };
        formOpt.initFieldCustomFunc = function (){
            $("select[name='state[]']").select2({tags: true});
        };
        formOpt.afterDel = function() {
            $('#jqxGrid').jqxGrid('clear');
            $('#packDetailGrid').jqxGrid('clear');
        }

        formOpt.editFunc = function() {
            $("#subPanel").show();
        }

        formOpt.copyFunc = function() {
            $("#subPanel").hide();
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

        var col = [
                [
                    {name: "id", type: "number"},
                    {name: "cd_type", type: "string"},
                    {name: "cd", type: "string"},
                    {name: "cd_descp", type: "string"},
                    {name: "value1", type: "string"},
                    {name: "value2", type: "string"},
                    {name: "value3", type: "string"}
                ],
                [
                    {text: "{{ trans('bscode.id') }}", datafield: "id", width: 100, hidden: true},
                    {text: "{{ trans('bscode.cd_type') }}", datafield: "cd_type", width: 130, hidden: true},
                    {text: "{{ trans('bscode.cd') }}", datafield: "cd", width: 150},
                    {text: "{{ trans('bscode.cdDescp') }}", datafield: "cd_descp", width: 150},
                    {text: "{{ trans('bscode.value1') }}", datafield: "value1", width: 150},
                    {text: "{{ trans('bscode.value2') }}", datafield: "value2", width: 150},
                    {text: "{{ trans('bscode.value3') }}", datafield: "value3", width: 150}
                ]
            ];
            var opt = {};
            opt.gridId = "jqxGrid";
            opt.fieldData = col;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/bscode') }}" + '/' + cd_type;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/bscode') }}" + "/store";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/bscode') }}" + "/update/"+ cd_type;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/bscode') }}" + "/delete/";
            opt.defaultKey = {'cd_type': cd_type};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                
            }

            genDetailGrid(opt);
    })

</script>

@endsection