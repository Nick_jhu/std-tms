@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        運費表設定<small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix'),'shipProfile') }}">運費表設定</a></li>
        <li class="active">{{ trans('modCar.titleAddName') }}</li>
      </ol>
    </section>

@endsection
<style>
.select2-dropdown.increasedzindexclass {
  z-index: 999999;
}
.select2-dropdown {
   z-index:999999 ;
}
.jqx-grid-column-header{
    z-index:0!important;
  }
</style>
@section('content')
@include('backpack::template.toolbar')
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">運費表設定</a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                  <div class="form-group col-md-3">
                                    <label for="truck_cmp_nm">承運廠商</label>
                                    <input type="hidden" class="form-control" id="truck_cmp_no" name="truck_cmp_no">
                                    <input type="text" class="form-control" id="truck_cmp_nm" name="truck_cmp_nm">
                                  </div>
                                  <div class="form-group col-md-3">
                                    <input type="text" class="form-control" id="pick_cust_no" style="display:none" name="pick_cust_no">
                                    <label for="pick_cust_nm">起運點</label>
                                    <input type="text" class="form-control" id="pick_cust_nm" name="pick_cust_nm" required="required">
                                  </div>
                                  <div class="form-group col-md-3">
                                    <input type="text" class="form-control" id="dlv_cust_no" style="display:none" name="dlv_cust_no">
                                    <label for="dlv_cust_nm" >到著點</label>
                                    <input type="text" class="form-control" id="dlv_cust_nm" name="dlv_cust_nm" required="required">
                                  </div>
                                  <div class="form-group col-md-3">
                                    <label for="temperate">{{ trans('modOrder.temperate') }}</label>
                                    <select class="form-control" id="temperate" name="temperate" no-clear="Y">    
                                    </select>                                      
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="form-group col-md-3">
                                    <label for="car_type_floor">車輛噸級(上限)</label>
                                    <input type="number"  min="0" class="form-control" id="car_type_floor" name="car_type_floor">
                                  </div>
                                  <div class="form-group col-md-3">
                                    <label for="car_type_ceiling">車輛噸級(下限)</label>
                                    <input type="number" min="0" class="form-control" id="car_type_ceiling" name="car_type_ceiling">
                                  </div>
                                  <div class="form-group col-md-3">
                                    <label for="charge_type">計費方式</label>
                                    <select class="form-control" id="charge_type" name="charge_type" no-clear="Y">    
                                    </select>  
                                  </div>
                                  <div class="form-group col-md-3">
                                    <label for="trip_type">趟別</label>
                                    <select class="form-control" id="trip_type" name="trip_type" no-clear="Y">    
                                    </select>  
                                  </div>
                                </div>

                                 <div class="row">
                                  <div class="form-group col-md-3">
                                    <label for="fee">費用</label>
                                    <input type="number"  min="0" class="form-control" id="fee" name="fee">
                                  </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        {{-- <label for="created_by">{{ trans('modCar.createdBy') }}</label> --}}
                                        <input type="hidden" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{-- <label for="created_at">{{ trans('modCar.createdAt') }}</label> --}}
                                        <input type="hidden" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{-- <label for="updated_by">{{ trans('modCar.updatedBy') }}</label> --}}
                                        <input type="hidden" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{-- <label for="updated_at">{{ trans('modCar.updatedAt') }}</label> --}}
                                        <input type="hidden" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->                    
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>    



    <div class="row">

      <div class="col-md-12">
            <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_2" data-toggle="tab" aria-expanded="false">不收費中繼</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- /.tab-pane -->
                    <div class="tab-pane active" id="tab_2">
                        <div class="box box-primary" id="subBox" style="display:none">
                            <div class="box-header with-border">
                            <h3 class="box-title">不收費中繼</h3>
                            </div>
                            <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data" onsubmit="return false;">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <label for="relay_cust_nm">中繼點名稱</label>
                                        <div class="input-group input-group-sm">
                                          <input type="hidden" class="form-control" name="relay_cust_no" grid="true">
                                          <input type="text" class="form-control" name="relay_cust_nm" grid="true" >
                                          <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-flat lookup" btnname="relay_cust_nm"
                                              info1="{{Crypt::encrypt('mod_station_view')}}" 
                                              info2="{{Crypt::encrypt('cust_no+cname,cust_no,cname,area_type')}}" 
                                              info3="{{Crypt::encrypt('')}}"
                                              info4="" triggerfunc="" selectionmode="singlerow">
                                              <i class="fa fa-search" aria-hidden="true"></i>
                                            </button>
                                          </span> 
                                        </div> 
                                    </div>
                                    {{-- <div class="form-group col-md-2">
                                      <label for="relay_cust_nm">中繼點代碼</label>
                                      <input type="text" class="form-control" name="relay_cust_no" grid="true" disabled="true" >
                                    </div>                                     --}}
                                </div>
                            </div>
                            <!-- /.box-body -->
    
                            <div class="box-footer">
                                <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                                <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                                <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                            </div>
                          </form>
                        </div>
                        
                        <div id="jqxGrid"></div>
                    </div>
                    <!-- /.tab-pane -->
    
                </div>
                <!-- /.tab-content -->
            </div>
      </div>
      
    </div>

@endsection

@include('backpack::template.lookup')


@section('after_scripts')


<script>
    function dlvCallBack(data) {
        document.getElementById("dlv_cust_nm").style.backgroundColor = "";
    }
    function pickCallBack(data) {
        document.getElementById("pick_cust_nm").style.backgroundColor = "";
    }
    @if(!Auth::user()->hasRole('Admin'))
    $("#iAdd").remove();
    $("#iDel").remove();
    @endif
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var pickname ="";
    var dlvname = "";
    var truckname = "";
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/shipProfile') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
    @endif

    
    
    @if(isset($id))
        mainId = "{{$id}}";
        editData = '{{!! $entry !!}}';
        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        editObj = JSON.parse(editData);
        cust_no= editObj.cust_no;
        console.log(editObj);
    @endif

    @if(isset($pickdata))
      pickname = "{{$pickdata}}";
    @endif
    
    @if(isset($dlvdata))
      dlvname = "{{$dlvdata}}";
    @endif
    @if(isset($truckdata))
      truckname = "{{$truckdata}}";
		@endif
    $(function(){
          $('#subBox button[btnName="relay_cust_nm"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('relay_cust_nm', "車商", callBackFunc=function(data){
              $("#subBox input[name='relay_cust_no']").val(data.cust_no);
					    $("#subBox input[name='relay_cust_nm']").val(data.cname);
            });
          });
      
          $('#subBox input[name="relay_cust_nm"]').on('click', function(){
          var check = $('#subBox input[name="relay_cust_nm"]').data('ui-autocomplete') != undefined;
          if(check == false) {
            initAutocomplete("subForm","relay_cust_nm",callBackFunc=function(data){
              $("#subBox input[name='relay_cust_no']").val(data.cust_no);
					    $("#subBox input[name='relay_cust_nm']").val(data.cname);
            },"relay_cust_nm");
          }
        });

        $( "#lookupEvent" ).on( "custContactFunc", function(event,indexs) {
            /*$.each(indexs,function(k,v){
                alert(k+v);
            });*/
            //alert(1);
        });

        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/shipProfile') }}";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_shippingfee') }}/" + mainId;
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/shipProfile') }}";
        formOpt.afterInit = function() {

        }
        formOpt.saveErrorFunc = function(data) {
          if(data.msg!="success"){
            if(data.errorLog!=""){
              swal(data.errorLog, "", "warning");
            }
          }
        } 
        formOpt.initFieldCustomFunc = function (){
            $.get( formOpt.fieldsUrl , function( data ) {
                if(typeof formOpt.afterInit === "function") {
					formOpt.afterInit();
          console.log(data);
          $('#pick_cust_nm').val(pickname);
          $('#dlv_cust_nm').val(dlvname);
          $('#truck_cmp_nm').val(truckname);
				}
				// $('#jqxGrid').jqxGrid('autoresizecolumns');
				// $('#jqxGrid2').jqxGrid('autoresizecolumns');
            });
            
        };
        formOpt.beforesaveFunc = function (){
            if( $('#pick_cust_nm').val() == "" || $('#dlv_cust_nm').val() == ""){
              if($('#pick_cust_nm').val() == ""){
                document.getElementById("pick_cust_nm").style.backgroundColor = "FCE8E6";
              }
              if($('#dlv_cust_nm').val() == ""){
                document.getElementById("dlv_cust_nm").style.backgroundColor = "FCE8E6";
              }
                swal("警告", "請輸入必填欄位", "warning");
                return false;
            }
            return true;
        }
        $("#pick_cust_nm").on("change", function(){
            document.getElementById("pick_cust_nm").style.backgroundColor = "";
        });
        $("#dlv_cust_nm").on("change", function(){
            document.getElementById("dlv_cust_nm").style.backgroundColor = "";
        });
        @if(isset($id))
        var col = [
                [
                    {name: "id", type: "number"},
                    {name: "shipfee_id", type: "string"},
                    {name: "relay_cust_no", type: "string"},
                    {name: "relay_cust_nm", type: "string"}
                ],
                [
                    {text: "{{ trans('bscode.id') }}", datafield: "id", width: 100, hidden: true},
                    {text: "{{ trans('bscode.shipfee_id') }}", datafield: "shipfee_id", width: 130, hidden: true},
                    {text: "中繼點代碼", datafield: "relay_cust_no", nullable: false, width: 150, editable: false},
                    {text: "中繼點名稱", datafield: "relay_cust_nm", nullable: false, width: 150}
                ]
            ];
            var opt = {};
            opt.gridId = "jqxGrid";
            opt.fieldData = col;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/shipProfile') }}" + '/get/' + mainId;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/shipProfile') }}" + "/detail/store";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/shipProfile') }}" + "/detailupdate/"+ mainId;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/shipProfile') }}" + "/delete/";
            opt.defaultKey = {'shipfee_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
                if(data.msg!="success"){
                  swal(data.log, "", "warning");
                }
            }
            

            genDetailGrid(opt);
            @endif
    })
</script>    


@endsection
