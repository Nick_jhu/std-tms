@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        司機設定<small></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">司機設定</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.pageId        = "modCar";
gridOpt.enabledStatus = false;
gridOpt.searchOpt     = true;
gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_driver_favorite') }}";
gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/sys_driver_favorite') }}";
gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/drivercontrol/create') }}";
gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/drivercontrol') }}" + "/{id}/edit";
gridOpt.height = 800;

var btnGroup = [
  {
    btnId: "btnSearchWindow",
    btnIcon: "fa fa-search",
    btnText: "{{ trans('common.search') }}",
    btnFunc: function () {
        $('#searchWindow').jqxWindow('open');
    }
  },
  {
    btnId: "btnExportExcel",
    btnIcon: "fa fa-cloud-download",
    btnText: "{{ trans('common.exportExcel') }}",
    btnFunc: function () {
      $("#jqxGrid").jqxGrid('exportdata', 'xls', '{{ trans("modCar.titleName") }}');
    }
  },
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "{{ trans('common.gridOption') }}",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  // {
  //   btnId:"btnAdd",
  //   btnIcon:"fa fa-edit",
  //   btnText:"{{ trans('common.add') }}",
  //   btnFunc:function(){
  //     location.href= gridOpt.createUrl;
  //   }
  // }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"{{ trans('common.delete') }}",
    btnFunc:function(){
        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        var ids = new Array();
        for (var m = 0; m < rows.length; m++) {
        var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
        if(typeof row != "undefined") {
            ids.push(row.id);
        }
        }
        if(ids.length == 0) {
          swal("請至少選擇一筆資料", "", "warning");
          return;
        }
        swal({
            title: "確認視窗",
            text: "您確定要刪除嗎？",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#dc3545',
            cancelButtonColor: '#6c757d',
            confirmButtonText: "確定",
            cancelButtonText: "取消"
        }).then((result) => {
            if (result.value) {
              $.post(BASE_URL + '/drivercontrol/multi/del', {'ids': ids}, function(data){
                if(data.msg == "success") {
                    swal("刪除成功", "", "success");
                    $("#jqxGrid").jqxGrid('updatebounddata');
                }
                else{
                    swal("操作失敗", "", "error");
                }
              });
            
            } else {
            
            }
        });
    }
  },
];
</script>
@endsection

@include('backpack::template.search')
