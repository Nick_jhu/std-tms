@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      國家建檔<small></small>
      </h1>
      <ol class="breadcrumb">
          <li class="active">國家建檔</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.enabledStatus = false;
gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_country') }}";
gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/sys_country') }}";
gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysCountry/create') }}";
gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysCountry') }}" + "/{id}/edit";
gridOpt.height = 800;
var enabledheader = [] ;
var filtercolumndata = [];
var btnGroup = [
  {
    btnId: "btnExportExcel",
    btnIcon: "fa fa-cloud-download",
    btnText: "{{ trans('common.exportExcel') }}",
    btnFunc: function () {
      // $("#jqxGrid").jqxGrid('exportdata', 'json', '{{ trans("sysCustomers.titleName") }}', true, null, false, BASE_API_URL+'/admin/export/data');
      var url  =BASE_URL;
      url = url.replace("admin","");
      var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
      filtercolumndata = [];
      for (var i = 0; i < filterGroups.length; i++) {
          var filterGroup = filterGroups[i];
          var filters = filterGroup.filter.getfilters();
              for (var k = 0; k < filters.length; k++) {
                 if(filters[k].condition!="CONTAINS" && filters[k].condition!="NOT_EQUAL" ){
                  var filtercolumn = {
                  'column': filterGroup.filtercolumn,
                  'value': Date.parse(filters[k].value),
                  'condition': filters[k].condition
              };
              }else{
                  var filtercolumn = {
                  'column': filterGroup.filtercolumn,
                  'value': filters[k].value,
                  'condition': filters[k].condition
              };
              }
              filtercolumndata.push(filtercolumn);
          }
      }
      var sortdata = $("#jqxGrid").jqxGrid("getsortcolumns");
      var sorttype = "";
      var sortfield = "";
      for(i=0;i<sortdata.length;i++){
          if(sortdata[i].ascending==true){
              type="asc";
          }else{
              type="desc";
          }
          if(sortfield==""){
              sortfield = sortdata[i].dataField;
          }else{
              sortfield = sortfield+";"+sortdata[i].dataField;
          }
          if(sorttype==""){
              sorttype = type;
          }else{
              sorttype = sorttype+";"+type;
          }
      }
      $.post(BASE_API_URL + '/admin/export/data', {
          'table': 'sys_country',
          'filename':'國家建檔' ,
          'columndata': filtercolumndata,
          'header': enabledheader,
          'sorttype': sorttype,
          'sortfield': sortfield,
      }, function(data){
          if(data.msg == "success") {
              window.location.href =url+"storage/excel/"+data.downlink;
          }
      });
    }
  },
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "{{ trans('common.gridOption') }}",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"{{ trans('common.add') }}",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"{{ trans('common.delete') }}",
    btnFunc:function(){
      alert("Delete");
    }
  }
];
</script>
@endsection

@include('backpack::template.search')
