@extends('backpack::layout')

@section('header')
<section class="content-header">
    <h1>
    醫療<small></small>
    </h1>
    <ol class="breadcrumb">
    <li class="active">醫療</li>
    </ol>
</section>
<form method="POST" accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="file" id="excelFile" name="import_file" style="display:none;"/>
    <button type="submit" class="btn btn-primary" id="btnImport" style="display:none;">{{ trans('excel.btnImport') }}</button>
</form>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.pageId        = "medical";
gridOpt.enabledStatus = false;
gridOpt.fieldsUrl     = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/medical_terms') }}";
gridOpt.dataUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/medical_terms') }}";
// gridOpt.createUrl     = "{{ url(config('backpack.base.route_prefix', 'admin') . '/goodsProfile/create') }}";
// gridOpt.editUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/goodsProfile') }}" + "/{id}/edit";
gridOpt.searchOpt = true;
gridOpt.height        = 800;
var enabledheader = [] ;
var filtercolumndata = [];
var btnGroup = [
    {
            btnId:"btnUploadExcel",
            btnIcon:"fa fa-cloud-upload",
            btnText:"excel匯入",
            btnFunc:function(){
                $("#excelFile").click();
            }
    },
    {
        btnId: "btnExportExcel",
        btnIcon: "fa fa-cloud-download",
        btnText: "{{ trans('common.exportExcel') }}",
        btnFunc: function () {
          location.href = BASE_URL + "/ExcelImport/getMed";
        }
    },
    {
        btnId: "btnOpenGridOpt",
        btnIcon: "fa fa-table",
        btnText: "{{ trans('common.gridOption') }}",
        btnFunc: function () {
            $('#gridOptModal').modal('show');
        }
    },
];
$(function(){
        $("#myForm").submit(function () {
            var postData = new FormData($(this)[0]);
            var mainCol = 14;
			$.ajax({
				url: BASE_URL + "/ExcelImport/importMed",
				type: 'POST',
				data: postData,
				async: false,
				beforeSend: function () {
					
				},
				error: function () {
                    swal("excel 匯入發生錯誤", "", "error");
                    $('#myForm')[0].reset();
					return false;
				},
				success: function (data) {
					if(data.msg == "error") {
                        swal("excel 匯入發生錯誤", "", "error");
                        $('#myForm')[0].reset();
                        return;
                    }
                    if(data.msg == "exist") {
                        swal(data.msg2, "", "error");
                        $('#myForm')[0].reset();
                        return;
                    }
                    
                    swal("匯入成功", "", "success");
                    $('#myForm')[0].reset();
                    $("#jqxGrid").jqxGrid('updatebounddata');
                    $("#jqxGrid").jqxGrid('clearselection');
				},
				cache: false,
				contentType: false,
				processData: false
			});
			return false;
        });

        
        $("#excelFile").on("change", function(){
            $("#myForm").submit();
        });
    })
</script>
@endsection

@include('backpack::template.search')
