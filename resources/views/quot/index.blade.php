@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>        
{{ trans('modQuot.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">{{ trans('backpack::logmanager.existing_logs') }}</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.enabledStatus = false;
gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_quot') }}";
gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_quot') }}";
gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/QuotMgmt/create') }}";
gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/QuotMgmt') }}" + "/{id}/edit";
gridOpt.height = 800;

var btnGroup = [
  {
    btnId: "btnExportExcel",
    btnIcon: "fa fa-cloud-download",
    btnText: "{{ trans('common.exportExcel') }}",
    btnFunc: function () {
      $("#jqxGrid").jqxGrid('exportdata', 'xls', '{{ trans("modQuot.titleName") }}');
    }
  },
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "{{ trans('common.gridOption') }}",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"{{ trans('common.add') }}",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"{{ trans('common.delete') }}",
    btnFunc:function(){
      swal("{{ trans('common.deleteMsg') }}", "", "success");
    }
  },
  {
    btnId:"btnDownload",
    btnIcon:"fa fa-download",
    btnText:"{{ trans('modQuot.downloadQuote') }}",
    btnFunc:function(){
      //swal('已發送', "派車訊息已發送至app", "success");
    }
  }, 
];
</script>
@endsection

@include('backpack::template.search')
