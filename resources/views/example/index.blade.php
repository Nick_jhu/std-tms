@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        Example.<small>{{ trans('example.log_manager_description') }}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/log') }}">{{ trans('backpack::logmanager.log_manager') }}</a></li>
        <li class="active">{{ trans('backpack::logmanager.existing_logs') }}</li>
      </ol>
    </section>
@endsection

@section('content')
<div class="box">
    <div class="box-body">
  

    <div id='jqxTabs'>
        <ul>
            <li style="margin-left: 30px;">
                Grid 1
            </li>
            <li>
                Grid 2
            </li>
        </ul>
        <div style="overflow: hidden;">
            <div style="border:none;" id="jqxGrid">
            </div>
        </div>
        <div style="overflow: hidden;">
            <div style="border:none;" id="jqxGrid2"></div>
        </div>
    </div>

    </div><!-- /.box-body -->
  </div><!-- /.box -->

@endsection


    @section('after_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
         
            var initGrid = function () {
                var source =
               {
                   datatype: "json",
                   datafields: [
                        { name: 'id' , type: 'string' },
                        { name: 'name' , type: 'string' },
                        { name: 'email' , type: 'string' },
                        { name: 'password' , type: 'string' },
                        { name: 'g_key' , type: 'string' },
                        { name: 'c_key' , type: 'string' },
                        { name: 's_key' , type: 'string' },
                        { name: 'd_key' , type: 'string' },
                        { name: 'remember_token' , type: 'string' },
                        { name: 'created_at' , type: 'date', format: "yyyy-MM-dd" },
                        { name: 'updated_at' , type: 'date',format: "yyyy-MM-dd" },
                    ],
                    root:"Rows",
                    pagenum: 1,
                    beforeprocessing: function (data) {
                        source.totalrecords = data[0].TotalRows;
                    },
                    filter: function () {
                    // update the grid and send a request to the server.
                        $("#jqxGrid").jqxGrid('updatebounddata', 'filter');
                    },
                    sort: function () {
                        // update the grid and send a request to the server.
                        $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
                    },
                    pagesize: 20,
                    url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/example/getData') }}"
               }
                var dataAdapter = new $.jqx.dataAdapter(source, { async: false, loadError: function (xhr, status, error) { alert('Error loading "' + source.url + '" : ' + error); } });
                $("#jqxGrid").jqxGrid(
                {
                    width: '100%',
                    height: '100%',
                    source: dataAdapter,
                    sortable: true,
                    filterable: true,
                    altrows: true,
                    showfilterrow: true,
                    pageable: true,
                    virtualmode: true,
                    autoshowfiltericon: true,
                    rendergridrows: function (params) {
                        return params.data;
                    },
                    selectionmode: 'multiplecellsextended',
                    columns: [
                        { text: 'id', datafield: 'id' , width: 250 },
                        { text: 'name', datafield: 'name' , width: 250 },
                        { text: 'email', datafield: 'email' , width: 250 },
                        { text: 'password', datafield: 'password' , width: 250 },
                        { text: 'Group', datafield: 'g_key' , width: 250 },
                        { text: 'Company', datafield: 'c_key' , width: 250 },
                        { text: 'Station', datafield: 's_key' , width: 250 },
                        { text: 'Derpartment', datafield: 'd_key' , width: 250 },
                        { text: 'remember_token', datafield: 'remember_token' , width: 250 },
                        { text: 'created_at',filtertype: 'date', datafield: 'created_at' , width: 250, cellsformat: 'yyyy-MM-dd' },
                        { text: 'updated_at',filtertype: 'date', datafield: 'updated_at' , width: 250 },
                    ]
                });
            }
            var initGrid2 = function () {
                var data = '[{ "CompanyName": "Alfreds Futterkiste", "ContactName": "Maria Anders", "ContactTitle": "Sales Representative", "Address": "Obere Str. 57", "City": "Berlin", "Country": "Germany" }, { "CompanyName": "Ana Trujillo Emparedados y helados", "ContactName": "Ana Trujillo", "ContactTitle": "Owner", "Address": "Avda. de la Constitucin 2222", "City": "Mxico D.F.", "Country": "Mexico" }, { "CompanyName": "Antonio Moreno Taquera", "ContactName": "Antonio Moreno", "ContactTitle": "Owner", "Address": "Mataderos 2312", "City": "Mxico D.F.", "Country": "Mexico" }, { "CompanyName": "Around the Horn", "ContactName": "Thomas Hardy", "ContactTitle": "Sales Representative", "Address": "120 Hanover Sq.", "City": "London", "Country": "UK" }, { "CompanyName": "Berglunds snabbkp", "ContactName": "Christina Berglund", "ContactTitle": "Order Administrator", "Address": "Berguvsvgen 8", "City": "Lule", "Country": "Sweden" }, { "CompanyName": "Blauer See Delikatessen", "ContactName": "Hanna Moos", "ContactTitle": "Sales Representative", "Address": "Forsterstr. 57", "City": "Mannheim", "Country": "Germany" }, { "CompanyName": "Blondesddsl pre et fils", "ContactName": "Frdrique Citeaux", "ContactTitle": "Marketing Manager", "Address": "24, place Klber", "City": "Strasbourg", "Country": "France" }, { "CompanyName": "Blido Comidas preparadas", "ContactName": "Martn Sommer", "ContactTitle": "Owner", "Address": "C\/ Araquil, 67", "City": "Madrid", "Country": "Spain" }, { "CompanyName": "Bon app\'", "ContactName": "Laurence Lebihan", "ContactTitle": "Owner", "Address": "12, rue des Bouchers", "City": "Marseille", "Country": "France" }, { "CompanyName": "Bottom-Dollar Markets", "ContactName": "Elizabeth Lincoln", "ContactTitle": "Accounting Manager", "Address": "23 Tsawassen Blvd.", "City": "Tsawassen", "Country": "Canada" }, { "CompanyName": "B\'s Beverages", "ContactName": "Victoria Ashworth", "ContactTitle": "Sales Representative", "Address": "Fauntleroy Circus", "City": "London", "Country": "UK" }, { "CompanyName": "Cactus Comidas para llevar", "ContactName": "Patricio Simpson", "ContactTitle": "Sales Agent", "Address": "Cerrito 333", "City": "Buenos Aires", "Country": "Argentina" }, { "CompanyName": "Centro comercial Moctezuma", "ContactName": "Francisco Chang", "ContactTitle": "Marketing Manager", "Address": "Sierras de Granada 9993", "City": "Mxico D.F.", "Country": "Mexico" }, { "CompanyName": "Chop-suey Chinese", "ContactName": "Yang Wang", "ContactTitle": "Owner", "Address": "Hauptstr. 29", "City": "Bern", "Country": "Switzerland" }, { "CompanyName": "Comrcio Mineiro", "ContactName": "Pedro Afonso", "ContactTitle": "Sales Associate", "Address": "Av. dos Lusadas, 23", "City": "Sao Paulo", "Country": "Brazil" }, { "CompanyName": "Consolidated Holdings", "ContactName": "Elizabeth Brown", "ContactTitle": "Sales Representative", "Address": "Berkeley Gardens 12 Brewery", "City": "London", "Country": "UK" }, { "CompanyName": "Drachenblut Delikatessen", "ContactName": "Sven Ottlieb", "ContactTitle": "Order Administrator", "Address": "Walserweg 21", "City": "Aachen", "Country": "Germany" }, { "CompanyName": "Du monde entier", "ContactName": "Janine Labrune", "ContactTitle": "Owner", "Address": "67, rue des Cinquante Otages", "City": "Nantes", "Country": "France" }, { "CompanyName": "Eastern Connection", "ContactName": "Ann Devon", "ContactTitle": "Sales Agent", "Address": "35 King George", "City": "London", "Country": "UK" }, { "CompanyName": "Ernst Handel", "ContactName": "Roland Mendel", "ContactTitle": "Sales Manager", "Address": "Kirchgasse 6", "City": "Graz", "Country": "Austria"}]';
                // prepare the data
                var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'CompanyName', type: 'string' },
                        { name: 'ContactName', type: 'string' },
                        { name: 'ContactTitle', type: 'string' },
                        { name: 'Address', type: 'string' },
                        { name: 'City', type: 'string' },
                        { name: 'Country', type: 'string' }
                    ],
                    localdata: data
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $("#jqxGrid2").jqxGrid(
                {
                    width: '100%',
                    height: '100%',
                    source: dataAdapter,
                    columns: [
                        { text: 'Company Name', datafield: 'CompanyName', width: 250 },
                        { text: 'Contact Name', datafield: 'ContactName', width: 150 },
                        { text: 'Contact Title', datafield: 'ContactTitle', width: 180 },
                        { text: 'City', datafield: 'City', width: 120 },
                        { text: 'Country', datafield: 'Country' }
                    ]
                });
            }
            // init widgets.
            var initWidgets = function (tab) {
                switch (tab) {
                    case 0:
                        initGrid();
                        break;
                    case 1:
                        initGrid2();
                        break;
                }
            }
            $('#jqxTabs').jqxTabs({ width: 600, height: 560,  initTabContent: initWidgets });
        });
    </script>

@endsection
