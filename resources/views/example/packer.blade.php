


<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>Index</title>
    <style>
        body {
            margin: 0;
        }
    </style>
</head>
<body>
    <div style="margin: 20px;">
        <h2>ontainer planning</h2><br/>
        <div id="chart1"></div><br/>
    </div>
    <script type="text/javascript" src="{{ asset('vendor/threejs') }}/3DAnimate.js"></script>


    <script>
        var data1 = {
            size: [20, 20, 100],
            boxes: [
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [0, 0, 0] },
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [6, 0, 0] },
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [0, 0, 4] },
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [6, 0, 4] },
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [0, 0, 8] },
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [6, 0, 8] },
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [0, 0, 12] },
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [6, 0, 12] },
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [0, 0, 16] },
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [6, 0, 16] },
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [0, 0, 20] },
                { size: [6, 15, 4], color: 0xffff00, lineColor: 0xcccccc, pos: [6, 0, 20] },
                { size: [8, 10, 10], color: 0x00ff00, lineColor: 0xcccccc, pos: [12, 0, 0] },
                { size: [8, 10, 10], color: 0x00ff00, lineColor: 0xcccccc, pos: [12, 0, 10] },
                { size: [10, 5, 30], color: 0x00ffff, lineColor: 0xff0000, pos: [0, 0, 30] },
                { size: [10, 5, 30], color: 0x00ffff, lineColor: 0xff0000, pos: [10, 0, 30] },
                { size: [10, 5, 30], color: 0x00ffff, lineColor: 0xff0000, pos: [0, 0, 60] },
                { size: [10, 5, 30], color: 0x00ffff, lineColor: 0xff0000, pos: [10, 0, 60] }
            ]
        };
        Init3DAnimate(data1, "chart1");
    </script>
</body>
</html>
