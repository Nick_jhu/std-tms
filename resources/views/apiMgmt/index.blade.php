@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
		{{ trans('dashboard.titleName') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">{{ trans('dashboard.titleName') }}</li>
	</ol>
</section>
@endsection 
@section('content')


<div class="row">
	<div class="col-md-12">
		<div class="box">
			<table class="table table-bordered">
                <thead>
                    <th>id</th>
                    <th>User Id</th>
                    <th>Name</th>
                    <th>secret</th>
                    <th>Create Time</th>
                    <th>Update Time</th>
                </thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection 
@section('after_scripts') 

<script>
    $(function(){
        $.get('{{url("oauth/clients")}}', {}, function(data){
            if(data) {
                var tr = "";
                for(i in data) {
                    tr += "<tr>\
                                <td>"+data[i].id+"</td>\
                                <td>"+data[i].user_id+"</td>\
                                <td>"+data[i].name+"</td>\
                                <td>"+data[i].secret+"</td>\
                                <td>"+data[i].created_at+"</td>\
                                <td>"+data[i].updated_at+"</td>\
                        ";
                }

                $("table tbody").html(tr);
            }
        });
    })
</script>
@endsection