@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        自動派車建檔<small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix'),'autosendcar') }}">自動派車建檔</a></li>
        <li class="active">自動派車建檔</li>
      </ol>
    </section>
@endsection

@section('content')
@include('backpack::template.toolbar')
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">自動派車建檔</a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="name">區域名稱</label>
                                        <input type="text" class="form-control" id="name" name="name">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>郵遞區號</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a Type" style="width: 100%;" name="zip_code[]">
                                        </select>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-3">
                                        {{-- <label for="created_by">{{ trans('modCar.createdBy') }}</label> --}}
                                        <input type="hidden" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{-- <label for="created_at">{{ trans('modCar.createdAt') }}</label> --}}
                                        <input type="hidden" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{-- <label for="updated_by">{{ trans('modCar.updatedBy') }}</label> --}}
                                        <input type="hidden" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{-- <label for="updated_at">{{ trans('modCar.updatedAt') }}</label> --}}
                                        <input type="hidden" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->                    
                    </div>
                    <!-- /.tab-content -->
                </div>
        </form>

        <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_6" data-toggle="tab" aria-expanded="false">車輛順序設定</a></li>
                {{-- <li class=""><a href="#tab_11" data-toggle="tab" aria-expanded="false">費用</a></li> --}}
                {{--  <li><a href="#tab_4" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.loadingPlan') }}</a></li>  --}}
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_6">
                    <div class="box box-primary" id="subBox" style="display:none">
                        <div class="box-header with-border">
                            <h3 class="box-title">車輛順序設定</h3>
                        </div>
                        <form method="POST" accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <label for="car_no">車號</label>
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control input-sm"  id="car_no" name="car_no" grid="car_no" required="required">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="car_no"
                                                    info1="{{Crypt::encrypt('mod_car')}}" 
                                                    info2="{{Crypt::encrypt('car_no,car_no,belong_nm,cust_no')}}" 
                                                    info3="{{Crypt::encrypt('')}}"
                                                    info4="car_no=car_no;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group col-md-2">
                                        <label for="car_no">車號</label>
                                        <input type="text" class="form-control input-sm" id="car_no"name="car_no" grid="true" >
                                    </div> --}}
                                    <div class="form-group col-md-2">
                                        <label for="sorted">順序</label>
                                        <input type="number" min="0" class="form-control input-sm" id="sorted" name="sorted" grid="true" required="required">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                @if(isset($id))
                                    <input type="hidden" class="form-control input-sm noClear"  name="auto_send_id" value="{{$id}}" grid="true">
                                @endif
                                <input type="hidden" class="form-control input-sm" name="id" grid="true">
                                <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                                <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                            </div>
                        </form>
                    </div>

                    <div id="jqxGrid"></div>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
    </div>
</div>    


@endsection

@include('backpack::template.lookup')


@section('after_scripts')


<script>
    @if(!Auth::user()->hasRole('Admin'))
    $("#iAdd").remove();
    $("#iDel").remove();
    @endif
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/autosendcar') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
    @endif

    
    
    @if(isset($id))
        mainId = "{{$id}}";
        editData = '{{!! $entry !!}}';
        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        editObj = JSON.parse(editData);
        cust_no= editObj.cust_no;
        console.log(editObj);
    @endif

  
   

    $(function(){

        $('#subBox button[btnName="car_no"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('car_no', "車輛建檔", callBackFunc=function(data){
                $("#subBox input[name='car_no']").val(data.car_no);
            });
        });

        $('#subBox input[name="car_no"]').on('click', function(){
            var check = $('#subBox input[name="car_no"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("subForm","car_no",callBackFunc=function(data){
                    $("#subBox input[name='car_no']").val(data.car_no);				
                },"car_no");
            }
        });

        $( "#lookupEvent" ).on( "custContactFunc", function(event,indexs) {
            /*$.each(indexs,function(k,v){
                alert(k+v);
            });*/
            //alert(1);
        });
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/autosendcar') }}";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_auto_send') }}/" + mainId;
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/autosendcar') }}";
        
        formOpt.initFieldCustomFunc = function (){
            
        };
        //setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);
        $.ajax({
				url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_auto_send_detail') }}",
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {

					var col = [
						[
							{name: "id", type: "number"},
                            {name: "car_no", type: "string"},
							{name: "auto_send_id", type: "string"},
							{name: "sorted", type: "string"},
							{name: "area", type: "string"},
						],
						[
							{text: "{{ trans('modOrderDetail.id') }}", datafield: "id", width: 100, hidden: true},
                            {text: "{{ trans('modOrderDetail.id') }}", datafield: "auto_send_id", width: 100, hidden: true},
                            // {text: "所屬站所", datafield: "area", width: 130, nullable: false},
                            {text: "車號", datafield: "car_no", width: 130, nullable: false},
							{text: "順序", datafield: "sorted", width: 100},
						]
					];
					
					var opt = {};
					opt.gridId = "jqxGrid";
					opt.fieldData = col;
					opt.formId = "subForm";
					opt.saveId = "Save";
					opt.cancelId = "Cancel";
					opt.showBoxId = "subBox";
					opt.height = 300;
					opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/autosendcarDetail/get') }}" + '/' + mainId;
					opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/autosendcarDetail') }}" + "/store";
					opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/autosendcarDetail') }}" + "/update";
					opt.delUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/autosendcarDetail') }}" + "/delete/";
					opt.commonBtn = true;
                    opt.showtoolbar = true;
					opt.defaultKey = {
						'auto_send_id': mainId
					};
					opt.beforeSave = function (row) {
						return true;
					}

					opt.afterSave = function (data) {
                        if(data.msg!="success"){
                            swal("警告", data.errormsg, "warning");
                        }
						// $('#feeGrid').jqxGrid('updatebounddata');
					}

					opt.beforeCancel = function() {

					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			});
    })
</script>    

@endsection
