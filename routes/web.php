<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin/board');
});
Route::post('updatefordrsize', 'DashboardController@updatefordrsize'); 
Route::get('lang/{locale}', ['as'=>'lang.change', 'uses'=>'LanguageController@setLocale']);
Route::post('checklogin', 'Auth\LoginController@checklogin');
Route::group(['prefix' => config('backpack.base.route_prefix')], function () {
     //Admin authentication routes
    Route::auth();
    Route::get('logout', 'Auth\LoginController@logout');
   
    
});

Route::group([
            'middleware' => ['web', 'admin'],
            'prefix'     => config('backpack.base.route_prefix', 'admin'),
    ], function () {

        Route::get('/apiMgmt', 'HomeController@apiMgmt');
        Route::get('/', 'Auth\LoginController@login');
        Route::get('getFieldsLangSepc/{table}', 'BaseApiController@getFieldsLangSepc');
        
        Route::get('example', 'ExampleController@index');
        Route::get('example/googleMapExample', 'ExampleController@googleMapExample');
        Route::get('example/packerExample', 'ExampleController@packerExample');
        Route::get('example/preview/{file_name}', 'ExampleController@preview');
        Route::get('example/download/{file_name}', 'ExampleController@download');
        Route::delete('example/delete/{file_name}', 'ExampleController@delete');
        CRUD::resource('user', 'UserController');
        CRUD::resource('custuser', 'CustuserController');
        Route::post('custuser/multi/del', 'CustuserController@multiDel'); 
        Route::get('example/testApi', 'ExampleController@testApi');

        
        CRUD::resource('customer', 'CustomerCrudController');
        Route::get('sample_detail/{cust_no?}', 'CustomerCrudController@get');
        Route::post('sample_detail/store', 'CustomerCrudController@detailStore');
        Route::post('sample_detail/update', 'CustomerCrudController@detailUpdate');
        Route::get('sample_detail/delete/{id}', 'CustomerCrudController@detailDel');
        Route::get('customer/detail/{cust_no}', 'CustomerCrudController@get');

        CRUD::resource('customerProfile', 'CustomerProfileCrudController');
        Route::post('customerFee/importExcel/{custid}', 'CustomerProfileCrudController@uploadExcel');
        Route::post('customerFee/exportExcel/{custid}', 'CustomerProfileCrudController@exportExcel');


        CRUD::resource('autosendcar', 'autosendProfileCrudController');
        Route::get('autosendcar/{mainId?}', 'autosendProfileCrudController@get');
        Route::post('autosendcar/multi/del', 'autosendProfileCrudController@multiDel');

        Route::post('autosendcarDetail/store', 'autosendProfileCrudController@detailStore');
        Route::post('autosendcarDetail/update', 'autosendProfileCrudController@detailUpdate');
        Route::get('autosendcarDetail/delete/{id}', 'autosendProfileCrudController@detailDel');
        Route::get('autosendcarDetail/get/{ord_id?}', 'autosendProfileCrudController@get');
        
        
        Route::post('customerProfile/store', 'CustomerProfileCrudController@feeStore');
        Route::post('customerProfile/update', 'CustomerProfileCrudController@feeUpdate');
        Route::get('customerProfile/delete/{id}', 'CustomerProfileCrudController@feeDel');
        Route::get('customerProfile/get/{cust_id?}', 'CustomerProfileCrudController@feeGet');

        Route::post('customererrorProfile/store', 'CustomerProfileCrudController@errorStore');
        Route::post('customererrorProfile/update', 'CustomerProfileCrudController@errorUpdate');
        Route::get('customererrorProfile/delete/{id}', 'CustomerProfileCrudController@errorDel');
        Route::get('customererrorProfile/get/{cust_id?}', 'CustomerProfileCrudController@errorGet');

        Route::post('longway/store', 'CustomerProfileCrudController@longwayStore');
        Route::post('longway/update', 'CustomerProfileCrudController@longwayUpdate');
        Route::get('longway/delete/{id}', 'CustomerProfileCrudController@longwayDel');
        Route::get('longway/get/{cust_id?}', 'CustomerProfileCrudController@longwayGet');

        Route::post('custProfile/multi/del', 'CustomerProfileCrudController@multiDel'); 
        Route::post('custProfile/multi/export', 'CustomerProfileCrudController@custsendedi'); 

        CRUD::resource('companyProfile', 'CompanyProfileCrudController');
        
        CRUD::resource('bulletin', 'BulletinCrudController');

        Route::get('SysSite', 'SysSiteController@index');
        Route::post('SysSite/get', 'SysSiteController@get');
        Route::post('SysSite/store', 'SysSiteController@store');
        Route::post('SysSite/del', 'SysSiteController@delete');
        Route::post('SysSite/update', 'SysSiteController@update');
        Route::get('/board', 'DashboardController@dashboard');
        Route::get('/map', 'DashboardController@map');
        Route::get('getAmtReportByMonth', 'DashboardController@getAmtReportByMonth');
        Route::get('getDriver', 'DashboardController@getDriver');
        Route::get('getAmtReportByDay', 'DashboardController@getAmtReportByDay');
        Route::get('getOrderReportByMonth', 'DashboardController@getOrderReportByMonth');
        Route::get('getOrderReportByDay', 'DashboardController@getOrderReportByDay');

        CRUD::resource('bscodeKind', 'BscodeKindCrudController');
        Route::get('bscode/{cd_type?}', 'BscodeKindCrudController@get');
        Route::post('bscode/store', 'BscodeKindCrudController@detailStore');
        Route::post('bscode/update/{cd_type}', 'BscodeKindCrudController@detailUpdate');
        Route::get('bscode/delete/{id}', 'BscodeKindCrudController@detailDel');
        Route::get('bscodeKind/detail/{cd_type}', 'BscodeKindCrudController@get');
  
        CRUD::resource('mailFormat', 'MailFormatCrudController');
        CRUD::resource('OrderMgmt', 'OrderMgmtCrudController');   //訂單管理
        CRUD::resource('OrderMgmtclose', 'OrderMgmtcloseCrudController');   //結案管理
        Route::post('orderclose/fail', 'OrderMgmtcloseCrudController@failOrder');
        Route::post('orderclose/reclose', 'OrderMgmtcloseCrudController@reclose');
        Route::post('orderclose/close', 'OrderMgmtcloseCrudController@close');

        Route::get('getfeevalue', 'OrderMgmtcloseCrudController@getfeevalue');

        Route::get('ConfirmOrderMgmt', 'OrderMgmtCrudController@confirmordOverView');   //回單管理
        Route::get('OrderMgmtconfirm/{ord_no?}', 'OrderMgmtCrudController@confirmordOveredit');
        Route::post('order/confirm', 'OrderMgmtCrudController@orderconfirm'); 
        Route::post('order/check/{ord_no?}', 'OrderMgmtCrudController@ordercheck'); 
        Route::post('order/checkforsn/{ord_no?}', 'OrderMgmtCrudController@checkforsn');

        Route::post('clearbotask', 'OrderMgmtCrudController@clearbotask');

        Route::post('order/checkasnrask', 'OrderMgmtCrudController@checkasnrask');
        Route::post('order/snnostore/', 'OrderMgmtCrudController@snnostore'); 

        Route::post('saveboxdetail', 'OrderMgmtCrudController@saveboxdetail');
        Route::post('changeboxno', 'OrderMgmtCrudController@changeboxno');
        Route::post('changeboxcheck', 'OrderMgmtCrudController@changeboxcheck');

        Route::get('dlvorder/check/{ord_no?}', 'OrderMgmtCrudController@dlvordercheck'); 
        Route::post('confirmordfee/{feecd?}/{ord_id?}', 'OrderMgmtCrudController@confirmordfee');
        Route::get('ordOverView', 'OrderMgmtCrudController@ordOverView');   //訂單管理

        Route::get('ordOverViewN', 'OrderMgmtCrudController@ordOverViewN');   //訂單管理
        Route::get('dashboard_data', 'OrderMgmtCrudController@getdata');

        Route::get('ordOverViewT', 'OrderMgmtCrudController@ordOverViewT');   //訂單管理
        Route::get('ordDetailOverView', 'OrderMgmtCrudController@ordDetailOverView');   //訂單管理
        Route::get('ordDetailOverCloseView', 'OrderMgmtCrudController@ordDetailOverCloseView');   //訂單明細總覽(結案)
        Route::post('orderDetail/store', 'OrderMgmtCrudController@detailStore');
        Route::post('orderDetail/update', 'OrderMgmtCrudController@detailUpdate');
        Route::get('orderDetail/delete/{id}', 'OrderMgmtCrudController@detailDel');
        Route::get('orderDetail/get/{ord_id?}', 'OrderMgmtCrudController@get');

        Route::post('orderclose/store', 'OrderMgmtcloseCrudController@detailStore');
        Route::post('orderclose/update', 'OrderMgmtcloseCrudController@detailUpdate');
        Route::get('orderclose/delete/{id}', 'OrderMgmtcloseCrudController@detailDel');
        Route::get('orderclose/get/{ord_id?}', 'OrderMgmtcloseCrudController@get');

        Route::get('order/getTsData', 'OrderMgmtCrudController@getTsData');
        Route::post('order/fail', 'OrderMgmtCrudController@failOrder');
        Route::post('order/close', 'OrderMgmtCrudController@closeOrder');
        Route::post('order/multi/del', 'OrderMgmtCrudController@multiDel'); 
        Route::post('order/multi/close', 'OrderMgmtCrudController@multiClose'); 

        Route::post('order/error/changecar', 'OrderMgmtCrudController@errorchangecar'); 
        Route::post('order/mistakecomplete', 'OrderMgmtCrudController@mistakecomplete'); 
        Route::post('fee/export', 'OrderMgmtCrudController@feeexport'); 

        Route::post('order/multi/reClose', 'OrderMgmtCrudController@reClose'); 
        Route::post('order/compulsiveDel/del', 'OrderMgmtCrudController@compulsiveDel');
        Route::post('order/sendedi', 'OrderMgmtCrudController@sendedi');  
        Route::post('order/reCalculateFee', 'OrderMgmtCrudController@reCalculateFee'); 

        Route::post('order/sylreport/down', 'reportTestController@sylreport1'); 
        Route::post('order/sylreportnew/down', 'reportTestController@sylreport1_new'); 
        Route::post('order/sylreportlist/down', 'reportTestController@sylreport1list'); 
        
        Route::post('order/sylreportlistnew/down', 'reportTestController@sylreport1list_new'); 
        Route::post('order/sylreportlistbag/down', 'reportTestController@sylreport1list_bag'); 
        Route::post('order/sylreportprivate/down', 'reportTestController@sylreportprivate'); 

        Route::post('order/autosylreportlistnew/down', 'reportTestController@autosylreport1list_new'); 
        Route::post('order/autosylreportlistbag/down', 'reportTestController@autosylreport1list_bag'); 
        Route::post('order/sylnewreportByowner/down', 'reportTestController@sylnewreportByowner'); 

        
        Route::post('ordertemp/autosylreportlistnew/down', 'reportforTestController@autosylreport1list_new'); 
        Route::post('ordertemp/autosylreportlistbag/down', 'reportforTestController@autosylreport1list_bag'); 
        Route::post('ordertemp/sylnewreportByowner/down', 'reportforTestController@sylnewreportByowner'); 

        Route::post('order/trackingadd/down', 'OrderMgmtCrudController@trackingadd'); 
        Route::post('order/cancelError', 'OrderMgmtCrudController@cancelError'); 

        // Route::post('orderPack/store', 'OrderMgmtCrudController@packStore');
        // Route::post('orderPack/update', 'OrderMgmtCrudController@packUpdate');
        // Route::get('orderPack/delete/{id}', 'OrderMgmtCrudController@packDel');
        // Route::get('orderPack/get/{ord_no?}', 'OrderMgmtCrudController@packGet');

        Route::post('orderPackDetail/update', 'OrderMgmtCrudController@packDetailUpdate');
        Route::get('orderPackDetail/get/{ord_no?}', 'OrderMgmtCrudController@packDetailGet');


        Route::post('orderdetailamt/store', 'OrderMgmtCrudController@DeatailamtStore');
        Route::post('orderdetailamt/storenew', 'OrderMgmtCrudController@DefatailamtStore_new');
        Route::post('orderdetailamt/detailstorenew', 'OrderMgmtCrudController@DeatailStore_new');
        Route::post('orderdetailamt/delall/{ord_no?}', 'OrderMgmtCrudController@Deatailamtdelall');
        Route::post('orderdetailamt/deldetailall/{ord_no?}', 'OrderMgmtCrudController@Deataildelall');

        Route::post('orderdetailamt/update', 'OrderMgmtCrudController@DeatailamtUpdate');
        Route::get('orderdetailamt/delete/{id}', 'OrderMgmtCrudController@DeatailamtDel');
        Route::get('orderdetailamt/get/{ord_id?}', 'OrderMgmtCrudController@DeatailamtGet');
        Route::get('orderdetailimg/delete/{id}', 'OrderMgmtCrudController@DeatailimgDel');
        Route::get('orderdetailimg/get/{ord_id?}', 'OrderMgmtCrudController@DeatailimgGet');

        Route::get('orderdetailfee/delete/{id}', 'OrderMgmtCrudController@Deatailfeedel');
        Route::get('orderdetailfee/get/{ord_id?}', 'OrderMgmtCrudController@DeatailifeeGet');

        Route::get('ordercloseimg/delete/{id}', 'OrderMgmtcloseCrudController@DeatailimgDel');
        Route::get('ordercloseimg/get/{ord_id?}', 'OrderMgmtcloseCrudController@DeatailimgGet');
        Route::post('orderimg/store', 'OrderMgmtcloseCrudController@Deatailimgstore');

        Route::get('ordertally/get/{ord_id?}', 'OrderMgmtCrudController@ordertallyGet');
        
        Route::post('updatefordrsize', 'DashboardController@updatefordrsize'); 
        CRUD::resource('OrderMgmttest', 'OrderMgmtCrudController');   //訂單管理
        Route::get('/tracking', function () {
            $title = trans('tracking.titleName');
            echo "<title>$title</title>";
            try{
                if(Auth::user()->hasPermissionTo('Tracking'))
                {
                    return view('tracking.tracking');
                }else{
                    return back();
                }
            }
            catch(\Exception $e) {
                return back();
            }
            return back();
        });
        Route::post('trackingApi/insertTracking', 'TrackingApiController@insertTracking');
        Route::get('trackingApi/getCarLocation', 'TrackingApiController@getCarLocation');

        CRUD::resource('TranPlanMgmt/SendCar', 'DlvCrudController');   //運輸計劃
        Route::get('TranPlanMgmt/DlvPlanSearch', 'DlvCrudController@DlvPlanSearch');   //運輸計劃
        Route::post('TranPlanMgmt/SendCar/getPack', 'DlvCrudController@getPack');
        Route::get('carLoad/get/{dlv_no?}', 'DlvCrudController@getCarLoad');
        Route::post('TranPlanMgmt/SendCar/handPickup', 'DlvCrudController@handPickup');
        Route::post('TranPlanMgmt/SendCar/autoPickup', 'DlvCrudController@autoPickup');
        Route::post('TranPlanMgmt/insertToDlv', 'DlvCrudController@insertToDlv');
        Route::post('TranPlanMgmt/sendToApp', 'DlvCrudController@sendToApp');
        Route::post('TranPlanMgmt/error/changecar', 'DlvCrudController@errorchangecar');
        Route::post('TranPlanMgmt/readyback', 'DlvCrudController@readyback');

        CRUD::resource('QuotMgmt', 'QuotMgmtCrudController');   //報價管理

        CRUD::resource('carProfile', 'CarProfileCrudController');   //卡車建檔
        Route::post('carProfile/multi/del', 'CarProfileCrudController@multiDel'); 
        CRUD::resource('sysCountry', 'SysCountryCrudController');   //國家代碼建檔
        CRUD::resource('sysArea', 'SysAreaCrudController');   //區域建檔
        CRUD::resource('modTransStatus', 'ModTransStatusCrudController');   //貨況建檔

        CRUD::resource('goodsProfile', 'GoodsCrudController');   //料號建檔
        Route::post('goodsProfile/multi/del', 'GoodsCrudController@multiDel'); 

        Route::post('goodsProfile/importExcel', 'GoodsCrudController@uploadExcel');


        Route::get('carPath/get', 'TrackingApiController@getCarPath');
        Route::get('car/get/{car_no?}', 'TrackingApiController@getCar');
        Route::get('car/getcar', 'TrackingApiController@initgetcar');
        Route::get('testPath', 'TrackingApiController@testPath');
        //Route::get('testMail', 'TrackingApiController@testMail');

        CRUD::resource('medical', 'medicalCrudController');
        Route::get('import', 'ExcelImportController@import');
        Route::get('ExcelImport/getMed', 'ExcelImportController@getMed');
        Route::post('ExcelImport/importMed', 'ExcelImportController@importMed');
        Route::post('ExcelImport/importExcel', 'ExcelImportController@importExcel');
        Route::post('ExcelImport/importExcelname', 'ExcelImportController@importExcelname');
        Route::post('ExcelImport/checkExcelname', 'ExcelImportController@checkExcelname');
        Route::get('ExcelImport/get', 'ExcelImportController@get');
        Route::post('ExcelImport/store', 'ExcelImportController@saveToModOrder');

        Route::get('clearTmpOrderByWeblink', 'ExcelImportController@clearTmpOrderByWeblink');

        CRUD::resource('weblinkProfile', 'WeblinkProfileCrudController');

        Route::get('clearTmpOrder', 'ExcelImportController@clearTmpOrder');
        Route::get('repairimport', 'ExcelrepairImportController@repairimport');
        Route::get('ExcelrepairImport/get', 'ExcelrepairImportController@get');
        Route::post('ExcelrepairImport/importExcel', 'ExcelrepairImportController@importExcel');
        Route::post('ExcelrepairImport/store', 'ExcelrepairImportController@saveToModOrder');

        Route::get('TranPlanMgmt/DlvCar', 'DlvCrudController@DlvCar');
        Route::get('TranPlanMgmt/getcar', 'DlvCrudController@getcar');
        Route::get('TranPlanMgmt/DlvCarnew', 'DlvCrudController@DlvCartest');
        Route::get('TranPlanMgmt/GetOrd', 'DlvCrudController@GetOrd');
        Route::post('TranPlanMgmt/sendDlvCar', 'DlvCrudController@sendDlvCar');
        Route::post('TranPlanMgmt/autosend', 'DlvCrudController@autosend');
        Route::get('TranPlanMgmt/getDlvPlan/{dlvNo?}', 'DlvCrudController@getDlvPlan');
        Route::post('sendormsg', 'DlvCrudController@sendappmsg'); 
        Route::post('OrderMgmt/chStation', 'OrderMgmtCrudController@chStation');
        Route::post('OrderMgmt/chStationall', 'OrderMgmtCrudController@chStationall');
        Route::post('OrderMgmt/sendMessage', 'OrderMgmtCrudController@sendMessage');
        Route::post('OrderMgmt/changeReady', 'OrderMgmtCrudController@changeReady');
        Route::post('OrderMgmt/errorselect', 'OrderMgmtCrudController@errorselect');
        Route::get('TranPlanMgmt/getStantiondata', 'DlvCrudController@getStantiondata');
        Route::get('TranPlanMgmt/getMessagedata', 'DlvCrudController@getMessagedata');
        Route::get('TranPlanMgmt/getDlvEtaPlan/{dlvNo?}', 'DlvCrudController@getDlvEtaPlan');
        Route::get('TranPlanMgmt/getDlvsortPlan/{dlvNo?}', 'DlvCrudController@getDlvsortPlan');
        Route::get('TranPlanMgmt/getDlvsortPlan2/{dlvNo?}/{type?}/{sorttype?}', 'DlvCrudController@getDlvsortPlan2');
        Route::get('TranPlanMgmt/getDlvsortPlan3/{dlvNo?}', 'DlvCrudController@getDlvsortPlan3');
        Route::post('TranPlanMgmt/upDlvPlansort', 'DlvCrudController@upDlvPlansort');
        Route::post('TranPlanMgmt/customupDlvPlansort', 'DlvCrudController@customupDlvPlansort');
        Route::post('TranPlanMgmt/autosendorder', 'DlvCrudController@autosendorder');
        Route::post('TranPlanMgmt/rmDlvPlan', 'DlvCrudController@rmDlvPlan');
        Route::post('TranPlanMgmt/delDlvPlan', 'DlvCrudController@delDlvPlan');

        Route::get('sysRefFeeCar', 'SysRefFeeCrudController@sysRefFeeCar');
        Route::post('sysRefFeeCar/store', 'SysRefFeeCrudController@CarStore');
        Route::post('sysRefFeeCar/update', 'SysRefFeeCrudController@CarUpdate');
        Route::get('sysRefFeeCar/delete/{id}', 'SysRefFeeCrudController@CarDel');
        Route::get('sysRefFeeCar/get', 'SysRefFeeCrudController@CarGet');
        Route::post('sysRefFeeCar/importExcel', 'SysRefFeeCrudController@importCarExcel');
        Route::get('sysRefFeeCar/download', 'SysRefFeeCrudController@getCarDownload');
        
        Route::get('sysRefFeeNonCar', 'SysRefFeeCrudController@sysRefFeeNonCar');
        Route::post('sysRefFeeNonCar/store', 'SysRefFeeCrudController@NonCarStore');
        Route::post('sysRefFeeNonCar/update', 'SysRefFeeCrudController@NonCarUpdate');
        Route::get('sysRefFeeNonCar/delete/{id}', 'SysRefFeeCrudController@NonCarDel');
        Route::get('sysRefFeeNonCar/get', 'SysRefFeeCrudController@NonCarGet');
        Route::post('sysRefFeeNonCar/importExcel', 'SysRefFeeCrudController@importNonCarExcel');
        Route::get('sysRefFeeNonCar/download', 'SysRefFeeCrudController@getNonCarDownload');

        Route::get('sysRefFeeNonCarNoDiscount', 'SysRefFeeCrudController@sysRefFeeNonCarNoDiscount');
        Route::post('sysRefFeeNonCarNoDiscount/store', 'SysRefFeeCrudController@NonCarNoDiscountStore');
        Route::post('sysRefFeeNonCarNoDiscount/update', 'SysRefFeeCrudController@NonCarNoDiscountUpdate');
        Route::get('sysRefFeeNonCarNoDiscount/delete/{id}', 'SysRefFeeCrudController@NonCarNoDiscountDel');
        Route::get('sysRefFeeNonCarNoDiscount/get', 'SysRefFeeCrudController@NonCarNoDiscountGet');

        Route::get('sysRefFeeDelivery', 'SysRefFeeCrudController@sysRefFeeDelivery');
        Route::post('sysRefFeeDelivery/store', 'SysRefFeeCrudController@DeliveryStore');
        Route::post('sysRefFeeDelivery/update', 'SysRefFeeCrudController@DeliveryUpdate');
        Route::get('sysRefFeeDelivery/delete/{id}', 'SysRefFeeCrudController@DeliveryDel');
        Route::get('sysRefFeeDelivery/get', 'SysRefFeeCrudController@DeliveryGet');

        Route::post('sysRefFeePiece/store', 'SysRefFeeCrudController@PieceStore');
        Route::post('sysRefFeePiece/update', 'SysRefFeeCrudController@PieceUpdate');
        Route::get('sysRefFeePiece/delete/{id}', 'SysRefFeeCrudController@PieceDel');
        Route::get('sysRefFeePiece/get', 'SysRefFeeCrudController@PieceGet');

        Route::post('sysRefFeeCbm/store', 'SysRefFeeCrudController@CbmStore');
        Route::post('sysRefFeeCbm/update', 'SysRefFeeCrudController@CbmUpdate');
        Route::get('sysRefFeeCbm/delete/{id}', 'SysRefFeeCrudController@CbmDel');
        Route::get('sysRefFeeCbm/get', 'SysRefFeeCrudController@CbmGet');

        Route::post('sysRefFeeGw/store', 'SysRefFeeCrudController@GwStore');
        Route::post('sysRefFeeGw/update', 'SysRefFeeCrudController@GwUpdate');
        Route::get('sysRefFeeGw/delete/{id}', 'SysRefFeeCrudController@GwDel');
        Route::get('sysRefFeeGw/get', 'SysRefFeeCrudController@GwGet');

        Route::post('sysRefFeeDelivery/importExcel', 'SysRefFeeCrudController@importDeliveryExcel');
        Route::get('sysRefFeeDelivery/download', 'SysRefFeeCrudController@getDeliveryDownload');

        Route::get('orderExcelSample/download', 'ExcelImportController@getOrderExcel');

        Route::get('get/{table?}/{id?}', 'CommonController@getData'); 
        Route::get('gcsd/get', 'CommonController@getCompnayList'); 

        Route::get('show/{ord_no?}/{type?}', 'OrderMgmtCrudController@showChkImg');     
        Route::get('export/detail', 'DlvCrudController@exportDetail');     
        Route::get('searchList/get/{key}', 'CommonController@getSearchLayoutList');
        Route::get('searchHtml/get/{id?}', 'CommonController@getSearchHtml');

        Route::get('getnewfee/{feecd?}/{ownercd?}/{goodsno?}', 'OrderMgmtCrudController@getnewfee');
        Route::get('getnewfeelookup/{detailid?}/{ownercd?}', 'OrderMgmtCrudController@getnewfeelookup');
        Route::post('exportfee', 'OrderMgmtCrudController@exportfee');

        Route::post('saveSearchLayout', 'CommonController@saveSearchLayout');
        Route::put('setSearchDefault/{key}/{id}', 'CommonController@setSearchDefault');
        Route::post('delSearchLayout/{id}', 'CommonController@delSearchLayout');
        Route::delete('delSearchLayout/{id}', 'CommonController@delSearchLayout');
        Route::get('layout', 'CommonController@Layout');
        Route::post('layout/copylayout', 'CommonController@copylayout');
        Route::post('layout/copylayoutconfirm', 'CommonController@copylayoutconfirm');
        Route::get('getCarType', 'CommonController@getCarType');
        Route::post('userPwd/update', 'CommonController@updatePwd');

        Route::get('img/get', 'OrderMgmtCrudController@imgDownload');
        Route::get('closeimg/get', 'OrderMgmtcloseCrudController@imgDownload');
        Route::get('snno', function(){
            $user = Auth::user();
            $data = array(
                'crud' => (object)array(
                    'colModel' => array()
                ),
            );
            try{
                if(Auth::user()->hasPermissionTo('snno'))
                {
                    return view('order.sn');
                    // return view('order.test', ['title' => $title,'content' => ""]);
                }
            }
            catch(\Exception $e) {
                
                return back();
            }
            return back();
        });

        Route::get('boxtask', function(){
            $user = Auth::user();
            $data = array(
                'crud' => (object)array(
                    'colModel' => array()
                ),
            );
            try{
                if(Auth::user()->hasPermissionTo('boxtask'))
                {
                    $sql = "SELECT value1 , value2  FROM bscode WHERE cd_type = 'PINTERNAME' GROUP BY value1 , value2 ";
                    $result = DB::select($sql);
                    $channelData = array();
                    foreach($result as $row) {
                        $channelData[]  = $row;
                    }
                    return view('order.boxtask')->with('channelData', $channelData);
                }
            }
            catch(\Exception $e) {
                dd($e->getMessage());
                return back();
            }
            return back();
        });

        Route::get('boxtask2', function(){
            $user = Auth::user();
            $data = array(
                'crud' => (object)array(
                    'colModel' => array()
                ),
            );
            try{
                if(Auth::user()->hasPermissionTo('boxtask'))
                {
                    return view('order.boxtask2');
                }
            }
            catch(\Exception $e) {
                
                return back();
            }
            return back();
        });

        Route::get('OrderMgmt1', function(){
            $user = Auth::user();
            $data = array(
                'crud' => (object)array(
                    'colModel' => array()
                ),
            );
            try{
                if(Auth::user()->hasPermissionTo('OrderMgmt1'))
                {
                    // dd( $data);
                    $msgdata = DB::table('bscode')->where('g_key', $user->g_key)->where('cd_type', 'MESSAGETYPE')->get();
                    $errordata = DB::table('bscode')->select("cd","cd_descp")->where('g_key', $user->g_key)->where('cd_type', 'ERRORTYPE')->get();
                    $viewData = array(
                        'msgdata' => $msgdata,
                        'errordata' => $errordata,
                    );
                    return view('order.test')->with('viewData', $viewData);
                    // return view('order.test', ['title' => $title,'content' => ""]);
                }
            }
            catch(\Exception $e) {
                
                return back();
            }
            return back();
        });
});

Auth::routes();

Route::get('/apiHome', 'HomeController@index')->name('apiHome');

Route::get('/queryTracking', "TrackingApiController@trackingView");

Route::get('admin/mapfly', function(){
    return view('order.map');
});
