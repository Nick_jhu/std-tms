<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    //'middleware' => ['web', 'admin'],
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
], function () {
// Route::post('login', 
    Route::get('example/getData', 'ExampleController@getData');
    Route::get('example/getFieldsJson', 'ExampleController@getFieldsJson');
    Route::get('baseApi/getGridJson/{table}/{status?}', 'BaseApiController@getGridJson');
    Route::get('baseApi/getGridJsonForMongo/{table}/{status?}', 'BaseApiController@getGridJsonForMongo');
    Route::get('baseApi/getStatusCount/{tableName}', 'BaseApiController@getStatusCount');
    Route::get('baseApi/getFieldsJson/{table}', 'BaseApiController@getFieldsJson');
    Route::get('baseApi/getLookupGridJson/{info1}/{info2}/{info3}', 'BaseApiController@getLookupGridJson');
    Route::get('baseApi/getLookupFieldsJson/{info1}/{info2}', 'BaseApiController@getLookupFieldsJson');
    Route::get('baseApi/getAutocompleteJson/{info1}/{info2}/{info3}/{input}', 'BaseApiController@getAutocompleteJson');
    Route::get('baseApi/getAutoNumber/{model}', 'BaseApiController@getAutoNumber');
    
    Route::get('baseApi/getLayoutJson', 'BaseApiController@getLayoutJson');
    Route::post('baseApi/saveLayoutJson', 'BaseApiController@saveLayoutJson');
    Route::post('baseApi/clearLayout', 'BaseApiController@clearLayout');

    //貨況相關
    Route::get('trackingApi/get/{ord_no?}', 'TrackingApiController@getTrackingByOrder');
    Route::get('tracking/get/{ord_no?}', 'TrackingApiController@getTsRecord');
    

    Route::get('dlvApi/getDlvData', 'DlvCrudController@getDlvData');
    Route::get('getOrderData/{dlv_no?}', 'OrderMgmtCrudController@getOrderData');
    Route::get('getOrderDetail/{ord_no?}', 'OrderMgmtCrudController@getOrderDetail');
    Route::post('uploadChk', 'OrderMgmtCrudController@orderUpload');
    Route::post('sendMessage', 'FcmController@sendMessage');
    Route::post('export/data', 'CommonController@exportData');
});

Route::group([    
    'middleware' => ['auth:api','cors']
], function () {
    Route::get('show/{ord_no?}', 'OrderMgmtCrudController@showChkImg');
    //app api
    Route::get('dlvData/get/{car_no?}', 'DlvCrudController@getDlvDataByCarNo');
    //send logout
    Route::post('/user/logout','Api\DataController@logout' );

    //-------------------------------------
    //修改使𡪤者資訊
    Route::post('/std/refreshpass', 'Api\DataController@stdrefreshpass');
    //抓取Dlv 主檔 配送中
    Route::post('std/user/getDlvData','Api\DataController@stdgetDlvData' );
    //抓取Dlv 細檔 配送中NEW
    Route::post('/std/user/getDlvDetailDataNew','Api\DataController@stdgetDlvDetailData_NEW' );
    //send gps
    Route::post('/std/sendGpsData','Api\DataController@stdsendGpsData' );
    //std找取訂單明細new(含照片)
    Route::get('/std/stdordDetail/get/{sysOrdNo?}/{type?}/','Api\DataController@stdordDetail');
    //查詢任務
    Route::get('/std/missonfind/get/{sysOrdNo?}','Api\DataController@stdmissonfind' );
    // 代收款回報
    Route::post('/std/sendExtraData','Api\DataController@stdordConfirm_NEW' );
    // 版本檢查
    Route::post('/std/user/appcheck','Api\DataController@stdcheckversion' );
    // 異常回報
    Route::post('/std/error/reportnew','Api\DataController@stderrorReport_JSON' );
    // 出發_new
    Route::post('/std/departurenew/{dlvNo?}','Api\DataController@stddeparturenew' );
    //send 提貨資料
    Route::post('/std/sendpickorder','Api\DataController@stdsendpickorder' );
    //查詢任務
    Route::get('/std/getorderinfo','Api\DataController@stdgetorderinfo' );
    //上傳照片
    Route::post('/std/uploadSignImg', 'Api\DataController@stduploadSignImg');
    //上傳異常照片
    Route::post('/std/uploadAbnormalImg', 'Api\DataController@stduploadAbnormalImg');
    //更新序號new
    Route::post('/std/updateSnNo_new', 'Api\DataController@stdupdateSnNo_new');
    //刪除照片
    Route::post('/std/delPhoto', 'Api\DataController@delPhoto');
    //接單
    Route::post('/std/user/checkdlvorder','Api\DataController@stdcheckdlvorder' );
    //小車司機刷訂單派車
    Route::post('/std/user/dlvorder','Api\DataController@stddlvorder' );
    Route::post('/std/reorder', 'Api\DataController@stdreorderDlvPlan');
    //-------------------------------------

    //抓取Dlv 主檔 配送中
    Route::post('/user/getDlvData','Api\DataController@getDlvData' );
    //抓取Dlv 細檔 配送中
    Route::post('/user/getDlvDetailData','Api\DataController@getDlvDetailData' );
    //抓取Dlv 細檔 配送中NEW
    Route::post('/user/getDlvDetailDataNew','Api\DataController@getDlvDetailData_NEW' );
    //找取訂單明細
    Route::get('/ordDetail/get/{sysOrdNo?}/{type?}','Api\DataController@getOrdDetail' );
    //找取訂單明細_new
    Route::get('/ordDetail_new/get/{sysOrdNo?}/{type?}/{dlv_no?}','Api\DataController@getOrdDetail_new' );
    


    //查詢任務
    Route::get('/missonfind/get/{sysOrdNo?}','Api\DataController@missonfind' );
    //查詢任務
    Route::get('/getorderinfo','Api\DataController@getorderinfo' );
    // Order Confirm
    Route::post('/order/confirm','Api\DataController@ordConfirm' );
    // 異常回報
    Route::post('/error/report','Api\DataController@errorReport' );
    // 異常回報
    Route::post('/error/reportnew','Api\DataController@errorReport_JSON' );
    // 版本檢查
    Route::post('/user/appcheck','Api\DataController@checkversion' );
    // 出發
    Route::post('/departure/{dlvNo?}','Api\DataController@departure' );
    // 出發_new
    Route::post('/departurenew/{dlvNo?}','Api\DataController@departurenew' );
    
    //抓取異常原因下拉
    Route::get('getErrorData','Api\DataController@getErrorData' );
    //抓取代收款下拉
    Route::get('getExtraData','Api\DataController@getExtraData' );

    //抓取異常原因下拉
    Route::get('getErrorData_new','Api\DataController@getErrorData_new' );
    //抓取代收款下拉
    Route::get('getExtraData_new','Api\DataController@getExtraData_new' );

    //抓取異常原因下拉
    Route::post('getErrorData_new','Api\DataController@getErrorData_new' );
    //抓取代收款下拉
    Route::post('getExtraData_new','Api\DataController@getExtraData_new' );

    // 代收款回報
    // Route::post('/sendExtraData','Api\DataController@sendExtraData' );
    Route::post('/sendExtraData','Api\DataController@ordConfirm_NEW' );
    //抓取系統公告
    Route::get('/user/getNoticeData','Api\DataController@getNoticeData' );
    //系統公告設為已讀
    Route::post('/user/updateNoticeData','Api\DataController@updateNoticeData' );
    //send gps
    Route::post('/sendGpsData','Api\DataController@sendGpsData' );
    Route::post('/trackingadd','Api\DataController@trackingadd' );
    //send 提貨資料
    Route::post('/sendpickorder','Api\DataController@sendpickorder' );
    //修改使𡪤者資訊
    Route::post('/user/update', 'Api\DataController@modifyUser');
    //修改使𡪤者資訊
    Route::post('/refreshpass', 'Api\DataController@refreshpass');
    //取得user資訊
    Route::post('/user/getData','Api\DataController@getUserData' );
    //取得聊天列表
    Route::get('/chatroom/get', 'Api\DataController@getChatroomList');
    //Tracking開關
    Route::post('/tracking/switch/{switch?}', 'Api\DataController@trackingSwitch');
    //Tracking開關
    Route::get('/tracking/get/status', 'Api\DataController@getTrackingStatus');
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('/user/dToken', 'Api\DataController@updateDtoken');

    Route::get('/notice/count/get', 'Api\DataController@getNoticeCnt');
    Route::get('/notice/read', 'Api\DataController@setNoticeRead');
    Route::post('/reorder', 'Api\DataController@reorderDlvPlan');
    Route::post('uploadSignImg', 'Api\DataController@uploadSignImg');
    Route::post('uploadAbnormalImg', 'Api\DataController@uploadAbnormalImg');
    Route::post('updateSnNo', 'Api\DataController@updateSnNo');

    Route::post('updateSnNo_new', 'Api\DataController@updateSnNo_new');
    Route::post('delPhoto', 'Api\DataController@delPhoto');

    //小車司機刷訂單派車檢查
    Route::post('/user/checkdlvorder','Api\DataController@checkdlvorder' );
    //小車司機刷訂單派車
    Route::post('/user/dlvorder','Api\DataController@dlvorder' );
    //派單接單權限
    Route::get('/user/menupersion','Api\DataController@menupersion' );

    //get order img
    Route::post('/user/get/order/img','Api\DataController@getorderimg');
    //delete order img
    Route::post('/user/del/order/img','Api\DataController@delorderimg');


    Route::post('app/crash/log', 'AppCrashLogController@store');

});

Route::get('/updateTime/get', function (Request $request) {
    return response()->json(['msg' => 'success', 'time1' => 1000, 'time2' => 60000]);
});

Route::get('/version/get', function (Request $request) {
    return response()->json(['msg' => 'success', 'version' => '1.0.6']);
});
//benq
Route::middleware('cors')->post('/user/benqxmlget','Api\DataController@benqxmlget');
Route::middleware('cors')->post('/user/benqxmlReverseget','Api\DataController@benqxmlunget');
Route::middleware('cors')->post('/user/login','Api\LoginController@login');
Route::post('trackingApi/sendGps', 'TrackingApiController@testGpsData');
Route::get('/out/ordDetail/get/{sysOrdNo?}/{type?}','Api\DataController@getOrdDetail' );


Route::post('/geteditoken','Api\DataController@geteditoken' );
Route::post('/utmsorder','Api\DataController@ediblno' );

//mi
// https://core.standard-info.com/api/miorder/
Route::post('/miorder/{toekn?}','Api\DataController@miorder' );

Route::post('/migetorder/{toekn?}','Api\DataController@migetorder' );

Route::post('/miapitest','Api\DataController@testmiapireturn' );

Route::post('/miconfirm/{toekn?}','Api\DataController@miconfirm' );

Route::get('/check', function(){
    return response()->json(["msg" => "success"]);
});

Route::get('/getsql/status','Api\DataController@getsqlstatus' );

