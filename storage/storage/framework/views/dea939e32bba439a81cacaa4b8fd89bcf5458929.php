 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
		<?php echo e(trans('modOrder.titleName')); ?>

		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?php echo e(trans('modOrder.titleName')); ?></li>
	</ol>
</section>
<div class="modal fade" id="ts-modal" tabindex="-1" role="dialog" aria-labelledby="lookupTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="lookupTitle"><?php echo e(trans('modOrder.cargochoice')); ?></h5>
			</div>
			<div class="modal-body" id="ts-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
	<input type="hidden" id="lookupEvent" name="lookupEvent" />
</div>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('before_scripts'); ?>


<script>
	var gridOpt = {};
    gridOpt.enabledStatus = true;
    gridOpt.fieldsUrl     = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order')); ?>";
    gridOpt.dataUrl       = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order')); ?>";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl     = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt/create')); ?>";
    gridOpt.editUrl       = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>" + "/{id}/edit";
    gridOpt.height        = 800;
    statusGridObj = [{"key":"SEND","val":"123"}];
    var btnGroup = [
    {
        btnId: "btnExportExcel",
        btnIcon: "fa fa-cloud-download",
        btnText: "<?php echo e(trans('common.exportExcel')); ?>",
        btnFunc: function () {
        $("#jqxGrid").jqxGrid('exportdata', 'xls', '<?php echo e(trans("modOrder.titleName")); ?>');
        }
    },
    {
        btnId: "btnOpenGridOpt",
        btnIcon: "fa fa-table",
        btnText: "<?php echo e(trans('common.gridOption')); ?>",
        btnFunc: function () {
            $('#gridOptModal').modal('show');
        }
    },
    {
        btnId: "btnAdd",
        btnIcon: "fa fa-edit",
        btnText: "<?php echo e(trans('common.add')); ?>",
        btnFunc: function () {
        location.href = gridOpt.createUrl;
        }
    },
    /*{
        btnId: "btnAutoLoad",
        btnIcon: "fa fa-archive",
        btnText: "<?php echo e(trans('modOrder.autoLoading')); ?>",
        btnFunc: function () {
        swal("<?php echo e(trans('modOrder.autoSuccess')); ?>", "", "success");
        }
    },*/
    {
        btnId:"btnDelete",
        btnIcon:"fa fa-trash-o",
        btnText:"<?php echo e(trans('common.delete')); ?>",
        btnFunc:function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
            var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
            if(typeof row != "undefined") {
                ids.push(row.id);
            }
            }
            if(ids.length == 0) {
            swal("請至少選擇一筆資料", "", "warning");
            }
            $.post(BASE_URL + '/order/multi/del', {'ids': ids}, function(data){
            if(data.msg == "success") {
                swal("刪除成功", "", "success");
                $("#jqxGrid").jqxGrid('updatebounddata');
            }
            else{
                swal("操作失敗", "", "error");
            }
            });
        }
    },
    {
        btnId: "btnSendStatus",
        btnIcon: "fa fa-newspaper-o",
        btnText: "<?php echo e(trans('modOrder.sendCargo')); ?>",
        btnFunc: function () {
        var rowid = $("#jqxGrid").jqxGrid('getselectedrowindex');
        if(rowid == -1) {
            swal("<?php echo e(trans('modOrder.sendCargoWarning')); ?>", "", "warning");
            return;
        }


        $.get("<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/order/getTsData')); ?>", {}, function(data){
            var ttl = '';
            $.each(data, function(i, v){
            var p = '', ul = '', li = '';
            p  += '<h4>'+data[i]["ts_type"]+'</h4>';
            ul += '<ul class="list-inline">';
            
            $.each(data[i]["status"], function(k, vv) {
                li += '<li><button type="button" class="btn bg-purple btn-flat margin tsBtn" tsid="'+data[i]["status"][k]["id"]+'">'+data[i]["status"][k]["ts_name"]+'</button></li>';
            });
            ul += li;
            ul += '</ul>';

            ttl += p + ul;
            });

            $("#ts-body").html(ttl);

            $(".tsBtn").unbind( "click" ).on("click", function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    selectedRecords.push({tsid: $(this).attr("tsid"), ord_no: row.ord_no});
                }
            }

            $.ajax({
                url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/trackingApi/insertTracking')); ?>",
                type: 'POST',
                async: false,
                data: {data: JSON.stringify(selectedRecords)},
                beforeSend: function () {
                loadingFunc.show();
                },
                error: function (jqXHR, exception) {
                swal('Oops...', "<?php echo e(trans('modOrder.error')); ?>", "error");
                loadingFunc.hide();
                return;
                },
                success: function (data) {
                if(data.msg == "success") {
                    swal('Success', "<?php echo e(trans('modOrder.sendSuccess')); ?>", "success");

                }
                else {
                    swal('Oops...', "<?php echo e(trans('modOrder.error')); ?>", "error");

                }
                loadingFunc.hide();
                return;
                }
            });
            });

            $("#ts-modal").modal("show");
        }, 'JSON');
        }
    },
    {
        btnId: "btnShowImg",
        btnIcon: "fa fa-picture-o",
        btnText: "簽收照片",
        btnFunc: function () {
        var rowid = $("#jqxGrid").jqxGrid('getselectedrowindex');
        if(rowid == -1) {
            swal("<?php echo e(trans('modOrder.sendCargoWarning')); ?>", "", "warning");
            return;
        }
        var value = $('#jqxGrid').jqxGrid('getcellvalue', rowid, "ord_no");

        window.open("<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/show')); ?>" + "/" + value, '簽收照片', config='height=500,width=600');
        }
    },
    {
        btnId:"btnFail",
        btnIcon:"fa fa-ban",
        btnText:"<?php echo e(trans('common.fail')); ?>",
        btnFunc:function(){
        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        var ids = new Array();
        for (var m = 0; m < rows.length; m++) {
            var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
            if(typeof row != "undefined") {
            ids.push(row.id);
            }
        }
        if(ids.length == 0) {
            swal("請至少選擇一筆資料", "", "warning");
        }
        $.post(BASE_URL + '/order/fail', {'ids': ids}, function(data){
            if(data.msg == "success") {
            swal("作廢完成", "", "success");
            $("#jqxGrid").jqxGrid('updatebounddata');
            }
            else{
            swal("操作失敗", "", "error");
            }
        });
        }
    }
    ];

</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('backpack::template.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>