

<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
      <?php echo e(trans('sysCustomers.titleAddName')); ?><small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="<?php echo e(url(config('backpack.base.route_prefix'),'customerProfile')); ?>"><?php echo e(trans('sysCustomers.titleName')); ?></a></li>
		<li class="active"><?php echo e(trans('sysCustomers.titleAddName')); ?></li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php echo $__env->make('backpack::template.toolbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">        
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?php echo e(trans('sysCustomers.baseInfo')); ?></a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="cust_no"><?php echo e(trans('sysCustomers.custNo')); ?></label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no" placeholder="Enter No." required="required">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="cname"><?php echo e(trans('sysCustomers.cname')); ?></label>
                                        <input type="text" class="form-control" id="cname" name="cname" placeholder="Enter Chinese Name">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="ename"><?php echo e(trans('sysCustomers.ename')); ?></label>
                                        <input type="text" class="form-control" id="ename" name="ename" placeholder="Enter English Name">
                                    </div>                                    
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="email"><?php echo e(trans('sysCustomers.email')); ?></label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input type="eamil" class="form-control" id="email" name="email" >
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="cmp_abbr"><?php echo e(trans('sysCustomers.cmpAbbr')); ?></label>
                                        <input type="text" class="form-control" id="cmp_abbr" name="cmp_abbr" placeholder="Enter Abbreviation" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="status"><?php echo e(trans('sysCustomers.status')); ?></label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="A"><?php echo e(trans('sysCustomers.STATUS_A')); ?></option>
                                            <option value="B"><?php echo e(trans('sysCustomers.STATUS_B')); ?></option>
                                        </select>
                                    </div>                                                                       
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="contact"><?php echo e(trans('sysCustomers.contact')); ?></label>
                                        <input type="text" class="form-control" id="contact" name="contact" placeholder="Enter Contact Person" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="phone"><?php echo e(trans('sysCustomers.phone')); ?></label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" class="form-control" id="phone" name="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="fax"><?php echo e(trans('sysCustomers.fax')); ?></label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-fax"></i>
                                            </div>
                                            <input type="text" class="form-control" id="fax" name="fax">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label><?php echo e(trans('sysCustomers.custType')); ?></label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a Type" style="width: 100%;" name="cust_type[]">
                                        </select>
                                    </div>                                                                       
                                </div>                               

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="address"><?php echo e(trans('sysCustomers.address')); ?></label>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><?php echo e(trans('sysCustomers.remark')); ?></label>
                                            <textarea class="form-control" rows="3" name="remark" placeholder="Enter ..."></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by"><?php echo e(trans('sysCustomers.createdBy')); ?></label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at"><?php echo e(trans('sysCustomers.createdAt')); ?></label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by"><?php echo e(trans('sysCustomers.updatedBy')); ?></label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at"><?php echo e(trans('sysCustomers.updatedAt')); ?></label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                                                
                                <?php if(isset($id)): ?>
                                    <input type="hidden" name="id" value="<?php echo e($id); ?>" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                <?php endif; ?>

                            </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>   

<?php $__env->stopSection(); ?>


<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->startSection('after_scripts'); ?>

<script type="text/javascript" src="<?php echo e(asset('vendor/jqwidgets')); ?>/jqxwindow.js"></script>


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/customerProfile')); ?>";

    var fieldData = null;
    var fieldObj = null;

    <?php if(isset($crud->create_fields)): ?>
        fieldData = '{<?php echo json_encode($crud->create_fields); ?>}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
    <?php endif; ?>


    <?php if(isset($id)): ?>
        mainId = "<?php echo e($id); ?>";
        editData = '{<?php echo json_encode($entry["original"]); ?>}';

        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        //editObj = JSON.parse(editData);
        //cust_no= editObj.cust_no;
        //console.log(editObj);
    <?php endif; ?>


    $(function(){
        
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/customerProfile')); ?>";
        //formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_customers')); ?>";
        formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') .'/get/sys_customers')); ?>/" + mainId;
        formOpt.saveUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/customerProfile')); ?>";
        
        formOpt.initFieldCustomFunc = function (){
            $("select[name='state[]']").select2({tags: true});
        };

        formOpt.addFunc = function() {
            $("#status").val("B");
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);      
    })
</script>    

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>