<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo e(trans('menu.map')); ?>111</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="http://materializecss.com/css/ghpages-materialize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <style>
        body,
        html {
            height: 100%;
            width: 100%;
        }

        #map_canvas {
            height: 100%;
            width: 100%
        }

        #map_canvas img {
            max-width: none;
        }

        #map_canvas div {
            -webkit-transform: translate3d(0, 0, 0);
        }
    </style>
</head>

<body>
    <div class="fixed-action-btn" style="top: 45px; left: 24px;width:56px;height:56px;">
        <a class="btn-floating btn-large button-collapse" data-activates="nav-mobile">
            <i class="material-icons">menu</i>
        </a>
    </div>
    <ul id="nav-mobile" class="side-nav">
        <li>
            <div class="user-view">
                <div class="background">
                    <img src="https://images.unsplash.com/photo-1493679349286-c570804c71ec?auto=format&fit=crop&w=1351&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D"
                        width="800">
                </div>
                <a href="#!user">
                    <!-- <img class="circle" src="images/yuna.jpg"> -->
                    <img src="https://placehold.it/160x160/26a69a/ffffff/&text=<?php echo e(mb_substr(Auth::user()->name, 0, 1)); ?>" class="circle" alt="User Image">
                </a>
                <a href="#!name">
                    <span class="white-text"><?php echo e(Auth::user()->name); ?></span>
                </a>
                <a href="#!email">
                    <span class="white-text email"><?php echo e(Auth::user()->email); ?></span>
                </a>
            </div>
        </li>
        <li style="padding: 10px;">
            <div class="input-field">
                <input id="car_no" id="car_no" type="text" class="validate">
                <label for="car_no">車號：</label>
            </div>
        </li>
        <li style="padding: 10px;">
            <div class="input-field">
                <input id="s_date" id="s_date" type="text" class="validate">
                <label for="s_date">開始時間：</label>
            </div>
        </li>
        <li style="padding: 10px;">
            <div class="input-field">
                <input id="e_date" id="e_date" type="text" class="validate">
                <label for="e_date">結束時間：</label>
            </div>
        </li>
        <li>
            <a class="waves-effect waves-light btn" id="searchPathBtn">卡車路線</a>
        </li>
        <li>
            <a class="waves-effect waves-light btn" id="searchCar">卡車目前狀態</a>
        </li>
        <li>
            <div class="divider"></div>
        </li>
    </ul>
    <div id="map_canvas" style="position: relative; overflow: hidden;"></div>
</body>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBZ79gLT1qaLfNJDf98OJr8cT5FZ4MJ4_w&libraries=geometry,places&ext=.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.0/firebase.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script>
    $('#s_date').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });

    $('#e_date').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });

    $("#s_date").click(function(){
        $("div.bootstrap-datetimepicker-widget").find("a.btn").removeClass("btn");
    });

    $("#e_date").click(function(){
        $("div.bootstrap-datetimepicker-widget").find("a.btn").removeClass("btn");
    });

    var markers = [];
    var styledMapType = new google.maps.StyledMapType(
        [{
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }]
            },
            {
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#616161"
                }]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "color": "#f5f5f5"
                }]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#bdbdbd"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#eeeeee"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#757575"
                }]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e5e5e5"
                }]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#9e9e9e"
                }]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#757575"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#dadada"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#616161"
                }]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#9e9e9e"
                }]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e5e5e5"
                }]
            },
            {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#eeeeee"
                }]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#c9c9c9"
                }]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#9e9e9e"
                }]
            }
        ], {
            name: 'Styled Map'
        });
    var map = new google.maps.Map(document.getElementById('map_canvas'), {
        center: new google.maps.LatLng(25.041591, 121.538248),
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    });

    //Associate the styled map with the MapTypeId and set it to display.
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');

    
    var flightPath = null;
    $(function () {
        $(".button-collapse").sideNav({
            menuWidth: 300
        });

        $("#searchPathBtn").click(function () {

            $.get("<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/carPath/get')); ?>", {'s_date': $("#s_date").val(), 'e_date': $('#e_date').val(), 'car_no': $('#car_no').val()}, function (data) {
                if(data.length == 0) {
                    swal("查無結果", "", "warning");
                    return;
                }
                if(flightPath != null) {
                    flightPath.setMap(null);
                }
                clearMarkers()
                var flightPlanCoordinates = data;
                flightPath = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    geodesic: true,
                    strokeColor: '#ec407a ',
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                });

                flightPath.setMap(null);
                flightPath.setMap(map);
                map.setCenter({
                    lat: data[0].lat,
                    lng: data[0].lng
                });

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(data[0].lat, data[0].lng),
                    map: map,
                    icon: 'https://cdn3.iconfinder.com/data/icons/social-media-2068/64/_Location-32.png'
                });
                markers.push(marker);

                var marker1 = new google.maps.Marker({
                    position: new google.maps.LatLng(data[data.length - 1].lat, data[
                        data.length - 1].lng),
                    map: map,
                    icon: 'https://cdn3.iconfinder.com/data/icons/social-media-2068/64/_Location-32.png'
                });
                markers.push(marker1);
            });
        });
    });


    $(function(){
        $("#searchCar").click(function(){
            $.get("<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/car/get')); ?>/" + $("#car_no").val(), {}, function(data){
                if(flightPath != null) {
                    flightPath.setMap(null);
                }
                
                clearMarkers()
                map.setCenter({
                            lat: data.lat,
                            lng: data.lng
                        });
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(data.lat,data.lng),
                    map: map,
                    icon: "https://cdn4.iconfinder.com/data/icons/shopping-colorful-flat-long-shadow/136/Shopping_icons-31-24-48.png"
                });

                markers.push(marker);
            });
        });
    });

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }
</script>

</html>