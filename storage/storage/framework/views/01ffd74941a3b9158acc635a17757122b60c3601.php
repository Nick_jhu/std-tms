 
<?php $__env->startSection('header'); ?>
<section class="content-header">

</section>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('content'); ?>
<style>
	.font-white { color: white}
</style>
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?php echo e(trans('sysRefFee.titleName')); ?></a></li>                    
			<li><a href="#tab_2" data-toggle="tab" aria-expanded="false"><?php echo e(trans('sysRefFee.titleNameNonCar')); ?></a></li>                    
			<li><a href="#tab_3" data-toggle="tab" aria-expanded="false"><?php echo e(trans('sysRefFee.titleNameDelivery')); ?></a></li>                    
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<div class="row">
					<div class="col-md-12">	
						<form method="POST"  accept-charset="UTF-8" id="myForm1" enctype="multipart/form-data">
								<?php echo e(csrf_field()); ?>

								<input type="file" name="import_file" id="importFile1" class="btn-primary font-white"/>
								<button type="submit" class="btn btn-primary" id="btnImport1"><?php echo e(trans('excel.btnImport')); ?></button>						
								<a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeCar/download')); ?>" class="btn btn-primary"><?php echo e(trans('sysRefFee.downloadExample')); ?></a>
						</form>
					</div>
				</div>
				<div class="box box-primary" id="subBox1" style="display:none">
					<form method="POST" accept-charset="UTF-8" id="subForm1" enctype="multipart/form-data">
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-4">
									<input type="hidden" name="car_type_cd">
									<input type="hidden" name="type">
									<label for="car_type_nm"><?php echo e(trans('sysRefFee.carTypeNm')); ?></label>
								<div class="input-group input-group-sm">
									<input type="text" class="form-control" id="car_type_nm" name="car_type_nm">
									<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-flat lookup" btnname="car_type_nm"
											info1="<?php echo e(Crypt::encrypt('bscode')); ?>" 
											info2="<?php echo e(Crypt::encrypt('cd+cd_descp,cd,cd_descp')); ?>" 
											info3="<?php echo e(Crypt::encrypt('cd_type=\'CARTYPE\'')); ?>"
											info4="cd=car_type_cd;cd_descp=car_type_nm" triggerfunc="" selectionmode="singlerow">
											<i class="fa fa-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
								</div>
								<div class="form-group col-md-4">
									<label for="distance"><?php echo e(trans('sysRefFee.distance')); ?></label>
									<input type="text" class="form-control input-sm" name="distance" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="regular_fee"><?php echo e(trans('sysRefFee.regularFee')); ?></label>
									<input type="text" class="form-control input-sm" name="regular_fee" grid="true" >
								</div>											
							</div>
							<div class="row">
								<div class="form-group col-md-4">
									<label for="fee_from"><?php echo e(trans('sysRefFee.feeFrom')); ?></label>
									<input type="text" class="form-control input-sm" name="fee_from" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee_to"><?php echo e(trans('sysRefFee.feeTo')); ?></label>
									<input type="text" class="form-control input-sm" name="fee_to" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee"><?php echo e(trans('sysRefFee.fee')); ?></label>
									<input type="text" class="form-control input-sm" name="fee" grid="true" >
								</div>											
							</div>
							<div class="row">
								<div class="form-group col-md-8">
									<label for="remark"><?php echo e(trans('sysRefFee.remark')); ?></label>
									<input type="text" class="form-control input-sm" name="remark" grid="true" >
								</div>																					
							</div>											
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="hidden" class="form-control input-sm" name="id" grid="true">
							<button type="button" class="btn btn-sm btn-primary" id="Save1"><?php echo e(trans('common.save')); ?></button>
							<button type="button" class="btn btn-sm btn-danger" id="Cancel1"><?php echo e(trans('common.cancel')); ?></button>
						</div>
					</form>
				</div>
				<div id="jqxGrid1"></div>
			</div>

			<div class="tab-pane" id="tab_2">
				<div class="row">
					<div class="col-md-12">
						<div class="callout callout-danger" id="errorMsg" style="display:none">
							<h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4>
							<ul>

							</ul>
						</div>
						<form method="POST" accept-charset="UTF-8" id="myForm2" enctype="multipart/form-data">
							<?php echo e(csrf_field()); ?>

							<input type="file" name="import_file" id="importFile2" class="btn-primary font-white" />
							<button type="submit" class="btn btn-primary" id="btnImport2"><?php echo e(trans('excel.btnImport')); ?></button>
							<a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeNonCar/download')); ?>" class="btn btn-primary"><?php echo e(trans('sysRefFee.downloadExample')); ?></a>
						</form>
					</div>
				</div>

				<div class="box box-primary" id="subBox2" style="display:none">
					<form method="POST" accept-charset="UTF-8" id="subForm2" enctype="multipart/form-data">
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-4">
									<input type="hidden" id="car_type_cd2" name="car_type_cd">
									<input type="hidden" id="type2" name="type" value="N">
									<label for="car_type_nm"><?php echo e(trans('sysRefFee.carTypeNm')); ?></label>
									<div class="input-group input-group-sm">
										<input type="text" class="form-control" id="car_type_nm2" name="car_type_nm">
										<span class="input-group-btn">
											<button type="button" class="btn btn-default btn-flat lookup" btnname="car_type_nm2"
												info1="<?php echo e(Crypt::encrypt('bscode')); ?>" 
												info2="<?php echo e(Crypt::encrypt('cd+cd_descp,cd,cd_descp')); ?>" 
												info3="<?php echo e(Crypt::encrypt('cd_type=\'CARTYPE\'')); ?>"
												info4="cd=car_type_cd2;cd_descp=car_type_nm2" triggerfunc="" selectionmode="singlerow">
												<i class="fa fa-search" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="fee_unit"><?php echo e(trans('sysRefFee.feeUnit')); ?></label>
									<input type="text" class="form-control input-sm" name="fee_unit" grid="true">
								</div>
								
							</div>
							<div class="row">
								<div class="form-group col-md-4">
									<label for="fee_from"><?php echo e(trans('sysRefFee.feeFrom')); ?></label>
									<input type="text" class="form-control input-sm" name="fee_from" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee_to"><?php echo e(trans('sysRefFee.feeTo')); ?></label>
									<input type="text" class="form-control input-sm" name="fee_to" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee"><?php echo e(trans('sysRefFee.fee')); ?></label>
									<input type="text" class="form-control input-sm" name="fee" grid="true">
								</div>
							</div>
							<div class="row">
								
								<div class="form-group col-md-8">
									<label for="remark"><?php echo e(trans('sysRefFee.remark')); ?></label>
									<input type="text" class="form-control input-sm" name="remark" grid="true">
								</div>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="hidden" class="form-control input-sm" name="id" grid="true">
							<button type="button" class="btn btn-sm btn-primary" id="Save2"><?php echo e(trans('common.save')); ?></button>
							<button type="button" class="btn btn-sm btn-danger" id="Cancel2"><?php echo e(trans('common.cancel')); ?></button>
						</div>
					</form>
				</div>

				<div id="jqxGrid2"></div>
			</div>

			<div class="tab-pane" id="tab_3">
				<div class="row">
					<div class="col-md-12">
						<form method="POST"  accept-charset="UTF-8" id="myForm3" enctype="multipart/form-data">
							<?php echo e(csrf_field()); ?>

							<input type="file" name="import_file" id="importFile3" class="btn-primary font-white"/>
							<button type="submit" class="btn btn-primary" id="btnImport3"><?php echo e(trans('excel.btnImport')); ?></button>						
							<a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/download')); ?>" class="btn btn-primary"><?php echo e(trans('sysRefFee.downloadExample')); ?></a>
						</form>
					</div>
				</div>
				<div class="box box-primary" id="subBox3" style="display:none">
					<form method="POST" accept-charset="UTF-8" id="subForm3" enctype="multipart/form-data">
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-4">
									<input type="hidden" name="trans_type_cd">
									<input type="hidden" id="type" name="type">
									<label for="car_type_nm"><?php echo e(trans('sysRefFee.transTypeNm')); ?></label>
								<div class="input-group input-group-sm">
									<input type="text" class="form-control" id="trans_type_nm" name="trans_type_nm">
									<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-flat lookup" btnname="trans_type_nm"
											info1="<?php echo e(Crypt::encrypt('bscode')); ?>" 
											info2="<?php echo e(Crypt::encrypt('cd+cd_descp,cd,cd_descp')); ?>" 
											info3="<?php echo e(Crypt::encrypt('cd_type=\'DELIVERYTYPE\'')); ?>"
											info4="cd=trans_type_cd;cd_descp=trans_type_nm" triggerfunc="" selectionmode="singlerow">
											<i class="fa fa-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
								</div>
								<div class="form-group col-md-4">
									<label for="fee_op"><?php echo e(trans('sysRefFee.feeOp')); ?></label>
									<input type="text" class="form-control input-sm" name="fee_op" grid="true" >
								</div>
								<div class="form-group col-md-4">
									<label for="fee_weight"><?php echo e(trans('sysRefFee.feeWeight')); ?></label>
									<input type="text" class="form-control input-sm" name="fee_weight" grid="true" >
								</div>
							</div>
							<div class="row">											
								<div class="form-group col-md-4">
									<label for="fee"><?php echo e(trans('sysRefFee.fee')); ?></label>
									<input type="text" class="form-control input-sm" name="fee" grid="true" >
								</div>
								<div class="form-group col-md-8">
									<label for="remark"><?php echo e(trans('sysRefFee.remark')); ?></label>
									<input type="text" class="form-control input-sm" name="remark" grid="true" >
								</div>																					
							</div>																		
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="hidden" class="form-control input-sm" name="id" grid="true">
							<button type="button" class="btn btn-sm btn-primary" id="Save3"><?php echo e(trans('common.save')); ?></button>
							<button type="button" class="btn btn-sm btn-danger" id="Cancel3"><?php echo e(trans('common.cancel')); ?></button>
						</div>
					</form>
				</div>

				<div id="jqxGrid3"></div>
			</div>
		</div>
	</div>
	


<?php $__env->stopSection(); ?> 
<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $__env->startSection('after_scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset('js')); ?>/bootstrap.file-input.js"></script>
<script>
	var mainId = "";

	$(function () {			
		car();
		nonCar();
		express();
	});

	function car() {
		$('#importFile1').bootstrapFileInput();
		$('#subBox1 button[btnName="car_type_nm"]').on('click', function(){
			$('#lookupModal').modal('show');
			initLookup('car_type_nm', "<?php echo e(trans('sysRefFee.carTypeNm')); ?>", callBackFunc=function(data){
				$("#subBox1 input[name='car_type_nm'").val(data.cd_descp);
				$("#subBox1 input[name='car_type_cd'").val(data.cd);
			});
		});
		$('#subBox1 input[name="car_type_nm"]').on('click', function(){
			var check = $('#subBox1 input[name="car_type_nm"]').data('ui-autocomplete') != undefined;
			if(check == false) {
				initAutocomplete("subForm1","car_type_nm",callBackFunc=function(data){
					$("#subBox1 input[name='car_type_cd'").val(data.cd);
				});
			}
		});
		mainId = "mainId"
		$.ajax({
			url: "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee')); ?>"+ "?basecon=type;EQUAL;C",					
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				var custFieldData = [];
				custFieldData[0] = [
					{name: 'id', type: 'integer'},
					{name: 'car_type_nm', type: 'string'},
					{name: 'distance', type: 'float'},
					{name: 'regular_fee', type: 'float'},
					{name: 'fee_from', type: 'float'},
					{name: 'fee_to', type: 'float'},
					{name: 'fee', type: 'float'},
					{name: 'remark', type: 'string'},
				];
				custFieldData[1] = [
					{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.carTypeNm')); ?>", datafield: 'car_type_nm', filtertype:"textbox", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.distance')); ?>", datafield: 'distance', filtertype:"number", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.regularFee')); ?>", datafield: 'regular_fee', filtertype:"number", width:100},
					{text:"<?php echo e(trans('sysRefFee.feeFrom')); ?>", datafield: 'fee_from', filtertype:"number", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.feeTo')); ?>", datafield: 'fee_to', filtertype:"number", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.fee')); ?>", datafield: 'fee', filtertype:"number", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.remark')); ?>", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
				];
				var opt = {};
				opt.gridId = "jqxGrid1";
				opt.fieldData = custFieldData;
				opt.formId = "subForm1";
				opt.saveId = "Save1";
				opt.cancelId = "Cancel1";
				opt.showBoxId = "subBox1";
				opt.height = $( window ).height() - 250;
				opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeCar/get')); ?>";
				opt.addUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeCar')); ?>" + "/store";
				opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeCar')); ?>" + "/update";
				opt.delUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeCar')); ?>" + "/delete/";
				opt.commonBtn = true;
				opt.showtoolbar = true;
				opt.defaultKey = {
					
				};
				opt.beforeSave = function (formData) {
					// $addBtn.jqxButton({disabled: false});
					// $delBtn.jqxButton({disabled: false});
					
				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});

		$("#myForm1").submit(function () {
			if ($('#importFile1').get(0).files.length === 0) {
				swal("<?php echo e(trans('sysRefFee.msg1')); ?>", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			$.ajax({
				url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeCar/importExcel')); ?>",
				type: 'POST',
				data: postData,
				async: false,
				beforeSend: function () {
					
				},
				error: function () {
					swal("<?php echo e(trans('sysRefFee.msg2')); ?>", "", "error");
					// swal("<?php echo e(trans('excel.msg2')); ?>", {
					// 	icon: "error",
					// });
					return false;
				},
				success: function (data) {
					//alert(data);
					if(data.msg == "error") {
						swal("<?php echo e(trans('sysRefFee.msg2')); ?>", "", "error");
						// swal("<?php echo e(trans('excel.msg2')); ?>", {
						// icon: "error",
						// });
					}else{
						swal("<?php echo e(trans('sysRefFee.msg3')); ?>", "", "success");
					}
					$('#jqxGrid1').jqxGrid('updatebounddata');
					// var dataAdapter = new $.jqx.dataAdapter(data.data);
					// $("#jqxGrid").jqxGrid({ source: dataAdapter });
				
				},
				cache: false,
				contentType: false,
				processData: false
			});
			return false;
		});
	}

	function express() {
		$('#importFile3').bootstrapFileInput();
		$('button[btnName="trans_type_nm"]').on('click', function(){
		$('#lookupModal').modal('show');
		initLookup('trans_type_nm', "<?php echo e(trans('sysRefFee.transTypeNm')); ?>", callBackFunc=function(data){
			
			});
		});
		$('#trans_type_nm').on('click', function(){
			var check = $('#trans_type_nm').data('ui-autocomplete') != undefined;
			if(check == false) {
				initAutocomplete("subForm","trans_type_nm",callBackFunc=function(data){
					
				});
			}
		});

		
		mainId = "mainId"
		$.ajax({
			url: "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee')); ?>"+ "?basecon=type;EQUAL;D",					
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				var custFieldData = [];
				custFieldData[0] = [
					{name: 'id', type: 'integer'},
					{name: 'trans_type_nm', type: 'string'},
					{name: 'fee_op', type: 'string'},
					{name: 'fee_weight', type: 'float'},
					{name: 'fee', type: 'float'},			
					{name: 'remark', type: 'string'},
				];
				custFieldData[1] = [
					{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.transTypeNm')); ?>", datafield: 'trans_type_nm', filtertype:"textbox", width:220, editable: false},
					{text:"<?php echo e(trans('sysRefFee.feeOp')); ?>", datafield: 'fee_op', filtertype:"textbox", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.feeWeight')); ?>", datafield: 'fee_weight', filtertype:"number", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.fee')); ?>", datafield: 'fee', filtertype:"number", width:100},				
					{text:"<?php echo e(trans('sysRefFee.remark')); ?>", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
				];
				var opt = {};
				opt.gridId = "jqxGrid3";
				opt.fieldData = custFieldData;
				opt.formId = "subForm3";
				opt.saveId = "Save3";
				opt.cancelId = "Cancel3";
				opt.showBoxId = "subBox3";
				opt.height = $( window ).height() - 250;
				opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/get')); ?>";
				opt.addUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery')); ?>" + "/store";
				opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery')); ?>" + "/update";
				opt.delUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery')); ?>" + "/delete/";
				opt.commonBtn = true;
				opt.showtoolbar = true;
				opt.defaultKey = {
					
				};
				opt.beforeSave = function (formData) {
					// $addBtn.jqxButton({disabled: false});
					// $delBtn.jqxButton({disabled: false});
					
				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});

		$("#myForm3").submit(function () {
			if ($('#importFile3').get(0).files.length === 0) {
				swal("<?php echo e(trans('sysRefFee.msg1')); ?>", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			$.ajax({
			url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/importExcel')); ?>",
			type: 'POST',
			data: postData,
			async: false,
			beforeSend: function () {
				
			},
			error: function () {
				swal("<?php echo e(trans('sysRefFee.msg2')); ?>", "", "error");
				// swal("<?php echo e(trans('excel.msg2')); ?>", {
				// 	icon: "error",
				// });
				return false;
			},
			success: function (data) {
				//alert(data);
				if(data.msg == "error") {
					swal("<?php echo e(trans('sysRefFee.msg2')); ?>", "", "error");
					// swal("<?php echo e(trans('excel.msg2')); ?>", {
					// icon: "error",
					// });
				}else{
					swal("<?php echo e(trans('sysRefFee.msg3')); ?>", "", "success");
				}
			$('#jqxGrid').jqxGrid('updatebounddata');
			// var dataAdapter = new $.jqx.dataAdapter(data.data);
			// $("#jqxGrid").jqxGrid({ source: dataAdapter });
			
			},
			cache: false,
			contentType: false,
			processData: false
			});
			return false;
		});
	}

	function nonCar() {
		$('#importFile2').bootstrapFileInput();
		$('#subBox2 button[btnName="car_type_nm2"]').on('click', function(){
			$('#lookupModal').modal('show');
			initLookup('car_type_nm2', "<?php echo e(trans('sysRefFee.carTypeNm')); ?>", callBackFunc=function(data){
				$("#subBox2 input[name='car_type_nm'").val(data.cd_descp);
				$("#subBox2 input[name='car_type_cd'").val(data.cd);
			});
		});
		$('#subBox2 input[name="car_type_nm"]').on('click', function(){
			var check = $('#subBox2 input[name="car_type_nm"]').data('ui-autocomplete') != undefined;
			if(check == false) {
				initAutocomplete("subForm2","car_type_nm2",callBackFunc=function(data){
					$("#subBox2 input[name='car_type_cd'").val(data.cd);
				},"car_type_nm");
			}
		});

		
		mainId = "mainId"
		$.ajax({
			url: "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee')); ?>"+ "?basecon=type;EQUAL;N",					
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				var custFieldData = [];
				custFieldData[0] = [
					{name: 'id', type: 'integer'},
					{name: 'car_type_nm', type: 'string'},
					{name: 'fee_unit', type: 'string'},
					{name: 'fee_from', type: 'float'},
					{name: 'fee_to', type: 'float'},
					{name: 'fee', type: 'float'},
					//{name: 'second_fee', type: 'float'},			
					{name: 'remark', type: 'string'},
				];
				custFieldData[1] = [
					{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false, hidden: true},
					{text:"<?php echo e(trans('sysRefFee.carTypeNm')); ?>", datafield: 'car_type_nm', filtertype:"textbox", width:220, editable: false},
					{text:"<?php echo e(trans('sysRefFee.feeUnit')); ?>", datafield: 'fee_unit', filtertype:"textbox", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.feeFrom')); ?>", datafield: 'fee_from', filtertype:"number", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.feeTo')); ?>", datafield: 'fee_to', filtertype:"number", width:100, editable: false},
					{text:"<?php echo e(trans('sysRefFee.fee')); ?>", datafield: 'fee', filtertype:"number", width:100, editable: false},
					//{text:"<?php echo e(trans('sysRefFee.secondFee')); ?>", datafield: 'second_fee', filtertype:"number", width:100},				
					{text:"<?php echo e(trans('sysRefFee.remark')); ?>", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
				];
				var opt = {};
				opt.gridId = "jqxGrid2";
				opt.fieldData = custFieldData;
				opt.formId = "subForm2";
				opt.saveId = "Save2";
				opt.cancelId = "Cancel2";
				opt.showBoxId = "subBox2";
				opt.height = $( window ).height() - 250;
				opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeNonCar/get')); ?>";
				opt.addUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeNonCar')); ?>" + "/store";
				opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeNonCar')); ?>" + "/update";
				opt.delUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeNonCar')); ?>" + "/delete/";
				opt.commonBtn = true;
				opt.showtoolbar = true;
				opt.defaultKey = {
					
				};
				opt.beforeSave = function (formData) {
					// $addBtn.jqxButton({disabled: false});
					// $delBtn.jqxButton({disabled: false});
					
				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});

		$("#myForm2").submit(function () {
			if ($('#importFile2').get(0).files.length === 0) {
				swal("<?php echo e(trans('sysRefFee.msg1')); ?>", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			$.ajax({
			url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeNonCar/importExcel')); ?>",
			type: 'POST',
			data: postData,
			async: false,
			beforeSend: function () {
				
			},
			error: function () {
				swal("<?php echo e(trans('sysRefFee.msg2')); ?>", "", "error");
				// swal("<?php echo e(trans('excel.msg2')); ?>", {
				// 	icon: "error",
				// });
				return false;
			},
			success: function (data) {
				//alert(data);
				if(data.msg == "error") {
					swal("<?php echo e(trans('sysRefFee.msg2')); ?>", "", "error");
					// swal("<?php echo e(trans('excel.msg2')); ?>", {
					// icon: "error",
					// });
				}else{
					swal("<?php echo e(trans('sysRefFee.msg3')); ?>", "", "success");
				}
			$('#jqxGrid2').jqxGrid('updatebounddata');
			// var dataAdapter = new $.jqx.dataAdapter(data.data);
			// $("#jqxGrid").jqxGrid({ source: dataAdapter });
			
			},
			cache: false,
			contentType: false,
			processData: false
			});
			return false;
		});
	}
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>