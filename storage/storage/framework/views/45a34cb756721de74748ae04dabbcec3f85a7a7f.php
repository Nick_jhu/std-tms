<?php if(Auth::check()): ?>
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="https://placehold.it/160x160/00a65a/ffffff/&text=<?php echo e(mb_substr(Auth::user()->name, 0, 1)); ?>" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p><?php echo e(Auth::user()->name); ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header"><?php echo e(trans('backpack::base.administration')); ?></li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Dashboard')): ?>
          <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/board')); ?>"><i class="fa fa-dashboard"></i> <span><?php echo e(trans('backpack::base.dashboard')); ?></span></a></li>
          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Map')): ?>
          <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/map')); ?>" target="_blank"><i class="fa fa-map"></i> <span><?php echo e(trans('menu.map')); ?></span></a></li>
          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('BasicInfo')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span><?php echo e(trans('menu.basicInfo')); ?></span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/customerProfile')); ?>"><i class="fa fa-group"></i> <span><?php echo e(trans('menu.customerProfile')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/companyProfile')); ?>"><i class="fa fa-building-o"></i> <span><?php echo e(trans('menu.groupSetting')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/bulletin')); ?>"><i class="fa fa-newspaper-o"></i> <span><?php echo e(trans('menu.bulletin')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.bscode')); ?></span></a></li>
              
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/mailFormat')); ?>"><i class="fa fa-envelope"></i> <span><?php echo e(trans('menu.mailFormat')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/example')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.autoCode')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/example')); ?>"><i class="fa fa-info"></i> <span><?php echo e(trans('menu.errorSetting')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/carProfile')); ?>"><i class="fa fa-truck"></i> <span><?php echo e(trans('menu.carProfile')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysCountry')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.sysCountry')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysArea')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.sysArea')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/modTransStatus')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.modTransStatus')); ?></span></a></li>        
            </ul>
          </li>
          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Order')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span><?php echo e(trans('menu.orderBasic')); ?></span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>"><i class="fa fa-list-alt"></i> <span><?php echo e(trans('menu.orderMgmt')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/import')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.orderImport')); ?></span></a></li>              
            </ul>
          </li>     
          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Tracking')): ?>
          <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/tracking')); ?>"><i class="fa fa-map-marker"></i> <span><?php echo e(trans('menu.tracking')); ?></span></a></li>
          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('TransportationPlan')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span><?php echo e(trans('menu.transportationPlan')); ?></span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/DlvCar')); ?>"><i class="fa fa-truck"></i> <span><?php echo e(trans('menu.sendCar')); ?>1</span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar')); ?>"><i class="fa fa-truck"></i> <span><?php echo e(trans('menu.sendCar')); ?>2</span></a></li> 
            </ul>
          </li>  
          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Quote')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span><?php echo e(trans('menu.quotMgmt')); ?></span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeCar')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.sysRefFeeCar')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeNonCar')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.sysRefFeeNonCar')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.sysRefFeeDelivery')); ?></span></a></li>      
            </ul>
          </li>
          <?php endif; ?>
          
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('setting')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span><?php echo e(trans('menu.sysSetting')); ?></span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/apiMgmt')); ?>"><i class="fa fa-files-o"></i> <span>API Mgmt.</span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/elfinder')); ?>"><i class="fa fa-files-o"></i> <span><?php echo e(trans('menu.fileMgmt')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/language')); ?>"><i class="fa fa-flag-o"></i> <span><?php echo e(trans('menu.langSetting')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/language/texts')); ?>"><i class="fa fa-language"></i> <span><?php echo e(trans('menu.langFile')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/backup')); ?>"><i class="fa fa-hdd-o"></i> <span><?php echo e(trans('menu.backup')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/log')); ?>"><i class="fa fa-terminal"></i> <span><?php echo e(trans('menu.logs')); ?></span></a></li>              
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/setting')); ?>"><i class="fa fa-cog"></i> <span><?php echo e(trans('menu.settings')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/page')); ?>"><i class="fa fa-file-o"></i> <span><?php echo e(trans('menu.pages')); ?></span></a></li>
              <li><a href="<?php echo e(url('admin/menu-item')); ?>"><i class="fa fa-list"></i> <span>Menu</span></a></li>              
            </ul>
          </li>
          <?php endif; ?>
          <!-- Users, Roles Permissions -->
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Permissions')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span><?php echo e(trans('menu.permissions')); ?></span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/user')); ?>"><i class="fa fa-user"></i> <span><?php echo e(trans('menu.users')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/role')); ?>"><i class="fa fa-group"></i> <span><?php echo e(trans('menu.roles')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/permission')); ?>"><i class="fa fa-key"></i> <span><?php echo e(trans('menu.permission')); ?></span></a></li>              
            </ul>
          </li>
          <?php endif; ?>
          <!-- ======================================= -->
          <li class="header"><?php echo e(trans('backpack::base.user')); ?></li>
          <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/logout')); ?>"><i class="fa fa-sign-out"></i> <span><?php echo e(trans('backpack::base.logout')); ?></span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
<?php endif; ?>
