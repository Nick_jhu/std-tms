 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>		
<?php echo e(trans('modQuot.buildQuote')); ?><small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(url(config('backpack.base.route_prefix'),'QuotMgmt')); ?>"><?php echo e(trans('modQuot.titleName')); ?></a></li>
		<li class="active"><?php echo e(trans('modQuot.buildQuote')); ?></li>
	</ol>
</section>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('content'); ?>

	<div class="row">
		<?php echo $__env->make('backpack::template.toolbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="col-md-10">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?php echo e(trans('modQuot.basicInfo')); ?></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">

							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-4">
										<label for="quot_no"><?php echo e(trans('modQuot.quotNo')); ?></label>
										<input type="text" class="form-control" id="quot_no" name="quot_no">
									</div>
                                    <div class="form-group col-md-4">
										<label for="quot_date"><?php echo e(trans('modQuot.quotDate')); ?></label>
										
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" id="quot_date" name="quot_date">
                                        </div>
									</div>
									<div class="form-group col-md-4">
										<label for="status"><?php echo e(trans('modQuot.status')); ?></label>
										<select class="form-control" id="status" name="status">
                                            <option value="UNTREATED"><?php echo e(trans('modQuot.setUp')); ?></option>
                                            <option value="SEND"><?php echo e(trans('modQuot.pendingReview')); ?></option>
                                            <option value="LOADING"><?php echo e(trans('modQuot.audited')); ?></option>
                                            <option value="SETOFF"><?php echo e(trans('modQuot.void')); ?></option>
                                        </select>
									</div>
								</div>

								<div class="row">
									<div class="form-group col-md-4">
										<label for="effect_from"><?php echo e(trans('modQuot.effectFrom')); ?></label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" id="effect_from" name="effect_from">
                                        </div>
									</div>
									<div class="form-group col-md-4">
										<label for="effect_to"><?php echo e(trans('modQuot.effectTo')); ?></label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" id="effect_to" name="effect_to">
                                        </div>
									</div>
									<div class="form-group col-md-4">
										<label for="cur"><?php echo e(trans('modQuot.cur')); ?></label>
										<input type="text" class="form-control" id="cur" name="cur">
									</div>
								</div>

								<div class="row">
									<div class="form-group col-md-4">
										<label for="cust_no"><?php echo e(trans('modQuot.custNo')); ?></label>
										<input type="text" class="form-control" id="cust_no" name="cust_no">
									</div>
									<div class="form-group col-md-4">
										<label for="cust_nm"><?php echo e(trans('modQuot.custNm')); ?></label>
										<input type="text" class="form-control" id="cust_nm" name="cust_nm">
									</div>
								</div>

                                <div class="row">
									<div class="form-group col-md-12">
										<label for="remark"><?php echo e(trans('modQuot.remark')); ?></label>
										<textarea class="form-control" rows="3" name="remark"></textarea>
									</div>
								</div>


								<?php if(isset($id)): ?>
								<input type="hidden" name="id" value="<?php echo e($id); ?>" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								<?php endif; ?>

							</div>
						</div>
						<!-- /.tab-pane -->
					</div>
					<!-- /.tab-content -->
				</div>
			</form>
			<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_3" data-toggle="tab" aria-expanded="false"><?php echo e(trans('modQuot.quotDetail')); ?></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_3">
							<div class="box box-primary" id="subBox" style="display:none">
								<div class="box-header with-border">
									<h3 class="box-title"><?php echo e(trans('modQuot.quotDetail')); ?></h3>
								</div>
								<form method="POST" accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
									<!-- /.box-header -->
									<div class="box-body">
										<div class="row">
											<div class="form-group col-md-3">
												<label for="goods_no"><?php echo e(trans('modOrderDetail.goodsNo')); ?></label>
												<input type="text" class="form-control input-sm" name="goods_no" grid="true" >
											</div>
											<div class="form-group col-md-3">
												<label for="goods_nm"><?php echo e(trans('modOrderDetail.goodsNm')); ?></label>
												<input type="text" class="form-control input-sm" name="goods_nm" grid="true" >
											</div>
											<div class="form-group col-md-3">
												<label for="pkg_num"><?php echo e(trans('modOrderDetail.pkgNum')); ?></label>
												<input type="text" class="form-control input-sm" name="pkg_num" grid="true" >
											</div>
											<div class="form-group col-md-3">
												<label for="pkg_unit"><?php echo e(trans('modOrderDetail.pkgUnit')); ?></label>
												<input type="text" class="form-control input-sm" name="pkg_unit" grid="true" >
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-3">
												<label for="gw"><?php echo e(trans('modOrderDetail.gw')); ?></label>
												<input type="text" class="form-control input-sm" name="gw" grid="true" >
											</div>
											<div class="form-group col-md-3">
												<label for="gwu"><?php echo e(trans('modOrderDetail.gwu')); ?></label>
												<input type="text" class="form-control input-sm" name="gwu" grid="true" >
											</div>
											<div class="form-group col-md-3">
												<label for="cbm"><?php echo e(trans('modOrderDetail.cbm')); ?></label>
												<input type="text" class="form-control input-sm" name="cbm" grid="true" >
											</div>
											<div class="form-group col-md-3">
												<label for="cbmu"><?php echo e(trans('modOrderDetail.cbmu')); ?></label>
												<input type="text" class="form-control input-sm" name="cbmu" grid="true" >
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-3">
												<label for="length"><?php echo e(trans('modOrderDetail.length')); ?></label>
												<input type="text" class="form-control input-sm" name="length" grid="true" >
											</div>
											<div class="form-group col-md-3">
												<label for="weight"><?php echo e(trans('modOrderDetail.weight')); ?></label>
												<input type="text" class="form-control input-sm" name="weight" grid="true" >
											</div>
											<div class="form-group col-md-3">
												<label for="height"><?php echo e(trans('modOrderDetail.height')); ?></label>
												<input type="text" class="form-control input-sm" name="height" grid="true" >
											</div>
										</div>
									</div>
									<!-- /.box-body -->

									<div class="box-footer">
										<input type="hidden" class="form-control input-sm" name="id" grid="true">
										<button type="button" class="btn btn-sm btn-primary" id="Save"><?php echo e(trans('common.save')); ?></button>
										<button type="button" class="btn btn-sm btn-danger" id="Cancel"><?php echo e(trans('common.cancel')); ?></button>
									</div>
								</form>
							</div>
							<button type="button" class="btn btn-sm btn-primary" id="uploadBtn"><?php echo e(trans('common.excelImport')); ?></button>
							<div id="jqxGrid"></div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
		</div>	
	</div>

<div class="modal fade" id="excelModal" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span></button>
			<h4 class="modal-title"><?php echo e(trans('common.excelImport')); ?></h4>
			</div>
			<div class="modal-body">
				<input type="file" id="exampleInputFile">
			</div>
			<div class="modal-footer">
			<button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo e(trans('common.close')); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

 <?php $__env->stopSection(); ?> 
 <?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
 <?php $__env->startSection('after_scripts'); ?>
	<script>
		var mainId = "";
		var quot_no = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/QuotMgmt')); ?>";

		var fieldData = null;
		var fieldObj = null;

		<?php if(isset($crud -> create_fields)): ?>
		fieldData = '{<?php echo json_encode($crud->create_fields); ?>}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		console.log(fieldObj);
		<?php endif; ?>



		<?php if(isset($id)): ?>
		mainId = "<?php echo e($id); ?>";
		editData = '{<?php echo $entry; ?>}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
		editObj = JSON.parse(editData);
		quot_no = editObj.quot_no;
		console.log(editObj);
		<?php endif; ?>




		$(function () {

			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/QuotMgmt')); ?>";
			formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_quot')); ?>";
			formOpt.saveUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/QuotMgmt')); ?>";

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {
				$('#jqxGrid').jqxGrid('clear');
			}

			formOpt.addFunc = function() {
				$('#jqxGrid').jqxGrid('clear');
			}

			var btnGroup = [];

			initBtn(btnGroup);

            var fieldData = [];
            fieldData[0] = [
				{name: 'pol_nm', type: 'string'},
				{name: 'pod_nm', type: 'string'},
				{name: 'chg_cd', type: 'string'},
				{name: 'car_type', type: 'string'},
				{name: 'f1', type: 'number'},
				{name: 'f2', type: 'number'},
				{name: 'f3', type: 'number'},
				{name: 'f4', type: 'number'},
				{name: 'f5', type: 'number'},
				{name: 'f6', type: 'number'},
				{name: 'f7', type: 'number'},
			];
			fieldData[1] = [
				{text:"<?php echo e(trans('modQuot.departure')); ?>", datafield: 'pol_nm', filtertype:"textbox", width:150},
				{text:"<?php echo e(trans('modQuot.destination')); ?>", datafield: 'pod_nm', filtertype:"textbox", width:150},
				{text:"<?php echo e(trans('modQuot.charges')); ?>", datafield: 'chg_cd', filtertype:"textbox", width:100},
				{text:"<?php echo e(trans('modQuot.vehicles')); ?>", datafield: 'car_type', filtertype:"textbox", width:100},
				{text:"<?php echo e(trans('modQuot.minimumCharge')); ?>", datafield: 'f1', filtertype:"number", width:100},
				{text:"<?php echo e(trans('modQuot.inKilos')); ?>", datafield: 'f2', filtertype:"number", width:100},
				{text:"<?php echo e(trans('modQuot.byCbm')); ?>", datafield: 'f3', filtertype:"number", width:100},
				{text:"<?php echo e(trans('modQuot.truck1')); ?>", datafield: 'f4', filtertype:"number", width:100},
				{text:"<?php echo e(trans('modQuot.truck2')); ?>", datafield: 'f5', filtertype:"number", width:100},
				{text:"<?php echo e(trans('modQuot.truck3')); ?>", datafield: 'f6', filtertype:"number", width:100},
				{text:"<?php echo e(trans('modQuot.truck4')); ?>", datafield: 'f7', filtertype:"number", width:100},
			];

			var opt = {};
            opt.gridId      = "jqxGrid";
            opt.fieldData   = fieldData;
            opt.formId      = "subForm";
            opt.saveId      = "Save";
            opt.cancelId    = "Cancel";
            opt.showBoxId   = "subBox";
            opt.height      = 300;
            opt.getUrl      = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/orderDetail/get')); ?>" + '/' + quot_no;
            opt.addUrl      = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderDetail')); ?>" + "/store";
            opt.updateUrl   = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderDetail')); ?>" + "/update";
            opt.delUrl      = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderDetail')); ?>" + "/delete/";
            opt.commonBtn   = true;
            opt.showtoolbar = true;
            opt.defaultKey  = {
                'quot_no'    : quot_no
            };
            opt.beforeSave  = function (formData) {

            }

            opt.afterSave   = function (data) {

            }

            genDetailGrid(opt);

			$("#uploadBtn").click(function(){
				$("#excelModal").modal("show");
			});

			setField.disabled('mainForm', ['cust_nm']);
		})
	</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>