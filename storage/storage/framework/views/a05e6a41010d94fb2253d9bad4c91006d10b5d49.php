

<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
      <?php echo e(trans('dlvCar.titleName')); ?><small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url(config('backpack.base.route_prefix'),'dashboard')); ?>"><?php echo e(trans('backpack::crud.admin')); ?></a></li>
        <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/log')); ?>"><?php echo e(trans('dlvCar.tranPlan')); ?></a></li>
        <li class="active"><?php echo e(trans('dlvCar.deliveryWork')); ?></li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-md-9">
        <div id="jqxGrid"></div>
    </div>
    <div class="col-md-3">
        <div class="box">
            <div class="box-body">
                <form action="" id="dlvForm">
                    <p><?php echo e(trans('dlvCar.carCapacity')); ?>：<span id="carCbm">0</span>&nbsp;&nbsp;&nbsp;<?php echo e(trans('dlvCar.selected')); ?>：<span id="gridCbm">0</span></p>
                    <p id="warningText" style="color:red"></p>
                    <p><?php echo e(trans('dlvCar.loadingRate')); ?>：<span id="loadRate">0%</span>&nbsp;&nbsp;&nbsp;<?php echo e(trans('dlvCar.selectedRate')); ?>：<span id="gridLoadRate">0%</span></p>
                    <div class="form-group">
                        <label for="dlv_date"><?php echo e(trans('dlvCar.sendCarDate')); ?></label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="dlv_date" name="dlv_date">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cust_nm"><?php echo e(trans('dlvCar.carDealer')); ?></label>
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" id="cust_nm" name="cust_nm" <?php if(isset($carData)): ?> value="<?php echo e($carData->cname); ?>" <?php endif; ?>>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-flat lookup" btnname="cust_nm"
                                info1="<?php echo e(Crypt::encrypt('sys_customers')); ?>" 
                                info2="<?php echo e(Crypt::encrypt('cust_no+cname,cust_no,cname')); ?>" 
                                info3="<?php echo e(Crypt::encrypt('status=\'B\'')); ?>"
                                info4="cust_no=cust_no;cname=cust_nm" triggerfunc="" selectionmode="singlerow">
                                <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                        <input type="hidden" class="form-control" id="cust_no" name="cust_no" <?php if(isset($carData)): ?> value="<?php echo e($carData->cust_no); ?>" <?php endif; ?>>
                    </div>
                    <div class="form-group">
                        <label for="car_no"><?php echo e(trans('dlvCar.carNo')); ?></label>
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" id="car_no" name="car_no">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-flat lookup" btnname="car_no"
                                info1="<?php echo e(Crypt::encrypt('mod_car')); ?>" 
                                info2="<?php echo e(Crypt::encrypt('car_no,car_no,car_type,car_type_nm,cbm,load_rate,driver_nm')); ?>" 
                                info3="<?php echo e(Crypt::encrypt('')); ?>"
                                info4="car_no=car_no;car_type=car_type;load_rate=load_rate;driver_nm=driver_nm;cbm=cbm" triggerfunc="" selectionmode="singlerow">
                                <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                        
                        <input type="hidden" class="form-control" id="load_rate" name="load_rate">
                        <input type="hidden" class="form-control" id="cbm" name="cbm">
                    </div>
                    <div class="form-group">
                        <label for="car_type"><?php echo e(trans('dlvCar.carType')); ?></label>
                        <input type="text" class="form-control" id="car_type" disabled="disabled">
                    </div>
                    <div class="form-group">
                        <label for="driver_nm"><?php echo e(trans('dlvCar.driver')); ?></label>
                        <input type="text" class="form-control" id="driver_nm">
                    </div>
                    <div class="form-group">
                        <label for="driver_phone"><?php echo e(trans('dlvCar.phone')); ?></label>
                        <input type="text" class="form-control" id="driver_phone">
                    </div>
                    <button type="button" class="btn btn-primary" id="confirmBtn"><?php echo e(trans('dlvCar.confirmSendCar')); ?></button>
                    <button type="button" class="btn btn-danger" id="emptyBtn"><?php echo e(trans('dlvCar.clear')); ?></button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->startSection('after_scripts'); ?>

<script type="text/javascript" src="<?php echo e(asset('vendor/jqwidgets')); ?>/jqxwindow.js"></script>


<script>
    $(function(){
        $('button[btnName="car_no"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('car_no', "<?php echo e(trans('dlvCar.car')); ?>", callBackFunc=function(data){
                $("#carCbm").text(data.cbm);
                $("#loadRate").text((data.load_rate * 100) + '%');
            });
        });
        $('#car_no').on('click', function(){
            var check = $('#car_no').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("dlvForm","car_no",callBackFunc=function(data){
                    $("#carCbm").text(data.cbm);
                    $("#loadRate").text((data.load_rate * 100) + '%');
                });
            }
        });

        $('button[btnName="cust_nm"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('cust_nm', "<?php echo e(trans('dlvCar.carDealer')); ?>");
        });

        $('#cust_nm').on('click', function(){
            var check = $('#cust_nm').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("dlvForm","cust_nm");
            }
        });

        

        $.jqx.theme = "bootstrap";
        var items = new Array();

        var statusCode = [
            {value: "UNTREATED", label: "<?php echo e(trans('modOrder.STATUS_UNTREATED')); ?>"}
        ]

        var statusSource =
            {
                 datatype: "array",
                 datafields: [
                     { name: 'label', type: 'string' },
                     { name: 'value', type: 'string' }
                 ],
                 localdata: statusCode
            };

            var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                autoBind: true
            });


        var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'id', type: 'number' },
                    { name: 'status', type: 'string', value: 'status', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
                    { name: 'etd', type: 'date', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
                    { name: 'ord_no', type: 'string' },
                    { name: 'total_cbm', type: 'float' },
                    { name: 'pkg_num', type: 'float' },
                    { name: 'dlv_attn', type: 'string' },
                    //{ name: 'dlv_city_nm', type: 'string' },
                    //{ name: 'dlv_area_nm', type: 'string' },
                    { name: 'pick_addr', type: 'string' },
                    { name: 'dlv_addr', type: 'string' },
                    { name: 'dlv_tel', type: 'number' },
                    { name: 'remark', type: 'string' },                
                ],
                root:"Rows",
                pagenum: 1,					
                beforeprocessing: function (data) {
                    var ostatus = "";
                    for(i in data[0].Rows) {
                        if(i == 0) {
                            ostatus = data[0].Rows[i].status;
                            items.push(data[0].Rows[i].status);
                        }

                        if(ostatus != data[0].Rows[i].status) {
                            items.push(data[0].Rows[i].status);
                        }

                        ostatus = data[0].Rows[i].status;
                    }
                },
                filter: function () {
                // update the grid and send a request to the server.
                    $("#jqxGrid").jqxGrid('updatebounddata', 'filter');
                },
                pagesize: 200,
                sort: function () {
                    // update the grid and send a request to the server.
                    $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
                },
                url: "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order')); ?>" + "?basecon=status;EQUAL;UNTREATED",					
            };
        /*var dataAdapter = new $.jqx.dataAdapter(source, {
            loadComplete: function () {

            }
        });*/
        var dataAdapter = new $.jqx.dataAdapter(source);
        $("#jqxGrid").jqxGrid(
        {
            width: '100%',
            height: $(".content-wrapper").height() - 50,
            source: dataAdapter,
            selectionmode: 'checkbox',
            columnsresize: true,
            sortable: true,
            filterable: true,
            altrows: true,
            showfilterrow: true,
            pagesize: 1000,
            enablebrowserselection: true,
            ready: function () {
                $('#jqxGrid').jqxGrid('autoresizecolumns');
            },
            columns: [
                { text: "id", datafield: 'id', width: 50, hidden:true },
                { text: "<?php echo e(trans('modOrder.status')); ?>", datafield: 'status', width: '120', filtertype: 'list', filteritems: items },
                { text: "<?php echo e(trans('modOrder.etd')); ?>", datafield: 'etd', width: 100,cellsformat: 'yyyy-MM-dd',filtertype: 'date' },
                { text: "<?php echo e(trans('modOrder.ordNo')); ?>", datafield: 'ord_no', width:120 },
                { text: "<?php echo e(trans('modOrder.totalCbm')); ?>", datafield: 'total_cbm', width: 70, cellsalign: 'right', filtertype: 'number' },
                { text: "<?php echo e(trans('modOrder.pkgNum')); ?>", datafield: 'pkg_num', width: 70, cellsalign: 'right', filtertype: 'number' },
                { text: "<?php echo e(trans('modOrder.dlvAttn')); ?>", datafield: 'dlv_attn', width: 100 },
                //{ text: "<?php echo e(trans('modOrder.dlvCityNm')); ?>", datafield: 'dlv_city_nm', width: 100 },
                //{ text: "<?php echo e(trans('modOrder.dlvAreaNm')); ?>", datafield: 'dlv_area_nm', width: 100 },
                { text: "<?php echo e(trans('modOrder.pickAddr')); ?>", datafield: 'pick_addr', width: 300 },
                { text: "<?php echo e(trans('modOrder.dlvAddr')); ?>", datafield: 'dlv_addr', width: 300 },
                { text: "<?php echo e(trans('modOrder.dlvTel')); ?>", datafield: 'dlv_tel', width: 120, cellsalign: 'right' },
                { text: "<?php echo e(trans('modOrder.remark')); ?>", datafield: 'remark', width: 300 },
            ]
        });

        var chkLoadRate = function(gridCbm) {
            var carCbm = parseInt($("#cbm").val());
            var loadRate = parseFloat($("#load_rate").val());

            var carLoadable = carCbm * loadRate;
            if(carCbm > 0) {
                if(gridCbm > carCbm) {
                    return -1;
                }
                else if(gridCbm > carLoadable) {
                    return 0;
                }
            }

            return 1;
        }

        $("#jqxGrid").on("rowselect", function (event) {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            if(chkLoadRate(sum) == 0) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("");
            }
            else if(chkLoadRate(sum) == -1) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("<?php echo e(trans('dlvCar.msg1')); ?>");
            }
            else {
                $("#gridCbm").css('color', '#000');
                $("#warningText").text("");
            }

            var cbm          = parseFloat($("#cbm").val());
            var loadRate     = parseFloat($("#load_rate").val());
            if(cbm) {
                var gridLoadRate = sum / cbm;
                $("#gridLoadRate").text((Math.round(gridLoadRate * 100)) + "%");

                if(gridLoadRate > loadRate) {
                    $("#gridLoadRate").css('color', 'red');
                }
                else {
                    $("#gridLoadRate").css('color', '#000');
                }
            }
            

            $("#gridCbm").text(sum);
        });  

        $('#jqxGrid').on('rowunselect', function (event) 
        {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            if(chkLoadRate(sum) == 0) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("");
            }
            else if(chkLoadRate(sum) == -1) {
                $("#gridCbm").css('color', 'red');
                $("#warningText").text("<?php echo e(trans('dlvCar.msg1')); ?>");
            }
            else {
                $("#gridCbm").css('color', '#000');
                $("#warningText").text("");
            }

            var cbm          = parseFloat($("#cbm").val());
            var loadRate     = parseFloat($("#load_rate").val());
            if(cbm) {
                var gridLoadRate = sum / cbm;
                $("#gridLoadRate").text((Math.round(gridLoadRate * 100)) + "%");

                if(gridLoadRate > loadRate) {
                    $("#gridLoadRate").css('color', 'red');
                }
                else {
                    $("#gridLoadRate").css('color', '#000');
                }
            }
            

            $("#gridCbm").text(sum);
        });

        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        $('#dlv_date').val(date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate());
        $('#dlv_date').datepicker({
            startDate: today,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $("#confirmBtn").on("click", function(){
            var gridCbm = getGridCbm();
            if(chkLoadRate(gridCbm) == 0 || chkLoadRate(gridCbm) == -1) {
                swal({
                    text: "<?php echo e(trans('dlvCar.msg2')); ?>",
                    title: "<?php echo e(trans('dlvCar.msg3')); ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: "<?php echo e(trans('dlvCar.cancel')); ?>",
                    confirmButtonText: "<?php echo e(trans('dlvCar.yes')); ?>"
                    }).then(function (result) {
                    if (result.value) {
                        sendDlvCar();
                    }
                })
            }
            else {
                sendDlvCar();
            }
        });

        var sendDlvCar = function() {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var dlvData = [];
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                dlvData.push(row.id);
            }

            var postData = {
                car_no      : $("#car_no").val(),
                driver_nm   : $("#driver_nm").val(),
                driver_phone: $("#driver_phone").val(),
                dlv_date    : $("#dlv_date").val(),
                ord_ids     : dlvData
            }
            console.log(postData);

            $.ajax({
                url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/TranPlanMgmt/sendDlvCar')); ?>",
                type: 'POST',
                async: false,
                data: postData,
                beforeSend: function () {
                    loadingFunc.show();
                },
                error: function (jqXHR, exception) {
                    swal('Oops...', "<?php echo e(trans('modOrder.error')); ?>", "error");
                    loadingFunc.hide();
                    return;
                },
                success: function (data) {
                    if(data.msg == "success") {
                        swal('Success', "<?php echo e(trans('modOrder.sendSuccess')); ?>", "success");
                        
                        $('#jqxGrid').jqxGrid('clearselection');
                        $("#jqxGrid").jqxGrid('clearfilters');
                    }
                    else {
                        swal('Oops...', "<?php echo e(trans('modOrder.error')); ?>", "error");
                    }
                    loadingFunc.hide();
                    $('#jqxGrid').jqxGrid('updatebounddata');
                    return;
                }
            });
        }

        var getGridCbm = function() {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            return sum;
        }

        $("#emptyBtn").on("click", function(){
            document.getElementById('dlvForm').reset();
            $("#carCbm").text(0);
            $("#loadRate").text(0);
        })

        $("#car_no").on("change", function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var selectedRecords = new Array();
            var sum = 0;
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    sum += row.total_cbm;
                    
                }
            }

            if(!chkLoadRate(sum)) {
                $("#gridCbm").css('color', 'red');
            }
            else {
                $("#gridCbm").css('color', '#000');
            }

            var cbm          = parseFloat($("#cbm").val());
            var loadRate     = parseFloat($("#load_rate").val());
            var gridLoadRate = sum / cbm;
            $("#gridLoadRate").text((Math.round(gridLoadRate * 100)) + "%");

            if(gridLoadRate > loadRate) {
                $("#gridLoadRate").css('color', 'red');
            }
            else {
                $("#gridLoadRate").css('color', '#000');
            }
        });
    });
</script>    

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>