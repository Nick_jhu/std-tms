

<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
      <?php echo e(trans('dashboard.titleName')); ?><small></small>
      </h1>
      <ol class="breadcrumb">
            <li class="active"><?php echo e(trans('dashboard.titleName')); ?></li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.css">
    <style>
      #map_canvas {
          height: 500px;
          width: 100%
      }
      
      #map_canvas img {
          max-width: none;
      }
      
      #map_canvas div {
          -webkit-transform: translate3d(0, 0, 0);
      }

      .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

</style>
<?php $__env->startSection('content'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="<?php echo e(asset('vendor/ejs')); ?>/ejs.min.js"></script>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-list-alt" style="margin-top: 17px"></i></span>

        <div class="info-box-content">
            <span class="info-box-text"><?php echo e(trans('dashboard.order')); ?></span>
            <span class="info-box-number"><span id="ord">80</span><small>%</small></span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-exclamation-triangle" style="margin-top: 17px"></i></span>

        <div class="info-box-content">
            <span class="info-box-text"><?php echo e(trans('dashboard.error')); ?></span>
            <span class="info-box-number" id="errorNum">5</span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-percent" style="margin-top: 17px"></i></span>

        <div class="info-box-content">
            <span class="info-box-text"><?php echo e(trans('dashboard.rate')); ?></span>
            <span class="info-box-number"><span id="ord1">95</span>%</span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-clock-o" style="margin-top: 17px"></i></span>

        <div class="info-box-content">
            <span class="info-box-text"><?php echo e(trans('dashboard.eta')); ?></span>
            <span class="info-box-number">19:05</span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo e(trans('dashboard.map')); ?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div id="map_canvas" style="position: relative; overflow: hidden;"></div>

            </div>
        </div>
    </div>
</div>



<script id="transports-template" type="x-ejs">
<% transports.forEach(function(transport) { %>
<div class="mdl-card" style="background:rgb(250, <%= Math.round(transport.power * 2.5 ) %>, <%= Math.round(transport.power * 2.5 ) %>)">
  <div class="mdl-card__title">
    <h3 class="mdl-card__title-text"><%= transport.id %></h3>
  </div>
  <div class="mdl-card__actions">
  <img src="<%= transport.map %>">
  <ul>
    <!--<li><label>Location:</label><%= transport.lat %>,<%= transport.lng %></li>-->
    <li><label><?php echo e(trans('dashboard.battery')); ?>:</label><%= transport.power %>%</li>
    <li><label><?php echo e(trans('dashboard.speed')); ?>:</label><%= transport.speed %>km/h</li>
    <li><label><?php echo e(trans('dashboard.updated')); ?>:</label><%= transport.time %></li>
  </ul>
  </div>
</div>
<% }) %>
</script>


<div class="row">
    <div class="col-md-12">
        <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo e(trans('dashboard.monthly')); ?></h3>

            <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <div class="btn-group">
                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-wrench"></i></button>
                <ul class="dropdown-menu" role="menu">
                <li><a href="#"><?php echo e(trans('dashboard.action')); ?></a></li>
                <li><a href="#"><?php echo e(trans('dashboard.anotherAction')); ?></a></li>
                <li><a href="#"><?php echo e(trans('dashboard.somethingElseHere')); ?></a></li>
                <li class="divider"></li>
                <li><a href="#"><?php echo e(trans('dashboard.separatedLink')); ?></a></li>
                </ul>
            </div>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
            <div class="col-md-8">
                <p class="text-center">
                <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                </p>

                <div class="chart">
                <!-- Sales Chart Canvas -->
                <canvas id="salesChart" style="height: 180px; width: 1072px;" width="1072" height="180"></canvas>
                </div>
                <!-- /.chart-responsive -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
                <p class="text-center">
                <strong><?php echo e(trans('dashboard.goalCompletion')); ?></strong>
                </p>

                <div class="progress-group">
                <span class="progress-text"><?php echo e(trans('dashboard.addProducts')); ?></span>
                <span class="progress-number"><b>160</b>/200</span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                <span class="progress-text"><?php echo e(trans('dashboard.completePurchase')); ?></span>
                <span class="progress-number"><b>310</b>/400</span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                <span class="progress-text"><?php echo e(trans('dashboard.visitPremium')); ?></span>
                <span class="progress-number"><b>480</b>/800</span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                <span class="progress-text"><?php echo e(trans('dashboard.sendInquiries')); ?></span>
                <span class="progress-number"><b>250</b>/500</span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                </div>
                </div>
                <!-- /.progress-group -->
            </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- ./box-body -->
        <div class="box-footer">
            <div class="row">
            <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                <h5 class="description-header">$35,210.43</h5>
                <span class="description-text"><?php echo e(trans('dashboard.totalRevenue')); ?></span>
                </div>
                <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                <h5 class="description-header">$10,390.90</h5>
                <span class="description-text"><?php echo e(trans('dashboard.totalCost')); ?></span>
                </div>
                <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                <h5 class="description-header">$24,813.53</h5>
                <span class="description-text"><?php echo e(trans('dashboard.totalProfit')); ?></span>
                </div>
                <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
                <div class="description-block">
                <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                <h5 class="description-header">1200</h5>
                <span class="description-text"><?php echo e(trans('dashboard.goalCompletions')); ?></span>
                </div>
                <!-- /.description-block -->
            </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

<style>
    .chat-room{
        position: fixed;
        bottom: 0px;
        right: 0px;
    }
</style>




<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
<script src="<?php echo e(asset('vendor/adminlte/plugins/chartjs')); ?>/Chart.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ79gLT1qaLfNJDf98OJr8cT5FZ4MJ4_w&libraries=geometry,places&ext=.js"></script>
<script type="text/javascript" src="https://aishek.github.io/jquery-animateNumber/javascripts/jquery.animateNumber.js"></script>
<script>
    $(function(){
        doPollForOrd();
        doPollForError();
        doPollForOrd1();
        function doPollForOrd(){
            var num = Math.floor((Math.random() * (99-80+1)) + 80);
            $('#ord').animateNumber({ number: num });
            setTimeout(doPollForOrd,5000);
        }

        function doPollForError(){
            var num = Math.floor((Math.random() * (5-1+1)) +1);
            $('#errorNum').animateNumber({ number: num });
            setTimeout(doPollForError,10000);
        }

        function doPollForOrd1(){
            var num = Math.floor((Math.random() * (99-90+1)) + 90);
            $('#ord1').animateNumber({ number: num });
            setTimeout(doPollForOrd1,5000);
        }
    });

    $(function(){
        // Get context with jQuery - using jQuery's .get() method.
        var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
        // This will get the first returned node in the jQuery collection.
        var salesChart       = new Chart(salesChartCanvas);

        var salesChartData = {
            labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
            {
                label               : 'Electronics',
                fillColor           : 'rgb(210, 214, 222)',
                strokeColor         : 'rgb(210, 214, 222)',
                pointColor          : 'rgb(210, 214, 222)',
                pointStrokeColor    : '#c1c7d1',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgb(220,220,220)',
                data                : [65, 59, 80, 81, 56, 55, 40]
            },
            {
                label               : 'Digital Goods',
                fillColor           : 'rgba(60,141,188,0.9)',
                strokeColor         : 'rgba(60,141,188,0.8)',
                pointColor          : '#3b8bba',
                pointStrokeColor    : 'rgba(60,141,188,1)',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data                : [28, 48, 40, 19, 86, 27, 90]
            }
            ]
        };

        var salesChartOptions = {
            // Boolean - If we should show the scale at all
            showScale               : true,
            // Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines      : false,
            // String - Colour of the grid lines
            scaleGridLineColor      : 'rgba(0,0,0,.05)',
            // Number - Width of the grid lines
            scaleGridLineWidth      : 1,
            // Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            // Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines  : true,
            // Boolean - Whether the line is curved between points
            bezierCurve             : true,
            // Number - Tension of the bezier curve between points
            bezierCurveTension      : 0.3,
            // Boolean - Whether to show a dot for each point
            pointDot                : false,
            // Number - Radius of each point dot in pixels
            pointDotRadius          : 4,
            // Number - Pixel width of point dot stroke
            pointDotStrokeWidth     : 1,
            // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,
            // Boolean - Whether to show a stroke for datasets
            datasetStroke           : true,
            // Number - Pixel width of dataset stroke
            datasetStrokeWidth      : 2,
            // Boolean - Whether to fill the dataset with a color
            datasetFill             : true,
            // String - A legend template
            legendTemplate          : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio     : true,
            // Boolean - whether to make the chart responsive to window resizing
            responsive              : true
        };

        // Create the line chart
        salesChart.Line(salesChartData, salesChartOptions);

        // ---------------------------
        // - END MONTHLY SALES CHART -
        // --------------------------- 
    });
</script>
<script src="https://unpkg.com/vue"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.0/firebase.js"></script>
<script>
    $(function() {

        $("#fileUpload").on("click",function(event){
            //event.stopPropagation()
            $('#myInput')[0].click();
        });    
    
    });
    

    var map;
    var directionDisplay;
    var directionsService;
    var stepDisplay;
    var markerArray = [];
    var position;
    var marker = null;
    var polyline = null;
    var poly2 = null;
    var speed = 0.000005,
        wait = 1;
    var infowindow = null;
    var timerHandle = null;

    infowindow = new google.maps.InfoWindow({
        size: new google.maps.Size(150, 50)
    });
    // Instantiate a directions service.
    directionsService = new google.maps.DirectionsService();

    // Create a map and center it on Manhattan.
    var myOptions = {
        center:new google.maps.LatLng(25.041591, 121.538248),
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    var car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";
    var icon = {
        path: car,
        scale: .7,
        strokeColor: 'white',
        strokeWeight: .10,
        fillOpacity: 1,
        fillColor: '#404040',
        offset: '5%',
        rotation: 90,
        anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
    };

    var infowindow = new google.maps.InfoWindow({
        content: '<table class="table table-bordered"><tr><td colspan="2">Car Info.</td></tr><tr><td>車況</td><td>良好</td></tr><tr><tr><td>latitude</td><td>25.041591</td></tr><tr><td>longitude</td><td>121.538248</td></tr></table>'
    });


    $(function(){
        


        // marker = new google.maps.Marker({
        //     position: new google.maps.LatLng(25.041591, 121.538248),
        //     map: map,
        //     icon: icon
        // });

        // marker.addListener('click', function() {
        //     infowindow.open(map, marker);
        // });

        // var icon = {
        //     path: car,
        //     scale: .7,
        //     strokeColor: 'white',
        //     strokeWeight: .10,
        //     fillOpacity: 1,
        //     fillColor: '#404040',
        //     offset: '5%',
        //     rotation: 270,
        //     anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
        // };

        // var marker1 = new google.maps.Marker({
        //     position: new google.maps.LatLng(25.048162, 121.549448),
        //     map: map,
        //     icon: icon
        // });


        // var icon = {
        //     path: car,
        //     scale: .7,
        //     strokeColor: 'white',
        //     strokeWeight: .10,
        //     fillOpacity: 1,
        //     fillColor: '#404040',
        //     offset: '5%',
        //     anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
        // };

        // var marker2 = new google.maps.Marker({
        //     position: new google.maps.LatLng(25.050339, 121.543912),
        //     map: map,
        //     icon: icon
        // });
    });  
</script>
<script src="<?php echo e(asset('js')); ?>/main.js?v=<?php echo e(Config::get('app.version')); ?>"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>