
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
		<?php echo e(trans('sysSite.titleName')); ?>

		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?php echo e(trans('sysSite.titleName')); ?></li>
	</ol>
</section>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('before_scripts'); ?>


<script>
	var gridOpt = {};
    gridOpt.enabledStatus = false;
    gridOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_customers')); ?>";
    gridOpt.dataUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/sys_customers')); ?>" + "?basecon=type;EQUAL;SELF&";
    gridOpt.createUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/companyProfile/create')); ?>";
    gridOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/companyProfile')); ?>" + "/{id}/edit";

    var btnGroup = [
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "<?php echo e(trans('common.exportExcel')); ?>",
            btnFunc: function () {
                $("#jqxGrid").jqxGrid('exportdata', 'xls', '<?php echo e(trans("sysSite.titleName")); ?>');
            }
        },
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "<?php echo e(trans('common.gridOption')); ?>",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        {
            btnId:"btnAdd",
            btnIcon:"fa fa-edit",
            btnText:"<?php echo e(trans('common.add')); ?>",
            btnFunc:function(){
                location.href= gridOpt.createUrl;
            }
        }, 
        {
            btnId:"btnDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"<?php echo e(trans('common.delete')); ?>",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    ids.push(row.id);
                }
                }
                if(ids.length == 0) {
                swal("請至少選擇一筆資料", "", "warning");
                }
                $.post(BASE_URL + '/custProfile/multi/del', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("刪除成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });
            }
        }
    ];

</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('backpack::template.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>