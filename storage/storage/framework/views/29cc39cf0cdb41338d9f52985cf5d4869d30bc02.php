

<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
      <?php echo e(trans('modCar.titleAddName')); ?><small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url(config('backpack.base.route_prefix'),'carProfile')); ?>"><?php echo e(trans('modCar.titleName')); ?></a></li>
        <li class="active"><?php echo e(trans('modCar.titleAddName')); ?></li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php echo $__env->make('backpack::template.toolbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?php echo e(trans('modCar.titleAddName')); ?></a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="cust_no"><?php echo e(trans('modCar.custNo')); ?></label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="car_no"><?php echo e(trans('modCar.carNo')); ?></label>
                                        <input type="text" class="form-control" id="car_no" name="car_no" placeholder="Enter No.">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="dlv_type"><?php echo e(trans('modCar.dlvType')); ?></label>
                                        <select type="text" class="form-control" id="dlv_type" name="dlv_type" ></select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="car_type"><?php echo e(trans('modCar.carType')); ?></label>
                                        <input type="text" class="form-control" id="car_type" name="car_type"  placeholder="Enter Car Type" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="car_type_nm"><?php echo e(trans('modCar.carTypeNm')); ?></label>
                                        <input type="text" class="form-control" id="car_type_nm" name="car_type_nm">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="car_ton"><?php echo e(trans('modCar.carTon')); ?></label>
                                        <input type="text" class="form-control" id="car_ton" name="car_ton" >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="cbm"><?php echo e(trans('modCar.cbm')); ?></label>
                                        <input type="number" class="form-control" id="cbm" name="cbm" min="0.01" max="9999999999999999.99">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="cbmu"><?php echo e(trans('modCar.cbmu')); ?></label>
                                        <input type="text" class="form-control" id="cbmu" name="cbmu" >
                                    </div>
                                   <div class="form-group col-md-4">
                                        <label for="load_rate"><?php echo e(trans('modCar.loadRate')); ?></label>
                                        <input type="number" class="form-control" id="load_rate" name="load_rate" min="0.01" max="100.00">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="load_weight"><?php echo e(trans('modCar.loadWeight')); ?></label>
                                        <input type="number" class="form-control" id="load_weight" name="load_weight" min="0.01" max="9999999999999999.99">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="driver_no"><?php echo e(trans('modCar.driverNo')); ?></label>
                                        <input type="text" class="form-control" id="driver_no" name="driver_no" >
                                    </div>
                                   <div class="form-group col-md-4">
                                        <label for="driver_nm"><?php echo e(trans('modCar.driverNm')); ?></label>
                                        <input type="text" class="form-control" id="driver_nm" name="driver_nm" >
                                    </div>
                                </div>
                                <div class="row">                                  
                                    <div class="form-group col-md-4">
                                        <label for="person_no"><?php echo e(trans('modCar.personNo')); ?></label>
                                        <input type="text" class="form-control" id="person_no" name="person_no" >
                                    </div>
                                   <div class="form-group col-md-4">
                                        <label for="person_nm"><?php echo e(trans('modCar.personNm')); ?></label>
                                        <input type="text" class="form-control" id="person_nm" name="person_nm" >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by"><?php echo e(trans('modCar.createdBy')); ?></label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at"><?php echo e(trans('modCar.createdAt')); ?></label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by"><?php echo e(trans('modCar.updatedBy')); ?></label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at"><?php echo e(trans('modCar.updatedAt')); ?></label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                <?php if(isset($id)): ?>
                                    <input type="hidden" name="id" value="<?php echo e($id); ?>" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                <?php endif; ?>

                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->                    
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>    


<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->startSection('after_scripts'); ?>


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/carProfile')); ?>";

    var fieldData = null;
    var fieldObj = null;

    <?php if(isset($crud->create_fields)): ?>
        fieldData = '{<?php echo json_encode($crud->create_fields); ?>}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
    <?php endif; ?>

    
    
    <?php if(isset($id)): ?>
        mainId = "<?php echo e($id); ?>";
        editData = '{<?php echo $entry; ?>}';
        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        editObj = JSON.parse(editData);
        cust_no= editObj.cust_no;
        console.log(editObj);
    <?php endif; ?>

  
   

    $(function(){

        $( "#lookupEvent" ).on( "custContactFunc", function(event,indexs) {
            /*$.each(indexs,function(k,v){
                alert(k+v);
            });*/
            alert(1);
        });
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/carProfile')); ?>";
        formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_car')); ?>";
        formOpt.saveUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/carProfile')); ?>";
        
        formOpt.initFieldCustomFunc = function (){
            
        };
        setField.disabled("myForm",["created_by","created_at","car_type_nm","updated_by","updated_at","driver_nm","person_nm"]);

    })
</script>    

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>