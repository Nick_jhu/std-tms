 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
		<?php echo e(trans('excel.titleName')); ?>

		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?php echo e(trans('excel.titleName')); ?></li>
	</ol>
</section>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('content'); ?>
<style>
	.font-white {
		color: white
	}
	button {
		margin-right: 10px;
	}
</style>

<div class="row">
	<div class="col-md-10">
		<div class="callout callout-danger" id="errorMsg" style="display:none">
			<h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4>
			<ul>

			</ul>
		</div>
		<form method="POST" accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
			<?php echo e(csrf_field()); ?>

			<p>
				<a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/orderExcelSample/download')); ?>" class="btn btn-primary">Excel 範例下載</a>
				<input type="file" name="import_file" id="importFile" class="btn-primary font-white" />
			</p>
			<button type="submit" class="btn btn-primary" id="btnImport"><?php echo e(trans('excel.btnImport')); ?></button>
			<button type="button" class="btn btn-primary" id="btnDel"><?php echo e(trans('excel.btnDel')); ?></button>
			<button type="button" class="btn btn-primary" id="btnSave"><?php echo e(trans('excel.btnSave')); ?></button>
			<?php if(Auth::user()->roles->first()->name == "Admin"): ?>
			<button type="button" class="btn btn-primary" id="btnLayout">欄位調整</button>
			<?php endif; ?>

			<div id="jqxGrid" style="margin-top: 10px"></div>
		</form>
	</div>
</div>


<?php $__env->stopSection(); ?> 
<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('after_scripts'); ?>

<script type="text/javascript" src="<?php echo e(asset('vendor/jqwidgets')); ?>/jqxwindow.js"></script>
<script type="text/javascript" src="<?php echo e(asset('js')); ?>/bootstrap.file-input.js"></script>

<script>
	$(function(){
		$('#importFile').bootstrapFileInput();
		$.jqx.theme = "bootstrap";
		var source =
		{
			datatype: "json",
			datafields: [
				{ name: 'dlv_cust_nm', type: 'string' },
				{ name: 'etd', type: 'date',cellsformat: 'yyyy-MM-dd' },
				{ name: 'driver', type: 'string' },
				{ name: 'remark', type: 'string' },
				{ name: 'ord_no', type: 'string' },
				{ name: 'dlv_attn', type: 'string' },
				{ name: 'dlv_email', type: 'string' },
				{ name: 'pkg_num', type: 'float' },
				{ name: 'total_cbm', type: 'float' },
				{ name: 'total_gw', type: 'float'},
				{ name: 'dlv_addr', type: 'string' },
				{ name: 'dlv_tel', type: 'string' },
				{ name: 'pick_attn', type: 'string' },
				{ name: 'pick_addr', type: 'string' },
				{ name: 'pick_tel', type: 'string' },
				{ name: 'distance', type: 'float' }
			],
			root:"Rows",
			pagenum: 1,					
			beforeprocessing: function (data) {
				source.totalrecords = data[0].TotalRows;
			},
			filter: function () {
			// update the grid and send a request to the server.
				$("#jqxGrid").jqxGrid('updatebounddata', 'filter');
			},
			sort: function () {
				// update the grid and send a request to the server.
				$("#jqxGrid").jqxGrid('updatebounddata', 'sort');
			},
			pagesize: 20,
			url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/ExcelImport/get')); ?>",					
		};
		var dataAdapter = new $.jqx.dataAdapter(source, {
			loadComplete: function () {

			}
		});
		//var dataAdapter = new $.jqx.dataAdapter(source);
		$.get(BASE_API_URL+'/admin/baseApi/getFieldsJson/mod_order', {'key': 'excelOrderGrid'}, function(data){
			columns = [
				{ text: "<?php echo e(trans('excel.custName')); ?>", datafield: 'dlv_cust_nm', width: 250 },
				{ text: "<?php echo e(trans('excel.etd')); ?>", datafield: 'etd', width: 150,cellsformat: 'yyyy-MM-dd' },
				{ text: "<?php echo e(trans('excel.driver')); ?>", datafield: 'driver', width: 180 },
				{ text: "<?php echo e(trans('excel.remark')); ?>", datafield: 'remark', width: 120 },
				{ text: "<?php echo e(trans('excel.orderNo')); ?>", datafield: 'ord_no' },
				{ text: "<?php echo e(trans('excel.dlvAttn')); ?>", datafield: 'dlv_attn', width: 120 },
				{ text: "<?php echo e(trans('excel.dlvEmail')); ?>", datafield: 'dlv_email', width: 120 },
				{ text: "<?php echo e(trans('excel.pkgNum')); ?>", datafield: 'pkg_num', width: 120, cellsalign: 'right' },
				{ text: "<?php echo e(trans('excel.totalCbm')); ?>", datafield: 'total_cbm', width: 120, cellsalign: 'right' },
				{ text: "<?php echo e(trans('excel.totalGw')); ?>", datafield: 'total_gw', width: 120, cellsalign: 'right' },
				{ text: "<?php echo e(trans('excel.dlvAddr')); ?>", datafield: 'dlv_addr', width: 120 },
				{ text: "<?php echo e(trans('excel.dlvTel')); ?>", datafield: 'dlv_tel', width: 120, cellsalign: 'right' },
				{ text: "<?php echo e(trans('excel.pickAttn')); ?>", datafield: 'pick_attn', width: 120 },
				{ text: "<?php echo e(trans('excel.pickAddr')); ?>", datafield: 'pick_addr', width: 120 },
				{ text: "<?php echo e(trans('excel.pickTel')); ?>", datafield: 'pick_tel', width: 120,cellsalign: 'right' },
				{ text: "<?php echo e(trans('excel.distance')); ?>", datafield: 'distance', width: 120,cellsalign: 'right' }
			];

			$("#jqxGrid").jqxGrid({
				width: '100%',
				height: $(".content-wrapper").height() - 150,
				source: dataAdapter,
				selectionmode: 'checkbox',
				columnsresize: true,
				rendergridrows: function (params) {
					//alert("rendergridrows");
					return params.data;
				},
				ready: function () {
					$('#jqxGrid').jqxGrid('autoresizecolumns');
					if(typeof data[2] !== "undefined") {
						$("#jqxGrid").jqxGrid('loadstate', data[2]);
					}
					loadState();
				},
				columns: data[1]
			});
			
		}, 'JSON');

		
			
	
		$("#myForm").submit(function () {
			if ($('#importFile').get(0).files.length === 0) {
				swal("<?php echo e(trans('excel.msg1')); ?>", "", "warning");
				return false;
			}
			var postData = new FormData($(this)[0]);
			$.ajax({
				url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/ExcelImport/importExcel')); ?>",
				type: 'POST',
				data: postData,
				async: false,
				beforeSend: function () {
					
				},
				error: function () {
					swal("<?php echo e(trans('excel.msg2')); ?>", "", "error");
					// swal("<?php echo e(trans('excel.msg2')); ?>", {
					// 	icon: "error",
					// });
					return false;
				},
				success: function (data) {
					//alert(data);
					if(data.msg == "error") {
						swal("<?php echo e(trans('excel.msg2')); ?>", "", "error");
						// swal("<?php echo e(trans('excel.msg2')); ?>", {
						// icon: "error",
						// });
					}
					$('#jqxGrid').jqxGrid('updatebounddata');
					// var dataAdapter = new $.jqx.dataAdapter(data.data);
					// $("#jqxGrid").jqxGrid({ source: dataAdapter });
				},
				cache: false,
				contentType: false,
				processData: false
			});
			return false;
		});
		
		$("#btnDel").on("click",function(event){
            //event.stopPropagation()
			var rowIndexes = $('#jqxGrid').jqxGrid('getselectedrowindexes');
			if(rowIndexes.length===0){
				swal("<?php echo e(trans('excel.msg3')); ?>", "", "warning");
				return;
			}
			var rowIds = new Array();
			for (var i = 0; i < rowIndexes.length; i++) {
				var currentId = $('#jqxGrid').jqxGrid('getrowid', rowIndexes[i]);
				rowIds.push(currentId);
			};
			$('#jqxGrid').jqxGrid('deleterow', rowIds);
			$('#jqxGrid').jqxGrid('clearselection');

		});    
		
		$("#btnSave").on("click",function(event){
			var rows = $('#jqxGrid').jqxGrid('getrows');
			swal({
				title: "<?php echo e(trans('excel.msg4')); ?>?",
				text: "<?php echo e(trans('excel.msg5')); ?>!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: "<?php echo e(trans('excel.btnSave')); ?>",
				cancelButtonText: "<?php echo e(trans('excel.btnCancel')); ?>"
				})
				.then((result) => {
				if (result.value) {
				//var postData = new FormData($('#myForm')[0]);
				// $.post("<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/ExcelImport/store')); ?>",{'obj':JSON.stringify(rows)}, function(data){

				// }, 'JSON');
				$.ajax({
				url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/ExcelImport/store')); ?>",
				type: "POST",				
				data: {'obj':JSON.stringify(rows)},
				async: false,
				dataType:"json",
				beforeSend: function () {
					loadingFunc.show();
				},
				error: function (jqXHR, exception) {
					swal("<?php echo e(trans('excel.msg6')); ?>", "", "error");
					// swal("<?php echo e(trans('excel.msg6')); ?>", {
					// 	icon: "error",
					// });
					loadingFunc.hide();
					return false;
				},
				success: function (data) {
					if(data.msg == "success") {
						swal("<?php echo e(trans('excel.msg7')); ?>", "", "success");
						// swal("<?php echo e(trans('excel.msg7')); ?>", {
						// icon: "success",
						// });
					}else{
						swal("<?php echo e(trans('excel.msg6')); ?>", "", "error");
						// swal("<?php echo e(trans('excel.msg6')); ?>", {
						// icon: "error",
						// });
					}
					$('#jqxGrid').jqxGrid('updatebounddata');
					loadingFunc.hide();
					return false;
				}

					});	
				
				} else {
					//swal("Your imaginary file is safe!");
				}
			});
        });

		$("#btnLayout").on("click", function(){
			$('#gridOptModal').modal('show');
		});

		$("#saveGrid").on("click", function(){
			var items = $("#jqxlistbox").jqxListBox('getItems');
			$("#jqxGrid").jqxGrid('beginupdate');

			$.each(items, function(i, item) {
				var thisIndex = $('#jqxGrid').jqxGrid('getcolumnindex', item.value)-1;
				if(thisIndex != item.index){
					//console.log(item.value+":"+thisIndex+"="+item.index);
					$('#jqxGrid').jqxGrid('setcolumnindex', item.value,  item.index);
				}
				if (item.checked) {
					$("#jqxGrid").jqxGrid('showcolumn', item.value);
				}
				else {
					$("#jqxGrid").jqxGrid('hidecolumn', item.value);
				}
			})
			
			$("#jqxGrid").jqxGrid('endupdate');
			var state = $("#jqxGrid").jqxGrid('getstate');

			var saveUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/saveLayoutJson')); ?>"; 	  	  	
			var stateToSave = JSON.stringify(state);

			$.ajax({
				type: "POST",										
				url: saveUrl,		
				data: { data: stateToSave,key: "excelOrderGrid" },		 
				success: function(response) {
					if(response == "true"){
						//alert("save successful");
						swal("save successful", "", "success");
						$('#gridOptModal').modal('hide');
					}else{
						swal("save failded", "", "error");
					}
					
				}
			});	
		});

		function loadState() {  	
			var loadURL = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/getLayoutJson')); ?>"; 	  	  	
						
			$.ajax({
				type: "GET", //  OR POST WHATEVER...
				url: loadURL,
				data: { key: 'excelOrderGrid' },		 
				success: function(response) {										
					if (response != "") {	
						response = JSON.parse(response);
					}
					var listSource = [];
					state = $("#jqxGrid").jqxGrid('getstate');
					$.each(state.columns, function(i, item) {
						if(item.text != "" && item.text != "undefined"){
							listSource.push({ 
							label: item.text, 
							value: i, 
							checked: !item.hidden });
						}
					});
					$("#jqxlistbox").jqxListBox({ 
						allowDrop: true, 
						allowDrag: true,
						source: listSource,
						width: "99%",
						height: 200,
						checkboxes: true,
						filterable: true,
						searchMode: 'contains'
					});
				}
			});			  	  	  	  	
		}
    });

</script>

<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(trans('common.close')); ?></button>
				<button type="button" class="btn btn-primary" id="saveGrid"><?php echo e(trans('common.saveChange')); ?></button>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>