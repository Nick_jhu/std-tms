 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
	<?php echo e(trans('sysRefFee.titleNameDelivery')); ?><small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?php echo e(trans('sysRefFee.titleNameDelivery')); ?></li>
	</ol>
</section>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('content'); ?>
<style>
	.font-white { color: white}
</style>
		<div class="row">
			<div class="col-md-12">
				<div class="callout callout-danger" id="errorMsg" style="display:none">
					<h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4>
					<ul>

					</ul>
				</div>		
				<form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">

							<?php echo e(csrf_field()); ?>

							<input type="file" name="import_file" id="importFile" class="btn-primary font-white"/>
							<button type="submit" class="btn btn-primary" id="btnImport"><?php echo e(trans('excel.btnImport')); ?></button>						
							<a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/download')); ?>" class="btn btn-primary"><?php echo e(trans('sysRefFee.downloadExample')); ?></a>
				</form>
			</div>
		</div>
		<div class="box box-primary" id="subBox" style="display:none">
			
			<form method="POST" accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="form-group col-md-4">
							<input type="hidden" id="trans_type_cd" name="trans_type_cd">
							<input type="hidden" id="type" name="type">
							<label for="car_type_nm"><?php echo e(trans('sysRefFee.transTypeNm')); ?></label>
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="trans_type_nm" name="trans_type_nm">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-flat lookup" btnname="trans_type_nm"
									info1="<?php echo e(Crypt::encrypt('bscode')); ?>" 
									info2="<?php echo e(Crypt::encrypt('cd+cd_descp,cd,cd_descp')); ?>" 
									info3="<?php echo e(Crypt::encrypt('cd_type=\'DELIVERYTYPE\'')); ?>"
									info4="cd=trans_type_cd;cd_descp=trans_type_nm" triggerfunc="" selectionmode="singlerow">
									<i class="fa fa-search" aria-hidden="true"></i>
								</button>
							</span>
						</div>
						</div>
						<div class="form-group col-md-4">
							<label for="fee_op"><?php echo e(trans('sysRefFee.feeOp')); ?></label>
							<input type="text" class="form-control input-sm" name="fee_op" grid="true" >
						</div>
						<div class="form-group col-md-4">
							<label for="fee_weight"><?php echo e(trans('sysRefFee.feeWeight')); ?></label>
							<input type="text" class="form-control input-sm" name="fee_weight" grid="true" >
						</div>
					</div>
					<div class="row">											
						<div class="form-group col-md-4">
							<label for="fee"><?php echo e(trans('sysRefFee.fee')); ?></label>
							<input type="text" class="form-control input-sm" name="fee" grid="true" >
						</div>
						<div class="form-group col-md-8">
							<label for="remark"><?php echo e(trans('sysRefFee.remark')); ?></label>
							<input type="text" class="form-control input-sm" name="remark" grid="true" >
						</div>																					
					</div>																		
				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					<input type="hidden" class="form-control input-sm" name="id" grid="true">
					<button type="button" class="btn btn-sm btn-primary" id="Save"><?php echo e(trans('common.save')); ?></button>
					<button type="button" class="btn btn-sm btn-danger" id="Cancel"><?php echo e(trans('common.cancel')); ?></button>
				</div>
			</form>
		</div>

		<div id="jqxGrid"></div>


 <?php $__env->stopSection(); ?> 
 <?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
 <?php $__env->startSection('after_scripts'); ?>
 <script type="text/javascript" src="<?php echo e(asset('js')); ?>/bootstrap.file-input.js"></script>
	<script>
		var mainId = "";

		$(function () {			
			$('#importFile').bootstrapFileInput();
			$('button[btnName="trans_type_nm"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('trans_type_nm', "<?php echo e(trans('sysRefFee.transTypeNm')); ?>", callBackFunc=function(data){
                
           		});
        	});
			$('#trans_type_nm').on('click', function(){
				var check = $('#trans_type_nm').data('ui-autocomplete') != undefined;
				if(check == false) {
					initAutocomplete("subForm","trans_type_nm",callBackFunc=function(data){
						
					});
				}
			});

			
			mainId = "mainId"
			$.ajax({
				url: "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_ref_fee')); ?>"+ "?basecon=type;EQUAL;D",					
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {
					console.log(fieldData);
					var custFieldData = [];
					custFieldData[0] = [
				{name: 'id', type: 'integer'},
				{name: 'trans_type_nm', type: 'string'},
				{name: 'fee_op', type: 'string'},
				{name: 'fee_weight', type: 'float'},
				{name: 'fee', type: 'float'},			
				{name: 'remark', type: 'string'},
			];
			custFieldData[1] = [
				{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false},
				{text:"<?php echo e(trans('sysRefFee.transTypeNm')); ?>", datafield: 'trans_type_nm', filtertype:"textbox", width:220, editable: false},
				{text:"<?php echo e(trans('sysRefFee.feeOp')); ?>", datafield: 'fee_op', filtertype:"textbox", width:100, editable: false},
				{text:"<?php echo e(trans('sysRefFee.feeWeight')); ?>", datafield: 'fee_weight', filtertype:"number", width:100, editable: false},
				{text:"<?php echo e(trans('sysRefFee.fee')); ?>", datafield: 'fee', filtertype:"number", width:100},				
				{text:"<?php echo e(trans('sysRefFee.remark')); ?>", datafield: 'remark', filtertype:"textbox", width:200, editable: false},
					];
					var opt = {};
					opt.gridId = "jqxGrid";
					opt.fieldData = custFieldData;
					opt.formId = "subForm";
					opt.saveId = "Save";
					opt.cancelId = "Cancel";
					opt.showBoxId = "subBox";
					opt.height = 300;
					opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/get')); ?>";
					opt.addUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery')); ?>" + "/store";
					opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery')); ?>" + "/update";
					opt.delUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeDelivery')); ?>" + "/delete/";
					opt.commonBtn = true;
					opt.showtoolbar = true;
					opt.defaultKey = {
						
					};
					opt.beforeSave = function (formData) {
						// $addBtn.jqxButton({disabled: false});
						// $delBtn.jqxButton({disabled: false});
						
					}
	
					opt.afterSave = function (data) {

					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			});

			$("#myForm").submit(function () {
				if ($('#importFile').get(0).files.length === 0) {
    				swal("<?php echo e(trans('sysRefFee.msg1')); ?>", "", "warning");
					return false;
				}
				var postData = new FormData($(this)[0]);
				$.ajax({
                url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/sysRefFeeDelivery/importExcel')); ?>",
                type: 'POST',
                data: postData,
                async: false,
                beforeSend: function () {
                    
                },
                error: function () {
					swal("<?php echo e(trans('sysRefFee.msg2')); ?>", "", "error");
					// swal("<?php echo e(trans('excel.msg2')); ?>", {
					// 	icon: "error",
					// });
					return false;
                },
                success: function (data) {
				   //alert(data);
				   if(data.msg == "error") {
						swal("<?php echo e(trans('sysRefFee.msg2')); ?>", "", "error");
						// swal("<?php echo e(trans('excel.msg2')); ?>", {
						// icon: "error",
						// });
					}else{
						swal("<?php echo e(trans('sysRefFee.msg3')); ?>", "", "success");
					}
				$('#jqxGrid').jqxGrid('updatebounddata');
				// var dataAdapter = new $.jqx.dataAdapter(data.data);
                // $("#jqxGrid").jqxGrid({ source: dataAdapter });
				
                },
                cache: false,
                contentType: false,
                processData: false
            	});
				return false;
			});
		})
	</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>