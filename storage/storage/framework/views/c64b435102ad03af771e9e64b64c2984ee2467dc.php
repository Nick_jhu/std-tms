

<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
      <?php echo e(trans('sysCustomers.titleAddName')); ?><small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="<?php echo e(url(config('backpack.base.route_prefix'),'customerProfile')); ?>"><?php echo e(trans('sysCustomers.titleName')); ?></a></li>
		<li class="active"><?php echo e(trans('sysCustomers.titleAddName')); ?></li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php echo $__env->make('backpack::template.toolbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">        
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?php echo e(trans('sysCustomers.baseInfo')); ?></a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cust_no"><?php echo e(trans('sysCustomers.custNo')); ?></label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cname"><?php echo e(trans('sysCustomers.cname')); ?></label>
                                        <input type="text" class="form-control" id="cname" name="cname">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="ename"><?php echo e(trans('sysCustomers.ename')); ?></label>
                                        <input type="text" class="form-control" id="ename" name="ename">
                                    </div>      
                                    <div class="form-group col-md-3">
                                        <label for="email"><?php echo e(trans('sysCustomers.email')); ?></label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input type="eamil" class="form-control" id="email" name="email" >
                                        </div>
                                    </div>                              
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cmp_abbr"><?php echo e(trans('sysCustomers.cmpAbbr')); ?></label>
                                        <input type="text" class="form-control" id="cmp_abbr" name="cmp_abbr">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="contact"><?php echo e(trans('sysCustomers.contact')); ?></label>
                                        <input type="text" class="form-control" id="contact" name="contact" =>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="status"><?php echo e(trans('sysCustomers.status')); ?></label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="A"><?php echo e(trans('sysCustomers.STATUS_A')); ?></option>
                                            <option value="B"><?php echo e(trans('sysCustomers.STATUS_B')); ?></option>
                                        </select>
                                    </div>                                                                       
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="identity"><?php echo e(trans('sysCustomers.identity')); ?></label>
                                        <select class="form-control" id="identity" name="identity" <?php if(isset($id)): ?> switch="off" <?php endif; ?> >
                                            <option value="G">集團</option>
                                            <option value="C">公司</option>
                                            <option value="S">站別</option>
                                            <option value="D">部門</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3" id="group" style="display:none">
                                        <label for="g_key">集團</label>
                                        <select class="form-control" id="g_key" name="g_key"></select>
                                    </div>
                                    <div class="form-group col-md-3" id="cmp" style="display:none">
                                        <label for="c_key">公司</label>
                                        <select class="form-control" id="c_key" name="c_key"></select>
                                    </div>
                                    <div class="form-group col-md-3" id="stn" style="display:none">
                                        <label for="d_key">站別</label>
                                        <select class="form-control" id="d_key" name="d_key"></select>
                                    </div>
                                    
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="phone"><?php echo e(trans('sysCustomers.phone')); ?></label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" class="form-control" id="phone" name="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="fax"><?php echo e(trans('sysCustomers.fax')); ?></label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-fax"></i>
                                            </div>
                                            <input type="text" class="form-control" id="fax" name="fax">
                                        </div>
                                    </div>     
                                    <div class="form-group col-md-6">
                                        <label for="address"><?php echo e(trans('sysCustomers.address')); ?></label>
                                        <input type="text" class="form-control" id="address" name="address">
                                    </div>                                                               
                                </div>     
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><?php echo e(trans('sysCustomers.remark')); ?></label>
                                            <textarea class="form-control" rows="3" name="remark"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by"><?php echo e(trans('sysCustomers.createdBy')); ?></label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at"><?php echo e(trans('sysCustomers.createdAt')); ?></label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by"><?php echo e(trans('sysCustomers.updatedBy')); ?></label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at"><?php echo e(trans('sysCustomers.updatedAt')); ?></label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                                                
                                <?php if(isset($id)): ?>
                                    <input type="hidden" name="id" value="<?php echo e($id); ?>" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                <?php endif; ?>

                            </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>   

<?php $__env->stopSection(); ?>


<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->startSection('after_scripts'); ?>

<script type="text/javascript" src="<?php echo e(asset('vendor/jqwidgets')); ?>/jqxwindow.js"></script>


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/customerProfile')); ?>";

    var fieldData = null;
    var fieldObj = null;

    <?php if(isset($crud->create_fields)): ?>
        fieldData = '{<?php echo json_encode($crud->create_fields); ?>}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
    <?php endif; ?>


    <?php if(isset($id)): ?>
        mainId = "<?php echo e($id); ?>";
        editData = '{<?php echo json_encode($entry["original"]); ?>}';

        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        //editObj = JSON.parse(editData);
        //cust_no= editObj.cust_no;
        //console.log(editObj);
    <?php endif; ?>


    $(function(){
        
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/companyProfile')); ?>";
        //formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_customers')); ?>";
        formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') .'/get/sys_customers')); ?>/" + mainId;
        formOpt.saveUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/companyProfile')); ?>";
        
        formOpt.initFieldCustomFunc = function (){
            $("select[name='state[]']").select2({tags: true});
        };

        formOpt.addFunc = function() {
            $("#status").val("B");
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);      


        $("#identity").on("change", function(){
            var key = $(this).val();

            if(key == "C") {
                $("#group").show();
                $("#cmp").hide();
                $("#stn").hide();
            }
            else if(key == "S") {
                $("#group").show();
                $("#cmp").show();
                $("#stn").hide();
            }
            else if(key == "D") {
                $("#group").show();
                $("#cmp").show();
                $("#stn").show();
            }
            else {
                $("#group").hide();
                $("#cmp").hide();
                $("#stn").hide();
            }

            $.get(BASE_URL+'/gcsd/get', {}, function(data){
                if(data.gData.length > 0) {
                    var opt = "";
                    for(i in data.gData) {
                        opt += '<option value="'+data.gData[i].cust_no+'">'+data.gData[i].cname+'</option>'; 
                    }
                    $("#g_key").html(opt);
                }

                if(data.cData.length > 0) {
                    var opt = "";
                    for(i in data.cData) {
                        opt += '<option value="'+data.cData[i].cust_no+'">'+data.cData[i].cname+'</option>'; 
                    }
                    $("#c_key").html(opt);
                }

                if(data.sData.length > 0) {
                    var opt = "";
                    for(i in data.sData) {
                        opt += '<option value="'+data.sData[i].cust_no+'">'+data.sData[i].cname+'</option>'; 
                    }
                    $("#s_key").html(opt);
                }
            });
        });
    })
</script>    

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>