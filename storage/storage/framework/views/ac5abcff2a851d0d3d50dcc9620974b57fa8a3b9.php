

<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>        
<?php echo e(trans('modQuot.titleName')); ?><small></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><?php echo e(trans('backpack::logmanager.existing_logs')); ?></li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('before_scripts'); ?>


<script>
var gridOpt = {};
gridOpt.enabledStatus = true;
gridOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_quot')); ?>";
gridOpt.dataUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_quot')); ?>";
gridOpt.createUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/QuotMgmt/create')); ?>";
gridOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/QuotMgmt')); ?>" + "/{id}/edit";
gridOpt.height = 800;

var btnGroup = [
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "<?php echo e(trans('common.gridOption')); ?>",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"<?php echo e(trans('common.add')); ?>",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"<?php echo e(trans('common.delete')); ?>",
    btnFunc:function(){
      swal("<?php echo e(trans('common.deleteMsg')); ?>", "", "success");
    }
  },
  {
    btnId:"btnDownload",
    btnIcon:"fa fa-download",
    btnText:"<?php echo e(trans('modQuot.downloadQuote')); ?>",
    btnFunc:function(){
      //swal('已發送', "派車訊息已發送至app", "success");
    }
  }, 
];
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::template.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>