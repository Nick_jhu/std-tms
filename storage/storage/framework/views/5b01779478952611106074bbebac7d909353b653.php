<!-- Modal -->
<div class="modal fade" id="lookupModal" tabindex="-1" role="dialog" aria-labelledby="lookupTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="lookupTitle">Modal title</h5>
      </div>
      <div class="modal-body">
        <div id="lookupGrid"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  <input type="hidden" id="lookupEvent" name="lookupEvent" />
</div>
<?php $__env->startSection('lookup_scripts'); ?>

<script type="text/javascript" src="<?php echo e(asset('js/core')); ?>/grid-core.js"></script>
<script>
function initLookup(fieldName, title, callBackFunc=null){

  var fieldObj = $('button[btnName="'+fieldName+'"]');
  fieldObj.info1 = fieldObj.attr("info1");
  fieldObj.info2 = fieldObj.attr("info2");
  fieldObj.info3 = fieldObj.attr("info3");
  fieldObj.info4 = fieldObj.attr("info4");
  fieldObj.selectionmode = fieldObj.attr("selectionmode");
  fieldObj.triggerfunc = fieldObj.attr("triggerfunc");
  var gridOpt = {};
  gridOpt.gridId = "lookupGrid";
  gridOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getLookupFieldsJson')); ?>"+"/"+fieldObj.info1 +"/"+fieldObj.info2;
  gridOpt.dataUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getLookupGridJson')); ?>"+"/"+fieldObj.info1+"/"+fieldObj.info2+"/"+fieldObj.info3;
  
  if(fieldObj.selectionmode === "undefined" || fieldObj.selectionmode === "singlerow"){
    gridOpt.selectionmode = "singlerow";
  }else if(fieldObj.selectionmode === "checkbox"){
    gridOpt.selectionmode = "checkbox";
  }

  
  gridOpt.height = 500;
  $('#lookupTitle').text(title);
  var docHeight = $( document ).height();
  if(docHeight < 800) {
      gridOpt.height = 300;
  }
  $.get( gridOpt.fieldsUrl, function( fieldData ) {
      console.log(fieldData);
      initGrid(gridOpt.gridId, fieldData, gridOpt.dataUrl,gridOpt);
  });


  $('#'+gridOpt.gridId).off().on('rowdoubleclick', function (event) 
  { 
      var args = event.args;
      // row's bound index.
      var boundIndex = args.rowindex;
      // row's visible index.
      var visibleIndex = args.visibleindex;
      // right click.
      var rightclick = args.rightclick; 
      // original event.
      var ev = args.originalEvent;


      var rowindexes = $('#'+gridOpt.gridId).jqxGrid('getselectedrowindexes');
      if(gridOpt.selectionmode === "checkbox" && rowindexes.length > 0){
        
      }else{
        var datarow = $('#'+gridOpt.gridId).jqxGrid('getrowdata', boundIndex);
        console.log(datarow);

        var fieldMapping = fieldObj.info4.split(";");
      
        $.each(fieldMapping,function(k,v){
          k = v.split("=");
          $("#"+k[1]).val(datarow[k[0]]);
        });
      }
      $("#lookupEvent").trigger(fieldObj.triggerfunc,[rowindexes]);

      if(callBackFunc !== null) {
        callBackFunc(datarow);
      }
     
      $('#lookupModal').modal('hide');
  });
}

function initAutocomplete(formId,fieldName,callBackFunc=null,btnName=""){
  var fieldObj = $('button[btnName="'+fieldName+'"]');
  fieldObj.info1 = fieldObj.attr("info1");
  fieldObj.info2 = fieldObj.attr("info2");
  fieldObj.info3 = fieldObj.attr("info3");
  fieldObj.info4 = fieldObj.attr("info4");
  var fieldMapping = fieldObj.info4.split(";");

  if(btnName != "") {
    fieldName = btnName;
  }

  $('#'+formId+' input[name="'+fieldName+'"]').autocomplete({
      source: function( request, response ) {
          $.getJSON( "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getAutocompleteJson')); ?>"+"/"+fieldObj.info1+"/"+fieldObj.info2+"/"+fieldObj.info3+"/"+request.term, function (data) {
            response(data);
        });
        },
      minLength: 1,
      autoFocus: true,
      select: function( event, ui ) {
        console.log(ui);
        var thisId = $(this).attr("id");
        $.each(fieldMapping,function(k,v){
          k = v.split("=");
          if(thisId == k[1]){
            ui.item.value = ui.item[k[0]];
          }else{
            console.log(k[1]);
            $("#"+k[1]).val(ui.item[k[0]]);
          }
        });
        if(callBackFunc !== null) {
          callBackFunc(ui.item);
        }
      },
      change: function( event, ui ) {
        //console.log(ui);
        if(ui.item === null){
          $.each(fieldMapping,function(k,v){
            k = v.split("=");
            $("#"+k[1]).val("");
          });
        }
      }
  });
}
</script>    


<?php $__env->stopSection(); ?>