

<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
      <?php echo e(trans('bscodeKind.titleName')); ?><small></small>
      </h1>
      <ol class="breadcrumb">
      <li class="active"><?php echo e(trans('bscodeKind.titleName')); ?></li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('before_scripts'); ?>


<script>
var gridOpt = {};
gridOpt.enabledStatus = false;
gridOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/bscode_kind')); ?>";
gridOpt.dataUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/bscode_kind')); ?>";
gridOpt.createUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind/create')); ?>";
gridOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind')); ?>" + "/{id}/edit";

var btnGroup = [
  {
    btnId: "btnExportExcel",
    btnIcon: "fa fa-cloud-download",
    btnText: "<?php echo e(trans('common.exportExcel')); ?>",
    btnFunc: function () {
      $("#jqxGrid").jqxGrid('exportdata', 'xls', '<?php echo e(trans("bscodeKind.titleName")); ?>');
    }
  },
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "<?php echo e(trans('common.gridOption')); ?>",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"<?php echo e(trans('common.add')); ?>",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"<?php echo e(trans('common.delete')); ?>",
    btnFunc:function(){
      alert("Delete");
    }
  }
];
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::template.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>