<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [
        'custom-ftp' => [
            'driver' => 'ftp',

            'host' => '61.222.102.21',
            
            'port'     => 20710,

            'username' => 'Jusda',
						
            'password' => 'JUSDA_2071011303',
            'root' => '/A0007',
        ],
        'custom-sftp' => [
            'driver' => 'sftp',

            'host' => '60.250.27.6',
            
            'port'     => 22,

            'username' => 'Jusda',
						
            'password' => 'Jusda_2071',
            'root' => '/',
        ],
		'syl-sftp' => [
            'driver' => 'sftp',

            'host' => '61.222.102.21',
            
            'port'     => 20711,

            'username' => 'TMS',
						
            'password' => 'Oaw;??',
            'root' => '/',
        ],
		'pchome' => [
            'driver' => 'sftp',

            'host' => '61.222.102.21',
            
            'port'     => 20711,

            'username' => 'TMS',
						
            'password' => 'Oaw;??',
            'root' => '/PCHOME/Delivery',
        ],
        'pchomeeod' => [
            'driver' => 'sftp',

            'host' => '61.222.102.21',
            
            'port'     => 20711,

            'username' => 'TMS',
						
            'password' => 'Oaw;??',
            'root' => '/PCHOME/BACKEOD',
        ],
        'pchomedc' => [
            'driver' => 'sftp',

            'host' => '61.222.102.21',
            
            'port'     => 20711,

            'username' => 'TMS',
						
            'password' => 'Oaw;??',
            'root' => '/PCHOME/TDL',
        ],
        'IDL' => [
            'driver' => 'ftp',

            'host' => '60.251.251.147',
            
            'port'     => 21,

            'username' => 'SY',
						
            'password' => 'UndTvd6$',
            'root' => '/',
        ],
        'uploads' => [
            'driver' => 'local',
            'root' => public_path('uploads'),
        ],
        'report' => [
            'driver' => 'local',
            'root' => storage_path('app/report'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        'uploadslocal' => [
            'driver' => 'local',
            'root' => public_path(),
        ],
        // used for Backpack/BackupManager
        'backups' => [
            'driver' => 'local',
            'root'   => storage_path('backups'), // that's where your backups are stored by default: storage/backups
        ],
        // used for Backpack/LogManager
        'local' => [
            'driver' => 'local',
            'root'   => storage_path('app/'),
        ],
        'storage' => [
            'driver' => 'local',
            'root'   => storage_path(),
        ],
        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        'txt' => [
            'driver' => 'local',
            'root' => storage_path('app/txt'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],
		'azure' => [
            'driver'    => 'azure',
            'name'      => env('AZURE_STORAGE_NAME', 'bqtwecstorage'),
            'key'       => env('AZURE_STORAGE_KEY', 'ycxkQCkrcS/mueDD2QviocSUTcSmeJ2p8Ofl1yYHWqrcXBbID67fQQjca5qSDzN36YJ+D99myifEGXIpyG30Sg=='),
            'container' => env('AZURE_STORAGE_CONTAINER', 'tw-ec-image-storage'),
            'url'       => env('AZURE_STORAGE_URL',"https://bqtwecstorage.blob.core.windows.net/tw-ec-image-storage"),
            'prefix'    => null,
        ],
        'senao' => [
            // 神腦正式運作SFTP
            // -------------------------
            // 主機:sysenao.assftp.com 連接埠:20711
            // 帳號:senao_12228473
            // 密碼:QfGn-?
            'driver' => 'sftp',

            'host' => 'sysenao.assftp.com',
            
            'port'     => 20711,

            'username' => 'senao_12228473',
						
            'password' => 'QfGn-?',
            'root' => '/12228473_senao/bak',
        ],

    ],

];
