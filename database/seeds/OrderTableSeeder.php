<?php

use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        $row = 1;
        setlocale(LC_ALL, 'zh_TW');
        if (($handle = fopen("./order.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;

                DB::table('mod_order')->insert([
                    'etd'         => mb_convert_encoding($data[0], "UTF-8"),
                    'remark'      => mb_convert_encoding($data[1], "UTF-8"),
                    'ord_no'      => mb_convert_encoding($data[2], "UTF-8"),
                    'dlv_cust_nm' => mb_convert_encoding($data[3], "UTF-8"),
                    'pkg_num'     => mb_convert_encoding($data[4], "UTF-8"),
                    'cbm'         => mb_convert_encoding($data[5], "UTF-8"),
                    'dlv_addr'    => mb_convert_encoding($data[6], "UTF-8"),
                    'status'      => 'UNTREATED',
                    'g_key'       => 'STD',
                    'c_key'       => 'STD',
                    'd_key'       => 'STD',
                    's_key'       => 'STD',
                ]);

                $orderDetailId = DB::table('mod_order_detail')->insertGetId([
                    'ord_no'   => mb_convert_encoding($data[2], "UTF-8"),
                    'goods_no' => str_random(10),
                    'goods_nm' => '冷氣',
                    'pkg_num'  => mb_convert_encoding($data[4], "UTF-8"),
                    'pkg_unit' => '箱',
                    'cbm'      => mb_convert_encoding($data[5], "UTF-8")
                ]);
                
                $pack_no = str_random(10);
                DB::table('mod_order_pack')->insert([
                    'pack_no'   => $pack_no,
                    'ord_no'    => mb_convert_encoding($data[2], "UTF-8"),
                    'total_num' => mb_convert_encoding($data[4], "UTF-8"),
                    'cbm'       => mb_convert_encoding($data[5], "UTF-8"),
                ]);

                DB::table('mod_order_pack_detail')->insert([
                    'pack_no'       => $pack_no,
                    'ord_no'        => mb_convert_encoding($data[2], "UTF-8"),
                    'ord_detail_id' => $orderDetailId,
                    'num'           => mb_convert_encoding($data[4], "UTF-8")
                ]);
            }
            fclose($handle);
        }
        return;
        */

        $json = file_get_contents('./32zip.json');
        $data = json_decode($json, TRUE);
        
        dd($data[1]);

        foreach($data as $key=>$val) {
            foreach($val as $key1=>$val1) {
                DB::table('sys_area')->insert([
                    'cntry_cd'   => 'TW',
                    'cntry_nm'   => '台灣',
                    'city_nm'    => $key,
                    'dist_cd'    => $val1,
                    'dist_nm'    => $key1,
                    'zip_f'      => $val1,
                    'zip_e'      => $val1,
                    'created_by' => 'tim@standard-info.com',
                    'updated_by' => 'tim@standard-info.com',
                    'g_key'      => 'STD',
                    'c_key'      => 'STD',
                    'd_key'      => 'STD',
                    's_key'      => 'STD',
                ]);
            }
            
        }
        
        return;
    }
}
